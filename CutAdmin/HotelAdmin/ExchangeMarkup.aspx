﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelAdmin/Master.Master" AutoEventWireup="true" CodeBehind="ExchangeMarkup.aspx.cs" Inherits="CutAdmin.HotelAdmin.ExchangeMarkup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    <script>
        $(function () {
            //debugger
            $(document).on({
                ajaxStart: function () {
                    $("#loders").css("display", "")
                }, ajaxStop: function () { $("#loders").css("display", "none") }
            });
        })
    </script>
    <script type="text/javascript">
        function AddMarkup(id) {
            if (id != "radio_Amt") {
                $("#div_MrkAmt").css("display", "none")
                $("#div_MrkPer").css("display", "")
            }

            else {
                $("#div_MrkPer").css("display", "none")
                $("#div_MrkAmt").css("display", "")
            }
        }
        $(document).ready(function () {
            $("#radio_Per").click(function () {
                AddMarkup(this.id);
            });
            $("#radio_Amt").click(function () {
                AddMarkup(this.id);
            });
            $("#dateId").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd/mm/yy",
                maxDate: new Date(),
                //minDate: "-1D"
            });
        });
    </script>
    <script src="Scripts/ExchangeMarkup.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Exchange Markup</h1><span> </b> Rate: ( Update Date & Time: <span class="countoverstay size18" id="SpanUdtDate"></span> )
         <%--   <h2 class="updateDate"><span class="lato size15 grey">Update Online Rate</span></h2>--%>
        </hgroup>


        <div class="with-padding">
            <div class="Exchange anthracite-gradient" id="demo" style="display: none">
                <input type="hidden" id="Hdn_Id" />
                <input type="hidden" id="Hdn_FCY" />
                <br />
                <div class="columns">
                    <div class="six-columns twelve-columns-mobile">
                        <label>Currency:</label><div class="full-width button-height" id="div_Currency">
                            <select id="sel_Currency" name="validation-select" class="select" onchange="GetCurrencyValue(this.value)">
                            </select>
                            <label style="color: red; margin-top: 3px; display: none" id="lbl_txt_Currency">
                                <b>* This field is required</b></label>
                        </div>
                    </div>
                    <div class="six-columns twelve-columns-mobile">
                        <label>Online Rate.:</label><div class="input full-width">
                            <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                            <input class="input-unstyled full-width" readonly="readonly" id="txt_FCY" type="text" autocomplete="off">
                            <label style="color: red; margin-top: 3px; display: none" id="lbl_txt_FCY">
                                <b>* This field is required</b></label>
                        </div>
                    </div>
                </div>
                <div class="columns">
                    <div class="three-columns twelve-columns-mobile">
                        <input type="radio" name="radio" id="radio_Amt" value="" checked="checked" class=" mid-margin-left" onchange="AddMarkup(this.id)">
                        <label for="radio_Amt" class="label">Markup Amount</label>
                    </div>
                    <div class="three-columns twelve-columns-mobile" style="width:24%; margin-left:1%">
                        <input type="radio" name="radio" id="radio_Per" value="" class=" mid-margin-left" onchange="AddMarkup(this.id)">
                        <label for="radio_Per" class="label">Markup in Percent</label>
                    </div>
                    <label style="color: red; margin-top: 3px; display: none" id="lbl_txt_Application">
                        <b>* This field is required</b></label>

                      <input type="hidden" id="txt_MarkupAmt" value="" />

                    <div class="three-columns six-columns-mobile" id="div_MrkAmt">
                        <label>Markup Amount:</label><div class="input full-width">
                            <input value="" class="input-unstyled full-width" type="text" id="txt_SetMarkupAmt" onkeyup="GetMarkupAmt(this.id)">
                        </div>
                    </div>
                     <div class="three-columns six-columns-mobile" style="display:none" id="div_MrkPer">
                        <label>Markup Percent:</label><div class="input full-width">
                            <input value="" class="input-unstyled full-width" type="text" id="txt_SetMarkupPer" onkeyup="GetMarkupAmt(this.id)">
                        </div>
                    </div>
                    <div class="three-columns six-columns-mobile">
                        <label>Total Exchange:</label><div class="input full-width">
                            <input value="" class="input-unstyled full-width" readonly="readonly" type="text" id="txt_TotalFCY">
                        </div>
                    </div>
                       <label style="color: red; margin-top: 3px; display: none" id="lbl_txt_SetMarkupPer">
                                                <b>* This field is required</b></label>
                </div>

                <p class="text-aligncenter">
                    <button type="submit" class="button anthracite-gradient" onclick="Submit()">Update</button>
                    <button type="submit" class="button anthracite-gradient" onclick="FCYHide()">Cancel</button>
                </p>
                <div class="clear"></div>
            </div>

            <div class="respTable">
                <table class="table responsive-table" id="tbl_log">
                    <thead>
                        <tr>
                            <th scope="col" class="align-center">Currency</th>
                            <th scope="col" class="align-center">Exchange Rate</th>
                            <th scope="col" class="align-center">Markup</th>
                            <th scope="col" class="align-center">Total</th>
                            <%--<th scope="col" class="align-center">Update Date</th>--%>
                            <th scope="col" class="align-center">Update By</th>
                            <th scope="col" class="align-center">Edit</th>
                        </tr>
                    </thead>
                    <%--<tbody>
                    </tbody>--%>
                </table>
            </div>
        </div>
    </section>
</asp:Content>
