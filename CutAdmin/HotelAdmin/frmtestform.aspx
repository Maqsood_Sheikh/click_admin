﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelAdmin/Master.Master" AutoEventWireup="true" CodeBehind="frmtestform.aspx.cs" Inherits="CutAdmin.HotelAdmin.frmtestform" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    <script src="Scripts/testform.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Bank Details</h1>
        </hgroup>
        <div class="with-padding">
            <hr>
            <div class="columns">
                <div class="two-columns four-columns-mobile">
                    <label>Bank Name<span class="red">*</span>:</label>
                </div>
                <div class="four-columns eight-columns-mobile">
                    <div class="input full-width">
                        <input class="input-unstyled full-width" placeholder="Bank Name" type="text" id="txtBankName">
                    </div>
                </div>
                <div class="two-columns four-columns-mobile">
                    <label>Account No<span class="red">*</span>:</label>
                </div>
                <div class="four-columns eight-columns-mobile">
                    <div class="input full-width">
                        <input value="" class="input-unstyled full-width" placeholder="Account No" type="text" id="txtAccountNo">
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="two-columns four-columns-mobile">
                    <label>Branch<span class="red">*</span>:</label>
                </div>
                <div class="four-columns eight-columns-mobile">
                    <div class="input full-width">
                        <input value="" class="input-unstyled full-width" placeholder="Branch" type="text" id="txtBranch">
                    </div>
                </div>
                <div class="two-columns four-columns-mobile">
                    <label>Swift Code<span class="red">*</span>:</label>
                </div>
                <div class="four-columns eight-columns-mobile">
                    <div class="input full-width">
                        <input value="" class="input-unstyled full-width" placeholder="Swift Code" type="text" id="txtSwiftCode">
                    </div>
                </div>
            </div>
            <p class="text-right">
                <input type="button" class="button anthracite-gradient" id="btnAddBankDetails" onclick="AddBankDetails()" value="Add" title="Submit Details">
                <a id="btn_Cancel" class="button anthracite-gradient" onclick="Cancel()" style="cursor:pointer; display:none">Cancel</a>
            </p>
        </div>
        <div class="with-padding">
            <div class="respTable" id="tblbankdetails">
                <%--<table class="table responsive-table" id="tbl_BankDetails">
                    <thead>
                        <tr>
                            <th scope="col" class="align-center">Sr.No.</th>
                            <th scope="col" class="align-center">Bank Name</th>
                            <th scope="col" class="align-center">Account No</th>
                            <th scope="col" class="align-center">Branch</th>
                            <th scope="col" class="align-center">Swift Code</th>
                            <th scope="col" class="align-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>--%>
            </div>
        </div>
    </section>
   
</asp:Content>
