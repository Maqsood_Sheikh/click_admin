﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.HotelAdmin.Handler
{
    /// <summary>
    /// Summary description for MarkupHandller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class MarkupHandller : System.Web.Services.WebService
    {

        string jsonString = "";
        JavaScriptSerializer objserialize = new JavaScriptSerializer();
        AdminDBHandlerDataContext DB = new AdminDBHandlerDataContext();

        [WebMethod]
        public string GetGlobalMarkup()
        {
            var SupplierList = (from obj in DB.tbl_AdminLogins where obj.UserType == "Supplier" select obj).ToList();
            if (SupplierList.Count() > 0)
            {
                return objserialize.Serialize(new { Session = 1, retCode = 1, SupplierList = SupplierList });
            }
            else
            {
                return objserialize.Serialize(new { Session = 1, retCode = 0 });
            }

        }
    }
}
