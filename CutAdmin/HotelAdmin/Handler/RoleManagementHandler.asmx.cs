﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.HotelAdmin.Handler
{
    /// <summary>
    /// Summary description for RoleManagementHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class RoleManagementHandler : System.Web.Services.WebService
    {
        AdminDBHandlerDataContext DB = new AdminDBHandlerDataContext();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

        private string escapejosndata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }

        #region AdminRoles
        [WebMethod(EnableSession = true)]
        public string GetRoleList()
        {
            string jsonString = "";
            // DBHelper.DBReturnCode retCode = RoleManager.GetRoleList(out dtResult);

            var RoleList = (from obj in DB.tblRoles select obj).ToList();

            if (RoleList.Count > 0 && RoleList != null)
            {

                jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = RoleList });

            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetFormList()
        {
            string jsonString = "";
            try
            {
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired , Please login and try again.");
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                   
                if (objGlobalDefault.UserType == "Admin")
                {
                    var FormList = (from obj in DB.tblForms select obj).ToList();
                    jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = FormList });
                }
                else if (objGlobalDefault.UserType == "Supplier")
                {
                    Int64 UserID = Convert.ToInt64(this.Context.Request.QueryString["sid"]);
                        CutAdmin.BL.DBHandlerDataContext db = new DBHandlerDataContext();
                    var arrForm = (from obj in db.tbl_AgentForms
                                   select obj).ToList();
                    var arrAuthForm = (from obj in db.tbl_AgentForms
                                       join
                                           objAuth in db.tblAgentRoleManagers
                                           on obj.nId equals objAuth.nFormId
                                       where objAuth.nUid == UserID
                                       select new
                                         {
                                             obj.nId,
                                             obj.sFormName,

                                         }).ToList();
                    jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = arrForm, arrAuthForm = arrAuthForm });
                }
                else
                    jsonString = jsSerializer.Serialize(new { Session = 1, retCode =2});
               
            }
            catch
            {

            } 
           
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetFormsForRole(string sRoleName)
        {
            string jsonString = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

            Int64 nId = (from obj in DB.tblRoles where obj.sRoleName == sRoleName select obj.nId).FirstOrDefault();

            var FormsForRoleList = (from obj in DB.tblForms
                                    join Role in DB.tblRoleManagers on obj.nId equals Role.nFormId
                                    where Role.nRoleId == nId
                                    select new
                                    {
                                        obj.sFormName,
                                    }).ToList();

            if (FormsForRoleList.Count > 0 && FormsForRoleList != null)
            {
                jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = FormsForRoleList });
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string SetFormsForRole(string sSelectedRole, params string[] arr)
        {
            try
            {
                Int64 nId = (from obj in DB.tblRoles where obj.sRoleName == sSelectedRole select obj.nId).FirstOrDefault();

                var Role = (from obj in DB.tblRoleManagers where obj.nRoleId == nId select obj).ToList();

                DB.tblRoleManagers.DeleteAllOnSubmit(Role);
                DB.SubmitChanges();

                List<tblRoleManager> ListRoleManager = new List<tblRoleManager>();
                for (int i = 0; i < arr.Length; i++)
                {
                    ListRoleManager.Add(new tblRoleManager
                    {
                        nRoleId = (DB.tblRoles.Where(d => d.sRoleName == sSelectedRole).FirstOrDefault().nId),
                        nFormId = (DB.tblForms.Where(d => d.sFormName == arr[i]).FirstOrDefault().nId),
                    });
                }

                DB.tblRoleManagers.InsertAllOnSubmit(ListRoleManager);
                DB.SubmitChanges();

                return jsSerializer.Serialize(new { retCode = 1 });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0 });
            }
        }
        #endregion


        #region Admin StaffRoles
        [WebMethod(EnableSession = true)]
        public string GetFormsForAdminStaff(Int64 StaffUid)
        {
            string jsonString = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

            Int64 sid = (from obj in DB.tbl_StaffLogins where obj.sid == StaffUid select obj.sid).FirstOrDefault();

            var FormsForAdminStaffList = (from obj in DB.tblForms
                                          join Role in DB.tblStaffRoleManagers on obj.nId equals Role.nFormId
                                          where Role.nUid == sid
                                          select new
                                          {
                                              obj.sFormName,
                                          }).ToList();

            if (FormsForAdminStaffList.Count > 0 && FormsForAdminStaffList != null)
            {
                jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, tblFormsForRole = FormsForAdminStaffList });
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;


        }

        [WebMethod(EnableSession = true)]
        public string SetFormsForAdminStaff(Int64 StaffUid, params string[] arr)
        {
            try
            {
                 if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired , Please login and try again.");
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

                    if (objGlobalDefault.UserType == "Admin")
                    {
                        Int64 sid = (from obj in DB.tbl_StaffLogins where obj.sid == StaffUid select obj.sid).FirstOrDefault();

                        var Role = (from obj in DB.tblStaffRoleManagers where obj.nUid == sid select obj).ToList();

                        DB.tblStaffRoleManagers.DeleteAllOnSubmit(Role);
                        DB.SubmitChanges();

                        List<tblStaffRoleManager> ListblStaffRoleManager = new List<tblStaffRoleManager>();
                        for (int i = 0; i < arr.Length; i++)
                        {
                            ListblStaffRoleManager.Add(new tblStaffRoleManager
                            {
                                nUid = StaffUid,
                                nFormId = Convert.ToInt64(arr[i]),
                            });
                        }

                        DB.tblStaffRoleManagers.InsertAllOnSubmit(ListblStaffRoleManager);
                        DB.SubmitChanges();
                    }
                else
                    {
                        CutAdmin.BL.DBHandlerDataContext db = new DBHandlerDataContext();
                        List<CutAdmin.BL.tblAgentRoleManager> arrList = new List<tblAgentRoleManager>();
                        List<CutAdmin.BL.tblAgentRoleManager> arrOldForm = (from obj in db.tblAgentRoleManagers 
                                                                            where obj.nUid == StaffUid select obj).ToList();
                        if (arrOldForm.Count != 0)
                        {
                            db.tblAgentRoleManagers.DeleteAllOnSubmit(arrOldForm);
                            db.SubmitChanges();
                        }
                        foreach (var nFormID in arr)
                        {
                            arrList.Add(new tblAgentRoleManager {nFormId = Convert.ToInt64(nFormID) ,nUid =StaffUid });
                        }
                        db.tblAgentRoleManagers.InsertAllOnSubmit(arrList);
                        db.SubmitChanges();
                    }
               

                return jsSerializer.Serialize(new { retCode = 1 });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0 });
            }
        }
        #endregion
    }
}
