﻿using CommonLib.Response;
using CutAdmin.BL;
using CutAdmin.Common;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.HotelAdmin.Handler
{
    /// <summary>
    /// Summary description for BookingHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class BookingHandler : System.Web.Services.WebService
    {
        string json = ""; string Texts = "";
        string jsonString = "";
        DataTable dtResult;
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        DBHandlerDataContext DB = new DBHandlerDataContext();
        DBHelper.DBReturnCode retCode;
        List<RecordInv> Record = new List<RecordInv>();
        public class Addons
        {
            public string Date { get; set; }
            public string Name { get; set; }
            public string Type { get; set; }
            public string TotalRate { get; set; }
            public string Quantity { get; set; }
        }
        public class RecordInv
        {
            public string BookingId { get; set; }
            public string InvSid { get; set; }
            public string SupplierId { get; set; }
            public string HotelCode { get; set; }
            public string RoomType { get; set; }
            public string RateType { get; set; }
            public string InvType { get; set; }
            public string Date { get; set; }
            public string Month { get; set; }
            public string Year { get; set; }
            public string TotalAvlRoom { get; set; }
            public string OldAvlRoom { get; set; }
            public string NoOfBookedRoom { get; set; }
            public string NoOfCancleRoom { get; set; }
            public DateTime UpdateDate { get; set; }
            public string UpdateOn { get; set; }
        }
        [WebMethod(EnableSession = true)]
        public string ValidateTransaction(List<Addons> arrAdons)
        {
            jsSerializer = new JavaScriptSerializer();
            bool IsValid = false;
            try
            {
                float AddOnsPrice = 0;
                if (arrAdons.Count != 0)
                {
                    arrAdons.ForEach(d => d.TotalRate = (Convert.ToSingle(d.TotalRate) * Convert.ToSingle(d.Quantity)).ToString());
                    AddOnsPrice = arrAdons.Select(d => Convert.ToSingle(d.TotalRate)).ToList().Sum();
                }
                if (Session["RatesDetails"] == null)
                    throw new Exception("Not Valid Booking Please Search again.");
                List<RatesManager.Supplier> Supplier = (List<RatesManager.Supplier>)Session["RatesDetails"];
                float BaseRate = Supplier[0].Details.Select(d => d.Rate.TotalCharge.TotalRate).ToList().Sum() + AddOnsPrice;
                #region checking AvailCredit with Booking Amount
                float AvailableCredit = 0;
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired ,Please Login and try Again");
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                dbTaxHandlerDataContext dbTax = new dbTaxHandlerDataContext();
                var dtAvailableCredit = (from obj in dbTax.tbl_AdminCreditLimits where obj.uid == objGlobalDefault.sid select obj).FirstOrDefault();
                if (dtAvailableCredit == null)
                    throw new Exception("Please Contact  Administrator and try Again");
                AvailableCredit = Convert.ToSingle(dtAvailableCredit.AvailableCredit);
                float @Creditlimit = Convert.ToSingle(dtAvailableCredit.CreditAmount);
                bool Credit_Flag = (bool)dtAvailableCredit.Credit_Flag;
                bool OTC = (bool)dtAvailableCredit.OTC;
                float @MAXCreditlimit = Convert.ToSingle(dtAvailableCredit.MaxCreditLimit);
                if (AvailableCredit >= 0 && AvailableCredit >= BaseRate)
                {
                    IsValid = true;
                }
                else if (@AvailableCredit <= 0 && @Creditlimit <= 0 && @MAXCreditlimit >= BaseRate && @OTC == true)
                {
                    IsValid = true;
                }
                else if (@AvailableCredit >= 0 && (@AvailableCredit + @MAXCreditlimit + @Creditlimit) >= BaseRate && @OTC == true && @Credit_Flag == true)
                {
                    IsValid = true;
                }
                else if (@AvailableCredit >= 0 && (@AvailableCredit + @MAXCreditlimit) >= BaseRate && @OTC == true)
                {
                    IsValid = true;
                }
                else if (@AvailableCredit >= 0 && (@AvailableCredit + @Creditlimit) >= BaseRate && @Credit_Flag == true)
                {
                    IsValid = true;
                }
                else if (@AvailableCredit < 0 && @Creditlimit > 0 && @Creditlimit >= BaseRate && @Credit_Flag == true)
                {
                    IsValid = true;
                }

                #endregion
                if (IsValid)
                    return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
                else
                    throw new Exception("Your Balance is insufficient to Make this booking");
            }

            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1, ErrorMsg = ex.Message });
            }

        }

        [WebMethod(EnableSession = true)]
        public string Booking(List<RatesManager.Supplier> Supplier, Int16 Total, List<string> Amount, List<Addons> arrAddOns)
        {
            try
            {
                /*Checking User Details*/
                Int64 Uid = 0; string sTo = "";
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session expired.");
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                if (objGlobalDefault.UserType == "AdminStaff")
                {
                    Uid = objGlobalDefault.ParentId;
                    sTo = objGlobalDefault.uid;
                    sTo += "," + (from obj in DB.tbl_AdminLogins where obj.sid == Uid select obj).FirstOrDefault().uid;
                }
                else
                {
                    Uid = objGlobalDefault.sid;
                    sTo = objGlobalDefault.uid;
                }

                var arrLastReservationID = (from obj in DB.tbl_CommonHotelReservations select obj).ToList().Count;
                if (arrLastReservationID == 0)
                    arrLastReservationID = 0;
                String ReservationID = (arrLastReservationID + 1).ToString("D" + 6);
                bool response = false;

                #region Inventory Update & Validate
                bool valid = InventoryManager.CheckInventory(Supplier);
                if (valid)
                {

                    response = InventoryManager.UpdateInventory(Supplier, ReservationID);
                    List<tbl_CommonInventoryRecord> AddRecord = new List<tbl_CommonInventoryRecord>();
                    List<InventoryManager.RecordInv> Record = InventoryManager.Record;
                    for (int i = 0; i < Record.Count; i++)
                    {
                        tbl_CommonInventoryRecord Recordnew = new tbl_CommonInventoryRecord();
                        Recordnew.BookingId = Record[i].BookingId;
                        Recordnew.InvSid = Record[i].InvSid;
                        Recordnew.SupplierId = Record[i].SupplierId;
                        Recordnew.HotelCode = Record[i].HotelCode;
                        Recordnew.RoomType = Record[i].RoomType;
                        Recordnew.RateType = Record[i].RateType;
                        Recordnew.InvType = Record[i].InvType;
                        Recordnew.Date = Record[i].Date;
                        Recordnew.Month = Record[i].Month;
                        Recordnew.Year = Record[i].Year;
                        Recordnew.TotalAvlRoom = Record[i].TotalAvlRoom;
                        Recordnew.OldAvlRoom = Record[i].OldAvlRoom;
                        Recordnew.NoOfBookedRoom = Record[i].NoOfBookedRoom;
                        Recordnew.NoOfCancleRoom = Record[i].NoOfCancleRoom;
                        Recordnew.UpdateDate = Record[i].UpdateDate.ToString();
                        Recordnew.UpdateOn = Record[i].UpdateOn;
                        AddRecord.Add(Recordnew);
                    }
                    DB.tbl_CommonInventoryRecords.InsertAllOnSubmit(AddRecord);
                    DB.SubmitChanges();
                }
                #endregion

                /*AB Qudus*/
                #region AddOns  Save
                List<tbl_CommonBookingHotelAddon> Addons = new List<tbl_CommonBookingHotelAddon>();
                for (int l = 0; l < arrAddOns.Count; l++)
                {

                    string[] Date = arrAddOns[l].Date.Split('_');
                    Addons.Add(new tbl_CommonBookingHotelAddon
                    {
                        BookingId = ReservationID,
                        Date = Date[0],
                        RoomNo = Date[1],
                        Name = arrAddOns[l].Name,
                        Type = arrAddOns[l].Type,
                        Quantity = arrAddOns[l].Quantity,
                        Rate = arrAddOns[l].TotalRate,
                    });
                }
                DB.tbl_CommonBookingHotelAddons.InsertAllOnSubmit(Addons);
                DB.SubmitChanges();
                Addons.ForEach(r => r.Rate = (Convert.ToDecimal(r.Rate) * Convert.ToDecimal(r.Quantity)).ToString());
                #endregion



                #region Booking Transactions
                CutAdmin.DataLayer.ReservationDetails objReservation = new ReservationDetails();
                objReservation.TotalAmount = Supplier[0].Details.Select(r => r.Rate.TotalCharge.BaseRate).ToList().Sum();
                objReservation.AdminCommission = 0;
                objReservation.ListCharges = new List<TaxRate>();
                foreach (var objRoom in Supplier[0].Details)
                {
                    foreach (var objRate in objRoom.Rate.TotalCharge.Charge)
                    {
                        if (objReservation.ListCharges.Where(d => d.RateName == objRate.RateName).ToList().Count == 0)
                        {
                            objReservation.ListCharges.Add(objRate);
                        }
                        else
                        {
                            objReservation.ListCharges.Single(d => d.RateName == objRate.RateName).TotalRate += objRate.TotalRate;
                        }
                    }


                }
                Int64 noNights = Convert.ToInt64(Supplier[0].Details[0].Rate.ListDates.Count - 1);
                objReservation.AdminCommission = BookingManager.AdminCommission(Convert.ToInt64(Supplier[0].HotelCode), noNights);
                objReservation.AddOnsCharge = Addons.Select(D => Convert.ToSingle(D.Rate)).ToList().Sum();
                CutAdmin.DataLayer.BookingManager.objReservation = objReservation;
                objReservation.TotalPayable = BookingManager.TransactionAmount("AG");
                #region Hotel Booking Details
                var arrHotel = (from obj in DB.tbl_CommonHotelMasters where obj.sid == Convert.ToInt64(Supplier[0].HotelCode) select obj).FirstOrDefault();
                tbl_CommonHotelReservation HotelReserv = new tbl_CommonHotelReservation();
                HotelReserv.ReservationID = Convert.ToString(ReservationID);
                HotelReserv.HotelCode = Supplier[0].HotelCode;
                HotelReserv.HotelName = arrHotel.HotelName;
                HotelReserv.mealplan = Supplier[0].Details[0].MealName;
                HotelReserv.TotalRooms = Convert.ToInt16(Supplier[0].Details.Select(d => d.noRooms).ToList().Sum());
                //HotelReserv.TotalRooms = Convert.ToInt16(Supplier[0].Details.Select(d => d.noRooms));
                HotelReserv.RoomCode = "";
                HotelReserv.sightseeing = 0;
                HotelReserv.Source = Supplier[0].Name;
                if (response)
                    HotelReserv.Status = "Vouchered";
                else
                    HotelReserv.Status = "OnRequest";
                HotelReserv.SupplierCurrency = "INR";
                HotelReserv.terms = arrHotel.HotelNote;
                HotelReserv.Uid = Uid;
                HotelReserv.Updatedate = DateTime.Now.ToString("dd-MM-yyyy");
                HotelReserv.Updateid = objGlobalDefault.sid.ToString();
                HotelReserv.VoucherID = "VCH-" + ReservationID;
                HotelReserv.DeadLine = "";
                HotelReserv.AgencyName = "";
                HotelReserv.AgentContactNumber = "";
                HotelReserv.AgentEmail = "";
                HotelReserv.AgentMarkUp_Per = 0;
                HotelReserv.AgentRef = "";
                HotelReserv.AgentRemark = "";
                HotelReserv.AutoCancelDate = "";
                HotelReserv.AutoCancelTime = "";
                HotelReserv.bookingname = "";
                HotelReserv.BookingStatus = "";
                HotelReserv.CancelDate = "";
                HotelReserv.CancelFlag = false;
                HotelReserv.CheckIn = Convert.ToString(Supplier[0].Details[0].Rate.ListDates.Select(d => d.Date).ToList().First());
                HotelReserv.CheckOut = Convert.ToString(Supplier[0].Details[0].Rate.ListDates.Select(d => d.Date).ToList().Last());
                HotelReserv.ChildAges = "";
                HotelReserv.Children = Supplier[0].Details[0].Rate.LisCustumer.Where(d => d.type == "CH").ToList().Count();
                HotelReserv.City = arrHotel.CityId;
                HotelReserv.deadlineemail = false;
                HotelReserv.Discount = 0;
                HotelReserv.ExchangeValue = 0;
                HotelReserv.ExtraBed = 0;
                HotelReserv.GTAId = "";
                HotelReserv.holdbooking = 0;
                HotelReserv.HoldTime = "";
                HotelReserv.HotelBookingData = "";
                HotelReserv.HotelDetails = "";
                HotelReserv.Infants = 0;
                HotelReserv.InvoiceID = "-";
                HotelReserv.IsAutoCancel = false;
                HotelReserv.IsConfirm = false;
                HotelReserv.LatitudeMGH = arrHotel.HotelLatitude;
                HotelReserv.LongitudeMGH = arrHotel.HotelLatitude;
                HotelReserv.LuxuryTax = 0;
                HotelReserv.mealplan_Amt = 0;
                HotelReserv.NoOfAdults = Supplier[0].Details[0].Rate.LisCustumer.Where(d => d.type == "AD").ToList().Count(); ;
                Int32 night = Convert.ToInt16(Supplier[0].Details[0].Rate.ListDates.Select(d => d.Date).ToList().Count());
                night = night - 1;
                // HotelReserv.NoOfDays = Convert.ToInt16(Supplier[0].Details[0].Rate.ListDates.Select(d => d.Date).ToList().Count());
                HotelReserv.NoOfDays = Convert.ToInt16(night);
                HotelReserv.OfferAmount = 0;
                HotelReserv.OfferId = 0;
                HotelReserv.RefAgency = "";
                HotelReserv.ReferenceCode = "";
                HotelReserv.Remarks = "";
                HotelReserv.ReservationDate = DateTime.Now.ToString("dd-MM-yyyy");
                HotelReserv.ReservationTime = "";
                HotelReserv.RoomRate = 0;
                HotelReserv.SalesTax = 0;
                HotelReserv.Servicecharge = 0;
                HotelReserv.ComparedFare = Convert.ToDecimal(objReservation.TotalAmount + objReservation.ListCharges.Select(d => d.TotalRate).ToList().Sum()) + Addons.Select(D => Convert.ToDecimal(D.Rate)).ToList().Sum();
                HotelReserv.ComparedCurrency = "";
                // HotelReserv.TotalFare = Convert.ToDecimal(Supplier[0].Details[0].Rate.ListDates.Select(d => d.TotalPrice).ToList().Sum());
                // HotelReserv.TotalFare = Total;
                HotelReserv.TotalFare = Convert.ToDecimal(BookingManager.TransactionAmount("AG") - objReservation.AdminCommission);
                DB.tbl_CommonHotelReservations.InsertOnSubmit(HotelReserv);
                DB.SubmitChanges();
                #endregion
                objReservation.objHotelDetails = new tbl_CommonHotelReservation();
                objReservation.objHotelDetails = HotelReserv;
                BookingManager.objReservation.objHotelDetails = new tbl_CommonHotelReservation();
                BookingManager.objReservation.objHotelDetails = HotelReserv;
                DBHelper.DBReturnCode retCode = CutAdmin.DataLayer.BookingManager.BookingTransact("Hotel");
                #endregion



                #region Booked Room

                for (var i = 0; i < Supplier[0].Details.Count; i++)
                {

                    for (int j = 0; j < Supplier[0].Details[i].noRooms; j++)
                    {

                        string Ages = "";
                        string CancellationAmountMultiple = "";
                        string CancellationDateTime = "";
                        foreach (var ages in Supplier[0].Details[i].Rate.LisCustumer.Where(d => d.RoomNo == j + 1 && d.type == "CH").ToList().Select(c => c.Age).ToList())
                        {
                            Ages += ages + "|";
                        }

                        tbl_CommonBookedRoom BookedRoom = new tbl_CommonBookedRoom();
                        BookedRoom.Adults = Supplier[0].Details[i].Rate.LisCustumer.Where(d => d.RoomNo == j + 1 && d.type == "AD").ToList().Count;
                        BookedRoom.BoardText = Convert.ToString(Supplier[0].Details[i].MealName);
                        BookedRoom.CanAmtWithTax = "";
                        // BookedRoom.CancellationAmount = Convert.ToString(Supplier[0].Details[i].Rate.ListCancellation[0].CancellationAmount);

                        //for (int k = 0; k < Amount.Count; k++)
                        //{
                        //    if (Amount[k] == Amount[j])
                        //    {
                        //        BookedRoom.RoomAmount = Convert.ToDecimal(Amount[k]);
                        //    }

                        //}

                        //var ChargeAddOns= Addons.Where(d=>d.RoomNo == (i+1).ToString()).Select(r=> Convert.ToDecimal( r.Rate)).ToList().Sum();
                        BookedRoom.RoomAmount = Convert.ToDecimal(Supplier[0].Details[i].Rate.TotalCharge.BaseRate) + 0;
                        foreach (var Canclellation in Supplier[0].Details[i].Rate.ListCancellation.ToList())
                        {
                            if (Canclellation.nonRefundable == true)
                            {
                                CancellationAmountMultiple += BookedRoom.RoomAmount + "|";
                                CancellationDateTime += Canclellation.CancellationDateTime + "|";
                            }
                            else
                            {
                                CancellationAmountMultiple += Canclellation.objCharges.TotalRate + "|";
                                CancellationDateTime += Canclellation.CancellationDateTime + "|";
                            }


                        }

                        BookedRoom.CancellationAmount = CancellationAmountMultiple;
                        BookedRoom.Child = Supplier[0].Details[i].Rate.LisCustumer.Where(d => d.RoomNo == j + 1 && d.type == "CH").ToList().Count;
                        BookedRoom.ChildAge = Ages;
                        BookedRoom.Commision = 0;
                        BookedRoom.CutCancellationDate = CancellationDateTime;
                        BookedRoom.Discount = 0;
                        BookedRoom.EssentialInfo = "";
                        BookedRoom.ExchangeRate = 0;
                        BookedRoom.FranchiseeMarkup = 0;
                        BookedRoom.FranchiseeTaxDetails = "";
                        BookedRoom.LeadingGuest = "";
                        BookedRoom.MealPlanID = Convert.ToString(Supplier[0].Details[i].MealID);
                        BookedRoom.Remark = "";
                        BookedRoom.ReservationID = Convert.ToString(ReservationID);
                        //BookedRoom.RoomAmount = Convert.ToInt64(Supplier[0].Details[i].Rate.ListDates[i].TotalPrice);
                        BookedRoom.TaxDetails = "";
                        foreach (var objCharge in Supplier[0].Details[i].Rate.TotalCharge.Charge)
                        {
                            BookedRoom.TaxDetails += objCharge.RateName + ":" + objCharge.TotalRate;
                        }

                        BookedRoom.RoomAmtWithTax = 0;
                        BookedRoom.RoomCode = Convert.ToString(Supplier[0].Details[i].RateTypeID);
                        BookedRoom.RoomNumber = Convert.ToString(i + 1);
                        BookedRoom.RoomServiceTax = 0;
                        BookedRoom.RoomType = Supplier[0].Details[i].RateType;
                        BookedRoom.SupplierAmount = 0;
                        BookedRoom.SupplierCurrency = "INR";
                        BookedRoom.SupplierNoChargeDate = "";


                        BookedRoom.TDS = 0;
                        BookedRoom.TotalRooms = Convert.ToInt16(Supplier[0].Details.Select(d => d.noRooms).ToList().Sum());
                        //   BookedRoom.TotalRooms = Convert.ToInt16(Supplier[0].Details.Select(d => d.noRooms)); 

                        DB.tbl_CommonBookedRooms.InsertOnSubmit(BookedRoom);
                    }

                    // DB.SubmitChanges();

                    for (int k = 0; k < Supplier[0].Details[i].Rate.LisCustumer.Count; k++)
                    {
                        tbl_CommonBookedPassenger BookedPassenger = new tbl_CommonBookedPassenger();
                        BookedPassenger.Age = Supplier[0].Details[i].Rate.LisCustumer[k].Age;
                        if (i == 0)
                            BookedPassenger.IsLeading = true;
                        else
                            BookedPassenger.IsLeading = false;
                        BookedPassenger.LastName = Supplier[0].Details[i].Rate.LisCustumer[k].LastName;
                        BookedPassenger.Name = Supplier[0].Details[i].Rate.LisCustumer[k].Title + " " + Supplier[0].Details[i].Rate.LisCustumer[k].Name;
                        BookedPassenger.PassengerType = Supplier[0].Details[i].Rate.LisCustumer[k].type;
                        BookedPassenger.ReservationID = ReservationID;
                        BookedPassenger.RoomCode = "";
                        BookedPassenger.RoomNumber = Convert.ToString(i + 1);

                        DB.tbl_CommonBookedPassengers.InsertOnSubmit(BookedPassenger);
                    }

                    DB.SubmitChanges();
                }
                #endregion

                /*Tausif Work*/
                #region Send Mail

                string URL = System.Configuration.ConfigurationManager.AppSettings["URL"];
                Int64 ParentID = Convert.ToInt64(ConfigurationManager.AppSettings["AdminKey"]);
                var sData = (from obj in DB.tbl_AdminLogins where obj.sid == ParentID select obj).FirstOrDefault();
                string title = "";
                InvoiceMailManager.CheckIfProcessAlreadyRunning();
                string Invoice = InvoiceManager.GetInvoice(ReservationID, Uid, out  title);
                CutAdmin.DataLayer.EmailManager.GenrateAttachment(Invoice, ReservationID, "Invoice");
                InvoiceMailManager.CheckIfProcessAlreadyRunning();
                string Voucher = VoucherManager.GenrateVoucher(ReservationID, Uid.ToString(), "Vouchered");
                InvoiceMailManager.CheckIfProcessAlreadyRunning();
                if (Voucher != "")
                    CutAdmin.DataLayer.EmailManager.GenrateAttachment(Voucher, ReservationID, "Voucher");
                string sMail = CutAdmin.DataLayer.EmailManager.BookingMail4Agent(ReservationID);
                string DocPath = URL + "InvoicePdf/" + ReservationID + "_Invoice.pdf";
                if (Voucher != "")
                    DocPath += ";" + URL + "InvoicePdf/" + ReservationID + "_Voucher.pdf";

                // Booking Mail To Agent 
                SendMail(sTo, title, sMail, DocPath, "online@clickurtrip.com");


                // Booking Confirm Mail To Admin
                string Admin_Bc, Admin_Cc;
                string sAdminMail = EmailManager.BookingMail2CUT(ReservationID, Uid, out Admin_Bc, out Admin_Cc);
                SendMail(Admin_Bc, title, sAdminMail, DocPath, Admin_Cc);
                #endregion
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            catch
            {
                string Message = EmailManager.GetErrorMessage("Booking Error");
                return jsSerializer.Serialize(new { retCode = 0, Session = 1, Message = Message });
            }
        }

        public static void SendMail(string sTo, string title, string sMail, string DocPath, string Cc)
        {
            //int effected = 0;
            //SqlParameter[] sqlParams = new SqlParameter[5];
            //sqlParams[0] = new SqlParameter("@sTo", sTo);
            //sqlParams[1] = new SqlParameter("@sSubject", title);
            //sqlParams[2] = new SqlParameter("@VarBody", sMail);
            //sqlParams[3] = new SqlParameter("@Path", DocPath);
            //sqlParams[4] = new SqlParameter("@Cc", "online@clickurtrip.com");
            //DBHelper.DBReturnCode retcode = DBHelper.ExecuteNonQuery("Proc_InvoiceAttachmentMail", out effected, sqlParams);

            List<string> from = new List<string>();
            List<string> DocPathList = new List<string>();
            Dictionary<string, string> Email1List = new Dictionary<string, string>();
            Dictionary<string, string> BccList = new Dictionary<string, string>();
            from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));

            foreach (string mail in sTo.Split(','))
            {
                if (mail != "")
                {
                    Email1List.Add(mail, mail);
                }
            }
            foreach (string mail in Cc.Split(';'))
            {
                if (mail != "")
                {
                    BccList.Add(mail, mail);
                }
            }
            foreach (string link in DocPath.Split(';'))
            {
                if (link != "")
                {
                    DocPathList.Add(link);
                }
            }

            //string URL = Convert.ToString(ConfigurationManager.AppSettings["URL"]);
            string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

            MailManager.SendMail(accessKey, Email1List, BccList, title, sMail.ToString(), from, DocPathList);


        }

        
        [WebMethod(EnableSession = true)]
        public string BookingList()
        {
            try
            {
                DataTable dtResult = new DataTable();
                Session["SesBookingList"] = null;
                var BookingList = (from obj in DB.tbl_CommonHotelReservations
                                   join objPass in DB.tbl_CommonBookedPassengers on obj.ReservationID equals objPass.ReservationID
                                   join AgName in DB.tbl_AdminLogins on obj.Uid equals AgName.sid
                                   where objPass.IsLeading == true
                                   select new
                                   {
                                       obj.Sid,
                                       obj.ReservationID,
                                       obj.ReservationDate,
                                       AgName.AgencyName,
                                       obj.Status,
                                       obj.TotalFare,
                                       obj.TotalRooms,
                                       obj.HotelName,
                                       obj.CheckIn,
                                       obj.CheckOut,
                                       obj.City,
                                       obj.Children,
                                       obj.BookingStatus,
                                       obj.NoOfAdults,
                                       obj.Source,
                                       obj.Uid,
                                       obj.LatitudeMGH,
                                       obj.LongitudeMGH,
                                       obj.ParentID,
                                       objPass.Name,
                                       objPass.LastName,
                                   }).Distinct().ToList();
                var BookingListNew = BookingList.OrderByDescending(s => s.Sid).ToList();
                var SupplierList = BookingList.Select(z => z.ParentID).Distinct().ToList();
                GenralHandler.ListtoDataTable lsttodt = new GenralHandler.ListtoDataTable();
                dtResult = lsttodt.ToDataTable(BookingListNew);
                Session["SesBookingList"] = dtResult;
                dtResult.Dispose();
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = BookingListNew, SupplierList = SupplierList });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string BookingListFilter(string status, string type)
        {
            string json = "";
            try
            {

                if (status == "Vouchered" && type == "BookingPending")
                {
                    var BookingPending = CutAdmin.HotelAdmin.DataLayer.BookingList.BookingListNew().Where(d => d.Status == "Vouchered" && d.IsConfirm == false).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = BookingPending });
                }
                else if (status == "Vouchered" && type == "BookingConfirmed")
                {
                    var BookingConfirmed = CutAdmin.HotelAdmin.DataLayer.BookingList.BookingListNew().Where(d => d.Status == "Vouchered" && d.IsConfirm == true).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = BookingConfirmed });
                }
                else if (status == "Vouchered" && type == "BookingRejected")
                {
                    var BookingRejected = CutAdmin.HotelAdmin.DataLayer.BookingList.BookingListNew().Where(d => d.Status == "Cancelled").ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = BookingRejected });
                }
                else if (status == "OnRequest" && type == "Bookings")
                {
                    var OnHoldBookings = CutAdmin.HotelAdmin.DataLayer.BookingList.BookingListNew().Where(d => d.Status == "OnRequest" && d.IsConfirm == false).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = OnHoldBookings });
                }
                else if (status == "OnRequest" && type == "Requested")
                {
                    var OnHoldRequested = CutAdmin.HotelAdmin.DataLayer.BookingList.BookingListNew().Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = OnHoldRequested });
                }
                else if (status == "OnRequest" && type == "Confirmed")
                {
                    var OnHoldConfirmed = CutAdmin.HotelAdmin.DataLayer.BookingList.BookingListNew().Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = OnHoldConfirmed });
                }

            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string Search(string Agency, string CheckIn, string CheckOut, string Passenger, string BookingDate, string Reference, string SupplierRef, string Supplier, string HotelName, string Location, string ReservationStatus)
        {
            DataTable BookingList = (DataTable)Session["SesBookingList"];
            DataRow[] row = null;
            try
            {
                if (Agency != "")
                {
                    row = BookingList.Select("AgencyName like '%" + Agency + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                    //else
                    //    BookingList.Rows.Clear();

                }
                if (CheckIn != "")
                {
                    row = BookingList.Select("CheckIn like '%" + CheckIn + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                    //else
                    //    BookingList.Rows.Clear();
                }
                if (CheckOut != "")
                {
                    row = BookingList.Select("CheckOut like '%" + CheckOut + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                    //else
                    //    BookingList.Rows.Clear();
                }
                if (Passenger != "")
                {
                    row = BookingList.Select("Name like '%" + Passenger + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                    //else
                    //    BookingList.Rows.Clear();
                }
                if (BookingDate != "")
                {
                    row = BookingList.Select("ReservationDate like '%" + BookingDate + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                    //else
                    //    BookingList.Rows.Clear();
                }
                if (Reference != "")
                {
                    row = BookingList.Select("ReservationID like '%" + Reference + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                    //else
                    //    BookingList.Rows.Clear();
                }
                //if (SupplierRef != "")
                //{
                //    row = BookingList.Select("ReservationID like '%" + SupplierRef + "%'");
                //    if (row.Length != 0)
                //        BookingList = row.CopyToDataTable();
                //    else
                //        BookingList.Rows.Clear();
                //}
                if (Supplier != "" && Supplier != "All")
                {
                    row = BookingList.Select("AgencyName ='" + Supplier + "'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                  //  else
                    //    BookingList.Rows.Clear();
                }
                if (HotelName != "")
                {
                    row = BookingList.Select("HotelName like '%" + HotelName + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                    //else
                    //    BookingList.Rows.Clear();
                }
                if (HotelName != "")
                {
                    row = BookingList.Select("HotelName like '%" + HotelName + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                    //else
                    //    BookingList.Rows.Clear();
                }
                if (Location != "")
                {
                    row = BookingList.Select("City like '%" + Location + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                    //else
                    //    BookingList.Rows.Clear();
                }

                if (ReservationStatus != "" && ReservationStatus != "All")
                {
                    row = BookingList.Select("Status = '" + ReservationStatus + "'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                    //else
                    //    BookingList.Rows.Clear();
                }
                Session["SearchBookingList"] = BookingList;
                List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                BookingList.Dispose();
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;

        }

        [WebMethod(EnableSession = true)]
        public string BookingCancle(string ReservationID, string Status)
        {
            try
            {

                bool response = InventoryManager.UpdateInvOnCancellation(ReservationID);

                if (response)
                {
                    List<tbl_CommonInventoryRecord> AddRecord = new List<tbl_CommonInventoryRecord>();
                    for (int i = 0; i < Record.Count; i++)
                    {
                        tbl_CommonInventoryRecord Recordnew = new tbl_CommonInventoryRecord();
                        Recordnew.BookingId = Record[i].BookingId;
                        Recordnew.InvSid = Record[i].InvSid;
                        Recordnew.SupplierId = Record[i].SupplierId;
                        Recordnew.HotelCode = Record[i].HotelCode;
                        Recordnew.RoomType = Record[i].RoomType;
                        Recordnew.RateType = Record[i].RateType;
                        Recordnew.InvType = Record[i].InvType;
                        Recordnew.Date = Record[i].Date;
                        Recordnew.Month = Record[i].Month;
                        Recordnew.Year = Record[i].Year;
                        Recordnew.TotalAvlRoom = Record[i].TotalAvlRoom;
                        Recordnew.OldAvlRoom = Record[i].OldAvlRoom;
                        Recordnew.NoOfBookedRoom = Record[i].NoOfBookedRoom;
                        Recordnew.NoOfCancleRoom = Record[i].NoOfCancleRoom;
                        Recordnew.UpdateDate = Record[i].UpdateDate.ToString();
                        Recordnew.UpdateOn = Record[i].UpdateOn;
                        AddRecord.Add(Recordnew);
                    }
                    DB.tbl_CommonInventoryRecords.InsertAllOnSubmit(AddRecord);
                    DB.SubmitChanges();
                }
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                tbl_CommonHotelReservation Update = DB.tbl_CommonHotelReservations.Single(x => x.ReservationID == ReservationID && x.Uid == objGlobalDefault.sid);
                Update.Status = "Canceled";

                DB.SubmitChanges();

                var CancleDetail = (from obj in DB.tbl_CommonBookedRooms where obj.ReservationID == ReservationID select obj).ToList();
                var BookingDetail = (from obj in DB.tbl_CommonHotelReservations where obj.ReservationID == ReservationID select obj).ToList();
                var Cancle = CancleDetail[0].CutCancellationDate.Split('|');
                var Cancleamnt = CancleDetail[0].CancellationAmount.Split('|');
                List<DateTime> CDate = new List<DateTime>();
                DateTime CancleDate = DateTime.Now;
                decimal RefundAmnt;
                decimal CanclAmnt;
                decimal Total;
                //for (int i = 0; i < CancleDetail.Count; i++)
                //{
                //    CDate.Add(DateTime.ParseExact(CancleDetail[i].CutCancellationDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(i));
                //}

                for (int i = 0; i < Cancle.Length - 1; i++)
                {

                    if (CancleDate <= DateTime.ParseExact(Cancle[i], "dd-MM-yyyy hh:mm", System.Globalization.CultureInfo.InvariantCulture))
                    {
                        CanclAmnt = Convert.ToDecimal(Cancleamnt[i]);
                        Total = Convert.ToDecimal(CancleDetail[0].RoomAmount);
                        RefundAmnt = Total - CanclAmnt;
                        break;
                    }
                }

                //Email Sending


                Int64 ParentID = Convert.ToInt64(ConfigurationManager.AppSettings["AdminKey"]);
                // GlobalDefault objGlobalDefault = new GlobalDefault();
                if (HttpContext.Current.Session["LoginUser"] != null)
                {
                    objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    // ParentID = objGlobalDefault.sid;
                }

                //DBHandlerDataContext DB = new DBHandlerDataContext();
                var sReservation = (from obj in DB.tbl_CommonHotelReservations
                                    from objAgent in DB.tbl_AdminLogins
                                    where obj.ReservationID == ReservationID
                                    select new
                                    {
                                        customer_Email = objAgent.uid,
                                        customer_name = obj.AgencyName,
                                        VoucherNo = obj.VoucherID,
                                        Uid = obj.Uid,
                                        Email = obj.Uid,
                                        ContactPerson = objAgent.ContactPerson,
                                        CurrencyCode = objAgent.CurrencyCode,
                                        HotelName = obj.HotelName,
                                        City = obj.City,
                                        checkin = obj.CheckIn,
                                        checkout = obj.CheckOut,
                                        Ammount = obj.TotalFare,
                                        Nights = obj.NoOfDays,
                                        Passenger = obj.bookingname,
                                        InvoiceID = obj.InvoiceID,
                                        VoucherID = obj.VoucherID,
                                        bookingdate = obj.ReservationDate,
                                        Status = obj.Status,
                                    }).FirstOrDefault();

                //  var sMail = (from obj in DB.tbl_ActivityMails where obj.ParentID == objGlobalDefault.ParentId && obj.Activity == "Booking Cancelled" select obj).FirstOrDefault();

                var sMail = new tbl_ActivityMail();
                if (objGlobalDefault.UserType != "SupplierStaff")
                    sMail = (from obj in DB.tbl_ActivityMails where obj.ParentID == objGlobalDefault.ParentId && obj.Activity.Contains("Booking Cancelled") select obj).FirstOrDefault();
                else
                    sMail = (from obj in DB.tbl_ActivityMails where obj.ParentID == 232 && obj.Activity.Contains("Booking Cancelled") select obj).FirstOrDefault();

                string BCcTeamMails = sMail.BCcMail;
                string CcTeamMail = sMail.CcMail;
                string Logo = HttpContext.Current.Session["logo"].ToString();
                string Url = Convert.ToString(ConfigurationManager.AppSettings["URL"]);
                StringBuilder sb = new StringBuilder();

                sb.Append("<!DOCTYPE html>");
                sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">    ");
                sb.Append("<head>    ");
                sb.Append("<meta charset=\"utf-8\" />");
                sb.Append("</head>");
                sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: Segoe UI, Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">    ");
                sb.Append("<div style=\"height:50px;width:auto\">    ");
                sb.Append("<br />");
                sb.Append("<img src=\""+ Url + Logo + "\" alt=\"\" style=\"padding-left:10px;\" />    ");
                sb.Append("</div>");
                sb.Append("<br>");
                sb.Append("<div style=\"margin-left:10%\">");
                sb.Append("<p> <span style=\\\"margin-left:2%;font-weight:400\\\">Dear " + sReservation.ContactPerson + ",</span><br /></p>");
                sb.Append("<p> <span style=\\\"margin-left:2%;font-weight:400\\\">We have received your request, your Booking Details.</span><br /></p>       ");
                sb.Append("<style type=\"text/css\">");
                sb.Append(".tg  {border-collapse:collapse;border-spacing:0;}");
                sb.Append(".tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}    ");
                sb.Append(".tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}    ");
                sb.Append(".tg .tg-9hbo{font-weight:bold;vertical-align:top}");
                sb.Append(".tg .tg-yw4l{vertical-align:top}");
                sb.Append("@media screen and (max-width: 767px) {.tg {width: auto !important;}.tg col {width: auto !important;}.tg-wrap {overflow-x: auto;-webkit-overflow-scrolling: touch;}}</style>    ");
                sb.Append("<div class=\"tg-wrap\"><table class=\"tg\">");
                sb.Append("<tr>");
                sb.Append("<td class=\"tg-9hbo\"><b>Transaction ID</b></td>");
                sb.Append("<td class=\"tg-yw4l\">:  " + ReservationID + "</td>  ");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td class=\"tg-9hbo\"><b>Reservation Status</b></td>  ");
                sb.Append("<td class=\"tg-yw4l\">:" + sReservation.Status + "</td>  ");
                sb.Append("</tr>  ");
                sb.Append("<tr>  ");
                sb.Append("<td class=\"tg-9hbo\"><b>Reservation Date</b></td>  ");
                sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.bookingdate + "</td>");
                sb.Append("</tr>  ");
                sb.Append("<tr>  ");
                sb.Append("<td class=\"tg-9hbo\"><b>Hotel Name</b></td>  ");
                sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.HotelName + "</td>");
                sb.Append("</tr>  ");
                sb.Append("<tr>  ");
                sb.Append("<td class=\"tg-9hbo\"><b>Location</b></td>  ");
                sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.City + "</td>");
                sb.Append("</tr>");
                sb.Append("<tr>  ");
                sb.Append("<td class=\"tg-9hbo\"><b>Check-In</b></td>  ");
                sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.checkin + "</td>");
                sb.Append("</tr>");
                sb.Append("<tr>  ");
                sb.Append("<td class=\"tg-9hbo\"><b>Check-Out </b></td>");
                sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.checkout + "</td>");
                sb.Append("</tr>  ");
                sb.Append("<tr>  ");
                sb.Append("<td class=\"tg-9hbo\"><b>Rate</b></td>");
                sb.Append("<td class=\"tg-yw4l\">:" + sReservation.CurrencyCode + " " + Convert.ToInt32(sReservation.Ammount) / Convert.ToInt32(sReservation.Nights) + "</td>  ");
                sb.Append("</tr>  ");
                sb.Append("<tr>  ");
                sb.Append("<td class=\"tg-9hbo\"><b>Total</b></td>");
                sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.CurrencyCode + "" + sReservation.Ammount + "</td>  ");
                sb.Append("</tr>");
                sb.Append("</table></div>                            ");
                sb.Append("<p><span style=\\\"margin-left:2%;font-weight:400\\\">For any further clarification & query kindly feel free to contact us</span><br /></p>    ");
                sb.Append("<span style=\\\"margin-left:2%\\\">    ");
                sb.Append("<b>Thank You,</b><br />    ");
                sb.Append("</span>    ");
                sb.Append("<span style=\\\"margin-left:2%\\\">    ");
                sb.Append("Administrator    ");
                sb.Append("</span>                 ");
                sb.Append("        </div>       ");
                sb.Append("     <div>    ");
                sb.Append("        <table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">    ");
                sb.Append("            <tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">    ");
                sb.Append("                <td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">  Trivo.in</div></td>    ");
                sb.Append("            </tr>    ");
                sb.Append("        </table>    ");
                sb.Append("     </div>    ");
                sb.Append("     </body>    ");
                sb.Append("     </html>");

                string Title = "Cancel Confirmation";
                SendMail(sReservation.customer_Email, Title, sb.ToString(), "", CcTeamMail);

                string Message = EmailManager.GetErrorMessage("Booking Cancelled");
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, Message = Message });
            }
            catch
            {
                string Message = EmailManager.GetErrorMessage("Booking Cancelled Error");
                return jsSerializer.Serialize(new { retCode = 0, Session = 1, Message = Message });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetDetail(string ReservationID)
        {
            try
            {
                var Detail = (from obj in DB.tbl_CommonHotelReservations
                              join objPass in DB.tbl_CommonBookedPassengers on obj.ReservationID equals objPass.ReservationID
                              join objroom in DB.tbl_CommonBookedRooms on obj.ReservationID equals objroom.ReservationID
                              where objPass.IsLeading == true && obj.ReservationID == ReservationID
                              select new
                              {
                                  obj.Sid,
                                  obj.ReservationID,
                                  obj.ReservationDate,
                                  obj.Status,
                                  obj.TotalFare,
                                  obj.TotalRooms,
                                  obj.HotelName,
                                  obj.CheckIn,
                                  obj.CheckOut,
                                  obj.City,
                                  obj.Children,
                                  obj.BookingStatus,
                                  obj.NoOfAdults,
                                  obj.Source,
                                  obj.Uid,
                                  obj.NoOfDays,
                                  objPass.Name,
                                  objPass.LastName,
                                  objPass.RoomNumber,
                                  objroom.CancellationAmount,
                                  objroom.CutCancellationDate
                              }).ToList().Distinct();

                return jsSerializer.Serialize(new { retCode = 1, Session = 1, Detail = Detail });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }


        [WebMethod(EnableSession = true)]
        public string SaveConfirmDetail(string HotelName, string ReservationId, string ConfirmDate, string StaffName, string ReconfirmThrough, string HotelConfirmationNo, string Comment)
        {
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 UserId = objGlobalDefault.sid;
                tbl_CommonHotelReconfirmationDetail Confirm = new tbl_CommonHotelReconfirmationDetail();
                Confirm.UserID = UserId;
                Confirm.ReservationId = ReservationId;
                Confirm.ReconfirmDate = ConfirmDate;
                Confirm.Staff_Name = StaffName;
                Confirm.ReconfirmThrough = ReconfirmThrough;
                Confirm.HotelConfirmationNo = HotelConfirmationNo;
                Confirm.Comment = Comment;
                DB.tbl_CommonHotelReconfirmationDetails.InsertOnSubmit(Confirm);
                DB.SubmitChanges();

                tbl_CommonHotelReservation Bit = DB.tbl_CommonHotelReservations.Single(x => x.ReservationID == ReservationId);
                Bit.IsConfirm = true;
                DB.SubmitChanges();

                ReconfirmationMail(Confirm.sid, HotelName, ReservationId, StaffName, Comment);

                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        public static bool ReconfirmationMail(Int64 Sid, string HotelName, string ReservationId, string ReconfirmBy, string Comment)
        {

            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                var sReservation = (from obj in DB.tbl_CommonHotelReservations
                                    from objAgent in DB.tbl_AdminLogins
                                    where obj.ReservationID == ReservationId
                                    select new
                                    {
                                        customer_Email = objAgent.uid,
                                        customer_name = obj.AgencyName,
                                        VoucherNo = obj.VoucherID,

                                    }).FirstOrDefault();

                var sMail = (from obj in DB.tbl_ActivityMails where obj.Activity == "Booking Reconfirm" select obj).FirstOrDefault();
                string BCcTeamMails = sMail.BCcMail;
                string CcTeamMail = sMail.CcMail;


                StringBuilder sb = new StringBuilder();

                sb.Append("<html>");
                sb.Append("<head>");
                sb.Append("<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">");
                sb.Append("<meta name=Generator content=\"Microsoft Word 12 (filtered)\">");
                sb.Append("<style>");
                sb.Append("<!--");
                sb.Append(" /* Font Definitions */");
                sb.Append(" @font-face");
                sb.Append("	{font-family:\"Cambria Math\";");
                sb.Append("	panose-1:2 4 5 3 5 4 6 3 2 4;}");
                sb.Append("@font-face");
                sb.Append("	{font-family:Calibri;");
                sb.Append("	panose-1:2 15 5 2 2 2 4 3 2 4;}");
                sb.Append(" /* Style Definitions */");
                sb.Append(" p.MsoNormal, li.MsoNormal, div.MsoNormal");
                sb.Append("	{margin-top:0cm;");
                sb.Append("	margin-right:0cm;");
                sb.Append("	margin-bottom:10.0pt;");
                sb.Append("	margin-left:0cm;");
                sb.Append("	line-height:115%;");
                sb.Append("	font-size:11.0pt;");
                sb.Append("	font-family:\"Calibri\",\"sans-serif\";}");
                sb.Append("a:link, span.MsoHyperlink");
                sb.Append("	{color:blue;");
                sb.Append("	text-decoration:underline;}");
                sb.Append("a:visited, span.MsoHyperlinkFollowed");
                sb.Append("	{color:purple;");
                sb.Append("	text-decoration:underline;}");
                sb.Append(".MsoPapDefault");
                sb.Append("	{margin-bottom:10.0pt;");
                sb.Append("	line-height:115%;}");
                sb.Append("@page Section1");
                sb.Append("	{size:595.3pt 841.9pt;");
                sb.Append("	margin:72.0pt 72.0pt 72.0pt 72.0pt;}");
                sb.Append("div.Section1");
                sb.Append("	{page:Section1;}");
                sb.Append("-->");
                sb.Append("</style>");
                sb.Append("</head>");
                sb.Append("<body lang=EN-IN link=blue vlink=purple>");
                sb.Append("<div class=Section1>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Dear ");
                sb.Append("" + sReservation.customer_name + ",</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Greetings ");
                sb.Append("from&nbsp;<b>Click<span style='color:#ED7D31'>Ur</span><span style='color:#4472C4'>Trip</span></b></span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Once ");
                sb.Append("again thanks for choosing our service, we believe that smooth check-in will");
                sb.Append("defiantly satisfy your guest, so we take this efforts to reconfirm your booking</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                sb.Append("<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0");
                sb.Append(" style='border-collapse:collapse'>");
                sb.Append(" <tr>");
                sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Transaction");
                sb.Append("  No.</span></p>");
                sb.Append("  </td>");
                sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + sReservation.customer_name + "</span></p>");
                sb.Append("  </td>");
                sb.Append(" </tr>");
                sb.Append(" <tr>");
                sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hotel");
                sb.Append("  Name</span></p>");
                sb.Append("  </td>");
                sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'> " + HotelName + "</span></p>");
                sb.Append("  </td>");
                sb.Append(" </tr>");
                sb.Append(" <tr>");
                sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Reservation");
                sb.Append("  ID</span></p>");
                sb.Append("  </td>");
                sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + ReservationId + "</span></p>");
                sb.Append("  </td>");
                sb.Append(" </tr>");
                sb.Append(" <tr>");
                sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Reconfirmed");
                sb.Append("  by</span></p>");
                sb.Append("  </td>");
                sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + ReconfirmBy + "</span></p>");
                sb.Append("  </td>");
                sb.Append(" </tr>");
                sb.Append(" <tr>");
                sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Remark");
                sb.Append("  / Note</span></p>");
                sb.Append("  </td>");
                sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + Comment + "</span></p>");
                sb.Append("  </td>");
                sb.Append(" </tr>");
                sb.Append("</table>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span lang=EN-US style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hope ");
                sb.Append("the above is correct &amp;&nbsp;</span><span style='font-size:12.0pt;");
                sb.Append("font-family:\"Times New Roman\",\"serif\"'>your guest will enjoy the stay</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>In ");
                sb.Append("case of any discrepancy kindly contact us immediately at&nbsp;<a");
                sb.Append("href=\"mailto:hotels@clickurtrip.com\" target=\"_blank\"><span style='color:#1155CC'>hotels@clickurtrip.com</span></a></span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                sb.Append("\"Arial\",\"sans-serif\";color:#222222'>&nbsp;</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                sb.Append("\"Arial\",\"sans-serif\";color:#222222'>Best Regards,</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                sb.Append("\"Arial\",\"sans-serif\";color:#222222'>Online Reservation Team</span></p>");
                sb.Append("<p class=MsoNormal>&nbsp;</p>");
                sb.Append("</div>");
                sb.Append("</body>");
                sb.Append("</html>");

                string Title = "Hotel reconfirmation - " + HotelName + " - " + sReservation.VoucherNo + "";

                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));
                List<string> attachmentList = new List<string>();

                Dictionary<string, string> Email1List = new Dictionary<string, string>();
                Dictionary<string, string> Email2List = new Dictionary<string, string>();
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                Email1List.Add(sReservation.customer_Email, "");
                Email1List.Add(BCcTeamMails, "");
                Email2List.Add(CcTeamMail, "");

                MailManager.SendMail(accessKey, Email1List, Email2List, Title, sb.ToString(), from, attachmentList);
                return true;

            }
            catch
            {
                return false;
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetSupplier()
        {
            try
            {
                var List = (from obj in DB.tbl_AdminLogins where obj.UserType=="Supplier"

                            select new
                            {
                                obj.sid,
                                obj.AgencyName
                            }).ToList();
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string GetBookingListSupplier(string[] SupplierList)
        {
            try
            {
                List<tbl_AdminLogin> ListData = new List<tbl_AdminLogin>();
                for (int i = 0; i < SupplierList.Length; i++)
                {
                    if(SupplierList[i] != null)
                    {
                        var Liist = (from obj in DB.tbl_AdminLogins
                                     where obj.ParentID == Convert.ToInt32(SupplierList[i])

                                    select new
                                    {
                                        obj.sid,
                                        obj.AgencyName
                                    }).ToList();

                        for (int j = 0; j < Liist.Count; j++)
                        {
                            ListData.Add(new tbl_AdminLogin
                            {
                                sid = Liist[j].sid,
                                AgencyName = Liist[j].AgencyName
                            });
                        }
                        
                    }
                    
                }
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = ListData });
                
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        public class ListtoDataTable
        {
            public DataTable ToDataTable<T>(List<T> items)
            {
                DataTable dataTable = new DataTable(typeof(T).Name);
                //Get all the properties by using reflection   
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names  
                    dataTable.Columns.Add(prop.Name);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {

                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }

                return dataTable;
            }
        }


    }
}
