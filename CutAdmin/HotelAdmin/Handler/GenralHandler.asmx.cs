﻿using CutAdmin.BL;
using CutAdmin.Common;
using CutAdmin.DataLayer;
using CutAdmin.HotelAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.HotelAdmin.Handler
{
    /// <summary>
    /// Summary description for GenralHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class GenralHandler : System.Web.Services.WebService
    {
        AdminDBHandlerDataContext DB = new AdminDBHandlerDataContext();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        string json = "";

        //[WebMethod(EnableSession = true)]
        //public string GetActiveAgentDetail()
        //{
        //    try
        //    {
        //        var List = (from obj in DB.tbl_AdminLogins
        //                    join CrdLimt in DB.tbl_AdminCreditLimits on obj.sid equals CrdLimt.uid
        //                    join Contact in DB.tbl_Contacts on obj.ContactID equals Contact.ContactID

        //                    select new
        //                    {
        //                        obj.sid,

        //                    }).ToList();
        //        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
        //    }
        //    catch
        //    {
        //        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return json;
        //}

        [WebMethod(EnableSession = true)]
        public string AddBillingCycle(Int64 Id, Int64 Billing)
        {
            try
            {
                tbl_AdminLogin Update = DB.tbl_AdminLogins.Single(x => x.sid == Id);
                Update.CommssionsDays = Billing;
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string GetPassword(string password)
        {
            string decryptpassword = Common.Cryptography.DecryptText(password);
            if (decryptpassword != null)
                return "{\"Session\":\"1\",\"retCode\":\"1\",\"password\":\"" + decryptpassword + "\"}";
            else
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }

        [WebMethod(EnableSession = true)]
        public string GetLogo()
        {
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                var AdminCode = objGlobalDefault.Agentuniquecode;
                var AdminName = objGlobalDefault.AgencyName;
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, AdminCode = AdminCode, AdminName = AdminName });
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        [WebMethod(true)]
        public string ActivateLogin(Int64 sid, string status)
        {
            bool response = true;
            try
            {
                if (status == "0")
                    response = false;
                tbl_AdminLogin Update = DB.tbl_AdminLogins.Single(x => x.sid == sid);
                Update.LoginFlag = response;
                DB.SubmitChanges();

                var AdminData = (from obj in DB.tbl_AdminLogins where obj.sid == sid select obj).FirstOrDefault();
                string sEmail = AdminData.uid;
                string AgentCode = AdminData.Agentuniquecode;
                string AgencyName = AdminData.AgencyName;

                if (status == "0")
                {
                    EmailManager.DeActivateNotificationMail(sEmail, AgentCode, AgencyName);
                }
                else if (status == "1")
                {
                    EmailManager.ActivateNotificationMail(sEmail, AgentCode, AgencyName);
                }

                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        public class ListtoDataTable
        {
            public DataTable ToDataTable<T>(List<T> items)
            {
                DataTable dataTable = new DataTable(typeof(T).Name);
                //Get all the properties by using reflection   
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names  
                    dataTable.Columns.Add(prop.Name);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {

                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }

                return dataTable;
            }
        }

        [WebMethod(EnableSession = true)]
        public string ChangePassword(string sOldPassword, string sNewPassword)
        {
            string retString = SupplierProfileManager.ChangePassword(sOldPassword, sNewPassword);
            if (retString == "Success")
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                objGlobalDefault.password = sNewPassword;
                HttpContext.Current.Session["LoginUser"] = objGlobalDefault;
                return "{\"Session\":\"1\",\"retCode\":\"1\",\"Status\":\"1\"}";
            }
            else if (retString == "Exception_PasswordsNotMatch")
                return "{\"Session\":\"1\",\"retCode\":\"0\",\"Status\":\"0\"}";
            else if (retString == "Exception_SQLException")
                return "{\"Session\":\"1\",\"retCode\":\"0\",\"Status\":\"-1\"}";
            else
                return "{\"Session\":\"0\",\"retCode\":\"0\",\"Status\":\"-1\"}";
        }

        [WebMethod(EnableSession = true)]
        public string AgentChangePassword(int sid, string password)
        {
            int rows;
            string jsonString = "";
            string encryptpass = Cryptography.EncryptText(password);
            try
            {
                tbl_AdminLogin Update = DB.tbl_AdminLogins.Single(x => x.sid == sid);
                Update.password = encryptpass;
                DB.SubmitChanges();


                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch (Exception)
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return jsonString;
        }

        [WebMethod(true)]
        public string GetBankNames()
        {
            try
            {
                var List = (from obj in DB.tbl_BankNames select obj).ToList();
                if (List.Any())
                {
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                }
                else
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        #region BankDetails
        [WebMethod(EnableSession = true)]
        public string AddBankDetails(string BankName, string AccountNo, string Branch, string SwiftCode, string Country)
        {
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                tbl_BankDetail Add = new tbl_BankDetail();
                Add.ParentId = objGlobalDefault.sid;
                Add.BankName = BankName;
                Add.AccountNo = AccountNo;
                Add.Branch = Branch;
                Add.SwiftCode = SwiftCode;
                Add.Country = Country;
                DB.tbl_BankDetails.InsertOnSubmit(Add);
                DB.SubmitChanges();

                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetBankDetails()
        {
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                var List = (from obj in DB.tbl_BankDetails where obj.ParentId == objGlobalDefault.sid select obj).ToList();

                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BankDetails = List });
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateBankDetail(string sid, string BankName, string AccountNo, string Branch, string SwiftCode, string Country)
        {
            try
            {
                Int64 ID = Convert.ToInt64(sid);
                tbl_BankDetail Update = DB.tbl_BankDetails.Single(x => x.sid == ID);
                Update.BankName = BankName;
                Update.AccountNo = AccountNo;
                Update.Branch = Branch;
                Update.SwiftCode = SwiftCode;
                Update.Country = Country;
                DB.SubmitChanges();

                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string DeleteBankDetail(string sid)
        {
            try
            {
                Int64 ID = Convert.ToInt64(sid);
                tbl_BankDetail Delete = DB.tbl_BankDetails.Single(x => x.sid == ID);
                DB.tbl_BankDetails.DeleteOnSubmit(Delete);
                DB.SubmitChanges();

                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }
        #endregion

        [WebMethod(true)]
        public string GetLastUpdateCreditDetails(Int64 uid)
        {
            string jsonString = "";
            DataTable dtResult;
            JavaScriptSerializer objSerialize = new JavaScriptSerializer();
            DBHelper.DBReturnCode retcode = UpdateCreditManager.GetLastUpdateCreditDetails(uid, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Dictionary<string, object>> dtTable = new List<Dictionary<string, object>>();
                dtTable = JsonStringManager.ConvertDataTable(dtResult);
                return objSerialize.Serialize(new { Session = 1, retCode = 1, tbl_AdminCreditLimit = dtTable });
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }


        #region BankDeposit

        [WebMethod(EnableSession = true)]
        public string GetBankDepositDetail()
        {
            string jsonString = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = BankDepositManager.GetBankDepositDetail(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Dictionary<string, object>> BankDeposit = new List<Dictionary<string, object>>();
                BankDeposit = JsonStringManager.ConvertDataTable(dtResult);
                dtResult.Dispose();
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, tbl_BankDeposit = BankDeposit });
            }
            else
            {
                return jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

        }

        [WebMethod(EnableSession = true)]
        public string UnApproveDeposit(int uid, Int64 sId)
        {
            try
            {
                tbl_BankDeposit List = DB.tbl_BankDeposits.Where(d => d.sId == sId).FirstOrDefault();
                List.UnApproveFlag = true;
                DB.SubmitChanges();

                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(true)]
        public string UserLogin(string sUserName, string sPassword)
        {
            DBHelper.DBReturnCode retCode = BankDepositManager.UserLogin(sUserName, sPassword);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                if (objGlobalDefault.UserType == "Admin" || objGlobalDefault.UserType == "Franchisee")
                {
                    return "{\"Session\":\"1\",\"retCode\":\"1\",\"roleID\":\"Admin\"}";
                }
                else if (objGlobalDefault.UserType == "Agent")
                {
                    return "{\"Session\":\"1\",\"retCode\":\"1\",\"roleID\":\"Agent\"}";
                }
                else if(objGlobalDefault.UserType == "Supplier" || objGlobalDefault.UserType == "SupplierStaff")
                {
                    return "{\"Session\":\"1\",\"retCode\":\"1\",\"roleID\":\"Supplier\"}";
                }
                else
                    return "{\"Session\":\"1\",\"retCode\":\"0\",\"roleID\":\"Null\"}";

            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\",\"roleID\":\"Null\"}";
            }
        }

        [WebMethod(true)]
        public string ApproveDeposit(int uid, decimal dDepositAmount, string sTypeofCash, string sTypeofcheque, string sComment, string sLastUpdatedDate, Int64 sId)
        {
            int rows;
            string jsonString = "";
            DataTable dtResult = null;
            DBHelper.DBReturnCode retcode1 = UpdateCreditManager.ValidateLimit(uid, out dtResult);

            DBHelper.DBReturnCode retcode = BankDepositManager.ApproveDeposit(uid, dDepositAmount, sTypeofCash, sTypeofcheque, sComment, sLastUpdatedDate, sId, out rows);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        #endregion BankDeposit

        #region AccountDetails

        [WebMethod(true)]
        public string GetAccountDetails(Int64 id)
        {
            try
            {
                var List = (from obj in DB.tbl_AdminCreditLimits where obj.uid == id orderby obj.sid descending select obj).ToList();
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, AccountDetails = List });
            }
            catch (Exception)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }
        #endregion AccountDetails

        #region AdminStaff

        [WebMethod(EnableSession = true)]
        public string GetStaffDetails()
        {
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 ParentId = objGlobalDefault.ParentId;

                var Stafflist = (from obj in DB.tbl_StaffLogins
                                 join objc in DB.tbl_Contacts on obj.ContactID equals objc.ContactID
                                 where obj.ParentId == ParentId && obj.UserType == "AdminStaff"
                                 select new
                                 {
                                     obj.ContactPerson,
                                     obj.Last_Name,
                                     obj.Designation,
                                     obj.dtLastAccess,
                                     obj.Gender,
                                     obj.uid,
                                     obj.sid,
                                     obj.Department,
                                     obj.StaffUniqueCode,
                                     obj.password,
                                     obj.LoginFlag,
                                     obj.PANNo,
                                     obj.Updatedate,
                                     objc.Address,
                                     objc.email,
                                     objc.Mobile,
                                     objc.phone

                                 }).ToList();
                Session["StafflistSession"] = ConvertToDatatable(Stafflist);
                return jsSerializer.Serialize(new { Session = 1, retCode = 1, Staff = Stafflist });
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string StaffDetails()
        {
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 Id = objGlobalDefault.sid;
                var Stafflist = (from obj in DB.tbl_StaffLogins
                                 join objc in DB.tbl_Contacts on obj.ContactID equals objc.ContactID
                                 where obj.sid == Id && (obj.UserType == "AdminStaff" || obj.UserType == "SupplierStaff")
                                 select new
                                 {
                                     obj.ContactPerson,
                                     obj.Designation,
                                     obj.dtLastAccess,
                                     obj.Gender,
                                     obj.uid,
                                     obj.sid,
                                     obj.Department,
                                     obj.StaffUniqueCode,
                                     obj.password,
                                     obj.LoginFlag,
                                     obj.PANNo,
                                     obj.Updatedate,
                                     objc.Address,
                                     objc.email,
                                     objc.Mobile,
                                     objc.phone

                                 }).ToList();
                Session["StafflistSession"] = ConvertToDatatable(Stafflist);
                return jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = Stafflist });
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
        }

        [WebMethod(true)]
        public string CheckUser()
        {
            if (Session["LoginUser"] != null)
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                if (objGlobalDefault.UserType == "Admin" || objGlobalDefault.UserType == "Franchisee")
                {
                    return "{\"Session\":\"1\",\"retCode\":\"1\",\"UserType\":\"Admin\"}";
                }
                else if (objGlobalDefault.UserType == "AdminStaff")
                {
                    return "{\"Session\":\"1\",\"retCode\":\"1\",\"UserType\":\"AdminStaff\"}";
                }

                else if (objGlobalDefault.UserType == "Supplier")
                {
                    return "{\"Session\":\"1\",\"retCode\":\"1\",\"UserType\":\"Supplier\"}";
                }
                else if (objGlobalDefault.UserType == "SupplierStaff")
                {
                    return "{\"Session\":\"1\",\"retCode\":\"1\",\"UserType\":\"SupplierStaff\"}";
                }
                return "";
            }
            else
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }

        [WebMethod(EnableSession = true)]
        public string StaffChangePassword(int sid, string password)
        {
            string jsonString = "";
            string encryptpass = Cryptography.EncryptText(password);
            try
            {
                tbl_StaffLogin Update = DB.tbl_StaffLogins.Single(x => x.sid == sid);
                Update.password = encryptpass;
                DB.SubmitChanges();
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch (Exception)
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return jsonString;
        }

        [WebMethod(true)]
        public string ActivateStaffLogin(Int64 sid, bool status)
        {
            bool response = true;
            try
            {
                if (status == true)
                    response = false;
                tbl_StaffLogin Update = DB.tbl_StaffLogins.Single(x => x.sid == sid);
                Update.LoginFlag = response;
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod]
        public string Delete(string uid)
        {
            try
            {
                tbl_StaffLogin Delete = DB.tbl_StaffLogins.Single(x => x.uid == uid);
                DB.tbl_StaffLogins.DeleteOnSubmit(Delete);
                DB.SubmitChanges();

                tbl_Contact DeleteCont = DB.tbl_Contacts.Single(x => x.email == uid);
                DB.tbl_StaffLogins.DeleteOnSubmit(Delete);
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        //[WebMethod(EnableSession = true)]
        //public string UpdateStaff(Int64 sid, string sFirstName, string sMiddleName, string sLastName, string sDesignation, string sMobile, string sEmail, string sPhone, string sAddress, string sCode, string sPinCode, string hiddenLoginFlag, string Department)
        //{
        //    try
        //    {
        //        string Updatedate = DateTime.Now.ToString("dd-MM-yyyy");
        //        tbl_StaffLogin Update = DB.tbl_StaffLogins.Single(x => x.sid == sid);
        //        Update.ContactPerson = sFirstName;
        //        Update.Last_Name = sLastName;
        //        Update.Designation = sDesignation;
        //        Update.Updatedate = Convert.ToDateTime(Updatedate);
        //        Update.Department = Department;

        //        tbl_Contact ContUpdate = DB.tbl_Contacts.Single(x => x.email == sEmail);
        //        ContUpdate.phone = sPhone;
        //        ContUpdate.Mobile = sMobile;
        //        ContUpdate.email = sEmail;
        //        ContUpdate.Address = sAddress;
        //        ContUpdate.Code = sCode;
        //        ContUpdate.PinCode = sPinCode;

        //        DB.SubmitChanges();
        //        json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
        //    }
        //    catch (Exception)
        //    {
        //        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return json;
        //}

        //[WebMethod(EnableSession = true)]
        //public string AddNewStaff(string sFirstName, string sMiddleName, string sLastName, string sDesignation, string sMobile, string sEmail, string sPhone, string sAddress, string sCode, string sPinCode, string hiddenLoginFlag, string Department)
        //{
        //    try
        //    {
        //        if (HttpContext.Current.Session["LoginUser"] != null)
        //        {
        //            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //            Int64 ParentID = objGlobalDefault.ParentId;
        //            string sUserType = "AdminStaff";
        //            if (objGlobalDefault.UserType == "Admin")
        //            {
        //                sUserType = "AdminStaff";
        //            }
        //            else
        //            {
        //                sUserType = "FranchiseeStaff";
        //            }

        //            string StaffUniqueCode = "STF-" + GenerateRandomString(4);
        //            string sPassword = GenerateRandomString(8);
        //            string sEncryptedPassword = Common.Cryptography.EncryptText(sPassword);
        //            bool LoginFlag = false;
        //            string Updatedate = DateTime.Now.ToString("dd-MM-yyyy");
        //            string Validity = DateTime.Now.AddYears(1).ToString("dd-MM-yyyy");

        //            tbl_StaffLogin Add = new tbl_StaffLogin();
        //            Add.ContactPerson = sFirstName + " " + sLastName;
        //            Add.Last_Name = sLastName;
        //            Add.Designation = sDesignation;
        //            Add.Department = Department;
        //            Add.LoginFlag = LoginFlag;
        //            Add.ParentId = ParentID;
        //            Add.UserType = sUserType;
        //            Add.uid = sEmail;
        //            Add.password = sEncryptedPassword;
        //            Add.Updatedate = Convert.ToDateTime(Updatedate);
        //            Add.dtLastAccess = Convert.ToDateTime(Updatedate);
        //            Add.Validity = Convert.ToDateTime(Validity);
        //            Add.RoleID = 0;
        //            Add.StaffUniqueCode = StaffUniqueCode;
        //            DB.tbl_StaffLogins.InsertOnSubmit(Add);
        //            DB.SubmitChanges();
        //            var ContactId = Add.sid;


        //            tbl_Contact AddCont = new tbl_Contact();
        //            AddCont.Address = sAddress;
        //            AddCont.ContactID = ContactId;
        //            AddCont.email = sEmail;
        //            AddCont.Mobile = sMobile;
        //            DB.tbl_Contacts.InsertOnSubmit(AddCont);
        //            DB.SubmitChanges();

        //        }

        //        json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
        //    }
        //    catch (Exception)
        //    {
        //        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return json;


        //    return json;
        //}

        [WebMethod(EnableSession = true)]
        public string AddNewStaff(string sFirstName, string sMiddleName, string sLastName, string sDesignation, string sMobile, string sEmail, string sPhone, string sAddress, string sCode, string sPinCode, string Department)
        {
            Int64 sid = 0;
            string hiddenLoginFlag = "";
            DBHelper.DBReturnCode retCode = StaffManager.AddUpdateStaff(sid, sFirstName, sMiddleName, sLastName, sDesignation, sMobile, sEmail, sPhone, sAddress, sCode, sPinCode, hiddenLoginFlag, Department);
            if (DBHelper.DBReturnCode.SUCCESS == retCode)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }

        [WebMethod(EnableSession = true)]
        public string UpdateStaff(Int64 sid, string sFirstName, string sMiddleName, string sLastName, string sDesignation, string sMobile, string sEmail, string sPhone, string sAddress, string sCode, string sPinCode, string hiddenLoginFlag, string Department)
        {
            DBHelper.DBReturnCode retCode = StaffManager.AddUpdateStaff(sid, sFirstName, sMiddleName, sLastName, sDesignation, sMobile, sEmail, sPhone, sAddress, sCode, sPinCode, hiddenLoginFlag, Department);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

        [WebMethod(EnableSession = true)]
        public string UpdateStaffProfile(Int64 sid, string sFirstName, string sLastName, string sDesignation, string sMobile, string sEmail, string Department)
        {
            try
            {
                string Updatedate = DateTime.Now.ToString("dd-MM-yyyy");
                tbl_StaffLogin Update = DB.tbl_StaffLogins.Single(x => x.sid == sid);
                Update.ContactPerson = sFirstName + " " + sLastName;
                Update.Last_Name = sLastName;
                Update.Designation = sDesignation;
                Update.Updatedate = Convert.ToDateTime(Updatedate);
                Update.Department = Department;
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 ContactID = objGlobalDefault.ContactID;
                tbl_Contact ContUpdate = DB.tbl_Contacts.Single(x => x.ContactID == ContactID);
                ContUpdate.Mobile = sMobile;
                ContUpdate.email = sEmail;
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }



        public static string GenerateRandomString(int length)
        {
            string rndstring = "";
            bool IsRndlength = false;
            Random rnd = new Random((int)DateTime.Now.Ticks);
            if (IsRndlength)
                length = rnd.Next(4, length);
            for (int i = 0; i < length; i++)
            {
                int toss = rnd.Next(1, 10);
                if (toss > 5)
                    rndstring += (char)rnd.Next((int)'A', (int)'Z');
                else
                    rndstring += rnd.Next(0, 9).ToString();
            }
            return rndstring;
        }

        public static int GenerateRandomNumber()
        {
            int rndnumber = 0;
            Random rnd = new Random((int)DateTime.Now.Ticks);
            rndnumber = rnd.Next();
            return rndnumber;
        }

        #endregion

        [WebMethod(EnableSession = true)]
        public string GetGroups()
        {
            try
            {
                var List = (from obj in DB.tbl_GroupMarkups select obj).ToList();
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Group = List });
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }




        #region Supplier

        [WebMethod(true)]
        public string AddAgent(string sAgencyName, string sEmail, string sFirstName, string sLastName, string sDesignation, string sAddress, string sCity, string nPinCode, string nIATA, string sUsertype, string nPAN, string nPhone, string nMobile, string nFax, string sServiceTaxNumber, string sWebsite, string sGroupId, string sGroupName, string sAgencyLogo, string sRemarks, string sbuttonvalue, string Currency, string FranchiseeId, string GSTNO)
        {
            Int64 sid = 0;
            string hiddenAgentUniqueCode = "";
            string hiddenLoginFlag = "";
            DBHelper.DBReturnCode retCode = AdminDetailsManager.AddUpdateAgent(sid, sAgencyName, sEmail, sFirstName, sLastName, sDesignation, sAddress, sCity, nPinCode, nIATA, sUsertype, nPAN, nPhone, nMobile, nFax, sServiceTaxNumber, sWebsite, sGroupId, sGroupName, sAgencyLogo, sRemarks, hiddenAgentUniqueCode, hiddenLoginFlag, sbuttonvalue, Currency, FranchiseeId, GSTNO);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {

                // string Contact = sFirstName + " " + sLastName;
                // AdminDetailsManager.AgencyTranfer(sAgencyName, GSTNO, "", Contact, sDesignation, sAddress, "", "", nPinCode, nPhone, nMobile, nFax, sEmail, sWebsite, nPAN);
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

        [WebMethod(EnableSession = true)]
        public string UpdateRegisteredAgent(Int64 sid, string sAgencyName, string sEmail, string sFirstName, string sLastName, string sDesignation, string sAddress, string sCity, string nPinCode, string nIATA, string sUsertype, string nPAN, string nPhone, string nMobile, string nFax, string sServiceTaxNumber, string sWebsite, string sGroupId, string sGroupName, string sAgencyLogo, string sRemarks, string hiddenAgentUniqueCode, string hiddenLoginFlag, string sbuttonvalue, string Currency, string FranchiseeId, string GSTNO)
        {
            DBHelper.DBReturnCode retCode = AdminDetailsManager.AddUpdateAgent(sid, sAgencyName, sEmail, sFirstName, sLastName, sDesignation, sAddress, sCity, nPinCode, nIATA, sUsertype, nPAN, nPhone, nMobile, nFax, sServiceTaxNumber, sWebsite, sGroupId, sGroupName, sAgencyLogo, sRemarks, hiddenAgentUniqueCode, hiddenLoginFlag, sbuttonvalue, Currency, FranchiseeId, GSTNO);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }


        //[WebMethod(EnableSession = true)]
        //public string AddAgent(string sAgencyName, string sEmail, string sFirstName, string sLastName, string sDesignation,
        //    string sAddress, string sCity, string nPinCode, string nIATA, string sUsertype, string nPAN, string nPhone,
        //    string nMobile, string nFax, string sServiceTaxNumber, string sWebsite, string sGroupId, string sGroupName,
        //    string sAgencyLogo, string sRemarks, string sbuttonvalue,
        //    string Currency, string FranchiseeId, string GSTNO)
        //{
        //    try
        //    {
        //        string UserCode = "";
        //        if (sUsertype == "Agent")
        //        {
        //            UserCode = "AG-";
        //        }
        //        else if (sUsertype == "Supplier")
        //        {
        //            UserCode = "SU-";
        //        }
        //        else if (sUsertype == "Franchisee")
        //        {
        //            UserCode = "FR-";
        //        }
        //        else if (sUsertype == "Corporate")
        //        {
        //            UserCode = "CO-";
        //        }

        //        string AgencyLogoPath = "";
        //        string AgencyType = "A";
        //        string sPassword = GenerateRandomString(8);
        //        string sEncryptedPassword = Common.Cryptography.EncryptText(sPassword);
        //        int RoleId;
        //        string MerchantID = "CUT-" + GenerateRandomString(4);
        //        DateTime Validity = DateTime.Now.AddYears(1);
        //        DateTime LastAccess = DateTime.Now;
        //        string ValidationCode = "VC-" + GenerateRandomNumber();

        //        bool LoginFlag;

        //        string AgentUniqueCode = UserCode + GenerateRandomString(4);

        //        string Groups = sGroupName;
        //        GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //        Int64 ParentID = objGlobalDefault.ParentId;

        //        LoginFlag = false;
        //        if (sUsertype == "Agent")
        //        {
        //            RoleId = 4;
        //            if (objGlobalDefault.UserType == "Franchisee")
        //            {
        //                FranchiseeId = objGlobalDefault.Franchisee;
        //            }
        //        }
        //        else if (sUsertype == "Corporate")
        //        {
        //            RoleId = 5;
        //        }
        //        else if (sUsertype == "Franchisee" || sUsertype == "Supplier")
        //        {
        //            RoleId = 3;
        //            if (FranchiseeId == "")
        //            {
        //                FranchiseeId = sAgencyName + "-" + GenerateRandomString(4);
        //            }
        //        }
        //        else
        //            RoleId = 7;

        //        tbl_Contact AddContact = new tbl_Contact();
        //        AddContact.Address = sAddress;
        //        AddContact.PinCode = nPinCode;
        //        AddContact.phone = nPhone;
        //        AddContact.Mobile = nMobile;
        //        AddContact.Fax = nFax;
        //        AddContact.Website = sWebsite;
        //        DB.tbl_Contacts.InsertOnSubmit(AddContact);
        //        DB.SubmitChanges();
        //        Int64 ContactId = AddContact.ContactID;

        //        tbl_AdminLogin Add = new tbl_AdminLogin();
        //        Add.AgencyName = sAgencyName;
        //        Add.uid = sEmail;
        //        Add.ContactPerson = sFirstName + " " + sLastName;
        //        Add.Last_Name = sLastName;
        //        Add.Designation = sDesignation;
        //        Add.IATANumber = nIATA;
        //        Add.UserType = sUsertype;
        //        Add.PANNo = nPAN;
        //        Add.ContactID = ContactId;
        //        Add.ServiceTaxNO = sServiceTaxNumber;
        //        Add.AgencyLogo = sAgencyLogo;
        //        Add.Remarks = sRemarks;
        //        Add.CurrencyCode = Currency;
        //        Add.FranchiseeId = FranchiseeId;
        //        Add.GSTNumber = GSTNO;
        //        Add.RoleID = RoleId;
        //        Add.LoginFlag = Convert.ToBoolean(LoginFlag);
        //        Add.Agentuniquecode = AgentUniqueCode;
        //        Add.AgencyType = AgencyType;
        //        Add.password = sEncryptedPassword;
        //        Add.MerchantID = MerchantID;
        //        Add.Validity = Validity;
        //        Add.dtLastAccess = LastAccess;
        //        Add.ValidationCode = ValidationCode;
        //        DB.tbl_AdminLogins.InsertOnSubmit(Add);
        //        DB.SubmitChanges();



        //        json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
        //    }
        //    catch
        //    {
        //        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return json;
        //}



        //[WebMethod(EnableSession = true)]
        //public string UpdateRegisteredSupplier(Int64 sid, string sAgencyName, string sEmail, string sFirstName, string sLastName, string sDesignation, string sAddress, string sCity, string nPinCode, string nIATA, string sUsertype, string nPAN, string nPhone, string nMobile, string nFax, string sServiceTaxNumber, string sWebsite, string sGroupId, string sGroupName, string sAgencyLogo, string sRemarks, string hiddenAgentUniqueCode, string hiddenLoginFlag, string sbuttonvalue, string Currency, string FranchiseeId, string GSTNO)
        //{
        //    try
        //    {
        //        GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //        tbl_Contact cont = DB.tbl_Contacts.Single(x => x.ContactID == sid);

        //        cont.phone = nPhone;
        //        cont.Mobile = nMobile;
        //        cont.Fax = nFax;
        //        cont.email = sEmail;
        //        cont.Website = sWebsite;
        //        cont.Address = sAddress;
        //        cont.PinCode = nPinCode;

        //        DB.SubmitChanges();

        //        return "{\"Session\":\"1\",\"retCode\":\"1\"}";
        //    }
        //    catch
        //    {
        //        return jsSerializer.Serialize(new { retCode = 0 });

        //    }
        //}

        [WebMethod(true)]
        public string CheckForUniqueAgent(string sEmail)
        {
            try
            {
                var List1 = (from obj in DB.tbl_AdminLogins where obj.uid == sEmail select obj).ToList();
                var List2 = (from obj in DB.tbl_StaffLogins where obj.uid == sEmail select obj).ToList();

                if (List1.Count != 0 || List2.Count != 0)
                {
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1 });
                }
                else
                {
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 2 });
                }
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;

        }

        #endregion

        #region GetCountry

        [WebMethod(EnableSession = true)]
        public string GetCountry()
        {
            try
            {
                var List = (from obj in DB.tbl_HCities
                            select new
                            {
                                obj.Countryname,
                                obj.Country,
                            }).Distinct().ToList();
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Country = List });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0 });

            }
            return json;
        }

        #endregion

        #region GetCity
        [WebMethod(EnableSession = true)]
        public string GetCity(string country)
        {
            string json;
            try
            {
                var List = (from obj in DB.tbl_HCities
                            where obj.Country == country
                            select new
                            {
                                obj.Code,
                                obj.Description
                            }).Distinct().ToList();
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, City = List });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0 });

            }
            return json;
        }
        #endregion

        #region GetCountryCity
        [WebMethod(EnableSession = true)]
        public string GetCountryCity(string country)
        {
            string json;

            try
            {
                var City = (from obj in DB.tbl_HCities
                            where obj.Countryname == country

                            select new
                {
                    obj.Code,
                    obj.Description

                }).Distinct().ToList();

                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, CityList = City });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0 });

            }
            return json;
        }
        #endregion


        [WebMethod(true)]
        public string MailPassword(string sEmail, string sTo)
        {
            string sJsonString = "{\"retCode\":\"0\"}";
            DataSet dsResult;
            DataTable dtFirstResult;
            DataTable dtSecondResult;
            DBHelper.DBReturnCode retCode = DefaultManager.isAgentValid(sEmail, out dsResult);
            dtFirstResult = dsResult.Tables[0];
            dtSecondResult = dsResult.Tables[1];
            if (retCode == DBHelper.DBReturnCode.SUCCESS && dtFirstResult.Rows.Count > 0)
            {
                string sDecryptedPassword = Cryptography.DecryptText(dtFirstResult.Rows[0]["password"].ToString());
                if (EmailManager.SendEmail(sTo, "Your Password Detail", "Information Received", dtFirstResult.Rows[0]["ContactPerson"].ToString(), sEmail, dtFirstResult.Rows[0]["Mobile"].ToString(), sDecryptedPassword) == true)//info@redhillindia.com
                {
                    sJsonString = "{\"retCode\":\"1\",}";
                }
            }
            if (retCode == DBHelper.DBReturnCode.SUCCESS && dtSecondResult.Rows.Count > 0)
            {
                string sDecryptedPassword = Cryptography.DecryptText(dtSecondResult.Rows[0]["password"].ToString());
                if (EmailManager.SendEmail(sEmail, "Your Password Detail", "Information Received", dtSecondResult.Rows[0]["ContactPerson"].ToString(), sEmail, dtSecondResult.Rows[0]["Mobile"].ToString(), sDecryptedPassword) == true)//info@redhillindia.com
                {
                    sJsonString = "{\"retCode\":\"1\",}";
                }
            }
            return sJsonString;
        }


        private static DataTable ConvertToDatatable<T>(List<T> data)
        {
            PropertyDescriptorCollection props =
            TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    table.Columns.Add(prop.Name, prop.PropertyType.GetGenericArguments()[0]);
                else
                    table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }
    }
}