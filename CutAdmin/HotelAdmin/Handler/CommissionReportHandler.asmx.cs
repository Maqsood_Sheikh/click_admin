﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.BL;
using System.Configuration;
namespace CutAdmin.HotelAdmin.Handler
{
    /// <summary>
    /// Summary description for CommissionReportHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class CommissionReportHandler : System.Web.Services.WebService
    {
        AdminDBHandlerDataContext DB = new AdminDBHandlerDataContext();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        string json = "";
        DBHandlerDataContext db = new DBHandlerDataContext();
        dbTaxHandlerDataContext dbTax = new dbTaxHandlerDataContext();

        [WebMethod(EnableSession = true)]
        public string GetCommissionReport(Int64 Id)
        {
            try
            {
                List<string> ListDetail = new List<string>();

                if(Id!=0)
                {
                    var List = (from obj in DB.Comm_CommissionReports
                            join supp in DB.tbl_AdminLogins on obj.SupplierID equals supp.sid
                            where obj.SupplierID == Id
                            select new
                            {
                                obj.Commission,
                                obj.ID,
                                obj.LastCycle,
                                obj.SupplierID,
                                obj.FullPaid,
                                obj.PartialPaid,
                                obj.UnPaidAmunt,
                                obj.InsertDate,
                                obj.UpdateDate,
                                obj.ParentID,
                                obj.InvoiceID,
                                supp.AgencyName,
                                supp.CurrencyCode

                            }).ToList();

                    if (List.Any())
                    {
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                    }
                    else
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }

                }
                else
                {
                    var List1 = (from obj in DB.Comm_CommissionReports
                            join supp in DB.tbl_AdminLogins on obj.SupplierID equals supp.sid
                            select new
                            {
                                obj.Commission,
                                obj.ID,
                                obj.LastCycle,
                                obj.SupplierID,
                                obj.FullPaid,
                                obj.PartialPaid,
                                obj.UnPaidAmunt,
                                obj.InsertDate,
                                obj.UpdateDate,
                                obj.ParentID,
                                obj.InvoiceID,
                                supp.AgencyName,
                                supp.CurrencyCode

                            }).ToList();

                    if (List1.Any())
                    {
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List1 });
                    }
                    else
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }

                }
               
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetCommissionInvoiceReport(Int64 Id, string Month,string Year)
        {
            try
            {
                Session["CommisionReportList"] = null;
                var arrInvoice = (from obj in dbTax.tbl_Invoices where obj.ParentID == Id select obj).ToList().OrderBy(d => d.ID).ToList();

                var arrReser = (from obj in db.tbl_CommonHotelReservations where obj.ParentID == Id select obj).ToList();

                List<tbl_Invoice> ListDetail = new List<tbl_Invoice>();
                List<tbl_CommonHotelReservation> ListReserve = new List<tbl_CommonHotelReservation>();
                List<CommissionReport> ListReport = new List<CommissionReport>();

                foreach (var item in arrInvoice)
                {
                    string Mnth = item.InvoiceDate.Split('-')[1];
                    string Yer = item.InvoiceDate.Split('-')[2];
                    if (Month == Mnth && Year == Yer)
                    {
                        if (arrReser.Where(d => d.InvoiceID == item.InvoiceNo && d.ParentID == Id).FirstOrDefault() != null)
                        {
                            ListDetail.Add(item);
                            ListReserve.Add(new tbl_CommonHotelReservation
                            {
                                CheckIn = arrReser.Where(d => d.InvoiceID == item.InvoiceNo && d.ParentID == Id).FirstOrDefault().CheckIn,
                                CheckOut = arrReser.Where(d => d.InvoiceID == item.InvoiceNo && d.ParentID == Id).FirstOrDefault().CheckOut,
                                NoOfDays = arrReser.Where(d => d.InvoiceID == item.InvoiceNo && d.ParentID == Id).FirstOrDefault().NoOfDays,
                            });

                            ListReport.Add(new CommissionReport
                            {
                                InvoiceDate = item.InvoiceDate,
                                InvoiceNo = item.InvoiceNo,
                                CheckIn = arrReser.Where(d => d.InvoiceID == item.InvoiceNo && d.ParentID == Id).FirstOrDefault().CheckIn,
                                CheckOut = arrReser.Where(d => d.InvoiceID == item.InvoiceNo && d.ParentID == Id).FirstOrDefault().CheckOut,
                                RoomNight = arrReser.Where(d => d.InvoiceID == item.InvoiceNo && d.ParentID == Id).FirstOrDefault().NoOfDays.ToString(),
                                InvoicAmount = item.InvoiceAmount.ToString(),
                                CommissionAmount = item.Commission.ToString()
                            });
                        }
                      
                    }
                }
                DataTable dtResult = new DataTable();
                CutAdmin.GenralHandler.ListtoDataTable lsttodt = new CutAdmin.GenralHandler.ListtoDataTable();
                dtResult = lsttodt.ToDataTable(ListReport);
                Session["CommisionReportList"] = dtResult;
                dtResult.Dispose();


                if (ListDetail.Any())
                    {
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = ListDetail, ArrReser = ListReserve });
                    }
                    else
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }

                }

           
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateCommission(Int64 ID, Int64 SupplierID, string Commission, decimal UnPaidAmunt, decimal PaidAmunt)
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            var Name = objGlobalDefault.ContactPerson;
            DateTime Date = DateTime.Now;
            try
            {
                Comm_CommissionReport Comm = DB.Comm_CommissionReports.Single(x => x.ID == ID);
                Comm.UnPaidAmunt = UnPaidAmunt;
                Comm.UpdateBy = Name;
                Comm.History = Convert.ToString(Date) + " - " + PaidAmunt;
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        //[WebMethod(EnableSession = true)]
        //public string GetCommissionStatement(Int64 Id,string Year)
        //{
        //    try
        //    {
        //        List<tbl_Invoice> ListDetail = new List<tbl_Invoice>();

        //        var arrInvoice = (from obj in dbTax.tbl_Invoices where obj.AgentID == Id && obj.ParentID==232  && obj.InvoiceType == "Hotel Commission" select obj).ToList().OrderBy(d => d.ID).ToList();

        //            foreach (var item in arrInvoice)
        //            {
        //                string Yr = item.InvoiceDate.Split('-')[2];
        //                if (Year == Yr)
        //                {
        //                    ListDetail.Add(item);
        //                }
        //            }

        //            if (ListDetail.Any())
        //            {
        //                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = ListDetail });
        //            }
        //            else
        //            {
        //                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //            }
                
        //    }
        //    catch (Exception)
        //    {
        //        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return json;
        //}

        [WebMethod(EnableSession = true)]
        public string GetCommissionStatement(Int64 Id, string Month, string Year)
        {
            try
            {
                List<tbl_Invoice> ListDetail = new List<tbl_Invoice>();

                var arrInvoice = (from obj in dbTax.tbl_Invoices where obj.AgentID == Id && obj.ParentID == Convert.ToInt64(ConfigurationManager.AppSettings["AdminKey"]) && obj.InvoiceType == "Hotel Commission" select obj).ToList().OrderBy(d => d.ID).ToList();

                foreach (var item in arrInvoice)
                {
                    string Mnth = item.InvoiceDate.Split('-')[1];
                    string Yr = item.InvoiceDate.Split('-')[2];
                    if (Year == Yr && Month == Mnth)
                    {
                        ListDetail.Add(item);
                    }
                }

                if (ListDetail.Any())
                {
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = ListDetail });
                }
                else
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }

            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetDetailsCommission(Int64 Id)
        {
            try
            {
                var dtInvoiceDetails = (from obj in dbTax.tbl_Invoices where obj.CycleId == Id && obj.InvoiceType != "Hotel Commission" select obj).ToList();
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = dtInvoiceDetails });
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        public class CommissionReport
        {
            public string InvoiceDate { get; set; }
            public string InvoiceNo { get; set; }
            public string CheckIn { get; set; }
            public string CheckOut { get; set; }

            public string RoomNight { get; set; }
            public string InvoicAmount { get; set; }
            public string CommissionAmount { get; set; }
        }
    }
}
