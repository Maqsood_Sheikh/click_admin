﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.HotelAdmin.Handler
{
    /// <summary>
    /// Summary description for DashBoard
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class DashBoard : System.Web.Services.WebService
    {
        AdminDBHandlerDataContext DB = new AdminDBHandlerDataContext();
        DBHandlerDataContext db = new DBHandlerDataContext();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        string json = "";

        [WebMethod(EnableSession = true)]
        public string GetCount()
        {
            try
            {
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired ,Please Login and try Again");

                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 Uid = AccountManager.GetUserByLogin();
                //if (objGlobalDefault.UserType != "Supplier")
                //   Uid = objGlobalDefault.ParentId;
                var CommissionReports = (from obj in DB.Comm_CommissionReports select obj).Select(x => x.SupplierID).Distinct().ToList().Count;
                var SupplierList = (from obj in DB.tbl_AdminLogins where obj.ParentID == Uid && obj.UserType == "Supplier" select obj).ToList().Count;
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, CommissionReports = CommissionReports, Supplier = SupplierList });
            }
            catch (Exception ex)
            {
                json = jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetHotelUnApproved()
        {
            try
            {               
                var HotelListCount = (from obj in db.tbl_CommonHotelMasters where obj.Approved == false select obj).ToList().Count;
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, HotelListCount = HotelListCount});
            }
            catch (Exception ex)
            {
                json = jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetRoomUnApproved()
        {
            try
            {
                var RoomListCount = (from obj in db.tbl_commonRoomDetails where obj.Approved == false select obj).ToList().Count;
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, RoomListCount = RoomListCount });
            }
            catch (Exception ex)
            {
                json = jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetHotelsData()
        {
            try
            {
                var HotelList = (from obj in db.tbl_CommonHotelMasters
                                 join AgName in db.tbl_AdminLogins on obj.ParentID equals AgName.sid
                                 where obj.Approved == false
                                 select new
                                     {
                                         obj.HotelName,
                                         obj.sid,
                                         obj.Approved,
                                         AgName.AgencyName,
                                         obj.ParentID
                                     }).ToList();
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, HotelList = HotelList });
            }
            catch (Exception ex)
            {
                json = jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetRoomsData()
        {
            try
            {
                var RoomList = (from obj in db.tbl_commonRoomDetails
                                join AgName in db.tbl_AdminLogins on obj.CreatedBy equals AgName.sid
                                join Room in db.tbl_commonRoomTypes on obj.RoomTypeId equals Room.RoomTypeID
                                join Hotel in db.tbl_CommonHotelMasters on obj.HotelId equals Hotel.sid
                                where obj.Approved == false
                                select new
                                {
                                    obj.RoomId,
                                    obj.RoomTypeId,
                                    obj.Approved,
                                    AgName.AgencyName,
                                    obj.CreatedBy,
                                    Room.RoomType,
                                    Hotel.HotelName,
                                    Hotel.sid
                                }).ToList();

                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, RoomList = RoomList });
            }
            catch (Exception ex)
            {
                json = jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string Unapproved(Int32 HotelCode)
        {
            try
            {
                tbl_CommonHotelMaster Delete = db.tbl_CommonHotelMasters.Single(x => x.sid == HotelCode);
                db.tbl_CommonHotelMasters.DeleteOnSubmit(Delete);
                db.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";

            }
            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string UnapprovedRoom(Int32 RoomId)
        {
            try
            {
                tbl_commonRoomDetail Delete = db.tbl_commonRoomDetails.Single(x => x.RoomId == RoomId);
                db.tbl_commonRoomDetails.DeleteOnSubmit(Delete);
                db.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";

            }
            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }
    }
}
