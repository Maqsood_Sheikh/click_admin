﻿using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.HotelAdmin.Handler
{
    /// <summary>
    /// Summary description for AgencyStatementHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class AgencyStatementHandler : System.Web.Services.WebService
    {
        AdminDBHandlerDataContext DB = new AdminDBHandlerDataContext();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        string json = "";

        [WebMethod(EnableSession = true)]
        public string GetAgencyStatementByAgent(Int64 uid)
        {
            try
            {
                var List = (from obj in DB.tbl_Transactions where obj.uid == uid orderby obj.sid descending select obj).ToList();
                if (List.Any())
                {
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, AgentStatement = List });
                }
                else
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                
            }
            catch (Exception)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
            
        }

        [WebMethod(EnableSession = true)]
        public string GetAllStatement()
        {
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 uid = objGlobalDefaults.sid;
            try
            {
                var List = (from obj in DB.tbl_Transactions
                            join agency in DB.tbl_AdminLogins on obj.uid equals agency.sid
                            where obj.Particulars.Contains("Fix") == false && obj.Particulars.Contains("One") == false && obj.Particulars.Contains("Credit Limit Reverse") == false && agency.ParentID==uid
                            orderby obj.sid descending
                            select new
                            {
                                obj.uid,
                                obj.ReferenceNo,
                                obj.Particulars,
                                obj.Balance,
                                obj.TransactionDate,
                                obj.DebitedAmount,
                                obj.CreditedAmount,
                                agency.AgencyName,
                                agency.CurrencyCode

                            }).ToList();
                jsSerializer.MaxJsonLength = int.MaxValue;
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                Session["PaggingSession"] = ConvertToDatatable(List);
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string FilterAgencyDetails(string dFrom, string dTo, string TransType, string RefNo)
        {
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 uid = objGlobalDefaults.sid;
            try
            {
                var List = (from obj in DB.tbl_Transactions
                            join agency in DB.tbl_AdminLogins on obj.uid equals agency.sid
                            where obj.Particulars.Contains("Fix") == false && obj.Particulars.Contains("One") == false && obj.Particulars.Contains("Credit Limit Reverse") == false && agency.ParentID == uid
                            orderby obj.sid descending
                            select new
                            {
                                obj.uid,
                                obj.ReferenceNo,
                                obj.Particulars,
                                obj.Balance,
                                obj.TransactionDate,
                                obj.DebitedAmount,
                                obj.CreditedAmount,
                                agency.AgencyName,
                                agency.CurrencyCode

                            }).ToList();


                if (TransType != "")
                {
                    if (TransType == "Both")
                    {
                        List = List.Where(s => s.DebitedAmount != null && s.CreditedAmount != null).ToList();
                    }
                    else if (TransType == "Credited")
                    {
                        List = List.Where(s => s.CreditedAmount != null && s.DebitedAmount == null).ToList();
                    }

                    else if (TransType == "Debited")
                    {
                        List = List.Where(s => s.DebitedAmount != null && s.CreditedAmount == null).ToList();
                    }

                }


                if (dFrom != "" && dTo != "")
                {
                    DateTime From = ConvertDateTime(dFrom);
                    DateTime To = ConvertDateTime(dTo);

                    DateTime Today = DateTime.Today;
                    string TodayDate = Today.ToString("dd-MM-yyyy");
                    DateTime todays = Convert.ToDateTime(TodayDate);

                    List = List.Where(s => ConvertDateTime(s.TransactionDate) >= From && ConvertDateTime(s.TransactionDate) <= To).ToList();
                }

                if (RefNo != "")
                {
                    List = List.Where(s => s.ReferenceNo == RefNo).ToList();
                }

                Session["PaggingSearch"] = ConvertToDatatable(List);
                if (List.Count > 0)
                {
                    jsSerializer.MaxJsonLength = int.MaxValue;
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                }
                else
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        private static DataTable ConvertToDatatable<T>(List<T> data)
        {
            PropertyDescriptorCollection props =
            TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    table.Columns.Add(prop.Name, prop.PropertyType.GetGenericArguments()[0]);
                else
                    table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }

        public static DateTime ConvertDateTime(string Date)
        {
            DateTime date = new DateTime();
            try
            {
                string CurrentPattern = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
                string[] Split = new string[] { "-", "/", @"\", "." };
                string[] Patternvalue = CurrentPattern.Split(Split, StringSplitOptions.None);
                string[] DateSplit = Date.Split(Split, StringSplitOptions.None);
                string NewDate = "";
                if (Patternvalue[0].ToLower().Contains("d") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[0] + "/" + DateSplit[1] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("m") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[0] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("d") == true)
                {
                    NewDate = DateSplit[2] + "/" + DateSplit[1] + "/" + DateSplit[0];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("m") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[2] + "/" + DateSplit[0];
                }
                date = DateTime.Parse(NewDate, Thread.CurrentThread.CurrentCulture);
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

            return date;

        }
    }
}
