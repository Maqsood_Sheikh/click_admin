﻿var Nex = 0;
var Pre = 10;
var Limit = 0;
var SearchBit = false;
$(function () {
    GetAgencyStatementAll();
})

function numberWithCommas(x) {
    // x = "-0.50";
    //x = x.toString();
    //var afterPoint = '';
    //if (x.indexOf('.') > 0)
    //    afterPoint = x.substring(x.indexOf('.'), x.length);
    //x = Math.floor(x);
    //x = x.toString();
    //var lastThree = x.substring(x.length - 3);
    //var otherNumbers = x.substring(0, x.length - 3);
    //if (otherNumbers != '')
    //    lastThree = ',' + lastThree;
    //var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
    //return res;
    // get stuff before the dot
    var d = x.indexOf('.');
    var s2 = d === -1 ? x : x.slice(0, d);

    // insert commas every 3 digits from the right
    for (var i = s2.length - 3; i > 0; i -= 3)
        s2 = s2.slice(0, i) + ',' + s2.slice(i);

    // append fractional part
    if (d !== -1)
        s2 += x.slice(d);

    return s2;

}

function GetAgencyStatementAll() {
    debugger;
    SearchBit = false;
    $.ajax({
        type: "POST",
        url: "handler/AgencyStatementHandler.asmx/GetAllStatement",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var ul = "";
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var Arr = result.Arr;

                for (var i = 0; i < Arr.length; i++) {

                    if ((Arr[i].ReferenceNo) == "" || (Arr[i].ReferenceNo) == null) {
                        Ref = "-";
                    }
                    else {
                        Ref = Arr[i].ReferenceNo;
                    }

                    if ((Arr[i].CreditedAmount) == "" || (Arr[i].CreditedAmount) == null) {
                        CreditedAmount = "-";
                    }
                    else {
                        CreditedAmount = Arr[i].CreditedAmount;
                    }

                    if ((Arr[i].DebitedAmount) == "" || (Arr[i].DebitedAmount) == null) {
                        DebitedAmount = "-";
                    }
                    else {
                        DebitedAmount = Arr[i].DebitedAmount;
                    }
                    ul += '<tr>'
                    ul += '<td align="center" style="width:5%">' + parseInt(i + 1) + '</td>'
                    ul += '<td align="center" style="width:15%">' + Arr[i].TransactionDate + '</td>'
                    ul += '<td align="center" style="width:15%">' + Ref + '</td>'
                    ul += '<td align="center" style="width:25%">' + Arr[i].Particulars + '</td>'
                    ul += '<td align="center" style="width:10%">' + CreditedAmount + '</td>'
                    ul += '<td align="center" style="width:10%">' + DebitedAmount + '</td>'
                    ul += '</tr>'
                }
                $("#tbl_AgencyStatement tbody").html(ul);
                $("#tbl_AgencyStatement").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }
            else {
                $("#tbl_AgencyStatement").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }

        }
    });
}

function GetAgencyStatementByDate() {
    $("#tbl_AgencyStatement").dataTable().fnClearTable();
    $("#tbl_AgencyStatement").dataTable().fnDestroy();
    debugger;
    var dateFrom = $("#datepicker3").val();
    var dateTo = $("#datepicker4").val();
    var RefNo = $("#txt_Reference").val();
    var TransType = $("#TransactionType option:selected").val();

    var data = {
        dFrom: dateFrom,
        dTo: dateTo,
        RefNo: RefNo,
        TransType: TransType,
    }
    $.ajax({
        type: "POST",
        url: "handler/AgencyStatementHandler.asmx/FilterAgencyDetails",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var ul = "";
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {

                var Arr = result.Arr;

                for (var i = 0; i < Arr.length; i++) {

                    if ((Arr[i].ReferenceNo) == "" || (Arr[i].ReferenceNo) == null) {
                        Ref = "-";
                    }
                    else {
                        Ref = Arr[i].ReferenceNo;
                    }

                    if ((Arr[i].CreditedAmount) == "" || (Arr[i].CreditedAmount) == null) {
                        CreditedAmount = "-";
                    }
                    else {
                        CreditedAmount = Arr[i].CreditedAmount;
                    }

                    if ((Arr[i].DebitedAmount) == "" || (Arr[i].DebitedAmount) == null) {
                        DebitedAmount = "-";
                    }
                    else {
                        DebitedAmount = Arr[i].DebitedAmount;
                    }
                    ul += '<tr>'
                    ul += '<td align="center" style="width:5%">' + parseInt(i + 1) + '</td>'
                    ul += '<td align="center" style="width:15%">' + Arr[i].TransactionDate + '</td>'
                    ul += '<td align="center" style="width:15%">' + Ref + '</td>'
                    ul += '<td align="center" style="width:25%">' + Arr[i].Particulars + '</td>'
                    ul += '<td align="center" style="width:10%">' + CreditedAmount + '</td>'
                    ul += '<td align="center" style="width:10%">' + DebitedAmount + '</td>'
                    ul += '</tr>'

                }
                $("#tbl_AgencyStatement tbody").append(ul);
                $("#tbl_AgencyStatement").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }
            else {
                $("#tbl_AgencyStatement").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }

        }
    });
}

function ExportAgencyStatement(Document) {
    debugger;
    var Type = $("#TransactionType option:selected").val();
    var RefNo = $('#txt_Reference').val();
    var From = $('#datepicker3').val();
    var To = $('#datepicker4').val();

    if (Type == "Both" && From == "" && To == "" && RefNo == "") {
        window.location.href = "Handler/ExportToExcelHandler.ashx?datatable=AgencyStatement&Type=All&Document=" + Document;
    }
    else {
        window.location.href = "andler/ExportToExcelHandler.ashx?datatable=AgencyStatement&Type=Search&Document=" + Document;
    }
}