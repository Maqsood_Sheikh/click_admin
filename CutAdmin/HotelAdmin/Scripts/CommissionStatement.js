﻿
$(document).ready(function () {
    GetAgentDetail();

});

function GetAgentDetail() {
    $("#tbl_SupplierDetails").dataTable().fnClearTable();
    $("#tbl_SupplierDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "handler/UserHanler.asmx/GetAgentDetail",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                List_AgentDetails = result.List_Agent;
                var div = "";
                for (var i = 0; i < List_AgentDetails.length; i++) {
                    div += '<option value="' + List_AgentDetails[i].sid + '">' + List_AgentDetails[i].AgencyName + '</option>'
                }

                $("#selSupplier").append(div);
                $("#tbl_SupplierDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                $('#tbl_SupplierDetails').removeAttr("style");
            }
            else if (result.retCode == 0) {
                $("#tbl_SupplierDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                $('#tbl_SupplierDetails').removeAttr("style");
            }
        },
        error: function () {
        }

    });
}

//function Search() {
//    var Id = $("#selSupplier option:selected").val();
//    if (Id == "" || Id == "-") {
//        Success("Please Select Supplier.");
//        return false;
//    }

//    var Year = $("#selMonth option:selected").val();
//    if (Year == "" || Year == "-") {
//        Success("Please Select Year.");
//        return false;
//    }

//    $("#tbl_SupplierDetails").dataTable().fnClearTable();
//    $("#tbl_SupplierDetails").dataTable().fnDestroy();
//    $.ajax({
//        type: "POST",
//        url: "handler/CommissionReportHandler.asmx/GetCommissionStatement",
//        data: JSON.stringify({ Id: Id, Year: Year }),
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        success: function (response) {
//            var result = JSON.parse(response.d)
//            $("#UnPaidAmt").empty();
//            if (result.retCode == 1) {
//                var List_Details = result.Arr;
//                var div = "";
//                var Amount = 0;
//                var Currency = ""; var status = "";
//                for (var i = 0; i < List_Details.length; i++)
//                {
//                    if (List_Details[i].Status == 1)
//                        status = "UnPaid";
//                    else
//                        status = "Paid";
//                    Amount += List_Details[i].InvoiceAmount;
//                    Currency = List_Details[i].Currency;
//                    div += '<tr>';
//                    div += '<td class="align-center hide-on-mobile">' + List_Details[i].InvoiceNo + '</td>';
//                    div += '<td class="align-center hide-on-mobile">' + List_Details[i].InvoiceDate + ' <a href="#" style="font-size:10px" title="View Report" onclick="StatementModal(\'' + List_Details[i].CycleId + '\')">(View Report)</a></td>';
//                    div += '<td class="align-center hide-on-mobile">' + List_Details[i].Particulars + '</td>';
//                    div += '<td class="align-center hide-on-mobile">' + List_Details[i].Currency + "  " + List_Details[i].InvoiceAmount + '</td>';
//                    div += '<td class="align-center hide-on-mobile">' + status + '</td>';
//                    div += '<td class="align-center hide-on-mobile"><a href="#" class="button" title="View" onclick="GetPrintInvoice(\'' + List_Details[i].CycleId + '\',\'' + List_Details[i].InvoiceNo + '\',\'' + List_Details[i].Status + '\')">View</a> </td>';
//                }
//                var AmountCurrency = Currency + " " + Amount;
//                $("#UnPaidAmt").text(AmountCurrency);
//                $("#tbl_SupplierDetails").append(div);
//                $("#tbl_SupplierDetails").dataTable({
//                    bSort: false, sPaginationType: 'full_numbers',
//                });
//                $('#tbl_SupplierDetails').removeAttr("style");
//            }
//            else if (result.retCode == 0) {
//                $("#tbl_SupplierDetails").dataTable({
//                    bSort: false, sPaginationType: 'full_numbers',
//                });
//                $('#tbl_SupplierDetails').removeAttr("style");
//            }
//        },
//        error: function () {
//        }

//    });
//}

function Search() {
    var Id = $("#selSupplier option:selected").val();
    if (Id == "" || Id == "-") {
        Success("Please Select Supplier.");
        return false;
    }

    var Month = $("#selMonth option:selected").val();
    if (Month == "" || Month == "-") {
        Success("Please Select Month.");
        return false;
    }

    var Year = $("#selYear option:selected").val();
    if (Year == "" || Year == "-") {
        Success("Please Select Year.");
        return false;
    }

    $("#tbl_SupplierDetails").dataTable().fnClearTable();
    $("#tbl_SupplierDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "handler/CommissionReportHandler.asmx/GetCommissionStatement",
        data: JSON.stringify({ Id: Id, Month: Month, Year: Year }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            $("#UnPaidAmt").empty();
            if (result.retCode == 1) {
                var List_Details = result.Arr;
                var div = "";
                var Amount = 0;
                var Currency = ""; var status = "";
                for (var i = 0; i < List_Details.length; i++) {
                    if (List_Details[i].Status == 1)
                        status = "UnPaid";
                    else
                        status = "Paid";
                    Amount += List_Details[i].InvoiceAmount;
                    Currency = List_Details[i].Currency;
                    div += '<tr>';
                    div += '<td class="align-center hide-on-mobile">' + List_Details[i].InvoiceNo + '</td>';
                    div += '<td class="align-center hide-on-mobile">' + List_Details[i].InvoiceDate + ' <a href="#" style="font-size:10px" title="View Report" onclick="StatementModal(\'' + List_Details[i].CycleId + '\')">(View Report)</a></td>';
                    div += '<td class="align-center hide-on-mobile">' + List_Details[i].Particulars + '</td>';
                    div += '<td class="align-center hide-on-mobile">' + List_Details[i].Currency + "  " + List_Details[i].InvoiceAmount + '</td>';
                    div += '<td class="align-center hide-on-mobile">' + status + '</td>';
                    div += '<td class="align-center hide-on-mobile"><a href="#" class="button" title="View" onclick="GetPrintInvoice(\'' + List_Details[i].CycleId + '\',\'' + List_Details[i].InvoiceNo + '\',\'' + List_Details[i].Status + '\')">View</a> </td>';
                }
                var AmountCurrency = Currency + " " + Amount;
                $("#UnPaidAmt").text(AmountCurrency);
                $("#tbl_SupplierDetails").append(div);
                $("#tbl_SupplierDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                $('#tbl_SupplierDetails').removeAttr("style");
            }
            else if (result.retCode == 0) {
                $("#tbl_SupplierDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                $('#tbl_SupplierDetails').removeAttr("style");
            }
        },
        error: function () {
        }

    });
}

var UnPaidAmount = 0;

function UpdateModal(ID, SupplierID, Commission, UnPaidAmunt) {
    UnPaidAmount = UnPaidAmunt;
    $.modal({
        content: '<div class="modal-body">' +
          '<div class="scrollingDiv">' +
          '<div class="columns">' +
          '<div class="twelve-columns three-columns-mobile">' +
          '<label>Commission Amount</label>' +
          '<div class="input full-width">' +
          '<input class="input-unstyled full-width" value="' + Commission + '" readonly="readonly" type="text" id="txt_CommissionAmount">' +
          '</div>' +
          '</div>' +
          '<div class="twelve-columns three-columns-mobile">' +
          '<label>Un-Paid Amount</label>' +
          '<div class="input full-width">' +
          '<input  class="input-unstyled full-width" value="' + UnPaidAmunt + '" readonly="readonly"  type="text" id="txt_Un-PaidAmount">' +
          '</div>' +
          '</div>' +
          '<div class="twelve-columns three-columns-mobile">' +
          '<label>Paid Amount</label><span class="red" id="lbl_PaidAmount">*</span>' +
          '<div class="input full-width">' +
          '<input value="" class="input-unstyled full-width" type="text" id="txt_PaidAmount" onkeyup="Calculate()">' +
          '</div>' +
          '</div>' +
          '</div></div>' +
          '<p class="text-alignright"><a style="cursor:pointer" href="#" class="button compact anthracite-gradient editpro" onclick="UpdateCommission(\'' + ID + '\',\'' + SupplierID + '\',\'' + Commission + '\',\'' + UnPaidAmount + '\')">Save</a></p>' +
          '</div>',
        //onclick="UpdateCommission(\'' + SupplierID + '\',\'' + Commission + '\',\'' + UnPaidAmunt + '\')"
        title: 'Update Commission',
        width: 300,
        scrolling: true,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'anthracite-gradient glossy displayNone',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
}

function UpdateCommission(ID, SupplierID, Commission) {
    var PaidAmunt = $("#txt_PaidAmount").val();
    var UnPaidAmunt = $("#txt_Un-PaidAmount").val();
    var data = {
        ID: ID,
        SupplierID: SupplierID,
        Commission: Commission,
        UnPaidAmunt: UnPaidAmunt,
        PaidAmunt: PaidAmunt
    }
    $.ajax({
        type: "POST",
        url: "handler/CommissionReportHandler.asmx/UpdateCommission",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                Success("Updated Successfully");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            }
            else {
                Success("Something Went Wrong");
                window.location.reload();
            }
        }
    });

}

function Calculate() {
    var reg = new RegExp('[0-9]$');
    if ($('#txt_PaidAmount').val() != "") {
        if (!(reg.test($('#txt_PaidAmount').val()))) {
            Success("Only numeric values is allowed");
            return false;
        }
        var PaidAmount = parseInt($('#txt_PaidAmount').val(), 10);
    }
    if (PaidAmount != undefined && PaidAmount <= UnPaidAmount) {
        var Total = PaidAmount - UnPaidAmount;
        $("#txt_Un-PaidAmount").val(Math.abs(Total));
    }
    else if (PaidAmount != undefined && PaidAmount >= UnPaidAmount) {
        Success("Paid Amount Should Not be greater than UnPaid Amount");
        $('#txt_PaidAmount').val("");
        $("#txt_Un-PaidAmount").val(UnPaidAmount);
    }
    else {
        $("#txt_Un-PaidAmount").val(UnPaidAmount);
    }

}

function StatementModal(Id) {

    var div = "";
    var data = { Id: Id }
    $.ajax({
        type: "POST",
        url: "Handler/CommissionReportHandler.asmx/GetDetailsCommission",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                var List_Details = obj.Arr;
                var Amount = 0;
                var Currency = "";


                for (var i = 0; i < List_Details.length; i++) {
                    Amount += List_Details[i].Commission;
                    Currency = List_Details[i].Currency;
                    div += '<tr>';
                    div += '<td class="align-center hide-on-mobile">' + (i + 1) + '</td>';
                    div += '<td class="align-center hide-on-mobile">' + List_Details[i].InvoiceDate + '</td>';
                    div += '<td class="align-center hide-on-mobile" title="View Details" style="cursor:pointer" onclick="GetDetails(\'' + List_Details[i].InvoiceNo + '\')">' + List_Details[i].InvoiceNo + '</td>';
                    div += '<td class="align-center hide-on-mobile">' + Currency + " " + List_Details[i].InvoiceAmount + '</td>';
                    div += '<td class="align-center hide-on-mobile">' + List_Details[i].Commission + '</td>';
                    div += '</tr>';
                }
                $.modal({
                    content: '  <div class="with-padding">' +
                        '<div class="respTable">' +
                        '    <table class="table responsive-table" id="tbl_CommInvoiceDetails">' +
                        '        <thead>' +
                        '            <tr>' +
                        '                <th scope="col" class="align-center hide-on-mobile">Sr No.</th>' +
                        '                <th scope="col" class="align-center hide-on-mobile">Invoice Date</th>' +
                        '                <th scope="col" class="align-center hide-on-mobile">Invoice No </th>' +
                        '                <th scope="col" class="align-center hide-on-mobile-portrait">Invoice Amount</th>' +
                        '                <th scope="col" class="align-center hide-on-mobile-portrait">Commision Amount</th>' +
                        '            </tr>' +
                        '        </thead>' +
                         div +
                        '    </table>' +
                        '</div>' +

                        ' <br><br><div class="columns">' +
                        ' <div class="twelve-columns ">' +
                        '     <label><b>Total Amount :  </b> </label>' +
                           Currency + "  " + Amount +
                        ' </div>' +
                        '</div>' +
                         '</div>' +
                        '',
                    title: 'Commission  Details',
                    width: 600,
                    scrolling: true,
                    actions: {
                        'Close': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttons: {
                        'Close': {
                            classes: 'anthracite-gradient glossy displayNone',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttonsLowPadding: true
                });
            }
            else {
                Success("Something Went Wrong");
                window.location.reload();
            }
        }
    });





}

function GetPrintInvoice(CycleId, ReservationID, Status) {
    var Type = CycleId;
    var Uid = $("#selSupplier").val();
    var win = window.open('ViewCommissionInvoice.aspx?ReservationId=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status + '&Supplier=' + Type, '_blank');

}

function GetDetails(ReservationID) {

    var Res = ReservationID.split("-")[1];

    var data = {
        ReservationID: Res
    }
    $.ajax({
        type: "POST",
        url: "../handler/BookingHandler.asmx/GetDetail",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var Cancle = "";
            var Cancleamnt = "";
            var Policy = "";
            if (result.retCode == 1) {
                try {
                    var Detail = result.Detail;

                    $.modal({
                        content:

                      '<div class="modal-body">' +
                      '' +
                      '<table class="table table-hover table-responsive" id="tbl_Confirmation" style="width: 90%">' +
                      '<tr>' +
                      '<td>' +
                      '<span class="text-left">Hotel:&nbsp;&nbsp;<b>' + Detail[0].HotelName + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                       '<td>' +
                      '<span class="text-left">CheckIn:&nbsp;&nbsp;<b>' + Detail[0].CheckIn + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                       '<td>' +
                      '<span class="text-left">CheckOut:&nbsp;&nbsp;<b>' + Detail[0].CheckOut + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                      '</tr>' +
                      '<tr>' +
                      '<td>' +
                      '<span class="text-left">Passenger: &nbsp;&nbsp;<b>' + Detail[0].Name + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                       '<td>' +
                      '<span class="text-left">Location:&nbsp;&nbsp;<b> ' + Detail[0].City + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                       '<td>' +
                      '<span class="text-left">Nights:&nbsp;&nbsp; <b>' + Detail[0].NoOfDays + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                      '</tr>' +
                      '<tr>' +
                      '<td>' +
                      '<span class="text-left">Booking Id:&nbsp;&nbsp; <b>' + Detail[0].ReservationID + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                       '<td>' +
                      '<span class="text-left">Booiking Date:&nbsp;&nbsp;<b> ' + Detail[0].ReservationDate + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                       '<td>' +
                      '<span class="text-left">Amount:&nbsp;&nbsp;<b>' + Detail[0].TotalFare + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                      '</tr>' +
                      '</table>' +



                       '<br/>' +
                      '</div>',
                        title: 'Booking Details',
                        width: 600,
                        height: 200,
                        scrolling: true,
                        actions: {
                            'Close': {
                                color: 'red',
                                click: function (win) { win.closeModal(); }
                            }
                        },
                        buttons: {
                            'Close': {
                                classes: 'huge anthracite-gradient displayNone',
                                click: function (win) { win.closeModal(); }
                            }
                        },
                        buttonsLowPadding: true

                    });
                }
                catch (ex) {

                }


            }
            else if (result.retCode == 0) {
                $('#SpnMessege').text('Something Went Wrong');
                $('#ModelMessege').modal('show')
                // alert("error occured while getting cancellation details")
            }
        },
        error: function (xhr, status, error) {
            $('#SpnMessege').text("Getting Error");
            $('#ModelMessege').modal('show')
            // alert("Error on cancellation popup:" + " " + xhr.readyState + " " + xhr.status);
        }
    });
}