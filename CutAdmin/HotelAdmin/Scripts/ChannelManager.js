﻿
var Name;
var List_Supplier;
var ChannelList;
$(function () {
    $("#spName").text(GetQueryStringParams('Name').replace(/%20/g, ' '))
    Name = GetQueryStringParams('Name').replace(/%20/g, ' ');
    Uid = GetQueryStringParams('sid').replace(/%20/g, ' ');
    GetSupplier(Uid);
   
})

function GetSupplier(Uid)
{
    var data = { Uid: Uid }
    $.ajax({
        type: "POST",
        url: "handler/ChannelHandler.asmx/GetSupplier",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            // var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                List_Supplier = result.List_Supplier;
                ChannelList = result.ChannelList;
                htmlGenrator();
            }
            else if (result.retCode == 0) {
                $("#tbl_Channels tbody").remove();
                var tRow = '<tbody>';
                //tRow += '<tr><td align="center" style="padding-top: 2%" colspan="4"><span><b>No record found</b></span></td></tr>';
                tRow += '<tr> <td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td></tr>';
                tRow += '</tbody>';
                $("#tbl_Channels").append(tRow);
 
            }
        },
        error: function () {
            Success("An error occured while loading details.")
        }
    });
}

function htmlGenrator() {
    var tRow = "";
   
    for (var i = 0; i < List_Supplier.length; i++) {
        if (List_Supplier[i].AgencyName != Name)
        {
            tRow += '<tr class="Supplier_Details" style="text-align:center">'
            tRow += '<td style="width:5%">' + (i + 1) + '</td>';
            tRow += '<td style="width:25%">' + List_Supplier[i].AgencyName.toUpperCase() + '<input type="hidden" class="SupplierID" value="' + List_Supplier[i].sid + '"</td>';
            for (var j = 0; j < ChannelList.length; j++)
            {
                
                   if (ChannelList[j].SupplierID == List_Supplier[i].sid)
                    {
                        tRow += '<td class="align-center"><input type="checkbox" id="chk_On' + List_Supplier[i].sid + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1" checked onclick="Activate(\'' + List_Supplier[i].sid + '\',\'' + List_Supplier[i].AgencyName + '\')"></td>';
                        break;
                    }
                        
                   if (j == ChannelList.length-1)
                    {
                        tRow += '<td class="align-center"><input type="checkbox" id="chk_On' + List_Supplier[i].sid + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1" onclick="Activate(\'' + List_Supplier[i].sid + '\',\'' + List_Supplier[i].AgencyName + '\')"></td>';
                      
                    }
                     
          
            }
            if (ChannelList.length == null || ChannelList.length == 0)
            {
                tRow += '<td class="align-center"><input type="checkbox" id="chk_On' + List_Supplier[i].sid + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1" onclick="Activate(\'' + List_Supplier[i].sid + '\',\'' + List_Supplier[i].AgencyName + '\')"></td>';
            }

            tRow += '</tr>';
        }
   
    }            
    
    $("#tbl_Channels tbody").append(tRow);

    $("#tbl_Channels").dataTable({
        bSort: false, sPaginationType: 'full_numbers',
    });
}


function SaveChannels()
{
    var arrSupplierMap = new Array();
    var arrSupplier = $(".Supplier_Details");
    for (var i = 0; i < arrSupplier.length; i++) {
        if ($(arrSupplier[i]).find(".checked").length != 0) {
            arrSupplierMap.push({
                SupplierID: $($(arrSupplier[i]).find(".SupplierID")).val(),
                UserID: GetQueryStringParams('sid').replace(/%20/g, ' '),
            })
        }
    }
    var data =
        {
            arrSupplierMap: arrSupplierMap
        }
    $.ajax({
                type: "POST",
                url: "handler/ChannelHandler.asmx/SaveChannels",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.retCode == 1) {
                        Success("Channel Assign Successfully");
                       
                    }
                    else {
                        Success(" Error");
                    }
                },

            });
}
