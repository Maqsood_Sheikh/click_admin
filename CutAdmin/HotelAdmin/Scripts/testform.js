﻿$(function () {
    LoadBankDetails();
});

function LoadBankDetails() {
    //$("#tbl_BankDetails").dataTable().fnClearTable();
    //$("#tbl_BankDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "Handler/GenralHandler.asmx/GetTestformBankDetails",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.session == 0 && result.retCode == 0) {
                alert("Your session has been expired!")
                window.location.href = "../Default.aspx";
            }
            else if (result.retCode == 1) {
               
                $('#tblbankdetails').html(result.BankDetails);
                //$("#tblbankdetails").dataTable({
                //    bSort: false, sPaginationType: 'full_numbers',
                //});
            }
        },
        error: function () {
            alert("An error occured while loading details.");
        }
    });
    //$('#imgserchload').show();
    //$.post("Handler/GenralHandler.asmx/GetTestformBankDetails", {
    //},
	//function (data, status) {

	//    $('#tblbankdetails').html(data);
	//    $('#imgserchload').hide();
	//}).fail(function (xhr, textStatus, errorThrown) {S
	//    //alert(xhr.responseText);	    
	//    $('#divsearcherr').show();
	//    $('#divsearcherr').html(xhr.responseText);
	//    $('#imgserchload').hide();
	//});
}