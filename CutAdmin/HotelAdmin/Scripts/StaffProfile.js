﻿$(document).ready(function () {
    GetStaffDetails();
});

var sMiddleName = '';
var hiddenLoginFlag;
var sPhone = "";
var sAddress = '';
var hiddenID;
var sCode;
var sPinCode;

function GetStaffDetails() {
    $.ajax({
        type: "POST",
        url: "Handler/GenralHandler.asmx/StaffDetails",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            debugger;
            Arr = obj.Arr;
            if (obj.retCode == 1) {
                hiddenLoginFlag = Arr[0].LoginFlag;
                sPhone = Arr[0].phone;
                sAddress = Arr[0].Address;
                hiddenID = Arr[0].sid;
                var fullnamesplit = Arr[0].ContactPerson.split(" ");
                $("#txtFirstName").val(fullnamesplit[0]);
                sMiddleName = $('#txtMiddleName').val(fullnamesplit[1]);
                $("#txtLastName").val(fullnamesplit[2]);
                $("#txtDesignation").val(Arr[0].Designation);
                $("#txtDepartment").val(Arr[0].Department);
                $("#txtEmail").val(Arr[0].email);
                $("#txtMobileNumber").val(Arr[0].Mobile);
            }
            else {
                Success("Something went wrong!")
            }
        }
    });
}


function AddStaff() {
    debugger;
    sFirstName = $('#txtFirstName').val();
    sLastName = $('#txtLastName').val();
    sDesignation = $('#txtDesignation').val();
    sAddress = $('#txtAddress').val();
    sPhone = $('#txtPhone').val();
    sMobile = $('#txtMobileNumber').val();
    sEmail = $('#txtEmail').val();
    sDepartment = $("#txtDepartment").val();
    bvalid = Validate_Staff();
    if (bvalid == true) {
        if ($("#btnAddStaff").val() == "Update") {
            debugger;
            $.ajax({
                type: "POST",
                url: "Handler/GenralHandler.asmx/UpdateStaffProfile",
                data: '{"sid":"' + hiddenID + '","sFirstName":"' + sFirstName + '","sLastName":"' + sLastName + '","sDesignation":"' + sDesignation + '","sMobile":"' + sMobile + '","sEmail":"' + sEmail + '","Department":"' + sDepartment + '"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.Session == 0) {
                        Success("Something went wrong!");
                        return false;
                    }
                    if (result.retCode == 1) {
                        Success("Profile updated successfully.");
                        setTimeout(function () {
                            window.location.href = "Dashboard.aspx";
                        }, 2000);
                      
                        return false;
                    }
                    else {
                    }
                },
                error: function () {
                    Success("An error occured while updating Profile");
                }
            });
        }
    }
}

function Validate_Staff() {
    bvalid = true;
    var reg = new RegExp('[0-9]$');
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    if (sFirstName == "") {
        bvalid = false;
        $("#lbl_txtFirstName").css("display", "");
    }
    if (sFirstName != "" && reg.test(sFirstName)) {
        bValid = false;
        $("#lbl_txtFirstName").html("* FirstName must not be numeric.");
        $("#lbl_txtFirstName").css("display", "");
    }
    if (sLastName == "") {
        bvalid = false;
        $("#lbl_txtLastName").css("display", "");
    }
    if (sLastName != "" && reg.test(sLastName)) {
        bValid = false;
        $("#lbl_txtLastName").html("* LastName must not be numeric.");
        $("#lbl_txtLastName").css("display", "");
    }
    if (sDesignation == "") {
        bvalid = false;
        $("#lbl_txtDesignation").css("display", "");
    }
    if (sDesignation != "" && reg.test(sDesignation)) {
        bvalid = false;
        $("#lbl_txtDesignation").html("* Designation must not be numeric at end.");
        $("#lbl_txtDesignation").css("display", "");
    }
    if (sDepartment == "") {
        bvalid = false;
        $("#lbl_txtDepartment").css("display", "");
    }
    if (sDepartment != "" && reg.test(sDepartment)) {
        bvalid = false;
        $("#lbl_txtDepartment").html("* Department must not be numeric at end.");
        $("#lbl_txtDepartment").css("display", "");
    }
    if (sMobile == "") {
        bvalid = false;
        $("#lbl_txtMobileNumber").css("display", "");
    }
    if (sMobile != "" && !(reg.test(sMobile))) {
        bvalid = false;
        $("#lbl_txtMobileNumber").html("* Mobile Number must be numeric.");
        $("#lbl_txtMobileNumber").css("display", "");
    }
    if (sEmail == "") {
        bvalid = false;
        $("#lbl_txtEmail").html("* This field is required");
        $("#lbl_txtEmail").css("display", "");
    }
    else {
        if (!(pattern.test(sEmail))) {
            bvalid = false;
            $("#lbl_txtEmail").html("* Wrong email format.");
            $("#lbl_txtEmail").css("display", "");
        }
    }
    return bvalid;
}

