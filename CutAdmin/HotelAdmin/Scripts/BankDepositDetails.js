﻿$(document).ready(function () {
    GetBankDepositDetail();
});

var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
var hiddensid;
var uid;
var DepositAmount;
var Typeofcash;
var Typeofcheque;
var LastUpdatedDate;
var total = 0;

function GetBankDepositDetail() {
    total = 0;
    $("#tbl_BankDepositDetails").dataTable().fnClearTable();
    $("#tbl_BankDepositDetails").dataTable().fnDestroy();
    //$("#tbl_BankDepositDetails tbody tr").remove();
    $.ajax({
        type: "POST",
        url: "Handler/GenralHandler.asmx/GetBankDepositDetail",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            //var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var List_BankDepositDetails = result.tbl_BankDeposit;
                var tRow = '';
                //var tRow = '<tr><td><span class="text-left" style="font-weight: bold">uid</span></td><td><span class="text-left" style="font-weight: bold">Deposit Amount</span></td><td><span class="text-left" style="font-weight: bold">Deposit Date</span></td>';
                //tRow += '<td><span class="text-left" style="font-weight: bold">Type of Cash</span></td><td><span class="text-left" style="font-weight: bold">Type of Cheque</span></td>';
                //tRow += '<td><span class="text-left" style="font-weight: bold">Employee Name</span></td><td><span class="text-left" style="font-weight: bold">Remarks</span></td><td><span class="text-left" style="font-weight: bold">Receipt Number</span></td><td><span class="text-left" style="font-weight: bold">Transaction Id</span></td>';
                //tRow += '<td><span class="text-left" style="font-weight: bold">Mobile</span></td><td><span class="text-left" style="font-weight: bold">Approve | Unapprove</span></td></tr>';
                for (var i = 0; i < List_BankDepositDetails.length; i++) {
                    tRow += '<tr>';
                    tRow += '<td>' + List_BankDepositDetails[i].AgencyName + '</td>';
                    tRow += '<td>' + List_BankDepositDetails[i].Agentuniquecode + '</td>';
                    tRow += '<td>' + List_BankDepositDetails[i].DepositDate.split(' ')[0] + '</td>';
                    //tRow += '<td>' + List_BankDepositDetails[i].Typeofdeposit + '</td>';
                    //tRow += '<td>' + List_BankDepositDetails[i].Typeofcash + '</td>';
                    //tRow += '<td>' + List_BankDepositDetails[i].Typeofcheque + '</td>';
                    //tRow += '<td>' + List_BankDepositDetails[i].EmployeeName + '</td>';
                    tRow += '<td>' + List_BankDepositDetails[i].Remarks + '</td>';
                    //tRow += '<td>' + List_BankDepositDetails[i].ReceiptNumber + '</td>';
                    //tRow += '<td>' + List_BankDepositDetails[i].TransactionId + '</td>';
                    tRow += '<td>' + List_BankDepositDetails[i].Mobile + '</td>';
                    tRow += '<td>' + List_BankDepositDetails[i].DepositAmount + '</td>';
                    tRow += '<td align="center"><a style="cursor:pointer" data-toggle="modal" data-target="#dlgAdminConfirmation" onclick="ApproveDialog(\'' + List_BankDepositDetails[i].uId + '\',\'' + List_BankDepositDetails[i].DepositAmount + '\',\'' + List_BankDepositDetails[i].Typeofcash + '\',\'' + List_BankDepositDetails[i].Typeofcheque + '\',\'' + List_BankDepositDetails[i].DepositDate.split(' ')[0] + '\',\'' + List_BankDepositDetails[i].sId + '\'); return false"><span class="fa fa-thumbs-o-up" title="Approve" aria-hidden="true"></span></a>|<a style="cursor:pointer" data-toggle="modal" data-target="#dlgAdminConfirmation" onclick="DisapproveDialog(\'' + List_BankDepositDetails[i].uId + '\',\'' + List_BankDepositDetails[i].DepositAmount + '\',\'' + List_BankDepositDetails[i].Typeofcash + '\',\'' + List_BankDepositDetails[i].Typeofcheque + '\',\'' + List_BankDepositDetails[i].DepositDate.split(' ')[0] + '\',\'' + List_BankDepositDetails[i].sId + '\'); return false"><span class="fa fa-thumbs-o-down" title="Unapprove" aria-hidden="true"></span></a></td>';
                    tRow += '</tr>';
                    total = Number(total) + Number(List_BankDepositDetails[i].DepositAmount);
                }
                $("#spnTotal").text(total);
                $("#tbl_BankDepositDetails tbody").html(tRow);
                $("#tbl_BankDepositDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                $('#tbl_BankDepositDetails').removeAttr("style");
            }
        },
        error: function () {
        }

    });
}

function ApproveDialog(ruid, rDepositAmount, rTypeofcash, rTypeofcheque, rDepositDate, rsId) {
    uid = ruid;
    DepositAmount = rDepositAmount;
    Typeofcash = rTypeofcash;
    Typeofcheque = rTypeofcheque;
    LastUpdatedDate = rDepositDate;
    hiddensid = rsId;
    $("#btnApprove").val("Approve");
    $.modal({
        content: '<div class="modal-body">' +
'<div class="scrollingDiv">' +
'<div class="columns">' +
'<div class="three-columns bold">Enter User Name</div>' +
'<div class="nine-columns"><div class="input full-width"><input name="prompt-value" id="txt_UserId" value="" class="input-unstyled full-width" type="text"></div></div></div> ' +
'<div class="columns">' +
'<div class="three-columns bold">Enter Password</div>' +
'<div class="nine-columns"><div class="input full-width"><input name="prompt-value" id="txt_UserPassword" value="" class="input-unstyled full-width" type="text"></div></div> ' +
'</div>' +

'<div class="columns">' +
'<div class="three-columns bold">Comments</div>' +
'<div class="nine-columns"><textarea id="txtComment" class="input full-width autoexpanding" rows="3"></textarea></div> ' +
'</div>' +
'<div class="columns">' +
'<div class="three-columns bold">&nbsp;</div>' +
'<div class="nine-columns bold"><input type="button" id="btnApprove" value="Approve" class="button compact marright anthracite-gradient" onclick="ApproveConfirmation()" />' +
'</div></div></div>',

        title: 'Admin Confirmation ',
        width: 500,
        scrolling: true,
        actions: {
            'Cancel': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Cancel': {
                classes: 'anthracite-gradient glossy',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
}

function DisapproveDialog(ruid, rDepositAmount, rTypeofcash, rTypeofcheque, rDepositDate, rsId) {
    uid = ruid;
    DepositAmount = rDepositAmount;
    Typeofcash = rTypeofcash;
    Typeofcheque = rTypeofcheque;
    LastUpdatedDate = rDepositDate;
    hiddensid = rsId;
    $("#btnApprove").val("Unapprove")

    $.modal({
        content: '<div class="modal-body">' +
'<div class="scrollingDiv">' +
'<div class="columns">' +
'<div class="three-columns bold">Enter User Name</div>' +
'<div class="nine-columns"><div class="input full-width"><input name="prompt-value" id="txt_UserId" value="" class="input-unstyled full-width" type="text"></div></div></div> ' +
'<div class="columns">' +
'<div class="three-columns bold">Enter Password</div>' +
'<div class="nine-columns"><div class="input full-width"><input name="prompt-value" id="txt_UserPassword" value="" class="input-unstyled full-width" type="text"></div></div> ' +
'</div>' +

'<div class="columns">' +
'<div class="three-columns bold">Comments</div>' +
'<div class="nine-columns"><textarea id="txtComment" class="input full-width autoexpanding" rows="3"></textarea></div> ' +
'</div>' +
'<div class="columns">' +
'<div class="three-columns bold">&nbsp;</div>' +
'<div class="nine-columns bold"><input type="button" id="btnApprove" value="Unapprove" class="button compact marright anthracite-gradient" onclick="ApproveConfirmation()" />' +
'</div></div></div>',

        title: 'Admin Confirmation ',
        width: 500,
        scrolling: true,
        actions: {
            'Cancel': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Cancel': {
                classes: 'anthracite-gradient glossy',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
}

function ApproveConfirmation() {
    var sUserName = $("#txt_UserId").val();
    var sPassword = $("#txt_UserPassword").val();
    var sComment = $("#txtComment").val();
    var bValid = true;

    if (sUserName == "") {
        bValid = false;
        Success("User Name/email cannot be blank");
        document.getElementById('txt_UserId').focus();
        return false;
    }
    else if (!emailReg.test(sUserName)) {
        bValid = false;
        Success("The email address you have entered is invalid");
        document.getElementById('txt_UserId').focus();
        return false;
    }
    if (sPassword == "") {
        bValid = false;
        Success("Password cannot be blank");
        document.getElementById('txt_UserPassword').focus();
        // $("#txt_UserPassword").focus();
        return false;
    }
    if (sComment == "") {
        bValid = false;
        Success("Comments cannot be blank");
        document.getElementById('txtComment').focus();
        // $("#txt_UserPassword").focus();
        return false;
    }
    if (bValid == true) {
        if ($("#btnApprove").val() == "Unapprove") {
            var Status = "Unapprove";
            //Success("Are you sure you want to unapprove the request?", "Approve", [sUserName, sPassword, "Unapprove"])
            $.modal({
                content: '<p class="avtiveDea">Are you sure you want to unapprove the request</strong></p>' +
'<p class="text-alignright text-popBtn"><button type="button" class="button anthracite-gradient" onclick="Approve(\'' + sUserName + '\',\'' + sPassword + '\',\'' + Status + '\')">OK</button></p>',
                width: 300,
                scrolling: false,
                actions: {
                    'Close': {
                        color: 'red',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttons: {
                    'Close': {
                        classes: 'anthracite-gradient glossy',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttonsLowPadding: false
            });
        }
        else {
            //Success("Are you sure you want to Approve the request?", "Approve", [sUserName, sPassword, "approve"])
            var Status = "approve";
            $.modal({
                content: '<p class="avtiveDea">Are you sure you want to approve the request</strong></p>' +
'<p class="text-alignright text-popBtn"><button type="button" class="button anthracite-gradient" onclick="Approve(\'' + sUserName + '\',\'' + sPassword + '\',\'' + Status + '\')">OK</button></p>',
                width: 300,
                scrolling: false,
                actions: {
                    'Close': {
                        color: 'red',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttons: {
                    'Close': {
                        classes: 'anthracite-gradient glossy',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttonsLowPadding: false
            });
        }
    }
}
function Approve(sUserName, sPassword, Status) {
    var sComment = $("#txtComment").val();
    if (Status == "Unapprove") {
        $.ajax({
            type: "POST",
            url: "Handler/GenralHandler.asmx/UserLogin",
            data: '{"sUserName":"' + sUserName + '","sPassword":"' + sPassword + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.Session == 0) {
                    Success("Something went wrong,Please try again.");
                    return false;
                }
                else if (result.retCode == 1) {
                    $.ajax({
                        url: "Handler/GenralHandler.asmx/UnApproveDeposit",
                        type: "POST",
                        data: '{"uid":"' + uid + '","sId":"' + hiddensid + '"}',
                        contentType: "application/json; charset=utf-8",
                        datatype: "json",
                        success: function (response) {
                            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                            if (result.Session != 1) {
                                Success("Session Expired!");
                            }
                            if (result.retCode == 1) {
                                Success("Credit Unapproved successfully");

                                setTimeout(function () {
                                    window.location.reload();
                                }, 2000)



                              
                            }
                            if (result.retCode == 0) {
                                Success("Something went wrong while processing your request! Please try again.");
                            }
                        },
                        error: function () {
                            Success('Error occured while processing your request! Please try again.');
                        }
                    });
                    $("#txt_UserId").val("");
                    $("#txt_UserPassword").val("");
                    $("#txtComment").val("");
                    return false;
                }
                else {
                    Success('Username/Password did not match.');
                }
            },
            error: function () {
            }
        });
    }
    else {
        $.ajax({
            type: "POST",
            url: "Handler/GenralHandler.asmx/UserLogin",
            data: '{"sUserName":"' + sUserName + '","sPassword":"' + sPassword + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.Session == 0) {
                    Success("Something went wrong,Please try again.");
                    return false;
                }
                else if (result.retCode == 1) {
                    if (result.roleID == "Admin") {
                        $.ajax({
                            url: "Handler/GenralHandler.asmx/ApproveDeposit",
                            type: "POST",
                            data: '{"uid":"' + uid + '","dDepositAmount":"' + DepositAmount + '","sTypeofCash":"' + Typeofcash + '","sTypeofcheque":"' + Typeofcheque + '","sComment":"' + sComment + '","sLastUpdatedDate":"' + LastUpdatedDate + '","sId":"' + hiddensid + '"}',
                            contentType: "application/json; charset=utf-8",
                            datatype: "json",
                            success: function (response) {
                                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                                if (result.Session != 1) {
                                    Success("Session Expired!");
                                }
                                if (result.retCode == 1) {
                                    Success("Credit Approved successfully");
                                    setTimeout(function () {
                                        window.location.reload();
                                    }, 2000)
                                
                                }
                                if (result.retCode == 0) {
                                    Success("Something went wrong while processing your request! Please try again.");
                                }
                            },
                            error: function () {
                                Success('Error occured while processing your request! Please try again.');
                            }
                        });
                    }
                    $("#txt_UserId").val("");
                    $("#txt_UserPassword").val("");
                    $("#txtComment").val("");
                    return false;
                }
                else {
                    Success('Username/Password did not match.');
                }
            },
            error: function () {
            }
        });
    }
}