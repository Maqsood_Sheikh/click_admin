﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelAdmin/Master.Master" AutoEventWireup="true" CodeBehind="ActivityMails.aspx.cs" Inherits="CutAdmin.HotelAdmin.ActivityMails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    <link href="../js/semantic.min.css" rel="stylesheet" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.12.2/semantic.min.js"></script>
    <link href="../js/multiple-emails.css" rel="stylesheet" />
    <script src="../js/multiple-emails.js"></script>
    
    <%--<script type="text/javascript">
        $(function () {
            //To render the input device to multiple email input using SemanticUI icon
            $('#example_emailSUI').multiple_emails({ theme: "SemanticUI" });
            //Shows the value of the input device, which is in JSON format
            $('#current_emailsSUI').text($('#example_emailSUI').val());
            $('#example_emailSUI').change(function () {
                var Departure = document.getElementsByClassName('remove icon'), i;
                for (i = 0; i < Departure.length; i += 1) {
                    Departure[i].className = "glyphicon glyphicon-remove"
                }
                $('#current_emailsSUI').text($(this).val());
            });

        });
	</script>--%>
    <script src="Scripts/ActivityMails.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Hotel Mails</h1>
            <hr />
        </hgroup>
        <div class="with-padding">
            <%--<div class="standard-tabs margin-bottom" id="add-tabs">--%>
                <%--<ul class="tabs">--%>
                    <%--<li class="active"><a href="#VisaMails">Visa Mails</a></li>
                    <li><a href="#OTBMails">OTB Mails</a></li>--%>
                    <%--<li class="active"><a href="#HotelMails">Hotel Mails</a></li>--%>
                <%--</ul>--%>
                <%--<div class="tabs-content">--%>
                    <div id="HotelMails">
                        <div class="with-padding">
                            <div class="respTable">
                                <table class="table responsive-table" id="tbl_HotelDetails">
                                    <thead>
                                        <tr>
                                            <th scope="col">Activity</th>
                                            <th scope="col" class="align-center">Mail Id  </th>
                                            <th scope="col" class="align-center">CC Mail Id </th>
                                            <th scope="col" class="align-center">Error Message </th>
                                            <th scope="col" class="align-center">Add | Edit </th>
                                        </tr>
                                    </thead>

                                </table>
                            </div>
                        </div>
                    </div>
                    <%--<div id="VisaMails">
                        <div class="with-padding">

                            <div class="table-header button-height anthracite-gradient">
                                <div class="float-right">
                                    Search&nbsp;<input type="text" name="table_search" id="table_search" value="" class="input mid-margin-left">
                                </div>

                                Show&nbsp;<select name="range" class="select anthracite-gradient glossy">
                                    <option value="1" selected="selected">10</option>
                                    <option value="2">20</option>
                                    <option value="3">40</option>
                                    <option value="4">100</option>
                                </select>
                                entries
                            </div>
                            <div class="respTable">
                                <table class="table responsive-table" id="sorting-advanced">

                                    <thead>
                                        <tr>
                                            <th scope="col">Activity</th>
                                            <th scope="col" class="align-center">Mail Id  </th>
                                            <th scope="col" class="align-center">CC Mail Id </th>
                                            <th scope="col" class="align-center">Update</th>
                                        </tr>
                                    </thead>

                        
                                    <tbody>
                                        <tr>
                                            <td>Visa Apply</td>
                                            <td>dubaivisa@clickurtrip.com</td>
                                            <td>niyaz@clickurtrip.com;liyaquat@clickurtrip.com</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>
                                        <tr>
                                            <td>Incomplete application</td>
                                            <td>dubaivisa@clickurtrip.com</td>
                                            <td>niyaz@clickurtrip.com</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>
                                        <tr>
                                            <td>Delete Incomplete application</td>
                                            <td>dubaivisa@clickurtrip.com</td>
                                            <td>liyaquat@clickurtrip.com</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>
                                        <tr>
                                            <td>Application Approved</td>
                                            <td>dubaivisa@clickurtrip.com</td>
                                            <td>niyaz@clickurtrip.com;</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>
                                        <tr>
                                            <td>Application Rejected</td>
                                            <td>dubaivisa@clickurtrip.com</td>
                                            <td>niyaz@clickurtrip.com;liyaquat@clickurtrip.com</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>
                                        <tr>
                                            <td>Entered Country</td>
                                            <td>dubaivisa@clickurtrip.com</td>
                                            <td>niyaz@clickurtrip.com;liyaquat@clickurtrip.com</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>
                                        <tr>
                                            <td>Exit Country</td>
                                            <td>dubaivisa@clickurtrip.com</td>
                                            <td>niyaz@clickurtrip.com;liyaquat@clickurtrip.com</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>
                                        <tr>
                                            <td>Stay Limit Expiry</td>
                                            <td>dubaivisa@clickurtrip.com</td>
                                            <td>niyaz@clickurtrip.com;liyaquat@clickurtrip.com</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>

                                    </tbody>

                                </table>
                            </div>
                            <form method="post" action="" class="table-footer button-height large-margin-bottom anthracite-gradient">
                                <div class="float-right">
                                    <div class="button-group">
                                        <a href="#" title="First page" class="button anthracite-gradient glossy"><span class="icon-previous"></span></a>
                                        <a href="#" title="Previous page" class="button anthracite-gradient glossy"><span class="icon-backward"></span></a>
                                    </div>

                                    <div class="button-group">
                                        <a href="#" title="Page 1" class="button anthracite-gradient glossy">1</a>
                                        <a href="#" title="Page 2" class="button anthracite-gradient glossy active">2</a>
                                        <a href="#" title="Page 3" class="button anthracite-gradient glossy">3</a>
                                        <a href="#" title="Page 4" class="button anthracite-gradient glossy">4</a>
                                    </div>

                                    <div class="button-group">
                                        <a href="#" title="Next page" class="button anthracite-gradient glossy"><span class="icon-forward"></span></a>
                                        <a href="#" title="Last page" class="button anthracite-gradient glossy"><span class="icon-next"></span></a>
                                    </div>
                                </div>

                                <div class="dataTables_info" id="sorting-advanced_info">Showing 1 to 10 of 18 entries</div>
                            </form>




                        </div>
                    </div>

                    <div id="OTBMails">
                        <div class="with-padding">
                            <div class="table-header button-height anthracite-gradient">
                                <div class="float-right">
                                    Search&nbsp;<input type="text" name="table_search" id="table_search" value="" class="input mid-margin-left">
                                </div>

                                Show&nbsp;<select name="range" class="select anthracite-gradient glossy">
                                    <option value="1" selected="selected">10</option>
                                    <option value="2">20</option>
                                    <option value="3">40</option>
                                    <option value="4">100</option>
                                </select>
                                entries
                            </div>
                            <div class="respTable">
                                <table class="table responsive-table" id="sorting-advanced">

                                    <thead>
                                        <tr>
                                            <th scope="col">Activity</th>
                                            <th scope="col" class="align-center">Mail Id  </th>
                                            <th scope="col" class="align-center">CC Mail Id </th>
                                            <th scope="col" class="align-center">Add | Edit </th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        <tr>
                                            <td>Visa Issued by us</td>
                                            <td>otb@clickurtrip.com</td>
                                            <td>niyaz@clickurtrip.com;liyaquat@clickurtrip.com</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>
                                        <tr>
                                            <td>If OTB not required</td>
                                            <td>otb@clickurtrip.com</td>
                                            <td>niyaz@clickurtrip.com</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>
                                        <tr>
                                            <td>OTB Applied</td>
                                            <td>otb@clickurtrip.com</td>
                                            <td>liyaquat@clickurtrip.com</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>
                                        <tr>
                                            <td>OTB Approved</td>
                                            <td>otb@clickurtrip.com</td>
                                            <td>niyaz@clickurtrip.com;</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>
                                        <tr>
                                            <td>OTB Rejected</td>
                                            <td>otb@clickurtrip.com</td>
                                            <td>niyaz@clickurtrip.com;liyaquat@clickurtrip.com</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>
                                        <tr>
                                            <td>OTB Pending</td>
                                            <td>otb@clickurtrip.com</td>
                                            <td>niyaz@clickurtrip.com;liyaquat@clickurtrip.com</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>


                                    </tbody>

                                </table>
                            </div>
                            <form method="post" action="" class="table-footer button-height large-margin-bottom anthracite-gradient">
                                <div class="float-right">
                                    <div class="button-group">
                                        <a href="#" title="First page" class="button anthracite-gradient glossy"><span class="icon-previous"></span></a>
                                        <a href="#" title="Previous page" class="button anthracite-gradient glossy"><span class="icon-backward"></span></a>
                                    </div>

                                    <div class="button-group">
                                        <a href="#" title="Page 1" class="button anthracite-gradient glossy">1</a>
                                        <a href="#" title="Page 2" class="button anthracite-gradient glossy active">2</a>
                                        <a href="#" title="Page 3" class="button anthracite-gradient glossy">3</a>
                                        <a href="#" title="Page 4" class="button anthracite-gradient glossy">4</a>
                                    </div>

                                    <div class="button-group">
                                        <a href="#" title="Next page" class="button anthracite-gradient glossy"><span class="icon-forward"></span></a>
                                        <a href="#" title="Last page" class="button anthracite-gradient glossy"><span class="icon-next"></span></a>
                                    </div>
                                </div>

                                <div class="dataTables_info" id="sorting-advanced_info">Showing 1 to 10 of 18 entries</div>
                            </form>
                        </div>
                    </div>--%>
                <%--</div>--%>
            <%--</div>--%>

        </div>
    </section>
    <!-- End main content -->


</asp:Content>
