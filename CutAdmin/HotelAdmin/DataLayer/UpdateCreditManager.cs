﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;

namespace CutAdmin.HotelAdmin.DataLayer
{
    public class UpdateCreditManager
    {
        public static DBHelper.DBReturnCode ValidateLimit(Int64 uid, out DataTable dtResult)
        {
            SqlParameter[] SQLParams = new SqlParameter[1];
            SQLParams[0] = new SqlParameter("@uid", uid);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_AdminCreditLimt_Validation", out dtResult, SQLParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode UpdateCredit(Int64 sid, Int64 uid, int bActiveFlag, decimal DepositAmmount, decimal CreditAmmount, decimal MaxCreditAmmount, decimal ReduceAmmount, string Remark, string Invoice, out int rows)
        {
            Int64 sessionsid;
            //string ExecutiveName = "";
            //int MinLimit = 0;
            string Transactiontype, AgainInvoice = "";
            string PerticulaAmount = "";
            string otc;
            if (MaxCreditAmmount > 0)
            {
                otc = "True";
            }
            else
            {
                otc = "false";
            }
            if (DepositAmmount != 0)
            {
                Transactiontype = "Credit";
            }
            else
            {
                Transactiontype = "Debit";
            }
            if (DepositAmmount != 0)
            {
                PerticulaAmount = Convert.ToString(DepositAmmount);
            }
            else if (ReduceAmmount != 0)
            {
                PerticulaAmount = Convert.ToString(ReduceAmmount);
            }
            string Particulars = "";
            if (Invoice != "undefined")
            {
                AgainInvoice = " - " + Invoice;
            }

            GlobalDefault sessionobj;
            sessionobj = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            sessionsid = sessionobj.sid;
            string ContactPerson = sessionobj.ContactPerson;
            Particulars = Transactiontype + " - " + ContactPerson + " - " + PerticulaAmount + AgainInvoice;
            SqlParameter[] sqlParams = new SqlParameter[12];
            sqlParams[0] = new SqlParameter("@sid", sid);
            sqlParams[1] = new SqlParameter("@uid", uid);
            sqlParams[2] = new SqlParameter("@LastUpdatedDate", DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
            sqlParams[3] = new SqlParameter("@ActiveFlag", bActiveFlag);
            sqlParams[4] = new SqlParameter("@DepositAmmount", DepositAmmount);
            sqlParams[5] = new SqlParameter("@CreditAmmount", CreditAmmount);
            sqlParams[6] = new SqlParameter("@MaxCreditAmmount", MaxCreditAmmount);
            sqlParams[7] = new SqlParameter("@ReduceAmmount", ReduceAmmount);
            sqlParams[8] = new SqlParameter("@OTCActive", otc);
            sqlParams[9] = new SqlParameter("@Remark", Remark);
            sqlParams[10] = new SqlParameter("@Particulars", Particulars);
            sqlParams[11] = new SqlParameter("@InvoiceNo", AgainInvoice);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_AdminCreditLimitCreditDebit", out rows, sqlParams);
            //if (retCode == DBHelper.DBReturnCode.SUCCESS)
            //{
            //    if (ReduceAmmount == 0)
            //    {

            //        string sSubject = "Account credited";
            //        string sMessageTitle = "Account credited";
            //        string sMessage = "Your account is credited by Admin.<br> Amount of Rs." + DepositAmmount + " added in your account.";
            //       // UpdateCreditManager.AccountingEmail(Convert.ToInt16(uid), sSubject, sMessageTitle, sMessage);
            //    }
            //    if (ReduceAmmount > 0)
            //    {
            //        string sSubject = "Account Debited";
            //        string sMessageTitle = "Account Debited";
            //        string sMessage = "Your account is debited against " + Invoice + ".<br> Amount of Rs." + ReduceAmmount + " debited from your account.";
            //      //  UpdateCreditManager.AccountingEmail(Convert.ToInt16(uid), sSubject, sMessageTitle, sMessage);
            //    }

            //}

            return retCode;
        }

        public static DBHelper.DBReturnCode GetLastUpdateCreditDetails(Int64 sid, out DataTable dtResult)
        {

            try
            {
                bool CreditLmit = false;
                DateTime LimitDate = DateTime.Now;
                DateTime Today = DateTime.Now;
                SqlParameter[] sqlParams = new SqlParameter[1];
                sqlParams[0] = new SqlParameter("@uid", sid);
                DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_AdminCreditLimitLoadByKey", out dtResult, sqlParams);
                CreditLmit = Convert.ToBoolean(dtResult.Rows[0]["Credit_Flag"]);
                Int64 limitDays = 0;
                if (CreditLmit == true)
                {
                    LimitDate = ConvertDateTime(dtResult.Rows[0]["Spn_DateLimit"].ToString());
                    limitDays = Convert.ToInt64(dtResult.Rows[0]["Spn_NoDay"].ToString());
                    LimitDate = LimitDate.AddDays(limitDays);
                    if (LimitDate < Today)
                    {
                        retCode = ResetPriviouslimit(sid);
                        if (retCode == DBHelper.DBReturnCode.SUCCESS)
                        {
                            SqlParameter[] sqlParams1 = new SqlParameter[1];
                            sqlParams1[0] = new SqlParameter("@uid", sid);
                            retCode = DBHelper.GetDataTable("Proc_tbl_AdminCreditLimitLoadByKey", out dtResult, sqlParams1);
                        }

                    }
                }
                return retCode;
            }
            catch
            {
                SqlParameter[] sqlParams = new SqlParameter[1];
                sqlParams[0] = new SqlParameter("@uid", sid);
                DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_AdminCreditLimitLoadByKey", out dtResult, sqlParams);
                return retCode;
            }

        }

        #region Bank/ Cash Deposite Update
        public static DBHelper.DBReturnCode AddDeposit(Int64 Uid, string BankName, string AccountNumber, string AmountDeposit, string Remarks, string TypeOfDeposit, string TypeOfCash, string TypeOfCheque, string EmployeeName, string ReceiptNumber, string ChequeDDNumber, string ChequeDrawn, string Date)
        {
            int rowsAffected = 0;
            string DepositDate = Date;
            decimal dlAmountDeposit;
            decimal.TryParse(AmountDeposit, out dlAmountDeposit);
            string sCreationdate = DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 uId = objGlobalDefaults.sid;
            DataTable dtResult;
            string Particulars = "";
            if (TypeOfCash == "Bank")
            {
                Particulars = BankName + AccountNumber + " - " + TypeOfDeposit + " - " + ReceiptNumber;
            }
            else
            {
                //Particulars = TypeOfCash + " - " + EmployeeName + " - " + AmountDeposit + " - " + ReceiptNumber;
                Particulars = TypeOfCash + " - " + EmployeeName + " - " + dlAmountDeposit + " - " + ReceiptNumber;
            }

            SqlParameter[] SQLParams = new SqlParameter[37];
            SQLParams[0] = new SqlParameter("@uId", Uid);
            SQLParams[1] = new SqlParameter("@DepositAmount", dlAmountDeposit);
            SQLParams[2] = new SqlParameter("@BankName", BankName);
            SQLParams[3] = new SqlParameter("@AccountNumber", AccountNumber);
            SQLParams[4] = new SqlParameter("@ChequeDetail", "");
            SQLParams[5] = new SqlParameter("@CreationDate", sCreationdate);
            SQLParams[6] = new SqlParameter("@Remarks", Remarks);
            SQLParams[7] = new SqlParameter("@DepositUpdateFlag", 1);
            SQLParams[8] = new SqlParameter("@CreditedAmount", 000);
            SQLParams[9] = new SqlParameter("@Updatedate", "");
            SQLParams[10] = new SqlParameter("@BankNarration", "");
            SQLParams[11] = new SqlParameter("@ExecutiveName", "");
            SQLParams[12] = new SqlParameter("@DepositDate", DepositDate);
            SQLParams[13] = new SqlParameter("@Typeofdeposit", TypeOfDeposit);
            SQLParams[14] = new SqlParameter("@Typeofcash", TypeOfCash);
            SQLParams[15] = new SqlParameter("@Typeofcheque", TypeOfCheque);
            SQLParams[16] = new SqlParameter("@EmployeeName", EmployeeName);
            SQLParams[17] = new SqlParameter("@ReceiptNumber", ReceiptNumber);
            SQLParams[18] = new SqlParameter("@TransactionId", ReceiptNumber);
            SQLParams[19] = new SqlParameter("@ChequeorDDNumber", ChequeDDNumber);
            SQLParams[20] = new SqlParameter("@ChequeDrawn", ChequeDrawn);
            SQLParams[21] = new SqlParameter("@ExecutiveRemarks", "");
            SQLParams[22] = new SqlParameter("@Mobile", "N/A");
            SQLParams[23] = new SqlParameter("@ApproveFlag", 1);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_BankDepositAddByAdmin", out rowsAffected, SQLParams);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {
                    SqlParameter[] SQLParams2 = new SqlParameter[2];
                    SQLParams2[0] = new SqlParameter("@uid", Uid);
                    SQLParams2[1] = new SqlParameter("@CreationDate", sCreationdate);
                    retCode = DBHelper.GetDataTable("Proc_tbl_BankDepositLoadBySid", out dtResult, SQLParams2);
                    if (retCode == DBHelper.DBReturnCode.SUCCESS)
                    {
                        string sCreditType;
                        decimal dCreditAmount = 0;
                        decimal dMaxCreditLimit = 0;
                        decimal dAvailableCredit = 0;
                        string securitycheqamt = "";
                        string monthlybusiness = "";
                        string securitycheqbank = "";
                        string securitycheqdate = "";
                        string ExecutiveName = "";
                        int MinLimit = 0;
                        decimal dAvailableCash = 0;
                        decimal dReduce = 0;
                        decimal Result = 0;
                        int rows = 0;
                        Int64 sid = Convert.ToInt64(dtResult.Rows[0]["sId"]);
                        sCreditType = "Bank";
                        SqlParameter[] sqlParams = new SqlParameter[37];
                        sqlParams[0] = new SqlParameter("@uid", Uid);
                        sqlParams[1] = new SqlParameter("@LimitUpdate", dlAmountDeposit);
                        sqlParams[2] = new SqlParameter("@LastUpdateDate", DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
                        sqlParams[3] = new SqlParameter("@Comments", Remarks);
                        sqlParams[4] = new SqlParameter("@UpdateByID", uId);
                        sqlParams[5] = new SqlParameter("@ApprovedByID", uId);
                        sqlParams[6] = new SqlParameter("@ApproveDate", DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
                        sqlParams[7] = new SqlParameter("@ApproveFlag", 1);
                        sqlParams[8] = new SqlParameter("@CreditType", sCreditType);
                        sqlParams[9] = new SqlParameter("@ExecutiveName", ExecutiveName);
                        sqlParams[10] = new SqlParameter("@DepositAmount", dlAmountDeposit);
                        sqlParams[11] = new SqlParameter("@CreditAmount", dCreditAmount);
                        sqlParams[12] = new SqlParameter("@MaxCreditLimit", dMaxCreditLimit);
                        sqlParams[13] = new SqlParameter("@monthlybusiness", monthlybusiness);
                        sqlParams[14] = new SqlParameter("@securitycheqamt", securitycheqamt);
                        sqlParams[15] = new SqlParameter("@securitycheqbank", securitycheqbank);
                        sqlParams[16] = new SqlParameter("@securitycheqdate", securitycheqdate);
                        sqlParams[17] = new SqlParameter("@sid", sid);
                        sqlParams[18] = new SqlParameter("@AvailableCredit", dlAmountDeposit);
                        sqlParams[19] = new SqlParameter("@MinLimit", MinLimit);
                        sqlParams[20] = new SqlParameter("@ActiveFlag", 1);
                        sqlParams[21] = new SqlParameter("@StatementBalance", dAvailableCredit);
                        sqlParams[22] = new SqlParameter("@AvailableCash", dAvailableCash);
                        sqlParams[23] = new SqlParameter("@ReduceAmount", dReduce);
                        sqlParams[24] = new SqlParameter("@Result", Result);
                        sqlParams[25] = new SqlParameter("@LastUpdatedDate", DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
                        sqlParams[26] = new SqlParameter("@MaxCreditAmmount", 0);
                        sqlParams[27] = new SqlParameter("@OTCActive", 0);
                        sqlParams[28] = new SqlParameter("@Particulars", Particulars);
                        if (TypeOfCash == "Bank")
                        {
                            sqlParams[29] = new SqlParameter("@IsTypeBank", true);
                            sqlParams[30] = new SqlParameter("@BankName", BankName);
                            sqlParams[31] = new SqlParameter("@AccountNumber", AccountNumber);
                            sqlParams[32] = new SqlParameter("@TypeOfDeposit", TypeOfDeposit);
                            sqlParams[33] = new SqlParameter("@ReceiptNumber", ReceiptNumber);
                            sqlParams[34] = new SqlParameter("@TypeOfCash", TypeOfCash);
                            sqlParams[35] = new SqlParameter("@EmployeeName", EmployeeName);
                            sqlParams[36] = new SqlParameter("@dlAmountDeposit", dlAmountDeposit);
                        }
                        else
                        {
                            sqlParams[29] = new SqlParameter("@IsTypeBank", false);
                            sqlParams[30] = new SqlParameter("@BankName", BankName);
                            sqlParams[31] = new SqlParameter("@AccountNumber", AccountNumber);
                            sqlParams[32] = new SqlParameter("@TypeOfDeposit", TypeOfDeposit);
                            sqlParams[33] = new SqlParameter("@ReceiptNumber", ReceiptNumber);
                            sqlParams[34] = new SqlParameter("@TypeOfCash", TypeOfCash);
                            sqlParams[35] = new SqlParameter("@EmployeeName", EmployeeName);
                            sqlParams[36] = new SqlParameter("@dlAmountDeposit", dlAmountDeposit);
                        }


                        retCode = DBHelper.ExecuteNonQuery("Proc_tbl_AdminCreditLimit_LogAddUpdateByCredit", out rows, sqlParams);

                        //if (retCode == DBHelper.DBReturnCode.SUCCESS)
                        //{
                        //    int uid = Convert.ToInt16 (Uid);
                        //    string sSubject = "Account credited";
                        //    string sMessageTitle = "Account credited";
                        //    string sMessage = "Your account is credited against your successful payment.<br> Amount of Rs." + AmountDeposit + " added in your account.";
                        //    UpdateCreditManager.AccountingEmail(uid, sSubject, sMessageTitle, sMessage);
                        //}


                    }
                }

            }

            return retCode;
        }

        #endregion

        #region Limit/OTC
        public static DBHelper.DBReturnCode UpdateCreditLmit(Int64 sid, Int64 uid, int bActiveFlag, decimal MaxCreditAmmount, decimal CreditLimit, string days, string Remark, out int rows)
        {
            Int64 sessionsid;
            string otc, Particulars;
            decimal DepositAmmount = 0;
            decimal CreditAmmount = CreditLimit;
            decimal ReduceAmmount = 0;
            if (MaxCreditAmmount > 0)
            {
                otc = "True";
            }
            else
            {
                otc = "false";
            }
            string LimiType = "";
            if (days == "")
            {
                LimiType = "(OTC -)";
            }
            else
            {
                LimiType = "(FixLimit -)";
            }
            GlobalDefault sessionobj;
            sessionobj = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            sessionsid = sessionobj.sid;
            string Person = sessionobj.ContactPerson;


            if (otc == "false")
            {
                Particulars = "Fix Limit- " + " " + Person + " - " + CreditAmmount;
            }
            else
            {
                Particulars = "One Time Limit- " + " " + Person + " - " + MaxCreditAmmount;
            }


            SqlParameter[] sqlParams = new SqlParameter[13];
            sqlParams[0] = new SqlParameter("@sid", sid);
            sqlParams[1] = new SqlParameter("@uid", uid);
            sqlParams[2] = new SqlParameter("@LastUpdatedDate", DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
            sqlParams[3] = new SqlParameter("@ActiveFlag", bActiveFlag);
            sqlParams[4] = new SqlParameter("@DepositAmmount", DepositAmmount);
            sqlParams[5] = new SqlParameter("@CreditAmmount", CreditAmmount);
            sqlParams[6] = new SqlParameter("@MaxCreditAmmount", MaxCreditAmmount);
            sqlParams[7] = new SqlParameter("@ReduceAmmount", ReduceAmmount);
            sqlParams[8] = new SqlParameter("@OTCActive", otc);
            sqlParams[9] = new SqlParameter("@Remark", Remark);
            sqlParams[10] = new SqlParameter("@Particulars", Particulars);
            sqlParams[11] = new SqlParameter("@Spn_NoDay", days);
            sqlParams[12] = new SqlParameter("@Spn_DateLimit", DateTime.Now.ToString("dd-mm-yy"));
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_AdminCreditLimitUpdateByFixLimit", out rows, sqlParams);
            //if (retCode == DBHelper.DBReturnCode.SUCCESS)
            //{
            //    string sSubject = "";
            //    string sMessageTitle="";
            //    string sMessage="";
            //    if (CreditAmmount > 0)
            //    {
            //        sSubject = "Fix Credit Limit Approved";
            //        sMessageTitle= "Fix Credit Limit Approved";
            //        sMessage = "Admin has approved request for Fix credit Limit.<br>Your fix limit is Rs." + CreditAmmount;
            //    }
            //    if (MaxCreditAmmount>0)
            //    {
            //        sSubject = "One Time Credit Limit Approved";
            //        sMessageTitle= "One Time Credit Limit Approved";
            //        sMessage="Admin has approved request for One time credit Limit. <br>Your one time limit is Rs."+MaxCreditAmmount;
            //    }
            //  // UpdateCreditManager.AccountingEmail(Convert.ToInt16(uid), sSubject, sMessageTitle, sMessage);
            //}
            //DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_AdminCreditLimitDeposite", out rows, sqlParams);
            return retCode;
        }

        #endregion

        #region ResetAvaillimit
        public static DateTime ConvertDateTime(string Date)
        {
            DateTime date = new DateTime();
            try
            {
                string CurrentPattern = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
                string[] Split = new string[] { "-", "/", @"\", "." };
                string[] Patternvalue = CurrentPattern.Split(Split, StringSplitOptions.None);
                string[] DateSplit = Date.Split(Split, StringSplitOptions.None);
                string NewDate = "";
                if (Patternvalue[0].ToLower().Contains("d") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[0] + "/" + DateSplit[1] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("m") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[0] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("d") == true)
                {
                    NewDate = DateSplit[2] + "/" + DateSplit[1] + "/" + DateSplit[0];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("m") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[2] + "/" + DateSplit[0];
                }
                date = DateTime.Parse(NewDate, Thread.CurrentThread.CurrentCulture);
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

            return date;

        }
        public static DBHelper.DBReturnCode ResetPriviouslimit(Int64 Uid)
        {
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            int Rowseffected = 0;
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@uid", Uid);
            sqlParams[1] = new SqlParameter("@LastUpdatedDate", DateTime.Now.ToString("dd-mm-yyyy"));
            retCode = DBHelper.ExecuteNonQuery("Proc_tbl_AdminCreditLimitUpdateByCreditLimit", out Rowseffected, sqlParams);
            return retCode;

        }
        #endregion ResetAvaillimit
    }
}