﻿using CutAdmin.BL;
using CutAdmin.Common;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;

namespace CutAdmin.HotelAdmin.DataLayer
{
    public class SupplierProfileManager
    {
        public static string ChangePassword(string sOldPassword, string sNewPassword)
        {
            int rowsAffected = 0;
            //string sDecryptedOldPassword = Cryptography.DecryptText(sOldPassword);
            if (HttpContext.Current.Session["LoginUser"] != null)
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 sid = objGlobalDefault.sid;
                string sPassword = objGlobalDefault.password;
                //if (sOldPassword == sPassword)
                if (sOldPassword == sPassword)
                {
                    DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.SUCCESS;
                    string sEncryptedPassword = Cryptography.EncryptText(sNewPassword);
                    SqlParameter[] sqlParams = new SqlParameter[2];
                    sqlParams[0] = new SqlParameter("@sid", sid);
                    sqlParams[1] = new SqlParameter("@password", sEncryptedPassword);
                    if (objGlobalDefault.UserType == "Admin" || objGlobalDefault.UserType == "Franchisee" || objGlobalDefault.UserType == "Supplier" || objGlobalDefault.UserType == "Agent")
                    {
                        retCode = DBHelper.ExecuteNonQuery("Proc_tbl_AdminLoginPasswordChangeByKey", out rowsAffected, sqlParams);
                        if (retCode == DBHelper.DBReturnCode.SUCCESS)
                        {
                            retCode = DefaultManager.UserLogin(objGlobalDefault.uid, sNewPassword);
                        }
                    }
                    else if (objGlobalDefault.UserType == "AdminStaff" || objGlobalDefault.UserType == "SupplierStaff")
                    {
                        retCode = DBHelper.ExecuteNonQuery("Proc_tbl_StaffLoginPasswordChangeByKey", out rowsAffected, sqlParams);
                    }
                    if (DBHelper.DBReturnCode.SUCCESS == retCode)
                        return "Success";
                    else
                        return "Exception_SQLException";
                }
                else
                    return "Exception_PasswordsNotMatch";
            }
            else
                return "Exception_SessionExpired";
        }
    }
}