﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelAdmin/Master.Master" AutoEventWireup="true" CodeBehind="Statements.aspx.cs" Inherits="CutAdmin.HotelAdmin.Statements" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    
    <script src="Scripts/Statement.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Statements</h1>
            <hr />
        </hgroup>
        <div class="with-padding">
            <div class="columns">
                <div class="three-columns twelve-columns-mobile four-columns-tablet">
                    <label>Type</label>
                    <div class="full-width button-height typeboth">
                        <select id="TransactionType" class="select">
                            <option value="Both">Both</option>
                            <option value="Credited">Credited</option>
                            <option value="Debited">Debited</option>
                        </select>
                    </div>
                </div>
                <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                    <label>From</label>
                    <span class="input full-width">
                        <span class="icon-calendar"></span>
                        <input type="text" name="datepicker" id="datepicker3" class="input-unstyled datepicker" value="">
                    </span>
                </div>
                <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                    <label>To</label>
                    <span class="input full-width">
                        <span class="icon-calendar"></span>
                        <input type="text" name="datepicker" id="datepicker4" class="input-unstyled datepicker" value="">
                    </span>
                </div>
                <div class="three-columns four-columns-tablet twelve-columns-mobile">
                    <label>Reference No</label>
                    <div class="input full-width">
                        <input type="text" id="txt_Reference" class="input-unstyled full-width">
                    </div>
                </div>
                <div class="ten-columns twelve-columns-mobile formBTn text-alignright">
                    <button type="button" class="button anthracite-gradient" onclick="GetAgencyStatementByDate()">Search</button>
                </div>
                <div class="two-columns twelve-columns-mobile formBTn">
                    <span class="icon-pdf right" onclick="ExportAgencyStatement('PDF')">
                        <img src="../img/PDF-Viewer-icon.png" style="cursor: pointer" title="Export To Pdf" height="35" width="35">
                    </span>
                    <span class="icon-excel right" onclick="ExportAgencyStatement('excel')">
                        <img src="../img/Excel-2-icon.png" style="cursor: pointer" title="Export To Excel" id="btnPDF" height="35" width="35"></span>
                </div>

            </div>
            <div class="respTable">
                <table class="table responsive-table" id="tbl_AgencyStatement">
                    <thead>
                        <tr>
                            <th scope="col">S.No</th>
                            <th scope="col">Date</th>
                            <th scope="col" class="align-center">Reference No.</th>
                            <th scope="col" class="align-center">Particulars</th>
                            <th scope="col" class="align-center">Credited</th>
                            <th scope="col" class="align-center">Debited</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</asp:Content>
