﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddHotelDetails.aspx.cs" Inherits="CutAdmin.MappingDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/AddHotelDetails.js?v3.3"></script>

    
    <!-- Additional styles -->
	<link rel="stylesheet" href="css/styles/form.css?v=1">
	<link rel="stylesheet" href="css/styles/switches.css?v=1">
	<link rel="stylesheet" href="css/styles/table.css?v=1">

	<!-- DataTables -->
	<link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

    <style>
        .selectedImg {
              border:3px solid #1b397b;
        }
    </style>
    <script>
       
    </script>
	<!-- Microsoft clear type rendering -->
	<meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <!-- Main content -->
	<section  id="main" >

        <div class="with-padding">
      
                      <div class="DivAddMapping" style="display:none">
                    <div class="full-page-wizard">
                        <form id="AddfrmWizard" class="wizard same-height columns">

                            <h3 class="block-title">Add Hotel Mapping <small style="float: right" class="blue">HOTEL:
                                <label id="lblHotelname"></label>
                            </small></h3>

                            <fieldset class="wizard-fieldset" style="border: 1px ridge">
                                <legend class="legend">Hotel Details</legend>
                                <div class="button-height">
                                    <div class="columns" style="min-height: 300px;">
                                        <div class="one-columns">
                                            <small class="input-info">Hotel Name:&nbsp;&nbsp;&nbsp;&nbsp;</small>
                                        </div>
                                        <div class="five-columns">
                                            <input type="text" id="HtlName" size="9" class="input full-width" value="">
                                        </div>
                                        <div class="one-columns">
                                            <small class="input-info">Ratings</small>
                                        </div>
                                        <div class="four-columns">

                                            <%--<input type="text" id="HtlRatings" class="input full-width" value="">--%>
                                            <div id="HtlRatings"></div>
                                            &nbsp;
                                             <select id="SelStars"  style="height:25px;background-color:#f0efef" onchange="fnStarRatings(this.value,'ClickUrTrip','#HtlRatings',this.id);">
                                                 <option value="1">1 Star</option>
                                                 <option value="2">2 Star</option>
                                                 <option value="3">3 Star</option>
                                                 <option value="4">4 Star</option>
                                                 <option value="5">5 Star</option>
                                                 <option value="0">No Ratings</option>
                                             </select>
                                        </div>
                                        <div class="new-row one-columns">
                                            <small class="input-info">Description:&nbsp;&nbsp;&nbsp;&nbsp;</small>
                                        </div>
                                        <div class=" ten-columns">

                                            <textarea name="autoexpanding" id="HtlDescription" class="input full-width autoexpanding" style="min-height: 130px; resize: none"></textarea>
                                        </div>
                                        <div class="new-row one-columns">
                                            <small class="input-info">Hotel Adress:</small>
                                        </div>
                                        <div class="ten-columns">
                                            <textarea name="autoexpanding" id="HtlAddress" class="input full-width autoexpanding" style="resize: none;"></textarea>
                                        </div>
                                        <div class="new-row one-columns">
                                            <small class="input-info">City:</small>
                                        </div>
                                        <div class="two-columns" style="margin-left: 72px;">
                                            <input type="text" id="htlCity" class="input full-width" value="" readonly>
                                        </div>
                                        <div class="one-columns">
                                            <small class="input-info">Country</small>
                                        </div>
                                        <div class="two-columns">
                                            <input type="text" id="htlCountry" class="input full-width" value="" readonly>
                                        </div>
                                          <div class="one-columns">
                                            <small class="input-info">Location</small>
                                        </div>
                                        
                                        <div class="four-columns">
                                            <input type="text" id="txtLocation"  list="listLocation" class="input" style="width:75%" value="" onchange="ConfirmLocation()">
                                            <datalist id="listLocation"></datalist>
                                           <%-- <span class="right-side button icon-download blue" onclick="SaveLocation()"></span>--%>
                                        </div>
                                        <div class="new-row one-columns">
                                            <small class="input-info">Zip Code:</small>
                                        </div>
                                        <div class="two-columns" style="margin-left: 50px;">
                                            <input type="text" id="htlZipcode" class="input full-width" value="">
                                        </div>
                                        <div class="one-columns">
                                            <small class="input-info">Langitude:</small>
                                        </div>
                                        <div class="three-columns">
                                            <input type="text" id="htlLangitude" class="input full-width" value="">
                                        </div>
                                        <div class="one-columns">
                                            <small class="input-info">Latitude:</small>
                                        </div>
                                        <div class="three-columns">
                                            <input type="text" id="htlLatitude" class="input full-width" value="">
                                        </div>

                                    </div>
                                    <hr />
                                </div>
                            </fieldset>

                            <fieldset class="wizard-fieldset" style="border: 1px ridge" id="tab_facility">
                                <legend class="legend">Facilities</legend>
                                <div class="button-height">
                                    <div class="columns" style="margin-bottom: 250px;">
                                        <div class="one-columns">
                                            <small class="input-info">Facilities Selected</small>
                                        </div>
                                        <div class="twelve-columns">
                                            <div id="divAddFacilities" class="boxed" style="border-style: groove; border-width: 1px; background-color: white">
                                            </div>
                                            <%--  <textarea name="autoexpanding" id="HtlFacilities" class="input full-width autoexpanding" style="min-height: 50px;" readonly></textarea>--%>
                                        </div>



                                    </div>
                                    <hr />
                                </div>
                            </fieldset>

                            <fieldset class="wizard-fieldset" style="border: 1px ridge">
                                <legend class="legend">Policies</legend>
                                <div class="button-height" id="tab_Plocies">
                                    <div class="columns" style="margin-bottom: 200px;">
                                        <div class="new-row two-columns"><small class="input-info">Pates Allowed: </small></div>
                                        <div class="two-columns">
                                            <select id="selPatesAllowed" class="pates full-width  select  expandable-list ">
                                                <option selected="selected" value="-" disabled>Select</option>
                                                <option value="No">No</option>
                                                <option value="Yes">Yes</option>
                                                <option value="Not Specified">Not Specified</option>
                                            </select>
                                        </div>
                                        <div class="two-columns"><small class="input-info">Liquor Policy: </small></div>
                                        <div class="two-columns">
                                            <select id="selLiquorPolicy" class="full-width select  expandable-list ">
                                                <option selected="selected" value="-" disabled>Select</option>
                                                <option value="Allowed in room">Allowed in room</option>
                                                <option value="Allowed in lobby">Allowed in lobby</option>
                                                <option value="Allowed in restaurant">Allowed in restaurant</option>
                                                <option value="Allowed in dedicated area">Allowed in dedicated area</option>
                                                <option value="Not allowed">Not allowed</option>
                                                <option value="Not Specified">Not Specified</option>
                                            </select>
                                        </div>
                                        <div class="two-columns"><small class="input-info">Smooking: </small></div>
                                        <div class="two-columns">
                                            <select id="selSmooking" class="pates full-width  select  expandable-list ">
                                                <option selected="selected" value="-" disabled>Select</option>
                                                <option value="No">No</option>
                                                <option value="Yes">Yes</option>
                                                <option value="Specific Area">Specific Area</option>
                                                <option value="Not Specified">Not Specified</option>
                                            </select>
                                        </div>
                                        <div class="new-row two-columns"><small class="input-info">Child Age From:</small></div>
                                        <div class="three-columns">
                                            <input type="number" class="input full-width" value="0" id="txtChildAgeFrom">
                                        </div>
                                        <div class="two-columns"><small class="input-info">Child Age To:&nbsp;</small></div>
                                        <div class="three-columns">
                                            <input type="number" class="input full-width" value="0" id="txtChildAgeTo">
                                        </div>
                                        <div class="new-row two-columns"><small class="input-info">Checkin Time:&nbsp;&nbsp;&nbsp;&nbsp;</small></div>
                                        <div class="three-columns">
                                            <input type="time" class="input full-width" value="" id="txtCheckinTime">
                                        </div>
                                        <div class="two-columns"><small class="input-info">Checkout Time:</small></div>
                                        <div class="three-columns">
                                            <input type="time" class="input full-width" value="" id="txtCheckoutTime">
                                        </div>

                                        <div class="new-row two-columns"><small class="input-info">Total Rooms:&nbsp;&nbsp;&nbsp;&nbsp;</small></div>
                                        <div class="three-columns">
                                            <input type="number" class="input full-width" value="0" id="TotalRooms">
                                        </div>
                                        <div class="two-columns"><small class="input-info">Total Floors</small></div>
                                        <div class="three-columns">
                                            <input type="number" class="input full-width " value="0" id="TotalFloors">
                                        </div>

                                        <div class="new-row two-columns"><small class="input-info">Build Year:&nbsp;&nbsp;&nbsp;&nbsp;</small></div>
                                        <div class="three-columns">
                                            <input type="text" class="input full-width" value="" id="BuildYear">
                                        </div>
                                        <div class="two-columns"><small class="input-info">Renovation Year</small></div>
                                        <div class="three-columns">
                                            <input type="text" class="input full-width " value="" id="RenovationYear">
                                        </div>


                                        <div class="new-row two-columns"><small class="input-info">Hotel Group:&nbsp;&nbsp;&nbsp;&nbsp;</small></div>
                                        <div class="three-columns">
                                            <input type="text" class="input full-width" value="" id="txtHotelGroup">
                                        </div>
                                        <div class="two-columns"><small class="input-info">Trip Adviser Link</small></div>
                                        <div class="four-columns">
                                            <input type="text" class="input full-width " value="" id="txtTripAdviserLink">
                                        </div>




                                        <%--<div class="new-row four-columns">
                                                <small class="input-info">Adult Min. Age:</small>
                                               <input type="text" class="input full-width " value="0" id="txtAdultMinAge">
                                          </div>--%>
                                    </div>
                                    <hr />
                                </div>
                            </fieldset>


                            <%-- <fieldset class="wizard-fieldset" style="border:1px ridge">
                                <legend class="legend">Image</legend>
                                <div class="button-height">
                                    <div class="columns" style="margin-bottom: 200px;">
                                          <div class="new-row one-columns">
                                            <small class="input-info">New Image Url</small>
                                        </div>
                                        <div class="six-columns" style="margin-left:90px;">
                                            <input type="text" id="ImgUrl-Add" size="9" class="input full-width" value="" style="display:none;">   
                                            <input type="file" id="ImgHotelUpload" size="9" class="input full-width" onchange="ShowMainImg(event);">                                   
                                        </div>
                                        <div class="new-row one-columns">
                                            <small class="input-info">Hotel Image:</small>
                                        </div>
                                        <div class="eight-columns">
                                             <img src='' id='ImgMain-Add' class='selMainImage-Add' width='70%'>
                                        </div>
                                       
                                    </div>

                                    <hr />
                                </div>
                            </fieldset>--%>

                            <fieldset class="wizard-fieldset" style="border: 1px ridge">
                                <legend class="legend">Images</legend>
                                <div class="button-height" style="min-height: 400px;">
                                    <div class="columns">
                                        <div class="new-row one-columns">
                                            <small class="input-info">Select Images</small>
                                        </div>
                                        <div class="six-columns">
                                            <input type="file" name="images[]" id="images" size="9" class="input full-width" onchange="preview_images();" multiple>
                                            <p class="red">(Please select multiple Images by pressing '<b>CTRL</b> button')</p>
                                        </div>
                                        <div class="two-columns">
                                            <a class="button icon-trash">
                                                <input id="btnSubImageClearAdd" type="button" value="Clear All" onclick="ClearSubImageAdd();" style="background-color: transparent; border: none;" />
                                            </a>
                                        </div>



                                    </div>
                                    <div class="columns" id="image_preview">
                                    </div>
                                </div>
                                <hr />

                            </fieldset>


                            <fieldset class="wizard-fieldset" style="border: 1px ridge">
                                <legend class="legend">Contacts</legend>
                                <div class="button-height" style="margin-bottom: 100px;">
                                    <div id="idHotelContacts">
                                        <div class="columns">
                                            <div class="new-row three-columns">
                                                <b><u>HOTEL CONTACT</u></b><br />
                                                <p>
                                                    Contacts: <b>
                                                        <label id="clickC">1</label></b>
                                                </p>
                                                <%-- <div class="one-columns button icon-plus-round" style="float:left;">--%>
                                                <div class="one-columns button icon-plus-round blue">
                                                    <input id="btnAddHotelContacts" class="HotelContacts" type="button" value="More" onclick="AddHotelContacts()" style="background-color: transparent; border: none;" />
                                                </div>
                                                <%--<div class="one-columns button icon-minus"  style="float:right;">
                                                <input id="btnLessHotelContacts" class="HotelContacts" type="button" value="Less" onclick="LessHotelContacts()" style="background-color: transparent; border: none;" /></div>--%>
                                            </div>
                                            <div class="five-columns">
                                                <input type="text" id="txtContactPerson0" size="9" class="input full-width HotelPerson" value="" placeholder="Contact Person Name*">
                                            </div>
                                            <div class="four-columns">
                                                <input type="email" id="txtEmail0" class="input full-width HotelEmail" value="" placeholder="Email Id*">
                                            </div>
                                            <div class="three-columns">
                                                <input type="text" id="txtContactPhone0" onkeypress='return event.charCode >= 48 && event.charCode <= 57' size="9" class="input full-width HotelPhone" value="" placeholder="Phone Number*">
                                            </div>
                                            <div class="three-columns">
                                                <input type="text" id="txtMobileNo0" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="input full-width HotelMobile" value="" placeholder="Mobile Number*">
                                            </div>
                                            <div class="three-columns">
                                                <input type="text" id="txtFaxNo0" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="input full-width HotelFax" value="" placeholder="Fax Number">
                                            </div>
                                            
                                        </div>

                                    </div>
                                    <br />
                                    <hr />

                                    <div id="idReservationContacts">
                                        <div class="columns">
                                            <div class="new-row three-columns">
                                                <label for="chkSameReservation" class="label">
                                                    <input type="checkbox" id="chkSameReservation" value="1" class="checkbox" onchange="CheckReservation();">
                                                    Same as Above</label>
                                                <b><u>RESERVATION CONTACT</u></b><br />
                                                <p>
                                                    Contacts: <b>
                                                        <label id="clickR">1</label></b>
                                                </p>
                                                <div class="one-columns button icon-plus-round blue">
                                                    <input id="btnAddReservationContacts" class="ReservationContacts" type="button" value="More" onclick="AddReservationContacts()" style="background-color: transparent; border: none;" />
                                                </div>

                                            </div>
                                             <div class="five-columns">
                                                <input type="text" id="txtReservationPerson0" size="9" class="input full-width ReservationPerson" value="" placeholder="Contact Person Name*">
                                            </div>
                                            <div class="four-columns">
                                                <input type="email" id="txtReservationEmail0" class="input full-width ReservationEmail" value="" placeholder="Email Id*">
                                            </div>
                                            <div class="three-columns">
                                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtReservationPhone0" size="9" class="input full-width ReservationPhone" value="" placeholder="Phone Number*">
                                            </div>
                                            <div class="three-columns">
                                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtReservationMobNo0" class="input full-width ReservationMobile" value="" placeholder="Mobile Number*">
                                            </div>
                                            <div class="three-columns">
                                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtReservationFaxNo0" class="input full-width ReservationFax" value="" placeholder="Fax Number">
                                            </div>
                                           
                                        </div>
                                    </div>
                                    <br />
                                    <hr />
                                    <div id="idAccountsContacts">
                                        <div class="columns">
                                            <div class="new-row three-columns">
                                                <label for="chkSameAccounts" class="label">
                                                    <input type="checkbox" id="chkSameAccounts" value="1" class="checkbox" onchange="CheckAccounts();">
                                                    Same as Above</label>
                                                <b><u>ACCOUNTS CONTACT</u></b><br />
                                                <p>
                                                    Contact: <b>
                                                        <label id="clickA">1</label></b>
                                                </p>
                                                <div class="one-columns button icon-plus-round blue">
                                                    <input id="btnAddAccountsContacts" class="AccountsContacts" type="button" value="More" onclick="AddAccountsContacts()" style="background-color: transparent; border: none;" />
                                                </div>

                                            </div>
                                             <div class="five-columns">
                                                <input type="text" id="txtAccountsPerson0" size="9" class="input full-width AccountsPerson" value="" placeholder="Contact Person Name*">
                                            </div>
                                            <div class="four-columns">
                                                <input type="email" id="txtAccountsEmail0" class="input full-width AccountsEmail" value="" placeholder="Email Id*">
                                            </div>
                                            <div class="three-columns">
                                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtAccountsPhone0" size="9" class="input full-width AccountsPhone" value="" placeholder="Phone Number*">
                                            </div>
                                            <div class="three-columns">
                                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtAccountsMobNo0" class="input full-width AccountsMobile" value="" placeholder="Mobile Number*">
                                            </div>
                                            <div class="three-columns">
                                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtAccountsFaxNo0" class="input full-width AccountsFax" value="" placeholder="Fax Number">
                                            </div>
                                           
                                        </div>
                                    </div>
                                </div>
                                <hr />
                                <button type="button" class="button glossy blue-gradient float-right" onclick="SaveHotelMapping();">Add Mapping</button>

                            </fieldset>



                        </form>
                    </div>

















                </div>

                <div class="DivUpdateMapping" style="display: none;">
                    <div class="full-page-wizard">
                        <form class="wizard same-height columns">

                            <h3 class="block-title">Update Hotel Mapping</h3>

                            <fieldset class="wizard-fieldset" style="border:1px ridge">
                                <legend class="legend">Hotel Details</legend>
                                <div class="button-height">
                                    <div class="columns" style="min-height: 300px;">
                                        <div class="one-columns">
                                             <small class="input-info">Hotel Name:&nbsp;&nbsp;&nbsp;&nbsp;</small>
                                        </div>
                                        <div class="five-columns">                                            
                                            <input type="text" id="HtlName1" size="9" class="input full-width" value="">
                                        </div>
                                         <div class="one-columns">
                                              <small class="input-info">Ratings:</small>
                                         </div>
                                        <div class="four-columns">                                           
                                            <%--<input type="text" id="HtlRatings1" class="input full-width" value="">--%>
                                              <div id="HtlRatings1"></div>&nbsp;
                                             <select id="SelStars1" style="height:25px;background-color:#f0efef" tabindex="-1" onchange="fnStarRatings(this.value,'ClickUrTrip','#HtlRatings1',this.id);">
                                                    
                                                    <option value="1">1 Star</option>
                                                    <option value="2">2 Star</option>
                                                    <option value="3">3 Star</option>
                                                    <option value="4">4 Star</option>
                                                    <option value="5">5 Star</option>
                                                    <option value="0">No Ratings</option>
                                                </select>
                                        </div>
                                        <div class="new-row one-columns">
                                            <small class="input-info">Description:&nbsp;&nbsp;&nbsp;&nbsp;</small>
                                        </div>
                                        <div class=" ten-columns">
                                            
                                            <textarea name="autoexpanding" id="HtlDescription1" class="input full-width autoexpanding" style="min-height: 130px; resize: none"></textarea>
                                        </div>
                                         <div class="new-row one-columns">
                                             <small class="input-info">Hotel Adress:</small>
                                         </div>
                                        <div class="ten-columns">
                                            <textarea name="autoexpanding" id="HtlAddress1" class="input full-width autoexpanding" style="resize: none;"></textarea>
                                        </div>
                                          <div class="new-row one-columns">
                                               <small class="input-info">City:</small>
                                          </div>
                                         <div class="two-columns" style="margin-left:72px;">                                           
                                            <input type="text" id="htlCity1" class="input full-width" value="" readonly>
                                        </div>
                                         <div class="one-columns">
                                              <small class="input-info">Country</small>
                                         </div>
                                        <div class="two-columns">                                           
                                            <input type="text" id="htlCountry1" class="input full-width" value="" readonly>
                                        </div>
                                         <div class="one-columns">
                                            <small class="input-info">Location</small>
                                        </div>
                                         <div class="four-columns">
                                            <input type="text" id="txtLocation1"  list="listLocation1" class="input" style="width:75%" value="" onchange="ConfirmLocation()">
                                            <datalist id="listLocation1"></datalist>
                                            <%--<span class="right-side button icon-download blue" onclick="SaveLocation()"></span>--%>
                                        </div>
                                         <div class="new-row one-columns" >
                                             <small class="input-info">Zip Code:</small>
                                         </div>
                                         <div class="two-columns" style="margin-left:50px;">
                                            <input type="text" id="htlZipcode1" class="input full-width" value="">
                                        </div>
                                         <div class="one-columns">
                                              <small class="input-info">Langitude:</small>
                                         </div>
                                         <div class="three-columns">                                           
                                            <input type="text" id="htlLangitude1" class="input full-width" value="">
                                        </div>
                                         <div class="one-columns">
                                              <small class="input-info">Latitude:</small>
                                         </div>
                                        <div class="three-columns">                                           
                                            <input type="text" id="htlLatitude1" class="input full-width" value="">
                                        </div>
                                        
                                    </div>
                                    <hr />
                                </div>
                            </fieldset>

                            <fieldset class="wizard-fieldset" style="border:1px ridge" id="tab_facility1">
                                <legend class="legend">Facilities</legend>
                                <div class="button-height">
                                    <div class="columns" style="margin-bottom: 150px;">                                     
                                        <div class="one-columns">
                                             <small class="input-info">Facilities</small>
                                        </div>
                                         <div class="twelve-columns">                                           
                                            <div id="divUpdateFacilities" class="boxed" style="border-style: groove; border-width: 1px;background-color:white">
                                            </div>
                                        </div>

                                       
                                       
                                    </div>
                                    <hr />
                                </div>
                            </fieldset>

                            <fieldset class="wizard-fieldset" style="border:1px ridge" >
                                <legend class="legend">Policies</legend>
                                <div class="button-height">
                                     <div class="columns" style="margin-bottom:200px;" id="tab_Plocies1">
                                             <div class="new-row two-columns"><small class="input-info">Pates Allowed: </small></div>
                                        <div class="two-columns">                                            
                                            <select id="selPatesAllowed1" class="pates full-width  select  expandable-list ">
                                                <option value="-" disabled>Select</option>
                                                <option value="No">No</option>
                                                <option value="Yes">Yes</option>
                                                <option value="Not Specified">Not Specified</option>
                                            </select>
                                        </div>
                                        <div class="one-columns"> <small class="input-info">Liquor Policy: </small></div>
                                        <div class="two-columns">                                           
                                            <select id="selLiquorPolicy1" class="full-width select  expandable-list ">
                                                <option value="-" disabled>Select</option>
                                                <option value="Allowed in room">Allowed in room</option>
                                                <option value="Allowed in lobby">Allowed in lobby</option>
                                                <option value="Allowed in restaurant">Allowed in restaurant</option>
                                                <option value="Allowed in dedicated area">Allowed in dedicated area</option>
                                                <option value="Not allowed">Not allowed</option>
                                                <option value="Not Specified">Not Specified</option>
                                            </select>
                                        </div>
                                          <div class="one-columns"> <small class="input-info">Smooking Allowed: </small></div>
                                        <div class="two-columns">                                           
                                            <select id="selSmooking1" class="pates full-width  select  expandable-list ">
                                                <option value="-" disabled>Select</option>
                                                <option value="No">No</option>
                                                <option value="Yes">Yes</option>
                                                <option value="Specific Area">Specific Area</option>
                                                <option value="Not Specified">Not Specified</option>
                                            </select>
                                        </div>
                                         <div class="new-row two-columns"> <small class="input-info">Child Age From:</small></div>
                                        <div class="three-columns">                                           
                                            <input type="number" class="input full-width" value="0" id="txtChildAgeFrom1">
                                        </div>
                                         <div class="two-columns"><small class="input-info">Child Age To:&nbsp;</small></div>
                                        <div class="three-columns">                                            
                                            <input type="number" class="input full-width" value="0" id="txtChildAgeTo1">
                                        </div>
                                         <div class="new-row two-columns"><small class="input-info">Checkin Time:&nbsp;&nbsp;&nbsp;&nbsp;</small></div>
                                         <div class="three-columns">                                            
                                            <input type="time" class="input full-width" value="" id="txtCheckinTime1">
                                        </div>
                                        <div class="two-columns"><small class="input-info">Checkout Time:</small></div>
                                        <div class="three-columns">                                            
                                            <input type="time" class="input full-width" value="" id="txtCheckoutTime1">
                                        </div>                                     
                                        
                                       
                                         <div class="new-row two-columns"><small class="input-info">Total Rooms:&nbsp;&nbsp;&nbsp;&nbsp;</small></div>
                                         <div class="three-columns">                                            
                                            <input type="number" class="input full-width" value="0" id="TotalRooms1">
                                        </div>
                                         <div class="two-columns"><small class="input-info">Total Floors</small></div>
                                        <div class="three-columns">                                            
                                            <input type="number" class="input full-width " value="0" id="TotalFloors1">
                                        </div>

                                          <div class="new-row two-columns"><small class="input-info">Build Year:&nbsp;&nbsp;&nbsp;&nbsp;</small></div>
                                         <div class="three-columns">                                            
                                            <input type="text" class="input full-width" value="" id="BuildYear1">
                                        </div>
                                         <div class="two-columns"><small class="input-info">Renovation Year</small></div>
                                        <div class="three-columns">                                            
                                            <input type="text" class="input full-width " value="" id="RenovationYear1">
                                        </div>

                                          <div class="new-row two-columns"><small class="input-info">Hotel Group:&nbsp;&nbsp;&nbsp;&nbsp;</small></div>
                                         <div class="three-columns">                                            
                                            <input type="text" class="input full-width" value="" id="txtHotelGroup1">
                                        </div>
                                         <div class="two-columns"><small class="input-info">Trip Adviser Link</small></div>
                                        <div class="four-columns">                                            
                                            <input type="text" class="input full-width " value="" id="txtTripAdviserLink1">
                                        </div>                                  
                                    </div>
                                  
                                      <hr />
                                </div>
                            </fieldset>

                           <%-- <fieldset class="wizard-fieldset" style="border:1px ridge">
                                <legend class="legend">Image</legend>
                                <div class="button-height">
                                    <div class="columns" style="margin-bottom: 200px;">
                                          <div class="new-row one-columns">
                                            <small class="input-info">New Image Url</small>
                                        </div>
                                        <div class="six-columns" style="margin-left:90px;">
                                            <input type="text" id="ImgUrl" size="9" class="input full-width" value="" onkeyup="ShowNewImg();">
                                        </div>
                                        <div class="new-row one-columns">
                                            <small class="input-info">Hotel Image:</small>
                                        </div>
                                        <div class="eight-columns">
                                            <img src='' id='ImgMain' class='selMainImage' width='70%'>
                                        </div>
                                       
                                    </div>

                                    <hr />
                                </div>
                            </fieldset>--%>

                             <fieldset class="wizard-fieldset" style="border:1px ridge">
                              <legend class="legend">Images</legend>
                              <div class="button-height" style="min-height:400px;">
                                  <div class="columns" >
                                       <div class="new-row one-columns">
                                            <small class="input-info">Select Images</small>
                                       </div>
                                      <div class="six-columns">
                                            <input type="file"  id="images1" size="9" class="input full-width" onchange="preview_images1();" multiple> 
                                          <p class="red">(Please select multiple Images by pressing '<b>CTRL</b> button')</p> 
                                     </div>
                                      <div class="two-columns">
                                            <a class="button icon-trash">
                                                  <input id="btnSubImageClearAdd1" type="button" value="Clear All" onclick="ClearSubImage1();" style="background-color: transparent; border: none;" />
                                            </a>                                             
                                       </div>
                                    
                                     

                                  </div>
                                   <div class="columns" id="image_preview1">
                                      </div>
                              </div>
                              <hr />
                            
                          </fieldset>
                          

                            <fieldset class="wizard-fieldset" style="border: 1px ridge">
                                <legend class="legend">Contacts</legend>
                                <div class="button-height" style="margin-bottom: 100px;">
                                   <div id="idHotelContacts1">
                                       <div class="columns">
                                           <div class="new-row three-columns">

                                               <b><u>HOTEL CONTACT</u></b><br />
                                               <p>Contacts: <b>
                                                   <label id="clickC1">1</label></b></p>
                                               <%-- <div class="one-columns button icon-plus-round" style="float:left;">--%>
                                               <div class="one-columns button icon-plus-round blue">
                                                   <input id="btnAddHotelContacts1" class="HotelContacts" type="button" value="More" onclick="AddHotelContacts1()" style="background-color: transparent; border: none;" />
                                               </div>
                                               <%--<div class="one-columns button icon-minus"  style="float:right;">
                                                <input id="btnLessHotelContacts" class="HotelContacts" type="button" value="Less" onclick="LessHotelContacts()" style="background-color: transparent; border: none;" /></div>--%>
                                           </div>
                                            <div class="five-columns">
                                               <input type="text" id="txtContactPersonU0" size="9" class="input full-width HotelPerson1" value="" placeholder="Contact Person Name">
                                           </div>
                                           <div class="four-columns">
                                               <input type="email" id="txtEmailU0" class="input full-width HotelEmail1" value="" placeholder="Email Id">
                                           </div>
                                           <div class="three-columns">
                                               <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtContactPhoneU0" size="9" class="input full-width HotelPhone1" value="" placeholder="Phone Number">
                                           </div>
                                           <div class="three-columns">
                                               <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtMobileNoU0" class="input full-width HotelMobile1" value="" placeholder="Mobile Number">
                                           </div>
                                           <div class="three-columns">
                                               <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtFaxNoU0" class="input full-width HotelFax1" value="" placeholder="Fax Number">
                                           </div>
                                          
                                       </div>
                                      
                                   </div><br />
                                   <hr />
                                   <div id="idReservationContacts1">
                                       <div class="columns">
                                           <div class="new-row three-columns">
                                                <label for="chkSameReservationU" class="label">
                                                    <input type="checkbox" id="chkSameReservationU" value="1" class="checkbox" onchange="CheckReservationU();">
                                                    Same as Above</label>
                                               <b><u>RESERVATION CONTACT</u></b><br />
                                               <p>
                                                   Contacts: <b>
                                                       <label id="clickR1">1</label></b>
                                               </p>
                                               <div class="one-columns button icon-plus-round blue">
                                                   <input id="btnAddReservationContacts1" class="ReservationContacts" type="button" value="More" onclick="AddReservationContacts1()" style="background-color: transparent; border: none;" />
                                               </div>

                                           </div>
                                           <div class="five-columns">
                                               <input type="text" id="txtReservationPersonU0" size="9" class="input full-width ReservationPerson1" value="" placeholder="Contact Person Name">
                                           </div>
                                           <div class="four-columns">
                                               <input type="email" id="txtReservationEmailU0" class="input full-width ReservationEmail1" value="" placeholder="Email Id">
                                           </div>
                                           <div class="three-columns">
                                               <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtReservationPhoneU0" size="9" class="input full-width ReservationPhone1" value="" placeholder="Phone Number">
                                           </div>
                                           <div class="three-columns">
                                               <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtReservationMobNoU0" class="input full-width ReservationMobile1" value="" placeholder="Mobile Number">
                                           </div>
                                           <div class="three-columns">
                                               <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtReservationFaxNoU0" class="input full-width ReservationFax1" value="" placeholder="Fax Number">
                                           </div>
                                           
                                       </div>
                                   </div><br />
                                   <hr />
                                   <div id="idAccountsContacts1">
                                       <div class="columns">
                                           <div class="new-row three-columns">
                                               <label for="chkSameAccountsU" class="label">
                                                    <input type="checkbox" id="chkSameAccountsU" value="1" class="checkbox" onchange="CheckAccountsU();">
                                                    Same as Above</label>
                                               <b><u>ACCOUNTS CONTACT</u></b><br />
                                               <p>
                                                   Contact: <b>
                                                       <label id="clickA1">1</label></b>
                                               </p>
                                               <div class="one-columns button icon-plus-round blue">
                                                   <input id="btnAddAccountsContacts1" class="AccountsContacts1" type="button" value="More" onclick="AddAccountsContacts1()" style="background-color: transparent; border: none;" />
                                               </div>

                                           </div>
                                           <div class="five-columns">
                                               <input type="text" id="txtAccountsPersonU0" size="9" class="input full-width AccountsPerson1" value="" placeholder="Contact Person Name">
                                           </div>
                                           <div class="four-columns">
                                               <input type="email" id="txtAccountsEmailU0" class="input full-width AccountsEmail1" value="" placeholder="Email Id">
                                           </div>
                                           <div class="three-columns">
                                               <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtAccountsPhoneU0" size="9" class="input full-width AccountsPhone1" value="" placeholder="Phone Number">
                                           </div>
                                           <div class="three-columns">
                                               <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtAccountsMobNoU0" class="input full-width AccountsMobile1" value="" placeholder="Mobile Number">
                                           </div>
                                           <div class="three-columns">
                                               <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtAccountsFaxNoU0" class="input full-width AccountsFax1" value="" placeholder="Fax Number">
                                           </div>
                                           
                                       </div>
                                   </div>
                               </div>
                                <hr />
                                <button type="button" class="button glossy blue-gradient float-right" onclick="UpdateHotelMapping();">Update Mapping</button>

                            </fieldset>

                        </form>
                    </div>

                </div>

            <input value="<%=DescriptionHotel %>" id="HotelDescription" hidden="hidden"/>
        </div>
	</section>
	<!-- End main content -->



	<!-- JavaScript at the bottom for fast page loading -->
	<!-- Scripts -->
	<script src="js/libs/jquery-1.10.2.min.js"></script>
	<script src="js/setup.js"></script>

	<!-- Template functions -->
	<script src="js/developr.input.js"></script>
	<script src="js/developr.navigable.js"></script>
	<script src="js/developr.notify.js"></script>
	<script src="js/developr.scroll.js"></script>
	<script src="js/developr.tooltip.js"></script>
	<script src="js/developr.table.js"></script>
    <script src="js/developr.accordions.js"></script>
    <script src="js/developr.wizard.js"></script>

	<!-- Plugins -->
	<script src="js/libs/jquery.tablesorter.min.js"></script>
	<script src="js/libs/DataTables/jquery.dataTables.min.js"></script>

	<script>

	    // Call template init (optional, but faster if called manually)
	    $.template.init();

	    // Table sort - DataTables
	    var table = $('#sorting-advanced');
	    table.dataTable({
	        'aoColumnDefs': [
				{ 'bSortable': false, 'aTargets': [0, 5] }
	        ],
	        'sPaginationType': 'full_numbers',
	        'sDom': '<"dataTables_header"lfr>t<"dataTables_footer"ip>',
	        'fnInitComplete': function (oSettings) {
	            // Style length select
	            table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
	            tableStyled = true;
	        }
	    });

	    // Table sort - styled
	    $('#sorting-example1').tablesorter({
	        headers: {
	            0: { sorter: false },
	            5: { sorter: false }
	        }
	    }).on('click', 'tbody td', function (event) {
	        // Do not process if something else has been clicked
	        if (event.target !== this) {
	            return;
	        }

	        var tr = $(this).parent(),
				row = tr.next('.row-drop'),
				rows;

	        // If click on a special row
	        if (tr.hasClass('row-drop')) {
	            return;
	        }

	        // If there is already a special row
	        if (row.length > 0) {
	            // Un-style row
	            tr.children().removeClass('anthracite-gradient glossy');

	            // Remove row
	            row.remove();

	            return;
	        }

	        // Remove existing special rows
	        rows = tr.siblings('.row-drop');
	        if (rows.length > 0) {
	            // Un-style previous rows
	            rows.prev().children().removeClass('anthracite-gradient glossy');

	            // Remove rows
	            rows.remove();
	        }

	        // Style row
	        tr.children().addClass('anthracite-gradient glossy');

	        // Add fake row
	        $('<tr class="row-drop">' +
				'<td colspan="' + tr.children().length + '">' +
					'<div class="float-right">' +
						'<button type="submit" class="button glossy mid-margin-right">' +
							'<span class="button-icon"><span class="icon-mail"></span></span>' +
							'Send mail' +
						'</button>' +
						'<button type="submit" class="button glossy">' +
							'<span class="button-icon red-gradient"><span class="icon-cross"></span></span>' +
							'Remove' +
						'</button>' +
					'</div>' +
					'<strong>Name:</strong> John Doe<br>' +
					'<strong>Account:</strong> admin<br>' +
					'<strong>Last connect:</strong> 05-07-2011<br>' +
					'<strong>Email:</strong> john@doe.com' +
				'</td>' +
			'</tr>').insertAfter(tr);

	    }).on('sortStart', function () {
	        var rows = $(this).find('.row-drop');
	        if (rows.length > 0) {
	            // Un-style previous rows
	            rows.prev().children().removeClass('anthracite-gradient glossy');

	            // Remove rows
	            rows.remove();
	        }
	    });

	    // Table sort - simple
	    $('#sorting-example2').tablesorter({
	        headers: {
	            5: { sorter: false }
	        }
	    });

	</script>

    <script>

        $(document).ready(function () {
            // Elements
            var form = $('.wizard'),

				// If layout is centered
				centered;

            // Handle resizing (mostly for debugging)
            function handleWizardResize() {
                centerWizard(false);
            };

            // Register and first call
            $(window).on('normalized-resize', handleWizardResize);

            /*
			 * Center function
			 * @param boolean animate whether or not to animate the position change
			 * @return void
			 */
            function centerWizard(animate) {
                form[animate ? 'animate' : 'css']({ marginTop: Math.max(0, Math.round(($.template.viewportHeight - 30 - form.outerHeight()) / 2)) + 'px' });
            };

            // Initial vertical adjust
            centerWizard(false);

            // Refresh position on change step
            form.on('wizardchange', function () { centerWizard(true); });

            // Validation
            if ($.validationEngine) {
                form.validationEngine();
            }
        });

	</script>
</asp:Content>
