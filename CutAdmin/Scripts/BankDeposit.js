﻿
var arrAdminCredit = new Array();
var arrCreditLog = new Array();
var arrBankDeposit = new Array();
var Nex = 0;
var Pre = 10;
var Limit = 0;
var href;

$(document).ready(function () {
    $("#datepicker1").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#datepicker2").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    href = "#Bank";
    $('#tabs li a').click(function (e) {
        e.preventDefault()
        href = $(this).attr('href');
        //ClearAllTabs();
    })

    LoadAllBanks();
    GetDepositDetails();

});

function LoadAllBanks() {

    $.ajax({
        type: "POST",
        url: "../handler/AgentHandler.asmx/LoadAllBanks",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {

                BankList = obj.BankName;


                //************************************************************* Bank List
                var ul = '';
                $("#Select_BankAccount").empty();

                ul += '<option selected="selected" value="-">Select Bank</option>';
                if (BankList.length > 0) {
                    for (var i = 0; i < BankList.length; i++) {
                        ul += '<option  value="' + BankList[i].BankName + '-' + BankList[i].AccountNo + '">' + BankList[i].BankName + '-' + BankList[i].AccountNo + '</option>';
                    }

                }
                $('#Select_BankAccount').append(ul);

            }
        }
    });
}

function ValidateDepositUpdates() {
    debugger;
    var isValid = true;

    if (href == "#Bank") {

        if ($('#Select_TypeOfCash').val() == "-") {
            isValid = false;
            Success("Select Type of Transaction");
            return
        }

        if ($('#Select_BankAccount').val() == "-") {
            isValid = false;
            Success("Select Bank");
            //$("#lblerr_Select_BankAccount").css("display", "");
            return
        }

        if ($('#txt_AmountGiven').val() == "") {
            isValid = false;
            Success("Enter Amount");
            //$("#lblerr_txt_AmountGiven").css("display", "");
            return
        }
        if ($('#dteBankDeposite').val() == "") {
            isValid = false;
            Success("Select Date");
            // $("#lblerr_txt_DepositDate").css("display", "");
            return
        }

        if (($('#txt_BankTransactionNumber').val() == "") && ($('#Select_TypeOfCash option:selected').val() != "Cash")) {
            isValid = false;
            Success("Enter Receipt Number/Transaction ID");
            //$("#lblerr_txt_TransactionNumber").css("display", "");
            return
        }

    }

    if (href == "#Cash") {
        //if ($('#Select_Branch option:selected').val() == "-") {
        //    isValid = false;
        //    $("#lblerr_Select_Branch").css("display", "");
        //}

        if ($("#txt_Ammount").val() == "") {
            isValid = false;
            Success("Enter Amount");
            return
            // $("#lblerr_txt_Ammount").css("display", "");
        }
        if ($('#txt_Given_To').val() == "") {
            isValid = false;
            Success("Enter Given To");
            return
            // $("#lblerr_txt_Given_To").css("display", "");
        }
        if ($('#dteCashDeposit').val() == "") {
            isValid = false;
            Success("Select Date");
            return
            //$("#lblerr_txt_Cash_Date").css("display", "");

        }

    }
    return isValid;
}


function submit() {
    debugger;
    //$("label").css("display", "none");
    var bValue = ValidateDepositUpdates();
    //var Datee = "";
    if (bValue == true) {
        if (href == "#Bank") {

            // sending date in transaction id, to not to change parameter type in handlers and c#
            var BankWAct = ($('#Select_BankAccount ').val()).split('-');
            //var BankWActt = ($('#Select_BankAccount option:selected').val()).split('-');

            // TypeOfDeposit = $('#Select_TypeOfCash option:selected').val();
            TypeOfDeposit = $('#Select_TypeOfCash ').val();
            SelectBankName = BankWAct[0];
            AccountNumber = BankWAct[1];
            DepositAmount = $('#txt_AmountGiven').val();
            DepositDate = $('#dteBankDeposite').val();
            txtRemarks = $("#txt_BankRemarks").val();

            SelectTypeOfCash = "N/A";
            txtEmployeeName = "N/A";
            txtReceiptNumber = $('#txt_BankTransactionNumber').val();
            txtChequeDDNumber = "N/A";
            txtChequeDDNumber = "N/A";
            txtChequeDrwanOnBank = "N/A";
            SelectTypeOfCheque = "N/A";

        }
        if (href == "#Cash") {


            SelectBankName = "";                          //$('#Select_Branch option:selected').val();
            TypeOfDeposit = "Cash";
            DepositAmount = $('#txt_Ammount').val();
            txtEmployeeName = $('#txt_Given_To').val();
            DepositDate = $('#dteCashDeposit').val();
            txtReceiptNumber = $('#txt_ReceiptNumberCheque').val();
            txtRemarks = $("#txt_CashRemarks").val();

            SelectTypeOfCash = "N/A";
            SelectTypeOfCheque = "N/A";
            AccountNumber = "";
            txtChequeDDNumber = "N/A";
            txtTransactionId = "N/A";
            txtChequeDrwanOnBank = "N/A";


        }

        $.ajax({
            url: "../handler/AgentHandler.asmx/InsertDeposit",
            type: "POST",
            data: '{"BankName":"' + SelectBankName + '","AccountNumber":"' + AccountNumber + '","AmountDeposit":"' + DepositAmount + '","Remarks":"' + txtRemarks + '","TypeOfDeposit":"' + TypeOfDeposit + '","TypeOfCash":"' + SelectTypeOfCash + '","TypeOfCheque":"' + SelectTypeOfCheque + '","EmployeeName":"' + txtEmployeeName + '","ReceiptNumber":"' + txtReceiptNumber + '","ChequeDDNumber":"' + txtChequeDDNumber + '","ChequeDrawn":"' + txtChequeDrwanOnBank + '","DepositDate":"' + DepositDate + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.Session != 1) {
                    //$('#SpnMessege').text("Session Expired!");
                    //$('#ModelMessege').modal('show')
                    Success("Session Expired!");
                }
                if (result.retCode == 1) {
                    debugger;
                    //$('#SpnMessege').text("Your deposit details has been submitted successfully!The Credit changes reflect after Admin approval.");
                    //$('#ModelMessege').modal('show')
                    Success("Your deposit details has been submitted successfully!The Credit changes reflect after Admin approval.");
                    setTimeout(function () {
                        //window.location.href = "Deposite.aspx"
                        window.location.reload();
                    }, 2000);

                    //GetLastUpdateCreditDetails();
                    //  ClearAllTabs();
                    //$('#txt_MobileNumber').val("");
                    //$('#datepicker_TransferDate').val("");
                    //$("#txt_Remarks").val("");
                }
                if (result.retCode == 0) {
                    //$('#SpnMessege').text("Something went wrong while processing your request! Please try again.");
                    //$('#ModelMessege').modal('show')
                    Success("Something went wrong while processing your request! Please try again.");
                }
            },
            error: function () {
                //$('#SpnMessege').text('An error occured while processing your request! Please try again.');
                //$('#ModelMessege').modal('show')
                Success('An error occured while processing your request! Please try again.');
            }
        });
    }
}

function GetDepositDetails() {
    $("#tblCreditDetails").dataTable().fnClearTable();
    $("#tblCreditDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "../handler/AccountHandler.asmx/GetCreditInformation",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrAdminCredit = result.AdminCredit;
                arrCreditLog = result.CreditLog;
                arrBankDeposit = result.BankDeposit;

                var tr = '';
                if (arrBankDeposit.length == 0) {
                    $("#tblCreditDetails").dataTable({
                        bSort: false, sPaginationType: 'full_numbers',
                    });
                }
                else {
                    var PageCount = result.Count;
                    for (var i = 0; i < arrBankDeposit.length; i++) {
                        tr += "<tr>";
                        tr += "<td style='text-align:center'>" + (i + 1) + "</td>";
                        tr += "<td style='text-align:center'><i class='" + CurrencyClass + "'></i>&nbsp" + numberWithCommas(arrBankDeposit[i].DepositAmount) + "</td>";
                        var creatdt = (arrBankDeposit[i].CreationDate).split(" ");
                        tr += "<td style='text-align:center'>" + creatdt[0] + "</td>";
                        tr += "<td style='text-align:center'>" + arrBankDeposit[i].Typeofdeposit + "</td>";
                        if (arrBankDeposit[i].BankName == "") {
                            tr += "<td style='text-align:center'>" + " " + "</td>";
                        }
                        else {
                            tr += "<td style='text-align:center'>" + (arrBankDeposit[i].BankName) + "-" + (arrBankDeposit[i].AccountNumber) + "</td>";
                        }

                        tr += "<td style='text-align:center'>" + arrBankDeposit[i].ReceiptNumber + "</td>";

                        //var Status = (arrBankDeposit[i].ApproveFlag == "True") ? "Approved" : "Pending";
                        var Status = ""
                        if (arrBankDeposit[i].ApproveFlag == true) {
                            Status = "Approved";
                            tr += "<td style='text-align:center'>" + Status + "</td>";
                        }
                        else {
                            Status = "Pending";
                            tr += "<td style='text-align:center; cursor:pointer'><a href='#' title='Delete' onclick='DeleteRecord(" + arrBankDeposit[i].sId + ")'>" + Status + "</a></td>";
                        }

                        var AppDate = arrBankDeposit[i].Updatedate;
                        if (AppDate == "") {
                            tr += "<td style='text-align:center'>" + " " + "</td>";
                        }
                        else {
                            var ApproveDate = AppDate.split(" ")
                            tr += "<td style='text-align:center'>" + ApproveDate[0] + "</td>";
                        }

                        tr += "</tr>";
                    }
                    //$('#spAvailableCredit').append("<i class=\"fa fa-inr\"></i> " + arrAdminCredit[0].AvailableCredit);


                    $("#tblCreditDetails tbody").append(tr);
                    $("#tblCreditDetails").dataTable({
                        bSort: false, sPaginationType: 'full_numbers',
                    });
                    $("#tblCreditDetails").removeAttr("style");

                }

            }

            if (result.retCode == 0) {

                Success('Something Went Wrong');
            }

            if (result.retCode == 2) {
                $("#tblCreditDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });

            }
        },
        error: function () {
            Success("An error occured while loading credit information")
        }
    });
}


function DeleteRecord(sId) {

    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to delete this record ?</p>', function () {
        $.ajax({
            url: "../handler/AccountHandler.asmx/Deleterecord",
            type: "post",
            data: '{"Id":"' + sId + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                if (result.retCode == 1) {
                    Success("Record has been deleted successfully!");
                    window.location.reload();
                } else if (result.retCode == 0) {
                    Success("Something went wrong while processing your request! Please try again.");
                    setTimeout(function () {
                        window.location.reload();
                    }, 500)
                }
            },
            error: function () {
                Success('Error occured while processing your request! Please try again.');
                setTimeout(function () {
                    window.location.reload();
                }, 500)
            }
        });

    }, function () {
        $('#modals').remove();
    });
}

function numberWithCommas(x) {


    x = x.toString();
    var afterPoint = '';
    if (x.indexOf('.') > 0)
        afterPoint = x.substring(x.indexOf('.'), x.length);
    x = Math.floor(x);
    x = x.toString();
    var lastThree = x.substring(x.length - 3);
    var otherNumbers = x.substring(0, x.length - 3);
    if (otherNumbers != '')
        lastThree = ',' + lastThree;
    var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
    return res;



    // var parts = n.toString().split(".");
    // return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
}