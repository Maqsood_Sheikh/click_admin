﻿$(document).ready(function () {
    VisaActivityMails();
    //OTBActivityMails();
    //HotelActivityMails();

})

function VisaActivityMails() {

    $("#tbl_VisaDetails").dataTable().fnClearTable();
    $("#tbl_VisaDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "../Handler/MailsHandler.asmx/GetVisaActivityMails",
        data: '{ "Type": "Visa"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var VisaMailsList = result.Arr;
                var MailsList = result.MailsList;
                var tRow = '';

                var Email = '';
                var CcMail = '';
                var ErrorMessage = "";
                var Type = 'Visa';
                for (var i = 0; i < VisaMailsList.length; i++) {

                    try {
                        var List = $.grep(MailsList, function (p) { return p.Activity == VisaMailsList[i].Activity; })
                        .map(function (p) { return p; });

                        if (List.length != 0) {
                            Email = List[0].Email;
                            CcMail = List[0].CcMail;
                            ErrorMessage = List[0].ErroMessage;
                        }
                        else {
                            Email = "-";
                            CcMail = '-';
                            ErrorMessage = '';
                        }
                    }
                    catch (ex) {

                    }

                    tRow += '<tr>';
                    tRow += '<td>' + VisaMailsList[i].Activity + '</td>';
                    tRow += '<td>' + Email + '</td>';
                    tRow += '<td>' + CcMail + '</td>';
                    tRow += '<td class="align-center"><a href="#" data-toggle="modal" data-target="#VisaModal"  onclick="VisaModalFunc(\'' + VisaMailsList[i].Activity + '\',\'' + Email + '\',\'' + CcMail + '\',\'' + Type + '\',\'' + ErrorMessage + '\'); return false" class="button" title="Update"><span class="icon-publish "></span></a></td>';
                    tRow += '</tr>                                                ';
                }
                $("#tbl_VisaDetails tbody").html(tRow);
                $("#tbl_VisaDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                document.getElementById("tbl_VisaDetails").removeAttribute("style")
            }
        },
        error: function () {
        }
    });
}

function OTBActivityMails() {
    $("#tbl_OTBDetails").dataTable().fnClearTable();
    $("#tbl_OTBDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "./Handler/MailsHandler.asmx/GetOtbActivityMails",
        data: '{ "Type": "OTB"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var OtbMailsList = result.Arr;
                var MailsList = result.MailsList;
                var tRow = '';
                var Email = '';
                var CcMail = '';
                var ErrorMessage = "";
                var Type = 'OTB';
                for (var i = 0; i < OtbMailsList.length; i++) {

                    try {
                        var List = $.grep(MailsList, function (p) { return p.Activity == OtbMailsList[i].Activity; })
                        .map(function (p) { return p; });

                        if (List.length != 0) {
                            Email = List[0].Email;
                            CcMail = List[0].CcMail;
                            ErrorMessage = List[0].ErroMessage;
                        }
                        else {
                            Email = "-";
                            CcMail = '-';
                            ErrorMessage = '';
                        }
                    }
                    catch (ex) {

                    }

                    tRow += '<tr>';
                    tRow += '<td>' + OtbMailsList[i].Activity + '</td>';
                    tRow += '<td>' + Email + '</td>';
                    tRow += '<td>' + CcMail + '</td>';
                    tRow += '<td class="align-center"><a href="#" data-toggle="modal" data-target="#VisaModal"  onclick="VisaModalFunc(\'' + OtbMailsList[i].Activity + '\',\'' + Email + '\',\'' + CcMail + '\',\'' + Type + '\',\'' + ErrorMessage + '\'); return false" class="button" title="Update"><span class="icon-publish "></span></a></td>';
                    tRow += '</tr>                                                ';
                }
                $("#tbl_OTBDetails tbody").html(tRow);
                $("#tbl_OTBDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                document.getElementById("tbl_OTBDetails").removeAttribute("style")
            }
        },
        error: function () {
        }
    });
}


function HotelActivityMails() {
    $("#tbl_HotelDetails").dataTable().fnClearTable();
    $("#tbl_HotelDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "./Handler/MailsHandler.asmx/GetHotelActivityMails",
        data: '{ "Type": "Hotel"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var HotelMailsList = result.Arr;
                var MailsList = result.MailsList;
                var tRow = '';
                var Email = '';
                var CcMail = '';
                var ErrorMessage = "";
                var Type = 'Hotel';
                for (var i = 0; i < HotelMailsList.length; i++) {

                    try {
                        var List = $.grep(MailsList, function (p) { return p.Activity == HotelMailsList[i].Activity; })
                        .map(function (p) { return p; });

                        if (List.length != 0) {
                            Email = List[0].Email;
                            CcMail = List[0].CcMail;
                            ErrorMessage = List[0].ErroMessage;
                        }
                        else {
                            Email = "-";
                            CcMail = '-';
                            ErrorMessage = '';
                        }
                    }
                    catch (ex) {

                    }

                    tRow += '<tr>';
                    tRow += '<td>' + HotelMailsList[i].Activity + '</td>';
                    tRow += '<td>' + Email + '</td>';
                    tRow += '<td>' + CcMail + '</td>';
                    tRow += '<td class="align-center"><a href="#" data-toggle="modal" data-target="#VisaModal"  onclick="VisaModalFunc(\'' + HotelMailsList[i].Activity + '\',\'' + Email + '\',\'' + CcMail + '\',\'' + Type + '\',\'' + ErrorMessage + '\'); return false" class="button" title="Update"><span class="icon-publish "></span></a></td>';
                    tRow += '</tr>                                                ';
                }
                $("#tbl_HotelDetails tbody").html(tRow);
                $("#tbl_HotelDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                document.getElementById("tbl_HotelDetails").removeAttribute("style")
            }
        },
        error: function () {
        }
    });
}

var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
function AddMails() {
    debugger
    var bValid = true;
    var Activity;
    var Meassage;
    var MailId;
    var Type;
    if ($("#lst_Visa").hasClass("active") == true) {
        Meassage = $("#VisaMessage").val()
        Activity = $("#Sel_Activity").val();
        MailId = $("#txt_MailId").val();
        Type = "Visa"
        if (!emailReg.test(MailId)) {
            bValid = false;
            $("#txt_MailId").focus();
            Success("Please Insert Valid Mail Id")
        }
    }
    else if ($("#lst_Hotel").hasClass("active") == true) {
        Meassage = $("#HotelMessage").val()
        Activity = $("#Sel_HotelActivity").val();
        MailId = $("#txt_MailId").val();
        Type = "Hotel"
        if (!emailReg.test(MailId)) {
            bValid = false;
            $("#txt_MailId").focus();
            Success("Please Insert Valid Mail Id")
        }
    }
    else if ($("#lst_Otb").hasClass("active") == true) {
        Meassage = $("#OTBMessage").val()
        Activity = $("#Sel_AirLineInMail").val();
        MailId = $("#txt_MailId").val();
        Type = "OTB"
        if (!emailReg.test(MailId)) {
            bValid = false;
            $("#txt_MailId").focus();
            Success("Please Insert Valid Mail Id")
        }
    }
    var CcMails = "";
    var Mails = document.getElementsByClassName('email_name'), i;
    for (i = 0; i < Mails.length; i += 1) {
        if (i != (Mails.length - 1)) {

            if (!emailReg.test(Mails[i].textContent)) {
                bValid = false;
                $("CcMailList-input text-left").focus();
                Success("Please Insert Valid Mail Id")
            }
            else {
                if (!emailReg.test(Mails[i].textContent)) {
                    bValid = false;
                    $("CcMailList-input text-left").focus();
                    Success("Please Insert Valid Mail Id")
                }
                else {
                    CcMails += Mails[i].textContent + ";";
                }

            }
        }
        else {
            CcMails += Mails[i].textContent;
        }

        var Message = $("#Message")

    }
    if (bValid == true) {
        $.ajax({
            type: "POST",
            url: "./Handler/MailsHandler.asmx/UpdateVisaMails",
            data: '{ "Activity": "' + Activity + '","Type": "' + Type + '","MailsId": "' + MailId + '","CcMails": "' + CcMails + '","BCcMail": "","Message": "' + Meassage + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                debugger
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Mail Updated Successfully..")

                    $("CcMailList").empty();
                    // $('#VisaModal').modal('hide');

                    //$("#VisaModal .close").click()
                    if (Type == "Visa") {
                        $("#Sel_Activity").val("-");
                        $("#txt_MailId").val("");
                        VisaActivityMails();
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                    else if (Type == "OTB") {

                        $("#Sel_AirLineInMail").val("-");
                        $("#txt_AirlinesMailId").val("");
                        OTBActivityMails();
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                    else if (Type == "Hotel") {
                        $("#Sel_HotelActivity").val("-");
                        $("#txt_HotelMailId").val("");
                        HotelActivityMails();
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                }
                if (result.retCode == 0) {

                }
            },
            error: function () {
            }
        });
    }

}
function RemoveLi(link) {
    link.remove();
}
function MailsFor(id) {
    debugger
    if (id == "Visa") {
        $('#Visa').show();
        $('#OTB').hide();
        $('#Hotel').hide();
    }
    else if (id == "Otb") {
        $('#Visa').hide();
        $('#OTB').show();
        $('#Hotel').hide();
        OTBActivityMails();

    }
    else if (id == "Hotel") {
        $('#Visa').hide();
        $('#OTB').hide();
        $('#Hotel').show();
        HotelActivityMails();
    }

}


function RemoveLi(link) {
    link.remove();
}
var Act = "";
function VisaModalFunc(Activity, Email, CcMail, type, Msg) {
    $.modal({
        content: '<div class="columns" id="VisaModal">' +
                    '<div class="twelve-columns twelve-columns-mobile" id="divForVisa">' +
                        '<label>Activity: </label><br/> ' +
                        '<div class="input full-width">' +
                            '<input name="prompt-value" id="Sel_Activity" value="" class="input-unstyled full-width"  placeholder="Activity Name" type="text">' +
                        '</div>' +
                    '</div>' +
                    '<div class="twelve-columns twelve-columns-mobile">' +
                        '<label>Mail Id: </label><br/> ' +
                        '<div class="input full-width">' +
                            '<input name="prompt-value" id="txt_MailId" value="" class="input-unstyled full-width"  placeholder="Mail Id" type="text">' +
                        '</div>' +
                    '</div>' +
                    '<div class="twelve-columns twelve-columns-mobile" id="ErrorDiv">' +
                        '<label>Error Message: </label><br/> ' +
                        '<div class="input full-width">' +
                            '<input name="prompt-value" id="VisaMessage" value="" class="input-unstyled full-width"   type="text">' +
                        '</div>' +
                    '</div>' +
                    '<div class="updatemail">' +
                        '<ul id="CcMailList">' +
                        '</ul>' +
                        '<input type="text" id="inp_CcAddMail" style="border:none; margin-left:10px; margin-bottom:2px" onblur="AddCcMail()" placeholder="Enter Cc Mail here">' +
                    '</div>' +
                '</div>' +
                '<p class="text-right" style="text-align:right;">' +
                    '<button type="button" class="button anthracite-gradient" onclick="AddMails()">Add</button>' +
                '</p>',
        title: 'Update Mail ',
        width: 500,
        scrolling: true,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'huge anthracite-gradient displayNone',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
    debugger;


    $("CcMailList").empty();

    var CCmails = CcMail.split(';')
    var li = ''
    if (CCmails != "" && CCmails != "-") {
        for (var i = 0; i < CCmails.length ; i++) {
            li += '<li class="multiple_emails" onclick="RemoveLi(this)" ><a style="cursor:pointer"  class="multiple_emails-close" title="Remove"><i class="icon-cross"></i></a><span class="email_name">' + CCmails[i] + '</span></li>'
        }
        $("#CcMailList").append(li)
    }

    if (type == "Visa") {
        $("#divForVisa").empty();
        $("#ErrorDiv").empty();

        var VisaHtml = "";
        VisaHtml += '<label>Activity: </label><br/>'
        VisaHtml += '<div class="input full-width">'
        VisaHtml += '<input name="prompt-value" id="Sel_Activity" placeholder="Activity" value="" class="input-unstyled full-width" type="text" autocomplete="off" readonly>'
        VisaHtml += '</div>'
        $("#divForVisa").append(VisaHtml);

        var Error = "";
        Error += '<label>Error Message: </label><br/>'
        Error += '<div class="input full-width">'
        Error += '<input name="prompt-value" id="VisaMessage" value="" class="input-unstyled full-width" type="text" >'
        Error += '</div>'
        $("#ErrorDiv").append(Error);
        $("#VisaMessage").val(Msg);

        $("#Sel_Activity").val(Activity);
        if (Email == "-") {
            $("#txt_MailId").val("");
        } else {
            $("#txt_MailId").val(Email);
        }
    }
    else if (type == "OTB") {
        $("#divForVisa").empty();
        $("#ErrorDiv").empty();

        var OTBHtml = "";
        OTBHtml += '<label>Activity: </label><br/>'
        OTBHtml += '<div class="input full-width">'
        OTBHtml += '<input name="prompt-value" id="Sel_AirLineInMail" placeholder="Activity" value="" class="input-unstyled full-width" type="text" autocomplete="off" readonly>'
        OTBHtml += '</div>'
        $("#divForVisa").append(OTBHtml);

        var Error = "";
        Error += '<label>Error Message: </label><br/>'
        Error += '<div class="input full-width">'
        Error += '<input name="prompt-value" id="OTBMessage" value="" class="input-unstyled full-width" type="text" >'
        Error += '</div>'
        $("#ErrorDiv").append(Error);
        $("#OTBMessage").val(Msg);

        $("#Sel_AirLineInMail").val(Activity);
        if (Email == "-") {
            $("#txt_AirlinesMailId").val("");
        } else {
            $("#txt_MailId").val(Email);
        }
    }
    else if (type == "Hotel") {
        $("#divForVisa").empty();
        $("#ErrorDiv").empty();

        var HotelHtml = "";
        HotelHtml += '<label>Activity: </label><br/>'
        HotelHtml += '<div class="input full-width">'
        HotelHtml += '<input name="prompt-value" id="Sel_HotelActivity" placeholder="Activity" value="" class="input-unstyled full-width" type="text" autocomplete="off" readonly>'
        HotelHtml += '</div>'
        $("#divForVisa").append(HotelHtml);

        var Error = "";
        Error += '<label>Error Message: </label><br/>'
        Error += '<div class="input full-width">'
        Error += '<input name="prompt-value" id="HotelMessage" value="" class="input-unstyled full-width" type="text" >'
        Error += '</div>'
        //var Error = 'Error Message :<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span> <textarea id="VisaMessage" class="form-control col-md-10" style="height:60px;width:540px"></textarea><label style="color: red; margin-top: 3px; display: none" id="lbl_ErrorMessage"><b>* This field is required</b></label>';
        $("#ErrorDiv").append(Error);
        $("#HotelMessage").val(Msg);

        $("#Sel_HotelActivity").val(Activity);
        if (Email == "-") {
            $("#txt_HotelMailId").val("");
        }
        else {
            $("#txt_MailId").val(Email);
        }

    }

}

var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
function AddCcMail() {
    var NewCcMail = $('#inp_CcAddMail').val();
    if (!NewCcMail) {
        return false;
    }
    if (!emailReg.test(NewCcMail)) {
        bValid = false;
        $("inp_CcAddMail").focus();
        Success("Please Insert Valid Mail Id");
        $('#inp_CcAddMail').val('');
        return false;
    }
    var ccli = '<li class="multiple_emails" onclick="RemoveLi(this)"><a style="cursor:pointer" class="multiple_emails-close" title="Remove"><i class="icon-cross"></i></a><span class="email_name">' + NewCcMail + '</span></li>'
    $('#CcMailList').append(ccli);
    $('#inp_CcAddMail').val('');
}