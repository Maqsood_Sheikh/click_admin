﻿var global_CategoryCount = 0;
var sPricingData = [];
var globe_urlParamDecoded = 0;
var globalDynamicInclusionCount = 0;
var globalDynamicExclusionCount = 0;
var hcd;
$(document).ready(function () {
    debugger;
    hcd = GetQueryStringParams('hcd');
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    var urlParameter = atob(url[0]).split('=');
    globe_urlParamDecoded = urlParameter[1];
    GetPricingDetail(globe_urlParamDecoded);
    $('#lst_BasicInformation').on("click", function () { BasicDetails(globe_urlParamDecoded); });
    $('#lst_Pricing').on("click", function () { UpdatePricingDetails(globe_urlParamDecoded); });
    $('#lst_Itinerary').on("click", function () { ItineraryDetails(globe_urlParamDecoded); });
    $('#lst_HotelDetails').on("click", function () { HotelDetails(globe_urlParamDecoded); });
    $('#lst_PackageImages').on("click", function () { PackageImagesImages(globe_urlParamDecoded); });
});

function GetCategoryName(categoryID) {
    if (categoryID == 1) {
        return "Standard";
    }
    else if (categoryID == 2) {
        return "Deluxe";
    }
    else if (categoryID == 3) {
        return "Premium";
    }
    else if (categoryID == 4) {
        return "Luxury";
    }
}

function UpdatePricingDetails(nID) {
    $(location).attr('href', '../Admin/PricingDetails.aspx?' + btoa('nID=' + nID) + '&hcd=' + hcd);
}
function HotelDetails(nID) {
    $(location).attr('href', '../Admin/UpdateHotelDetails.aspx?' + btoa('nID=' + nID) + '&hcd=' + hcd);
}

function BasicDetails(nID) {
    $(location).attr('href', '../Admin/PackageDetails.aspx?' + btoa('nID=' + nID) + '&hcd=' + hcd);
}
function ItineraryDetails(nID) {
    $(location).attr('href', '../Admin/ItineraryDetail.aspx?' + btoa('nID=' + nID) + '&hcd=' + hcd);
}
function PackageImagesImages(nID) {
    $(location).attr('href', '../Admin/PackageImageDetail.aspx?' + btoa('nID=' + nID) + '&hcd=' + hcd);
}

function AddInclsion(sTabName) {
    var inclusionName = $("#txt_AddInclusion" + sTabName).val().trim();
    if (inclusionName == "") {
        alert("Add a Inclusion");
    }
    else {
        var tRow = '<tr><td><label class="label11"><input id="chkIn' + sTabName + globalDynamicInclusionCount + '" value="' + inclusionName + '" checked="checked" type="checkbox" class="margtop5 input11" name="Inclusion" />&nbsp;' + inclusionName + '</label></td></tr>';
        $("#tblDynamicInclusion" + sTabName).append(tRow);
        $("#txt_AddInclusion" + sTabName).val("");
        globalDynamicInclusionCount = globalDynamicInclusionCount + 1;
    }
}

function CreateDynamicInclusionEdit(sTabName, nCount, inclusionName) {
    if (inclusionName == "") {
        return false;
    }
    else {
        var tRow = '<tr><td><label class="label11"><input id="chkIn' + sTabName + nCount + '" value="' + inclusionName + '" type="checkbox" checked="checked" class="margtop5 input11" name="Inclusion" />&nbsp;' + inclusionName + '</label></td></tr>';
        $("#tblDynamicInclusion" + sTabName).append(tRow);
        $("#txt_AddInclusion" + sTabName).val("");
    }
}

function AddExclusion(sTabName) {
    var exclusionName = $("#txt_AddExclusion" + sTabName).val().trim();
    if (exclusionName == "") {
        alert("Add a Exclusion");
    }
    else {
        var tRow = '<tr><td><label class="label11"><input id="chkEx' + sTabName + globalDynamicExclusionCount + '" value="' + exclusionName + '" type="checkbox" checked="checked" class="margtop5 input11" name="Exclusion" />&nbsp;' + exclusionName + '</label></td></tr>';
        $("#tblDynamicExclusion" + sTabName).append(tRow);
        $("#txt_AddExclusion" + sTabName).val("");
        globalDynamicExclusionCount = globalDynamicExclusionCount + 1;
    }
}

function CreateDynamicExclusionEdit(sTabName, nCount, exclusionName) {
    if (exclusionName == "") {
        return false;
    }
    else {
        var tRow = '<tr><td><label class="label11"><input id="chkEx' + sTabName + nCount + '" value="' + exclusionName + '" type="checkbox" checked="checked" class="margtop5 input11" name="Exclusion" />&nbsp;' + exclusionName + '</label></td></tr>';
        $("#tblDynamicExclusion" + sTabName).append(tRow);
        $("#txt_AddExclusion" + sTabName).val("");
    }
}

function setStaticInclusion(sTabName, nIndexValue) {
    if (nIndexValue == 1) {
        $("#chkAirfairIn" + sTabName).attr("checked", "checked");
    }
    else if (nIndexValue == 2) {
        $("#chkSightIn" + sTabName).attr("checked", "checked");
    }
    else if (nIndexValue == 3) {
        $("#chkBreakfastIn" + sTabName).attr("checked", "checked");
    }
    else if (nIndexValue == 4) {
        $("#chkGuideIn" + sTabName).attr("checked", "checked");
    }
    else if (nIndexValue == 5) {
        $("#chkLunchIn" + sTabName).attr("checked", "checked");
    }
    else if (nIndexValue == 6) {
        $("#chkDinnerIn" + sTabName).attr("checked", "checked");
    }
}
function setStaticExclusion(sTabName, nIndexValue) {
    if (nIndexValue == 1) {
        $("#chkAirfairEx" + sTabName).attr("checked", "checked");
    }
    else if (nIndexValue == 2) {
        $("#chkSightEx" + sTabName).attr("checked", "checked");
    }
    else if (nIndexValue == 3) {
        $("#chkBreakfastEx" + sTabName).attr("checked", "checked");
    }
    else if (nIndexValue == 4) {
        $("#chkGuideEx" + sTabName).attr("checked", "checked");
    }
    else if (nIndexValue == 5) {
        $("#chkLunchEx" + sTabName).attr("checked", "checked");
    }
    else if (nIndexValue == 6) {
        $("#chkDinnerEx" + sTabName).attr("checked", "checked");
    }
}
function GetPricingDetail(nProductID) {
    $.ajax({
        url: "../Handler/PackageDetailHandler.asmx/GetPricingDetail",
        type: "post",
        data: '{"nID":"' + nProductID + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.Session == 0) {
                window.location.href = "index.htm";
            }
            if (result.retCode == 0) {
                alert("No packages found");
            }
            else if (result.retCode == 1) {
                global_CategoryCount = result.nCount;
                var sPricDetail = result.List_pricingDetail;
                sPricingData = result.List_pricingDetail;
                MakePriceTab(sPricDetail);
            }
        },
        error: function () {
        }
    });
}

function MakePriceTab(sPricDetail) {
    var sTabWidth = 100 / sPricDetail.length;
    var sTabRow = '';
    var sTabRowContent = '';
    var sclass = '';
    var sDisplay = '';
    for (var i = 0; i < sPricDetail.length; i++) {
        var sTabID = GetCategoryName(sPricDetail[i].nCategoryID);
        if (i == 0) {
            sclass = "active";
            sDisplay = ";display:block";

        }
        else {
            sclass = "";
            sDisplay = ";";
        }
        sTabRow += '<li onclick="mySelectUpdate()" id="li' + sTabID + '" class="' + sclass + '" style="width:' + sTabWidth + '%' + sDisplay + '"><a data-toggle="tab" href="#Tab' + sTabID + '"><span class=""></span><span class="hidetext">' + sTabID + '</span>&nbsp;</a></li>';
        sTabRowContent += '<div class="tab-pane ' + sclass + '" id="Tab' + sTabID + '">';
        sTabRowContent += '<table id="tbl_AddPackagePrice' + sTabID + '" class="table table-responsive" style="float: right"><tr><td>';
        sTabRowContent += '<span class="text-left" style="float: left; font-weight: bold">Single :</span><input id="txt_PriceSingle' + sTabID + '" value="' + sPricDetail[i].dSingleAdult + '" type="text" class="form-control margtop5" placeholder="0.00" />';
        sTabRowContent += '<label id="lblerr_sPriceAdult' + sTabID + '" style="color: red; margin-top: 3px; float: left; display: none; padding-left: 2%">* This field is required</label></td><td>';
        sTabRowContent += '<span class="text-left" style="float: left; font-weight: bold">Twin Sharing :</span>';
        sTabRowContent += '<input id="txt_PriceTwin' + sTabID + '" value="' + sPricDetail[i].dCouple + '" type="text" class="form-control margtop5" placeholder="0.00" />';
        sTabRowContent += '<label id="lblerr_sPriceAdultTwoSharing' + sTabID + '" style="color: red; margin-top: 3px; float: left; display: none; padding-left: 2%">* This field is required</label></td><td>';
        sTabRowContent += '<span class="text-left" style="float: left; font-weight: bold">Extra Adult :</span><input id="txt_PriceExtraAdult' + sTabID + '" value="' + sPricDetail[i].dExtraAdult + '" type="text" class="form-control margtop5" placeholder="0.00" /></td></tr><tr><td>';
        sTabRowContent += '<span class="text-left" style="float: left; font-weight: bold">Infant Kid :</span><input id="txt_PriceInfantKid' + sTabID + '" value="' + sPricDetail[i].dInfantKid + '" type="text" class="form-control margtop5" placeholder="0.00" /></td><td>';
        sTabRowContent += '<span class="text-left" style="float: left; font-weight: bold">Kid rate without bed :</span><input id="txt_PriceKidWithoutBed' + sTabID + '" value="' + sPricDetail[i].dKidWBed + '" type="text" class="form-control margtop5" placeholder="0.00" /></td><td>';
        sTabRowContent += '<span class="text-left" style="float: left; font-weight: bold">Kid rate with bed :</span><input id="txt_PriceKidWithBed' + sTabID + '" value="' + sPricDetail[i].dKidWOBed + '" type="text" class="form-control margtop5" placeholder="0.00" /></td></tr></table>';
        sTabRowContent += '<br/><table id="tbl_Inclusions' + sTabID + '" class="table-responsive" style="width: 49%; float: left"><tr><td><span class="text-left" style="float: left; font-weight: bold">Inclusions :</span> </td></tr>';
        sTabRowContent += '<tr><td><label class="label11"><input id="chkAirfairIn' + sTabID + '" value="1" type="checkbox" class="margtop5 input11" name="Inclusion" />&nbsp;Airfare with Airport transfer</label></td></tr>';
        sTabRowContent += '<tr><td><label class="label11"><input id="chkSightIn' + sTabID + '" value="2" type="checkbox" class="margtop5 input11" name="Inclusion" />&nbsp;Sight Seeing</label></td></tr>';
        sTabRowContent += '<tr><td><label class="label11"><input id="chkBreakfastIn' + sTabID + '" value="3" type="checkbox" class="margtop5 input11" name="Inclusion" />&nbsp;Breakfast</label></td></tr>';
        sTabRowContent += '<tr><td><label class="label11"><input id="chkGuideIn' + sTabID + '" value="4" type="checkbox" class="margtop5 input11" name="Inclusion" />&nbsp;Tour Guide</label></td></tr>';
        sTabRowContent += '<tr><td><label class="label11"><input id="chkLunchIn' + sTabID + '" value="5" type="checkbox" class="margtop5 input11" name="Inclusion" />&nbsp;Lunch</label></td></tr>';
        sTabRowContent += '<tr><td><label class="label11"><input id="chkDinnerIn' + sTabID + '" value="6" type="checkbox" class="margtop5 input11" name="Inclusion" />&nbsp;Dinner</label></td></tr>';
        sTabRowContent += '<tr><td><table class="table-responsive" id="tblDynamicInclusion' + sTabID + '"></table></td></tr>';
        sTabRowContent += '<tr><td><input type="text" style="width: 90%; float: left; margin-top: 5px" class="form-control" id="txt_AddInclusion' + sTabID + '" />&nbsp;<span class="glyphicon glyphicon-plus" style="margin-top: 10px; cursor: pointer" onclick="AddInclsion(\'' + sTabID + '\');"></span></td></tr></table>';
        sTabRowContent += '<table id="tbl_Exclusions' + sTabID + '" class="table-responsive" style="width: 49%; float: left"><tr><td><span class="text-left" style="float: left; font-weight: bold">Exclusion :</span> </td></tr>';
        sTabRowContent += '<tr><td><label class="label11"><input id="chkAirfairEx' + sTabID + '" value="1" type="checkbox" class="margtop5 input11" name="Inclusion" />&nbsp;Airfare with Airport transfer</label></td></tr>';
        sTabRowContent += '<tr><td><label class="label11"><input id="chkSightEx' + sTabID + '" value="2" type="checkbox" class="margtop5 input11" name="Inclusion" />&nbsp;Sight Seeing</label></td></tr>';
        sTabRowContent += '<tr><td><label class="label11"><input id="chkBreakfastEx' + sTabID + '" value="3" type="checkbox" class="margtop5 input11" name="Inclusion" />&nbsp;Breakfast</label></td></tr>';
        sTabRowContent += '<tr><td><label class="label11"><input id="chkGuideEx' + sTabID + '" value="4" type="checkbox" class="margtop5 input11" name="Inclusion" />&nbsp;Tour Guide</label></td></tr>';
        sTabRowContent += '<tr><td><label class="label11"><input id="chkLunchEx' + sTabID + '" value="5" type="checkbox" class="margtop5 input11" name="Inclusion" />&nbsp;Lunch</label></td></tr>';
        sTabRowContent += '<tr><td><label class="label11"><input id="chkDinnerEx' + sTabID + '" value="6" type="checkbox" class="margtop5 input11" name="Inclusion" />&nbsp;Dinner</label></td></tr>';
        sTabRowContent += '<tr><td><table class="table-responsive" id="tblDynamicExclusion' + sTabID + '"></table></td></tr>';
        sTabRowContent += '<tr><td><input type="text" style="width: 90%; float: left; margin-top: 5px" class="form-control" id="txt_AddExclusion' + sTabID + '" />&nbsp;<span class="glyphicon glyphicon-plus" style="margin-top: 10px; cursor: pointer" onclick="AddExclusion(\'' + sTabID + '\');"></span></td></tr></table></div></div>';
    }
    $("#sPriceTabs").html(sTabRow);
    $("#div_pricingTabContent").html(sTabRowContent);
    for (var k = 0; k < sPricDetail.length; k++) {
        var sTabName = GetCategoryName(sPricDetail[k].nCategoryID);
        var sStaticInclusion = sPricDetail[k].sStaticInclusions.split(',');
        var sStaticExclusion = sPricDetail[k].sStaticExclusion.split(',');
        var sDynamicInclusion = sPricDetail[k].sDynamicInclusion.split(',');
        var sDynamicExclusion = sPricDetail[k].sDynamicExclusion.split(',');
        for (var sInc = 0; sInc < sStaticInclusion.length; sInc++) {
            setStaticInclusion(sTabName, sStaticInclusion[sInc]);
        }
        for (var sInc = 0; sInc < sStaticExclusion.length; sInc++) {
            setStaticExclusion(sTabName, sStaticExclusion[sInc]);
        }

        for (var sDynInc = 0; sDynInc < sDynamicInclusion.length; sDynInc++) {
            CreateDynamicInclusionEdit(sTabName, sDynInc, sDynamicInclusion[sDynInc]);
        }
        for (var sDynEx = 0; sDynEx < sDynamicExclusion.length; sDynEx++) {
            CreateDynamicExclusionEdit(sTabName, sDynEx, sDynamicExclusion[sDynEx]);
        }
    }
}

function SavePricingDetails() {
    debugger;
    for (var i = 0; i < global_CategoryCount; i++) {
        var sTabName = GetCategoryName(sPricingData[i].nCategoryID);
        var sStaticInclusion = '';
        var sStaticExclusion = '';
        var sDynamicInclusion = '';
        var sDynamicExclusion = '';
        var nDynamicInclusion = 1;
        var nDynamicExclusion = 1;
        var sSingleAdult = $("#txt_PriceSingle" + sTabName).val();
        var sCouple = $("#txt_PriceTwin" + sTabName).val();
        var sExtraAdult = $("#txt_PriceExtraAdult" + sTabName).val();
        var sInfantKid = $("#txt_PriceInfantKid" + sTabName).val();
        var sKidWBed = $("#txt_PriceKidWithoutBed" + sTabName).val();
        var sKidWOBed = $("#txt_PriceKidWithBed" + sTabName).val();

        if (sSingleAdult == "") {
            $("#txt_PriceSingle" + sTabName).focus();
            alert("Please fill single person price for category " + sTabName);
            return false;
        }
        else if (sCouple == "") {
            alert("Please fill couple price for category " + sTabName);
            $("#txt_PriceTwin" + sTabName).focus();
            return false;
        }
        else if (sExtraAdult == "") {
            alert("Please fill extra adult person price for category " + sTabName);
            $("#txt_PriceInfantKid" + sTabName).focus();
            return false;
        }
        else if (sInfantKid == "") {
            alert("Please fill infant kid price for category " + sTabName);
            $("#txt_PriceSingle" + sTabName).focus();
            return false;
        }
        else if (sKidWBed == "") {
            alert("Please kid without bed price for category " + sTabName);
            $("#txt_PriceKidWithoutBed" + sTabName).focus();
            return false;
        }
        else if (sKidWOBed == "") {
            alert("Please fill kid with bed price for category " + sTabName);
            $("#txt_PriceKidWithBed" + sTabName).focus();
            return false;
        }
    }
    var sSuccessfulEntry = 0;
    for (var i = 0; i < global_CategoryCount; i++) {
        var sTabName = GetCategoryName(sPricingData[i].nCategoryID);
        var sStaticInclusion = '';
        var sStaticExclusion = '';
        var sDynamicInclusion = '';
        var sDynamicExclusion = '';
        var nDynamicInclusion = 1;
        var nDynamicExclusion = 1;
        var sSingleAdult = $("#txt_PriceSingle" + sTabName).val();
        var sCouple = $("#txt_PriceTwin" + sTabName).val();
        var sExtraAdult = $("#txt_PriceExtraAdult" + sTabName).val();
        var sInfantKid = $("#txt_PriceInfantKid" + sTabName).val();
        var sKidWBed = $("#txt_PriceKidWithoutBed" + sTabName).val();
        var sKidWOBed = $("#txt_PriceKidWithBed" + sTabName).val();
        //============ Checking Static Inclusion Start ================//
        if ($("#chkAirfairIn" + sTabName).is(':checked')) {
            sStaticInclusion = sStaticInclusion + $("#chkAirfairIn" + sTabName).val() + ',';
        }
        if ($("#chkSightIn" + sTabName).is(':checked')) {
            sStaticInclusion = sStaticInclusion + $("#chkSightIn" + sTabName).val() + ',';
        }
        if ($("#chkBreakfastIn" + sTabName).is(':checked')) {
            sStaticInclusion = sStaticInclusion + $("#chkBreakfastIn" + sTabName).val() + ',';
        }
        if ($("#chkGuideIn" + sTabName).is(':checked')) {
            sStaticInclusion = sStaticInclusion + $("#chkGuideIn" + sTabName).val() + ',';
        }
        if ($("#chkLunchIn" + sTabName).is(':checked')) {
            sStaticInclusion = sStaticInclusion + $("#chkLunchIn" + sTabName).val() + ',';
        }
        if ($("#chkDinnerIn" + sTabName).is(':checked')) {
            sStaticInclusion = sStaticInclusion + $("#chkDinnerIn" + sTabName).val() + ',';
        }
        //============ Checking Static Inclusion End ================//

        //============ Checking Static Exclusion Start================//
        if ($("#chkAirfairEx" + sTabName).is(':checked')) {
            sStaticExclusion = sStaticExclusion + $("#chkAirfairEx" + sTabName).val() + ',';
        }
        if ($("#chkSightEx" + sTabName).is(':checked')) {
            sStaticExclusion = sStaticExclusion + $("#chkSightEx" + sTabName).val() + ',';
        }
        if ($("#chkBreakfastEx" + sTabName).is(':checked')) {
            sStaticExclusion = sStaticExclusion + $("#chkBreakfastEx" + sTabName).val() + ',';
        }
        if ($("#chkGuideEx" + sTabName).is(':checked')) {
            sStaticExclusion = sStaticExclusion + $("#chkGuideEx" + sTabName).val() + ',';
        }
        if ($("#chkLunchEx" + sTabName).is(':checked')) {
            sStaticExclusion = sStaticExclusion + $("#chkLunchEx" + sTabName).val() + ',';
        }
        if ($("#chkDinnerEx" + sTabName).is(':checked')) {
            sStaticExclusion = sStaticExclusion + $("#chkDinnerEx" + sTabName).val() + ',';
        }
        //============ Checking Static Exclusion End================//


        //============ Checking Dynamic Inclusion Start ================//
        $("#tblDynamicInclusion" + sTabName + " tr").each(function () {
            //sDynamicInclusion += $(this).find('td').find($('#chkIn' + sTabName + nDynamicInclusion)).val() + ',';
            if ($(this).find('td').find('input:checked').val() != undefined) {
                sDynamicInclusion += $(this).find('td').find('input:checked').val() + ',';
                nDynamicInclusion = nDynamicInclusion + 1;
            }
        });
        //============ Checking Dynamic Inclusion End ================//

        //============ Checking Dynamic Exclusion Start================//
        $("#tblDynamicExclusion" + sTabName + " tr").each(function () {
            //sDynamicExclusion += $(this).find('td').find($('#chkEx' + sTabName + nDynamicExclusion)).val() + ',';
            if ($(this).find('td').find('input:checked').val() != undefined) {
                sDynamicExclusion += $(this).find('td').find('input:checked').val() + ',';
                nDynamicExclusion = nDynamicExclusion + 1;
            }
        });
        //============ Checking Dynamic Exclusion End================//

        $.ajax({
            url: "../Handler/PackageDetailHandler.asmx/UpdatePricingDetails",
            type: "post",
            data: '{"nPackageID":"' + globe_urlParamDecoded + '","nCategoryID":"' + sPricingData[i].nCategoryID + '","nCategoryName":"' + sTabName + '","sSingleAdult":"' + sSingleAdult + '","sCouple":"' + sCouple + '","sExtraAdult":"' + sExtraAdult + '","sInfantKid":"' + sInfantKid + '","sKidWBed":"' + sKidWBed + '","sKidWOBed":"' + sKidWOBed + '","sStaticInclusion":"' + sStaticInclusion + '","sDynamicInclusion":"' + sDynamicInclusion + '","sStaticExclusion":"' + sStaticExclusion + '","sDynamicExclusion":"' + sDynamicExclusion + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                if (result.Session == 0) {
                    window.location.href = "index.htm";
                }
                if (result.retCode == 0) {
                    alert("No packages found");
                }
                else if (result.retCode == 1) {
                    sSuccessfulEntry = sSuccessfulEntry + 1;
                    if (sSuccessfulEntry == global_CategoryCount) {
                        alert("Entry Added Successfully, Please add Itinerary Details");
                        $(location).attr('href', '../Admin/ItineraryDetail.aspx?' + btoa('nID=' + globe_urlParamDecoded) + '&hcd=' + hcd);
                    }
                }
            },
            error: function () {
            }
        });
    }
    //=== End For Loop =====//
}

function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}