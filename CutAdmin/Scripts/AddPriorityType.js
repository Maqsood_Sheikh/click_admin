﻿


function Save() {
    if ($("#btn_Supplier2").val() == "Update") {
        Update_Priority()
    }
    else
    {
        AddPriority();
    }
}

$(function () {
    Getpriority();
    var newId = Date.now().toString().substr(8); // or use any method that you want to achieve this string
    $("#Password").val(newId)
    //$("#AddCustomer").modal("show");
    //$("#btn_Supplier").val("Save");
})

function OpenPopUp1() {
    Getpriority();
    var newId = Date.now().toString().substr(8); // or use any method that you want to achieve this string
    $("#Password").val(newId)
    $("#AddCustomer2").modal("show")
}


function AddPriority() {
    debugger;

    var fName = $("#fname").val()

    var param = { fName: fName }
    $.ajax({
        type: "POST",
        url: "ActivityHandller.asmx/AddPriority",
        data: JSON.stringify(param),
        contentType: "application/json",
        datatype: "json",
        success: function (data) {
            var obj = JSON.parse(data.d);
            if (obj.retCode == 1) {
                Success("Priority Added Sucessfully.");
                $("#fname").val("");
                Getpriority();
            }
            else {
                Success("Something Went Wrog");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
             
            }
        }
    });
}

function Getpriority() {
    $("#tbl_GetPriority").dataTable().fnClearTable();
    $("#tbl_GetPriority").dataTable().fnDestroy();

    $.ajax({
        url: "ActivityHandller.asmx/Getpriority",
        type: "post",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                var arr_MatchSummary = result.dtresult;

                for (var i = 0; i < arr_MatchSummary.length; i++)
                {
                    var html = '';
                    var sid = arr_MatchSummary[i].Sid;
                    html += '<tr><td>' + (i + 1) + '</td>'
                    html += '<td align="center">' + arr_MatchSummary[i].PriorityType + '</td>'   
                    //html += '<td align="center"><a style="cursor:pointer" onclick="GetPriorityttoActive(\'' + sid + '\',\'' + arr_MatchSummary[i].PriorityType + '\')"><span class="icon-eye"  title="Active/In-Active"></span></a></td>'
                    if (arr_MatchSummary[i].Status== "False")
                        html += '<td align="center"><i style="Cursor:Pointer" onclick="GetPriorityActiveDeactive(\'' + sid + '\',\'True\')"  aria-hidden="true" title="Deactivate"><label for="pseudo-input-2" class="button blue-gradient" ><span class="icon-cross"></span></label></i></td>'
                    else 
                        html += '<td align="center"><i style="Cursor:Pointer" onclick="GetPriorityActiveDeactive(\'' + sid + '\',\'False\')"  aria-hidden="true" title="Activate"><label for="pseudo-input-2" class="button blue-gradient" ><span class="icon-tick"></span></label></i></td>'
                       
                    html += '<td align="center"><i style="cursor:pointer" onclick="GetPriorityttoupdate(\'' + sid + '\',\'' + arr_MatchSummary[i].PriorityType + '\')" aria-hidden="true" title="Update Details"><span class="icon-pencil icon-size2"></span></i>&nbsp |&nbsp <i style="cursor:pointer" onclick="DeletePriority(\'' + sid + '\')" aria-hidden="true" title="Delete"><span class="icon-trash"></span></i></td>'
                    html += '</tr>'
                    $("#tbl_GetPriority tbody").append(html);
                }

                $('[data-toggle="tooltip"]').tooltip()
                $("#tbl_GetPriority").dataTable({
                     bSort: false, sPaginationType: 'full_numbers',
                });
                $("#tbl_GetPriority").css("width", "100%")
            }
        }
    })
}

var Pid; 
function GetPriorityttoupdate(sid, name) {
    $("#btn_Supplier2").val("Update");
    $("#fname").val(name);
    Pid = sid;
    //Update_Priority();
}

function GetModetoupdate(sid, name) {
    $("#btn_Supplier").val("Update");
    $("#Fname").val(name);
    Pid = sid;
}

function Update_Priority() {

    var fName = $("#fname").val()

    $.ajax({
        type: "POST",
        url: "ActivityHandller.asmx/Update_Priority",
        data: '{"fName":"' + fName + '","Pid":"' + Pid + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                    
                Success("Priority Updated Sucessfully.");
                $("#fname").val("");
                Getpriority();
                $("#btn_Supplier2").val("Save");
            }
            if (result.retCode == 0) {
                Success("Something went wrong!")
            }
        },
        error: function () {
            Success("An error occured while updating details");
        }
    });
}

function DeletePriority(sid) {

    $.ajax({
        type: "POST",
        url: "ActivityHandller.asmx/DeletePriority",
        data: '{"sid":"' + sid + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0) {

                Success("Error in deleting ");
                return false;
            }
            if (result.retCode == 1) {
                Success("Priority Deleted Sucessfully.");
                Getpriority();
            }
        },
        error: function () {
            Success("Error in deleting ");
        }
    });
}

function GetPriorityActiveDeactive(sId, Status) {
    var Data = { Sid: sId, Status: Status };
    $.ajax({
        type: "POST",
        url: "ActivityHandller.asmx/GetPriorityActiveDeactive",
        data: JSON.stringify(Data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d)
            var SeasonList = obj.SeasonList;
            if (obj.retCode == 1) {
                if (Status == "True")
                    Success("Priority Activated Successfully");
                else
                    Success("Priority Deactivated Successfully");
                Getpriority();
            }
            else {
                Success("Error while Season Activate");
            }
        },
    });
}
