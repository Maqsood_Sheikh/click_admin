﻿$(document).ready(function () {
    GetPackageDetails();
});

function GetCategoryName(categoryID) {
    if (categoryID == 1) {
        return "Standard";
    }
    else if (categoryID == 2) {
        return "Deluxe";
    }
    else if (categoryID == 3) {
        return "Premium";
    }
    else if (categoryID == 4) {
        return "Luxury";
    }
}

function GetPackageType(id) {
    if (id == 1) {
        return "Holidays";
    }
    else if (id == 2) {
        return "Umrah";
    }
    else if (id == 3) {
        return "Hajj";
    }
    else if (id == 4) {
        return "Honeymoon";
    }
    else if (id == 5) {
        return "Summer";
    }
    else if (id == 6) {
        return "Adventure";
    }
    else if (id == 7) {
        return "Deluxe";
    }
    else if (id == 8) {
        return "Business";
    }
    else if (id == 9) {
        return "Premium";
    }
    else if (id == 10) {
        return "Wildlife";
    }
    else if (id == 11) {
        return "Weekend";

    }
    else if (id == 12) {
        return "New Year";
    }
    else {
        return "No Theme";
    }
}
function GetPackageDetails() {
    debugger;
    $("#tbl_PackageDetails").dataTable().fnClearTable();
    $("#tbl_PackageDetails").dataTable().fnDestroy();
    $.ajax({
        url: "../Handler/ViewPackageHandler.asmx/GetPackageDetails",
        type: "Post",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.Session == 0) {
                window.location.href = "index.htm";
            }
            if (result.retCode == 0) {
                alert("No packages found");
            }
            else if (result.retCode == 1) {
                List_Packages = result.List_Packages;
                var tRow = '';
                for (var i = 0; i < List_Packages.length; i++) {
                    var sCategory = List_Packages[i].sPackageCategory.split(',');
                    var sCategoryName = "";
                    if (sCategory.length == 1) {
                        for (var k = 0; k < sCategory.length; k++) {
                            sCategoryName = sCategoryName + "," + GetCategoryName(sCategory[k]);
                        }
                    }
                    else {
                        for (var k = 0; k < sCategory.length - 1; k++) {
                            sCategoryName = sCategoryName + "," + GetCategoryName(sCategory[k]);
                        }
                    }


                    var dfrom = (List_Packages[i].dValidityFrom).split(' ');

                    var dTO = (List_Packages[i].dValidityTo).split(' ');

                    var city = (List_Packages[i].sPackageDestination).split('|');
                    tRow += '<tr>';
                    tRow += '<td>' + List_Packages[i].sPackageName + '</td>';
                    tRow += '<td>' + city[0] + '</td>';
                    tRow += '<td>' + List_Packages[i].nDuration + ' Days</td>';
                    tRow += '<td>' + sCategoryName.replace(/^,|,$/g, ''); + '</td>';
                    tRow += '<td>' + dfrom[0] + '</td>';
                    tRow += '<td>' + dTO[0] + '</td>';
                    tRow += '<td class="align-center"><a class="button" title="Update"><span class="icon-publish " style="cursor:pointer" title="Click here to View package detail" onclick="PackageDetails(' + List_Packages[i].nID + ')"></span></a></td>';
                    //tRow += '<td style="width:5%;text-align: center;"><span class="glyphicon glyphicon-edit" style="cursor:pointer" title="Click here to View package detail" onclick="PackageDetails(' + List_Packages[i].nID + ')"></span></td>';
                    tRow += '<td data-title="Click here to delete the package" style="text-align:center;"><a style="cursor:pointer" href="#"><span class="icon-trash" title="Click here to delete the package" style="cursor:pointer" onclick="DeletePackage(' + List_Packages[i].nID + ')"></span></a></td>';
                    //tRow += '<td style="width:5%;text-align: center;"><span class="glyphicon glyphicon-trash" style="cursor:pointer" title="Click here to delete the package" onclick="DeletePackage(' + List_Packages[i].nID + ')"></span></td>';
                    tRow += '</tr>';
                }

                $("#tbl_PackageDetails tbody").html(tRow);
                $("#tbl_PackageDetails").dataTable({
                    bSort: false,
                    sPaginationType: 'full_numbers',
                    sSearch: false
                });
                document.getElementById("tbl_PackageDetails").removeAttribute("style")
                //$("#tbl_PackageDetails tbody").html(tRow);
            }
        },
        error: function () {
            alert('Error in getting package!');
        }
    });
}
function PackageDetails(ID) {

    window.location.href = "AddQuotation.aspx?ID=" + ID;

}

//function PackageDetails(nID) {
//    $(location).attr("target", "_blank");
//    $(location).attr('href', '../Admin/PackageDetail.aspx?' + btoa('nID=' + nID));
//}
function DeletePackage(nID) {
    debugger;
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure want to delete the package<br/> <span class=\"orange\">' + nID + '</span>?</span>?</p>',
        function () {

            $.ajax({
                url: "../Handler/PackageDetailHandler.asmx/DeletePackage",
                type: "post",
                data: '{"nID":"' + nID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        GetPackageDetails();
                        Success("Package deleted successfully");
                    } else if (result.retCode == 0) {
                        Success("Error in deleting package");
                        setTimeout(function () {
                            window.location.reload();
                        }, 500)
                    }
                },
                error: function () {
                    Success('Error occured while processing your request! Please try again.');
                    setTimeout(function () {
                        window.location.reload();
                    }, 500)
                }
            });

        },
        function () {
            $('#modals').remove();
        }
    );
}
//function DeletePackage(nID) {
//    var bSureDelete = confirm("Are you sure want to delete the package?");
//    Ok("Are you sure want to delete the package?", "Delete", [nID])
//    //if (bSureDelete) {

//    //}
//}
//function Delete(nID) {
//    $.ajax({
//        url: "../Handler/PackageDetailHandler.asmx/DeletePackage",
//        type: "post",
//        data: '{"nID":"' + nID + '"}',
//        contentType: "application/json; charset=utf-8 ",
//        dataType: "json",
//        success: function (data) {
//            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
//            if (result.retCode == 1) {
//                GetPackageDetails();
//                alert("Package deleted successfully");
//                return false;
//            }
//            else {
//                alert("Error in deleting package");
//                return false;
//            }
//        },
//        error: function () {

//        }
//    });
//}