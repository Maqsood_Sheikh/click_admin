﻿var arrFacilities = []; var arr;
var arrHotellist = new Array();
var arrFacilitiesList = new Array();
var pFacilities = "", selectedFacilities = [];
var arrRoomList = [];

$(document).ready(function () {

    var trRequest = "";

    //$("#tbl_HotelList").dataTable().fnClearTable();
    //$("#tbl_HotelList").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "HotelHandler.asmx/GetMappedHotels",
        //data: '{"dFrom":"' + dFrom + '","dTo":"' + dTo + '"}',
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrHotellist = result.MappedHotelList;
                OfferList = result.OfferList;
                RoomList = result.RoomList;
                arrRoomList = RoomList;
                $("#tbl_HotelList tbody").empty();
                //Hotels List
                if (arrHotellist.length > 0) {
                    for (i = 0; i < arrHotellist.length; i++) {
                        arr = arrHotellist[i].HotelFacilities;
                        arrFacilities = arr.split(',');
                        //trRequest += '<tr>';
                        //trRequest += '<th scope="col" width="35%">' + arrHotellist[i].HotelName + '</th>';
                        //trRequest += '<td scope="col" width="10%">' + arrHotellist[i].CountryId + '</td>';
                        //trRequest += '<td scope="col" width="10%">' + arrHotellist[i].CityId + '</td>';
                        //trRequest += '<td scope="col" width="10%">' + arrHotellist[i].HotelCategory + '</td>';
                        //trRequest += '<td scope="col" width="10%"><a  href="AddHotelDetails.aspx?sHotelID=' + arrHotellist[i].sid + '&City=' + arrHotellist[i].CityId + '&Country=' + arrHotellist[i].CountryId + '"><label for="pseudo-input-2" class="button blue-gradient" ><span class="icon-pencil"></span></label></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
                        //trRequest += '<td scope="col" width="10%">';
                        //trRequest += '<select onchange="javascript:location.href = this.value;" class="select expandable-list blue-gradient glossy" style="width:60px;" tabindex="2">';
                        //trRequest += '<option value="HotelOffer.aspx?sHotelID=' + arrHotellist[i].sid + '&HName=' + arrHotellist[i].HotelName + '">Create Offer</option>';
                        //var Offer = $.grep(OfferList, function (O) { return O.Hotel_Id == arrHotellist[i].sid; })
                        //      .map(function (O) { return O; });
                        //if (Offer.length != 0)
                        //    trRequest += '<option value="ViewOfferDetials.aspx?sHotelID=' + arrHotellist[i].sid + '&HName=' + arrHotellist[i].HotelName + '">View Offer</option>';
                        //else
                        //    trRequest += '';
                        //trRequest += '<option value="AddRooms.aspx?sHotelID=' + arrHotellist[i].sid + '&HName=' + arrHotellist[i].HotelName + '">Create Room</option>';
                        //trRequest += '<option value="3">Expand</option>';
                        //trRequest += '<option selected="selected" disabled>&#x2699;</option>';
                        //trRequest += '</select></td>';
                        //trRequest += '</tr>';
                        //trRequest += '<tr id="DropRow' + arrHotellist[i].sid + '" class="row-drop" style="display:none;">';
                        //trRequest += '<td  colspan="6">';
                        //trRequest += '<div class="float-right">abcd</div>';
                        //trRequest += '<strong>Address:</strong><small>' + arrHotellist[i].HotelAddress + '</small><br>';
                        //trRequest += '<strong>Account:</strong> admin<br></td>';
                        //trRequest += '</tr>';

                        trRequest += '<tr >';
                        trRequest += '<td id="' + arrHotellist[i].sid + '">' + arrHotellist[i].HotelName + '</td>';
                        trRequest += '<td id="' + arrHotellist[i].sid + '">' + arrHotellist[i].CountryId + '</td>';
                        trRequest += '<td id="' + arrHotellist[i].sid + '">' + arrHotellist[i].CityId + '</td>';
                        //trRequest += '<td id="' + arrHotellist[i].sid + '">' + arrHotellist[i].HotelCategory + '</td>';

                        // Ratings
                        if (arrHotellist[i].HotelCategory == 'Other' || arrHotellist[i].HotelCategory == '48055' || arrHotellist[i].HotelCategory == '0') {
                            trRequest += '<td  id="' + arrHotellist[i].sid + '"><i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</td>';
                        }
                        else if (arrHotellist[i].HotelCategory == '1EST' || arrHotellist[i].HotelCategory == '559' || arrHotellist[i].HotelCategory == '1') {
                            trRequest += '<td  id="' + arrHotellist[i].sid + '"><i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</td>';
                        }
                        else if (arrHotellist[i].HotelCategory == '2EST' || arrHotellist[i].HotelCategory == '560' || arrHotellist[i].HotelCategory == '2') {
                            trRequest += '<td  id="' + arrHotellist[i].sid + '"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</td>';
                        }
                        else if (arrHotellist[i].HotelCategory == '3EST' || arrHotellist[i].HotelCategory == '561' || arrHotellist[i].HotelCategory == '3') {
                            trRequest += '<td  id="' + arrHotellist[i].sid + '"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</td>';
                        }
                        else if (arrHotellist[i].HotelCategory == '4EST' || arrHotellist[i].HotelCategory == '562' || arrHotellist[i].HotelCategory == '4') {
                            trRequest += '<td  id="' + arrHotellist[i].sid + '"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</td>';
                        }
                        else if (arrHotellist[i].HotelCategory == '5EST' || arrHotellist[i].HotelCategory == '563' || arrHotellist[i].HotelCategory == '5') {
                            trRequest += '<td  id="' + arrHotellist[i].sid + '"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;</td>';
                        }
                        else {
                            trRequest += '<td  id="' + arrHotellist[i].sid + '">' + arrHotellist[i].HotelCategory + '</td>'
                            // trRequest += '<td  id="' + arrHotellist[i].sid + '"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;</td>';
                        }


                        //Ratings End


                        //edit
                        //trRequest += '<td class="low-padding" ><span class="select compact full-width" tabindex="0">';
                        //trRequest += '<a href="#" class="select-value">Manage</a><span class="select-arrow"></span>'
                        //trRequest += '<span class="drop-down">';
                        //trRequest += '<a href="HotelOffer.aspx?sHotelID=' + arrHotellist[i].sid + '&HName=' + arrHotellist[i].HotelName + '">Create Offer</a>';
                        //var Offer = $.grep(OfferList, function (O) { return O.Hotel_Id == arrHotellist[i].sid; })
                        //      .map(function (O) { return O; });
                        //if (Offer.length != 0)
                        //    trRequest += '<a href="ViewOfferDetials.aspx?sHotelID=' + arrHotellist[i].sid + '&HName=' + arrHotellist[i].HotelName + '">View Offer</option>';
                        //else
                        //    trRequest += '';
                        //trRequest += '<a href="#">Review</a>';
                        //trRequest += '<a href="#">Delete</a>';
                        //trRequest += '</span></span></td>';
                        //edit end

                        //Update
                        trRequest += '<td class="align-center"><a  href="AddHotelDetails.aspx?sHotelID=' + arrHotellist[i].sid + '"><span class="icon-pencil icon-size2"></span></a></td>';
                        // End Update

                        //trRequest += '<td class="align-center"><p><label for="hotelAcitve' + i + '"><input type="checkbox" name="switch-tiny" id="hotelAcitve' + i + '" class="switch tiny mid-margin-right" value="1"></label></p></td>'; //(Active/Deactive)
                        //if (arrHotellist[i].Active == 'True')
                        //{
                        //    trRequest += '<td class="align-center"><a onclick="ActivateHotel(' + arrHotellist[i].sid + ',false)"><span class="icon-unlock icon-size2 blue"></span></a></td>';
                        //}                           
                        //else {
                        //    trRequest += '<td class="align-center"><a onclick="ActivateHotel(' + arrHotellist[i].sid + ',true)"><span class="icon-lock icon-size2 "></span></a></td>';
                        //}

                        if (arrHotellist[i].Active == 'True') {
                            trRequest += '<td class="align-center"><a onclick="ActivateHotel(' + arrHotellist[i].sid + ',false)"><span class="switch tiny mid-margin-right replacement checked" tabindex="0"><span class="switch-on" style=""><span style="">ON</span></span><span class="switch-off" style=""><span>OFF</span></span><span class="switch-button" style=""></span><input type="checkbox" name="switch-tiny" id="switch-tiny" class="" value="1" tabindex="-1"></span></a></td>';
                        }
                        else {
                            trRequest += '<td class="align-center"><a onclick="ActivateHotel(' + arrHotellist[i].sid + ',true)"><span class="switch tiny mid-margin-right replacement" tabindex="0"><span class="switch-on" style=""><span style="">ON</span></span><span class="switch-off" style=""><span>OFF</span></span><span class="switch-button" style=""></span><input type="checkbox" name="switch-tiny" id="switch-tiny" class="" value="1" tabindex="-1"></span></a></td>';
                        }
                        trRequest += '<td class="align-center"><a  href="ShowInventory.aspx?sHotelID=' + arrHotellist[i].sid + '&HName=' + arrHotellist[i].HotelName + '&HAddress=' + arrHotellist[i].HotelAddress + '"><span class="icon-read icon-size2"></span></a></td>';
                        trRequest += '<br></tr>';

                    }
                    trRequest += '</tbody>';
                    $("#tbl_HotelList").append(trRequest);

                }
                $("#tbl_HotelList").dataTable(
                    {
                        "bLength": false,
                         bSort: false, sPaginationType: 'full_numbers',
                    });
            }
            else if (result.retCode == 0) {
                $("#tbl_HotelList tbody").remove();
                var trRequest = '<tbody>';
                trRequest += '<tr><td align="center" style="padding-top: 2%" colspan="8"><span><b>No record found</b></span></td></tr>';
                trRequest += '</tbody>';
                $("#tbl_HotelList").append(trRequest);
            }

        }

    });

    //}
});

function ActivateHotel(HotelId, Status) {
    var hotel = HotelId;
    var s = status;
    var Data = { HotelId: HotelId, Status: Status };
    $.ajax({
        type: "POST",
        url: "HotelHandler.asmx/HotelActivation",
        data: JSON.stringify(Data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d)
            var ActiveHotel = obj.ActiveHotel;
            if (obj.retCode == 1) {
                if (ActiveHotel == "True") {
                    Success("Hotel Activated Successfully");
                    location.reload();
                }
                else {
                    Success("Hotel Deactivated Successfully");
                    location.reload();
                }

                //GetAllSeasons();
            }
            else {
                Success("Error while Season Activate");
            }
        },
    });
}

function DropRowwithId(drophotelId) {

    var trDropleft = "";
    var trDropmiddle = "";
    var trDropRight = "";
    var HotelAddress = $.grep(arrHotellist, function (p) { return p.sid == drophotelId; })
               .map(function (p) { return p.HotelAddress; }); trDropRight = "";
    var HotelName = $.grep(arrHotellist, function (p) { return p.sid == drophotelId; })
               .map(function (p) { return p.HotelName; }); trDropRight = "";
    var Room = $.grep(arrRoomList, function (p) { return p.sid == drophotelId; })
               .map(function (p) { return p.RoomType; }); trDropRight = "";
    var RoomId = $.grep(arrRoomList, function (p) { return p.sid == drophotelId; })
                         .map(function (p) { return p.RoomId; }); trDropRight = "";
    //Left
    trDropleft += '<strong>Address:</strong><br><label>' + HotelAddress + '</label>';


    //middle 
    //trDropmiddle += '<span class="button-group">';
    //trDropmiddle += '<a href="AddRooms.aspx?HotelCode=' + drophotelId + '&HotelName=' + HotelName + '" class="button icon-plus">Add Room</a>';
    //trDropmiddle += '<a href="RoomList.aspx?sHotelID=' + drophotelId + '&HName=' + HotelName + '" class="button icon-page-list">Room List</a>';
    //trDropmiddle += ' </span>'
    //trDropmiddle += '<br/><br/><span class="button-group">';
    //trDropmiddle += '<a href="AddRate.aspx?HotelCode=' + drophotelId + '&HotelName=' + HotelName + '" class="button icon-plus">Add Rates</a>';
    ////trDropmiddle += '<a href="RoomRates.aspx?sHotelID=' + drophotelId + '&HName=' + HotelName + '" class="button icon-page-list">Rates List</a>';
    //trDropmiddle += '<a href="Ratelist.aspx?sHotelID=' + drophotelId + '&HName=' + HotelName + '" class="button icon-page-list">Rates List</a>';
    //trDropmiddle += ' </span>'
    //trDropmiddle += '<br/><br/><span class="button-group">';
    //trDropmiddle += '<a href="HotelInventory.aspx?sHotelID=' + drophotelId + '&HName=' + HotelName + '" class="button icon-plus">Add Inventory</a>';

    //trDropmiddle += ' </span>'


    //trDropmiddle += '<span class="button-group">';
    //trDropmiddle += '<a href="AddRooms.aspx?sHotelID=' + drophotelId + '&HotelName=' + HotelName + '" class="button icon-squared-plus" title="Add Rooms"></a>';
    //trDropmiddle += '<a href="RoomList.aspx?sHotelID=' + drophotelId + '&HName=' + HotelName + '" class="button icon-list" title="Room List"></a>';
    //trDropmiddle += '<a href="AddRate.aspx?HotelCode=' + drophotelId + '&HotelName=' + HotelName + '" class="button icon-pages" title="Add Rate"></a>';
    //trDropmiddle += '<a href="Ratelist.aspx?sHotelID=' + drophotelId + '&HName=' + HotelName + '" class="button icon-list-add" title="Rate List"></a>';
    //trDropmiddle += '<a href="HotelInventory.aspx?sHotelID=' + drophotelId + '&HName=' + HotelName + '" class="button icon-page-list" title="Add Inventory"></a>';
    //trDropmiddle += '</span>';

    //trDropRight += '<span class="select compact full-width focus tracked" tabindex="0">';
    //trDropRight += '<a href="#" class="select-value">Manage</a>';
    //trDropRight += '<span class="select-arrow"></span>';
    //trDropRight += '<span class="drop-down"><a href="AddRooms.aspx?HotelCode=' + drophotelId + ';HotelName=' + HotelName + '">Edit</a>';
    //trDropRight += '<a href="AddRate.aspx?HotelCode=18 &amp;HotelName=Arma Executive&amp;RoomId=6&amp;RoomType=General">Add Rates</a>';
    //trDropRight += '</span></span>';
    //trDropmiddle += '';
    //trDropmiddle += '';


    //Right
    trDropmiddle += '<h4 class="underline">Rooms List</h4><ul>'
    if (Room.length > 0) {
        for (var i = 0; i < Room.length; i++) {
            var RoomIdNew = $.grep(arrRoomList, function (p) { return p.RoomType == Room[i]; })
                     .map(function (p) { return p.RoomId; });
            trDropmiddle += '<br>';
            trDropmiddle += '<li title="Tooltip on top">' + Room[i] + '';
            trDropmiddle += '<div class="button-group compact" style="float:right">';
            trDropmiddle += '<a href="#" class="button icon-pencil" title="Edit">Edit</a>';
            trDropmiddle += '<a href="#" class="button icon-gear" title="Other actions"></a>';
            trDropmiddle += '<a href="#" class="button icon-trash" title="Delete"></a>';
            trDropmiddle += '</div>';
            trDropmiddle += '  </li>';
            // trDropmiddle += ' <span class="button blue-gradient glossy with-tooltip" title="Tooltip on top" style="width: 120px">Top</span>';
            //<span style="margin-left:8%" class="select compact focus tracked" tabindex="0"></span><a href="#" class="select-value">Manage</a><span class="select-arrow"></span><span class="drop-down"><a href="AddRooms.aspx?HotelCode=18 &amp;HotelName=Arma Executive&amp;RoomId=17">Edit</a><a href="AddRate.aspx?HotelCode=18 &amp;HotelName=Arma Executive&amp;RoomId=6&amp;RoomType=General">Add Rates</a><a href="HotelInventory.aspx?HotelCode=' + drophotelId + '&HotelName=' + HotelName + '&RoomType=' + Room[i] + '&HotelAddress=' + HotelAddress + '">Inventory</a></span></span>
        }
    }
    else {
        trDropmiddle += '<li>No Room Available</li>';
    }
    //trDropmiddle += '</ul>'

    $('#DropLeft').append(trDropleft);
    $('#Dropmiddle').append(trDropmiddle);
    $('#DropRight').append(trDropRight);

}

//complex modal
function GetMappedHotels() {
    GetMappedHotels();
    $.modal({
        content:
                 '<p class="button-height inline-label ">' +
                           '<label for="input-3" class="label">Facilities:</label>' +
                                 '<select id="ddl_HtlFacilities"  class="full-width select multiple-as-single easy-multiple-selection allow-empty blue-gradient check-list" multiple>' +
                                       '<option  value="" selected="selected" disabled>Select Facilities</option>' +
                            '</select>' +
                  '</p>' +
                  '<p class="button-height align-right">' +
						'<button onclick="AddtoTextArea();"  class="button black-gradient glossy">Add</button>' +
				 '</p>' +
                  '<p class="button-height inline-label ">' +
                         '<label class="label">Present Facilities:</label>' +
                         '<textarea id="addedFacilities" class="input full-width align-right">' + arr + '</textarea>' +
                 '</p>' +
                 '<br><p class="button-height align-right">' +
						'<button onclick="SaveFacilities(' + HotelCode + ');"  class="button black-gradient glossy">Save</button>' +
                         '<label id="lblFacilitiesMsg" style="color:red;display:none">Facilities Added</button>' +
				 '</p>',

        title: 'Add Facilities',
        width: 500,
        height: 200,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        buttonsLowPadding: true
    });

};


function AddRatings(HotelCode, Ratings) {
    $.modal({
        content:
                 '<p class="button-height inline-label ">' +
                           '<label for="input-3" class="label">Existing Ratings:</label>' +
                           '<input  class="input full-width align-right" value="' + Ratings + '" readonly>' +
                  '</p>' +
                  '<p class="button-height inline-label ">' +
                           '<label for="input-3" class="label">New Ratings:</label>' +
                           '<input id ="addedRatings" class="input full-width align-right" >' +
                  '</p>' +
                 '<br><p class="button-height align-right">' +
						'<button onclick="SaveRatings(' + HotelCode + ');"  class="button black-gradient glossy">Save</button><br>' +
                        '<label id="lblRatingsMsg" style="color:red;display:none">Saved Ratings</button>' +
				 '</p>',

        title: 'Add Facilities',
        width: 500,
        height: 200,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        buttonsLowPadding: true
    });

};

function ShowRow(dropID) {
    $(".row-drop").css("display", "none");
    $('#DropRow' + dropID).css("display", "");
}

function UpdateImage(HotelCode, Url) {
    $.modal({
        content:
                 '<p class="button-height inline-label ">' +
                           '<label for="input-3" class="label">Existing Ratings:</label>' +
                           '<input  class="input full-width align-right" value="' + Url + '" readonly>' +
                  '</p>' +
                  '<p class="button-height inline-label ">' +
                           '<label for="input-3" class="label">New Ratings:</label>' +
                           '<input id ="addedUrl" class="input full-width align-right" >' +
                  '</p>' +
                 '<br><p class="button-height align-right">' +
						'<button onclick="SaveImageUrl(' + HotelCode + ');"  class="button black-gradient glossy">Save</button><br>' +
                        '<label id="lblImageMsg" style="color:red;display:none">Image Saved </button>' +
				 '</p>',

        title: 'Add Facilities',
        width: 500,
        height: 200,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        buttonsLowPadding: true
    });

};



function fnFacilities() {

    var HtlFacilities = document.getElementsByClassName('HtlFacilities');

    for (var i = 0; i < HtlFacilities.length; i++) {
        if (HtlFacilities[i].selected == true) {
            selectedFacilities[i] = HtlFacilities[i].value;
            pFacilities += selectedFacilities[i] + ',';
        }


    }


}

function AddtoTextArea() {
    fnFacilities();
    var sFacilities = pFacilities.replace(/^,|,$/g, '');
    var nFacilities = arr + ',' + sFacilities
    $('#addedFacilities').val(nFacilities);
    // document.getElementById("ddl_HtlFacilities").disabled = true;
}



function getFacilities() {
    $.ajax({
        type: "POST",
        url: "HotelHandler.asmx/GetFacilities",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrFacilitiesList = result.HotelFacilities;
                if (arrFacilitiesList.length > 0) {
                    $("#ddl_HtlFacilities").empty();
                    var ddlRequest = '<option selected="selected" value="-" disabled>Select Facilities</option>';
                    var j = 0;
                    for (i = 0; i < arrFacilitiesList.length; i++) {
                        ddlRequest += '<option type="checkbox" class="HtlFacilities" value="' + arrFacilitiesList[i].HotelFacilityName + '">' + arrFacilitiesList[i].HotelFacilityName + '</option>';

                    }
                    $("#ddl_HtlFacilities").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });

}

function SaveFacilities(HotelCode) {
    var sFacilities = $('#addedFacilities').val();
    var sHotelId = HotelCode;
    var data = {
        sHotelId: sHotelId,
        sFacilities: sFacilities
    };
    $.ajax({
        type: "POST",
        url: "HotelHandler.asmx/SaveFacilities",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                document.getElementById('lblFacilitiesMsg').style.display = "";
            }
        },
        error: function () {
        }
    });
}

function SaveRatings(HotelCode) {
    var sRatings = $('#addedRatings').val();
    var sHotelId = HotelCode;
    var data = {
        sHotelId: sHotelId,
        sRatings: sRatings
    };
    $.ajax({
        type: "POST",
        url: "HotelHandler.asmx/SaveRatings",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                document.getElementById('lblRatingsMsg').style.display = "";
            }
        },
        error: function () {
        }
    });
}


function SaveImageUrl(HotelCode) {
    var addedUrl = $('#addedUrl').val();
    var sHotelId = HotelCode;
    var data = {
        sHotelId: sHotelId,
        addedUrl: addedUrl
    };
    $.ajax({
        type: "POST",
        url: "HotelHandler.asmx/SaveImageUrl",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                document.getElementById('lblImageMsg').style.display = "";
            }
        },
        error: function () {
        }
    });
}