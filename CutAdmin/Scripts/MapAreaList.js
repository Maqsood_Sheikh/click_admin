﻿$(function () {
  
    LoadAreaMap();

});


function LoadAreaMap() {
    // $("#tbl_AreaGroupMap").dataTable().fnClearTable();
    //$("#tbl_AreaGroupMap").dataTable().fnDestroy();

    $.ajax({
        type: "POST",
        url: "MapLocationHandler.asmx/LoadAreaMap",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var AreaGroupMap = result.areaGroup;
            console.log(result);
            if (result.retCode == 1) {
                var tr = '';
                for (var i = 0; i < AreaGroupMap.length; i++) {
                    var SrNo = i + 1;
                    tr += '<tr>';
                    tr += '<td>' + SrNo + '</td>';
                    tr += '<td>' + AreaGroupMap[i].DestinationID + '</td>';
                    tr += '<td>' + AreaGroupMap[i].Name + '</td>';
                    tr += '<td>' + AreaGroupMap[i].CountryCode + '</td>';
                    //tr += '<td>' + AreaGroupMap[i].GroupID + '</td>';
                   
                    if (AreaGroupMap[i].GTA != null)
                        tr += '<td>' + AreaGroupMap[i].GTA + '</td>';
                    else
                        tr += '<td>' + "-" + '</td>';
                    if (AreaGroupMap[i].HB != null)
                        tr += '<td>' + AreaGroupMap[i].HB + '</td>';
                    else
                        tr += '<td>' + "-" + '</td>';
                    if (AreaGroupMap[i].DOTW != null)
                        tr += '<td>' + AreaGroupMap[i].DOTW + '</td>';
                    else
                        tr += '<td>' + "-" + '</td>';

                    if (AreaGroupMap[i].MGH != null)
                        tr += '<td>' + AreaGroupMap[i].MGH + '</td>';
                    else
                        tr += '<td>' + "-" + '</td>';
                    if (AreaGroupMap[i].TBO != null)
                        tr += '<td>' + AreaGroupMap[i].TBO + '</td>';
                    else
                        tr += '<td>' + "-" + '</td>';
                    if (AreaGroupMap[i].GRN != null)
                        tr += '<td>' + AreaGroupMap[i].GRN + '</td>';
                    else
                        tr += '<td>' + "-" + '</td>';
                  
                    tr += '<td align="center"><a style="cursor:pointer"  href="MapArea.aspx?id='+AreaGroupMap[i].Sid+'"><span class="icon-pencil icon-size2" title="Edit"></span></a></td>'
                    tr += '</tr>';


                    //html += '<td style="width:20%" align="center"><a style="cursor:pointer" href="AddActivity.aspx?id=' + arrActivity[i].Aid + '"><span class="icon-pencil icon-size2" title="Edit"></span></a> &nbsp; | &nbsp; <a style="cursor:pointer" onclick="Delete(\'' + arrActivity[i].Aid + '\',\'' + arrActivity[i].Name + '\')"><span class="icon-trash" title="Delete"></span></a></td>'
                    //<td style="width:10%" align="center" class=" "><a style="cursor:pointer" onclick="Rates('0MN','Half Day City Tour','Deira','Dubai^','UAE')"><span class="icon-pencil icon-size2" title="Edit Rates"></span></a></td>
                }
                $("#tbl_AreaGroupMap tbody").append(tr);

                $('[data-toggle="tooltip"]').tooltip()

                $("#tbl_AreaGroupMap").dataTable({
                     bSort: false, sPaginationType: 'full_numbers',
                });
            }
        },
        error: function () {
        }
    });
}