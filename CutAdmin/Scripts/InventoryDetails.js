﻿var arrDate = new Array();
var arrInventory = new Array();
function GetInventory() {
    $("#div_tbl_InvList").empty();
    var Startdt = $('#datepicker_start').val();
    var Enddt = $('#datepicker_end').val();
    $.ajax({
        type: "Post",
        url: "../Inventoryhandler.asmx/GetInventory",
        data: JSON.stringify({ Startdt: Startdt, Enddt: Enddt }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            arrDate = result.ListDates;
            arrInventory = result.arrInventory;
            var html = '';
            html += '<table class="table responsive-table  colorTable responsive-table-on" id="tbl_HotelList"><thead>';
            html += '<tr>';
            html += '<th >Hotel Name</th>';
            for (var i = 0; i < arrDate.length; i++) {
                html += '<th class="align-center header" class="align-center header">' + arrDate[i] + '</th>';
            }
            html += '</tr>';
            html += '</thead>';
            if(result.retCode == 1)
            {
                html += '<tbody>';
                for (var i = 0; i < arrInventory.length; i++) {
                    html += '<tr>';
                    html += '<th><div class=" SelBox"><label>' + arrInventory[i].HotelName + '</label> <input type="hidden" class="HotelCode" value="' + arrInventory[i].HotelCode + '" /></div></th>';
                    for (var d = 0; d < arrDate.length; d++) {
                        html += '<td class="align-center header" class="align-center header">-</td>';
                    }
                    html += '</tr>';
                }
                html += '</tbody>';
              
            }
            html += '</table>';
            $("#div_tbl_InvList").append(html)
            $("#tbl_HotelList").dataTable({
                bSort: false,
                sPaginationType: 'full_numbers',
                sSearch: false
            });
            $("#tbl_HotelList_wrapper").addClass("respTable");
            GenrateTable();
        },
        error: function() {

        },
    });
}

function GenrateTable() {
    // Call template init (optional, but faster if called manually)
    $.template.init();
    $('#tbl_HotelList').tablesorter({
        headers: {
            0: { sorter: false },
            6: { sorter: false }
        }
    }).on('click', 'tbody th', function (event) {
        var drophotelId = this.id;
        // Do not process if something else has been clicked
        if (event.target !== this) {
            return;
        }
        var tr = $(this).parent(),
            row = tr.next('.row-drop'),
            rows;
        // If click on a special row
        if (tr.hasClass('row-drop')) {
            return;
        }
        // If there is already a special row
        if (row.length > 0) {
            // Un-style row
            tr.children().removeClass('anthracite-gradient glossy');
            // Remove row
            row.remove();
            return;
        }
        // Remove existing special rows
        rows = tr.siblings('.row-drop');
        if (rows.length > 0) {
            // Un-style previous rows
            rows.prev().children().removeClass('anthracite-gradient glossy');
            // Remove rows
            rows.remove();
        }
        // Style row
        tr.children().addClass('anthracite-gradient glossy');
        // Add fake row
        var HotelCode = $($(tr).find(".HotelCode")[0]).val();
        var arrRoom = $.grep(arrInventory, function (p) { return p.HotelCode == HotelCode; })
               .map(function (p) { return p.ListRoom; })[0];
   
        for (var i = 0; i < arrRoom.length; i++) {
            $('<tr class="row-drop"  id="div_Class' + i + HotelCode + '" >' +
                //'<td  style="padding:0 ;" colspan="' + tr.children().length + '" >' +
                //'</td>' +
               '</tr>').insertAfter(tr);
            GetRoomDetails(arrRoom[i] ,i+HotelCode);
        }
      
    }).on('sortStart', function () {
        var rows = $(this).find('.row-drop');
        if (rows.length > 0) {
            rows.prev().children().removeClass('anthracite-gradient glossy');
            rows.remove();
        }
    });
}

function GetRoomDetails(arrRoom, HotelCode) {
    try {
        var html = '';
        html += '<th style="padding:9px 13px;" ><div class="SelBox"><div class=" SelBox"><label>' + arrRoom.RoomTypeName + '</label></div></th>'
        for (var dt = 0; dt < arrRoom.Dates.length; dt++) {
            var cssClass = "";
            if (arrRoom.Dates[dt].InventoryType == "FreeSale")
                cssClass = "OrangeTD";
            else if (arrRoom.Dates[dt].InventoryType == "Allocation")
                cssClass = "YellowTD";
            else if (arrRoom.Dates[dt].InventoryType == "Allotment")
                cssClass = "GreenTD";
            if (cssClass != "")
                html += '<td class="' + cssClass + '"><span class="AddTxt"> ' + arrRoom.Dates[dt].Type + ' <span><input type="checkbox" name="checkbox-2" id="checkbox-2" value="1" class="checkbox" tabindex="-1"></span></span></span></span></span></td>'
            else
                html += '<td>-</td> '
        }
        $("#div_Class" + HotelCode).append(html)

    } catch (e) {

    }
}