﻿

$(function () {
   var HotelCode = getParameterByName('sHotelID');
   var HotelName = getParameterByName('HName');
   GetRatelist(HotelCode, HotelName)
   GetButton(HotelCode, HotelName)
})

function GetButton(HotelCode, HotelName)
{
    var html = '';
    html += '<a target="_blank" href="AddRate.aspx?sHotelID=' + HotelCode + '&HotelName=' + HotelName + '"" class="button anthracite-gradient"><i class="fa fa-plus"></i> &nbsp;Add New</a>                                                                                                  ';
    //html += '               <button type="button" class="button blue-gradient glossy" id="btn_Ratelist" onclick="">+Add New</button>  ';
    //html += '           </a>                                                                                                          ';
    //html += '           <br>                                                                                                          ';
    //html += '           <br>                                                                                                          ';
    $("#div_btn").append(html);
}                                                                                                                                   

function GetRatelist(HotelCode, HotelName) {
    $("#tbl_Ratelist").dataTable().fnClearTable();
    $("#tbl_Ratelist").dataTable().fnDestroy();
    // $("#tbl_Actlist tbody tr").remove();
    $.ajax({
        url: "handler/RoomHandler.asmx/GetRatelist",
        type: "post",
        data: '{"HotelCode":"' + HotelCode + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                arrRateList = result.RateList
                for (var i = 0; i < arrRateList.length; i++) {
                    var html = '';
                    html += '<tr><td style="width:3%" align="center">' + (i + 1) + '</td>'
                  //  html += '<td style="width:20%"  align="center">' + arrRateList[i].Supplier + ' </td>'
                    html += '<td style="width:10%"  align="center" title="\'' + arrRateList[i].ValidNationality + '\'">' + trancate_per(arrRateList[i].ValidNationality) + ' </td>'
                    html += '<td style="width:7%"  align="center">' + arrRateList[i].RateType + ' </td>'
                    html += '<td style="width:10%"  align="center">' + arrRateList[i].CurrencyCode + ' </td>'
                    html += '<td style="width:13%"  align="center">' + arrRateList[i].Checkin + ' </td>'
                    html += '<td style="width:13%"  align="center">' + arrRateList[i].Checkout + ' </td>'
                    html += '<td style="width:10%"  align="center">' + arrRateList[i].MealPlan + ' </td>'
                    if (arrRateList[i].DayWise == "No")
                    {
                        html += '<td style="width:10%"  align="center">' + arrRateList[i].RR + ' </td>'
                    }
                    else
                    {
                        html += '<td style="width:14%"  align="center">'
                        html += '<lable >DayWise Rate</lable>'
                        html += '    <span class="info-spot">'
                        html += '		<span class="icon-info-round"></span>'
                        html += '		<span class="info-bubble">'
                        
                        if (arrRateList[i].MonRR == null)
                            html += '<small>Mon:</small> - <br/>'
                        else
                            html += '<small>Mon:</small> ' + arrRateList[i].MonRR + '<br/>'

                        if (arrRateList[i].TueRR == null)
                            html += '<small>Tue:</small> - <br/>'
                        else
                            html += '<small>Tue:</small> ' + arrRateList[i].TueRR + '<br/>'

                        if (arrRateList[i].WedRR == null)
                            html += '<small>Wed:</small> - <br/>'
                        else
                            html += '<small>Wed:</small> ' + arrRateList[i].WedRR + '<br/>'

                        if (arrRateList[i].ThuRR == null)
                            html += '<small>Thu:</small> - <br/>'
                        else
                            html += '<small>Thu:</small> ' + arrRateList[i].ThuRR + '<br/>'

                        if (arrRateList[i].FriRR == null)
                            html += '<small>Fri:</small> - <br/>'
                        else
                            html += '<small>Fri:</small> ' + arrRateList[i].FriRR + '<br/>'

                        if (arrRateList[i].SatRR == null)
                            html += '<small>Sat:</small> - <br/>'
                        else
                            html += '<small>Sat:</small> ' + arrRateList[i].SatRR + '<br/>'

                        if (arrRateList[i].SunRR == null)
                            html += '<small>Sun:</small> - <br/>'
                        else
                            html += '<small>Sun:</small> ' + arrRateList[i].SunRR + '<br/>'

                      
                        html += '		</span>'
                        html += '	</span>'
                        html += '</td>'
                    }
                    
                    html += '<td style="width:10%"  align="center"><a target="_blank" style="cursor:pointer" class="button" href="EditRate.aspx?sHotelID=' + HotelCode + '&HotelName=' + HotelName + '&RateID=' + arrRateList[i].HotelRateID + '&RateType=' + arrRateList[i].RateType + '"><span class="icon-pencil" title="Edit Rates"></span></a></td>';
                   
                    // html += '<td style="width:10%" align="center"><a style="cursor:pointer" onclick="Rates(\'' + arrActivity[i].Aid + '\',\'' + arrActivity[i].Name + '\',\'' + arrActivity[i].Location + '\',\'' + arrActivity[i].City + '\',\'' + arrActivity[i].Country + '\')"><span class="icon-pencil icon-size2" title="Edit Rates"></span></a></td></tr>'
                   // html += '<td style="width:10%" align="center"><a style="cursor:pointer" onclick="Rates(\'' + arrActivity[i].Aid + '\',\'' + arrActivity[i].Name + '\',\'' + arrActivity[i].Location + '\',\'' + arrActivity[i].City + '\',\'' + arrActivity[i].Country + '\')"><span class="icon-pencil icon-size2" titl></span></a></td></tr>'

                    $("#tbl_Ratelist").append(html);
                }

                $('[data-toggle="tooltip"]').tooltip()

                $("#tbl_Ratelist").dataTable({
                     bSort: false, sPaginationType: 'full_numbers',
                });
            }
        }
    })
}
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function trancate_per(title) {
    var length = 20;
    if (title.length > length) {
        title = title.substring(0, length) + '...';
    }
    return title;
}