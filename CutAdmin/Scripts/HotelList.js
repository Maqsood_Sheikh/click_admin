﻿var arrFacilities = []; var arr;
var arrHotellist = new Array();
var arrFacilitiesList = new Array();
var pFacilities = "", selectedFacilities = [];
var arrRoomList = [];
var UserType = "";
$(document).ready(function () {
    GetHotelList()
});

function GetHotelList() {
    var trRequest = "";
    $("#tbl_HotelList").dataTable().fnClearTable();
    $("#tbl_HotelList").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/GetMappedHotels",
        //data: '{"dFrom":"' + dFrom + '","dTo":"' + dTo + '"}',
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrHotellist = result.MappedHotelList;
                OfferList = result.OfferList;
                RoomList = result.RoomList;
                arrRoomList = RoomList;
                arrUserType = UserType;
                SupplierId = result.SupplierId;
                //Hotels List
                if (arrHotellist.length > 0) {
                    for (i = 0; i < arrHotellist.length; i++) {
                        if (arrHotellist[i].Approved == false) {
                            continue;
                        }
                        arr = arrHotellist[i].HotelFacilities;
                        arrFacilities = arr.split(',');
                        trRequest += '<tr >';
                        trRequest += '<td style="cursor:pointer" id="' + arrHotellist[i].sid + '">' + arrHotellist[i].HotelName + '</td>';
                        trRequest += '<td style="cursor:pointer" id="' + arrHotellist[i].sid + '">' + arrHotellist[i].CountryId + '</td>';
                        trRequest += '<td style="cursor:pointer" id="' + arrHotellist[i].sid + '">' + arrHotellist[i].CityId + '</td>';
                        //trRequest += '<td id="' + arrHotellist[i].sid + '">' + arrHotellist[i].HotelCategory + '</td>';

                        // Ratings
                        if (arrHotellist[i].HotelCategory == 'Other' || arrHotellist[i].HotelCategory == '48055' || arrHotellist[i].HotelCategory == '0') {
                            trRequest += '<td style="cursor:pointer" id="' + arrHotellist[i].sid + '"><i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</td>';
                        }
                        else if (arrHotellist[i].HotelCategory == '1EST' || arrHotellist[i].HotelCategory == '559' || arrHotellist[i].HotelCategory == '1') {
                            trRequest += '<td style="cursor:pointer" id="' + arrHotellist[i].sid + '"><i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</td>';
                        }
                        else if (arrHotellist[i].HotelCategory == '2EST' || arrHotellist[i].HotelCategory == '560' || arrHotellist[i].HotelCategory == '2') {
                            trRequest += '<td style="cursor:pointer" id="' + arrHotellist[i].sid + '"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</td>';
                        }
                        else if (arrHotellist[i].HotelCategory == '3EST' || arrHotellist[i].HotelCategory == '561' || arrHotellist[i].HotelCategory == '3') {
                            trRequest += '<td style="cursor:pointer" id="' + arrHotellist[i].sid + '"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</td>';
                        }
                        else if (arrHotellist[i].HotelCategory == '4EST' || arrHotellist[i].HotelCategory == '562' || arrHotellist[i].HotelCategory == '4') {
                            trRequest += '<td style="cursor:pointer" id="' + arrHotellist[i].sid + '"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</td>';
                        }
                        else if (arrHotellist[i].HotelCategory == '5EST' || arrHotellist[i].HotelCategory == '563' || arrHotellist[i].HotelCategory == '5') {
                            trRequest += '<td style="cursor:pointer" id="' + arrHotellist[i].sid + '"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;</td>';
                        }
                        else {
                            trRequest += '<td style="cursor:pointer" id="' + arrHotellist[i].sid + '">' + arrHotellist[i].HotelCategory + '</td>'
                            // trRequest += '<td  id="' + arrHotellist[i].sid + '"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;</td>';
                        }


                        //Ratings End


                        //Update
                        trRequest += '<td class="align-center"><a  class="button" title="Edit" href="AddHotelDetails.aspx?sHotelID=' + arrHotellist[i].sid + '"><span class="icon-pencil"></span></a></td>';
                        // End Update

                        //trRequest += '<td class="align-center"><p><label for="hotelAcitve' + i + '"><input type="checkbox" name="switch-tiny" id="hotelAcitve' + i + '" class="switch tiny mid-margin-right" value="1"></label></p></td>'; //(Active/Deactive)
                        if (arrHotellist[i].Active == 'True') {
                            trRequest += '<td class="align-center"><input type="checkbox" id="chk' + arrHotellist[i].sid + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1"  checked  onclick="ActivateHotel(' + arrHotellist[i].sid + ',\'False\')"></td>';
                        }
                        else {
                            trRequest += '<td class="align-center"><input type="checkbox" id="chk' + arrHotellist[i].sid + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1"   onclick="ActivateHotel(' + arrHotellist[i].sid + ',\'True\')"></td>';
                        }
                        trRequest += '<td class="align-center"><a target="_blank" class="button" title="Inventory" href="ShowInventory.aspx?sHotelID=' + arrHotellist[i].sid + '&HName=' + arrHotellist[i].HotelName + '&SupplierId=' + SupplierId + '&HAddress=' + arrHotellist[i].HotelAddress + '"><span class="icon-read"></span></a></td>';
                      //  trRequest += '<td class="align-center"><a href="#" onclick="AddInventory(\'' + arrHotellist[i].sid + '\')" class="button" title="Inventory"><span class="icon-read"></span></a></td>';
                        trRequest += '<br></tr>';
                    }
                    $("#tbl_HotelList tbody").append(trRequest);
                    $(".tiny").click(function () {
                        $(this).find("input:checkbox").click();
                    })
                }
                $("#tbl_HotelList").dataTable(
                    {

                        sSort: true, sPaginationType: 'full_numbers',

                    });
                $("#tbl_HotelList").removeAttr("style");
            }
            else if (result.retCode == 0) {
                $("#tbl_HotelList").dataTable(
                    {

                        bSort: false, sPaginationType: 'full_numbers',

                    });
                $("#tbl_HotelList").removeAttr("style");
            }

        }

    });
}

function ActivateHotel(HotelId,  Status) {
    var hotel = HotelId;
    var s = status;
    var Data = { HotelId: HotelId, Status: Status };
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/HotelActivation",
        data: JSON.stringify(Data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d)
            var ActiveHotel = obj.ActiveHotel;
            if (obj.retCode == 1) {
                if (ActiveHotel == "True") {
                    Success("Hotel status has been changed successfully!");
                    GetHotelList()
                    //location.reload();
                }
                else {
                    Success("Hotel status has been changed successfully!");
                    GetHotelList()
                }

                //GetAllSeasons();
            }
            else {
                Success("Error while Season Activate");
            }
        },
    });
}

function DropRowwithId(drophotelId) {

    var trDropleft = "";
    var trDropmiddle = "";
    var trDropRight = "";
    var HotelAddress = $.grep(arrHotellist, function (p) { return p.sid == drophotelId; })
               .map(function (p) { return p.HotelAddress; }); trDropRight = "";
    var HotelName = $.grep(arrHotellist, function (p) { return p.sid == drophotelId; })
               .map(function (p) { return p.HotelName; }); trDropRight = "";
    var Room = $.grep(arrRoomList, function (p) { return p.sid == drophotelId; })
               .map(function (p) { return p.RoomType; }); trDropRight = "";
    var RoomId = $.grep(arrRoomList, function (p) { return p.sid == drophotelId; })
                         .map(function (p) { return p.RoomId; }); trDropRight = "";
    //Left
    trDropleft += '<strong>Address:</strong><br><label>' + HotelAddress + '</label>';


    //middle 
    //trDropmiddle += '<span class="button-group">';
    //trDropmiddle += '<a target="_blank" href="AddRooms.aspx?HotelCode=' + drophotelId + '&HotelName=' + HotelName + '" class="button icon-plus">Add Room</a>';
    //trDropmiddle += '<a target="_blank" href="RoomList.aspx?sHotelID=' + drophotelId + '&HName=' + HotelName + '" class="button icon-page-list">Room List</a>';
    //trDropmiddle += ' </span>'
    //trDropmiddle += '<br/><br/><span class="button-group">';
    //trDropmiddle += '<a target="_blank" href="AddRate.aspx?HotelCode=' + drophotelId + '&HotelName=' + HotelName + '" class="button icon-plus">Add Rates</a>';
    ////trDropmiddle += '<a target="_blank" href="RoomRates.aspx?sHotelID=' + drophotelId + '&HName=' + HotelName + '" class="button icon-page-list">Rates List</a>';
    //trDropmiddle += '<a target="_blank" href="Ratelist.aspx?sHotelID=' + drophotelId + '&HName=' + HotelName + '" class="button icon-page-list">Rates List</a>';
    //trDropmiddle += ' </span>'
    //trDropmiddle += '<br/><br/><span class="button-group">';
    //trDropmiddle += '<a target="_blank" href="HotelInventory.aspx?sHotelID=' + drophotelId + '&HName=' + HotelName + '" class="button icon-plus">Add Inventory</a>';

    //trDropmiddle += ' </span>'


    trDropmiddle += '<span class="button-group">';
    trDropmiddle += '<a href="AddRooms.aspx?sHotelID=' + drophotelId + '&HotelName=' + HotelName + '" class="button icon-squared-plus" title="Add Rooms"></a>';
    trDropmiddle += '<a href="RoomList.aspx?sHotelID=' + drophotelId + '&HName=' + HotelName + '" class="button icon-list" title="Room List"></a>';
    if (UserType != "Admin")
    {
        if (Room.length != 0)
            trDropmiddle += '<a href="AddRate.aspx?sHotelID=' + drophotelId + '&HotelName=' + HotelName + '" class="button icon-pages" title="Add Rate"></a>';
        trDropmiddle += '<a href="Ratelist.aspx?sHotelID=' + drophotelId + '&HName=' + HotelName + '" class="button icon-list-add" title="Rate List"></a>';
        trDropmiddle += '<a href="HotelInventory.aspx?sHotelID=' + drophotelId + '&HName=' + HotelName + '" class="button icon-page-list" title="Add Inventory"></a>';
    }
    
    trDropmiddle += '</span>';

    //trDropRight += '<span class="select compact full-width focus tracked" tabindex="0">';
    //trDropRight += '<a target="_blank" href="#" class="select-value">Manage</a>';
    //trDropRight += '<span class="select-arrow"></span>';
    //trDropRight += '<span class="drop-down"><a target="_blank" href="AddRooms.aspx?HotelCode=' + drophotelId + ';HotelName=' + HotelName + '">Edit</a>';
    //trDropRight += '<a target="_blank" href="AddRate.aspx?HotelCode=18 &amp;HotelName=Arma Executive&amp;RoomId=6&amp;RoomType=General">Add Rates</a>';
    //trDropRight += '</span></span>';
    //trDropmiddle += '';
    //trDropmiddle += '';


    //Right
    trDropRight += '<h4 class="underline">Rooms List</h4><ul>'
    if (Room.length > 0) {
        for (var i = 0; i < Room.length; i++) {
            var RoomIdNew = $.grep(arrRoomList, function (p) { return p.RoomType == Room[i]; })
                     .map(function (p) { return p.RoomId; });
            trDropRight += '<li title="Tooltip on top">' + Room[i] + '</li>';
            // trDropmiddle += ' <span class="button blue-gradient glossy with-tooltip" title="Tooltip on top" style="width: 120px">Top</span>';
            //<span style="margin-left:8%" class="select compact focus tracked" tabindex="0"></span><a target="_blank" href="#" class="select-value">Manage</a><span class="select-arrow"></span><span class="drop-down"><a target="_blank" href="AddRooms.aspx?HotelCode=18 &amp;HotelName=Arma Executive&amp;RoomId=17">Edit</a><a target="_blank" href="AddRate.aspx?HotelCode=18 &amp;HotelName=Arma Executive&amp;RoomId=6&amp;RoomType=General">Add Rates</a><a target="_blank" href="HotelInventory.aspx?HotelCode=' + drophotelId + '&HotelName=' + HotelName + '&RoomType=' + Room[i] + '&HotelAddress=' + HotelAddress + '">Inventory</a></span></span>
        }
    }
    else {
        trDropRight += '<li>No Room Available</li>';
    }
    //trDropmiddle += '</ul>'

    $('#DropLeft').append(trDropleft);
    $('#Dropmiddle').append(trDropmiddle);
    $('#DropRight').append(trDropRight);
    // var sHotelId = drophotelId;
    // getRooms(drophotelId);
}

//complex modal
function GetMappedHotels() {
    GetMappedHotels();
    $.modal({
        content:
                 '<p class="button-height inline-label ">' +
                           '<label for="input-3" class="label">Facilities:</label>' +
                                 '<select id="ddl_HtlFacilities"  class="full-width select multiple-as-single easy-multiple-selection allow-empty blue-gradient check-list" multiple>' +
                                       '<option  value="" selected="selected" disabled>Select Facilities</option>' +
                            '</select>' +
                  '</p>' +
                  '<p class="button-height align-right">' +
						'<button onclick="AddtoTextArea();"  class="button black-gradient glossy">Add</button>' +
				 '</p>' +
                  '<p class="button-height inline-label ">' +
                         '<label class="label">Present Facilities:</label>' +
                         '<textarea id="addedFacilities" class="input full-width align-right">' + arr + '</textarea>' +
                 '</p>' +
                 '<br><p class="button-height align-right">' +
						'<button onclick="SaveFacilities(' + HotelCode + ');"  class="button black-gradient glossy">Save</button>' +
                         '<label id="lblFacilitiesMsg" style="color:red;display:none">Facilities Added</button>' +
				 '</p>',

        title: 'Add Facilities',
        width: 500,
        height: 200,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        buttonsLowPadding: true
    });

};


function AddRatings(HotelCode, Ratings) {
    $.modal({
        content:
                 '<p class="button-height inline-label ">' +
                           '<label for="input-3" class="label">Existing Ratings:</label>' +
                           '<input  class="input full-width align-right" value="' + Ratings + '" readonly>' +
                  '</p>' +
                  '<p class="button-height inline-label ">' +
                           '<label for="input-3" class="label">New Ratings:</label>' +
                           '<input id ="addedRatings" class="input full-width align-right" >' +
                  '</p>' +
                 '<br><p class="button-height align-right">' +
						'<button onclick="SaveRatings(' + HotelCode + ');"  class="button black-gradient glossy">Save</button><br>' +
                        '<label id="lblRatingsMsg" style="color:red;display:none">Saved Ratings</button>' +
				 '</p>',

        title: 'Add Facilities',
        width: 500,
        height: 200,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        buttonsLowPadding: true
    });

};

function ShowRow(dropID) {
    $(".row-drop").css("display", "none");
    $('#DropRow' + dropID).css("display", "");
}

function UpdateImage(HotelCode, Url) {
    $.modal({
        content:
                 '<p class="button-height inline-label ">' +
                           '<label for="input-3" class="label">Existing Ratings:</label>' +
                           '<input  class="input full-width align-right" value="' + Url + '" readonly>' +
                  '</p>' +
                  '<p class="button-height inline-label ">' +
                           '<label for="input-3" class="label">New Ratings:</label>' +
                           '<input id ="addedUrl" class="input full-width align-right" >' +
                  '</p>' +
                 '<br><p class="button-height align-right">' +
						'<button onclick="SaveImageUrl(' + HotelCode + ');"  class="button black-gradient glossy">Save</button><br>' +
                        '<label id="lblImageMsg" style="color:red;display:none">Image Saved </button>' +
				 '</p>',

        title: 'Add Facilities',
        width: 500,
        height: 200,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        buttonsLowPadding: true
    });

};



function fnFacilities() {

    var HtlFacilities = document.getElementsByClassName('HtlFacilities');

    for (var i = 0; i < HtlFacilities.length; i++) {
        if (HtlFacilities[i].selected == true) {
            selectedFacilities[i] = HtlFacilities[i].value;
            pFacilities += selectedFacilities[i] + ',';
        }


    }


}

function AddtoTextArea() {
    fnFacilities();
    var sFacilities = pFacilities.replace(/^,|,$/g, '');
    var nFacilities = arr + ',' + sFacilities
    $('#addedFacilities').val(nFacilities);
    // document.getElementById("ddl_HtlFacilities").disabled = true;
}



function getFacilities() {
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/GetFacilities",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrFacilitiesList = result.HotelFacilities;
                if (arrFacilitiesList.length > 0) {
                    $("#ddl_HtlFacilities").empty();
                    var ddlRequest = '<option selected="selected" value="-" disabled>Select Facilities</option>';
                    var j = 0;
                    for (i = 0; i < arrFacilitiesList.length; i++) {
                        ddlRequest += '<option type="checkbox" class="HtlFacilities" value="' + arrFacilitiesList[i].HotelFacilityName + '">' + arrFacilitiesList[i].HotelFacilityName + '</option>';

                    }
                    $("#ddl_HtlFacilities").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });

}

function SaveFacilities(HotelCode) {
    var sFacilities = $('#addedFacilities').val();
    var sHotelId = HotelCode;
    var data = {
        sHotelId: sHotelId,
        sFacilities: sFacilities
    };
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/SaveFacilities",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                document.getElementById('lblFacilitiesMsg').style.display = "";
            }
        },
        error: function () {
        }
    });
}

function SaveRatings(HotelCode) {
    var sRatings = $('#addedRatings').val();
    var sHotelId = HotelCode;
    var data = {
        sHotelId: sHotelId,
        sRatings: sRatings
    };
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/SaveRatings",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                document.getElementById('lblRatingsMsg').style.display = "";
            }
        },
        error: function () {
        }
    });
}

function getRooms(drophotelId) {


    $.ajax({
        type: "POST",
        url: "../RoomHandler.asmx/GetRooms",
        data: '{"sHotelId":"' + drophotelId + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                //arrHotellist = result.MappedHotelList;
                arrRoomTypes = result.RoomTypeList;
                arrRoomList = result.RoomList;
                arrAmenities = result.RoomAmenityList;
                $("#tbl_RoomList tbody").empty();
                //Hotels List
                var trRequest = "";
                if (arrRoomList.length > 0) {
                    for (i = 0; i < arrRoomList.length; i++) {
                        trRequest += '<tr>';
                        for (j = 0; j < arrRoomTypes.length; j++) {


                        }


                    }

                }

            }
            else if (result.retCode == 0) {

            }

        }

    });
}


function SaveImageUrl(HotelCode) {
    var addedUrl = $('#addedUrl').val();
    var sHotelId = HotelCode;
    var data = {
        sHotelId: sHotelId,
        addedUrl: addedUrl
    };
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/SaveImageUrl",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                document.getElementById('lblImageMsg').style.display = "";
            }
        },
        error: function () {
        }
    });
}


function DeleteModel(sid, name) {

    $.modal({

        content: '<p style="font-size:15px" class="avtiveDea">Are you sure you want to Delete <span class=\"orange\"> "' + name + '</span>?</p>' + '<p class="text-alignright text-popBtn"><button type="button" class="button anthracite-gradient" onclick="DeleteHote(\'' + sid + '\')">OK</button></p>',
        width: 500,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Cancel': {
                classes: 'anthracite-gradient glossy',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: false
    });
}

function DeleteHote(sid) {
    $.ajax({
        url: "../HotelHandler.asmx/DeleteHote",
        type: "post",
        data: '{"Id":"' + sid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                Success("Hotel Deleted successfully!")
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            
            } else if (result.retCode == 0) {
                Success("Something went wrong while processing your request! Please try again.");
            }
        },
        error: function () {
            Success('Error occured while processing your request! Please try again.');
        }
    });
}

 