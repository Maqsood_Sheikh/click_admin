﻿ListRoom = new Array();

function ValidOccupancy() {
    var nationality = $('#sel_ValidNationality option:selected').val();
    var arrRates = new Array();
    var Supplier = $("#tabs .active").text();

    for (var i = 0; i < arrSuppliers.length; i++) {
        arrRates = $.grep(arrRoomRates, function (p) { return p.Name == arrSuppliers[i] && p.Nationality == nationality; })
                    .map(function (p) { return p; })

        if (Supplier == arrSuppliers[i]) {
            for (var rates = 0; rates < arrRates.length; rates++) {
                var noRooms = 0;
                ListRoom = new Array();

                for (var rooms = 0; rooms < arrRates[rates].RoomOccupancy.length; rooms++) {
                    if (arrRates[rates].RoomOccupancy[rooms].Rooms.length != 0) {
                        for (var detail = 0; detail < arrRates[rates].RoomOccupancy[rooms].Rooms.length; detail++) {
                            var arrDate = new Array();
                            var Name = arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomTypeName;
                            noRooms = $("#sel_" + i + "_" + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomTypeId + "_" + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomDescription).val();
                            if (noRooms != 0) {
                                var arrData = $(".Supplier" + i + "_" + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomTypeId + "_" + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomDescription);
                                if (arrData.length != 0) {
                                    for (var c = 0; c < arrData.length; c++) {

                                        // var ndListDate = $(arrData[c]).find(".checkbox");
                                        var sID = $($(arrData[c]).find("input:checkbox")).val().split('_')[1];
                                        var sRoomID = $($(arrData[c]).find("input:checkbox")).val().split('_')[4];
                                        var MealPlan = $($(arrData[c]).find("input:checkbox")).val().split('_')[5];
                                        var IsChecked = $(arrData[c]).hasClass("checked");
                                        if (IsChecked) {
                                            var sDate = $($(arrData[c]).find("input:checkbox")).val().split('_')[0]
                                            arrDate.push(sDate)
                                        }

                                    }
                                    if (arrDate.length != 0)
                                        ListRoom.push({ ID: sID, RoomID: sRoomID, noRooms: noRooms, Name: Name, MealPlan: MealPlan, Date: arrDate });
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    GenerateHtml();
}

var n = 2;
function GenerateHtml() {
    var ModalContent = "";

    ModalContent += '<div class="standard-tabs margin-bottom" id="add-tabs">'
    ModalContent += '<div class="respTable">'
    ModalContent += '<table class="table responsive-table" id="tbl_RoomOccupancy">'
    ModalContent += '<thead>'
    ModalContent += '<tr>'
    ModalContent += '<th scope="col" class="align-center">Room Type</th>'
    ModalContent += '<th scope="col" class="align-center">Adult</th>'
    ModalContent += '<th scope="col" class="align-center">Child</th>'
    ModalContent += '</tr>'
    ModalContent += '</thead>'
    n = n - 1;
    for (var i = 0; i < ListRoom.length; i++) {
        for (var j = 0; j < parseFloat(ListRoom[i].noRooms) ; j++) {
            if (ListRoom.length != 0) {

                ModalContent += '<tbody>'
                ModalContent += '<tr>'

                ModalContent += '<td class="align-center">' + ListRoom[i].Name + '(' + ListRoom[i].MealPlan + ')</td>'

                ModalContent += '<td class="align-center">'
                ModalContent += '<select id="SelGen_Adults' + n + 'd" name="validation-select" class="select" tabindex="-1">'
                ModalContent += '<option selected="selected" value="1">1</option>'
                ModalContent += '<option value="2">2</option>'
                ModalContent += '<option value="3">3</option>'
                ModalContent += '<option value="4">4</option>'
                ModalContent += '<option value="5">5</option>'
                ModalContent += '<option value="6">6</option>'
                ModalContent += '<option value="7">7</option>'
                ModalContent += '<option value="8">8</option>'
                ModalContent += '<option value="9">9</option>'
                ModalContent += '<option value="10">10</option>'
                ModalContent += '</select>'
                ModalContent += '</td>'

                ModalContent += '<td class="align-center">'
                ModalContent += '<select id="SelGen_Children' + n + '" name="validation-select" class="select" tabindex="-1" onchange="AddAgeColumn(this.value,' + n + ')">'
                ModalContent += '<option selected="selected" value="0">0</option>'
                ModalContent += '<option value="1">1</option>'
                ModalContent += '<option value="2">2</option>'
                ModalContent += '<option value="3">3</option>'
                ModalContent += '<option value="4">4</option>'
                ModalContent += '</select>'
                ModalContent += '<span id="Child_Div' + n + '">'
                ModalContent += '</span>'
                ModalContent += '</td>'
                
                ModalContent += '</tr>'
                ModalContent += '</tbody>'

                n = n + 1;
            } else {
                ModalContent += '<tbody>'
                ModalContent += '</tbody>'
            }
        }
    }

    ModalContent += '</table>'
    ModalContent += '<input type="button" class="button anthracite-gradient" onclick="SearchRatesOccupancy()" value="Search Rates" style="margin-top: 20px; cursor: pointer; float:right">'
    ModalContent += '</div>'

    $.modal({
        content: ModalContent,
        title: 'Room Occupancy',
        width: 600,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'huge anthracite-gradient displayNone',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
}

function AddAgeColumn(ChildNo, m) {
    var SelAge = "";
    var j = "";
    $("#Child_Div" + m).empty();
    for (var i = 0; i < ChildNo; i++) {
        j = i + 1;
        SelAge += '&nbsp<select id="child_age' + m + j + '" name="validation-select" class="select" tabindex="-1" style="cursor:pointer">'
        SelAge += '<option selected="selected" value="2">2</option>'
        SelAge += '<option value="3">3</option>'
        SelAge += '<option value="4">4</option>'
        SelAge += '<option value="5">5</option>'
        SelAge += '<option value="6">6</option>'
        SelAge += '<option value="7">7</option>'
        SelAge += '<option value="8">8</option>'
        SelAge += '<option value="9">9</option>'
        SelAge += '<option value="10">10</option>'
        SelAge += '<option value="11">11</option>'
        SelAge += '<option value="12">12</option>'
        SelAge += '</select>'
    }
    $("#Child_Div" + m).append(SelAge);
}

function SearchRatesOccupancy() {
    debugger;
    //document.getElementById('tbl_HotelList').style.display = "none";

    var Message = "";

    //if ($("#radio_gen").prop("checked")) {
    //    SearchValid = "General";
    //}
    //else if ($("#radio_adv").prop("checked")) {
    //    SearchValid = "Advance";
    //}

    if ($("#SearchType1").is(":checked")) {
        SearchValid = "General";
    }
    else if ($("#SearchType2").prop("checked")) {
        SearchValid = "Advance";
    }
    else
        SearchValid = "Advance";

    Destination = $('#hdnDCode').val();
    var Checkin = $('#txt_Checkin').val();
    var Checkout = $('#txt_Checkout').val();
    //var Ratings = $('#Select_StarRating').val();
    HotelName = $('#txt_HotelName').val();
    HotelCode.push($('#listHotels option').filter(function () { return this.value == HotelName; }).data('id'));
    //var Destination = $('#txt_Destination').val();
    nationalityCode = $('#sel_Nationality').val();
    validNationality = new Array();

    if (Destination == "") {
        Success("Enter the city name where you want to go!");
        return false;
    }
    if (HotelName == "") {
        Success("Enter the Hotel name!");
        return false;
    }
    if (Checkin == "") {
        Success('Please Enter Checkin Date');
        return false;
    }
    if (Checkout == "") {
        Success('Please Enter Checkout Date');
        return false;
    }

    if (nationalityCode == "" || nationalityCode == null) {
        Success("Please select Nationality");
        return false;
    }


    if (SearchValid != "Advance") {
        for (var i = 0; i < nationalityCode.length; i++) {
            if (nationalityCode[i] != "All" && nationalityCode[i] != "AllCountry") {
                var value = $.grep(arrCountry, function (p) { return p.Country == nationalityCode[i]; })
                                        .map(function (p) { return p; });
                validNationality.push(value[0])
            }
            else if (nationalityCode[i] == "AllCountry") {
                validNationality.push({ Country: "AllCountry", Countryname: "For All Nationality" })
            }


        }
    }
    else {
        nationalityCode = [];
        nationalityCode.push($('#sel_Nationality').val())
    }
    var nationalityName = $('#sel_Nationality option:selected').text();
    var DName = $('#txt_Destination').val();
    var Nights = $('#sel_Nights').val();
    var Adults = $('#Select_Adults1d').val();
    var Childs = $('#Select_Children1').val();
    if (SearchValid == "General") {
        var room = n - 1;
    } else {
        var room = $('#Select_Rooms').val();
    }

    var ddlRequest = '';

    var Supplier = "";
    if ($('#sel_Supplier').val() != null) {
        Supplier = $('#sel_Supplier').val();
    }
    var MealPlan = "";
    if ($('#sel_MealPlan').val() != null) {
        MealPlan = $('#sel_MealPlan').val();
    }
    var CurrencyCode = "";
    if ($('#sel_CurrencyCode').val() != null) {
        CurrencyCode = $('#sel_CurrencyCode').val();
    }


    if (HotelCode == null) {
        HotelCode = 0;
    }

    if (nationalityCode == null) {
        nationalityCode = "";
    }

    if (Nights == null) {
        Nights = 0;
    }
    if (Adults == null) {
        Adults = 0;
    }
    if (Childs == null) {
        Childs = 0;
    }

    else {
        var roomcount = parseInt(room);
        var occupancy = '';
        for (var i = 0; i < roomcount; i++) {
            if (i == 0) {
                occupancy = Rooms1();
            }
            else if (i == 1) {
                occupancy = occupancy + '$' + Rooms2();
            }
            else if (i == 2) {
                occupancy = occupancy + '$' + Rooms3();
            }
            else if (i == 3) {
                occupancy = occupancy + '$' + Rooms4();
            }
            else if (i == 4) {
                occupancy = occupancy + '$' + Rooms5();
            }
            else if (i == 5) {
                occupancy = occupancy + '$' + Rooms6();
            }
            else if (i == 6) {
                occupancy = occupancy + '$' + Rooms7();
            }
            else if (i == 7) {
                occupancy = occupancy + '$' + Rooms8();
            }
            else if (i == 8) {
                occupancy = occupancy + '$' + Rooms9();
            }
        }
        session = Destination + '_' + DName + '_' + Checkin + '_' + Checkout + '_' + room + '_' + occupancy + '_' + HotelCode + '_' + HotelName + '_' + nationalityCode + '_' + nationalityName;
        var trRequest = "";
        var data =
         {
             HotelCode: HotelCode,
             Destination: Destination,
             Checkin: Checkin,
             Checkout: Checkout,
             nationality: nationalityCode,
             Nights: Nights,
             Adults: Adults,
             Childs: Childs,
             Supplier: Supplier,
             MealPlan: MealPlan,
             CurrencyCode: CurrencyCode,
             AddSearchsession: session,
             SearchValid: SearchValid
         }
        $.ajax({
            type: "POST",
            url: "../handler/RoomHandler.asmx/GetRates",
            //data: '{"dFrom":"' + dFrom + '","dTo":"' + dTo + '"}',
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {

                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    //document.getElementById('tbl_HotelList').style.display = "";
                    $('#divFilters').css("display", "");
                    $("#modals").remove();
                    trRequest = ''
                    arrRoomRates = result.ListRate;
                    arrRoomDetails = result.AllRooms;
                    arrSuppliers = result.SupplierList;
                    arrCurrencyList = result.CurrencyList;
                    arrMealPlans = result.MealPlanList;
                    arrDatesList = result.ListDates;
                    arrRateType = result.arrRateType;
                    var Usertype = result.Usertype;
                    arrFilter = result.Filter;
                    if (arrRoomRates.length > 0 && Usertype != "Supplier") {

                        GetRates('RO', 0);

                        // $("#sel_ValidNationality option[value='All']").remove();

                        Natiion = [];

                        for (i = 0; i < arrRoomRates.length; i++) {
                            if (arrRoomRates[i].RoomOccupancy[0].Rooms.length != 0) {
                                for (var j = 0; j < arrCountry.length; j++) {

                                    if (arrCountry[j].Country == arrRoomRates[i].Nationality) {
                                        Natiion.push(arrCountry[j]);
                                    }
                                }
                            }

                            //if (arrRoomRates[i].Nationality == "AllCountry") {
                            //    Natiion.push({ Country: "AllCountry", Countryname: "For All Nationality" });
                            //}

                        }
                        $("#sel_ValidNationality").empty();
                        ddlRequest += '<option value="" selected="selected" disabled>Please Select</option>';
                        for (var i = 0; i < Natiion.length; i++) {

                            ddlRequest += '<option value="' + Natiion[i].Country + '">' + Natiion[i].Countryname + '</option>';
                        }
                        $("#sel_ValidNationality").append(ddlRequest);
                    }
                    else {
                        GenrateB2bRates();
                        GenerateFilter();
                    }



                }
                else if (result.retCode == -2) {
                    Message = result.ErrorMessage;
                    Success(Message);

                    return false;
                }
                else if (result.retCode == 0) {
                    $("#tbl_HotelList tbody").remove();
                    var trRequest = '<tbody>';
                    trRequest += '<tr><td align="center" style="padding-top: 2%" colspan="8"><span><b>No record found</b></span></td></tr>';
                    trRequest += '</tbody>';
                    $("#tbl_HotelList").append(trRequest);
                    AlertDanger(result.ex)
                }

            }
        });
    }
}

function Rooms1() {
    var adult = $('#SelGen_Adults1d').val();
    var child = $('#SelGen_Children1').val();
    var age = 0;
    if (parseInt(child) > 0) {
        age = $('#child_age11').val();
        if (parseInt(child) == 2) {
            age = age + '^' + $('#child_age12').val();
        }
        else if (parseInt(child) == 3) {
            age = age + '^' + $('#child_age12').val() + '^' + $('#child_age13').val();
        }
        else if (parseInt(child) == 4) {
            age = age + '^' + $('#child_age12').val() + '^' + $('#child_age13').val() + '^' + $('#child_age14').val();
        }
    }

    return adult + '|' + child + '^' + age;
}

function Rooms2() {
    var adult = $('#SelGen_Adults2d').val();
    var child = $('#SelGen_Children2').val();
    var age = 0;
    if (parseInt(child) > 0) {
        age = $('#child_age21').val();
        if (parseInt(child) == 2) {
            age = age + '^' + $('#child_age22').val();
        }
        else if (parseInt(child) == 3) {
            age = age + '^' + $('#child_age22').val() + '^' + $('#child_age23').val();
        }
        else if (parseInt(child) == 4) {
            age = age + '^' + $('#child_age22').val() + '^' + $('#child_age23').val() + '^' + $('#child_age24').val();
        }
    }
    return adult + '|' + child + '^' + age;
}

function Rooms3() {
    var adult = $('#SelGen_Adults3d').val();
    var child = $('#SelGen_Children3').val();
    var age = 0;
    if (parseInt(child) > 0) {
        age = $('#child_age31').val();
        if (parseInt(child) == 2) {
            age = age + '^' + $('#child_age32').val();
        }
        else if (parseInt(child) == 3) {
            age = age + '^' + $('#child_age32').val() + '^' + $('#child_age33').val();
        }
        else if (parseInt(child) == 4) {
            age = age + '^' + $('#child_age32').val() + '^' + $('#child_age33').val() + '^' + $('#child_age34').val();
        }
    }
    return adult + '|' + child + '^' + age;
}

function Rooms4() {
    var adult = $('#SelGen_Adults4d').val();
    var child = $('#SelGen_Children4').val();
    var age = 0;
    if (parseInt(child) > 0) {
        age = $('#child_age41').val();
        if (parseInt(child) == 2) {
            age = age + '^' + $('#child_age42').val();
        }
        else if (parseInt(child) == 3) {
            age = age + '^' + $('#child_age42').val() + '^' + $('#child_age43').val();
        }
        else if (parseInt(child) == 4) {
            age = age + '^' + $('#child_age42').val() + '^' + $('#child_age43').val() + '^' + $('#child_age44').val();
        }
    }
    return adult + '|' + child + '^' + age;
}
///////////////////////////////////////////////////////////////
function Rooms5() {
    var adult = $('#SelGen_Adults5d').val();
    var child = $('#SelGen_Children5').val();
    var age = 0;
    if (parseInt(child) > 0) {
        age = $('#child_age51').val();
        if (parseInt(child) == 2) {
            age = age + '^' + $('#child_age52').val();
        }
        else if (parseInt(child) == 3) {
            age = age + '^' + $('#child_age52').val() + '^' + $('#child_age53').val();
        }
        else if (parseInt(child) == 4) {
            age = age + '^' + $('#child_age52').val() + '^' + $('#child_age53').val() + '^' + $('#child_age54').val();
        }
    }
    return adult + '|' + child + '^' + age;
}
function Rooms6() {
    var adult = $('#SelGen_Adults6d').val();
    var child = $('#SelGen_Children6').val();
    var age = 0;
    if (parseInt(child) > 0) {
        age = $('#child_age61').val();
        if (parseInt(child) == 2) {
            age = age + '^' + $('#child_age62').val();
        }
        else if (parseInt(child) == 3) {
            age = age + '^' + $('#child_age62').val() + '^' + $('#child_age63').val();
        }
        else if (parseInt(child) == 4) {
            age = age + '^' + $('#child_age62').val() + '^' + $('#child_age63').val() + '^' + $('#child_age64').val();
        }
    }
    return adult + '|' + child + '^' + age;
}
function Rooms7() {
    var adult = $('#SelGen_Adults7d').val();
    var child = $('#SelGen_Children7').val();
    var age = 0;
    if (parseInt(child) > 0) {
        age = $('#child_age71').val();
        if (parseInt(child) == 2) {
            age = age + '^' + $('#child_age72').val();
        }
        else if (parseInt(child) == 3) {
            age = age + '^' + $('#child_age72').val() + '^' + $('#child_age73').val();
        }
        else if (parseInt(child) == 4) {
            age = age + '^' + $('#child_age72').val() + '^' + $('#child_age73').val() + '^' + $('#child_age74').val();
        }
    }
    return adult + '|' + child + '^' + age;
}
function Rooms8() {
    var adult = $('#SelGen_Adults8d').val();
    var child = $('#SelGen_Children8').val();
    var age = 0;
    if (parseInt(child) > 0) {
        age = $('#child_age81').val();
        if (parseInt(child) == 2) {
            age = age + '^' + $('#child_age82').val();
        }
        else if (parseInt(child) == 3) {
            age = age + '^' + $('#child_age82').val() + '^' + $('#child_age83').val();
        }
        else if (parseInt(child) == 4) {
            age = age + '^' + $('#child_age82').val() + '^' + $('#child_age83').val() + '^' + $('#child_age84').val();
        }
    }
    return adult + '|' + child + '^' + age;
}
function Rooms9() {
    var adult = $('#SelGen_Adults9d').val();
    var child = $('#SelGen_Children9').val();
    var age = 0;
    if (parseInt(child) > 0) {
        age = $('#child_age91').val();
        if (parseInt(child) == 2) {
            age = age + '^' + $('#child_age92').val();
        }
        else if (parseInt(child) == 3) {
            age = age + '^' + $('#child_age92').val() + '^' + $('#child_age93').val();
        }
        else if (parseInt(child) == 4) {
            age = age + '^' + $('#child_age92').val() + '^' + $('#child_age93').val() + '^' + $('#child_age94').val();
        }
    }
    return adult + '|' + child + '^' + age;
}