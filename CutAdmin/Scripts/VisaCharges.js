﻿var bValid;
$(document).ready(function () {
    debugger;
    $('#txtUrgent').attr("disabled", true);

});

function ServiceType() {
    sProcessing = $('#selProcessing').val();
    $('#txtUrgent').val('')
    $('#txtVisaFee').val('')
    $('#txtOtherFee').val('')
    $('#txtServiceTax').val('')
    $('#txtTotalAmount').val('')
    var Option;
    debugger;
    Option += '<option value="-" selected="selected">--Salect Visa Type--</option>'
    if (sProcessing == "Normal") {

        //Option += '<option value="6">96hrs VISA (Compulsory Confirm Ticket Copy)</option>';
        $('#selService').val('-');
        $('#txtUrgent').attr("disabled", true);
    }
    else if (sProcessing == "Urgent") {
        $('#selService').val('-');
        $('#txtUrgent').attr("disabled", false);
    }
}
function GetCharges() {
    $('#txtUrgent').val('')
    $('#txtVisaFee').val('')
    $('#txtOtherFee').val('')
    $('#txtServiceTax').val('')
    $('#txtTotalAmount').val('')
    $('#lbl_selService').css("display", "none");
    $('#btnAddCharge').css("display", "none");
    //ADD()
    var Type = $('#selService').val();
    var Process = $('#selVisaCountry').val();
    var arrCharges;
    if (Type != "-" && Process != "-") {
        $.ajax({
            type: "POST",
            url: "../Handler/VisaDetails.asmx/GetVisaChargesByNationality",
            data: '{"Type":"' + Type + '","Nationality":"' + Process + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    arrCharges = result.tbl_Visa;
                    if (arrCharges.length > 0) {
                        $('#selCurrencyType').val(arrCharges[0].Currency)
                        $("#DivCurrencyType .select span")[0].textContent = arrCharges[0].Currency;
                        $('#txtVisaFee').val(arrCharges[0].Fees)
                        $('#txtOtherFee').val(arrCharges[0].UrgentChg)
                        //$('#txtServiceTax').val(arrCharges[0].ServiceTax)
                        //$('#txtTotalAmount').val(arrCharges[0].TotalAmount)
                        //$('#lbl_selService').css("display", "none");
                    }
                }
                if (result.retCode == 0) {

                }
            },
            error: function () {
            }
        });
    }
    else {

    }
    //$('#selCity option:selected').val(tempcitycode);

}
function UpdateCharges() {
    $("#alSuccess1").css("display", "none")
    //ADD();
    var sid = $('#selService').val();
    var Urgent = $('#txtOtherFee').val();
    if (Urgent == "")
        Urgent = 0;
    var Nationality = $('#selVisaCountry').val();
    var Currency = $('#selCurrencyType').val();
    var VisaFee = $('#txtVisaFee').val();
    //var Service = $('#txtOtherFee').val();
    $('#lbl_selService').css("display", "none");
    bvalid = Validate_Charges();
    if (bvalid == true) {
        $.ajax({
            type: "POST",
            url: "../Handler/VisaDetails.asmx/UpdateVisa",
            data: '{"Type":"' + sid + '","Nationality":"' + Nationality + '","Currency":"' + Currency + '","Fee":"' + VisaFee + '","Urgent":"' + Urgent + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Visa Chargest Updated successfully!");
                    GetCharges();
                    window.location.reload();

                }
                if (result.retCode == 0) {

                }
            },
            error: function () {
                Success("An error occured while loading Visa Charges")
                //alert("An error occured while loading Visa Charges")
            }
        });
    }
}
function Validate_Charges() {
    var sid = $('#selService').val();
    var Urgent = $('#txtUrgent').val();
    var Visa = $('#txtVisaFee').val();
    var Other = $('#txtOtherFee').val();
    var Service = $('#txtServiceTax').val();
    var Process = $('#selProcessing').val();
    var Total = $('#txtTotalAmount').val();
    if (sid == "-") {
        $("#selService").focus();
        Success("Please select Service");
        return false;
    }
    if (Process == "Urgent") {
        if (Urgent == "") {
            $("#txtUrgent").focus();
            Success("Please enter Urgent ");
            return false;
        }

    }
    if (Visa == "") {
        $("#txtVisaFee").focus();
        Success("Please Visa Fee");
        return false;
    }
    if (Other == "") {
        $("#txtOtherFee").focus();
        Success("Please Other Fee");
        return false;
    }
    //if (Service == "") {
    //    $("#txtServiceTax").focus();
    //    $("#lbl_txtServiceTax").css("display", "");
    //    return false;
    //}
    //if (Total == "") {
    //    $("#txtTotalAmount").focus();
    //    $("#lbl_txtTotalAmount").css("display", "");
    //    return false;
    //}
    //$("#lbl_txtTotalAmount").text('');
    return true;
}


function off() {
    $("#alSuccess1").css("display", "none");
    $("#alSuccess2").css("display", "none");
}
//var pdf = new PDFObject({
//    url: "https://something.com/HTC_One_XL_User_Guide.pdf",
//    id: "pdfRendered",
//    pdfOpenParams: {
//        view: "FitH"
//    }
//}).embed("pdfRenderer");

function Notification() {
    var Type = $('#selService').val();
    var Process = $('#selVisaCountry').val();
    var Currency = $('#selCurrencyType').val();
    var arrCharges;
    if (Type != "-" && Process != "-" && Currency != "-") {

        $.ajax({
            type: "POST",
            url: "../Handler/VisaDetails.asmx/GetVisaChargesByCurrency",
            data: '{"Type":"' + Type + '","Nationality":"' + Process + '","Currency":"' + Currency + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    arrCharges = result.tbl_Visa;
                    if (arrCharges.length > 0) {
                        // $('#selCurrencyType').val(arrCharges[0].Currency)
                        $('#txtVisaFee').val(arrCharges[0].Fees)
                        $('#txtOtherFee').val(arrCharges[0].UrgentChg)
                        //$('#txtServiceTax').val(arrCharges[0].ServiceTax)
                        //$('#txtTotalAmount').val(arrCharges[0].TotalAmount)
                        $('#btnAddCharge').css("display", "none");
                        $('#btnAddStaff').css("display", "");

                    }
                }
                else if (result.retCode == 2) {
                    $('#txtVisaFee').val('')
                    $('#txtOtherFee').val('')
                    $('#btnAddCharge').css("display", "");
                    $('#btnAddStaff').css("display", "none");
                }
                if (result.retCode == 0) {

                }
            },
            error: function () {
            }
        });
    }
    else {
        alert("Please Select.");
    }
}

function AddCharges() {
    $("#alSuccess2").css("display", "none")
    //ADD();
    var sid = $('#selService').val();
    var Urgent = $('#txtOtherFee').val();
    if (Urgent == "")
        Urgent = 0;
    var Nationality = $('#selVisaCountry').val();
    var Currency = $('#selCurrencyType').val();
    var VisaFee = $('#txtVisaFee').val();
    //var Service = $('#txtOtherFee').val();
    $('#lbl_selService').css("display", "none");
    bvalid = Validate_Charges();
    if (bvalid == true) {
        $.ajax({
            type: "POST",
            url: "../Handler/VisaDetails.asmx/AddVisaCharge",
            data: '{"Type":"' + sid + '","Nationality":"' + Nationality + '","Currency":"' + Currency + '","Fee":"' + VisaFee + '","Urgent":"' + Urgent + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Visa Chargest Added successfully!");
                    GetCharges();
                    $('#btnAddCharge').css("display", "none");
                    $('#btnAddStaff').css("display", "");
                    //window.location.reload();
                }
                if (result.retCode == 0) {

                }
            },
            error: function () {
                Success("An error occured while loading Visa Charges")
                //alert("An error occured while loading Visa Charges")
            }
        });
    }
}