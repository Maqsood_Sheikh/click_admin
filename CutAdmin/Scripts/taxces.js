﻿$(function () {
    GetTaxSerices()
})


var NewService = '';
function GetTaxSerices() {
    $("#ul_Service").empty();
    $("#Tab_Service").empty();
    $.ajax({
        type: "POST",
        url: "../Handler/TaxHandler.asmx/GetTaxDetails",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                //  Services // 
                var arrServices = result.Sevices;
                var id = result.TaxDetails.tID
                var ul = '';
                var tab = '';
                for (var i = 0; i < arrServices.length; i++) {
                    if (i == 0) {
                        //ul += '<li class="active"><a href="#' + arrServices[i] + '" data-toggle="tab" onclick="GetSericeTax(\'' + arrServices[i] + '\')">' + arrServices[i] + '</a></li>'
                        ul += '<li class="active"><a href="#' + arrServices[i] + '" onclick="GetSericeTax(\'' + arrServices[i] + '\')">' + arrServices[i] + '</a></li>'
                        //tab += '<div class="tab-pane active" id="' + arrServices[i] + '">' + GenrateTab(id) + '</div>'
                        tab += '<div class="with-padding" id="' + arrServices[i] + '">' + GenrateTab(id) + '</div>'
                        //$("#txt_Service_" + id).val(arrServices[i])
                    }
                    else {
                        ul += '<li><a href="#' + arrServices[i] + '"  onclick="GetSericeTax(\'' + arrServices[i] + '\')">' + arrServices[i] + '</a></li>'
                        tab += '<div class="with-padding" id="' + arrServices[i] + '"></div>' //<span class="black">' + arrServices[i] + '</span><br/>
                    }
                }
                ul += '<li><a href="#0"  onclick="AddMore()">Add More</a></li>'
                tab += '<div class="with-padding"></div>'
                $("#ul_Service").append(ul);
                $("#Tab_Service").append(tab);
                
                var arrTax = result.TaxDetails;
                $("#txt_Service_" + arrTax.tID).val(arrServices[0])
                $("#txtOnMarkup_" + arrTax.tID).val(arrTax.GstTax)
                $("#txtVAT_" + arrTax.tID).val(arrTax.VatTax)
                $("#txtTDS_" + arrTax.tID).val(arrTax.TDS)
                $("#txtOnMain_" + arrTax.tID).val(arrTax.MarkupGstTax)
                if (arrTax.OnMarkup == false) {
                    $('#chk_onMarkup_' + arrTax.tID)//.bootstrapToggle('off')
                    $('#chk_onMain_' + arrTax.tID)//.bootstrapToggle('on')
                }
                else {
                    $('#chk_onMain_' + arrTax.tID).click();
                    $('#chk_onMarkup_' + arrTax.tID)//.bootstrapToggle('on')
                }
                $('#chk_onMarkup_0')//.bootstrapToggle('off')
                $('#chk_onMain_0')//.bootstrapToggle('off')
                setTimeout(function () {
                    GetSericeTax("Activity");
                },1000);
               
            }
            
        },
        error: function () {
        }
    });
}
function GetSericeTax(Service) {
    $.ajax({
        type: "POST",
        url: "../Handler/TaxHandler.asmx/GetTaxService",
        data: '{"Service":"' + Service + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var arrTax = result.TaxDetails;
                $("#" + Service).empty();
                var tab = GenrateTab(arrTax.tID);
                $("#" + Service).append(tab)
                $("#txt_Service_" + arrTax.tID).val(Service)
                $("#txtOnMarkup_" + arrTax.tID).val(arrTax.GstTax)
                $("#txtOnMain_" + arrTax.tID).val(arrTax.MarkupGstTax)
                $("#txtVAT_" + arrTax.tID).val(arrTax.VatTax)
                $("#txtTDS_" + arrTax.tID).val(arrTax.TDS)
                if (arrTax.OnMarkup == false) {
                    $('#chk_onMarkup_' + arrTax.tID)
                    $('#chk_onMain_' + arrTax.tID)//.bootstrapToggle('on')
                }
                else {
                    $('#chk_onMarkup_' + arrTax.tID).click();
                    $('#chk_onMain_' + arrTax.tID)//.bootstrapToggle('off')
                }

            }
        },
        error: function () {
        }
    });
}
function AddMore() {
    $("#0").empty();
    $("#0").append(GenrateTab(0))
    $('#chk_onMarkup_0')//.bootstrapToggle('off')
    $('#chk_onMain_0')//.bootstrapToggle('on')
}
function Save(ID) {
    var OnMarkupGST = "";
    var Active = false
    if ($("#chk_onMarkup_" + ID).is("checked"))
        Active = true;
    var data = {
        Service: $("#txt_Service_" + ID).val(),
        Gst: $("#txtOnMarkup_" + ID).val(),
        OnMarkupGST: $("#txtOnMain_" + ID).val(),
        Active: Active,
        tid: ID,
        Vat: $("#txtVAT_" + ID).val(),
        TDS: $("#txtTDS_" + ID).val()
    }

    $.ajax({
        type: "POST",
        url: "../Handler/TaxHandler.asmx/AddServiceDetails",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                alert("Tax Details Saved")
                GetTaxSerices()
            }
        },
        error: function () {
        }
    });
}

//var tabContent
function GenrateTab(id) {
    var tabContent =
        '<div class="sideTabsys">                                                                                                                                                                                                                                ' +
        '<div class="columns">                                                                                                                                                                                ' +
        '<div class="four-columns twelve-columns-mobile">                                                                                                                                                     ' +
        '<label>Service Name:</label><div class="input full-width">                                                                                                                                           ' +
        '<input value="" id="txt_Service_' + id + '" class="input-unstyled full-width" placeholder="Activity" type="text">                                                                                    ' +
        '</div>                                                                                                                                                                                               ' +
        '</div>                                                                                                                                                                                               ' +
        '<div class="four-columns twelve-columns-mobile">                                                                                                                                                     ' +
        '<label>GST on Markup %</label><div class="input full-width">                                                                                                                                         ' +
        '<input value="" id="txtOnMarkup_' + id + '" class="input-unstyled full-width" placeholder="20" type="text">                                                                                          ' +
        '</div>                                                                                                                                                                                               ' +
        '</div>                                                                                                                                                                                               ' +
        '<div class="four-columns twelve-columns-mobile">                                                                                                                                                     ' +
        '<label>GST on Main %</label><div class="input full-width">                                                                                                                                           ' +
        '<input value="" id="txtOnMain_' + id + '" class="input-unstyled full-width" placeholder="0" type="text">                                                                                             ' +
        '</div>                                                                                                                                                                                               ' +
        '</div>                                                                                                                                                                                               ' +
        '                                                                                                                                                                                                     ' +
        '</div>                                                                                                                                                                                               ' +
        '<p class="button-height">                                                                                                                                                                            ' +
        '<input type="checkbox" name="switch-medium-3" id="chk_onMarkup_' + id + '" class="switch medium wider mid-margin-right" value="1" data-text-on="on Amount" data-text-off="on Markup">' +
        '</p>                                                                                                                                                                                                 ' +
        '                                                                                                                                                                                                     ' +
        '<div class="columns">                                                                                                                                                                                ' +
        '<div class="four-columns twelve-columns-mobile">                                                                                                                                                     ' +
        '<label>VAT %</label><div class="input full-width">                                                                                                                                                   ' +
        '<input id="txtVAT_' + id + '" value="" class="input-unstyled full-width" placeholder="5" type="text">                                                                                                ' +
        '</div>                                                                                                                                                                                               ' +
        '</div>                                                                                                                                                                                               ' +
        '<div class="four-columns twelve-columns-mobile">                                                                                                                                                     ' +
        '<label>TDS %</label><div class="input full-width">                                                                                                                                                   ' +
        '<input value="" id="txtTDS_' + id + '" class="input-unstyled full-width" placeholder="5" type="text">                                                                                                ' +
        '</div>                                                                                                                                                                                               ' +
        '</div>                                                                                                                                                                                               ' +
        '                                                                                                                                                                                                     ' +
        '</div>                                                                                                                                                                                               ' +
        '</div>                                                                                                                                                                                               ' +
        '<p class="text-alignright">                                                                                                                                                                          ' +
        '<button type="button" onclick="Save(' + id + ')" class="button anthracite-gradient">Save</button></p>                                                                                                ' 
        
     return tabContent;
}