﻿using CutAdmin.BL;
using CutAdmin.Common;
using MGHLib.Common;
using MGHLib.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for SearchUtilityHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class SearchUtilityHandler : System.Web.Services.WebService
    {
        //DBHandlerDataContext DB = new DBHandlerDataContext();
        string Json = "";
        JavaScriptSerializer objSerlizer = new JavaScriptSerializer();


        [WebMethod(EnableSession = true)]
        public string GetAllHotelsOffer(Int64 HotelId)
        {
            using (var DB = new DBHandlerDataContext())
            {
                var OfferList = (from Offer in DB.tbl_HotelOfferUtilities where Offer.Hotel_Id == HotelId select Offer).ToList();
                if (OfferList.Count > 0)
                {
                    Json = objSerlizer.Serialize(new { Session = 1, retCode = 1, OfferList = OfferList });
                }
                else
                {
                    Json = objSerlizer.Serialize(new { Session = 1, retCode = 0 });
                }
                return Json;
            }
        }


        #region Offer
        [WebMethod(EnableSession = true)]
        public string GetAllOffers()
        {
            using (var DB = new DBHandlerDataContext())
            {
                var OfferList = (from Offer in DB.tbl_HotelOfferUtilities
                                 join Room in DB.tbl_RoomDetailUtilities on Offer.Room_Id equals Room.Room_Id
                                 join Hotel in DB.tbl_CommonHotelMasters on Offer.Hotel_Id equals Hotel.sid
                                 orderby Hotel.HotelName
                                 join OfferDays in DB.tbl_OfferDayUtilities on Offer.HotelOfferId equals OfferDays.Offer_Id
                                 select new
                                 {
                                     Offer.HotelOfferId,
                                     Offer.Room_Id,
                                     Hotel.sid,
                                     Hotel.HotelName,
                                     Room.RoomCategory
                                 }).ToList();
                //List<tbl_OfferDayUtility> af = af.Where(x => x.Hotel_Id == 0);
                var OfferDayList = (from OfferDays in DB.tbl_OfferDayUtilities select OfferDays).ToList();
                if (OfferList.Count > 0)
                {
                    //Json = objSerlizer.Serialize(new { Session = 1, retCode = 1, OfferList = OfferList});
                    Json = objSerlizer.Serialize(new { Session = 1, retCode = 1, OfferList = OfferList, OfferDayList = OfferDayList });
                }
                else
                {
                    Json = objSerlizer.Serialize(new { Session = 1, retCode = 0 });
                }
                return Json;
            }
        }

        [WebMethod(EnableSession = true)]
        public string AddOffer(Int64 HotelId, Int64 RoomCat, Int64 Supplier, string OfferNat, string FromInventory, string Name, string SeasonDate1, string SeasonDate2, string Names, string SpecialDate1, string SpecialDate2, string BlockDays)
        {
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    string[] NameSplit = Name.Split('^');
                    string[] Sdt1 = SeasonDate1.Split('^');
                    string[] Sdt2 = SeasonDate2.Split('^');
                    string[] NamesSplit = Names.Split('^');
                    string[] Spdt1 = SpecialDate1.Split('^');
                    string[] Spdt2 = SpecialDate2.Split('^');
                    List<tbl_OfferDayUtility> OfferDay = new List<tbl_OfferDayUtility>();
                    tbl_HotelOfferUtility Offer = new tbl_HotelOfferUtility();
                    Offer.Hotel_Id = HotelId;
                    //Offer.HotelCountry = Country;
                    //Offer.HotelCity = City;
                    Offer.Room_Id = RoomCat;
                    Offer.Supplier_Id = Supplier;
                    Offer.OfferNationality = OfferNat;
                    Offer.FromInventory = FromInventory;
                    Offer.BlockDays = BlockDays;
                    DB.tbl_HotelOfferUtilities.InsertOnSubmit(Offer);
                    DB.SubmitChanges();

                    for (int i = 0; i < NameSplit.Length - 1; i++)
                    {
                        tbl_OfferDayUtility Offerday = new tbl_OfferDayUtility();
                        Offerday.Hotel_Id = HotelId;
                        Offerday.Offer_Id = Offer.HotelOfferId;
                        Offerday.Room_Id = RoomCat;
                        Offerday.SeasonName = NameSplit[i];
                        Offerday.ValidFrom = Sdt1[i];
                        Offerday.ValidTo = Sdt2[i];
                        Offerday.DateType = "Season Date";
                        Offerday.Active = false;
                        OfferDay.Add(Offerday);
                    }
                    for (int i = 0; i < NamesSplit.Length - 1; i++)
                    {
                        if (NamesSplit[i] != "" || Spdt1[i] != "" || Spdt2[i] != "")
                        {
                            tbl_OfferDayUtility Offerdays = new tbl_OfferDayUtility();
                            Offerdays.Hotel_Id = HotelId;
                            Offerdays.Offer_Id = Offer.HotelOfferId;
                            Offerdays.Room_Id = RoomCat;
                            Offerdays.SeasonName = NamesSplit[i];
                            Offerdays.ValidFrom = Spdt1[i];
                            Offerdays.ValidTo = Spdt2[i];
                            Offerdays.DateType = "Block Date";
                            Offerdays.Active = true;
                            OfferDay.Add(Offerdays);
                        }
                    }
                    DB.tbl_OfferDayUtilities.InsertAllOnSubmit(OfferDay);
                    DB.SubmitChanges();
                    //Offer.HotelOfferId = 34;
                    Json = objSerlizer.Serialize(new { Session = 1, retCode = 1, Offer_Id = Offer.HotelOfferId });
                }
            }
            catch (Exception ex)
            {
                Json = objSerlizer.Serialize(new { Session = 1, retCode = 0 });
            }
            return Json;
        }



        [WebMethod(EnableSession = true)]
        public string AddOfferDetails(Int64 Sid, string OfferOn, string DaysPrior, string FixedDate, string OfferType, Int64 DiscountPer, Int64 DiscountAmount, Int64 NewRate, string FreeItemName, string FreeItemDetail, string HotelOfferCode, string HotelCode, string OfferTerm, string OfferNote, string MealPlan, string BookingType)
        {
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    tbl_OfferDayUtility Offer = DB.tbl_OfferDayUtilities.Single(x => x.Sid == Sid);
                    Offer.OfferOn = OfferOn;
                    Offer.DaysPrior = DaysPrior;
                    Offer.BookBefore = FixedDate;
                    Offer.OfferType = OfferType;
                    Offer.DiscountPer = DiscountPer;
                    Offer.DiscountAmount = DiscountAmount;
                    Offer.NewRate = NewRate;
                    Offer.FreebiItem = FreeItemName;
                    Offer.FreebiItemDetail = FreeItemDetail;
                    Offer.HotelOfferCode = HotelOfferCode;
                    Offer.OfferCode = HotelCode;
                    Offer.OfferTerms = OfferTerm;
                    Offer.OfferNote = OfferNote;
                    Offer.MealPlan = MealPlan;
                    Offer.Active = true;
                    Offer.BookingType = BookingType;
                    //DB.tbl_OfferDayUtilities.InsertOnSubmit(Offer);
                    DB.SubmitChanges();
                    Json = objSerlizer.Serialize(new { Session = 1, retCode = 1, Hotel_Id = Offer.Hotel_Id });
                }
            }
            catch (Exception ex)
            {
                Json = objSerlizer.Serialize(new { Session = 1, retCode = 0 });
            }
            return Json;
        }

        [WebMethod(EnableSession = true)]
        public string DeleteOffer(Int64 Sid)
        {
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    tbl_HotelOfferUtility Offer = DB.tbl_HotelOfferUtilities.Single(x => x.HotelOfferId == Sid);
                    DB.tbl_HotelOfferUtilities.DeleteOnSubmit(Offer);

                    List<tbl_OfferDayUtility> OfferDay = new List<tbl_OfferDayUtility>();
                    OfferDay = (from Offers in DB.tbl_OfferDayUtilities where Offers.Offer_Id == Sid select Offers).ToList();
                    DB.tbl_OfferDayUtilities.DeleteAllOnSubmit(OfferDay);
                    DB.SubmitChanges();

                    Json = objSerlizer.Serialize(new { Session = 1, retCode = 1 });
                }
            }
            catch (Exception ex)
            {
                Json = objSerlizer.Serialize(new { Session = 1, retCode = 0 });
            }
            return Json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateOffer(Int64 Sid, Int64 HotelId, string Country, string City, Int64 RoomCat, Int64 Supplier, string OfferNat, string FromInventory, string BlockDays)
        {
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    tbl_HotelOfferUtility Offer = DB.tbl_HotelOfferUtilities.Single(x => x.HotelOfferId == Sid);
                    Offer.Hotel_Id = HotelId;
                    Offer.HotelCountry = Country;
                    Offer.HotelCity = City;
                    Offer.Room_Id = RoomCat;
                    Offer.Supplier_Id = Supplier;
                    Offer.OfferNationality = OfferNat;
                    Offer.FromInventory = FromInventory;
                    Offer.BlockDays = BlockDays;
                    DB.SubmitChanges();

                    Json = objSerlizer.Serialize(new { Session = 1, retCode = 1 });
                }
            }
            catch (Exception ex)
            {
                Json = objSerlizer.Serialize(new { Session = 1, retCode = 0 });
            }
            return Json;
        }

        [WebMethod(EnableSession = true)]
        public string ActivateOffer(Int64 Sid, string Status)
        {
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    tbl_OfferDayUtility OfferDay = DB.tbl_OfferDayUtilities.Single(x => x.Sid == Sid);
                    OfferDay.Active = Convert.ToBoolean(Status);
                    DB.SubmitChanges();
                    Json = objSerlizer.Serialize(new { Session = 1, retCode = 1 });
                }
            }
            catch (Exception ex)
            {
                Json = objSerlizer.Serialize(new { Session = 1, retCode = 0 });
            }
            return Json;
        }

        [WebMethod(EnableSession = true)]
        public string GetOffer(Int64 Sid)
        {
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    tbl_HotelOfferUtility OfferDetail = DB.tbl_HotelOfferUtilities.Single(x => x.HotelOfferId == Sid);

                    var OfferDayDetail = (from Offers in DB.tbl_OfferDayUtilities where Offers.Offer_Id == Sid select Offers).ToList();

                    Json = objSerlizer.Serialize(new { Session = 1, retCode = 1, OfferDetail = OfferDetail, OfferDayDetail = OfferDayDetail });
                }
            }
            catch (Exception ex)
            {
                Json = objSerlizer.Serialize(new { Session = 1, retCode = 0 });
            }
            return Json;
        }
        #endregion

        #region Season Block
        [WebMethod(EnableSession = true)]
        public string GetAllSeasons(Int64 HotelId)
        {
            using (var DB = new DBHandlerDataContext())
            {
                var SeasonList = ((from OfferDay in DB.tbl_OfferDayUtilities
                                   join Room in DB.tbl_RoomDetailUtilities on OfferDay.Room_Id equals Room.Room_Id
                                   join Hotel in DB.tbl_CommonHotelMasters on OfferDay.Hotel_Id equals Hotel.sid
                                   where OfferDay.Hotel_Id == HotelId
                                   //&& OfferDay.DateType == "Season Date"
                                   select new
                                   {
                                       OfferDay.Sid,
                                       OfferDay.Offer_Id,
                                       OfferDay.SeasonName,
                                       OfferDay.ValidFrom,
                                       OfferDay.ValidTo,
                                       OfferDay.Active,
                                       OfferDay.BookBefore,
                                       OfferDay.BookingType,
                                       OfferDay.DateType,
                                       OfferDay.DaysPrior,
                                       OfferDay.DiscountPer,
                                       OfferDay.DiscountAmount,
                                       OfferDay.MealPlan,
                                       OfferDay.NewRate,
                                       OfferDay.OfferOn,
                                       OfferDay.OfferType,
                                       Room.RoomCategory,
                                       Hotel.CityId,
                                       Hotel.CountryId,
                                       Hotel.HotelName
                                   })).ToList();
                if (SeasonList.Count > 0)
                {
                    Json = objSerlizer.Serialize(new { Session = 1, retCode = 1, SeasonList = SeasonList });
                }
                else
                {
                    Json = objSerlizer.Serialize(new { Session = 1, retCode = 0 });
                }
                return Json;
            }
        }

        [WebMethod(EnableSession = true)]
        public string AddSeason(Int64 Sid, Int64 HotelId, string Country, string City, Int64 RoomCat, Int64 Supplier, string OfferNat, string Name, string SeasonDate1, string SeasonDate2)
        {
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    tbl_OfferDayUtility Offerday = new tbl_OfferDayUtility();
                    Offerday.Hotel_Id = HotelId;
                    Offerday.Offer_Id = Sid;
                    Offerday.Room_Id = RoomCat;
                    Offerday.SeasonName = Name;
                    Offerday.ValidFrom = SeasonDate1;
                    Offerday.ValidTo = SeasonDate2;
                    Offerday.DateType = "Season Date";
                    DB.tbl_OfferDayUtilities.InsertOnSubmit(Offerday);
                    DB.SubmitChanges();
                    Json = objSerlizer.Serialize(new { Session = 1, retCode = 1 });
                }
            }
            catch (Exception ex)
            {
                Json = objSerlizer.Serialize(new { Session = 1, retCode = 0 });
            }
            return Json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateSeason(Int64 Sid, Int64 OfferId, Int64 HotelId, string Country, string City, Int64 RoomCat, Int64 Supplier, string OfferNat, string Name, string SeasonDate1, string SeasonDate2)
        {
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    tbl_OfferDayUtility Offerday = DB.tbl_OfferDayUtilities.Single(x => x.Sid == Sid);
                    Offerday.Hotel_Id = HotelId;
                    //Offerday.Offer_Id = OfferId;
                    Offerday.Room_Id = RoomCat;
                    Offerday.SeasonName = Name;
                    Offerday.ValidFrom = SeasonDate1;
                    Offerday.ValidTo = SeasonDate2;
                    DB.SubmitChanges();
                    Json = objSerlizer.Serialize(new { Session = 1, retCode = 1 });
                }
            }
            catch (Exception ex)
            {
                Json = objSerlizer.Serialize(new { Session = 1, retCode = 0 });
            }
            return Json;
        }

        [WebMethod(EnableSession = true)]
        public string DeleteSeason(Int64 Sid, Int64 Offer_Id)
        {
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    tbl_OfferDayUtility OfferDay = DB.tbl_OfferDayUtilities.Single(x => x.Sid == Sid);
                    DB.tbl_OfferDayUtilities.DeleteOnSubmit(OfferDay);
                    DB.SubmitChanges();
                    Json = objSerlizer.Serialize(new { Session = 1, retCode = 1 });
                }
            }
            catch (Exception ex)
            {
                Json = objSerlizer.Serialize(new { Session = 1, retCode = 0 });
            }
            return Json;
        }

        [WebMethod(EnableSession = true)]
        public string AddSpecial(Int64 Sid, Int64 HotelId, string Country, string City, Int64 RoomCat, Int64 Supplier, string OfferNat, string Name, string SeasonDate1, string SeasonDate2)
        {
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    tbl_OfferDayUtility Offerday = new tbl_OfferDayUtility();
                    Offerday.Hotel_Id = HotelId;
                    Offerday.Offer_Id = Sid;
                    Offerday.Room_Id = RoomCat;
                    Offerday.SeasonName = Name;
                    Offerday.ValidFrom = SeasonDate1;
                    Offerday.ValidTo = SeasonDate2;
                    Offerday.DateType = "Block Date";
                    DB.tbl_OfferDayUtilities.InsertOnSubmit(Offerday);
                    DB.SubmitChanges();
                    Json = objSerlizer.Serialize(new { Session = 1, retCode = 1 });
                }
            }
            catch (Exception ex)
            {
                Json = objSerlizer.Serialize(new { Session = 1, retCode = 0 });
            }
            return Json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateSpecial(Int64 Sid, Int64 OfferId, Int64 HotelId, string Country, string City, Int64 RoomCat, Int64 Supplier, string OfferNat, string Name, string SeasonDate1, string SeasonDate2)
        {
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    tbl_OfferDayUtility Offerday = DB.tbl_OfferDayUtilities.Single(x => x.Sid == Sid);
                    Offerday.Hotel_Id = HotelId;
                    //Offerday.Offer_Id = OfferId;
                    Offerday.Room_Id = RoomCat;
                    Offerday.SeasonName = Name;
                    Offerday.ValidFrom = SeasonDate1;
                    Offerday.ValidTo = SeasonDate2;
                    DB.SubmitChanges();
                    Json = objSerlizer.Serialize(new { Session = 1, retCode = 1 });
                }
            }
            catch (Exception ex)
            {
                Json = objSerlizer.Serialize(new { Session = 1, retCode = 0 });
            }
            return Json;
        }

        [WebMethod(EnableSession = true)]
        public string DeleteSpecial(Int64 Sid, Int64 Offer_Id)
        {
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    var Offers = (from Offer in DB.tbl_OfferDayUtilities where Offer.Sid == Sid & Offer.Offer_Id == Offer_Id select Offer).ToList();
                    if (Offers.Count > 0)
                    {
                        tbl_OfferDayUtility OfferDay = DB.tbl_OfferDayUtilities.Single(x => x.Sid == Sid);
                        DB.tbl_OfferDayUtilities.DeleteOnSubmit(OfferDay);
                        DB.SubmitChanges();
                        Json = objSerlizer.Serialize(new { Session = 1, retCode = 1 });
                    }
                    else
                    {
                        Json = objSerlizer.Serialize(new { Session = 1, retCode = -1 });
                    }
                }
            }
            catch (Exception ex)
            {
                Json = objSerlizer.Serialize(new { Session = 1, retCode = 0 });
            }
            return Json;
        }
        #endregion
    }
}
