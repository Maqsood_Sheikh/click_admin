﻿using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for RoomImageHandler
    /// </summary>
    public class RoomImageHandler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            DataManager.DBReturnCode retCode = DataManager.DBReturnCode.EXCEPTION;
            //DataManager.DBReturnCode retCode = DataManager.DBReturnCode.SUCCESS;
            string HotelId = System.Convert.ToString(context.Request.QueryString["HotelId"]);
            string RoomId = System.Convert.ToString(context.Request.QueryString["RoomId"]);
            string ImgPath = System.Convert.ToString(context.Request.QueryString["ImagePath"]);
            string[] tokens = ImgPath.Split('^');
            int TotalImgs = tokens.Count() - 1;
            string[] newFiles;
            string NewImages = "";
            string SavedImages = "";


            for (int img = 0; img < tokens.Count(); img++)
            {
                if (tokens[img] != "")
                {
                    string token1 = tokens[img].Split(',')[0];
                    NewImages += token1 + "^";
                }
            }
            string[] New = NewImages.Split('^');

            if (context.Request.Files.Count > 0)
            {

                HttpFileCollection files = context.Request.Files;
                if (TotalImgs == files.Count)
                {
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFile file = files[i];
                        string token1 = tokens[i].Split(',')[0];
                        string myFilePath = token1.Split('.')[0];
                        string ext1 = tokens[i].Split('.')[1];
                        //string ext = ext1.Split(',')[0];
                        //string ext = "jpg";
                        //string pre = "t";
                        //string img = myFilePath[i].Split('.');
                        if (ext1 != ".pdf")
                        {
                            string FileName = myFilePath;
                            //FileName = Path.Combine(context.Server.MapPath("HotelImages/"), myFilePath + '.' + ext);
                            FileName = Path.Combine(context.Server.MapPath("RoomImages/"), myFilePath + '.' + ext1);
                            file.SaveAs(FileName);
                            //retCode = ActivityManager.UpdateImages(sFileName, sid);
                            context.Response.Write("1");
                        }
                        else
                        {
                            context.Response.Write("0");

                        }
                    }
                }
                else
                {
                    string Name;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFile file = files[i];
                        string myFilePath = New[i];
                        //string ext = Path.GetExtension(myFilePath);
                        string ext = "jpg";
                        string pre = "t";
                        if (ext != ".pdf")
                        {
                            string FileName = myFilePath;
                            FileName = Path.Combine(context.Server.MapPath("RoomImages/"), myFilePath + '.' + ext);
                            file.SaveAs(FileName);
                            //retCode = ActivityManager.UpdateImages(sFileName, sid);
                            context.Response.Write("1");
                        }
                        else
                        {
                            context.Response.Write("0");

                        }
                    }

                }


                // }
                //        if (ImageNames != null)
                //        {
                //            ImgPath = "";
                //            ImgPath = ImageNames;
                //            retCode = HotelMappingManager.HotelImages(ImgPath, sid);
                //        }

                //    }


                //if (context.Request.Files.Count > 0)
                //{
                //    List<string> Images = ImgPath.Split('^').ToList();
                //    HttpFileCollection files = context.Request.Files;
                //    for (int i = 0; i < Images.Count -1; i++)
                //    {
                //        string sFolderName = context.Server.MapPath("~/RoomImages/");
                //        if (!Directory.Exists(sFolderName))
                //        {
                //            Directory.CreateDirectory(sFolderName);
                //        }
                //        else
                //        {
                //            string[] Files = Directory.GetFiles(sFolderName);
                //            foreach (string fileName in Files)
                //            {
                //                if (fileName.Contains(Images[i]))
                //                {
                //                    continue;
                //                }
                //            }
                //        }
                //        HttpPostedFile file = files[i];
                //        //string myFilePath = file.FileName;
                //        string ext = Path.GetExtension(Images[i]);
                //        if (ext != ".pdf")
                //        {
                //            string FileName = Images[i];
                //            FileName = Path.Combine(context.Server.MapPath("RoomImages/"), Images[i]);
                //            file.SaveAs(FileName);
                //            context.Response.Write("1");
                //        }
                //        else
                //        {
                //            context.Response.Write("0");

                //        }
                //    }



            }
            else
            {
                //DBHelper.DBReturnCode retCode = ImageManager.InsertSliderImage(sid, sFileName, facility, service, start, details, Notes, "");
                if (retCode == DataManager.DBReturnCode.SUCCESS)
                {
                    context.Response.Write("1");
                }
                else
                {
                    context.Response.Write("0");
                }
            }
            if (ImgPath != "")
            {
                retCode = RoomManager.RoomImages(ImgPath, RoomId, HotelId);
            }
            ////DataManager.DBReturnCode retCode = DataManager.DBReturnCode.EXCEPTION;
            //DataManager.DBReturnCode retCode = DataManager.DBReturnCode.SUCCESS;
            //string HotelId = System.Convert.ToString(context.Request.QueryString["HotelId"]);
            //string RoomId = System.Convert.ToString(context.Request.QueryString["RoomId"]);
            //string ImgPath = System.Convert.ToString(context.Request.QueryString["ImagePath"]);
            //if (context.Request.Files.Count > 0)
            //{

            //    HttpFileCollection files = context.Request.Files;
            //    for (int i = 0; i < files.Count; i++)
            //    {
            //        HttpPostedFile file = files[i];
            //        string myFilePath = file.FileName;
            //        string ext = Path.GetExtension(myFilePath);
            //        if (ext != ".pdf")
            //        {
            //            string FileName = myFilePath;
            //            FileName = Path.Combine(context.Server.MapPath("RoomImages/"), myFilePath);
            //            file.SaveAs(FileName);
            //            context.Response.Write("1");
            //        }
            //        else
            //        {
            //            context.Response.Write("0");

            //        }
            //    }
               


            //}
            //else
            //{
            //    //DBHelper.DBReturnCode retCode = ImageManager.InsertSliderImage(sid, sFileName, facility, service, start, details, Notes, "");
            //    if (retCode == DataManager.DBReturnCode.SUCCESS)
            //    {
            //        context.Response.Write("1");
            //    }
            //    else
            //    {
            //        context.Response.Write("0");
            //    }
            //}
            //if (ImgPath != "")
            //{
            //    retCode = RoomManager.RoomImages(ImgPath, RoomId, HotelId);
            //}
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}