﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="MapArea.aspx.cs" Inherits="CutAdmin.MapArea" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <%-- <script src="Scripts/AddOfferRate.js"></script>--%>
    
    <!-- Additional styles -->
    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">

    <!-- DataTables -->

    

    <!-- For Retina displays -->
    <link rel="stylesheet" media="only all and (-webkit-min-device-pixel-ratio: 1.5), only screen and (-o-min-device-pixel-ratio: 3/2), only screen and (min-device-pixel-ratio: 1.5)" href="css/2x.css?v=1">

   <%-- <!-- Webfonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>--%>

    <!-- glDatePicker -->
    <link rel="stylesheet" href="js/libs/glDatePicker/developr.fixed.css?v=1">

    <!-- jQuery Form Validation -->
    <%--	<link rel="stylesheet" href="js/libs/formValidator/developr.validationEngine.css?v=1">--%>

    <!-- Additional styles -->

    <link rel="stylesheet" href="css/styles/modal.css?v=1">


 <%--   <!-- JavaScript at bottom except for Modernizr -->
    <script src="js/libs/modernizr.custom.js"></script>--%>


   
    <link rel="stylesheet" href="css/styles/table.css?v=1">
    <link href="css/custom.css" rel="stylesheet" />
    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
    <link href="css/ScrollingTable.css" rel="stylesheet" />
    <script src="Scripts/moments.js"></script>
     <script src="Scripts/jquery-ui.js"></script>
    <script src="Scripts/MapArea.js?v=1.6"></script>
    <%--<script>
        $(document).ready(function () {
            var aaa = AreaGroup;
            $('#tbl_AreaGroup').DataTable();
        });
        $(selector).dataTable();
        var table = $(selector),
	    tableStyled = false;

        table.dataTable({
            'sDom': '<"dataTables_header"lfr>t<"dataTables_footer"ip>',
            'fnDrawCallback': function (oSettings) {
                // Only run once
                if (!tableStyled) {
                    table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
                    tableStyled = true;
                }
            }
        });
    </script>--%>
    <img alt="" src="../loader1.gif" id="loders" width="128" height="128" style="position: fixed; z-index: 9999; left: 55%; margin-top: 8%; display: none">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">

        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

        <div class="with-padding">

            <div class="columns" id="DivAddOfferRate">

                <hgroup id="main-title" class="thin">
                    <h3>Map Area</h3>
                    <hr />


                </hgroup>

                <div class="new-row one-columns"></div>
                <div class="three-columns" id="div_MapAr">
                    <span class="text-left">Select Country:</span>
                    <br>
                    <select id="selCountryMapAr" onchange="GetLocation(this.value)" class="full-width  select OfferType">
                    </select>
                </div>
                <div class="three-columns">
                    <span class="text-left">City:</span>
                    <br>
                    <input id="txtCity" name="Text[]" list="Select_City" class="input full-width" type="text" onchange="GetCode(this.value)">
                    <datalist id="Select_City"></datalist>
                    <label style="color: red; margin-top: 3px; display: none" id="lbl_txtCity">
                        <b>* This field is required</b>
                    </label>

                </div>
                <div class="three-columns">
                    <div style="margin-top: 5px">
                        <span class="text-left">Code:</span>
                    </div>

                    <%--<input id="txtCountryCodeAr" class="input full-width" type="text" readonly>--%>
                    <div style="margin-top: 5px">
                        <b>
                            <label id="lbl_txtCountryCodeAr">
                            </label>
                        </b>
                    </div>

                </div>

                <div class="two-columns" style="float:right">
                    
                      <a href="MapAreaList.aspx">
                    <button type="button" class="button blue-gradient glossy" id="btn_list" onclick="">Map Area List</button>
                </a>
                    
                    </div>

                <section style="margin-left: 27px" id="row_section">
                    <%--<div id="row1" style="width: 100%;padding-left:20px">
                    <div class="columns">
                        

                        <div class="three-columns" id="div_GTAAr" style="margin-left: 24.125px; clear:left;">
                            <span class="text-left">GTA:</span>
                            <input type="radio" id="GTAradioAr" name="radio" onclick="GetCode(1)"/>
                            <br>
                            <select id="selGTAAr" class="full-width  select OfferType">
                            </select>
                        </div>

                        <div class="three-columns" id="div_HB1">
                            <span class="text-left">HB:</span>
                            <input type="radio" id="HBradioAr" name="radio" onclick="GetCode(2)" checked/>
                            <br>
                            <select id="selHBAr" onchange="BindCode(this.value)" class="full-width  select OfferType">
                            </select>
                        </div>

                        <div class="three-columns" id="div_TBOAr">
                            <span class="text-left">TBO:</span>
                           <input type="radio" id="TBOradioAr" name="radio" onclick="GetCode(3)"/>
                            <br>
                            <select id="selTBOAr" class="full-width  select OfferType">
                            </select>
                        </div>

                        

                      <div class="three-columns" id="div_DOTAr" style="clear:left">
                            <span class="text-left">DOTW:</span>
                            <input type="radio" id="DOTWradioAr" name="radio" onclick="GetCode(4)"/>
                            <br>
                            <select id="selDOTAr" class="full-width  select OfferType">
                            </select>
                        </div>

                        <div class="three-columns" id="div_MGHAr">
                            <span class="text-left">MGH:</span>
                             <input type="radio" id="MGHWradioAr" name="radio" onclick="GetCode(5)"/>
                            <br>
                            <select id="selMGHAr" class="full-width  select OfferType">
                            </select>
                        </div>

                        <div class="three-columns" id="div_GRNAr">
                            <span class="text-left">GRN:</span>
                            <input type="radio" id="GRNradioAr" name="radio" onclick="GetCode(6)"/>
                            <br>
                            <select id="selGRNAr" class="full-width  select OfferType">
                            </select>
                        </div>

                        <div id="RemoveRowDiv1" class="one-columns" style="padding-top: 16px; display:none;" title="Remove Row">
                            <i onclick="RemoveRow(1)" aria-hidden="true"><label class="button blue-gradient"><span class="icon-minus"></span></label></i>
                        </div>

                        <div id="AddRowDiv1" class="one-columns" style="padding-top:16px" title="Add Row">
                            <i onclick="AddRow()" aria-hidden="true"><label class="button blue-gradient" ><span class="icon-plus"></span></label></i>
                        </div>

                        <div class="columns">
                            <div class="one-columns" style="margin-top: 16px">
                                <button class="button glossy blue-gradient" id="btn_save" onclick="SaveCityLocation()">Save</button>
                                <button class="button glossy blue-gradient" id="btn_Update" onclick="UpdateCityLocation()" style="display:none">Update</button>
                            </div>
                        </div>



                    </div>
                </div>--%>
                </section>


                <%--<div id="row2" style="width: 100%;padding-left:20px">
                    <div class="columns">--%>
                <%--<div class="three-columns" id="div_DOTAr">
                            <span class="text-left">DOTW:</span>
                            <input type="radio" id="DOTWradioAr" name="radio" onclick="GetCode(4)"/>
                            <br>
                            <select id="selDOTAr" class="full-width  select OfferType">
                            </select>
                        </div>

                        <div class="three-columns" id="div_MGHAr">
                            <span class="text-left">MGH:</span>
                             <input type="radio" id="MGHWradioAr" name="radio" onclick="GetCode(5)"/>
                            <br>
                            <select id="selMGHAr" class="full-width  select OfferType">
                            </select>
                        </div>

                        <div class="three-columns" id="div_GRNAr">
                            <span class="text-left">GRN:</span>
                            <input type="radio" id="GRNradioAr" name="radio" onclick="GetCode(6)"/>
                            <br>
                            <select id="selGRNAr" class="full-width  select OfferType">
                            </select>
                        </div>

                        <div id="RemoveRowDiv1" class="one-columns" style="padding-top: 16px; display:none;" title="Remove Row">
                            <i onclick="RemoveRow(1)" aria-hidden="true"><label class="button blue-gradient"><span class="icon-minus"></span></label></i>
                        </div>

                        <div id="AddRowDiv1" class="one-columns" style="padding-top:16px" title="Add Row">
                            <i onclick="AddRow()" aria-hidden="true"><label class="button blue-gradient" ><span class="icon-plus"></span></label></i>
                        </div>

                        <div class="columns">
                            <div class="one-columns" style="margin-top: 16px">
                                <button class="button glossy blue-gradient" id="btn_save" onclick="SaveCityLocation()">Save</button>
                                <button class="button glossy blue-gradient" id="btn_Update" onclick="UpdateCityLocation()" style="display:none">Update</button>
                            </div>
                        </div>--%>


                <%--</div>
                </div>--%>
            </div>



        </div>
        <div>
            <div class="columns">
                <div class="three-columns"></div>
                <div class="three-columns"></div>
                <div class="three-columns"></div>
                <div class="three-columns">
                    <button class="button glossy blue-gradient" id="btn_save" onclick="SaveArea()">Save</button>
                    <button class="button glossy blue-gradient" id="btn_Update" onclick="UpdateAreaMapping()" style="display:none">Update</button>

                </div>
            </div>

        </div>
        <%--  </div>--%>
        <br />
        <br />
        <br />
        <%--<div class="panel panel-primary">
            <div class="with-padding">
                <div class="panel-body">

                    <div class="table-responsive">
                        <table class="table" id="tbl_AreaGroup">
                            <thead>
                                <tr>
                                    <th>Sr.No.</th>
                                    <th>Destination Code</th>
                                    <th>Name</th>
                                    <th>Country Code</th>
                                    <th>Group_ID</th>
                                    <th>GTA</th>
                                    <th>HB</th>
                                    <th>DOTW</th>
                                    <th>MGH</th>
                                    <th>TBO</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>--%>
        <%--<section style="padding-left: 15px; padding-right: 15px;">
            <table class="simple-table responsive-table" id="tbl_AreaGroupMapp">
                <thead>
                    <tr>
                        <th>Sr.No.</th>
                        <th>Destination Code</th>
                        <th>Name</th>
                        <th>Country Code</th>
                        <th>Group_ID</th>
                        <th>GTA</th>
                        <th>HB</th>
                        <th>DOTW</th>
                        <th>MGH</th>
                        <th>TBO</th>
                        <th>GRN</th>
                        <th>Edit</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </section>--%>
    </section>
    <!-- End main content -->


    <link href="css/jquery-ui.css" rel="stylesheet" />
    <script src="Scripts/jquery-ui.js"></script>
    <!-- Scripts -->
    <script src="js/libs/jquery-1.10.2.min.js"></script>
    <script src="js/setup.js"></script>

    <!-- Template functions -->
    <script src="js/developr.input.js"></script>
    <script src="js/developr.navigable.js"></script>
    <script src="js/developr.notify.js"></script>
    <script src="js/developr.scroll.js"></script>
    <script src="js/developr.tooltip.js"></script>
    <script src="js/developr.table.js"></script>

    <!-- glDatePicker -->
    <script src="js/libs/glDatePicker/glDatePicker.min.js?v=1"></script>

    <!-- jQuery Form Validation -->
    <script src="js/libs/formValidator/jquery.validationEngine.js?v=1"></script>
    <script src="js/libs/formValidator/languages/jquery.validationEngine-en.js?v=1"></script>

    <!-- Plugins -->
    <script src="js/libs/jquery.tablesorter.min.js"></script>
    <script src="js/libs/DataTables/jquery.dataTables.min.js"></script>
    <script src="js/developr.modal.js"></script>


    <script type="text/javascript">

        $(function () {
            $("#Fixeddatepicker3").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy",
                //onSelect: insertDepartureDate,
                //dateFormat: "yy-m-d",
                minDate: "dateToday",
                //maxDate: "+3M +10D"
            });
        });
    </script>


    <%--<script>

        // Call template init (optional, but faster if called manually)
        $.template.init();

        // Table sort - DataTables
        var table = $('#sorting-advanced');
        table.dataTable({
            'aoColumnDefs': [
				{ 'bSortable': false, 'aTargets': [0, 5] }
            ],
            'sPaginationType': 'full_numbers',
            'sDom': '<"dataTables_header"lfr>t<"dataTables_footer"ip>',
            'fnInitComplete': function (oSettings) {
                // Style length select
                table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
                tableStyled = true;
            }
        });

        // Table sort - styled
        $('#sorting-example1').tablesorter({
            headers: {
                0: { sorter: false },
                5: { sorter: false }
            }
        }).on('click', 'tbody td', function (event) {
            // Do not process if something else has been clicked
            if (event.target !== this) {
                return;
            }

            var tr = $(this).parent(),
				row = tr.next('.row-drop'),
				rows;

            // If click on a special row
            if (tr.hasClass('row-drop')) {
                return;
            }

            // If there is already a special row
            if (row.length > 0) {
                // Un-style row
                tr.children().removeClass('anthracite-gradient glossy');

                // Remove row
                row.remove();

                return;
            }

            // Remove existing special rows
            rows = tr.siblings('.row-drop');
            if (rows.length > 0) {
                // Un-style previous rows
                rows.prev().children().removeClass('anthracite-gradient glossy');

                // Remove rows
                rows.remove();
            }

            // Style row
            tr.children().addClass('anthracite-gradient glossy');

            // Add fake row
            $('<tr class="row-drop">' +
				'<td colspan="' + tr.children().length + '">' +
					'<div class="float-right">' +
						'<button type="submit" class="button glossy mid-margin-right">' +
							'<span class="button-icon"><span class="icon-mail"></span></span>' +
							'Send mail' +
						'</button>' +
						'<button type="submit" class="button glossy">' +
							'<span class="button-icon red-gradient"><span class="icon-cross"></span></span>' +
							'Remove' +
						'</button>' +
					'</div>' +
					'<strong>Name:</strong> John Doe<br>' +
					'<strong>Account:</strong> admin<br>' +
					'<strong>Last connect:</strong> 05-07-2011<br>' +
					'<strong>Email:</strong> john@doe.com' +
				'</td>' +
			'</tr>').insertAfter(tr);

        }).on('sortStart', function () {
            var rows = $(this).find('.row-drop');
            if (rows.length > 0) {
                // Un-style previous rows
                rows.prev().children().removeClass('anthracite-gradient glossy');

                // Remove rows
                rows.remove();
            }
        });

        // Table sort - simple
        $('#sorting-example2').tablesorter({
            headers: {
                5: { sorter: false }
            }
        });

    </script>--%>

    <script>

        $(document).ready(function () {
            // Elements
            var form = $('.wizard'),

				// If layout is centered
				centered;

            // Handle resizing (mostly for debugging)
            function handleWizardResize() {
                centerWizard(false);
            };

            // Register and first call
            $(window).on('normalized-resize', handleWizardResize);

            /*
			 * Center function
			 * @param boolean animate whether or not to animate the position change
			 * @return void
			 */
            function centerWizard(animate) {
                form[animate ? 'animate' : 'css']({ marginTop: Math.max(0, Math.round(($.template.viewportHeight - 30 - form.outerHeight()) / 2)) + 'px' });
            };

            // Initial vertical adjust
            centerWizard(false);

            // Refresh position on change step
            form.on('wizardchange', function () { centerWizard(true); });

            // Validation
            if ($.validationEngine) {
                form.validationEngine();
            }
        });

    </script>
</asp:Content>
