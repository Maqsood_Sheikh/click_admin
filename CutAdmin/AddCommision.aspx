﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddCommision.aspx.cs" Inherits="CutAdmin.AddCommision" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/AddCommision.js"></script>
    <!-- Additional styles -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">
    <!-- Additional styles -->
    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">
    <link rel="stylesheet" href="js/libs/glDatePicker/developr.fixed.css?v=1" />
    <!-- DataTables -->

   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">

        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <hgroup id="main-title" class="thin">
            <h3>Commision</h3>
            <hr>
        </hgroup>

        <div class="with-padding">
            <div class="columns">
                <div class="three-columns" id="div_Supplier">
                    <small class="input-info">Supplier:</small>
                    <select id="sel_Supplier" name="validation-select" class="select full-width OfferType">
                    </select>
                </div>
                <div class="three-columns">
                    <small class="input-info">Group:</small>
                    <select id="sel_Group" name="validation-select" class="select full-width">
                        <option value="-" selected="selected" disabled>Please select</option>
                        <option value="1">1 Star</option>
                        <option value="2">2 Star</option>
                        <option value="3">3 Star</option>
                        <option value="4">4 Star</option>
                        <option value="5">5 Star</option>
                    </select>
                </div>
                <div class="three-columns">
                    <small class="input-info">Commision:</small>
                    <input type="text" id="txt_Commision" placeholder="" class="input ui-autocomplete-input full-width">
                </div>
                <div class="three-columns">
                    <small class="input-info"></small>
                    <a class="button blue-gradient" onclick="Save()" style="cursor: pointer; margin-top: 6%">Save</a>
                    <br />

                </div>

                <div class="with-padding">
                    <table class="simple-table responsive-table" id="tbl_CommisionList" >

                        <thead>
                            <tr>
                                <th scope="col">S.N</th>                        
                                <th scope="col" class="align-center">Supplier</th>
                                <th scope="col" class="align-center">Group</th>
                                <th scope="col" class="align-center">Commision</th>
                                 <th scope="col" class="align-center">Edit</th>
                            </tr>

                        </thead>
                      <tbody>
                          <td>1</td>
                          <td>Click</td>
                           <td>A</td>
                          <td>5</td>
                           <td>1</td>
                          
                      </tbody>
                      
                    </table>
                </div>
            </div>
        </div>

    </section>


</asp:Content>
