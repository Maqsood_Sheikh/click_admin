﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for MasterHotelOfferHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class MasterHotelOfferHandler : System.Web.Services.WebService
    {
        GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //DBHandlerDataContext DB = new DBHandlerDataContext();
        string Json = "";
        JavaScriptSerializer objSerlizer = new JavaScriptSerializer();


        [WebMethod(EnableSession = true)]
        public string AddOfferMaster(string OfferNat, string SeasonName, string SeasonDate1, string SeasonDate2, string BlockNames, string SpecialDate1, string SpecialDate2, string BlockDays,
                                     string HotelOfferCode, string OfferTerm, string OfferNote, string DaysPrior, string FixedDate, string DiscountAmount, string DiscountPer,
                                     string NewRate, string FreeItemName, string FreeItemDetail, string CutCode, string OfferType)
        {



            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    Int64 Uid = 0;
                    Uid = AccountManager.GetUserByLogin();

                    string[] SeasonNameSplit = SeasonName.Split('^');
                    string[] Sdt1 = SeasonDate1.Split('^');
                    string[] Sdt2 = SeasonDate2.Split('^');
                    string[] BlockNamesSplit = BlockNames.Split('^');
                    string[] Spdt1 = SpecialDate1.Split('^');
                    string[] Spdt2 = SpecialDate2.Split('^');

                    Random generator = new Random();
                    String OfferId = generator.Next(0, 999).ToString("D3");
                    // List<tbl_CommonHotelOffer> HotelOffer = new List<tbl_CommonHotelOffer>();

                    for (int i = 0; i < SeasonNameSplit.Length - 1; i++)
                    {
                        tbl_CommonHotelOffer HotelOffer = new tbl_CommonHotelOffer();
                        //Random generator = new Random();
                        //String OfferId = generator.Next(0, 999).ToString("D3");

                        HotelOffer.OfferID = Convert.ToInt32(OfferId);
                        HotelOffer.OfferNationality = OfferNat;
                        HotelOffer.SeasonName = SeasonNameSplit[i];
                        HotelOffer.ValidFrom = Sdt1[i];
                        HotelOffer.ValidTo = Sdt2[i];
                        HotelOffer.DateType = "Season Date";
                        HotelOffer.DaysPrior = DaysPrior;
                        HotelOffer.OfferType = OfferType;
                        HotelOffer.BookBefore = FixedDate;
                        HotelOffer.DiscountPercent = Convert.ToInt32(DiscountPer);
                        HotelOffer.DiscountAmount = Convert.ToInt32(DiscountAmount);
                        HotelOffer.NewRate = Convert.ToInt32(NewRate);
                        HotelOffer.FreeItemName = FreeItemName;
                        HotelOffer.FreeItemDetail = FreeItemDetail;
                        HotelOffer.OfferTerm = OfferTerm;
                        HotelOffer.OfferNote = OfferNote;
                        HotelOffer.HotelOfferCode = HotelOfferCode;
                        HotelOffer.CutOfferCode = CutCode;
                        HotelOffer.OfferTerm = OfferTerm;
                        HotelOffer.BlockDay = BlockDays;
                        HotelOffer.Active = false;
                        HotelOffer.SupplierID = Uid;
                        DB.tbl_CommonHotelOffers.InsertOnSubmit(HotelOffer);
                        DB.SubmitChanges();

                    }
                    for (int i = 0; i < BlockNamesSplit.Length - 1; i++)
                    {
                        if (BlockNamesSplit[i] != "" || Spdt1[i] != "" || Spdt2[i] != "")
                        {
                            tbl_CommonHotelOffer HotelOffer = new tbl_CommonHotelOffer();
                            //Random generator = new Random();
                            //String OfferId = generator.Next(0, 999).ToString("D3");

                            HotelOffer.OfferID = Convert.ToInt32(OfferId);
                            HotelOffer.OfferNationality = OfferNat;
                            HotelOffer.SeasonName = BlockNamesSplit[i];
                            HotelOffer.ValidFrom = Spdt1[i];
                            HotelOffer.ValidTo = Spdt2[i];
                            HotelOffer.DateType = "Block Date";
                            HotelOffer.DaysPrior = DaysPrior;
                            HotelOffer.OfferType = OfferType;
                            HotelOffer.BookBefore = FixedDate;
                            HotelOffer.DiscountPercent = Convert.ToInt32(DiscountPer);
                            HotelOffer.DiscountAmount = Convert.ToInt32(DiscountAmount);
                            HotelOffer.NewRate = Convert.ToInt32(NewRate);
                            HotelOffer.FreeItemName = FreeItemName;
                            HotelOffer.FreeItemDetail = FreeItemDetail;
                            HotelOffer.OfferTerm = OfferTerm;
                            HotelOffer.OfferNote = OfferNote;
                            HotelOffer.HotelOfferCode = HotelOfferCode;
                            HotelOffer.CutOfferCode = CutCode;
                            HotelOffer.OfferTerm = OfferTerm;
                            HotelOffer.BlockDay = BlockDays;
                            HotelOffer.Active = true;
                            HotelOffer.SupplierID = Uid;
                            DB.tbl_CommonHotelOffers.InsertOnSubmit(HotelOffer);
                            DB.SubmitChanges();

                        }
                    }


                    Json = objSerlizer.Serialize(new { Session = 1, retCode = 1 });
                }
            }
            catch (Exception ex)
            {
                Json = objSerlizer.Serialize(new { Session = 1, retCode = 0 });
            }
            return Json;
        }


        [WebMethod(EnableSession = true)]
        public string UpdateOfferMaster(string OfferId, string OfferNat, string SeasonName, string SeasonDate1, string SeasonDate2, string BlockNames, string SpecialDate1, string SpecialDate2, string BlockDays,
                                     string HotelOfferCode, string OfferTerm, string OfferNote, string DaysPrior, string FixedDate, string DiscountAmount, string DiscountPer,
                                     string NewRate, string FreeItemName, string FreeItemDetail, string CutCode, string OfferType, string SidOffer)
        {



            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    string[] sidoffer = SidOffer.Split('^');

                    string[] SeasonNameSplit = SeasonName.Split('^');
                    string[] Sdt1 = SeasonDate1.Split('^');
                    string[] Sdt2 = SeasonDate2.Split('^');
                    string[] BlockNamesSplit = BlockNames.Split('^');
                    string[] Spdt1 = SpecialDate1.Split('^');
                    string[] Spdt2 = SpecialDate2.Split('^');

                    //  Random generator = new Random();
                    // String OfferId = generator.Next(0, 999).ToString("D3");
                    // List<tbl_CommonHotelOffer> HotelOffer = new List<tbl_CommonHotelOffer>();

                    for (int i = 0; i < SeasonNameSplit.Length - 1; i++)
                    {
                        //tbl_CommonHotelOffer HotelOffer = new tbl_CommonHotelOffer();
                        //Random generator = new Random();
                        //String OfferId = generator.Next(0, 999).ToString("D3");
                        tbl_CommonHotelOffer Update = DB.tbl_CommonHotelOffers.Single(x => x.OfferID == Convert.ToInt32(OfferId) && x.Sid == Convert.ToInt32(sidoffer[0]));
                        // HotelOffer.OfferID = Convert.ToInt32(OfferId);
                        Update.OfferNationality = OfferNat;
                        Update.SeasonName = SeasonNameSplit[i];
                        Update.ValidFrom = Sdt1[i];
                        Update.ValidTo = Sdt2[i];
                        Update.DateType = "Season Date";
                        Update.DaysPrior = DaysPrior;
                        Update.OfferType = OfferType;
                        Update.BookBefore = FixedDate;
                        Update.DiscountPercent = Convert.ToInt32(DiscountPer);
                        Update.DiscountAmount = Convert.ToInt32(DiscountAmount);
                        Update.NewRate = Convert.ToInt32(NewRate);
                        Update.FreeItemName = FreeItemName;
                        Update.FreeItemDetail = FreeItemDetail;
                        Update.OfferTerm = OfferTerm;
                        Update.OfferNote = OfferNote;
                        Update.HotelOfferCode = HotelOfferCode;
                        Update.CutOfferCode = CutCode;
                        Update.OfferTerm = OfferTerm;
                        Update.BlockDay = BlockDays;
                        Update.Active = false;
                        // DB.tbl_CommonHotelOffers.InsertOnSubmit(Update);
                        DB.SubmitChanges();

                    }
                    for (int i = 0; i < BlockNamesSplit.Length - 1; i++)
                    {
                        if (BlockNamesSplit[i] != "" || Spdt1[i] != "" || Spdt2[i] != "")
                        {
                            // tbl_CommonHotelOffer HotelOffer = new tbl_CommonHotelOffer();
                            //Random generator = new Random();
                            //String OfferId = generator.Next(0, 999).ToString("D3");
                            tbl_CommonHotelOffer Update = DB.tbl_CommonHotelOffers.Single(x => x.OfferID == Convert.ToInt32(OfferId) && x.Sid == Convert.ToInt32(sidoffer[1]));
                            Update.OfferID = Convert.ToInt32(OfferId);
                            Update.OfferNationality = OfferNat;
                            Update.SeasonName = BlockNamesSplit[i];
                            Update.ValidFrom = Spdt1[i];
                            Update.ValidTo = Spdt2[i];
                            Update.DateType = "Block Date";
                            Update.DaysPrior = DaysPrior;
                            Update.OfferType = OfferType;
                            Update.BookBefore = FixedDate;
                            Update.DiscountPercent = Convert.ToInt32(DiscountPer);
                            Update.DiscountAmount = Convert.ToInt32(DiscountAmount);
                            Update.NewRate = Convert.ToInt32(NewRate);
                            Update.FreeItemName = FreeItemName;
                            Update.FreeItemDetail = FreeItemDetail;
                            Update.OfferTerm = OfferTerm;
                            Update.OfferNote = OfferNote;
                            Update.HotelOfferCode = HotelOfferCode;
                            Update.CutOfferCode = CutCode;
                            Update.OfferTerm = OfferTerm;
                            Update.BlockDay = BlockDays;
                            Update.Active = true;
                            //  DB.tbl_CommonHotelOffers.InsertOnSubmit(Update);
                            DB.SubmitChanges();

                        }
                    }


                    Json = objSerlizer.Serialize(new { Session = 1, retCode = 1 });
                }
            }
            catch (Exception ex)
            {
                Json = objSerlizer.Serialize(new { Session = 1, retCode = 0 });
            }
            return Json;
        }



        //[WebMethod(EnableSession = true)]
        //public string GetMasterOfferSD(List<string> DateRangeSD, List<string> CheckinSD, List<string> CheckoutSD)
        //{
        //    var NewMasterOffer = (from obj in DB.tbl_CommonHotelOffers select obj).ToList();
        //    List<OfferHotel> arrOfferSD = new List<OfferHotel>();
        //    for (int i = 0; i < NewMasterOffer.Count; i++)
        //    {               
        //          //  DateTime.ParseExact(Cancellation.Date, "dd-mm-yyyy", System.Globalization.CultureInfo.InvariantCulture)
        //        for (int j = 0; j < CheckinSD.Count; j++)
        //        {
        //            if (DateTime.ParseExact(CheckinSD[j], "dd-mm-yyyy", System.Globalization.CultureInfo.InvariantCulture) <= DateTime.ParseExact(NewMasterOffer[i].ValidFrom, "dd-mm-yyyy", System.Globalization.CultureInfo.InvariantCulture) && DateTime.ParseExact(CheckoutSD[j], "dd-mm-yyyy", System.Globalization.CultureInfo.InvariantCulture) <= DateTime.ParseExact(NewMasterOffer[i].ValidTo, "dd-mm-yyyy", System.Globalization.CultureInfo.InvariantCulture))
        //            {

        //                arrOfferSD.Add(new OfferHotel { Sid = NewMasterOffer[i].Sid, SeasonName = NewMasterOffer[i].SeasonName });

        //            }
        //        }
        //    }

        //    if (arrOfferSD.Count() > 0)
        //    {
        //        return objSerlizer.Serialize(new { Session = 1, retCode = 1, arrOfferSD = arrOfferSD });
        //    }
        //    else
        //    {
        //        return objSerlizer.Serialize(new { Session = 1, retCode = 0 });
        //    }

        //}

        [WebMethod(EnableSession = true)]
        public string GetMasterOfferUp(string CheckinRV, string CheckoutRV, string Country)
        {
            using (var DB = new DBHandlerDataContext())
            {
                Int64 Uid = 0;
                Uid = AccountManager.GetUserByLogin();
                var MasterOffer = (from obj in DB.tbl_CommonHotelOffers where obj.SupplierID == Uid select obj).ToList();
                List<OfferHotelUp> arrOffer = new List<OfferHotelUp>();
                string[] Cntry = Country.Split(',');

                for (int i = 0; i < MasterOffer.Count; i++)
                {
                    string[] OfferNational = MasterOffer[i].OfferNationality.Split(',');
                    //string[] Cntry = Country.Split('^');
                    //  DateTime.ParseExact(Cancellation.Date, "dd-mm-yyyy", System.Globalization.CultureInfo.InvariantCulture)

                    for (int k = 0; k < Cntry.Length - 1; k++)
                    {
                        for (int l = 0; l < OfferNational.Length; l++)
                        {
                            DateTime checkin = DateTime.Parse(CheckinRV);
                            DateTime checkindbt = DateTime.Parse(MasterOffer[i].ValidFrom);
                            DateTime chkout = DateTime.Parse(CheckoutRV);
                            DateTime chkouttob = DateTime.Parse(MasterOffer[i].ValidTo);
                            DateTime dValidFrom = DateTime.ParseExact(MasterOffer[i].ValidFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            DateTime dValidTo = DateTime.ParseExact(MasterOffer[i].ValidTo, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            Double noDay = (dValidTo - dValidFrom).Days;
                            List<DateTime> col = new List<DateTime>();
                            while (dValidFrom <= dValidTo)
                            {
                                col.Add(dValidFrom);
                                dValidFrom = dValidFrom.AddDays(1);
                            }
                            DateTime currentDate = checkin;
                            var calendar = new List<DateTime>();
                            var targetDate = checkin;
                            while (targetDate <= chkout)
                            {
                                calendar.Add(targetDate);
                                targetDate = targetDate.AddDays(1);
                            }

                            var missingDates = (from date in calendar
                                                where col.Contains(date)
                                                select date).ToList();
                            if (missingDates.Count != 0 && (OfferNational[l] == Cntry[k]))
                            {
                                if (!arrOffer.Exists(data => data.SeasonName == MasterOffer[i].SeasonName))
                                    arrOffer.Add(new OfferHotelUp { Sid = MasterOffer[i].OfferID, SeasonName = MasterOffer[i].SeasonName });

                            }
                        }
                        //for (int l = 0; l < OfferNational.Length; l++)
                        //{
                        //    DateTime checkin = DateTime.Parse(CheckinRV);
                        //    DateTime checkindbt = DateTime.Parse(MasterOffer[i].ValidFrom);
                        //    DateTime chkout = DateTime.Parse(CheckoutRV);
                        //    DateTime chkouttob = DateTime.Parse(MasterOffer[i].ValidTo);
                        //    //if ((DateTime.ParseExact(CheckinRV[j], "dd-mm-yyyy", System.Globalization.CultureInfo.InvariantCulture) <= DateTime.ParseExact(MasterOffer[i].ValidFrom,"dd-mm-yyyy", System.Globalization.CultureInfo.InvariantCulture)) &&
                        //    //    (DateTime.ParseExact(CheckoutRV[j],"dd-mm-yyyy", System.Globalization.CultureInfo.InvariantCulture) <= DateTime.ParseExact(MasterOffer[i].ValidTo,"dd-mm-yyyy", System.Globalization.CultureInfo.InvariantCulture)) &&
                        //    //    (OfferNational[l] == Cntry[k]))
                        //    //{
                        //    //    if (!arrOffer.Exists(data => data.SeasonName == MasterOffer[i].SeasonName ))
                        //    //    arrOffer.Add(new OfferHotel { Sid = MasterOffer[i].Sid, SeasonName = MasterOffer[i].SeasonName });

                        //    //}
                        //    if ((checkin <= checkindbt) && (chkout >= chkouttob) && (OfferNational[l] == Cntry[k]))
                        //    {
                        //        if (!arrOffer.Exists(data => data.SeasonName == MasterOffer[i].SeasonName))
                        //            arrOffer.Add(new OfferHotelUp { Sid = MasterOffer[i].Sid, OfferId = MasterOffer[i].OfferID, SeasonName = MasterOffer[i].SeasonName });

                        //    }
                        //}



                    }
                }

                if (arrOffer.Count() > 0)
                {
                    return objSerlizer.Serialize(new { Session = 1, retCode = 1, arrOffer = arrOffer });
                }
                else
                {
                    return objSerlizer.Serialize(new { Session = 1, retCode = 0 });
                }
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetMasterOffer(List<string> CheckinRV, List<string> CheckoutRV, string Country)
        {
            using (var DB = new DBHandlerDataContext())
            {
                Int64 Uid = 0;
                Uid = AccountManager.GetUserByLogin();

                var MasterOffer = (from obj in DB.tbl_CommonHotelOffers where obj.SupplierID == Uid select obj).ToList();
                List<OfferHotel> arrOffer = new List<OfferHotel>();
                string[] Cntry = Country.Split(',');

                for (int i = 0; i < MasterOffer.Count; i++)
                {
                    string[] OfferNational = MasterOffer[i].OfferNationality.Split(',');
                    //string[] Cntry = Country.Split('^');
                    //  DateTime.ParseExact(Cancellation.Date, "dd-mm-yyyy", System.Globalization.CultureInfo.InvariantCulture)
                    for (int j = 0; j < CheckinRV.Count; j++)
                    {
                        for (int k = 0; k < Cntry.Length - 1; k++)
                        {
                            for (int l = 0; l < OfferNational.Length; l++)
                            {
                                DateTime checkin = DateTime.Parse(CheckinRV[j]);
                                DateTime checkindbt = DateTime.Parse(MasterOffer[i].ValidFrom);
                                DateTime chkout = DateTime.Parse(CheckoutRV[j]);
                                DateTime chkouttob = DateTime.Parse(MasterOffer[i].ValidTo);
                                DateTime dValidFrom = DateTime.ParseExact(MasterOffer[i].ValidFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                DateTime dValidTo = DateTime.ParseExact(MasterOffer[i].ValidTo, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                Double noDay = (dValidTo - dValidFrom).Days;
                                List<DateTime> col = new  List<DateTime>();
                                while (dValidFrom <= dValidTo)
                                {
                                    col.Add(dValidFrom);
                                    dValidFrom = dValidFrom.AddDays(1);
                                }
                                DateTime currentDate = checkin;
                                var calendar = new List<DateTime>();
                                var targetDate = checkin;
                                while (targetDate <= chkout)
                                {
                                    calendar.Add(targetDate);
                                    targetDate = targetDate.AddDays(1);
                                }

                                var missingDates = (from date in calendar
                                                    where col.Contains(date)
                                                    select date).ToList();
                                if (missingDates.Count != 0 && (OfferNational[l] == Cntry[k]))
                                {
                                    if (!arrOffer.Exists(data => data.SeasonName == MasterOffer[i].SeasonName))
                                        arrOffer.Add(new OfferHotel { Sid = MasterOffer[i].OfferID, SeasonName = MasterOffer[i].SeasonName });

                                }
                            }

                        }

                    }
                }

                if (arrOffer.Count() > 0)
                {
                    return objSerlizer.Serialize(new { Session = 1, retCode = 1, arrOffer = arrOffer });
                }
                else
                {
                    return objSerlizer.Serialize(new { Session = 1, retCode = 0 });
                }
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetAllSeasonsList()
        {
            using (var DB = new DBHandlerDataContext())
            {
                var UserType = objGlobalDefault.UserType;
                Int64 Uid = 0;

                GenralHandler objgnrl = new GenralHandler();
                Uid = objgnrl.GetUserID(UserType);

                var SeasonOffer = (from obj in DB.tbl_CommonHotelOffers where obj.SupplierID == Uid select obj).Distinct().OrderByDescending(d => d.Sid).ToList();

                if (SeasonOffer.Count() > 0)
                {
                    return objSerlizer.Serialize(new { Session = 1, retCode = 1, SeasonOffer = SeasonOffer });
                }
                else
                {
                    return objSerlizer.Serialize(new { Session = 1, retCode = 0 });
                }
            }
        }

        [WebMethod(true)]
        public string DeleteOffer(Int64 OfferId, Int64 sid)
        {

            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    tbl_CommonHotelOffer Delete = DB.tbl_CommonHotelOffers.Single(x => x.OfferID == OfferId && x.Sid == sid);
                    DB.tbl_CommonHotelOffers.DeleteOnSubmit(Delete);
                    DB.SubmitChanges();
                    return objSerlizer.Serialize(new { Session = 1, retCode = 1 });
                }
            }
            catch
            {
                return objSerlizer.Serialize(new { Session = 1, retCode = 0 });
            }

        }

        [WebMethod(true)]
        public string GetOfferByID(Int64 Id)
        {

            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    var Offer = (from obj in DB.tbl_CommonHotelOffers
                                 join objC in DB.tbl_HCities on obj.OfferNationality equals objC.Country
                                 where obj.OfferID == Id
                                 select new
                                 {
                                     obj.Active,
                                     obj.BlockDay,
                                     obj.BookBefore,
                                     obj.CutOfferCode,
                                     obj.DateType,
                                     obj.DaysPrior,
                                     obj.DiscountAmount,
                                     obj.DiscountPercent,
                                     obj.FreeItemDetail,
                                     obj.FreeItemName,
                                     obj.HotelOfferCode,
                                     obj.NewRate,
                                     obj.OfferID,
                                     obj.OfferNote,
                                     obj.OfferTerm,
                                     obj.OfferType,
                                     obj.SeasonName,
                                     obj.Sid,
                                     obj.ValidFrom,
                                     obj.ValidTo,
                                     objC.Countryname,
                                     objC.Country

                                 }).Distinct().ToList();
                    return objSerlizer.Serialize(new { Session = 1, retCode = 1, offer = Offer });
                }
            }
            catch
            {
                return objSerlizer.Serialize(new { Session = 1, retCode = 0 });
            }

        }
    }

    public class OfferHotel
    {
        public Int64 Sid { get; set; }
        public string SeasonName { get; set; }
    }

    public class OfferHotelUp
    {
        public Int64 Sid { get; set; }
        public Int64 OfferId { get; set; }
        public string SeasonName { get; set; }
    }
}
