﻿using DOTWLib.Request;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;

namespace CutAdmin.Models
{
    public class DoTWAvailRequest
    {
        public string username { get; set; }
        public string password { get; set; }
        public string id { get; set; }
        public int source { get; set; }
        public string product { get; set; }
        public string command { get; set; }
        public string fDate { get; set; }
        public string tDate { get; set; }
        public Currency currency { get; set; }
        public string rating { get; set; }
        public string hotelname { get; set; }
        //public Rooms roomTypeCode { get; set; }
        public int Night { get; set; }
        public List<Room> room { get; set; }
        public City city { get; set; }
        public List<HotelOccupancy> HotelOccupancy { get; set; }
        public string GenerateXML()
        {
            string m_xml = "";
            DateTime fd = Convert.ToDateTime(fDate, new System.Globalization.CultureInfo("en-GB"));
            DateTime td = Convert.ToDateTime(tDate, new System.Globalization.CultureInfo("en-GB"));
            string ffd = fd.ToString("yyyy-MM-dd");
            string ttd = td.ToString("yyyy-MM-dd");
            StringWriter sw = new StringWriter();
            using (XmlTextWriter textWriter = new XmlTextWriter(sw))
            {
                //textWriter.WriteStartDocument();
                textWriter.WriteStartElement("customer");
                textWriter.WriteStartElement("username");
                textWriter.WriteValue(username);
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("password");
                textWriter.WriteValue(password);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("id");
                textWriter.WriteValue(id);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("source");
                textWriter.WriteValue(source);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("product");
                textWriter.WriteValue(product);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("request");
                textWriter.WriteAttributeString("command", command);


                //booking details start
                textWriter.WriteStartElement("bookingDetails");

                textWriter.WriteStartElement("fromDate");
                //textWriter.WriteValue("2016-06-30");
                textWriter.WriteValue(ffd);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("toDate");
                //textWriter.WriteValue("2016-07-01");
                textWriter.WriteValue(ttd);
                textWriter.WriteEndElement();

                if (currency != null)
                {
                    textWriter.WriteStartElement("currency");
                    textWriter.WriteValue(currency.value);
                    textWriter.WriteEndElement();
                }

                //test begin
                //rooms start
                textWriter.WriteStartElement("rooms");
                // 1Feb
                textWriter.WriteAttributeString("no", room.Count.ToString());
                // end 1 Feb
                //room start


                //test 1 Feb
                //textWriter.WriteAttributeString("no", "1");
                for (int i = 0; i < room.Count; i++)
                {
                    textWriter.WriteStartElement("room");
                    textWriter.WriteAttributeString("runno", i.ToString());

                    textWriter.WriteStartElement("adultsCode");
                    textWriter.WriteValue(room[i].adults);
                    textWriter.WriteEndElement();
                    textWriter.WriteStartElement("children");
                    textWriter.WriteAttributeString("no", room[i].children.Count.ToString());
                    //textWriter.WriteValue(room[i].children.Count);
                    for (int j = 0; j < room[i].children.Count; j++)
                    {
                        textWriter.WriteStartElement("child");
                        textWriter.WriteAttributeString("runno", j.ToString());
                        textWriter.WriteValue(room[i].children[j].Age);
                        textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();


                    textWriter.WriteStartElement("rateBasis");
                    textWriter.WriteValue(room[i].rateBasis);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("passengerNationality");
                    textWriter.WriteValue(room[i].passengerNationality.code);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("passengerCountryOfResidence");
                    textWriter.WriteValue(room[i].passengerCountryOfResidence.code);
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();
                }


                //textWriter.WriteStartElement("room");
                //textWriter.WriteAttributeString("runno", "0");

                //textWriter.WriteStartElement("adultsCode");
                //textWriter.WriteValue("2");
                //textWriter.WriteEndElement();
                //textWriter.WriteStartElement("children");
                //textWriter.WriteAttributeString("no", "0");
                //textWriter.WriteEndElement();


                //textWriter.WriteStartElement("rateBasis");
                //textWriter.WriteValue("-1");
                //textWriter.WriteEndElement();

                //textWriter.WriteStartElement("passengerNationality");
                //textWriter.WriteValue("81");
                //textWriter.WriteEndElement();

                //textWriter.WriteStartElement("passengerCountryOfResidence");
                //textWriter.WriteValue("72");
                //textWriter.WriteEndElement();

                //textWriter.WriteEndElement();

                //end test 1 feb

                //....................................................................




                //textWriter.WriteEndElement();
                ////room end

                textWriter.WriteEndElement();
                //rooms end

                //test end

                textWriter.WriteEndElement();
                //bookingDetails ends

                //return starts
                textWriter.WriteStartElement("return");


                //getRooms starts
                textWriter.WriteStartElement("getRooms");
                textWriter.WriteValue("true");
                textWriter.WriteEndElement();
                //getRooms end


                //filters starts
                textWriter.WriteStartElement("filters");
                textWriter.WriteAttributeString("xmlns:a", "http://us.dotwconnect.com/xsd/atomicCondition");
                textWriter.WriteAttributeString("xmlns:c", "http://us.dotwconnect.com/xsd/complexCondition");

                textWriter.WriteStartElement("city");
                //textWriter.WriteValue("2014");
                textWriter.WriteValue(city.code);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("noPrice");
                textWriter.WriteValue("true");
                textWriter.WriteEndElement();


                textWriter.WriteEndElement();
                //filters ends



                //fields start
                textWriter.WriteStartElement("fields");
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("preferred");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("builtYear");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("renovationYear");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("floors");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("noOfRooms");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("luxury");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("fullAddress");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("description1");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("description2");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("description2");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("hotelName");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("address");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("zipCode");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("location");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("locationId");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("location1");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("location2");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("location3");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("cityName");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("cityCode");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("stateName");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("stateCode");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("countryName");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("countryCode");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("attraction");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("regionName");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("amenitie");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("leisure");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("business");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("transportation");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("hotelPhone");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("hotelCheckIn");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("hotelCheckOut");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("minAge");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("rating");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("images");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("field");
                textWriter.WriteValue("geoPoint");
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("roomField");
                textWriter.WriteValue("roomInfo");
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("roomField");
                textWriter.WriteValue("roomAmenities");
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("roomField");
                textWriter.WriteValue("name");
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("roomField");
                textWriter.WriteValue("twin");
                textWriter.WriteEndElement();


                textWriter.WriteEndElement();
                // fields end //

                textWriter.WriteEndElement();
                //return ends

                textWriter.WriteEndElement();
                //request end

                textWriter.WriteEndElement();
                //customer end
            }
            m_xml = sw.ToString();

            // sw.Close();
            sw.Dispose();
            return m_xml;
        }

        public string GeneratePriceXML()
        {
            string m_xml = "";

            StringWriter sw = new StringWriter();
            using (XmlTextWriter textWriter = new XmlTextWriter(sw))
            {
                DateTime fd = Convert.ToDateTime(fDate, new System.Globalization.CultureInfo("en-GB"));
                DateTime td = Convert.ToDateTime(tDate, new System.Globalization.CultureInfo("en-GB"));
                string ffd = fd.ToString("yyyy-MM-dd");
                string ttd = td.ToString("yyyy-MM-dd");
                textWriter.WriteStartElement("customer");
                textWriter.WriteStartElement("username");
                textWriter.WriteValue(username);
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("password");
                textWriter.WriteValue(password);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("id");
                textWriter.WriteValue(id);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("source");
                textWriter.WriteValue(source);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("product");
                textWriter.WriteValue(product);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("request");
                textWriter.WriteAttributeString("command", command);


                //booking details start
                textWriter.WriteStartElement("bookingDetails");

                textWriter.WriteStartElement("fromDate");
                //textWriter.WriteValue("2016-06-30");
                textWriter.WriteValue(ffd);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("toDate");
                //textWriter.WriteValue("2016-07-01");
                textWriter.WriteValue(ttd);
                textWriter.WriteEndElement();

                if (currency != null)
                {
                    textWriter.WriteStartElement("currency");
                    textWriter.WriteValue(currency.value);
                    textWriter.WriteEndElement();
                }

                //test begin
                //rooms start
                textWriter.WriteStartElement("rooms");
                // 1 Feb
                textWriter.WriteAttributeString("no", room.Count.ToString());
                //end 1 feb

                //test 1 Feb
                //textWriter.WriteAttributeString("no", "1");
                for (int i = 0; i < room.Count; i++)
                {
                    textWriter.WriteStartElement("room");
                    textWriter.WriteAttributeString("runno", i.ToString());

                    textWriter.WriteStartElement("adultsCode");
                    textWriter.WriteValue(room[i].adults);
                    textWriter.WriteEndElement();
                    textWriter.WriteStartElement("children");
                    textWriter.WriteAttributeString("no", room[i].children.Count.ToString());
                    //textWriter.WriteValue(room[i].children.Count);
                    for (int j = 0; j < room[i].children.Count; j++)
                    {
                        textWriter.WriteStartElement("child");
                        textWriter.WriteAttributeString("runno", j.ToString());
                        textWriter.WriteValue(room[i].children[j].Age);
                        textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();


                    textWriter.WriteStartElement("rateBasis");
                    textWriter.WriteValue(room[i].rateBasis);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("passengerNationality");
                    textWriter.WriteValue(room[i].passengerNationality.code);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("passengerCountryOfResidence");
                    textWriter.WriteValue(room[i].passengerCountryOfResidence.code);
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();
                }


         

                textWriter.WriteEndElement();
                //rooms end

                //test end

                textWriter.WriteEndElement();
                //bookingDetails ends

                //return starts
                textWriter.WriteStartElement("return");


                //getRooms starts
                textWriter.WriteStartElement("getRooms");
                textWriter.WriteValue("true");
                textWriter.WriteEndElement();
                //getRooms end


                //filters starts
                textWriter.WriteStartElement("filters");
                textWriter.WriteAttributeString("xmlns:a", "http://us.dotwconnect.com/xsd/atomicCondition");
                textWriter.WriteAttributeString("xmlns:c", "http://us.dotwconnect.com/xsd/complexCondition");

                textWriter.WriteStartElement("city");
                //textWriter.WriteValue("2014");
                textWriter.WriteValue(city.code);
                textWriter.WriteEndElement();

                //textWriter.WriteStartElement("noPrice");
                //textWriter.WriteValue("false");
                //textWriter.WriteEndElement();


                textWriter.WriteEndElement();
                //filters ends

                textWriter.WriteEndElement();
                //return ends

                textWriter.WriteEndElement();
                //request end

                textWriter.WriteEndElement();
                //customer end
            }
            //test 5 Feb
            m_xml = sw.ToString();

            // sw.Close();
            sw.Dispose();
            return m_xml;
        }
    }
}