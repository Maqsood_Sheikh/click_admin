﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CutAdmin.Models
{

    public class RoomsRate
    {
        public int SupplierID { get; set; }
        public string Supplier { get; set; }
        public Int64 RoomTypeID { get; set; }
        public string RoomType { get; set; }
        public List<Date> Rates { get; set; }

    }

    public class Date
    {
        public string Dates { get; set; }
        public List<Rate> Rates { get; set; }

    }
    public class Rate
    {
        public Int64 RoomTypeID { get; set; }
        public string RoomType { get; set; }
        public int MealPlanID { get; set; }
        public string MealPlan { get; set; }
        public float RoomRate { get; set; }
        public float CWB { get; set; }
        public float ExtraBed { get; set; }
        public float OfferRate { get; set; }
    }
}