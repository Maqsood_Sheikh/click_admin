﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CutAdmin.Models
{
    public class StatusCode
    {
        // For IN
        public string Request { get; set; }
        public string RequestHeader { get; set; }
        public string ResponseHeader { get; set; }
        public string Response { get; set; }
        public int Status { get; set; }

        // For OUT
        public string RequestOUT { get; set; }
        public string RequestHeaderOUT { get; set; }
        public string ResponseHeaderOUT { get; set; }
        public string ResponseOUT { get; set; }
        public int StatusOUT { get; set; }

        public AvailRequest DisplayRequest { get; set; }
        public DateTime FDate { get; set; }
        public DateTime TDate { get; set; }
        public List<HotelLib.Request.Occupancy> Occupancy { get; set; }
        public List<HotelLib.Request.Occupancy> ActualOccupancy { get; set; }
    }
}