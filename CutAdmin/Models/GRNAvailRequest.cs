﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CutAdmin.Models
{
    public class GRNAvailRequest
    {
        public string DestinationCode { get; set; }
        public string Location { get; set; }
        public string CheckIn { get; set; }
        public string CheckOut { get; set; }
        public int Room { get; set; }
        public int Adult { get; set; }
        public int Child { get; set; }
        public int Night { get; set; }
        public string NationalityCode { get; set; }
        //public string NationalityCountry { get; set; }
        public string HotelName { get; set; }
        public string HotelCode { get; set; }
        public string StarRating { get; set; }
        public List<GRNLib.Request.Availability.Occupancy> List_Occupancy { get; set; }
        //public List<HotelLib.Request.HotelOccupancy> List_ActualOccupancy { get; set; }
    }
}