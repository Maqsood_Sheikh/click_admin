﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CutAdmin.Models
{
    public class DOTWStatusCode
    {
        public int Night { get; set; }
        public string Request { get; set; }
        public string RequestHeader { get; set; }
        public string ResponseHeader { get; set; }
        public string Response { get; set; }
        public int Status { get; set; }
        public DoTWAvailRequest DisplayRequest { get; set; }
        public DateTime FDate { get; set; }
        public DateTime TDate { get; set; }
        public List<DOTWLib.Request.Occupancy> Occupancy { get; set; }
    }
}