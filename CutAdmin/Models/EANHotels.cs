﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CutAdmin.Models
{
    public class EANHotels
    {
        public string SessionId { get; set; }
        public Int64 noNights { get; set; }
        public Int64 noRoomRequest { get; set; }
        //public string Location { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public float MinPrice { get; set; }
        public int CountHotel { get; set; }
        public float MaxPrice { get; set; }
        public List<string> Facility { get; set; }
        public List<EANLib.Response.HotelAddress> Location { get; set; }
        public List<EANLib.Response.Category> Category { get; set; }
        public EANAvailRequest DisplayRequest { get; set; }
        public List<EANLib.Response.HotelSummary> HotelDetail { get; set; }
        public List<EANLib.Request.Room> Occupancy { get; set; }
        public EANLib.Response.HotelRoomAvailabilityResponse HotelRoomAvailabilityResponse { get; set; }
        public EANLib.Response.HotelRoomResponse HotelRoomResponse { get; set; }
    }
}