﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Agent/AgentMaster.Master" AutoEventWireup="true" CodeBehind="Agentstatements.aspx.cs" Inherits="CutAdmin.Agent.Agentstatements" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/AgencyStatement.js?v=1.1"></script>
    <script>
        $(function () {
            //debugger


            $("#datepicker3").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy"
            });
            $("#datepicker4").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy"
            });
            GetAgencyStatementAll()
        })
    </script>
    <script src="../Scripts/CountryCityCode.js"></script>
    <script type="text/javascript">
        var CurrencyClass = '<%=Session["CurrencyClass"]%>';
    </script>
    <script>
        jQuery(document).ready(function () {
            jQuery('.fa-filter').click(function () {
                jQuery(' #filter').slideToggle();
                jQuery('.searchBox li a ').on('click', function () {
                    jQuery('.filterBox #filter').slideUp();
                });
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <!-- Main content -->
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>My Statement</h1>
            <hr />
            <h2><%--<a href="AddSupplier.aspx" class="addnew"><i class="fa fa-user-plus"></i></a>--%><a href="#" class="addnew"><i class="fa fa-filter"></i></a></h2>
            <br />
            <div id="filter" class="with-padding anthracite-gradient" style="display: none;">
                <form class="form-horizontal">
                    <div class="columns">
                        <div class="one-columns">
                            <label>Type</label>
                            <div class="full-width button-height">
                                <select id="TransactionType" class="select">
                                    <option selected="selected" value="Both">Both</option>
                                    <option value="Credited">Credited</option>
                                    <option value="Debited">Debited</option>
                                </select>
                            </div>
                        </div>
                        <div class="three-columns four-columns-tablet twelve-columns-mobile">
                            <label>From </label>
                            <div class="input full-width">
                                <input type="text" name="prompt-value" placeholder="dd/mm/yy" id="datepicker3" class="input-unstyled" value="">
                            </div>
                        </div>

                        <div class="three-columns four-columns-tablet twelve-columns-mobile">
                            <label>To</label>
                            <div class="input full-width">
                                <input type="text" name="prompt-value" placeholder="dd/mm/yy" id="datepicker4" class="input-unstyled" value="">
                            </div>
                        </div>
                        <div class="four-columns four-columns-tablet twelve-columns-mobile">
                            <label>Reference No</label>
                            <div class="input full-width">
                                <input type="text" id="txt_Reference" class="input-unstyled full-width">
                            </div>
                        </div>
                    </div>

                    <div class="columns">
                        <div class="eight-columns twelve-columns-mobile formBTn">
                        </div>
                        <div class="two-columns twelve-columns-mobile formBTn">
                            <br />
                            <button type="button" class="button anthracite-gradient" onclick="GetAgencyStatementByDate()">Search</button>
                            <button type="button" class="button anthracite-gradient" onclick="GetAgencyStatementAll()">ALL</button>

                        </div>
                        <div class="two-columns four-columns-tablet twelve-columns-mobile">
                            <br />
                            <span class="icon-pdf right" onclick="ExportAgencyStatement('PDF')">
                                <img src="../img/PDF-Viewer-icon.png" style="cursor: pointer" title="Export To Pdf" height="35" width="35">
                            </span>
                            <span class="icon-excel right" onclick="ExportAgencyStatement('Excel')">
                                <img src="../img/Excel-2-icon.png" style="cursor: pointer" title="Export To Excel" id="btnPDF" height="35" width="35"></span>
                        </div>
                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
            <br />
        </hgroup>
        <div class="with-padding">
            <div class="respTable">
                <table class="table responsive-table" id="tbl_AgencyStatement">
                    <thead>
                        <tr>
                            <th scope="col" class="align-center">S.No</th>
                            <th scope="col" class="align-center">Date</th>
                            <th scope="col" class="align-center hide-on-mobile">Reference No.</th>
                            <th scope="col" class="align-center hide-on-mobile-portrait">Particulars</th>
                            <th scope="col" class="align-center hide-on-tablet">Credited</th>
                            <th scope="col" class="align-center">Debited</th>
                            <th scope="col" class="align-center">Balnce</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </section>


</asp:Content>