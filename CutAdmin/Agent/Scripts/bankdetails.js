﻿$(function () {
    LoadBankDetails();
    GetCountryy();
});

function LoadBankDetails() {
    $("#tbl_BankDetails").dataTable().fnClearTable();
    $("#tbl_BankDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "../GenralHandler.asmx/GetBankDetails",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0 && result.retCode == 0) {
                Success("Your session has been expired!")
                setTimeout(function () {
                    window.location.href = "../Default.aspx";
                }, 2000);
            }
            else if (result.retCode == 1) {
                var BankDetails = result.BankDetails;
                console.log(BankDetails);
                var row = '';
                for (var i = 0; i < BankDetails.length; i++) {
                    row += '<tr>'
                    row += '<td class="align-center">' + (i + 1) + '</td>'
                    row += '<td class="align-center">' + BankDetails[i].BankName + '</td>'
                    row += '<td class="align-center">' + BankDetails[i].AccountNo + '</td>'
                    row += '<td class="align-center">' + BankDetails[i].Branch + '</td>'
                    row += '<td class="align-center">' + BankDetails[i].SwiftCode + '</td>'
                    row += '<td class="align-center">' + BankDetails[i].Country + '</td>'
                    row += '<td class="align-center"><span class="button-group children-tooltip actiontab">'
                    row += '<a href="#" class="button" title="Edit" onclick="EditBankDetail(\'' + BankDetails[i].sid + '\',\'' + BankDetails[i].BankName + '\',\'' + BankDetails[i].AccountNo + '\',\'' + BankDetails[i].Branch + '\',\'' + BankDetails[i].SwiftCode + '\' ,\'' + BankDetails[i].Country + '\')"><span class="icon-pencil"></span></a>'
                    row += '<a href="#" class="button" title="Delete" onclick="DeleteBankDetail(\'' + BankDetails[i].sid + '\',\'' + BankDetails[i].BankName + '\')"><span class="icon-trash"></span></a>'
                    row += '</span></td>'
                    row += '</tr>'
                }
                $('#tbl_BankDetails tbody').html(row);
                $("#tbl_BankDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }
        },
        error: function () {
            Success("An error occured while loading details.");
        }
    });
}

function GetCountryy() {
    $.ajax({
        type: "POST",
        url: "../GenralHandler.asmx/GetCountry",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCountry = result.Country;
                if (arrCountry.length > 0) {
                    $("#sel_Nationality").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any Country</option>';
                    for (i = 0; i < arrCountry.length; i++) {
                        ddlRequest += '<option value="' + arrCountry[i].Countryname + '">' + arrCountry[i].Countryname + '</option>';
                    }
                    $("#sel_Nationality").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}

function AddBankDetails() {
    var BankName = $('#txtBankName').val();
    var AccountNo = $('#txtAccountNo').val();
    var Branch = $('#txtBranch').val();
    var SwiftCode = $('#txtSwiftCode').val();
    var Country = $('#sel_Nationality').val();

    if (!BankName) {
        Success('Please enter Bank Name.');
        return false;
    } else if (!AccountNo) {
        Success('Please enter Account No.');
        return false;
    } else if (!Branch) {
        Success('Please enter Branch.');
        return false;
    } else if (!SwiftCode) {
        Success('Please enter Swift Code.');
        return false;
    } else if (Country == '-') {
        Success('Please select country.');
        return false;
    }

    var data = {
        BankName: BankName,
        AccountNo: AccountNo,
        Branch: Branch,
        SwiftCode: SwiftCode,
        Country:Country
    }

    $.ajax({
        type: "POST",
        url: "../GenralHandler.asmx/AddBankDetails",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0 && result.retCode == 0) {
                Success("Your session has been expired!")
                setTimeout(function () {
                    window.location.href = "../Default.aspx";
                }, 2000);
            }
            else if (result.retCode == 1) {
                Success("Bank details saved successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            }
        },
        error: function () {
            Success("An error occured while saving details.");
        }
    });
}

var ID = "";

function EditBankDetail(sid, BankName, AccountNo, Branch, SwiftCode, Country) {
    ID = sid;
    $('#txtBankName').val(BankName);
    $('#txtAccountNo').val(AccountNo);
    $('#txtBranch').val(Branch);
    $('#txtSwiftCode').val(SwiftCode);
    GetNationality(Country);

    $('#btnAddBankDetails').attr("onclick", "UpdateBankDetail()");
    $('#btnAddBankDetails').attr("value", "Update");
    $('#btn_Cancel').css("display", "");
}

function GetNationality(Countryname) {
    try {
        var checkclass = document.getElementsByClassName('check');
        var Country = $.grep(arrCountry, function (p) { return p.Countryname == Countryname; }).map(function (p) { return p.Country; });

        $("#DivCountry .select span")[0].textContent = Countryname;
        for (var i = 0; i <= Country.length - 1; i++) {
            $('input[value="' + Countryname + '"][class="country"]').prop("selected", true);
            $("#sel_Nationality").val(Countryname);
        }
    }
    catch (ex)
    { }
}

function UpdateBankDetail() {
    var BankName = $('#txtBankName').val();
    var AccountNo = $('#txtAccountNo').val();
    var Branch = $('#txtBranch').val();
    var SwiftCode = $('#txtSwiftCode').val();
    var Country = $('#sel_Nationality').val();

    if (!BankName) {
        Success('Please enter Bank Name.');
        return false;
    } else if (!AccountNo) {
        Success('Please enter Account No.');
        return false;
    } else if (!Branch) {
        Success('Please enter Branch.');
        return false;
    } else if (!SwiftCode) {
        Success('Please enter Swift Code.');
        return false;
    } else if (Country == '-') {
        Success('Please select country.');
        return false;
    }

    var data = {
        sid: ID,
        BankName: BankName,
        AccountNo: AccountNo,
        Branch: Branch,
        SwiftCode: SwiftCode,
        Country: Country
    }

    $.ajax({
        type: "POST",
        url: "../GenralHandler.asmx/UpdateBankDetail",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0 && result.retCode == 0) {
                Success("Your session has been expired!")
                setTimeout(function () {
                    window.location.href = "../Default.aspx";
                }, 2000);
               
            }
            else if (result.retCode == 1) {
                Success("Bank details updated successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            
            }
        },
        error: function () {
            Success("An error occured while updating details.");
        }
    });
}

function Cancel() {
    window.location.reload();
}

function DeleteBankDetail(sid, BankName) {
//    $.modal({
//        content: '<p class="avtiveDea">Are you sure you want to delete this bank detail?</strong></p>' +
//'<p class="text-alignright text-popBtn"><button type="button" class="button anthracite-gradient" onclick="Delete(' + sid + ')">OK</button></p>',


//        width: 300,
//        scrolling: false,
//        actions: {
//            'Cancle': {
//                color: 'red',
//                click: function (win) { win.closeModal(); }
//            }
//        },
//        buttons: {
//            'Cancle': {
//                classes: 'anthracite-gradient glossy',
//                click: function (win) { win.closeModal(); }
//            }
//        },
//        buttonsLowPadding: false
//    });


    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to delete <span class=\"orange\">' + BankName + '</span>?</span>?</p>',
        function () {
            var data = {
                sid: sid
            }
            $.ajax({
                type: "POST",
                url: "../GenralHandler.asmx/DeleteBankDetail",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.Session == 0 && result.retCode == 0) {
                        Success("Your session has been expired!")
                        setTimeout(function () {
                            window.location.href = "../Default.aspx";
                        }, 2000);
                    }
                    else if (result.retCode == 1) {
                        Success("Bank detail deleted successfully.");
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                },
                error: function () {
                    Success("An error occured while deleting details.");
                }
            });
        },
        function () {
            $('#modals').remove();
        }
    );
}

//function Delete(sid) {
//    var data = {
//        sid: sid
//    }
//    $.ajax({
//        type: "POST",
//        url: "../GenralHandler.asmx/DeleteBankDetail",
//        data: JSON.stringify(data),
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        success: function (response) {
//            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//            if (result.Session == 0 && result.retCode == 0) {
//                Success("Your session has been expired!")
//                setTimeout(function () {
//                    window.location.href = "../Default.aspx";
//                }, 2000);
              
//            }
//            else if (result.retCode == 1) {
//                Success("Bank detail deleted successfully.");
//                setTimeout(function () {
//                    window.location.reload();
//                }, 2000);
         
//            }
//        },
//        error: function () {
//            Success("An error occured while deleting details.");
//        }
//    });
//}
