﻿function Success(msg) {
    $.modal({
        contentBg: false,
        contentAlign: 'center',
        content: msg,
        resizable: false,
        actions: {},
        buttons: {
            'Close': {
                classes: 'green-gradient',
                click: function (modal) { modal.closeModal(); }
            }

        },
        buttonsAlign: 'center'
    });
}

function Confirm(msg, Method, id) {
    $.modal.confirm(msg, function () {
        var id = [];
        if (arg != null) {
            for (var i = 0; i < arg.length; i++) {
                id.push('"' + arg[i] + '"')
            }
        }
        if (arg == null)
            $(".blue-gradient").setAttribute("onclick", Method + "()")
        else
            $(".blue-gradient").setAttribute("onclick", Method + "(" + id + ")")
    }, function () {
        $('#modals').remove();
    });
}

function AlertDanger(msg) {
    notify(msg, {
        icon: '../img/demo/error.png',
        system: true
    });
}