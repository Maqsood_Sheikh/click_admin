﻿var arrGroup = new Array();
var arrSearchOccupancy = new Array();
var arrCharges = new Array();
function GenrateB2bRates(arrGroup,ID) {
    try {
        arrSearchOccupancy = new Array();
        $("#" + ID).empty();
        var Html = '';
        var arrRoomNo = new Array();
        for (var i = 0; i < arrGroup.RoomOccupancy.length; i++) {
            for (var r = 0; r < arrGroup.RoomOccupancy[i].RoomNo.length; r++) {
                arrRoomNo.push(arrGroup.RoomOccupancy[i].RoomNo[r])
            }
        }
        arrRoomNo.sort(function (a, b) { return a - b });// Sort By RoomNo
        for (var r = 0; r < arrRoomNo.length; r++) {
            for (var i = 0; i < arrGroup.RoomOccupancy.length; i++) {
                var arrRooms = arrGroup.RoomOccupancy[i].RoomNo;
                var arrOccupant = $.grep(arrRooms, function (R) { return R == arrRoomNo[r] })
                                    .map(function (R) { return R; });
                if(arrOccupant.length !=0)
                {
                    arrSearchOccupancy.push(arrGroup.RoomOccupancy[i]);
                    break;
                }
            }
        }
        // Re arrangging Room By Search
        for (var i = 0; i < arrSearchOccupancy.length; i++) {
            Html = ''
            var DefaultSelected = "checked";
            if (i != 0)
                Html += '<br/>'
            Html += '<h3 class="silver-gradient">Room No. '+parseInt(i+1)
            Html += Rooms;
            Html += '<span class="size13 bold"> (' + arrSearchOccupancy[i].AdultCount + ' Adults' + ' & ' + arrSearchOccupancy[i].ChildCount + ' Children </span>';
            if (arrSearchOccupancy[i].ChildCount != 0)
                Html += ' with Ages : ' + arrSearchOccupancy[i].ChildAges;

            Html += ')</h3>';
            Html += '<table class="tbl_Rates table responsive-table RoomOccupancy' + i + '_' + ID + '" id="tbl_Occupancy_'+i+ '_' + ID + '">'
            Html += '<thead>'
            Html += '<tr><th>Room Name</th><th>Room Type</th><th>Rate</th><th>Cancellation</th><th>Select'

            Html += '<input type="hidden" value="' + arrSearchOccupancy[i].RoomCount + '" class="TotalRoom_' + i + '" />'
            Html += '<input type="hidden" value="' + arrSearchOccupancy[i].AdultCount + '" class="AdultCount' + i + '" />'
            Html += '<input type="hidden" value="' + arrSearchOccupancy[i].ChildCount + '" class="ChildCount' + i + '" />'
            Html += '<input type="hidden" value="' + arrSearchOccupancy[i].ChildAges + '" class="ChildAges' + i + '" />'
            Html += '<input type="hidden" value="' + arrGroup.Name + '" class="Supplier' + i + '" />'
            Html += '<input type="hidden" value="' + arrGroup.AvailToken + '" class="HotelCode' + i + '" />'
            Html += '</th></tr>'
            Html += '</thead>'
            Html += '<tbody>'
            var arrRooms = arrSearchOccupancy[i].Rooms;
            Html += '';
            for (var r = 0; r < arrRooms.length; r++) {
                Html += '<tr >'
                Html += '<td style="cursor:pointer">' + arrRooms[r].RoomTypeName
                Html += '<input type="hidden" value="' + arrRooms[r].RoomTypeId + '" class="RoomTypeId' + r + '" />'
                Html += '<input type="hidden" value="' + arrRooms[r].RoomDescription + '" class="RoomDescription' + r + '" />'
                Html += '<input type="hidden" value="' + arrRooms[r].Total + '" class="TotalRate' + r + '" />'
                Html += '</td>'
                Html += '<td style="cursor:pointer">' + arrRooms[r].RoomDescription + '</td>'
                Html += '<td style="cursor:pointer"><i class="' + GetCurrencyIcon(arrRooms[r].Dates[0].Currency) + '"> <i/> ' + numberWithCommas(roundToTwo(arrRooms[r].Total).toString()) + '</td>'
                Html += '<td style="cursor:pointer"><ul class="bullet-list">'
                for (var c = 0; c < arrRooms[r].ListCancel.length; c++) {
                    Html += '<li >' + arrRooms[r].ListCancel[c] + '</li>';
                }
                Html += '</ul></td>'
                Html += '<td>'
                Html += '<input type="radio" name="chk_' + i + '' + ID + '" ' + DefaultSelected + ' class="Room_' + i + '" />'
                DefaultSelected = "";
                Html += '</td>'
                Html += '</tr>'
            }
            Html += '</tbody>'
            Html += '</table>'
            $("#" + ID).append(Html);
            GenrateTable(i+'_'+ID)
        }
        Html = ''
        Html += '<p class="SearchBtn"><input type="button" class="button anthracite-gradient buttonmrgTop" onclick="BookingRates(\'' + arrGroup.AvailToken + '\',\'' + ID + '\');" value="Book"></p>'
        $("#" + ID).append(Html);
    }
    catch (ex) {

    }

}
function ShowSearch() {
    $("#SPN_HotelName").text("Search Hotel");
    $("#div_Search").show(1000);
    $("#btn_mdfSearch").hide(1000);
    $("#filter").hide(2000);
}
function ShowFilter() {
    $("#div_Search").hide(1000);
    $("#btn_mdfSearch").show(1000);
    $("#filter").toggle(2000);
}

var arrRates = new Array();
var arrHotel = new Array();
function GetBookingDetails(session) {
    $.ajax({
        type: "POST",
        url: "Handler/BookingHandler.asmx/GetBookingDetails",
        data: JSON.stringify({ Serach: session }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrRates = result.ListRates;
                arrCharges = result.Charge;
                arrHotel = result.arrHotel;
                GenratePax();
                GrnrateBreakup();
                GenrateHotel();
            }
        }
    })
}
function roundToTwo(value) {
    return (Math.round(value * 100) / 100);
} // Genrate Design
function GenratePax()
{
    var html = '';
    try
    {
        for (var i = 0; i < arrRates.length; i++) {

            html += '<label>Room No.  ' + parseInt(i + 1) + '</label>'
            /* Genrate Adults Details*/
            for (var a = 0; a < arrRates[i].AdultCount; a++) {
                html += '<div class="new-row columns ' + arrRates[i].RoomTypeId + '' + arrRates[i].RoomDescription + '">'
                html += ''
                html += '<div class="three-columns twelve-columns-mobile">'
                html += '<select id="sel_Gender' + a + '_' + arrRates[i].RoomTypeId + '_' + arrRates[i].RoomDescription + '_' + parseInt(i + 1) + '" class="select full-width">'
                html += '<option value="Mr">Mr</option>'
                html += '<option value="Mrs">Mrs</option>'
                html += '</select>'
                html += '</div>'
                html += '<div class="four-columns twelve-columns-mobile">'
                html += ''
                html += '<input type="text" class="input full-width" placeholder="First Name" value="" id="txtFirstName' + a + '_' + arrRates[i].RoomTypeId + '_' + arrRates[i].RoomDescription + '_' + parseInt(i+1) + '">'
                html += '</div>'
                html += '<div class="four-columns twelve-columns-mobile">'
                html += ''
                html += '<input type="text" class="input full-width" placeholder="Last Name" value="" id="txtLastName' + a + '_' + arrRates[i].RoomTypeId + '_' + arrRates[i].RoomDescription + '_' + parseInt(i+1) + '">'
                html += '</div>'
                html += '</div>'
            }


            /* Genrate Child Details*/
            var sAges = arrRates[i].ChildAges.split(',');
            for (var c = 0; c < arrRates[i].ChildCount; c++) {
                html += '<div class="new-row columns">'
                html += ''
                html += '<div class="three-columns twelve-columns-mobile">'
                html += ''
                html += '<select id="sel_childGender' + c + '_' + arrRates[i].RoomTypeId + '_' + arrRates[i].RoomDescription + '_' + parseInt(i+1) + '" class="select  full-width">'
                html += '<option value="Master">Master</option>'
                html += '<option value="Miss">Miss</option>'
                html += '</select>'
                html += '</div>'
                html += '<div class="four-columns twelve-columns-mobile">'
                html += ''
                html += '<input type="text" class="input full-width FirstNC" placeholder="First Name" value="" id="txtCHFirstName' + c + '_' + arrRates[i].RateTypeID + '_' + arrRates[i].RoomDescription + '_' + parseInt(i + 1) + '">'
                html += '</div>'
                html += '<div class="four-columns twelve-columns-mobile">'
                html += ''
                html += '<input type="text" class="input full-width Age" placeholder="Child Age" value="' + sAges[c] + '" id="txtAge' + c + '_' + arrRates[i].RateTypeID + '_' + arrRates[i].RoomDescription + '_' + parseInt(i + 1) + '">'
                html += '</div>'
                html += ''
                html += ''
                html += ''
                html += '</div>'
            }
            html += '<div class="new-row columns">'
            html += '<div class="eleven-columns twelve-columns-mobile">'
            html += '<fieldset class="fieldset">'
            html += '<legend class="legend">Cancellation Policy</legend><ul class="bullet-list">'
            for (var j = 0; j < arrRates[i].ListCancel.length; j++) {
                html += '<li >' + arrRates[i].ListCancel[j] + '</li>';
            }
            html += '</ul></fieldset>'
            html += '</div>'
            html += '</div>'

        }
        html += '<div class="columns with-padding"><label>Remark</label>: <textarea class="input full-width" /></div>'
        html += '<div style="padding:50px  !important">'
        html += '<p class="SearchBtn "><input type="button" class="button anthracite-gradient buttonmrgTop" onclick="CheckAvailCredit();" value="Book"> '
        html += '<input type="button" class="button anthracite-gradient buttonmrgTop" onclick="window.location.href=\''+'searchhotel.aspx'+'\'" value="Cancel"> ' 
        html += '</p> </div>'
        $("#div_Rooms").append(html);
    }
    catch(ex)
    {

    }
}
function GenrateHotel() {
    try {
        var html = '';
        html += '<h1 class="green underline">' + arrHotel.HotelName + ' <span class="icon-map float-righ" style="width:30px;cursor:pointer;float: right;"></span>'
        html += '<a data-footer="Google Map" data-title="' + arrHotel.HotelName + '" data-gallery="multiimages" data-toggle="lightbox" style="">'
        html += '</a><small class="">' + arrHotel.Address + '</small></h1> <br/>'
        $("#main-title").append(html);
    } catch (e) {
        AlertDanger(e)
    }

}


function GrnrateBreakup() {
    var html = '';
    try {
        for (var i = 0; i < arrRates.length; i++) {
            html += '<div class="new-row columns">'
            html += '<div class="twele-columns twelve-columns-mobile">'
            html += '<fieldset class="wrapped button-height">'
            html += '<legend class="legend">Room ' + parseInt(i + 1) + '</legend>'
            html += '<label class="thin">' + arrRates[i].RoomTypeName + ' (' + arrRates[i].RoomDescription + ')</label>'
            html += '<label style="float: right;"><u></u>'
            html += arrRates[i].AdultCount + ' Adults '
            if (arrRates[i].ChildCount != 0) {
                html += arrRates[i].ChildCount + " Child (" + arrRates[i].ChildAges + ")";
            }

            html += '</label><hr/>'
        
           
            html += '<label class="thin">Rate Breakup </label> <span class="info-spot"> '
            html += ' <span class="icon-chevron-down"></span>'
            html += '<span class="info-bubble" style="width: 150px;">'
            for (var t = 0; t < arrRates[i].Dates.length; t++) {
                html += '<small class="orange"><b>' + arrRates[i].Dates[t].datetime + '</b>:</small> <small class="white"><i class="' + GetCurrencyIcon(arrRates[i].Dates[t].Currency) + '" />' + numberWithCommas(arrRates[i].Dates[t].Total.toString()) + '</small><br/>'
            }
            html += '</span>'
            html += '</span>'
            html += '<label class="thin" style="float: right;">'
            html += 'Room ' + parseInt(i + 1) + ' Total'
            html += '<br/><strong ><i class="' + GetCurrencyIcon(arrRates[i].Dates[0].Currency) + '" />' + numberWithCommas(roundToTwo(arrRates[i].Total)) + '</strong>'
            html += '</label>'
                
            html += '</fieldset>'
            html += '</div>'
            html += '</div>'
        }

        html += '<div class="new-row columns">'
        html += '<div class="twele-columns twelve-columns-mobile">'
        html += '<fieldset class="wrapped button-height">'
        html += '<legend class="legend">Bill Summary</legend>'
        html += '<p><label class="thin">Room Total </label>'
        html += '<label class="thin" style="float: right;">'
        html += '<strong ><i class="' + GetCurrencyIcon(arrRates[0].Dates[0].Currency) + '" />' + numberWithCommas(roundToTwo(arrCharges.RoomRate)) + '</strong>'
        html += '</label></p><hr/>'
        for (var i = 0; i < arrCharges.OtherRates.length; i++) {
           
            html += '<p><label class="thin">' + arrCharges.OtherRates[i].RateName + ' </label>'
            //html += ' <span class="info-spot"> <span class="icon-chevron-down"></span>'
            //html += '<span class="info-bubble" style="width: 150px;">'
            //for (var t = 0; t < arrCharges.OtherRates[i].TaxOn.length; t++) {
            //    html += '<small class="orange"><b>' + arrCharges.OtherRates[i].TaxOn[t].TaxName + '</b>:</small> <small class="white"><i class="' + GetCurrencyIcon(arrRates[0].Dates[0].Currency) + '" />' + numberWithCommas(arrCharges.OtherRates[i].TaxOn[t].TaxRate.toString()) + '</small>'
            //}
            //html += '</span>'
            //html += '</span>'
            html += '<label class="thin" style="float: right;">'
            html += '<strong ><i class="' + GetCurrencyIcon(arrRates[0].Dates[0].Currency) + '" />' + numberWithCommas(roundToTwo(arrCharges.OtherRates[i].TotalRate)) + '</strong>'
            html += '</label></p><hr/>'
         
        }
        html += '<p><label class="thin">Total Payable </label>'
        html += '<label class="thin" style="float: right;">'
        html += '<strong ><i class="' + GetCurrencyIcon(arrRates[0].Dates[0].Currency) + '" />' + numberWithCommas(roundToTwo(arrCharges.TotalPrice)) + '</strong>'
        html += '</label></p>'
        html += '</fieldset>'
        html += '</div>'
        html += '</div>'
        $("#div_Charges").append(html);
    }
    catch(ex)
    {

    }
}

function numberWithCommas(x) {
    x = x.toString();
    // get stuff before the dot
    var d = x.indexOf('.');
    var s2 = d === -1 ? x : x.slice(0, d);

    // insert commas every 3 digits from the right
    for (var i = s2.length - 3; i > 0; i -= 3)
        s2 = s2.slice(0, i) + ',' + s2.slice(i);

    // append fractional part
    if (d !== -1)
        s2 += x.slice(d);
    return s2;

}

function GetCurrencyIcon(Currency) {
    CurrencyClass = "";
    // switch (Currency) {
    if (Currency == "AED")
        CurrencyClass = "Currency-AED";
    //  break;
    if (Currency == "INR")
        CurrencyClass = "fa fa-inr";
    // break;
    if (Currency == "USD")
        CurrencyClass = "fa fa-dollar";
    // break;
    if (Currency == "SAR")
        CurrencyClass = "Currency-SAR";
    //break;
    if (Currency == "EUR")
        CurrencyClass = "fa fa-eur";
    // break;
    if (Currency == "GBP")
        CurrencyClass = "fa fa-gbp";
    //break;    
    // }
    return CurrencyClass;
}

function CheckAvailCredit() {
    if (ValidateField()) {
        var arrAddOns = new Array();
        var Search = getParameterByName('data')
        $.ajax({
            type: "POST",
            url: "Handler/BookingHandler.asmx/ValidateTransaction",
            data: JSON.stringify({ arrAdons: arrAddOns, Serach: Search }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    BookHotel()
                }
                else {
                    Success(result.ErrorMsg);
                    return false;
                }
            }
        })
    }
}
var arrLisCustumer = new Array();
function BookHotel() {
    var Search = getParameterByName('data')
    var arrAddOns = new Array();
        $.ajax({
            type: "POST",
            url: "Handler/BookingHandler.asmx/BookHotel",
            data: JSON.stringify({ arrAddOns: arrAddOns, Serach: Search, LisCustumer: arrLisCustumer }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    window.location.href = "BookingList.aspx"
                }
                else {
                    Success(result.ErrorMsg);
                    return false;
                }
            }
        })
}  // Booking

function ValidateField() {
    var bValid = true;
    arrLisCustumer = new Array();
    for (var r = 0; r < arrRates.length; r++) {
        var RateTypeID = arrRates[r].RoomTypeId;
        var MealID = arrRates[r].RoomDescription;
        AdultCount = arrRates[r].AdultCount;
        ChildCount = arrRates[r].ChildCount;
        for (var ad = 0; ad < AdultCount; ad++) {
            if (ad == 0) {
                if ($("#txtFirstName" + ad + "_" + RateTypeID + "_" + MealID + "_" + (r + 1)).val() == "") {
                    Success("Enter Adults First Name Of Room No" + (r + 1) + "");
                    return false;

                }
                if ($("#txtLastName" + ad + "_" + RateTypeID + "_" + MealID + "_" + (r + 1)).val() == "") {
                    Success("Enter Adults Last Name Of Room No" + (r + 1) + "");
                    return false;
                }
            }

            var arrCustumer = {
                Age: 30,
                type: "AD",
                LastName: $("#txtLastName" + ad + "_" + RateTypeID + "_" + MealID + "_" + (r + 1)).val(),
                Name: $("#txtFirstName" + ad + "_" + RateTypeID + "_" + MealID + "_" + (r + 1)).val(),
                Title: $("#sel_Gender" + ad + "_" + RateTypeID + "_" + MealID + "_" + (r + 1) + " option:selected").val(),
                RoomNo: r + 1,
            }
            arrLisCustumer.push(arrCustumer);
        }
        for (var cd = 0; cd < ChildCount; cd++) {
            //if ($("#txtCHFirstName" + cd + "_" + RateTypeID + "_" + MealID + "_" + (0 + 1)).val() == "") {
            //   // Success("Enter Childs Name");
            //  //  return false;
            //}
            //if ($("#txtAge" + cd + "_" + RateTypeID + "_" + MealID + "_" + (0 + 1)).val() == "") {
            //   // Success("Select Childs Age");
            //   // return false;
            //}
            var arrCustumer = {
                Age: $("#txtAge" + cd + "_" + RateTypeID + "_" + MealID + "_" + (r + 1)).val(),
                type: "CH",
                Name: $("#txtCHFirstName" + cd + "_" + RateTypeID + "_" + MealID + "_" + (r + 1)).val(),
                Title: $("#sel_childGender" + cd + "_" + RateTypeID + "_" + MealID + "_" + (r + 1) + " option:selected").val(),
                RoomNo: r + 1,
            }
            arrLisCustumer.push(arrCustumer);
        }

    }
    return bValid;
} // Booking 

function GetHotelInfo() {
    var HotelCode = $('#listHotels option').filter(function () { return this.value == HotelName; }).data('id');
    $.ajax({
        type: "POST",
        url: "../Agent/GenralHandler.asmx/GetHotelInfoBySearch",
        data: JSON.stringify({ Search: session }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var arrHotel = result.arrHotel;
                var html = ''
                var Title = arrHotel.HotelName + ', ' + arrHotel.Address;
                if (arrHotel.Category != 0)
                    Title += ' <img src="../img/' + arrHotel.Category + '.png" style="float: right;">';
                html += '<div class="columns">'
                html += '<div id="div_Images" class="new-row four-columns   twelve-columns-mobile"> </div>'
                html += '<div class="eight-columns   twelve-columns-mobile">'
                if (arrHotel.Description != null)
                {
                    html += '<fieldset class="fieldset fields-list"><legend class="legend">About:</legend><p style="text-align: justify;">'
                    if (arrHotel.Description != null)
                        html += arrHotel.Description
                    html += ' <i class="icon-quote icon-size2"></i></p></fieldset>'
                }
               
                }

            html += '</div>'
            html += '<div class="new-row twelve-columns   twelve-columns-mobile">'
            if (arrHotel.Facility.length != 0) {
                html += '<fieldset class="fieldset fields-list"><legend class="legend">Hotel Facility:</legend>'
                html += '<div class="columns twelve-columns-mobile">'
                for (i = 0; i < arrHotel.Facility.length; i = i +3) {
                    html += FacilityDetails(arrHotel.Facility[i])
                    if ((i + 1) < arrHotel.Facility.length)
                        html += FacilityDetails(arrHotel.Facility[i + 1])
                    if ((i + 2) < arrHotel.Facility.length)
                        html += FacilityDetails(arrHotel.Facility[i + 2])
                }
                html += '</div>'
                html += '</fieldset>'
            html +='</div>'
                html+='</div>'
                $.modal({
                    content: html,
                    title: Title,
                    width: 1000,
                    height:400,
                    scrolling: true,
                });
                $("#div_Images").append(GetImages(arrHotel.Image))
            }
            else {
                Success(result.ErrorMsg);
                return false;
            }
        }
    })
}

function GenrateTable(ID) {
    // Call template init (optional, but faster if called manually)
    $.template.init();
    // Table sort - styled
    $('#tbl_Occupancy_'+ ID).tablesorter({
        headers: {
            0: { sorter: false },
            6: { sorter: false }
        }
    }).on('click', 'tbody td', function (event) {
        var drophotelId = this.id;

        // Do not process if something else has been clicked
        if (event.target !== this) {
            return;
        }

        var tr = $(this).parent(),
            row = tr.next('.row-drop'),
            rows;

        // If click on a special row
        if (tr.hasClass('row-drop')) {
            return;
        }

        // If there is already a special row
        if (row.length > 0) {

            // Un-style row
            tr.children().removeClass('anthracite-gradient glossy');

            // Remove row
            row.remove();

            return;
        }

        // Remove existing special rows
        rows = tr.siblings('.row-drop');
        if (rows.length > 0) {
            // Un-style previous rows
            rows.prev().children().removeClass('anthracite-gradient glossy');

            // Remove rows
            rows.remove();
        }

        // Style row
        tr.children().addClass('anthracite-gradient glossy');
        var RoomNo = tr[0].offsetParent.id.split('_')[2];
        var ndTable = tr[0].offsetParent.id;
        var Supplier = $($("#" + ndTable).find(".Supplier" + parseInt(RoomNo))[0]).val();
        var HotelCode = $($("#" + ndTable).find(".HotelCode" + parseInt(RoomNo))[0]).val();
        var chkRoom = $(tr).find(".Room_" + RoomNo)[0];
        $(chkRoom).click();
        var ndRoom = $("#"+ndTable).find(".Room_" + RoomNo);
        var RoomTypeID = "", RoomDescriptionId = "", Total = "0.00";
        for (var r = 0; r < ndRoom.length; r++) {
            if (ndRoom[r].checked) {
                RoomTypeID = $($("#" + ndTable).find(".RoomTypeId" + r)).val();
                RoomDescriptionId = $($("#" + ndTable).find(".RoomDescription" + r)).val();
                Total = $($("#" + ndTable).find(".TotalRate" + r)).val()
                break;
            }
        }
        // Add fake row
        $('<tr  class="row-drop">' +
            '<td  colspan="' + tr.children().length + '">' +
              '<div class="columns" style="overflow:scroll height:300px;margin-left:auto;margin-right:auto;">' +
                '<div   id="div_Dates' + RoomNo + '"   class="twelve-columns  twelve-columns-mobile"> </div>' +
              '</div>' +
            '</td>' +
        '</tr>').insertAfter(tr);
        GetAvailbility(drophotelId, RoomTypeID, RoomDescriptionId, RoomNo, HotelCode, Supplier)
    }).on('sortStart', function () {
        var rows = $(this).find('.row-drop');
        if (rows.length > 0) {
            // Un-style previous rows
            rows.prev().children().removeClass('anthracite-gradient glossy');

            // Remove rows
            rows.remove();
        }
    });
}
function GetAvailbility(id, RoomTypeID, RoomDescriptionId, RoomNo, HotelCode, Supplier) {
    $.ajax({
        type: "POST",
        url: "Handler/HotelHandler.asmx/GetAvailibility",
        data: JSON.stringify({ Serach: GetQueryStringParams("data").replace(/%20/g, ' '), RoomID: RoomTypeID, RoomDescID: RoomDescriptionId, RoomNo: parseInt(RoomNo) + 1, HotelCode: HotelCode, Supplier: Supplier }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var arrDates = result.arrDates;
                var arrImage = result.arrImage;
                var html = "";
                html += '<div class="new-row coloumns margin-top">'
                for (i = 0; i < arrDates.length; i = i + 4) {
                    html += DateDetails(arrDates[i])
                    if ((i + 1) < arrDates.length)
                        html += DateDetails(arrDates[i + 1])
                    if ((i + 2) < arrDates.length)
                        html += DateDetails(arrDates[i + 2])
                    if ((i + 3) < arrDates.length)
                        html += DateDetails(arrDates[i + 3])
                }
                html += '</div>'
                html += '<div class="new-row coloumns" style="text-align: right">'
                html += '<div class="new-row coloumns"><div class="three-row"><b>Note:</b>   No Inventory  <label style="'
                html += '"><span class="wrapped red-gradient " style="margin-bottom: -10px;"></span></label>  Low  Inventory <label style="'
                html += 'margin-bottom: -22px;'
                html += '"><span class="margin-bottom wrapped orange-gradient with-mid-padding align-center" style="'
                html += ' margin-bottom: -10px;'
                html += '"></span></label>High  Inventory <label style="'
                html += 'margin-bottom: -22px;'
                html += '"><span class="margin-bottom wrapped green-gradient with-mid-padding align-center"></span></label></div> </div>'
                html += '</div>'
             
                $('#div_Dates' + RoomNo).append(html);
                if (arrImage.length != 0)
                    $("#DropRight" + RoomNo).append(GetImages(arrImage));
            }
        }
    });
} // Get Room Details 

function DateDetails(arrDate) {
    var html = "";
    var Avail = "red";
    if (arrDate.NoOfCount >= 1 && arrDate.NoOfCount <= 100)
        Avail = "orange"
    else if (arrDate.NoOfCount == 0)
        Avail = "red"
    else
        Avail = "green"
    html += '<label  class="two-columns margin-bottom wrapped ' + Avail + '-gradient with-mid-padding align-center  twelve-columns-mobile"><n>' + arrDate.datetime + ' <br/>'
    html += '<i class="' + GetCurrencyIcon(arrDate.Currency) + '"> </i>' + arrDate.Total
    html += '</n></label>'
    return html;
}

function FacilityDetails(arrFacility) {
    var html = "";
    if (arrFacility.length > 15)
        arrFacility= arrFacility.substr(0, 15) +".."
    html += '<div class="three-columns twelve-columns-mobile"><a href="javascript:void(0)" class="button"><span class="button-icon green-gradient" ><span class="icon-tick"></span></span> '
    html += arrFacility
	html += '</a></div>'
    return html;
}



function GetImages(arrImage,i) {
    var html='';
    html += '    <div class="Imgslider">'
    html += '  <div class="slide_viewer">'
    html += '    <div class="slide_group">'
    for (var i = 0; i < arrImage.length; i++) {
        html += '      <div class="slide">'
        html += '<img class="mySlides" src="' + arrImage[i].Url + '" style="width:100%; height:100%" onerror="ErroImage(this)"/>'
        html += '      </div>'
        }
    html += '    </div>'
    html += '  </div>'
    html += '</div><!-- End // .Imgslider -->'
    html += ''
    html += '<div class="slide_buttons">'
    html += '</div>'
    html += ''
    html += '<div class="directional_nav">'
    html += '  <div class="previous_btn" title="Previous"  style="position: absolute;top: 50%;left: 0%;transform: translate(0%,-50%);>'
    html += '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25px" height="25px" viewBox="-11 -11.5 65 66">'
    html += '<g>'
    html += '<g>'
    html += '<path fill="#474544" d="M-10.5,22.118C-10.5,4.132,4.133-10.5,22.118-10.5S54.736,4.132,54.736,22.118'
    html += 'c0,17.985-14.633,32.618-32.618,32.618S-10.5,40.103-10.5,22.118z M-8.288,22.118c0,16.766,13.639,30.406,30.406,30.406 c16.765,0,30.405-13.641,30.405-30.406c0-16.766-13.641-30.406-30.405-30.406C5.35-8.288-8.288,5.352-8.288,22.118z"/>'
    html += '<path fill="#474544" d="M25.43,33.243L14.628,22.429c-0.433-0.432-0.433-1.132,0-1.564L25.43,10.051c0.432-0.432,1.132-0.432,1.563,0	c0.431,0.431,0.431,1.132,0,1.564L16.972,21.647l10.021,10.035c0.432,0.433,0.432,1.134,0,1.564	c-0.215,0.218-0.498,0.323-0.78,0.323C25.929,33.569,25.646,33.464,25.43,33.243z"/>'
    html += '</g>'
    html += '</g>'
    html += '</svg>'
    html += '</div>'
    html += '<div class="next_btn" title="Next" style="position: absolute;top: 50%;left: 90%;transform: translate(0%,-50%);">'
    html += '  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25px" height="25px" viewBox="-11 -11.5 65 66">'
    html += '    <g>'
    html += '      <g>'
    html += '        <path fill="#474544" d="M22.118,54.736C4.132,54.736-10.5,40.103-10.5,22.118C-10.5,4.132,4.132-10.5,22.118-10.5	c17.985,0,32.618,14.632,32.618,32.618C54.736,40.103,40.103,54.736,22.118,54.736z M22.118-8.288	c-16.765,0-30.406,13.64-30.406,30.406c0,16.766,13.641,30.406,30.406,30.406c16.768,0,30.406-13.641,30.406-30.406 C52.524,5.352,38.885-8.288,22.118-8.288z"/>'
    html += '        <path fill="#474544" d="M18.022,33.569c 0.282,0-0.566-0.105-0.781-0.323c-0.432-0.431-0.432-1.132,0-1.564l10.022-10.035 			L17.241,11.615c 0.431-0.432-0.431-1.133,0-1.564c0.432-0.432,1.132-0.432,1.564,0l10.803,10.814c0.433,0.432,0.433,1.132,0,1.564 L18.805,33.243C18.59,33.464,18.306,33.569,18.022,33.569z"/>'
    html += '      </g>'
    html += '    </g>'
    html += '  </svg>'
    html += '</div>'
    html += '</div><!-- End // .directional_nav -->'
    return html;
}

function ErroImage(Image) {
    Image.src = "../img/noImage.png"
    Image.onerror = "";
}

