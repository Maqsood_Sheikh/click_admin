﻿$(document).ready(function () {
    $.ajax({
        url: "../Handler/DefaultHandler.asmx/CheckSession",
        type: "post",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode != 1) {
                $(window).unbind();
                window.location.href = "../Login.aspx";
                // window.location.href = "../Dashboard.aspx";
                //                window.location.href = "../CUT/Dashboard.aspx";
            }
        },
       
    });
});
function btnLogout_Click() {
    $.ajax({
        url: "../Handler/DefaultHandler.asmx/Logout",
        type: "post",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                $(window).unbind();
                window.location.href = "../Login.aspx";
                // window.location.href = "../Dashboard.aspx";
                //                window.location.href = "../CUT/Dashboard.aspx";
            }
        },
        error: function () {
            Success('Error occured while logging out!');
        }
    });
}