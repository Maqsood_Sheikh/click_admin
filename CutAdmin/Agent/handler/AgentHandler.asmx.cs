﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CutAdmin.Agent.handler
{
    /// <summary>
    /// Summary description for AgentHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class AgentHandler : System.Web.Services.WebService
    {

          string json = "";
        CutAdmin.handler.AgentHandler obj = new CutAdmin.handler.AgentHandler();
        [WebMethod(EnableSession = true)]
        public string GetAgentInfo()
        {
            return json = obj.GetAgentInfo();
        }
        [WebMethod(EnableSession = true)]
        public string LoadAllBanks()
        {
            return json = obj.LoadAllBanks();
        }
        [WebMethod(EnableSession = true)]
        public string InsertDeposit(string BankName, string AccountNumber, string AmountDeposit, string Remarks, string TypeOfDeposit, string TypeOfCash, string TypeOfCheque, string EmployeeName, string ReceiptNumber, string ChequeDDNumber, string ChequeDrawn, string DepositDate)
        { 
         return json=obj.InsertDeposit( BankName,  AccountNumber,  AmountDeposit,  Remarks,  TypeOfDeposit,  TypeOfCash,  TypeOfCheque,  EmployeeName,  ReceiptNumber,  ChequeDDNumber,  ChequeDrawn,  DepositDate);
        }
    }
}
