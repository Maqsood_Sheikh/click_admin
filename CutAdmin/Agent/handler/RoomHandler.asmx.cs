﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CutAdmin.Agent.handler
{
    /// <summary>
    /// Summary description for RoomHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class RoomHandler : System.Web.Services.WebService
    {
        CutAdmin.RoomHandler obj = new CutAdmin.RoomHandler();

        public string GetRooms(Int64 sHotelId)
        {
            string json = "";
            return json = obj.GetRooms(sHotelId);
        }

        public string getRoomwithId(Int64 RoomId, Int64 HotelCode)
        {
            string json = "";
            return json = obj.getRoomwithId(RoomId, HotelCode);
        }

        public string AddRates(Int64 HotelCode, Int64 Supplier, string Country, string Currency, string MealPlan, List<string> CheckinR, List<string> CheckoutR, string RateInclude, string RateExclude, string MinStay, string MaxStay, string TaxIncluded, string TaxType, string TaxOn, string TaxValue, string TaxDetails, List<string> RoomTypeID, List<string> RateTypeCode, List<string> RoomRates, List<string> ExtraBeds, List<string> CWB, List<string> CWN, List<string> BookingCode, List<string> CancelPolicy, List<string> Offers, List<string> RateNote, List<string> RateType, List<string> Daywise, List<string> Sunday, List<string> Monday, List<string> Tuesday, List<string> Wednesday, List<string> Thursday, List<string> Friday, List<string> Saturday, List<List<CutAdmin.BL.Comm_AddOnsRate>> ListAddOnsRates, List<string> OfferNight)
        {
            string json = "";
            return json = obj.AddRates(HotelCode, Supplier, Country, Currency, MealPlan, CheckinR, CheckoutR, RateInclude, RateExclude, MinStay, MaxStay, TaxIncluded, TaxType, TaxOn, TaxValue, TaxDetails, RoomTypeID, RateTypeCode, RoomRates, ExtraBeds, CWB, CWN, BookingCode, CancelPolicy, Offers, RateNote, RateType, Daywise, Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, ListAddOnsRates, OfferNight);
        }

        public string UpdateRates(Int64 HotelCode, Int64 Supplier, string Country, string Currency, string MealPlan, string CheckinR, string CheckoutR, List<string> RoomTypeID, List<string> RateTypeCode, List<string> RoomRates, List<string> ExtraBeds, List<string> CWB, List<string> CWN, List<string> BookingCode, List<string> CancelPolicy, List<string> Offers, List<string> RateNote, List<string> Daywise, List<string> Sunday, List<string> Monday, List<string> Tuesday, List<string> Wednesday, List<string> Thursday, List<string> Friday, List<string> Saturday, List<string> TypeRate, List<List<CutAdmin.BL.Comm_AddOnsRate>> ListAddOnsRates, List<string> OfferNight)
        {
            string json = "";
            return json = obj.UpdateRates(HotelCode, Supplier, Country, Currency, MealPlan, CheckinR, CheckoutR, RoomTypeID, RateTypeCode, RoomRates, ExtraBeds, CWB, CWN, BookingCode, CancelPolicy, Offers, RateNote, Daywise, Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, TypeRate, ListAddOnsRates, OfferNight);
        }

        public void AddPrices(string RoomTypeID, Int64 HotelCode, Int64 Supplier, string Country, string Currency, string MealPlan, string RateTypeCode, string CheckinR, string CheckoutR, string RateInclude, string RateExclude, string MinStay, string MaxStay, string TaxIncluded, string TaxType, string TaxOn, string TaxValue, string TaxDetails, string BookingCode, string CancelPolicy, string Offers, string RateNote, string RateType, string Daywise, string Sunday, string Monday, string Tuesday, string Wednesday, string Thursday, string Friday, string Saturday, string RoomRates, string ExtraBeds, string CWB, string CWN, string OfferNight)
        {
            //string json = "";
            obj.AddPrices(RoomTypeID, HotelCode, Supplier, Country, Currency, MealPlan, RateTypeCode, CheckinR, CheckoutR, RateInclude, RateExclude, MinStay, MaxStay, TaxIncluded, TaxType, TaxOn, TaxValue, TaxDetails, BookingCode, CancelPolicy, Offers, RateNote, RateType, Daywise, Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, RoomRates, ExtraBeds, CWB, CWN, OfferNight);
        }

        public class RatePrice
        {
            public string Date { get; set; }
            public string Daywise { get; set; }
            public string RRRate { get; set; }
            public string EBRate { get; set; }
            public string CWBRate { get; set; }
            public string CNBRate { get; set; }
            public string MonRate { get; set; }
            public string TueRate { get; set; }
            public string WedRate { get; set; }
            public string ThuRate { get; set; }
            public string FriRate { get; set; }
            public string SatRate { get; set; }
            public string SunRate { get; set; }
            public string CancelationPolicy { get; set; }
            public string CheckinDate { get; set; }
            public string CheckoutDate { get; set; }
        }
        public class Rates
        {
            public string RateType { get; set; }
            public string RoomId { get; set; }
            public string RoomName { get; set; }
            public List<RatePrice> PriceList { get; set; }
        }
        public class RateList
        {
            public string RoomId { get; set; }
            public string HotelRateID { get; set; }
            public string ValidNationality { get; set; }
            public string Checkin { get; set; }
            public string Checkout { get; set; }
            public string CurrencyCode { get; set; }
            public string RR { get; set; }
            public string RateType { get; set; }
            public string MealPlan { get; set; }
            public string Supplier { get; set; }

            public string DayWise { get; set; }
            public string MonRR { get; set; }
            public string TueRR { get; set; }
            public string WedRR { get; set; }
            public string ThuRR { get; set; }
            public string FriRR { get; set; }
            public string SatRR { get; set; }
            public string SunRR { get; set; }
        }

        public string GetRates(Int64[] HotelCode, string Destination, string Checkin, string Checkout, string[] nationality, int Nights, int Adults, int Childs, string Supplier, string MealPlan, string CurrencyCode, string AddSearchsession, string SearchValid)
        {
            string json = "";
            return json = obj.GetRates(HotelCode, Destination, Checkin, Checkout, nationality, Nights, Adults, Childs, Supplier, MealPlan, CurrencyCode, AddSearchsession, SearchValid);
        }

        public static IEnumerable<Tuple<DateTime, DateTime>> SplitDateRange(DateTime start, DateTime end, int dayChunkSize)
        {
            DateTime chunkEnd;
            while ((chunkEnd = start.AddDays(dayChunkSize)) < end)
            {
                yield return Tuple.Create(start, chunkEnd);
                start = chunkEnd;
            }
            yield return Tuple.Create(start, end);
        }

        public string GetBookingRates()
        {
            string json = "";
            return json = obj.GetBookingRates();
        }
        public string GetHotelAddress(Int64 HotelCode)
        {
            string json = "";
            return json = obj.GetHotelAddress(HotelCode);
        }
        public string GetRoomOccupancy(Int64 AdultCount, Int64 ChildCount, int RateTypeID, int MealID, Int64 RateID)
        {
            string json = "";
            return json = obj.GetRoomOccupancy(AdultCount, ChildCount, RateTypeID, MealID, RateID);
        }
        public string UpdateInventory(List<CutAdmin.Common.RatesManager.Supplier> Supplier)
        {
            string json = "";
            return json = obj.UpdateInventory(Supplier);
        }
        public string StartSale(List<CutAdmin.Common.RatesManager.Supplier> Supplier)
        {
            string json = "";
            return json = obj.StartSale(Supplier);
        }
        public string StopSale(List<CutAdmin.Common.RatesManager.Supplier> Supplier)
        {
            string json = "";
            return json = obj.StopSale(Supplier);
        }
        public string CheckFreeSale(List<CutAdmin.Common.RatesManager.Supplier> Supplier)
        {
            string json = "";
            return json = obj.CheckFreeSale(Supplier);
        }
        public string GetRatelist(string HotelCode)
        {
            string json = "";
            return json = obj.GetRatelist(HotelCode);
        }
        public string ChekAvailRates(Int64 HotelCode, Int64 Supplier, string Country, string Currency, string MealPlan, List<string> CheckinR, List<string> CheckoutR)
        {
            string json = "";
            return json = obj.ChekAvailRates(HotelCode, Supplier, Country, Currency, MealPlan, CheckinR, CheckoutR);
        }
        public string GetURate(Int64 RateID)
        {
            string json = "";
            return json = obj.GetURate(RateID);
        }
        public class ListOfTaxes
        {

            public Int64 ID { get; set; }
            public Int64 TaxID { get; set; }
            public Int64 TaxOnID { get; set; }
            public Int64 IsAddOn { get; set; }
            public Int64 HotelID { get; set; }
            public Int64 ServiceID { get; set; }
            public string ServiceName { get; set; }
            public Int64 ParentID { get; set; }
        }
        public string GetOtherRates(Int64 HotelCode, Int64 Supplier, string Country, string Currency, string MealPlan, string CheckinR, string CheckoutR)
        {
            string json = "";
            return json = obj.GetOtherRates(HotelCode, Supplier, Country, Currency, MealPlan, CheckinR, CheckoutR);
        }
        public string FilterRate(List<string> RoomType, List<string> MealType, float MinPrice, float MaxPrice)
        {
            string json = "";
            return json = obj.FilterRate(RoomType, MealType, MinPrice, MaxPrice);
        }
    }
}
