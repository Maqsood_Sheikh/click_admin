﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.Agent.handler
{
    /// <summary>
    /// Summary description for DefaultHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class DefaultHandler : System.Web.Services.WebService
    {

        CutAdmin.DefaultHandler obj = new CutAdmin.DefaultHandler();
        [WebMethod(EnableSession = true)]
        public string UserLogin(string sUserName, string sPassword)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            string json = "";
            try
            {
                json = obj.UserLogin(sUserName, sPassword);
                CutAdmin.DataLayer.GlobalDefault objGlobalDefault = (CutAdmin.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                string Uid = ConfigurationManager.AppSettings["AdminKey"];
                if (objGlobalDefault.ParentId.ToString() != Uid)
                    throw new Exception("Invalid User Name or Password.");
                return json;
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });

            }


        }
        [WebMethod(EnableSession = true)]
        public string Logout()
        {
            string json = "";
            return json = obj.Logout();
        }
        [WebMethod(EnableSession = true)]
        public string CheckSession()
        {
            string json = "";
            return json = obj.CheckSession();
        }
        [WebMethod(true)]
        public string sendPassword(string sEmail)
        {
            string json = "";
            return json = obj.sendPassword(sEmail);
        }
    }
}
