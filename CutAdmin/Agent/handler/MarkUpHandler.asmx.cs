﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CutAdmin.Agent.handler
{
    /// <summary>
    /// Summary description for MarkUpHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
   [System.Web.Script.Services.ScriptService]
    public class MarkUpHandler : System.Web.Services.WebService
    {
        CutAdmin.handler.MarkUpHandler obj = new CutAdmin.handler.MarkUpHandler();
        string json = "";

        [WebMethod(EnableSession = true)]
        public string LoadMarkUp()
        {
            return json = obj.LoadMarkUp();
        }
        [WebMethod(EnableSession = true)]
        public string SaveMarkUp(string Perc, string Amt)
        {
            return json = obj.SaveMarkUp(Perc, Amt);
        }
        [WebMethod(EnableSession = true)]
        public string UpdateMarkUp(string Perc, string Amt)
        {
            return json = obj.UpdateMarkUp(Perc, Amt);
        }
    }
}
