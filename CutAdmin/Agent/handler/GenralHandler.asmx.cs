﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CutAdmin.Agent.handler
{
    /// <summary>
    /// Summary description for GenralHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class GenralHandler : System.Web.Services.WebService
    {
        CutAdmin.GenralHandler obj = new CutAdmin.GenralHandler();
        [WebMethod(EnableSession = true)]
        public string GetHotelActivityMails(string Type)
        {
            string json = "";
            return json = obj.GetHotelActivityMails(Type);
        }
        [WebMethod(EnableSession = true)]
        public string UpdateVisaMails(string Activity, string Type, string MailsId, string CcMails, string BCcMail, string Message)
        {
            string json = "";
            return json = obj.UpdateVisaMails(Activity, Type, MailsId, CcMails, BCcMail, Message);
        }
        [WebMethod(EnableSession = true)]
        public string GetCountry()
        {
            string json = "";
            return json = obj.GetCountry();
        }
        [WebMethod(EnableSession = true)]
        public string GetCity(string country)
        {
            string json = "";
            return json = obj.GetCity(country);
        }
        [WebMethod(EnableSession = true)]
        public string GetCity1(string country)
        {
            string json = "";
            return json = obj.GetCity1(country);
        }
        [WebMethod(EnableSession = true)]
        public string GetCityCode(string Description)
        {
            string json = "";
            return json = obj.GetCityCode(Description);
        }
        [WebMethod(EnableSession = true)]
        public string GetCountryCity(string country)
        {
            string json = "";
            return json = obj.GetCountryCity(country);
        }
        [WebMethod(EnableSession = true)]
        public string GetCurrency()
        {
            string json = "";
            return json = obj.GetCurrency();
        }
        [WebMethod(EnableSession = true)]
        public string GetCityCodeMapping(string Countryname, string CityName)
        {
            string json = "";
            return json = obj.GetCityCodeMapping(Countryname, CityName);
        }
        [WebMethod(EnableSession = true)]
        public string AddMapping(string CityName, string CityCode, string CountryName, string CountryCode, string dotwCityCode, string ExpediaCityCode, string GrnCityCode)
        {
            string json = "";
            return json = obj.AddMapping(CityName, CityCode, CountryName, CountryCode, dotwCityCode, ExpediaCityCode, GrnCityCode);
        }
        [WebMethod(EnableSession = true)]
        public string GetMappedCities()
        {
            string json = "";
            return json = obj.GetMappedCities();
        }
        [WebMethod(EnableSession = true)]
        public string GetSuppliers()
        {
            string json = "";
            return json = obj.GetSuppliers();
        }
        [WebMethod(EnableSession = true)]
        public string GetCancelationPolicies()
        {
            string json = "";
            return json = obj.GetCancelationPolicies();
        }
        [WebMethod(EnableSession = true)]
        public Int64 GetUserID(string UserType)
        {
            Int64 json;
            return json = obj.GetUserID(UserType);
        }
        [WebMethod(EnableSession = true)]
        public string GetALLOffer()
        {
            string json = "";
            return json = obj.GetALLOffer();
        }
        [WebMethod(EnableSession = true)]
        public string GetBankDepositDetail()
        {
            string json = "";
            return json = obj.GetBankDepositDetail();
        }
        [WebMethod(EnableSession = true)]
        public string UserLogin(string sUserName, string sPassword)
        {
            string json = "";
            return json = obj.UserLogin(sUserName, sPassword);
        }
        [WebMethod(EnableSession = true)]
        public string UnApproveDeposit(int uid, Int64 sId)
        {
            string json = "";
            return json = obj.UnApproveDeposit(uid, sId);
        }
        [WebMethod(EnableSession = true)]
        public string ApproveDeposit(int uid, decimal dDepositAmount, string sTypeofCash, string sTypeofcheque, string sComment, string sLastUpdatedDate, Int64 sId)
        {
            string json = "";
            return json = obj.ApproveDeposit(uid, dDepositAmount, sTypeofCash, sTypeofcheque, sComment, sLastUpdatedDate, sId);
        }
        [WebMethod(EnableSession = true)]
        public string AddBankDetails(string BankName, string AccountNo, string Branch, string SwiftCode, string Country)
        {
            string json = "";
            return json = obj.AddBankDetails(BankName, AccountNo, Branch, SwiftCode, Country);
        }
        [WebMethod(EnableSession = true)]
        public string GetHotelInfoBySearch(string Search)
        {
            CutAdmin.handler.GenralHandler obj = new CutAdmin.handler.GenralHandler();
            string json = "";
            return json = obj.GetHotelInfoBySearch(Search);
        }
        [WebMethod(EnableSession = true)]
        public string GetHotelInfo(Int64 HotelCode)
        {
            CutAdmin.handler.GenralHandler obj = new CutAdmin.handler.GenralHandler();
            string json = "";
            return json = obj.GetHotelInfo(HotelCode);
        }
        [WebMethod(EnableSession = true)]
        public string ChangePassword(string sOldPassword, string sNewPassword)
        {
            CutAdmin.HotelAdmin.Handler.GenralHandler obj = new CutAdmin.HotelAdmin.Handler.GenralHandler();
            string json = "";
            return json = obj.ChangePassword(sOldPassword, sNewPassword);
        }
    }
}
