﻿using CutAdmin.BL;
using CutAdmin.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Services;

namespace CutAdmin.Agent.handler
{
    /// <summary>
    /// Summary description for BookingHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class BookingHandler : System.Web.Services.WebService
    {

        string json = ""; string Texts = "";

        List<CutAdmin.BookingHandler.RecordInv> Record = new List<CutAdmin.BookingHandler.RecordInv>();


        CutAdmin.BookingHandler obj = new CutAdmin.BookingHandler();

        [WebMethod(EnableSession = true)]
        public string GetAvailibility(string Serach, string RoomID, string RoomDescID, int RoomNo)
        {
            string json = "";
            return json = obj.GetAvailibility(Serach, RoomID, RoomDescID, RoomNo);
        }

        [WebMethod(EnableSession = true)]
        public string GenrateBookingDetails(string Serach, List<CutAdmin.Common.BoookingManger.Group> ListRates, string HotelCode, string Supplier)
        {
            string json = "";
            return json = CutAdmin.Common.BoookingManger.BlockRoom(Serach, ListRates, HotelCode, Supplier);
        }

        [WebMethod(EnableSession = true)]
        public string GetBookingDetails(string Serach)
        {
            string json = "";
            return json = BoookingManger.GetBookingDetails(Serach);
        }

        [WebMethod(EnableSession = true)]
        public string ValidateTransaction(List<CutAdmin.BookingHandler.Addons> arrAdons, string Serach)
        {
            string json = "";
            return json = obj.ValidateTransaction(arrAdons, Serach);
        }

        [WebMethod(EnableSession = true)]
        public string BookHotel(List<CutAdmin.BookingHandler.Addons> arrAddOns, string Serach, List<CommonLib.Request.Customer> LisCustumer)
        {
            string json = "";
            return json = CutAdmin.Common.BoookingManger.BookHotel(arrAddOns, Serach, LisCustumer);
        }

        [WebMethod(EnableSession = true)]
        public string GroupRequest(Int64 noRoom, Int64 noPax, string HotelCode, string CheckIn, string CheckOut, string PaxName)
        {
            string json = "";
            return json = CutAdmin.Common.BoookingManger.GroupRequest(noRoom, noPax, HotelCode, CheckIn, CheckOut, PaxName);
        }

        [WebMethod(EnableSession = true)]
        public string BookingList()
        {
            string json = "";
            return json = obj.BookingList();
        }

        [WebMethod(EnableSession = true)]
        public string BookingListFilter(string status, string type)
        {
            string json = "";
            return json = obj.BookingListFilter(status, type);
        }

        [WebMethod(EnableSession = true)]
        public string Search(string CheckIn, string CheckOut, string Passenger, string BookingDate, string Reference, string HotelName, string Location, string ReservationStatus)
        {
            string json = "";
            return json = obj.Search(CheckIn, CheckOut, Passenger, BookingDate, Reference, HotelName, Location, ReservationStatus);
        }

        [WebMethod(EnableSession = true)]
        public string BookingCancle(string ReservationID, string Status)
        {
            string json = "";
            return json = obj.BookingCancle(ReservationID, Status);
        }

        [WebMethod(EnableSession = true)]
        public string GetDetail(string ReservationID)
        {
            string json = "";
            return json = obj.GetDetail(ReservationID);
        }

        [WebMethod(EnableSession = true)]
        public string SaveConfirmDetail(string HotelName, string ReservationId, string ConfirmDate, string StaffName, string ReconfirmThrough, string HotelConfirmationNo, string Comment)
        {
            string json = "";
            return json = obj.SaveConfirmDetail(HotelName, ReservationId, ConfirmDate, StaffName, ReconfirmThrough, HotelConfirmationNo, Comment);
        }

        [WebMethod(EnableSession = true)]
        public static bool ReconfirmationMail(Int64 Sid, string HotelName, string ReservationId, string ReconfirmBy, string Comment)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                var sReservation = (from obj in DB.tbl_CommonHotelReservations
                                    from objAgent in DB.tbl_AdminLogins
                                    where obj.ReservationID == ReservationId
                                    select new
                                    {
                                        customer_Email = objAgent.uid,
                                        customer_name = obj.AgencyName,
                                        VoucherNo = obj.VoucherID,

                                    }).FirstOrDefault();

                var sMail = (from obj in DB.tbl_ActivityMails where obj.Activity == "Booking Reconfirm" select obj).FirstOrDefault();
                string BCcTeamMails = sMail.BCcMail;
                string CcTeamMail = sMail.CcMail;


                StringBuilder sb = new StringBuilder();

                sb.Append("<html>");
                sb.Append("<head>");
                sb.Append("<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">");
                sb.Append("<meta name=Generator content=\"Microsoft Word 12 (filtered)\">");
                sb.Append("<style>");
                sb.Append("<!--");
                sb.Append(" /* Font Definitions */");
                sb.Append(" @font-face");
                sb.Append("	{font-family:\"Cambria Math\";");
                sb.Append("	panose-1:2 4 5 3 5 4 6 3 2 4;}");
                sb.Append("@font-face");
                sb.Append("	{font-family:Calibri;");
                sb.Append("	panose-1:2 15 5 2 2 2 4 3 2 4;}");
                sb.Append(" /* Style Definitions */");
                sb.Append(" p.MsoNormal, li.MsoNormal, div.MsoNormal");
                sb.Append("	{margin-top:0cm;");
                sb.Append("	margin-right:0cm;");
                sb.Append("	margin-bottom:10.0pt;");
                sb.Append("	margin-left:0cm;");
                sb.Append("	line-height:115%;");
                sb.Append("	font-size:11.0pt;");
                sb.Append("	font-family:\"Calibri\",\"sans-serif\";}");
                sb.Append("a:link, span.MsoHyperlink");
                sb.Append("	{color:blue;");
                sb.Append("	text-decoration:underline;}");
                sb.Append("a:visited, span.MsoHyperlinkFollowed");
                sb.Append("	{color:purple;");
                sb.Append("	text-decoration:underline;}");
                sb.Append(".MsoPapDefault");
                sb.Append("	{margin-bottom:10.0pt;");
                sb.Append("	line-height:115%;}");
                sb.Append("@page Section1");
                sb.Append("	{size:595.3pt 841.9pt;");
                sb.Append("	margin:72.0pt 72.0pt 72.0pt 72.0pt;}");
                sb.Append("div.Section1");
                sb.Append("	{page:Section1;}");
                sb.Append("-->");
                sb.Append("</style>");
                sb.Append("</head>");
                sb.Append("<body lang=EN-IN link=blue vlink=purple>");
                sb.Append("<div class=Section1>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Dear ");
                sb.Append("" + sReservation.customer_name + ",</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Greetings ");
                sb.Append("from&nbsp;<b>Click<span style='color:#ED7D31'>Ur</span><span style='color:#4472C4'>Trip</span></b></span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Once ");
                sb.Append("again thanks for choosing our service, we believe that smooth check-in will");
                sb.Append("defiantly satisfy your guest, so we take this efforts to reconfirm your booking</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                sb.Append("<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0");
                sb.Append(" style='border-collapse:collapse'>");
                sb.Append(" <tr>");
                sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Transaction");
                sb.Append("  No.</span></p>");
                sb.Append("  </td>");
                sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + sReservation.customer_name + "</span></p>");
                sb.Append("  </td>");
                sb.Append(" </tr>");
                sb.Append(" <tr>");
                sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hotel");
                sb.Append("  Name</span></p>");
                sb.Append("  </td>");
                sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'> " + HotelName + "</span></p>");
                sb.Append("  </td>");
                sb.Append(" </tr>");
                sb.Append(" <tr>");
                sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Reservation");
                sb.Append("  ID</span></p>");
                sb.Append("  </td>");
                sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + ReservationId + "</span></p>");
                sb.Append("  </td>");
                sb.Append(" </tr>");
                sb.Append(" <tr>");
                sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Reconfirmed");
                sb.Append("  by</span></p>");
                sb.Append("  </td>");
                sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + ReconfirmBy + "</span></p>");
                sb.Append("  </td>");
                sb.Append(" </tr>");
                sb.Append(" <tr>");
                sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Remark");
                sb.Append("  / Note</span></p>");
                sb.Append("  </td>");
                sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + Comment + "</span></p>");
                sb.Append("  </td>");
                sb.Append(" </tr>");
                sb.Append("</table>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span lang=EN-US style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hope ");
                sb.Append("the above is correct &amp;&nbsp;</span><span style='font-size:12.0pt;");
                sb.Append("font-family:\"Times New Roman\",\"serif\"'>your guest will enjoy the stay</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>In ");
                sb.Append("case of any discrepancy kindly contact us immediately at&nbsp;<a");
                sb.Append("href=\"mailto:hotels@clickurtrip.com\" target=\"_blank\"><span style='color:#1155CC'>hotels@clickurtrip.com</span></a></span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                sb.Append("\"Arial\",\"sans-serif\";color:#222222'>&nbsp;</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                sb.Append("\"Arial\",\"sans-serif\";color:#222222'>Best Regards,</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                sb.Append("\"Arial\",\"sans-serif\";color:#222222'>Online Reservation Team</span></p>");
                sb.Append("<p class=MsoNormal>&nbsp;</p>");
                sb.Append("</div>");
                sb.Append("</body>");
                sb.Append("</html>");

                string Title = "Hotel reconfirmation - " + HotelName + " - " + sReservation.VoucherNo + "";

                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));
                List<string> attachmentList = new List<string>();

                Dictionary<string, string> Email1List = new Dictionary<string, string>();
                Dictionary<string, string> Email2List = new Dictionary<string, string>();
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                Email1List.Add(sReservation.customer_Email, "");
                Email1List.Add(BCcTeamMails, "");
                Email2List.Add(CcTeamMail, "");

                CutAdmin.DataLayer.MailManager.SendMail(accessKey, Email1List, Email2List, Title, sb.ToString(), from, attachmentList);
                return true;

            }
            catch
            {
                return false;
            }
        }


        public class ListtoDataTable
        {
            public DataTable ToDataTable<T>(List<T> items)
            {
                DataTable dataTable = new DataTable(typeof(T).Name);
                //Get all the properties by using reflection   
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names  
                    dataTable.Columns.Add(prop.Name);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {

                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }

                return dataTable;
            }
        }
    }
}
