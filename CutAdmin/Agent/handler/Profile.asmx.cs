﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CutAdmin.Agent.handler
{
    /// <summary>
    /// Summary description for Profile
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class Profile : System.Web.Services.WebService
    {
        CutAdmin.Profile obj = new CutAdmin.Profile();
        string json = "";

        [WebMethod(EnableSession = true)]
        public string Save(string FistNAme, string LastNAme, string Designation, string Mobile, string CompanyName, string Companymail, string Phone,
            string Address, string Country, string City, string PinCode, string PanNo, string FaxNo)
        {
            return json = obj.Save(FistNAme, LastNAme, Designation, Mobile, CompanyName, Companymail, Phone,
             Address, Country, City, PinCode, PanNo, FaxNo);
        }
        [WebMethod(EnableSession = true)]
        public string GetUserDetail()
        {
            return json = obj.GetUserDetail();
        }
    }
}
