﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CutAdmin
{
    public partial class MappingDetails : System.Web.UI.Page
    {
        public string DescriptionHotel;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["SelectedHotelDetails"] != null)
            {

               this.DescriptionHotel = Session["SelectedHotelDetails"].ToString();
                
            }
            else
            {
                Session["SelectedHotelDetails"] = null;
            }
        }
    }
}