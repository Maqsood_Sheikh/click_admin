﻿using CommonLib.Response;
using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using CutAdmin.BL;
using System.Globalization;
namespace CutAdmin.Common
{
    public class BoookingManger
    {
        public class Group
        {
            public string RoomTypeID { get; set; }
            public string RoomDescriptionId { get; set; }
            public string Total { get; set; }
            public int noRooms { get; set; }
            public int AdultCount { get; set; }
            public int ChildCount { get; set; }
            public string ChildAges { get; set; }
        }
        public static string BlockRoom(string Serach, List<Group> ListRates, string HotelCode, string Supplier)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                HttpContext.Current.Session["BookingRates" + Serach] = ListRates;
                List<RoomType> Rooms = new List<RoomType>();
                CommonHotelDetails arrHotelDetails = (CommonHotelDetails)HttpContext.Current.Session["AvailDetails" + Serach];
                HotelFilter objHotel = new HotelFilter();
                if (HttpContext.Current.Session["ModelHotel" + Serach] == null)
                    throw new Exception("No Result Found.");
                objHotel = (HotelFilter)HttpContext.Current.Session["ModelHotel" + Serach];
                arrHotelDetails = objHotel.arrHotels.Where(d => d.HotelId == HotelCode.ToString()).FirstOrDefault();
                arrHotelDetails.RateList[0].Charge = new ServiceCharge();
                CommonLib.Response.RateGroup objAvailRate = arrHotelDetails.RateList.Where(d => d.Name == Supplier).FirstOrDefault();
                float RoomTotal = 0;
                List<TaxRate> other = new List<TaxRate>();
                float Other = 0;
                List<bool> bList = new List<bool>();
                List<Int64> noInventory = new List<Int64>();
                foreach (var objRate in ListRates)
                {
                    var arrRates = objAvailRate.RoomOccupancy.Where(d => d.AdultCount == objRate.AdultCount && d.ChildCount == objRate.ChildCount &&
                                                    d.ChildAges == objRate.ChildAges).FirstOrDefault();
                    if (arrRates != null)
                    {
                        #region Other Rates
                        var arrRate = arrRates.Rooms.Where(d => d.RoomTypeId == objRate.RoomTypeID
                                    && d.RoomDescription == objRate.RoomDescriptionId
                                    && d.Total == Convert.ToSingle(objRate.Total)).FirstOrDefault();
                        arrRate.OtherRates.ForEach(d => { d.TotalRate = (d.BaseRate * d.Per / 100); }); //  Take Other Rate
                        Other += arrRate.OtherRates.Select(d => d.TotalRate).ToList().Sum();
                        foreach (var objOther in arrRate.OtherRates)
                        {
                            if (other.Where(d => d.RateName == objOther.RateName).ToList().Count == 0)
                            {
                                other.Add(objOther);
                            }
                            else
                            {
                                other.Where(d => d.RateName == objOther.RateName).FirstOrDefault().TotalRate =
                                    other.Where(d => d.RateName == objOther.RateName).FirstOrDefault().TotalRate + objOther.TotalRate;
                            }
                        }

                        RoomTotal += arrRate.Total;
                        bList.Add(arrRate.CancellationPolicy.Any(d => d.CancelRestricted));
                        Rooms.Add(arrRate);
                        Rooms.Last().AdultCount = objRate.AdultCount;
                        Rooms.Last().ChildCount = objRate.ChildCount;
                        Rooms.Last().ChildAges = objRate.ChildAges;
                        #endregion

                        #region Check Inventory
                        foreach (var objDate in arrRate.Dates)
                        {
                            if (objDate.NoOfCount == 0)
                                objDate.NoOfCount = InventoryManager.GetInventoryCount(arrHotelDetails.HotelId, Convert.ToDecimal(objDate.RateTypeId), objDate.RoomTypeId.ToString(), objDate.datetime, Convert.ToInt64(objAvailRate.Name));
                            noInventory.Add(objDate.NoOfCount);

                        }
                        #endregion
                    }

                }
                if (noInventory.Any(d => d == 0) && bList.Any(d => d == true))
                    throw new Exception("Inventory is not available ,Please Contact Administrator or Change checking Date.");
                bool OnHold = false, OnRequest = false;
                DateTime ComapreDate = RatesManager.OnHoldDate(Rooms);
                if (noInventory.All(d => d == 0) && bList.Any(d => d != true))
                    OnRequest = true;
                else if (noInventory.All(d => d != 0) && bList.Any(d => d != true) && ComapreDate >= DateTime.Now)
                    OnHold = true;
                HttpContext.Current.Session["RateList" + Serach] = Rooms;
                arrHotelDetails.RateList.Where(d=>d.Name ==Supplier).FirstOrDefault().Charge = new ServiceCharge { RoomRate = RoomTotal, OtherRates = other, TotalPrice = RoomTotal + other.Select(d => d.TotalRate).ToList().Sum() };
                HttpContext.Current.Session["AvailDetails" + Serach] = arrHotelDetails;
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, OnHold = OnHold, OnRequest = OnRequest });

            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1, ex = ex.Message });
            }
        }


        public static string GetBookingDetails(string Serach)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                List<RoomType> Rooms = (List<RoomType>)HttpContext.Current.Session["RateList" + Serach];
                List<Group> ListRates = (List<Group>)HttpContext.Current.Session["BookingRates" + Serach];
                CommonHotelDetails arrHotelDetails = (CommonHotelDetails)HttpContext.Current.Session["AvailDetails" + Serach];
                return jsSerializer.Serialize(new { retCode = 1, ListRates = Rooms, arrHotel = arrHotelDetails, arrHotelDetails.RateList[0].Charge });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
        }


        public static string ValidateTransaction(List<CutAdmin.BookingHandler.Addons> arrAdons, string Serach)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            bool IsValid = false;
            try
            {
                CommonHotelDetails arrHotelDetails = (CommonHotelDetails)HttpContext.Current.Session["AvailDetails" + Serach];
                List<CommonLib.Response.RateGroup> objAvailRate = arrHotelDetails.RateList;
                float AddOnsPrice = 0;
                if (arrAdons.Count != 0)
                {
                    arrAdons.ForEach(d => d.TotalRate = (Convert.ToSingle(d.TotalRate) * Convert.ToSingle(d.Quantity)).ToString());
                    AddOnsPrice = arrAdons.Select(d => Convert.ToSingle(d.TotalRate)).ToList().Sum();
                }
                if (HttpContext.Current.Session["AvailDetails" + Serach] == null)
                    throw new Exception("Not Valid Booking Please Search again.");
                List<RatesManager.Supplier> Supplier = (List<RatesManager.Supplier>)HttpContext.Current.Session["RatesDetails"];
                float BaseRate = objAvailRate[0].Charge.TotalPrice + AddOnsPrice;
                #region checking AvailCredit with Booking Amount
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired ,Please Login and try Again");
                Int64 Uid = AccountManager.GetUserByLogin();
               
                #endregion
                if (IsValid)
                    return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
                else
                    throw new Exception("Your Balance is insufficient to Make this booking");
            }

            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1, ErrorMsg = ex.Message });
            }

        }


        public static bool ValidTransact(Int64 Uid, float BaseRate)
        {
            bool IsValid = false;
            try
            {
                float AvailableCredit = 0;
                 dbTaxHandlerDataContext dbTax = new dbTaxHandlerDataContext();
                var dtAvailableCredit = (from obj in dbTax.tbl_AdminCreditLimits where obj.uid == Uid select obj).FirstOrDefault();
                if (dtAvailableCredit == null)
                    throw new Exception("Please Contact  Administrator and try Again");
                AvailableCredit = Convert.ToSingle(dtAvailableCredit.AvailableCredit);
                float @Creditlimit = Convert.ToSingle(dtAvailableCredit.CreditAmount);
                bool Credit_Flag = (bool)dtAvailableCredit.Credit_Flag;
                bool OTC = (bool)dtAvailableCredit.OTC;
                float @MAXCreditlimit = Convert.ToSingle(dtAvailableCredit.MaxCreditLimit);
                if (AvailableCredit >= 0 && AvailableCredit >= BaseRate)
                {
                    IsValid = true;
                }
                else if (@AvailableCredit <= 0 && @Creditlimit <= 0 && @MAXCreditlimit >= BaseRate && @OTC == true)
                {
                    IsValid = true;
                }
                else if (@AvailableCredit >= 0 && (@AvailableCredit + @MAXCreditlimit + @Creditlimit) >= BaseRate && @OTC == true && @Credit_Flag == true)
                {
                    IsValid = true;
                }
                else if (@AvailableCredit >= 0 && (@AvailableCredit + @MAXCreditlimit) >= BaseRate && @OTC == true)
                {
                    IsValid = true;
                }
                else if (@AvailableCredit >= 0 && (@AvailableCredit + @Creditlimit) >= BaseRate && @Credit_Flag == true)
                {
                    IsValid = true;
                }
                else if (@AvailableCredit < 0 && @Creditlimit > 0 && @Creditlimit >= BaseRate && @Credit_Flag == true)
                {
                    IsValid = true;
                }
            }
            catch (Exception)
            {
                
                throw;
            }
            return IsValid;
        }

        public static string BookHotel(List<CutAdmin.BookingHandler.Addons> arrAddOns, string Serach, List<CommonLib.Request.Customer> LisCustumer)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                List<Group> ListRates = (List<Group>)HttpContext.Current.Session["BookingRates" + Serach];
                CommonHotelDetails arrHotelDetails = (CommonHotelDetails)HttpContext.Current.Session["AvailDetails" + Serach];
                Int64 Uid = 0; string sTo = "";
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session expired.");
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Uid = objGlobalDefault.sid;

                if (objGlobalDefault.UserType != "Supplier")
                {
                    Uid = objGlobalDefault.ParentId;
                    sTo = objGlobalDefault.uid;
                    sTo += "," + (from obj in DB.tbl_AdminLogins where obj.sid == Uid select obj).FirstOrDefault().uid;
                }
                else
                {
                    sTo = objGlobalDefault.uid;
                }
                var arrLastReservationID = (from obj in DB.tbl_CommonHotelReservations select obj).ToList().Count;
                String ReservationID = (arrLastReservationID + 1).ToString("D" + 6);
                bool response = false;
                #region Inventory Update & Validate
                List<RoomType> ListRate = (List<RoomType>)HttpContext.Current.Session["RateList" + Serach];
                bool valid = InventoryManager.CheckInventory(arrHotelDetails.HotelId, ListRate);
                if (valid)
                {

                    response = InventoryManager.UpdateInventory(ListRate, ReservationID, arrHotelDetails.HotelId);
                    List<tbl_CommonInventoryRecord> AddRecord = new List<tbl_CommonInventoryRecord>();
                    List<InventoryManager.RecordInv> Record = InventoryManager.Record;
                    for (int i = 0; i < Record.Count; i++)
                    {
                        tbl_CommonInventoryRecord Recordnew = new tbl_CommonInventoryRecord();
                        Recordnew.BookingId = Record[i].BookingId;
                        Recordnew.InvSid = Record[i].InvSid;
                        Recordnew.SupplierId = Record[i].SupplierId;
                        Recordnew.HotelCode = Record[i].HotelCode;
                        Recordnew.RoomType = Record[i].RoomType;
                        Recordnew.RateType = Record[i].RateType;
                        Recordnew.InvType = Record[i].InvType;
                        Recordnew.Date = Record[i].Date;
                        Recordnew.Month = Record[i].Month;
                        Recordnew.Year = Record[i].Year;
                        Recordnew.TotalAvlRoom = Record[i].TotalAvlRoom;
                        Recordnew.OldAvlRoom = Record[i].OldAvlRoom;
                        Recordnew.NoOfBookedRoom = Record[i].NoOfBookedRoom;
                        Recordnew.NoOfCancleRoom = Record[i].NoOfCancleRoom;
                        Recordnew.UpdateDate = Record[i].UpdateDate.ToString();
                        Recordnew.UpdateOn = Record[i].UpdateOn;
                        AddRecord.Add(Recordnew);
                    }
                    DB.tbl_CommonInventoryRecords.InsertAllOnSubmit(AddRecord);
                    DB.SubmitChanges();
                }
                #endregion

                /*AB Qudus*/
                #region AddOns  Save
                List<tbl_CommonBookingHotelAddon> Addons = new List<tbl_CommonBookingHotelAddon>();
                for (int l = 0; l < arrAddOns.Count; l++)
                {

                    string[] Date = arrAddOns[l].Date.Split('_');
                    Addons.Add(new tbl_CommonBookingHotelAddon
                    {
                        BookingId = ReservationID,
                        Date = Date[0],
                        RoomNo = Date[1],
                        Name = arrAddOns[l].Name,
                        Type = arrAddOns[l].Type,
                        Quantity = arrAddOns[l].Quantity,
                        Rate = arrAddOns[l].TotalRate,
                    });
                }
                DB.tbl_CommonBookingHotelAddons.InsertAllOnSubmit(Addons);
                DB.SubmitChanges();
                Addons.ForEach(r => r.Rate = (Convert.ToDecimal(r.Rate) * Convert.ToDecimal(r.Quantity)).ToString());
                #endregion

                #region Booking Transactions
                CutAdmin.DataLayer.ReservationDetails objReservation = new ReservationDetails();
                objReservation.TotalAmount = arrHotelDetails.RateList[0].Charge.TotalPrice;
                objReservation.AdminCommission = 0;
                objReservation.ListCharges = new List<TaxRate>();
                foreach (var objRate in arrHotelDetails.RateList[0].Charge.OtherRates)
                {
                    objReservation.ListCharges.Add(objRate);
                }
                Int64 noNights = Convert.ToInt64(ListRate[0].Dates.Count);
                objReservation.AdminCommission = BookingManager.AdminCommission(Convert.ToInt64(arrHotelDetails.HotelId), noNights);
                objReservation.AddOnsCharge = Addons.Select(D => Convert.ToSingle(D.Rate)).ToList().Sum();
                CutAdmin.DataLayer.BookingManager.objReservation = objReservation;
                objReservation.TotalPayable = BookingManager.TransactionAmount("AG");
                #region Hotel Booking Details
                var arrHotel = (from obj in DB.tbl_CommonHotelMasters where obj.sid == Convert.ToInt64(arrHotelDetails.HotelId) select obj).FirstOrDefault();
                tbl_CommonHotelReservation HotelReserv = new tbl_CommonHotelReservation();
                HotelReserv.ReservationID = Convert.ToString(ReservationID);
                HotelReserv.HotelCode = arrHotelDetails.HotelId;
                HotelReserv.HotelName = arrHotelDetails.HotelName;
                HotelReserv.mealplan = ListRate[0].RoomDescription;
                HotelReserv.TotalRooms = Convert.ToInt16(ListRate.Count);
                //HotelReserv.TotalRooms = Convert.ToInt16(Supplier[0].Details.Select(d => d.noRooms));
                HotelReserv.RoomCode = "";
                HotelReserv.sightseeing = 0;
                HotelReserv.Source = "From Hotel";
                if (response)
                    HotelReserv.Status = "Vouchered";
                else
                    HotelReserv.Status = "OnRequest";
                HotelReserv.SupplierCurrency = "INR";
                HotelReserv.terms = arrHotel.HotelNote;
                HotelReserv.ParentID = Uid;
                HotelReserv.Uid = AccountManager.GetUserByLogin();
                HotelReserv.Updatedate = DateTime.Now.ToString("dd-MM-yyyy");
                HotelReserv.Updateid = objGlobalDefault.sid.ToString();
                HotelReserv.VoucherID = "VCH-" + ReservationID;
                HotelReserv.DeadLine = "";
                HotelReserv.AgencyName = "";
                HotelReserv.AgentContactNumber = "";
                HotelReserv.AgentEmail = "";
                HotelReserv.AgentMarkUp_Per = 0;
                HotelReserv.AgentRef = "";
                HotelReserv.AgentRemark = "";
                HotelReserv.AutoCancelDate = "";
                HotelReserv.AutoCancelTime = "";
                HotelReserv.bookingname = LisCustumer[0].Title + ". " + LisCustumer[0].Name + " " + LisCustumer[0].LastName;
                HotelReserv.BookingStatus = "";
                HotelReserv.CancelDate = "";
                HotelReserv.CancelFlag = false;
                HotelReserv.CheckIn = arrHotelDetails.DateFrom;
                HotelReserv.CheckOut = arrHotelDetails.DateTo;
                HotelReserv.ChildAges = "";
                HotelReserv.Children = Convert.ToInt16(arrHotelDetails.RateList[0].RoomOccupancy.Select(d => d.ChildCount).ToList().Sum());
                HotelReserv.City = arrHotel.CityId;
                HotelReserv.deadlineemail = false;
                HotelReserv.Discount = 0;
                HotelReserv.ExchangeValue = 0;
                HotelReserv.ExtraBed = 0;
                HotelReserv.GTAId = "";
                HotelReserv.holdbooking = 0;
                HotelReserv.HoldTime = "";
                HotelReserv.HotelBookingData = "";
                HotelReserv.HotelDetails = "";
                HotelReserv.Infants = 0;
                HotelReserv.InvoiceID = "-";
                HotelReserv.IsAutoCancel = false;
                HotelReserv.IsConfirm = false;
                HotelReserv.LatitudeMGH = arrHotel.HotelLatitude;
                HotelReserv.LongitudeMGH = arrHotel.HotelLatitude;
                HotelReserv.LuxuryTax = 0;
                HotelReserv.mealplan_Amt = 0;
                HotelReserv.NoOfAdults = Convert.ToInt16(arrHotelDetails.RateList[0].RoomOccupancy.Select(d => d.AdultCount).ToList().Sum());
                // HotelReserv.NoOfDays = Convert.ToInt16(Supplier[0].Details[0].Rate.ListDates.Select(d => d.Date).ToList().Count());
                HotelReserv.NoOfDays = Convert.ToInt16(noNights);
                HotelReserv.OfferAmount = 0;
                HotelReserv.OfferId = 0;
                HotelReserv.RefAgency = "";
                HotelReserv.ReferenceCode = "";
                HotelReserv.Remarks = "";
                HotelReserv.ReservationDate = DateTime.Now.ToString("dd-MM-yyyy");
                HotelReserv.ReservationTime = "";
                HotelReserv.RoomRate = 0;
                HotelReserv.SalesTax = 0;
                HotelReserv.Servicecharge = 0;
                HotelReserv.ComparedFare = Convert.ToDecimal(objReservation.TotalAmount + objReservation.ListCharges.Select(d => d.TotalRate).ToList().Sum()) + Addons.Select(D => Convert.ToDecimal(D.Rate)).ToList().Sum();
                HotelReserv.ComparedCurrency = "";
                // HotelReserv.TotalFare = Convert.ToDecimal(Supplier[0].Details[0].Rate.ListDates.Select(d => d.TotalPrice).ToList().Sum());
                // HotelReserv.TotalFare = Total;
                HotelReserv.TotalFare = Convert.ToDecimal(BookingManager.TransactionAmount("AG")); //- objReservation.AdminCommission);
                DB.tbl_CommonHotelReservations.InsertOnSubmit(HotelReserv);
                DB.SubmitChanges();
                #endregion
                objReservation.objHotelDetails = new tbl_CommonHotelReservation();
                objReservation.objHotelDetails = HotelReserv;
                BookingManager.objReservation.objHotelDetails = new tbl_CommonHotelReservation();
                BookingManager.objReservation.objHotelDetails = HotelReserv;
                 DBHelper.DBReturnCode retCode =  DBHelper.DBReturnCode.EXCEPTION;
                if (valid)
                    retCode = CutAdmin.DataLayer.BookingManager.BookingTransact("Hotel");
                #endregion

                #region Booked Room
                DBHandlerDataContext dbRoom = new DBHandlerDataContext();
                List<tbl_CommonBookedRoom> arrBookedRoom = new List<tbl_CommonBookedRoom>();
                List<tbl_CommonBookedPassenger> arrPax = new List<tbl_CommonBookedPassenger>();
                foreach (var objRoom in ListRate)
                {

                    string Ages = "";
                    string CancellationAmountMultiple = "";
                    string CancellationDateTime = "";
                    Ages = objRoom.ChildAges;
                    tbl_CommonBookedRoom BookedRoom = new tbl_CommonBookedRoom();
                    BookedRoom.Adults = objRoom.AdultCount;
                    BookedRoom.BoardText = Convert.ToString(objRoom.RoomDescription);
                    BookedRoom.CanAmtWithTax = "";
                    BookedRoom.RoomAmount = Convert.ToDecimal(objRoom.Total) + 0;
                    foreach (var Canclellation in objRoom.CancellationPolicy)
                    {
                        CancellationAmountMultiple += Canclellation.objCharges.TotalRate + "|";
                        CancellationDateTime += Canclellation.CancellationDateTime + "|";
                    }
                    BookedRoom.CancellationAmount = CancellationAmountMultiple;
                    BookedRoom.Child = objRoom.ChildCount;
                    BookedRoom.ChildAge = Ages;
                    BookedRoom.Commision = 0;
                    BookedRoom.CutCancellationDate = CancellationDateTime;
                    BookedRoom.Discount = 0;
                    BookedRoom.EssentialInfo = "";
                    BookedRoom.ExchangeRate = 0;
                    BookedRoom.FranchiseeMarkup = 0;
                    BookedRoom.FranchiseeTaxDetails = "";
                    BookedRoom.LeadingGuest = "";
                    BookedRoom.MealPlanID = Convert.ToString(objRoom.RoomTypeId);
                    BookedRoom.Remark = "";
                    BookedRoom.ReservationID = Convert.ToString(ReservationID);
                    BookedRoom.TaxDetails = "";
                    foreach (var objCharge in objRoom.OtherRates)
                    {
                        BookedRoom.TaxDetails += objCharge.RateName + ":" + objCharge.TotalRate;
                    }
                    BookedRoom.RoomAmtWithTax = 0;
                    BookedRoom.RoomCode = Convert.ToString(objRoom.RoomDescriptionId);
                    BookedRoom.RoomNumber = Convert.ToString(ListRate.IndexOf(objRoom) + 1);
                    BookedRoom.RoomServiceTax = 0;
                    BookedRoom.RoomType = objRoom.RoomTypeName;
                    BookedRoom.SupplierAmount = 0;
                    BookedRoom.SupplierCurrency = objRoom.Dates[0].Currency;
                    BookedRoom.SupplierNoChargeDate = "";
                    BookedRoom.TDS = 0;
                    BookedRoom.TotalRooms = Convert.ToInt16(ListRate.Where(d => d.RoomTypeId == objRoom.RoomTypeId && d.RoomDescription == objRoom.RoomDescription).ToList().Count);
                    arrBookedRoom.Add(BookedRoom);
                    var arrRoomPax = LisCustumer.Where(d => d.RoomNo == ListRate.IndexOf(objRoom) + 1).ToList();
                    foreach (var objPax in arrRoomPax)
                    {
                        tbl_CommonBookedPassenger BookedPassenger = new tbl_CommonBookedPassenger();
                        BookedPassenger.Age = objPax.Age;
                        if (ListRate.IndexOf(objRoom) == 0)
                            BookedPassenger.IsLeading = true;
                        else
                            BookedPassenger.IsLeading = false;
                        BookedPassenger.LastName = objPax.LastName;
                        BookedPassenger.Name = objPax.Title + " " + objPax.Name;
                        BookedPassenger.PassengerType = objPax.type;
                        BookedPassenger.ReservationID = ReservationID;
                        BookedPassenger.RoomCode = "";
                        BookedPassenger.RoomNumber = Convert.ToString(ListRate.IndexOf(objRoom) + 1);
                        arrPax.Add(BookedPassenger);
                    }


                }
                dbRoom.tbl_CommonBookedRooms.InsertAllOnSubmit(arrBookedRoom);
                dbRoom.tbl_CommonBookedPassengers.InsertAllOnSubmit(arrPax);
                dbRoom.SubmitChanges();
                #endregion
                MailManager.SendInvoice(ReservationID, Uid, sTo);
                return jsSerializer.Serialize(new { retCode = 1, BookingID = ReservationID });
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { });
            }
        }

        public static string GroupRequest(Int64 noRoom, Int64 noPax, string HotelCode, string CheckIn, string CheckOut, string PaxName)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                string ReservationID = "";
                tbl_CommonHotelReservation HotelReserv = new tbl_CommonHotelReservation();
                DateTime dCheckIn = DateTime.ParseExact(CheckIn, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                DateTime dCheckOut = DateTime.ParseExact(CheckOut, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                double noNights = (dCheckOut - dCheckIn).TotalDays;
                using (var DB = new DBHandlerDataContext())
                {
                    var arrLastReservationID = (from obj in DB.tbl_CommonHotelReservations select obj).ToList().Count;
                    ReservationID = (arrLastReservationID + 1).ToString("D" + 6);
                    var arrHotel = (from obj in DB.tbl_CommonHotelMasters where obj.sid == Convert.ToInt64(HotelCode) select obj).FirstOrDefault();
                   
                    HotelReserv.ReservationID = Convert.ToString(ReservationID);
                    HotelReserv.HotelCode = HotelCode;
                    HotelReserv.HotelName = arrHotel.HotelName;
                    HotelReserv.mealplan ="RO";
                    HotelReserv.TotalRooms = Convert.ToInt16(noRoom);
                    HotelReserv.RoomCode = "";
                    HotelReserv.sightseeing = 0;
                    HotelReserv.Source = "From Hotel";
                    HotelReserv.Status = "GroupRequest";
                    HotelReserv.SupplierCurrency = "SAR";
                    HotelReserv.terms = arrHotel.HotelNote;
                    HotelReserv.ParentID = CutAdmin.DataLayer.AccountManager.GetSupplierByUser();
                    HotelReserv.Uid = AccountManager.GetUserByLogin();
                    HotelReserv.Updatedate = DateTime.Now.ToString("dd-MM-yyyy");
                    HotelReserv.Updateid = CutAdmin.DataLayer.AccountManager.GetUserByLogin().ToString();
                    HotelReserv.VoucherID = "";
                    HotelReserv.DeadLine = "";
                    HotelReserv.AgencyName = "";
                    HotelReserv.AgentContactNumber = "";
                    HotelReserv.AgentEmail = "";
                    HotelReserv.AgentMarkUp_Per = 0;
                    HotelReserv.AgentRef = "";
                    HotelReserv.AgentRemark = "";
                    HotelReserv.AutoCancelDate = "";
                    HotelReserv.AutoCancelTime = "";
                    HotelReserv.bookingname = PaxName;
                    HotelReserv.BookingStatus = "GroupRequest";
                    HotelReserv.CancelDate = "";
                    HotelReserv.CancelFlag = false;
                    HotelReserv.CheckIn = CheckIn;
                    HotelReserv.CheckOut = CheckOut;
                    HotelReserv.ChildAges = "";
                    HotelReserv.Children = Convert.ToInt16(0);
                    HotelReserv.City = arrHotel.CityId;
                    HotelReserv.deadlineemail = false;
                    HotelReserv.Discount = 0;
                    HotelReserv.ExchangeValue = 0;
                    HotelReserv.ExtraBed = 0;
                    HotelReserv.GTAId = "";
                    HotelReserv.holdbooking = 0;
                    HotelReserv.HoldTime = "";
                    HotelReserv.HotelBookingData = "";
                    HotelReserv.HotelDetails = "";
                    HotelReserv.Infants = 0;
                    HotelReserv.InvoiceID = "-";
                    HotelReserv.IsAutoCancel = false;
                    HotelReserv.IsConfirm = false;
                    HotelReserv.LatitudeMGH = arrHotel.HotelLatitude;
                    HotelReserv.LongitudeMGH = arrHotel.HotelLatitude;
                    HotelReserv.LuxuryTax = 0;
                    HotelReserv.mealplan_Amt = 0;
                    HotelReserv.NoOfAdults = Convert.ToInt16(noPax);
                    HotelReserv.NoOfDays = Convert.ToInt16(noNights);
                    HotelReserv.OfferAmount = 0;
                    HotelReserv.OfferId = 0;
                    HotelReserv.RefAgency = "";
                    HotelReserv.ReferenceCode = "";
                    HotelReserv.Remarks = "";
                    HotelReserv.ReservationDate = DateTime.Now.ToString("dd-MM-yyyy");
                    HotelReserv.ReservationTime = "";
                    HotelReserv.RoomRate = 0;
                    HotelReserv.SalesTax = 0;
                    HotelReserv.Servicecharge = 0;
                    HotelReserv.ComparedFare = 0;
                    HotelReserv.ComparedCurrency = "";
                    HotelReserv.ParentID = CutAdmin.DataLayer.AccountManager.GetSupplierByUser();
                    HotelReserv.TotalFare = 0; //- objReservation.AdminCommission);
                    DB.tbl_CommonHotelReservations.InsertOnSubmit(HotelReserv);
                    DB.SubmitChanges();
                }
              
                #region Hotel Booking Details
              
               
              
                #endregion
                return jsSerializer.Serialize(new { retCode = 1, ReservationID = HotelReserv.ReservationID});
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new {retCode=0, ex= ex.Message });
            }
        }
    }
}