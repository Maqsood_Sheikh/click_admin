﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using DOTWLib.Response;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Xml;

namespace CutAdmin.Common
{
    public class ParseDOTWResponse
    {
        List<DOTWLib.Response.Facility> List_Facility = new List<DOTWLib.Response.Facility>();
        List<DOTWLib.Response.Location> List_Location = new List<DOTWLib.Response.Location>();
        List<DOTWLib.Response.Category> List_Category = new List<DOTWLib.Response.Category>();
        public static System.Globalization.CultureInfo CultureInfos { get; set; }
        Int64 NoofRooms;
        public HttpContext Context { get; set; }
        //Int64 Night;

        public bool ParseXML(string xmlString, Int64 Night, Int64 NoofRooms, List<DOTWLib.Request.Occupancy> Occupancy, string session)
        {
            DBHelper.DBReturnCode retCode;
            DataTable dtResult;
            XmlDocument doc = new XmlDocument();
            bool nStatus;
            //string xmlPriceString;
            //doc.LoadXml(DOTW_data);
            doc.LoadXml(xmlString);
            XmlNode ndResponseStatus = doc.SelectSingleNode("result/successful");
            //XmlNode ndResponseStatus = doc.SelectSingleNode("result/");
            //bool nStatus = Convert.ToBoolean(ndResponseStatus.Attributes.GetNamedItem("successful").Value);

            string[] array_input = session.Split('_');
            string[] Country = array_input[1].Split(',');
            string cty = Country[0];

            string fDate = array_input[2].ToString();
            string tDate = array_input[3].ToString();

            //DateTime DoTWfDate = Convert.ToDateTime(array_input[2].ToString(), new System.Globalization.CultureInfo("en-GB"));
            //DateTime DoTWtDate = Convert.ToDateTime(array_input[3].ToString(), new System.Globalization.CultureInfo("en-GB"));

            //DateTime DoTWfDate = Convert.ToDateTime(fDate);
            //DateTime DoTWtDate = Convert.ToDateTime(tDate);

            nStatus = Convert.ToBoolean(ndResponseStatus.InnerText);
            if (nStatus == true)
            {
                XmlNode ndHotels = doc.SelectSingleNode("result/hotels");
                int nNumberOfHotels = Convert.ToInt32(ndHotels.Attributes.GetNamedItem("count").Value);
                Context.Session["DoTWNumberOfHotels"] = nNumberOfHotels;
                // Context.Session["DoTWNumberOfHotels"] = nNumberOfHotels;
                //Context.Session["Page"] = Convert.ToInt32(ndHotels.Attributes.GetNamedItem("page").Value);
                //Context.Session["Limit"] = Convert.ToInt32(ndHotels.Attributes.GetNamedItem("limit").Value);
                List<DOTWHotelDetails> DOTWHotelDetails = new List<DOTWHotelDetails>();
                DOTWHotelDetails[] objDOTWHotelDetails = new DOTWHotelDetails[nNumberOfHotels];
                for (int i = 0; i < nNumberOfHotels; i++)
                {
                    objDOTWHotelDetails[i] = new DOTWHotelDetails();

                    //XmlNode ndHotel = doc.SelectSingleNode("result/hotels/hotel").ChildNodes[i];
                    XmlNode ndHotel = doc.SelectSingleNode("result/hotels").ChildNodes[i];
                    objDOTWHotelDetails[i].Currency = doc.SelectSingleNode("result/currencyShort").InnerText;
                    objDOTWHotelDetails[i].HotelId = Convert.ToInt64(ndHotel.Attributes.GetNamedItem("hotelid").Value);
                    objDOTWHotelDetails[i].HotelName = ndHotel.SelectSingleNode("hotelName").InnerText;
                    objDOTWHotelDetails[i].CityCode = ndHotel.SelectSingleNode("cityCode").InnerText;
                    objDOTWHotelDetails[i].FullAddress = new FullAddress();
                    objDOTWHotelDetails[i].Description = ndHotel.SelectSingleNode("description1").InnerText;
                    objDOTWHotelDetails[i].Attraction = new List<Attraction>();
                    objDOTWHotelDetails[i].Amenitie = new List<Amenitie>();
                    objDOTWHotelDetails[i].Business = new List<Business>();
                    objDOTWHotelDetails[i].HotelPhone = ndHotel.SelectSingleNode("hotelPhone").InnerText;
                    objDOTWHotelDetails[i].HotelCheckIn = fDate + " " + ndHotel.SelectSingleNode("hotelCheckIn").InnerText;
                    objDOTWHotelDetails[i].HotelCheckOut = tDate + " " + ndHotel.SelectSingleNode("hotelCheckOut").InnerText;
                    objDOTWHotelDetails[i].Supplier = "DoTW";
                    int nRatingId = Convert.ToInt32(ndHotel.SelectSingleNode("rating").InnerText);
                    objDOTWHotelDetails[i].Rating = nRatingId.ToString();
                    if (!List_Category.Exists(data => data.Name == objDOTWHotelDetails[i].Rating))
                    {
                        List_Category.Add(new Category { Name = objDOTWHotelDetails[i].Rating });
                        //objCategory.Count = objCategory.Count + 1;
                    }
                   
                    objDOTWHotelDetails[i].HotelImages = new List<Image>();
                    objDOTWHotelDetails[i].Lat = Convert.ToDouble(ndHotel.SelectSingleNode("geoPoint/lat").InnerText);
                    objDOTWHotelDetails[i].Lng = Convert.ToDouble(ndHotel.SelectSingleNode("geoPoint/lng").InnerText);
                    //objDOTWHotelDetails[i].RoomType = new List<RoomType>();
                    objDOTWHotelDetails[i].Room = new List<Room>();

                    objDOTWHotelDetails[i].FullAddress.hotelStreetAddress = ndHotel.SelectSingleNode("fullAddress/hotelStreetAddress").InnerText;
                    objDOTWHotelDetails[i].FullAddress.hotelZipCode = ndHotel.SelectSingleNode("fullAddress/hotelZipCode").InnerText;
                    objDOTWHotelDetails[i].FullAddress.hotelCountry = ndHotel.SelectSingleNode("fullAddress/hotelCountry").InnerText;
                    objDOTWHotelDetails[i].FullAddress.hotelCity = ndHotel.SelectSingleNode("fullAddress/hotelCity").InnerText;

                    XmlNodeList ndAttractionList = ndHotel.SelectNodes("attraction/attractionItem");
                    Attraction[] objAttraction = new Attraction[ndAttractionList.Count];
                    for (int j = 0; j < ndAttractionList.Count; j++)
                    {
                        objAttraction[j] = new Attraction();
                        objAttraction[j].ItemNo = Convert.ToInt32(ndAttractionList[j].Attributes.GetNamedItem("runno").Value);
                        objAttraction[j].Name = ndAttractionList[j].SelectSingleNode("name").InnerText;
                        objAttraction[j].dist = new List<Dist>();

                        XmlNodeList ndDistList = ndAttractionList[j].SelectNodes("dist");
                        Dist[] objDist = new Dist[ndDistList.Count];
                        for (int k = 0; k < ndDistList.Count; k++)
                        {
                            objDist[k] = new Dist();
                            objDist[k].Attr = ndDistList[k].Attributes.GetNamedItem("attr").Value;
                            objDist[k].Value = ndDistList[k].InnerText;
                            objAttraction[j].dist.Add(objDist[k]);
                        }
                        objDOTWHotelDetails[i].Attraction.Add(objAttraction[j]);
                        if (!List_Location.Exists(data => data.Name == objAttraction[j].Name))
                        {
                            List_Location.Add(new Location { Name = objAttraction[j].Name });
                            objAttraction[j].Count = objAttraction[j].Count + 1;
                        }
                    }

                    XmlNodeList ndAmenitieList = ndHotel.SelectNodes("amenitie/language/amenitieItem");
                    Amenitie[] objAmenitie = new Amenitie[ndAmenitieList.Count];
                    for (int j = 0; j < ndAmenitieList.Count; j++)
                    {
                        objAmenitie[j] = new Amenitie();
                        objAmenitie[j].AmenitieId = Convert.ToInt32(ndAmenitieList[j].Attributes.GetNamedItem("id").Value);
                        objAmenitie[j].AmenitieName = ndAmenitieList[j].InnerText;
                        objDOTWHotelDetails[i].Amenitie.Add(objAmenitie[j]);
                        if (!List_Facility.Exists(data => data.Name == objAmenitie[j].AmenitieName))
                            List_Facility.Add(new Facility { Name = objAmenitie[j].AmenitieName });
                    }

                    XmlNodeList ndBusinessList = ndHotel.SelectNodes("business/language/businessItem");
                    Business[] objBusiness = new Business[ndBusinessList.Count];
                    for (int j = 0; j < ndBusinessList.Count; j++)
                    {
                        objBusiness[j] = new Business();
                        objBusiness[j].BusinessId = Convert.ToInt32(ndBusinessList[j].Attributes.GetNamedItem("id").Value);
                        objBusiness[j].BusinessName = ndBusinessList[j].InnerText;
                        objDOTWHotelDetails[i].Business.Add(objBusiness[j]);
                    }

                    if (ndHotel.SelectSingleNode("images/hotelImages") != null)
                    {
                        XmlNode nImageList = ndHotel.SelectSingleNode("images/hotelImages");
                        int nImage = Convert.ToInt32(nImageList.Attributes.GetNamedItem("count").Value);
                        XmlNodeList ndImageList = nImageList.SelectNodes("image");
                        ////for primary hotel image
                        //Image oImage = new Image();
                        //oImage.CategoryId = 1;
                        //oImage.CategoryName = "HotelImage";
                        //oImage.Url = nImageList.SelectSingleNode("thumb").InnerText;
                        //objDOTWHotelDetails[i].HotelImages.Add(oImage);
                        ////for primary hotel image -- end
                        Image[] objImage = new Image[nImage];
                        for (int j = 0; j < nImage; j++)
                        {
                            objImage[j] = new Image();
                            objImage[j].CategoryId = Convert.ToInt32(ndImageList[j].SelectSingleNode("category").Attributes.GetNamedItem("id").Value);
                            objImage[j].CategoryName = ndImageList[j].SelectSingleNode("category").InnerText;
                            objImage[j].Url = ndImageList[j].SelectSingleNode("url").InnerText;
                            objDOTWHotelDetails[i].HotelImages.Add(objImage[j]);
                        }
                    }


                    XmlNodeList ndRoom = ndHotel.SelectNodes("rooms/room");
                    //int nRoom = ndRoom.Count;
                    Room[] objRoom = new Room[ndRoom.Count];
                    for (int j = 0; j < ndRoom.Count; j++)
                    {
                        objRoom[j] = new Room();
                        objRoom[j].RoomType = new List<RoomType>();

                        XmlNodeList ndRoomType = ndRoom[j].SelectNodes("roomType");
                        //int nRoom = Convert.ToInt32(ndRoomType.Attributes.GetNamedItem("count").Value);

                        RoomType[] objRoomType = new RoomType[ndRoomType.Count];
                        //XmlNodeList ndRoomTypeList = ndRoomType.SelectNodes("roomType");
                        for (int k = 0; k < ndRoomType.Count; k++)
                        {
                            objRoomType[k] = new RoomType();
                            //XmlNode nRoomType = ndRoomType.SelectSingleNode("roomType");
                            objRoomType[k].RoomTypeCode = Convert.ToInt32(ndRoomType[k].Attributes.GetNamedItem("roomtypecode").Value);

                            XmlNodeList nRoomAmenities = ndRoomType[k].SelectNodes("roomAmenities/amenity");
                            //int nRoomAmenitiesCount = Convert.ToInt32(nRoomAmenities.Attributes.GetNamedItem("count").Value);
                            RoomAmenities[] objRoomAmenities = new RoomAmenities[nRoomAmenities.Count];
                            objRoomType[k].RoomAmenities = new List<RoomAmenities>();
                            for (int l = 0; l < nRoomAmenities.Count; l++)
                            {
                                objRoomAmenities[l] = new RoomAmenities();
                                objRoomAmenities[l].RoomAmenitiesId = Convert.ToInt32(nRoomAmenities[l].Attributes.GetNamedItem("id").Value);
                                objRoomAmenities[l].RoomAmenitiesName = nRoomAmenities[l].InnerText;
                                objRoomType[k].RoomAmenities.Add(objRoomAmenities[l]);
                            }

                            objRoomType[k].RoomTypeName = ndRoomType[k].SelectSingleNode("name").InnerText;
                            objRoomType[k].RoomInfo = new RoomInfo();
                            objRoomType[k].RoomCapacityInfo = new RoomCapacityInfo();
                            XmlNode ndRoomInfoList = ndRoomType[k].SelectSingleNode("roomInfo");
                            RoomInfo objRoomInfo = new RoomInfo();

                            if (ndRoomInfoList.SelectSingleNode("maxAdult").InnerText != "" && ndRoomInfoList.SelectSingleNode("maxAdult") != null)
                            {
                                objRoomType[k].RoomInfo.maxAdult = Convert.ToInt32(ndRoomInfoList.SelectSingleNode("maxAdult").InnerText);
                            }
                            else
                            {
                                objRoomType[k].RoomInfo.maxAdult = 0;
                            }
                            if (ndRoomInfoList.SelectSingleNode("maxExtraBed").InnerText != "" && ndRoomInfoList.SelectSingleNode("maxExtraBed") != null)
                            {
                                objRoomType[k].RoomInfo.maxExtraBed = Convert.ToInt32(ndRoomInfoList.SelectSingleNode("maxExtraBed").InnerText);
                            }
                            else
                            {
                                objRoomType[k].RoomInfo.maxExtraBed = 0;
                            }
                            if (ndRoomInfoList.SelectSingleNode("maxChildren").InnerText != "" && ndRoomInfoList.SelectSingleNode("maxChildren") != null)
                            {
                                objRoomType[k].RoomInfo.maxChildren = Convert.ToInt32(ndRoomInfoList.SelectSingleNode("maxChildren").InnerText);
                            }
                            else
                            {
                                objRoomType[k].RoomInfo.maxChildren = 0;
                            }
                            //objRoomType[k].RoomInfo.maxAdult = Convert.ToInt32(ndRoomInfoList.SelectSingleNode("maxAdult").InnerText);
                            //objRoomType[k].RoomInfo.maxExtraBed = Convert.ToInt32(ndRoomInfoList.SelectSingleNode("maxExtraBed").InnerText);
                            //objRoomType[k].RoomInfo.maxChildren = Convert.ToInt32(ndRoomInfoList.SelectSingleNode("maxChildren").InnerText);

                            RoomCapacityInfo objRoomCapacityInfo = new RoomCapacityInfo();
                            XmlNode ndRoomCapacityInfoList = ndRoomType[k].SelectSingleNode("roomCapacityInfo");
                            if (ndRoomCapacityInfoList.SelectSingleNode("roomPaxCapacity").InnerText != "" && ndRoomCapacityInfoList.SelectSingleNode("roomPaxCapacity") != null)
                            {
                                objRoomType[k].RoomCapacityInfo.roomPaxCapacity = Convert.ToInt32(ndRoomCapacityInfoList.SelectSingleNode("roomPaxCapacity").InnerText);
                            }
                            else
                            {
                                objRoomType[k].RoomCapacityInfo.roomPaxCapacity = 0;
                            }
                            if (ndRoomCapacityInfoList.SelectSingleNode("allowedAdultsWithoutChildren").InnerText != "" && ndRoomCapacityInfoList.SelectSingleNode("allowedAdultsWithoutChildren") != null)
                            {
                                objRoomType[k].RoomCapacityInfo.allowedAdultsWithoutChildren = Convert.ToInt32(ndRoomCapacityInfoList.SelectSingleNode("allowedAdultsWithoutChildren").InnerText);
                            }
                            else
                            {
                                objRoomType[k].RoomCapacityInfo.allowedAdultsWithoutChildren = 0;
                            }
                            if (ndRoomCapacityInfoList.SelectSingleNode("allowedAdultsWithChildren").InnerText != "" && ndRoomCapacityInfoList.SelectSingleNode("allowedAdultsWithChildren") != null)
                            {
                                objRoomType[k].RoomCapacityInfo.allowedAdultsWithChildren = Convert.ToInt32(ndRoomCapacityInfoList.SelectSingleNode("allowedAdultsWithChildren").InnerText);
                            }
                            else
                            {
                                objRoomType[k].RoomCapacityInfo.allowedAdultsWithChildren = 0;
                            }
                            if (ndRoomCapacityInfoList.SelectSingleNode("maxExtraBed").InnerText != "" && ndRoomCapacityInfoList.SelectSingleNode("maxExtraBed") != null)
                            {
                                objRoomType[k].RoomCapacityInfo.maxExtraBed = Convert.ToInt32(ndRoomCapacityInfoList.SelectSingleNode("maxExtraBed").InnerText);
                            }
                            else
                            {
                                objRoomType[k].RoomCapacityInfo.maxExtraBed = 0;
                            }


                            //objRoomType[k].RoomCapacityInfo.roomPaxCapacity = Convert.ToInt32(ndRoomCapacityInfoList.SelectSingleNode("roomPaxCapacity").InnerText);
                            //objRoomType[k].RoomCapacityInfo.allowedAdultsWithoutChildren = Convert.ToInt32(ndRoomCapacityInfoList.SelectSingleNode("allowedAdultsWithoutChildren").InnerText);
                            //objRoomType[k].RoomCapacityInfo.allowedAdultsWithChildren = Convert.ToInt32(ndRoomCapacityInfoList.SelectSingleNode("allowedAdultsWithChildren").InnerText);
                            //objRoomType[k].RoomCapacityInfo.maxExtraBed = Convert.ToInt32(ndRoomCapacityInfoList.SelectSingleNode("maxExtraBed").InnerText);

                            //objDOTWHotelDetails[i].RoomType.Add(objRoomType[k]);
                            objRoom[j].RoomType.Add(objRoomType[k]);
                        }
                        objDOTWHotelDetails[i].Room.Add(objRoom[j]);
                    }
                    //HotelHandler objHotelHandler = new HotelHandler();
                    //xmlPriceString = objHotelHandler.GetRoomDetails(objDOTWHotelDetails[i]);
                    //objDOTWHotelDetails[i] = ParsePrice(xmlPriceString, Night, NoofRooms, objDOTWHotelDetails[i], Occupancy);
                    // Context.Session["Hotel" + i] = objDOTWHotelDetails[i];

                    //test commented 2/6/2017 by maqsood
                    //Context.Session["Hotel" + i] = objDOTWHotelDetails[i];
                    //end test commented 2/6/2017 by maqsood
                    DOTWHotelDetails.Add(objDOTWHotelDetails[i]);
                }
                Context.Session["Hotel"] = DOTWHotelDetails;
                List_Category = List_Category.OrderBy(data => data.Name).ToList();
                List_Facility = List_Facility.OrderBy(data => data.Name).ToList();
                List_Location = List_Location.OrderBy(data => data.Name).ToList();
                return nStatus;
            }
            else
                return nStatus;
        }

        #region Parseprice
        public DOTWHotelDetails ParsePrice(string xmlPriceString, Int64 Night, Int64 NoofRooms, DOTWHotelDetails objDOTWHotelDetails, List<DOTWLib.Request.Occupancy> Occupancy, out string ErrorMessage) //test
        {
            DBHelper.DBReturnCode retCode;
            DataTable dtResult;
            XmlDocument doc = new XmlDocument();
            bool nStatus;
            float BaseAmt;

           

            float CutRoomAmountAmt = 0;

            float CutGlobalMarkupAmt = 0;
            float CutGlobalMarkupPer = 0;
            float ActualGlobalMarkupAmt = 0;

            float CutGroupMarkupAmt = 0;
            float CutGroupMarkupPer = 0;
            float ActualGroupMarkupAmt = 0;

            float CutIndividualMarkupAmt = 0;
            float CutIndividualMarkupPer = 0;
            float ActualIndividualMarkupAmt = 0;

            float ActualAgentRoomMarkup = 0;
            float CutRoomAmount = 0;
            float AgentRoomMarkup = 0;
            float AgentOwnMarkupPer = 0;
            float AgentOwnMarkupAmt = 0;

            string currency = "";

            float GlobalMarkup = 0;
            float GroupMarkup = 0;
            float IndividualMarkup = 0;

            string allowBook = "";
            List<Room> lstRoom_Final = new List<Room>();

            doc.LoadXml(xmlPriceString);
            XmlNode ndResponseStatus = doc.SelectSingleNode("result/successful");
            if (ndResponseStatus == null)
            {
                XmlNode ndResponseStatus_F = doc.SelectSingleNode("result/request/successful");
                nStatus = Convert.ToBoolean(ndResponseStatus_F.InnerText);
            }
            else
            {
                nStatus = Convert.ToBoolean(ndResponseStatus.InnerText);
            }
            if (nStatus == true)
            {
                currency = doc.SelectSingleNode("result/currencyShort").InnerText;
               DataLayer.ExchangeRateManager.GetForeignExchangeRate(currency, out dtResult);
                float dolarRate = (float)Convert.ToDecimal(dtResult.Rows[0]["INR"].ToString(), CultureInfos);
                XmlNode ndHotel = doc.SelectSingleNode("result/hotel");
                allowBook = ndHotel.SelectSingleNode("allowBook").InnerText;
                if (allowBook == "yes")
                {

                    MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();
                    if (Context.Session["markups"] != null)
                        objMarkupsAndTaxes = (MarkupsAndTaxes)Context.Session["markups"];
                    else if (Context.Session["AgentMarkupsOnAdmin"] != null)
                        objMarkupsAndTaxes = (MarkupsAndTaxes)Context.Session["AgentMarkupsOnAdmin"];
                    bool TaxOnMarkup = objMarkupsAndTaxes.listGroupMarkup.Single(data => data.Supplier == "Mgh").TaxOnMarkup;
                    XmlNodeList ndRoom = ndHotel.SelectNodes("rooms/room");
                    Room[] objRoom = new Room[ndRoom.Count];
                    for (int j = 0; j < ndRoom.Count; j++)
                    {
                        objRoom[j] = new Room();
                        objRoom[j].Count = Convert.ToInt64(ndRoom[j].Attributes.GetNamedItem("count").Value, CultureInfos);
                        objRoom[j].RoomType = new List<RoomType>();
                        XmlNodeList ndRoomType = ndRoom[j].SelectNodes("roomType");
                        RoomType[] objRoomType = new RoomType[ndRoomType.Count];
                        for (int k = 0; k < ndRoomType.Count; k++)
                        {
                            objRoomType[k] = new RoomType();
                            objRoomType[k].RoomTypeName = ndRoomType[k].SelectSingleNode("name").InnerText;
                            objRoomType[k].special = new List<special>();
                            objRoomType[k].rateBasis = new List<rateBasis>();

                            ////////////
                            objRoomType[k].RoomInfo = new RoomInfo();
                            objRoomType[k].RoomCapacityInfo = new RoomCapacityInfo();
                            XmlNode ndRoomInfoList = ndRoomType[k].SelectSingleNode("roomInfo");
                            RoomInfo objRoomInfo = new RoomInfo();

                            if (ndRoomInfoList.SelectSingleNode("maxOccupancy") != null)
                            {
                                if (ndRoomInfoList.SelectSingleNode("maxOccupancy").InnerText != "")
                                {
                                    objRoomType[k].RoomInfo.maxOccupancy = Convert.ToInt32(ndRoomInfoList.SelectSingleNode("maxOccupancy").InnerText, CultureInfos);
                                }
                                else
                                {
                                    objRoomType[k].RoomInfo.maxOccupancy = 0;
                                }
                            }
                            if (ndRoomInfoList.SelectSingleNode("maxAdultWithChildren") != null)
                            {
                                if (ndRoomInfoList.SelectSingleNode("maxAdultWithChildren").InnerText != "")
                                {
                                    objRoomType[k].RoomInfo.maxAdultWithChildren = Convert.ToInt32(ndRoomInfoList.SelectSingleNode("maxAdultWithChildren").InnerText, CultureInfos);
                                }
                                else
                                {
                                    objRoomType[k].RoomInfo.maxAdultWithChildren = 0;
                                }
                            }
                            if (ndRoomInfoList.SelectSingleNode("minChildAge") != null)
                            {
                                if (ndRoomInfoList.SelectSingleNode("minChildAge").InnerText != "")
                                {
                                    objRoomType[k].RoomInfo.minChildAge = Convert.ToInt32(ndRoomInfoList.SelectSingleNode("minChildAge").InnerText, CultureInfos);
                                }
                                else
                                {
                                    objRoomType[k].RoomInfo.minChildAge = 0;
                                }
                            }
                            if (ndRoomInfoList.SelectSingleNode("maxChildAge") != null)
                            {
                                if (ndRoomInfoList.SelectSingleNode("maxChildAge").InnerText != "")
                                {
                                    objRoomType[k].RoomInfo.maxChildAge = Convert.ToInt32(ndRoomInfoList.SelectSingleNode("maxChildAge").InnerText, CultureInfos);
                                }
                                else
                                {
                                    objRoomType[k].RoomInfo.maxChildAge = 0;
                                }
                            }
                            if (ndRoomInfoList.SelectSingleNode("maxAdult") != null)
                            {
                                if (ndRoomInfoList.SelectSingleNode("maxAdult").InnerText != "")
                                {
                                    objRoomType[k].RoomInfo.maxAdult = Convert.ToInt32(ndRoomInfoList.SelectSingleNode("maxAdult").InnerText, CultureInfos);
                                }
                                else
                                {
                                    objRoomType[k].RoomInfo.maxAdult = 0;
                                }
                            }
                            if (ndRoomInfoList.SelectSingleNode("maxExtraBed") != null)
                            {
                                if (ndRoomInfoList.SelectSingleNode("maxExtraBed").InnerText != "")
                                {
                                    objRoomType[k].RoomInfo.maxExtraBed = Convert.ToInt32(ndRoomInfoList.SelectSingleNode("maxExtraBed").InnerText, CultureInfos);
                                }
                                else
                                {
                                    objRoomType[k].RoomInfo.maxExtraBed = 0;
                                }
                            }
                            if (ndRoomInfoList.SelectSingleNode("maxChildren") != null)
                            {
                                if (ndRoomInfoList.SelectSingleNode("maxChildren").InnerText != "")
                                {
                                    objRoomType[k].RoomInfo.maxChildren = Convert.ToInt32(ndRoomInfoList.SelectSingleNode("maxChildren").InnerText, CultureInfos);
                                }
                                else
                                {
                                    objRoomType[k].RoomInfo.maxChildren = 0;
                                }
                            }





                           

                            RoomCapacityInfo objRoomCapacityInfo = new RoomCapacityInfo();
                            XmlNode ndRoomCapacityInfoList = ndRoomType[k].SelectSingleNode("roomCapacityInfo");

                            if (ndRoomCapacityInfoList != null)
                            {
                                if (ndRoomCapacityInfoList.SelectSingleNode("roomPaxCapacity") != null)
                                {
                                    if (ndRoomCapacityInfoList.SelectSingleNode("roomPaxCapacity").InnerText != "")
                                    {
                                        objRoomType[k].RoomCapacityInfo.roomPaxCapacity = Convert.ToInt32(ndRoomCapacityInfoList.SelectSingleNode("roomPaxCapacity").InnerText, CultureInfos);
                                    }
                                    else
                                    {
                                        objRoomType[k].RoomCapacityInfo.roomPaxCapacity = 0;
                                    }
                                }
                                if (ndRoomCapacityInfoList.SelectSingleNode("allowedAdultsWithoutChildren") != null)
                                {
                                    if (ndRoomCapacityInfoList.SelectSingleNode("allowedAdultsWithoutChildren").InnerText != "")
                                    {
                                        objRoomType[k].RoomCapacityInfo.allowedAdultsWithoutChildren = Convert.ToInt32(ndRoomCapacityInfoList.SelectSingleNode("allowedAdultsWithoutChildren").InnerText, CultureInfos);
                                    }
                                    else
                                    {
                                        objRoomType[k].RoomCapacityInfo.allowedAdultsWithoutChildren = 0;
                                    }
                                }
                                if (ndRoomCapacityInfoList.SelectSingleNode("allowedAdultsWithChildren") != null)
                                {
                                    if (ndRoomCapacityInfoList.SelectSingleNode("allowedAdultsWithChildren").InnerText != "")
                                    {
                                        objRoomType[k].RoomCapacityInfo.allowedAdultsWithChildren = Convert.ToInt32(ndRoomCapacityInfoList.SelectSingleNode("allowedAdultsWithChildren").InnerText, CultureInfos);
                                    }
                                    else
                                    {
                                        objRoomType[k].RoomCapacityInfo.allowedAdultsWithChildren = 0;
                                    }
                                }
                                if (ndRoomCapacityInfoList.SelectSingleNode("maxExtraBed") != null)
                                {
                                    if (ndRoomCapacityInfoList.SelectSingleNode("maxExtraBed").InnerText != "")
                                    {
                                        objRoomType[k].RoomCapacityInfo.maxExtraBed = Convert.ToInt32(ndRoomCapacityInfoList.SelectSingleNode("maxExtraBed").InnerText, CultureInfos);
                                    }
                                    else
                                    {
                                        objRoomType[k].RoomCapacityInfo.maxExtraBed = 0;
                                    }
                                }
                            }

                            ////



                            
                            XmlNode ndspecials = ndRoomType[k].SelectSingleNode("specials");
                            if (Convert.ToInt32(ndspecials.Attributes.GetNamedItem("count").Value, CultureInfos) > 0)
                            {
                                XmlNodeList ndspecial = ndRoomType[k].SelectNodes("specials/special");
                                special[] objspecial = new special[ndspecial.Count];
                                for (int l = 0; l < ndspecial.Count; l++)
                                {
                                    objspecial[l] = new special();
                                    objspecial[l].runno = l;
                                    if (ndspecial[l].SelectSingleNode("type") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("type").InnerText != "")
                                        {
                                            objspecial[l].type = ndspecial[l].SelectSingleNode("type").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("specialName") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("specialName").InnerText != "")
                                        {
                                            objspecial[l].specialName = ndspecial[l].SelectSingleNode("specialName").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("upgradeToRoomId") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("upgradeToRoomId").InnerText != "")
                                        {
                                            objspecial[l].upgradeToRoomId = ndspecial[l].SelectSingleNode("upgradeToRoomId").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("upgradeToMealId") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("upgradeToMealId").InnerText != "")
                                        {
                                            objspecial[l].upgradeToMealId = ndspecial[l].SelectSingleNode("upgradeToMealId").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("condition") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("condition").InnerText != "")
                                        {
                                            objspecial[l].condition = ndspecial[l].SelectSingleNode("condition").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("description") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("description").InnerText != "")
                                        {
                                            objspecial[l].description = ndspecial[l].SelectSingleNode("description").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("notes") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("notes").InnerText != "")
                                        {
                                            objspecial[l].notes = ndspecial[l].SelectSingleNode("notes").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("stay") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("stay").InnerText != "")
                                        {
                                            objspecial[l].stay = ndspecial[l].SelectSingleNode("stay").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("discount") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("discount").InnerText != "")
                                        {
                                            objspecial[l].discount = ndspecial[l].SelectSingleNode("discount").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("discountedNights") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("discountedNights").InnerText != "")
                                        {
                                            objspecial[l].discountedNights = ndspecial[l].SelectSingleNode("discountedNights").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("pay") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("pay").InnerText != "")
                                        {
                                            objspecial[l].pay = ndspecial[l].SelectSingleNode("pay").InnerText;
                                        }
                                    }
                                    objRoomType[k].special.Add(objspecial[l]);
                                }
                            }
                            XmlNode ndrateBasis = ndRoomType[k].SelectSingleNode("rateBases");
                            if (Convert.ToInt32(ndrateBasis.Attributes.GetNamedItem("count").Value, CultureInfos) > 0)
                            {
                                XmlNodeList nrateBasis = ndRoomType[k].SelectNodes("rateBases/rateBasis");
                                rateBasis[] objrateBasis = new rateBasis[nrateBasis.Count];
                                for (int l = 0; l < nrateBasis.Count; l++)
                                {
                                    objrateBasis[l] = new rateBasis();
                                    objrateBasis[l].RoomDescription = nrateBasis[l].Attributes.GetNamedItem("description").Value;
                                    objrateBasis[l].RoomDescriptionId = nrateBasis[l].Attributes.GetNamedItem("id").Value;
                                    objrateBasis[l].RoomRateType = nrateBasis[l].SelectSingleNode("rateType").InnerText;
                                    objrateBasis[l].RoomRateTypeCurrency = nrateBasis[l].SelectSingleNode("rateType").Attributes.GetNamedItem("currencyshort").Value;
                                    objrateBasis[l].RoomRateTypeCurrencyId = Convert.ToInt64(nrateBasis[l].SelectSingleNode("rateType").Attributes.GetNamedItem("currencyid").Value, CultureInfos);
                                    objrateBasis[l].RoomAllocationDetails = nrateBasis[l].SelectSingleNode("allocationDetails").InnerText;
                                    objrateBasis[l].minStay = nrateBasis[l].SelectSingleNode("minStay").InnerText;
                                    objrateBasis[l].dateApplyMinStay = nrateBasis[l].SelectSingleNode("dateApplyMinStay").InnerText;
                                    objrateBasis[l].status = nrateBasis[l].SelectSingleNode("status").InnerText;
                                    objrateBasis[l].passengerNamesRequiredForBooking = Convert.ToInt32(nrateBasis[l].SelectSingleNode("passengerNamesRequiredForBooking").InnerText, CultureInfos);
                                    if (nrateBasis[l].SelectSingleNode("validForOccupancy") != null)
                                    {
                                        objrateBasis[l].validForOccupancy = new validForOccupancy();
                                        if (nrateBasis[l].SelectSingleNode("validForOccupancy/adults") != null)
                                        {
                                            objrateBasis[l].validForOccupancy.adults = Convert.ToInt32(nrateBasis[l].SelectSingleNode("validForOccupancy/adults").InnerText, CultureInfos);
                                        }
                                        else
                                        {
                                            objrateBasis[l].validForOccupancy.adults = 0;
                                        }
                                        if (nrateBasis[l].SelectSingleNode("validForOccupancy/children") != null)
                                        {
                                            objrateBasis[l].validForOccupancy.children = Convert.ToInt32(nrateBasis[l].SelectSingleNode("validForOccupancy/children").InnerText, CultureInfos);
                                        }
                                        else
                                        {
                                            objrateBasis[l].validForOccupancy.children = 0;
                                        }
                                        if (nrateBasis[l].SelectSingleNode("validForOccupancy/childrenAges") != null)
                                        {
                                            objrateBasis[l].validForOccupancy.childrenAges = nrateBasis[l].SelectSingleNode("validForOccupancy/childrenAges").InnerText;
                                        }
                                        else
                                        {
                                            objrateBasis[l].validForOccupancy.childrenAges = "";
                                        }
                                        if (nrateBasis[l].SelectSingleNode("validForOccupancy/extraBed") != null)
                                        {
                                            objrateBasis[l].validForOccupancy.extraBed = Convert.ToInt32(nrateBasis[l].SelectSingleNode("validForOccupancy/extraBed").InnerText, CultureInfos);
                                        }
                                        else
                                        {
                                            objrateBasis[l].validForOccupancy.adults = 0;
                                        }
                                        if (nrateBasis[l].SelectSingleNode("validForOccupancy/extraBedOccupant") != null)
                                        {
                                            objrateBasis[l].validForOccupancy.extraBedOccupant = nrateBasis[l].SelectSingleNode("validForOccupancy/extraBedOccupant").InnerText;
                                        }
                                        else
                                        {
                                            objrateBasis[l].validForOccupancy.extraBedOccupant = "";
                                        }
                                        //objrateBasis[l].validForOccupancy.adults = Convert.ToInt32(nrateBasis[l].SelectSingleNode("validForOccupancy/adults").InnerText);
                                        //objrateBasis[l].validForOccupancy.children = Convert.ToInt32(nrateBasis[l].SelectSingleNode("validForOccupancy/children").InnerText);
                                        //objrateBasis[l].validForOccupancy.childrenAges = nrateBasis[l].SelectSingleNode("validForOccupancy/childrenAges").InnerText;
                                        //objrateBasis[l].validForOccupancy.extraBed = Convert.ToInt32(nrateBasis[l].SelectSingleNode("validForOccupancy/extraBed").InnerText);
                                        //objrateBasis[l].validForOccupancy.extraBedOccupant = nrateBasis[l].SelectSingleNode("validForOccupancy/extraBedOccupant").InnerText;
                                    }
                                    if (nrateBasis[l].SelectSingleNode("changedOccupancy") != null)
                                    {
                                        objrateBasis[l].changedOccupancy = nrateBasis[l].SelectSingleNode("changedOccupancy").InnerText;
                                    }

                                    objrateBasis[l].cancellationRules = new List<cancellationRules>();

                                    if (nrateBasis[l].SelectSingleNode("tariffNotes") != null)
                                    {
                                        objrateBasis[l].tariffNotes = nrateBasis[l].SelectSingleNode("tariffNotes").InnerText;
                                    }

                                    GlobalDefault objGlobalDefault = (GlobalDefault)Context.Session["LoginUser"];
                                    AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
                                    if (Context.Session["AgentDetailsOnAdmin"] != null)
                                    {
                                        objAgentDetailsOnAdmin = (AgentDetailsOnAdmin)Context.Session["AgentDetailsOnAdmin"];
                                    }
                                    string DoTWcurrency;
                                    if (Context.Session["AgentDetailsOnAdmin"] != null)
                                    {
                                        DoTWcurrency = objAgentDetailsOnAdmin.Currency;
                                    }
                                    else
                                    {
                                        DoTWcurrency = objGlobalDefault.Currency;
                                    }
                                   DataLayer.ExchangeRateManager.GetForeignExchangeRate(DoTWcurrency, out dtResult);
                                    float ExchangeRate = 0;
                                    if (DoTWcurrency == "INR")
                                    {
                                        ExchangeRate = 1;
                                    }
                                    else
                                    {
                                        //ExchangeRate = (float)Convert.ToDecimal(dtResult.Rows[0]["INR"].ToString(), CultureInfos);
                                        ExchangeRate = objGlobalDefault.ExchangeRate.Single(data => data.Currency == DoTWcurrency).Rate;
                                    }
                                    objrateBasis[l].LeftToSell = Convert.ToInt32(nrateBasis[l].SelectSingleNode("leftToSell").InnerText, CultureInfos) + 1;
                                    objrateBasis[l].Total = Convert.ToSingle(nrateBasis[l].SelectSingleNode("total/formatted").InnerText, CultureInfos) * dolarRate;
                                    //objrateBasis[l].Total = Convert.ToSingle(nrateBasis[l].SelectSingleNode("total/formatted").InnerText, CultureInfos);
                                    BaseAmt = objrateBasis[l].Total / ExchangeRate;
                                    CutRoomAmountAmt = 0;
                                    CutGlobalMarkupAmt = 0;
                                    CutGlobalMarkupPer = 0;
                                    ActualGlobalMarkupAmt = 0;
                                    CutGroupMarkupAmt = 0;
                                    CutGroupMarkupPer = 0;
                                    ActualGroupMarkupAmt = 0;
                                    CutIndividualMarkupAmt = 0;
                                    CutIndividualMarkupPer = 0;
                                    ActualIndividualMarkupAmt = 0;
                                    ActualAgentRoomMarkup = 0;
                                    CutRoomAmount = 0;
                                    AgentRoomMarkup = 0;
                                    AgentOwnMarkupPer = 0;
                                    AgentOwnMarkupAmt = 0;
                                    MarkupTaxManager.objMarkupsAndTaxes = objMarkupsAndTaxes;
                                    CutRoomAmount = (float)Math.Round(Convert.ToSingle(MarkupTaxManager.GetCutRoomAmount(BaseAmt, DoTWcurrency, NoofRooms, Night, "Dotw", out AgentRoomMarkup)), 2);
                                    AgentRoomMarkup = (float)Math.Round(Convert.ToSingle(AgentRoomMarkup), 2);
                                    if (nrateBasis[l].SelectSingleNode("specialsApplied") != null)
                                    {
                                        objrateBasis[l].specialsApplied = Convert.ToInt32(nrateBasis[l].SelectSingleNode("specialsApplied/special").InnerText, CultureInfos);
                                    }
                                    else
                                    {
                                        objrateBasis[l].specialsApplied = -1;
                                    }
                                    objrateBasis[l].CUTPrice = CutRoomAmount;
                                    objrateBasis[l].AgentMarkup = AgentRoomMarkup;
                                    XmlNode ncancellation = nrateBasis[l].SelectSingleNode("cancellationRules");
                                    Int32 ncancellationRules = Convert.ToInt32(ncancellation.Attributes.GetNamedItem("count").Value, CultureInfos);
                                    XmlNodeList ndcancellationRules = nrateBasis[l].SelectNodes("cancellationRules/rule");
                                    cancellationRules[] objcancellationRules = new cancellationRules[ncancellationRules];
                                    for (int m = 0; m < ncancellationRules; m++)
                                    {
                                        objcancellationRules[m] = new cancellationRules();
                                        if (ndcancellationRules[m].SelectSingleNode("noShowPolicy") == null && ndcancellationRules[m].SelectSingleNode("fromDate") == null)
                                        {
                                            objcancellationRules[m] = null;
                                        }
                                        else if (ndcancellationRules[m].SelectSingleNode("noShowPolicy") != null && ndcancellationRules[m].SelectSingleNode("fromDate") == null)
                                        {
                                            objcancellationRules[m] = null;
                                        }
                                        else if (ndcancellationRules[m].SelectSingleNode("noShowPolicy") != null && ndcancellationRules[m].SelectSingleNode("fromDate") != null)
                                        {
                                            objcancellationRules[m] = null;
                                        }
                                        else
                                        {
                                            if (ndcancellationRules[m].SelectSingleNode("amendRestricted") != null && ndcancellationRules[m].SelectSingleNode("cancelRestricted") != null)
                                            {
                                                if (ndcancellationRules[m].SelectSingleNode("amendRestricted").InnerText == "true" && ndcancellationRules[m].SelectSingleNode("cancelRestricted").InnerText == "true")
                                                {
                                                    objcancellationRules[m].fromDate = ndcancellationRules[m].SelectSingleNode("fromDate").InnerText;
                                                    objcancellationRules[m].cancelCharge = BaseAmt;
                                                    objcancellationRules[m].CUTcancelCharge = objrateBasis[l].CUTPrice;
                                                    objcancellationRules[m].AgentMarkup = objrateBasis[l].AgentMarkup;
                                                    objcancellationRules[m].AmendRestricted = true;
                                                    objcancellationRules[m].CancelRestricted = true;
                                                }
                                                else if (ndcancellationRules[m].SelectSingleNode("amendRestricted").InnerText == "false" && ndcancellationRules[m].SelectSingleNode("cancelRestricted").InnerText == "true")
                                                {
                                                    objcancellationRules[m].fromDate = ndcancellationRules[m].SelectSingleNode("fromDate").InnerText;
                                                    objcancellationRules[m].cancelCharge = BaseAmt;
                                                    objcancellationRules[m].CUTcancelCharge = objrateBasis[l].CUTPrice;
                                                    objcancellationRules[m].AgentMarkup = objrateBasis[l].AgentMarkup;
                                                    objcancellationRules[m].AmendRestricted = false;
                                                    objcancellationRules[m].CancelRestricted = true;
                                                }
                                                else
                                                {
                                                    objcancellationRules[m] = null;
                                                }
                                            }
                                            else if (ndcancellationRules[m].SelectSingleNode("amendRestricted") == null && ndcancellationRules[m].SelectSingleNode("cancelRestricted") != null)
                                            {
                                                if (ndcancellationRules[m].SelectSingleNode("cancelRestricted").InnerText == "true")
                                                {
                                                    objcancellationRules[m].fromDate = ndcancellationRules[m].SelectSingleNode("fromDate").InnerText;
                                                    objcancellationRules[m].cancelCharge = BaseAmt;
                                                    objcancellationRules[m].CUTcancelCharge = objrateBasis[l].CUTPrice;
                                                    objcancellationRules[m].AgentMarkup = objrateBasis[l].AgentMarkup;
                                                    objcancellationRules[m].AmendRestricted = false;
                                                    objcancellationRules[m].CancelRestricted = true;
                                                }
                                                else if (ndcancellationRules[m].SelectSingleNode("cancelRestricted").InnerText == "false")
                                                {
                                                    objcancellationRules[m].fromDate = ndcancellationRules[m].SelectSingleNode("fromDate").InnerText;
                                                    objcancellationRules[m].cancelCharge = Convert.ToSingle(ndcancellationRules[m].SelectSingleNode("cancelCharge/formatted").InnerText, CultureInfos) * dolarRate;

                                                    CutRoomAmountAmt = 0;

                                                    CutGlobalMarkupAmt = 0;
                                                    CutGlobalMarkupPer = 0;
                                                    ActualGlobalMarkupAmt = 0;

                                                    CutGroupMarkupAmt = 0;
                                                    CutGroupMarkupPer = 0;
                                                    ActualGroupMarkupAmt = 0;

                                                    CutIndividualMarkupAmt = 0;
                                                    CutIndividualMarkupPer = 0;
                                                    ActualIndividualMarkupAmt = 0;

                                                    ActualAgentRoomMarkup = 0;
                                                    CutRoomAmount = 0;
                                                    AgentRoomMarkup = 0;
                                                    AgentOwnMarkupPer = 0;
                                                    AgentOwnMarkupAmt = 0;

                                                    //BaseAmt = objcancellationRules[m].cancelCharge;
                                                    BaseAmt = objcancellationRules[m].cancelCharge / ExchangeRate;
                                                    CutRoomAmount = MarkupTaxManager.GetCutRoomAmount(BaseAmt, DoTWcurrency, NoofRooms, Night, "Dotw", out AgentRoomMarkup);
                                                    objcancellationRules[m].CUTcancelCharge = CutRoomAmount;
                                                    objcancellationRules[m].AgentMarkup = AgentRoomMarkup;
                                                    objcancellationRules[m].AmendRestricted = false;
                                                    objcancellationRules[m].CancelRestricted = false;
                                                }
                                                else
                                                {
                                                    objcancellationRules[m] = null;
                                                }
                                            }
                                            else
                                            {
                                                objcancellationRules[m].fromDate = ndcancellationRules[m].SelectSingleNode("fromDate").InnerText;
                                                //objcancellationRules[m].cancelCharge = Convert.ToSingle(ndcancellationRules[m].SelectSingleNode("cancelCharge/formatted").InnerText, CultureInfos) * dolarRate;
                                                objcancellationRules[m].cancelCharge = Convert.ToSingle(ndcancellationRules[m].SelectSingleNode("charge/formatted").InnerText, CultureInfos) * dolarRate;
                                                CutRoomAmountAmt = 0;
                                                CutGlobalMarkupAmt = 0;
                                                CutGlobalMarkupPer = 0;
                                                ActualGlobalMarkupAmt = 0;
                                                CutGroupMarkupAmt = 0;
                                                CutGroupMarkupPer = 0;
                                                ActualGroupMarkupAmt = 0;
                                                CutIndividualMarkupAmt = 0;
                                                CutIndividualMarkupPer = 0;
                                                ActualIndividualMarkupAmt = 0;
                                                ActualAgentRoomMarkup = 0;
                                                CutRoomAmount = 0;
                                                AgentRoomMarkup = 0;
                                                AgentOwnMarkupPer = 0;
                                                AgentOwnMarkupAmt = 0;
                                                BaseAmt = objcancellationRules[m].cancelCharge / ExchangeRate;
                                                CutRoomAmount = (float)Math.Round(Convert.ToSingle(MarkupTaxManager.GetCutRoomAmount(BaseAmt, DoTWcurrency, NoofRooms, Night, "Dotw", out AgentRoomMarkup)), 2);
                                                AgentRoomMarkup = (float)Math.Round(Convert.ToSingle(ActualAgentRoomMarkup), 2);
                                                objcancellationRules[m].CUTcancelCharge = CutRoomAmount;
                                                objcancellationRules[m].AgentMarkup = AgentRoomMarkup;
                                                objcancellationRules[m].AmendRestricted = false;
                                                objcancellationRules[m].CancelRestricted = false;
                                            }
                                        }
                                        objrateBasis[l].cancellationRules.Add(objcancellationRules[m]);
                                    }
                                    objRoomType[k].rateBasis.Add(objrateBasis[l]);
                                }
                            }
                            objRoomType[k].ChildrenAges = ndRoom[j].Attributes.GetNamedItem("childrenages").Value;
                            objRoomType[k].Children = Convert.ToInt64(ndRoom[j].Attributes.GetNamedItem("children").Value, CultureInfos);
                            objRoomType[k].Adults = Convert.ToInt64(ndRoom[j].Attributes.GetNamedItem("adults").Value, CultureInfos);
                            objRoomType[k].roomNumber = j + 1;
                            objRoomType[k].RoomTypeCode = Convert.ToInt32(ndRoomType[k].Attributes.GetNamedItem("roomtypecode").Value, CultureInfos);
                            foreach (DOTWLib.Response.Room Room in objDOTWHotelDetails.Room)
                            {
                                foreach (DOTWLib.Response.RoomType RoomType in Room.RoomType)
                                {
                                    if (RoomType.RoomTypeCode == objRoomType[k].RoomTypeCode)
                                    {
                                        objRoomType[k].RoomAmenities = RoomType.RoomAmenities;
                                        objRoomType[k].RoomCapacityInfo = RoomType.RoomCapacityInfo;
                                        //objRoomType[k].RoomInfo = RoomType.RoomInfo;
                                    }
                                }
                            }
                            objRoom[j].RoomType.Add(objRoomType[k]);
                        }
                        lstRoom_Final.Add(objRoom[j]);
                    }
                    lstRoom_Final = GetRoomOccupancy(lstRoom_Final, Occupancy);
                    if (lstRoom_Final.Count != 0)
                    {
                        var Room_Final = lstRoom_Final.OrderBy(data => data.CUTRoomPrice).FirstOrDefault();
                        objDOTWHotelDetails.RoomPrice = (float)Math.Round(Convert.ToSingle(Room_Final.RoomPrice), 2);
                        objDOTWHotelDetails.CUTPrice = (float)Math.Round(Convert.ToSingle(Room_Final.CUTRoomPrice), 2);
                        objDOTWHotelDetails.AgentMarkup = (float)Math.Round(Convert.ToSingle(Room_Final.AgentMarkup), 2);
                        objDOTWHotelDetails.Room = lstRoom_Final;
                    }
                    else
                    {
                        objDOTWHotelDetails = null;
                    }
                    //}
                    ErrorMessage = "";
                    return objDOTWHotelDetails;
                }
                else
                {
                    ErrorMessage = "";
                    return objDOTWHotelDetails = null;
                }
            }
            else
            {
                XmlNode ndResponseStatus_Err = doc.SelectSingleNode("result/request/error/details");
                ErrorMessage = ndResponseStatus_Err.InnerText;
                return objDOTWHotelDetails = null;
            }
        }
        #endregion Parseprice


        #region Parsepricexml
        //public bool ParsePriceXML(string xmlPriceString, Int64 Night, Int64 NoofRooms)
        public bool ParsePriceXML(string xmlPriceString, Int64 Night, Int64 NoofRooms, List<DOTWLib.Request.Occupancy> Occupancy)
        {


            DBHelper.DBReturnCode retCode;
            DataTable dtResult;
            XmlDocument doc = new XmlDocument();
            //Int64 NoofRooms = Occupancy.Count;
            bool nStatus;
            float BaseAmt;

            ////.................test................//
            //float GlobalMarkupAmtS = 0;
            //float GlobalMarkupPerS = 0;
            //float GlobalCommAmtS = 0;
            //float GlobalCommPerS = 0;

            //float GroupMarkupAmtS = 0;
            //float GroupMarkupPerS = 0;
            //float GroupCommAmtS = 0;
            //float GroupCommPerS = 0;

            //float IndividualMarkupAmtS = 0;
            //float IndividualMarkupPerS = 0;
            //float IndividualCommAmtS = 0;
            //float IndividualCommPerS = 0;

            //float AgentOwnMarkupPerS = 0;
            //float AgentOwnMarkupAmtS = 0;

            //float PerServiceTaxS = 0;
            //float TimeGapS = 0;

            //DataSet ds;
            //DBHelper.DBReturnCode retCode1 = DefaultManager.GetMarkupsAndTaxesforDoTW(out ds);
            //if (DBHelper.DBReturnCode.SUCCESS == retCode1)
            //{
            //    DataTable dtIndividualMarkup, dtGroupMarkup, dtGlobalMarkup, dtAgentOwnMarkp, dtServiceTax;
            //    dtIndividualMarkup = ds.Tables[2];
            //    dtGroupMarkup = ds.Tables[1];
            //    dtGlobalMarkup = ds.Tables[0];
            //    dtAgentOwnMarkp = ds.Tables[3];
            //    dtServiceTax = ds.Tables[4];
            //    MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();
            //    if (dtIndividualMarkup.Rows.Count != 0)
            //    {
            //        IndividualMarkupPerS = Convert.ToSingle(dtIndividualMarkup.Rows[0]["MarkupPercentage"]);
            //        IndividualMarkupAmtS = Convert.ToSingle(dtIndividualMarkup.Rows[0]["MarkupAmmount"]);
            //        IndividualCommPerS = Convert.ToSingle(dtIndividualMarkup.Rows[0]["CommessionPercentage"]);
            //        IndividualCommAmtS = Convert.ToSingle(dtIndividualMarkup.Rows[0]["CommessionAmmount"]);
            //    }
            //    if (dtGroupMarkup.Rows.Count != 0)
            //    {
            //        GroupMarkupPerS = Convert.ToSingle(dtGroupMarkup.Rows[0]["MarkupPercentage"]);
            //        GroupMarkupAmtS = Convert.ToSingle(dtGroupMarkup.Rows[0]["MarkupAmmount"]);
            //        GroupCommPerS = Convert.ToSingle(dtGroupMarkup.Rows[0]["CommessionPercentage"]);
            //        GroupCommAmtS = Convert.ToSingle(dtGroupMarkup.Rows[0]["CommessionAmmount"]);
            //    }
            //    if (dtGlobalMarkup.Rows.Count != 0)
            //    {
            //        GlobalMarkupPerS = Convert.ToSingle(dtGlobalMarkup.Rows[0]["MarkupPercentage"]);
            //        GlobalMarkupAmtS = Convert.ToSingle(dtGlobalMarkup.Rows[0]["MarkupAmmount"]);
            //        GlobalCommPerS = Convert.ToSingle(dtGlobalMarkup.Rows[0]["CommessionPercentage"]);
            //        GlobalCommAmtS = Convert.ToSingle(dtGlobalMarkup.Rows[0]["CommessionAmmount"]);
            //    }
            //    if (dtAgentOwnMarkp.Rows.Count != 0)
            //    {
            //        AgentOwnMarkupPerS = Convert.ToSingle(dtAgentOwnMarkp.Rows[0]["Percentage"]);
            //        AgentOwnMarkupAmtS = Convert.ToSingle(dtAgentOwnMarkp.Rows[0]["Amount"]);
            //    }
            //    if (dtServiceTax.Rows.Count != 0)
            //    {
            //        PerServiceTaxS = Convert.ToSingle(dtServiceTax.Rows[0]["ServiceTax"]);
            //        TimeGapS = Convert.ToInt32(dtServiceTax.Rows[0]["TimeGap"]);
            //    }
            //}
            ////.................test end............//


            float CutRoomAmountAmt = 0;

            float CutGlobalMarkupAmt = 0;
            float CutGlobalMarkupPer = 0;
            float ActualGlobalMarkupAmt = 0;

            float CutGroupMarkupAmt = 0;
            float CutGroupMarkupPer = 0;
            float ActualGroupMarkupAmt = 0;

            float CutIndividualMarkupAmt = 0;
            float CutIndividualMarkupPer = 0;
            float ActualIndividualMarkupAmt = 0;

            float ActualAgentRoomMarkup = 0;
            float CutRoomAmount = 0;
            float AgentRoomMarkup = 0;
            float AgentOwnMarkupPer = 0;
            float AgentOwnMarkupAmt = 0;

            string currency = "";
            int nNumberOfHotels = 0;

            float GlobalMarkup = 0;
            float GroupMarkup = 0;
            float IndividualMarkup = 0;


            doc.LoadXml(xmlPriceString);
            XmlNode ndResponseStatus = doc.SelectSingleNode("result/successful");
            currency = doc.SelectSingleNode("result/currencyShort").InnerText;
            ExchangeRateManager.GetForeignExchangeRate(currency, out dtResult);
            float dolarRate = (float)Convert.ToDecimal(dtResult.Rows[0]["INR"].ToString(), CultureInfos);
            nStatus = Convert.ToBoolean(ndResponseStatus.InnerText);
            if (nStatus == true)
            {
                XmlNode ndHotels = doc.SelectSingleNode("result/hotels");
                nNumberOfHotels = Convert.ToInt32(ndHotels.Attributes.GetNamedItem("count").Value);
                Context.Session["DoTWNumberOfHotelsPrice"] = nNumberOfHotels;
                List<DOTWHotelDetails> DOTWHotelDetails = new List<DOTWHotelDetails>();
                DOTWHotelDetails[] objDOTWHotelDetails = new DOTWHotelDetails[nNumberOfHotels];
                MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();
                if (Context.Session["markups"] != null)
                    objMarkupsAndTaxes = (MarkupsAndTaxes)Context.Session["markups"];
                else if (Context.Session["AgentMarkupsOnAdmin"] != null)
                    objMarkupsAndTaxes = (MarkupsAndTaxes)Context.Session["AgentMarkupsOnAdmin"];


                for (int i = 0; i < nNumberOfHotels; i++)
                {
                    objDOTWHotelDetails[i] = new DOTWHotelDetails();

                    //XmlNode ndHotel = doc.SelectSingleNode("result/hotels/hotel").ChildNodes[i];
                    XmlNode ndHotel = doc.SelectSingleNode("result/hotels").ChildNodes[i];
                    objDOTWHotelDetails[i].HotelId = Convert.ToInt64(ndHotel.Attributes.GetNamedItem("hotelid").Value);
                    //objDOTWHotelDetails[i].RoomPrice = Convert.ToSingle(ndHotel.SelectSingleNode("from/formatted").InnerText) * dolarRate;
                    //objDOTWHotelDetails[i].RoomType = new List<RoomType>();
                    objDOTWHotelDetails[i].Room = new List<Room>();

                    XmlNodeList ndRoom = ndHotel.SelectNodes("rooms/room");
                    Room[] objRoom = new Room[ndRoom.Count];
                    for (int j = 0; j < ndRoom.Count; j++)
                    {
                        objRoom[j] = new Room();
                        objRoom[j].Count = Convert.ToInt64(ndRoom[j].Attributes.GetNamedItem("count").Value);
                        //objRoom[j].ChildrenAges = ndRoom[j].Attributes.GetNamedItem("childrenages").Value;
                        //objRoom[j].Children = Convert.ToInt64(ndRoom[j].Attributes.GetNamedItem("children").Value);
                        //objRoom[j].Adults = Convert.ToInt64(ndRoom[j].Attributes.GetNamedItem("adults").Value);
                        objRoom[j].RoomType = new List<RoomType>();

                        XmlNodeList ndRoomType = ndRoom[j].SelectNodes("roomType");

                        RoomType[] objRoomType = new RoomType[ndRoomType.Count];
                        for (int k = 0; k < ndRoomType.Count; k++)
                        {
                            objRoomType[k] = new RoomType();

                            objRoomType[k].RoomTypeName = ndRoomType[k].SelectSingleNode("name").InnerText;
                            objRoomType[k].special = new List<special>();
                            objRoomType[k].rateBasis = new List<rateBasis>();

                            objRoomType[k].RoomInfo = new RoomInfo();
                            objRoomType[k].RoomInfo.maxOccupancy = Convert.ToInt64(ndRoomType[k].SelectSingleNode("roomInfo/maxOccupancy").InnerText);

                            XmlNode ndspecials = ndRoomType[k].SelectSingleNode("specials");
                            if (Convert.ToInt32(ndspecials.Attributes.GetNamedItem("count").Value) > 0)
                            {
                                XmlNodeList ndspecial = ndRoomType[k].SelectNodes("specials/special");
                                special[] objspecial = new special[ndspecial.Count];
                                for (int l = 0; l < ndspecial.Count; l++)
                                {
                                    objspecial[l] = new special();
                                    objspecial[l].runno = l;
                                    if (ndspecial[l].SelectSingleNode("type") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("type").InnerText != "")
                                        {
                                            objspecial[l].type = ndspecial[l].SelectSingleNode("type").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("specialName") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("specialName").InnerText != "")
                                        {
                                            objspecial[l].specialName = ndspecial[l].SelectSingleNode("specialName").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("upgradeToRoomId") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("upgradeToRoomId").InnerText != "")
                                        {
                                            objspecial[l].upgradeToRoomId = ndspecial[l].SelectSingleNode("upgradeToRoomId").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("upgradeToMealId") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("upgradeToMealId").InnerText != "")
                                        {
                                            objspecial[l].upgradeToMealId = ndspecial[l].SelectSingleNode("upgradeToMealId").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("condition") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("condition").InnerText != "")
                                        {
                                            objspecial[l].condition = ndspecial[l].SelectSingleNode("condition").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("description") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("description").InnerText != "")
                                        {
                                            objspecial[l].description = ndspecial[l].SelectSingleNode("description").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("notes") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("notes").InnerText != "")
                                        {
                                            objspecial[l].notes = ndspecial[l].SelectSingleNode("notes").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("stay") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("stay").InnerText != "")
                                        {
                                            objspecial[l].stay = ndspecial[l].SelectSingleNode("stay").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("discount") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("discount").InnerText != "")
                                        {
                                            objspecial[l].discount = ndspecial[l].SelectSingleNode("discount").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("discountedNights") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("discountedNights").InnerText != "")
                                        {
                                            objspecial[l].discountedNights = ndspecial[l].SelectSingleNode("discountedNights").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("pay") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("pay").InnerText != "")
                                        {
                                            objspecial[l].pay = ndspecial[l].SelectSingleNode("pay").InnerText;
                                        }
                                    }
                                    objRoomType[k].special.Add(objspecial[l]);
                                }
                            }
                            XmlNode ndrateBasis = ndRoomType[k].SelectSingleNode("rateBases");
                            if (Convert.ToInt32(ndrateBasis.Attributes.GetNamedItem("count").Value) > 0)
                            {
                                XmlNodeList nrateBasis = ndRoomType[k].SelectNodes("rateBases/rateBasis");
                                rateBasis[] objrateBasis = new rateBasis[nrateBasis.Count];
                                for (int l = 0; l < nrateBasis.Count; l++)
                                {
                                    objrateBasis[l] = new rateBasis();
                                    //objrateBasis[k].RoomDescription = ndRoomType[k].SelectSingleNode("rateBases/rateBasis").Attributes.GetNamedItem("description").Value;
                                    //objrateBasis[k].RoomDescriptionId = Convert.ToInt64(ndRoomType[k].SelectSingleNode("rateBases/rateBasis").Attributes.GetNamedItem("id").Value);
                                    //objrateBasis[k].RoomRateType = ndRoomType[k].SelectSingleNode("rateBases/rateBasis/rateType").InnerText;
                                    //objrateBasis[k].RoomRateTypeCurrency = ndRoomType[k].SelectSingleNode("rateBases/rateBasis/rateType").Attributes.GetNamedItem("currencyshort").Value;
                                    //objrateBasis[k].RoomRateTypeCurrencyId = Convert.ToInt64(ndRoomType[k].SelectSingleNode("rateBases/rateBasis/rateType").Attributes.GetNamedItem("currencyid").Value);
                                    //objrateBasis[k].RoomAllocationDetails = ndRoomType[k].SelectSingleNode("rateBases/rateBasis/allocationDetails").InnerText;


                                    objrateBasis[l].RoomDescription = nrateBasis[l].Attributes.GetNamedItem("description").Value;
                                    //objrateBasis[l].RoomDescriptionId = Convert.ToInt64(nrateBasis[l].Attributes.GetNamedItem("id").Value);
                                    objrateBasis[l].RoomDescriptionId = nrateBasis[l].Attributes.GetNamedItem("id").Value;
                                    objrateBasis[l].RoomRateType = nrateBasis[l].SelectSingleNode("rateType").InnerText;
                                    objrateBasis[l].RoomRateTypeCurrency = nrateBasis[l].SelectSingleNode("rateType").Attributes.GetNamedItem("currencyshort").Value;
                                    objrateBasis[l].RoomRateTypeCurrencyId = Convert.ToInt64(nrateBasis[l].SelectSingleNode("rateType").Attributes.GetNamedItem("currencyid").Value);
                                    objrateBasis[l].RoomAllocationDetails = nrateBasis[l].SelectSingleNode("allocationDetails").InnerText;
                                    objrateBasis[l].minStay = nrateBasis[l].SelectSingleNode("minStay").InnerText;
                                    objrateBasis[l].dateApplyMinStay = nrateBasis[l].SelectSingleNode("dateApplyMinStay").InnerText;
                                    objrateBasis[l].status = nrateBasis[l].SelectSingleNode("status").InnerText;
                                    objrateBasis[l].passengerNamesRequiredForBooking = Convert.ToInt32(nrateBasis[l].SelectSingleNode("passengerNamesRequiredForBooking").InnerText);
                                    if (nrateBasis[l].SelectSingleNode("validForOccupancy") != null)
                                    {
                                        objrateBasis[l].validForOccupancy = new validForOccupancy();
                                        if (nrateBasis[l].SelectSingleNode("validForOccupancy/adults") != null)
                                        {
                                            objrateBasis[l].validForOccupancy.adults = Convert.ToInt32(nrateBasis[l].SelectSingleNode("validForOccupancy/adults").InnerText);
                                        }
                                        else
                                        {
                                            objrateBasis[l].validForOccupancy.adults = 0;
                                        }
                                        if (nrateBasis[l].SelectSingleNode("validForOccupancy/children") != null)
                                        {
                                            objrateBasis[l].validForOccupancy.children = Convert.ToInt32(nrateBasis[l].SelectSingleNode("validForOccupancy/children").InnerText);
                                        }
                                        else
                                        {
                                            objrateBasis[l].validForOccupancy.children = 0;
                                        }
                                        if (nrateBasis[l].SelectSingleNode("validForOccupancy/childrenAges") != null)
                                        {
                                            objrateBasis[l].validForOccupancy.childrenAges = nrateBasis[l].SelectSingleNode("validForOccupancy/childrenAges").InnerText;
                                        }
                                        else
                                        {
                                            objrateBasis[l].validForOccupancy.childrenAges = "";
                                        }
                                        if (nrateBasis[l].SelectSingleNode("validForOccupancy/extraBed") != null)
                                        {
                                            objrateBasis[l].validForOccupancy.extraBed = Convert.ToInt32(nrateBasis[l].SelectSingleNode("validForOccupancy/extraBed").InnerText);
                                        }
                                        else
                                        {
                                            objrateBasis[l].validForOccupancy.extraBed = 0;
                                        }
                                        if (nrateBasis[l].SelectSingleNode("validForOccupancy/extraBedOccupant") != null)
                                        {
                                            objrateBasis[l].validForOccupancy.extraBedOccupant = nrateBasis[l].SelectSingleNode("validForOccupancy/extraBedOccupant").InnerText;
                                        }
                                        else
                                        {
                                            objrateBasis[l].validForOccupancy.extraBedOccupant = "";
                                        }

                                        //objrateBasis[l].validForOccupancy.children = Convert.ToInt32(nrateBasis[l].SelectSingleNode("validForOccupancy/children").InnerText);
                                        //objrateBasis[l].validForOccupancy.childrenAges = nrateBasis[l].SelectSingleNode("validForOccupancy/childrenAges").InnerText;
                                        //objrateBasis[l].validForOccupancy.extraBed = Convert.ToInt32(nrateBasis[l].SelectSingleNode("validForOccupancy/extraBed").InnerText);
                                        //objrateBasis[l].validForOccupancy.extraBedOccupant = nrateBasis[l].SelectSingleNode("validForOccupancy/extraBedOccupant").InnerText;
                                    }
                                    if (nrateBasis[l].SelectSingleNode("changedOccupancy") != null)
                                    {
                                        objrateBasis[l].changedOccupancy = nrateBasis[l].SelectSingleNode("changedOccupancy").InnerText;
                                    }
                                    objrateBasis[l].cancellationRules = new List<cancellationRules>();

                                    if (nrateBasis[l].SelectSingleNode("tariffNotes") != null)
                                    {
                                        objrateBasis[l].tariffNotes = nrateBasis[l].SelectSingleNode("tariffNotes").InnerText;
                                    }

                                    GlobalDefault objGlobalDefault = (GlobalDefault)Context.Session["LoginUser"];
                                    AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
                                    if (Context.Session["AgentDetailsOnAdmin"] != null)
                                    {
                                        objAgentDetailsOnAdmin = (AgentDetailsOnAdmin)Context.Session["AgentDetailsOnAdmin"];
                                    }
                                    string DoTWcurrency;
                                    if (Context.Session["AgentDetailsOnAdmin"] != null)
                                    {
                                        DoTWcurrency = objAgentDetailsOnAdmin.Currency;
                                    }
                                    else
                                    {                                 
                                            DoTWcurrency = objGlobalDefault.Currency;                                    
                                       
                                    }
                                    //ExchangeRateManager.GetForeignExchangeRate(currency, out dtResult);
                                    //ExchangeRateManager.GetForeignExchangeRate(DoTWcurrency, out dtResult);
                                    float ExchangeRate = 0;
                                    if (DoTWcurrency == "INR")
                                    {
                                        ExchangeRate = 1;
                                    }
                                    else
                                    {
                                        //ExchangeRate = (float)Convert.ToSingle(dtResult.Rows[0]["INR"].ToString(), CultureInfos);
                                        ExchangeRate = objGlobalDefault.ExchangeRate.Single(data => data.Currency == DoTWcurrency).Rate;
                                    }

                                    objrateBasis[l].LeftToSell = Convert.ToInt32(nrateBasis[l].SelectSingleNode("leftToSell").InnerText) + 1;
                                    objrateBasis[l].Total = Convert.ToSingle(nrateBasis[l].SelectSingleNode("total/formatted").InnerText, CultureInfos) * dolarRate;

                                    //BaseAmt = objRoomType[k].Total;
                                    BaseAmt = objrateBasis[l].Total / ExchangeRate;

                                    CutRoomAmountAmt = 0;

                                    CutGlobalMarkupAmt = 0;
                                    CutGlobalMarkupPer = 0;
                                    ActualGlobalMarkupAmt = 0;

                                    CutGroupMarkupAmt = 0;
                                    CutGroupMarkupPer = 0;
                                    ActualGroupMarkupAmt = 0;

                                    CutIndividualMarkupAmt = 0;
                                    CutIndividualMarkupPer = 0;
                                    ActualIndividualMarkupAmt = 0;

                                    ActualAgentRoomMarkup = 0;
                                    CutRoomAmount = 0;
                                    AgentRoomMarkup = 0;
                                    AgentOwnMarkupPer = 0;
                                    AgentOwnMarkupAmt = 0;



                                    if (nrateBasis[l].SelectSingleNode("specialsApplied") != null)
                                    {
                                        objrateBasis[l].specialsApplied = Convert.ToInt32(nrateBasis[l].SelectSingleNode("specialsApplied/special").InnerText);
                                    }
                                    else
                                    {
                                        objrateBasis[l].specialsApplied = -1;
                                    }

                                    MarkupTaxManager.objMarkupsAndTaxes = objMarkupsAndTaxes;
                                    objrateBasis[l].CUTPrice = MarkupTaxManager.GetCutRoomAmount(BaseAmt, DoTWcurrency, NoofRooms, Night, "Dotw", out AgentRoomMarkup);
                                    objrateBasis[l].AgentMarkup = AgentRoomMarkup;

                                    //new place for cancellation policy
                                    XmlNode ncancellation = nrateBasis[l].SelectSingleNode("cancellationRules");
                                    Int32 ncancellationRules = Convert.ToInt32(ncancellation.Attributes.GetNamedItem("count").Value);
                                    XmlNodeList ndcancellationRules = nrateBasis[l].SelectNodes("cancellationRules/rule");
                                    cancellationRules[] objcancellationRules = new cancellationRules[ncancellationRules];
                                    for (int m = 0; m < ncancellationRules; m++)
                                    {
                                        objcancellationRules[m] = new cancellationRules();
                                        if (ndcancellationRules[m].SelectSingleNode("noShowPolicy") == null && ndcancellationRules[m].SelectSingleNode("fromDate") == null)
                                        {
                                            objcancellationRules[m] = null;
                                        }
                                        else if (ndcancellationRules[m].SelectSingleNode("noShowPolicy") != null && ndcancellationRules[m].SelectSingleNode("fromDate") == null)
                                        {
                                            objcancellationRules[m] = null;
                                        }
                                        else if (ndcancellationRules[m].SelectSingleNode("noShowPolicy") != null && ndcancellationRules[m].SelectSingleNode("fromDate") != null)
                                        {
                                            objcancellationRules[m] = null;
                                        }
                                        else
                                        {
                                            if (ndcancellationRules[m].SelectSingleNode("amendRestricted") != null && ndcancellationRules[m].SelectSingleNode("cancelRestricted") != null)
                                            {
                                                if (ndcancellationRules[m].SelectSingleNode("amendRestricted").InnerText == "true" && ndcancellationRules[m].SelectSingleNode("cancelRestricted").InnerText == "true")
                                                {
                                                    objcancellationRules[m].fromDate = ndcancellationRules[m].SelectSingleNode("fromDate").InnerText;
                                                    objcancellationRules[m].cancelCharge = BaseAmt;
                                                    objcancellationRules[m].CUTcancelCharge = objrateBasis[l].CUTPrice;
                                                    objcancellationRules[m].AgentMarkup = objrateBasis[l].AgentMarkup;
                                                    objcancellationRules[m].AmendRestricted = true;
                                                    objcancellationRules[m].CancelRestricted = true;
                                                }
                                                else if (ndcancellationRules[m].SelectSingleNode("amendRestricted").InnerText == "false" && ndcancellationRules[m].SelectSingleNode("cancelRestricted").InnerText == "true")
                                                {
                                                    objcancellationRules[m].fromDate = ndcancellationRules[m].SelectSingleNode("fromDate").InnerText;
                                                    objcancellationRules[m].cancelCharge = BaseAmt;
                                                    objcancellationRules[m].CUTcancelCharge = objrateBasis[l].CUTPrice;
                                                    objcancellationRules[m].AgentMarkup = objrateBasis[l].AgentMarkup;
                                                    objcancellationRules[m].AmendRestricted = false;
                                                    objcancellationRules[m].CancelRestricted = true;
                                                }
                                                else
                                                {
                                                    objcancellationRules[m] = null;
                                                }
                                            }
                                            else if (ndcancellationRules[m].SelectSingleNode("amendRestricted") == null && ndcancellationRules[m].SelectSingleNode("cancelRestricted") != null)
                                            {
                                                if (ndcancellationRules[m].SelectSingleNode("cancelRestricted").InnerText == "true")
                                                {
                                                    objcancellationRules[m].fromDate = ndcancellationRules[m].SelectSingleNode("fromDate").InnerText;
                                                    objcancellationRules[m].cancelCharge = BaseAmt;
                                                    objcancellationRules[m].CUTcancelCharge = objrateBasis[l].CUTPrice;
                                                    objcancellationRules[m].AgentMarkup = objrateBasis[l].AgentMarkup;
                                                    objcancellationRules[m].AmendRestricted = false;
                                                    objcancellationRules[m].CancelRestricted = true;
                                                }
                                                else if (ndcancellationRules[m].SelectSingleNode("cancelRestricted").InnerText == "false")
                                                {
                                                    objcancellationRules[m].fromDate = ndcancellationRules[m].SelectSingleNode("fromDate").InnerText;
                                                    objcancellationRules[m].cancelCharge = Convert.ToSingle(ndcancellationRules[m].SelectSingleNode("cancelCharge/formatted").InnerText, CultureInfos) * dolarRate;

                                                    CutRoomAmountAmt = 0;

                                                    CutGlobalMarkupAmt = 0;
                                                    CutGlobalMarkupPer = 0;
                                                    ActualGlobalMarkupAmt = 0;

                                                    CutGroupMarkupAmt = 0;
                                                    CutGroupMarkupPer = 0;
                                                    ActualGroupMarkupAmt = 0;

                                                    CutIndividualMarkupAmt = 0;
                                                    CutIndividualMarkupPer = 0;
                                                    ActualIndividualMarkupAmt = 0;

                                                    ActualAgentRoomMarkup = 0;
                                                    CutRoomAmount = 0;
                                                    AgentRoomMarkup = 0;
                                                    AgentOwnMarkupPer = 0;
                                                    AgentOwnMarkupAmt = 0;

                                                    //BaseAmt = objcancellationRules[m].cancelCharge;
                                                    BaseAmt = objcancellationRules[m].cancelCharge / ExchangeRate;

                                                    CutRoomAmount = MarkupTaxManager.GetCutRoomAmount(BaseAmt, DoTWcurrency, NoofRooms, Night, "Dotw", out AgentRoomMarkup);
                                                    objcancellationRules[m].CUTcancelCharge = CutRoomAmount;
                                                    objcancellationRules[m].AgentMarkup = AgentRoomMarkup;
                                                    objcancellationRules[m].AmendRestricted = false;
                                                    objcancellationRules[m].CancelRestricted = false;
                                                }
                                                else
                                                {
                                                    objcancellationRules[m] = null;
                                                }
                                            }
                                            else
                                            {
                                                objcancellationRules[m].fromDate = ndcancellationRules[m].SelectSingleNode("fromDate").InnerText;
                                                objcancellationRules[m].cancelCharge = Convert.ToSingle(ndcancellationRules[m].SelectSingleNode("cancelCharge/formatted").InnerText, CultureInfos) * dolarRate;
                                                CutRoomAmountAmt = 0;

                                                CutGlobalMarkupAmt = 0;
                                                CutGlobalMarkupPer = 0;
                                                ActualGlobalMarkupAmt = 0;

                                                CutGroupMarkupAmt = 0;
                                                CutGroupMarkupPer = 0;
                                                ActualGroupMarkupAmt = 0;

                                                CutIndividualMarkupAmt = 0;
                                                CutIndividualMarkupPer = 0;
                                                ActualIndividualMarkupAmt = 0;

                                                ActualAgentRoomMarkup = 0;
                                                CutRoomAmount = 0;
                                                AgentRoomMarkup = 0;
                                                AgentOwnMarkupPer = 0;
                                                AgentOwnMarkupAmt = 0;

                                                //BaseAmt = objcancellationRules[m].cancelCharge;
                                                BaseAmt = objcancellationRules[m].cancelCharge / ExchangeRate;

                                                CutRoomAmount = MarkupTaxManager.GetCutRoomAmount(BaseAmt, DoTWcurrency, NoofRooms, Night, "Dotw", out AgentRoomMarkup);
                                                // CutRoomAmount = (float)Math.Round(Convert.ToSingle((BaseAmt + CutRoomAmountAmt)), 2);
                                                //  AgentRoomMarkup = (float)Math.Round(Convert.ToSingle(ActualAgentRoomMarkup), 2);

                                                objcancellationRules[m].CUTcancelCharge = CutRoomAmount;
                                                objcancellationRules[m].AgentMarkup = AgentRoomMarkup;
                                                objcancellationRules[m].AmendRestricted = false;
                                                objcancellationRules[m].CancelRestricted = false;
                                                //objcancellationRules[m].NoShowPolicy = "false";
                                            }
                                        }


                                        #region commented
                                        //if (ncancellationRules == 1 && l == 0)
                                        ////if (l == 0)
                                        //{
                                        //    //string noshow = ndcancellationRules[m].SelectSingleNode("noShowPolicy").InnerText;
                                        //    if (ndcancellationRules[m].SelectSingleNode("noShowPolicy") == null)
                                        //    {
                                        //        //objcancellationRules[m].fromDate = ndcancellationRules[m].SelectSingleNode("fromDate").InnerText;
                                        //        //objcancellationRules[m].cancelCharge = 0;
                                        //        //objcancellationRules[m].CUTcancelCharge = 0;
                                        //        //objcancellationRules[m].AgentMarkup = 0;
                                        //        //objcancellationRules[m].AmendRestricted = true;
                                        //        //objcancellationRules[m].CancelRestricted = true;
                                        //        objcancellationRules[m].fromDate = ndcancellationRules[m].SelectSingleNode("fromDate").InnerText;
                                        //        objcancellationRules[m].cancelCharge = BaseAmt;
                                        //        objcancellationRules[m].CUTcancelCharge = objRoomType[k].CUTPrice;
                                        //        objcancellationRules[m].AgentMarkup = objRoomType[k].AgentMarkup;
                                        //        objcancellationRules[m].AmendRestricted = true;
                                        //        objcancellationRules[m].CancelRestricted = true;
                                        //        objcancellationRules[m].NoShowPolicy = "false";
                                        //    }
                                        //    else
                                        //    {
                                        //        objcancellationRules[m].fromDate = DateTime.Now.ToString("dd/MM/yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
                                        //        objcancellationRules[m].cancelCharge = BaseAmt;
                                        //        objcancellationRules[m].CUTcancelCharge = objRoomType[k].CUTPrice;
                                        //        objcancellationRules[m].AgentMarkup = objRoomType[k].AgentMarkup;
                                        //        objcancellationRules[m].AmendRestricted = false;
                                        //        objcancellationRules[m].CancelRestricted = false;
                                        //        objcancellationRules[m].NoShowPolicy = "true";
                                        //    }
                                        //}
                                        //if (l == 1)
                                        //{
                                        //    //string noshow = ndcancellationRules[m].SelectSingleNode("noShowPolicy").InnerText;
                                        //    if (ndcancellationRules[m].SelectSingleNode("noShowPolicy") == null)
                                        //    {
                                        //        //objcancellationRules[m] = new cancellationRules();
                                        //        objcancellationRules[m].fromDate = ndcancellationRules[m].SelectSingleNode("fromDate").InnerText;
                                        //        objcancellationRules[m].cancelCharge = Convert.ToSingle(ndcancellationRules[m].SelectSingleNode("cancelCharge/formatted").InnerText) * dolarRate;
                                        //        CutRoomAmountAmt = 0;

                                        //        CutGlobalMarkupAmt = 0;
                                        //        CutGlobalMarkupPer = 0;
                                        //        ActualGlobalMarkupAmt = 0;

                                        //        CutGroupMarkupAmt = 0;
                                        //        CutGroupMarkupPer = 0;
                                        //        ActualGroupMarkupAmt = 0;

                                        //        CutIndividualMarkupAmt = 0;
                                        //        CutIndividualMarkupPer = 0;
                                        //        ActualIndividualMarkupAmt = 0;

                                        //        ActualAgentRoomMarkup = 0;
                                        //        CutRoomAmount = 0;
                                        //        AgentRoomMarkup = 0;
                                        //        AgentOwnMarkupPer = 0;
                                        //        AgentOwnMarkupAmt = 0;

                                        //        BaseAmt = objcancellationRules[m].cancelCharge;

                                        //        // CutGlobalMarkup //
                                        //        CutGlobalMarkupPer = ((objMarkupsAndTaxes.GlobalMarkupPer / 100) * BaseAmt);
                                        //        CutGlobalMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.GlobalMarkAmt;
                                        //        ActualGlobalMarkupAmt = Getgreatervalue(CutGlobalMarkupPer, CutGlobalMarkupAmt);
                                        //        GlobalMarkup = ActualGlobalMarkupAmt + BaseAmt;

                                        //        // CutGroupMarkup //
                                        //        CutGroupMarkupPer = ((objMarkupsAndTaxes.GroupMarkupPer / 100) * GlobalMarkup);
                                        //        CutGroupMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.GroupMarkAmt;
                                        //        ActualGroupMarkupAmt = Getgreatervalue(CutGroupMarkupPer, CutGroupMarkupAmt);
                                        //        GroupMarkup = ActualGroupMarkupAmt + GlobalMarkup;

                                        //        // CutIndividualMarkup //
                                        //        CutIndividualMarkupPer = ((objMarkupsAndTaxes.IndMarkupPer / 100) * GroupMarkup);
                                        //        CutIndividualMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.IndMarkAmt;
                                        //        ActualIndividualMarkupAmt = Getgreatervalue(CutIndividualMarkupPer, CutIndividualMarkupAmt);
                                        //        IndividualMarkup = ActualIndividualMarkupAmt + GroupMarkup;

                                        //        CutRoomAmountAmt = ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt;
                                        //        // Start Agent Markup //

                                        //        AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                                        //        AgentOwnMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.AgentOwnMarkupAmt;
                                        //        ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                                        //        //End Agent Markup // 

                                        //        CutRoomAmount = (BaseAmt + CutRoomAmountAmt);
                                        //        AgentRoomMarkup = ActualAgentRoomMarkup;

                                        //        objcancellationRules[m].CUTcancelCharge = CutRoomAmount;
                                        //        objcancellationRules[m].AgentMarkup = AgentRoomMarkup;
                                        //        //objRoomType[k].cancellationRules.Add(objcancellationRules[m]);
                                        //        //objDOTWHotelDetails[i].cancellationRules.Add(objcancellationRules[m]);
                                        //        objcancellationRules[m].AmendRestricted = false;
                                        //        objcancellationRules[m].CancelRestricted = false;
                                        //        objcancellationRules[m].NoShowPolicy = "false";
                                        //    }
                                        //    else
                                        //    {
                                        //        objcancellationRules[m].fromDate = DateTime.Now.ToString("dd/MM/yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
                                        //        objcancellationRules[m].cancelCharge = BaseAmt;
                                        //        objcancellationRules[m].CUTcancelCharge = objRoomType[k].CUTPrice;
                                        //        objcancellationRules[m].AgentMarkup = objRoomType[k].AgentMarkup;
                                        //        objcancellationRules[m].AmendRestricted = false;
                                        //        objcancellationRules[m].CancelRestricted = false;
                                        //        objcancellationRules[m].NoShowPolicy = "true";
                                        //    }
                                        #endregion commented
                                        objrateBasis[l].cancellationRules.Add(objcancellationRules[m]);
                                        //objRoomType[k].cancellationRules.Add(objcancellationRules[m]);
                                    }

                                    objRoomType[k].rateBasis.Add(objrateBasis[l]);
                                }
                            }

                            objRoomType[k].ChildrenAges = ndRoom[j].Attributes.GetNamedItem("childrenages").Value;
                            objRoomType[k].Children = Convert.ToInt64(ndRoom[j].Attributes.GetNamedItem("children").Value);
                            objRoomType[k].Adults = Convert.ToInt64(ndRoom[j].Attributes.GetNamedItem("adults").Value);



                            objRoomType[k].roomNumber = j + 1;

                            objRoomType[k].RoomTypeCode = Convert.ToInt32(ndRoomType[k].Attributes.GetNamedItem("roomtypecode").Value);

                            //objDOTWHotelDetails[i].RoomType.Add(objRoomType[k]);
                            objRoom[j].RoomType.Add(objRoomType[k]);
                            //....no think ...
                        }

                        objDOTWHotelDetails[i].Room.Add(objRoom[j]);
                    }
                    //.......


                    #region Test Commented

                    //XmlNode nRoomType = ndHotel.SelectSingleNode("rooms/room");
                    //int nRoom = Convert.ToInt32(nRoomType.Attributes.GetNamedItem("count").Value);
                    //RoomType[] objRoomType = new RoomType[nRoom];
                    ////XmlNodeList ndRoomTypeList = ndRoomType.SelectNodes("roomType");
                    //XmlNodeList ndRoomType = nRoomType.SelectNodes("roomType");
                    //for (int j = 0; j < nRoom; j++)
                    //{
                    //    objRoomType[j] = new RoomType();

                    //    objRoomType[j].RoomTypeCode = Convert.ToInt32(ndRoomType[j].Attributes.GetNamedItem("roomtypecode").Value);

                    //    XmlNode ncancellation = ndRoomType[j].SelectSingleNode("rateBases/rateBasis/cancellationRules");
                    //    Int32 ncancellationRules = Convert.ToInt32(ncancellation.Attributes.GetNamedItem("count").Value);
                    //    XmlNodeList ndcancellationRules = ndRoomType[j].SelectNodes("rateBases/rateBasis/cancellationRules/rule");
                    //    cancellationRules[] objcancellationRules = new cancellationRules[ncancellationRules];
                    //    for (int k = 0; k < ncancellationRules; k++)
                    //    {
                    //        if (k == 1)
                    //        {
                    //            objcancellationRules[k] = new cancellationRules();
                    //            objcancellationRules[k].fromDate = ndcancellationRules[k].SelectSingleNode("fromDate").InnerText;
                    //            objcancellationRules[k].cancelCharge = Convert.ToSingle(ndcancellationRules[k].SelectSingleNode("cancelCharge/formatted").InnerText) * dolarRate;
                    //            CutRoomAmountAmt = 0;

                    //            CutGlobalMarkupAmt = 0;
                    //            CutGlobalMarkupPer = 0;
                    //            ActualGlobalMarkupAmt = 0;

                    //            CutGroupMarkupAmt = 0;
                    //            CutGroupMarkupPer = 0;
                    //            ActualGroupMarkupAmt = 0;

                    //            CutIndividualMarkupAmt = 0;
                    //            CutIndividualMarkupPer = 0;
                    //            ActualIndividualMarkupAmt = 0;

                    //            ActualAgentRoomMarkup = 0;
                    //            CutRoomAmount = 0;
                    //            AgentRoomMarkup = 0;
                    //            AgentOwnMarkupPer = 0;
                    //            AgentOwnMarkupAmt = 0;

                    //            BaseAmt = objcancellationRules[k].cancelCharge;

                    //            // CutGlobalMarkup //
                    //            CutGlobalMarkupPer = ((objMarkupsAndTaxes.GlobalMarkupPer / 100) * BaseAmt);
                    //            CutGlobalMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.GlobalMarkAmt;
                    //            ActualGlobalMarkupAmt = Getgreatervalue(CutGlobalMarkupPer, CutGlobalMarkupAmt);
                    //            GlobalMarkup = ActualGlobalMarkupAmt + BaseAmt;

                    //            // CutGroupMarkup //
                    //            CutGroupMarkupPer = ((objMarkupsAndTaxes.GroupMarkupPer / 100) * GlobalMarkup);
                    //            CutGroupMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.GroupMarkAmt;
                    //            ActualGroupMarkupAmt = Getgreatervalue(CutGroupMarkupPer, CutGroupMarkupAmt);
                    //            GroupMarkup = ActualGroupMarkupAmt + GlobalMarkup;

                    //            // CutIndividualMarkup //
                    //            CutIndividualMarkupPer = ((objMarkupsAndTaxes.IndMarkupPer / 100) * GroupMarkup);
                    //            CutIndividualMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.IndMarkAmt;
                    //            ActualIndividualMarkupAmt = Getgreatervalue(CutIndividualMarkupPer, CutIndividualMarkupAmt);
                    //            IndividualMarkup = ActualIndividualMarkupAmt + GroupMarkup;

                    //            CutRoomAmountAmt = ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt;
                    //            // Start Agent Markup //

                    //            AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                    //            AgentOwnMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.AgentOwnMarkupAmt;
                    //            ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                    //            //End Agent Markup // 

                    //            CutRoomAmount = (BaseAmt + CutRoomAmountAmt);
                    //            AgentRoomMarkup = ActualAgentRoomMarkup;

                    //            objcancellationRules[k].CUTcancelCharge = CutRoomAmount;
                    //            objcancellationRules[k].AgentMarkup = AgentRoomMarkup;
                    //            objDOTWHotelDetails[i].cancellationRules.Add(objcancellationRules[k]);
                    //        }

                    //    }
                    //    //.................................................................................//
                    //    //.................................................................................//
                    //    //.................................................................................//
                    //    objDOTWHotelDetails[i].RoomType.Add(objRoomType[j]);

                    //}

                    #endregion Test Commented

                    //test commented by maqsood on 2/6/2017
                    //Context.Session["HotelPrice" + i] = objDOTWHotelDetails[i];
                    //end test commented by maqsood on 2/6/2017
                    DOTWHotelDetails.Add(objDOTWHotelDetails[i]);
                }
                Context.Session["HotelPrice"] = DOTWHotelDetails;
                //int m = 0;
                //Models.DoTWHotel objDOTWHotel = (Models.DoTWHotel)Context.Session["DoTWAvail"];
                List<DOTWHotelDetails> lstHotels = new List<DOTWHotelDetails>();
                List<DOTWHotelDetails> lstHotels_Final = new List<DOTWHotelDetails>();
                //List<Room> lstRoom_Final = new List<Room>();

                lstHotels = GetServiceHotel();
                try
                {
                    for (int i = 0; i < nNumberOfHotels; i++)
                    {
                        //objDOTWHotelDetails[i] = new DOTWHotelDetails();
                        //objDOTWHotelDetails[i] = (DOTWHotelDetails)Context.Session["HotelPrice" + i];
                        var objDOTWHotelDetailList = lstHotels.Where(data => data.HotelId == DOTWHotelDetails[i].HotelId).FirstOrDefault();
                        //if (objDOTWHotelDetails[i].HotelId == 89720)
                        //{

                        //}
                        if (objDOTWHotelDetailList != null)
                        {
                            List<Room> lstRoom_Final = new List<Room>();
                            Room[] objRoom = new Room[DOTWHotelDetails[i].Room.Count];
                            //if (objDOTWHotelDetailList != null)
                            //{
                            for (int j = 0; j < DOTWHotelDetails[i].Room.Count; j++)
                            {
                                objRoom[j] = new Room();
                                objRoom[j].Count = DOTWHotelDetails[i].Room[j].Count;
                                objRoom[j].RoomType = new List<RoomType>();


                                RoomType[] objRoomType = new RoomType[DOTWHotelDetails[i].Room[j].RoomType.Count];
                                for (int k = 0; k < DOTWHotelDetails[i].Room[j].RoomType.Count; k++)
                                {
                                    objRoomType[k] = new RoomType();
                                    for (int l = 0; l < objDOTWHotelDetailList.Room[0].RoomType.Count; l++)
                                    {
                                        if (DOTWHotelDetails[i].Room[j].RoomType[k].RoomTypeCode == objDOTWHotelDetailList.Room[0].RoomType[l].RoomTypeCode)
                                        {
                                            objRoomType[k].Adults = DOTWHotelDetails[i].Room[j].RoomType[k].Adults;
                                            objRoomType[k].Children = DOTWHotelDetails[i].Room[j].RoomType[k].Children;
                                            objRoomType[k].ChildrenAges = DOTWHotelDetails[i].Room[j].RoomType[k].ChildrenAges;
                                            objRoomType[k].RoomAmenities = objDOTWHotelDetailList.Room[0].RoomType[l].RoomAmenities;
                                            objRoomType[k].RoomCapacityInfo = objDOTWHotelDetailList.Room[0].RoomType[l].RoomCapacityInfo;
                                            if (DOTWHotelDetails[i].Room[j].RoomType[k].RoomInfo != null)
                                            {
                                                objDOTWHotelDetailList.Room[0].RoomType[l].RoomInfo.maxOccupancy = DOTWHotelDetails[i].Room[j].RoomType[k].RoomInfo.maxOccupancy;
                                            }
                                            objRoomType[k].RoomInfo = objDOTWHotelDetailList.Room[0].RoomType[l].RoomInfo;
                                            objRoomType[k].RoomTypeCode = objDOTWHotelDetailList.Room[0].RoomType[l].RoomTypeCode;
                                            objRoomType[k].RoomTypeName = objDOTWHotelDetailList.Room[0].RoomType[l].RoomTypeName;
                                            objRoomType[k].roomNumber = DOTWHotelDetails[i].Room[j].RoomType[k].roomNumber;
                                            objRoomType[k].special = DOTWHotelDetails[i].Room[j].RoomType[k].special;
                                            objRoomType[k].rateBasis = new List<rateBasis>();

                                            //rateBasis[] objrateBasis = new rateBasis[objDOTWHotelDetailList.Room[0].RoomType[k].rateBasis.Count];
                                            //for (int m = 0; m < objDOTWHotelDetailList.Room[0].RoomType[k].rateBasis.Count; m++)
                                            rateBasis[] objrateBasis = new rateBasis[DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis.Count];
                                            for (int m = 0; m < DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis.Count; m++)
                                            {
                                                //if (DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].RoomDescriptionId == objDOTWHotelDetailList.Room[0].RoomType[l].rateBasis[m].RoomDescriptionId)
                                                //{
                                                objrateBasis[m] = new rateBasis();
                                                objrateBasis[m].cancellationRules = DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].cancellationRules;

                                                objrateBasis[m].Total = DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].Total;
                                                objrateBasis[m].CUTPrice = DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].CUTPrice;
                                                objrateBasis[m].AgentMarkup = DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].AgentMarkup;
                                                objrateBasis[m].LeftToSell = DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].LeftToSell;

                                                objrateBasis[m].tariffNotes = DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].tariffNotes;
                                                objrateBasis[m].RoomDescription = DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].RoomDescription;
                                                objrateBasis[m].RoomDescriptionId = DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].RoomDescriptionId;

                                                objrateBasis[m].RoomRateType = DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].RoomRateType;
                                                objrateBasis[m].RoomRateTypeCurrency = DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].RoomRateTypeCurrency;
                                                objrateBasis[m].RoomRateTypeCurrencyId = DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].RoomRateTypeCurrencyId;

                                                objrateBasis[m].RoomAllocationDetails = DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].RoomAllocationDetails;
                                                objrateBasis[m].specialsApplied = DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].specialsApplied;
                                                objrateBasis[m].minStay = DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].minStay;
                                                objrateBasis[m].dateApplyMinStay = DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].dateApplyMinStay;
                                                objrateBasis[m].status = DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].status;
                                                objrateBasis[m].passengerNamesRequiredForBooking = DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].passengerNamesRequiredForBooking;
                                                if (DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].validForOccupancy != null)
                                                {
                                                    objrateBasis[m].validForOccupancy = new validForOccupancy();
                                                    objrateBasis[m].validForOccupancy.adults = DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].validForOccupancy.adults;
                                                    objrateBasis[m].validForOccupancy.children = DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].validForOccupancy.children;
                                                    objrateBasis[m].validForOccupancy.childrenAges = DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].validForOccupancy.childrenAges;
                                                    objrateBasis[m].validForOccupancy.extraBed = DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].validForOccupancy.extraBed;
                                                    objrateBasis[m].validForOccupancy.extraBedOccupant = DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].validForOccupancy.extraBedOccupant;
                                                }
                                                if (DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].changedOccupancy != null)
                                                {
                                                    objrateBasis[m].changedOccupancy = DOTWHotelDetails[i].Room[j].RoomType[k].rateBasis[m].changedOccupancy;
                                                }
                                                objRoomType[k].rateBasis.Add(objrateBasis[m]);
                                                // }
                                            }
                                            //objDOTWHotelDetailList.RoomPrice = objRoomType[k].Total;
                                            //objDOTWHotelDetailList.CUTPrice = objRoomType[k].CUTPrice;
                                            //objDOTWHotelDetailList.AgentMarkup = objRoomType[k].AgentMarkup;



                                            objRoom[j].RoomType.Add(objRoomType[k]);
                                            //objDOTWHotelDetails[i].Room[j].RoomType[k].RoomAmenities = objDOTWHotelDetailList.Room[j].RoomType[l].RoomAmenities;
                                            //objDOTWHotelDetails[i].Room[j].RoomType[k].RoomCapacityInfo = objDOTWHotelDetailList.Room[j].RoomType[l].RoomCapacityInfo;
                                            //objDOTWHotelDetails[i].Room[j].RoomType[k].RoomInfo = objDOTWHotelDetailList.Room[j].RoomType[l].RoomInfo;
                                            //objDOTWHotelDetails[i].Room[j].RoomType[k].RoomTypeName = objDOTWHotelDetailList.Room[j].RoomType[l].RoomTypeName;

                                            //lstRoomType_Final.Add(objRoomType[k]);
                                        }
                                        //objDOTWHotelDetailList.Room[j].RoomType[l] = objDOTWHotelDetails[i].Room[j].RoomType[k];

                                        //objDOTWHotelDetailList.Room[j].RoomType[k].cancellationRules = objDOTWHotelDetails[i].Room[j].RoomType[k].cancellationRules;
                                    }
                                    //objDOTWHotelDetailList.Room.Add(objRoom[j]);
                                }
                                //if (j == 0)
                                //    objDOTWHotelDetailList.Room = new List<Room>();

                                //lstRoom_Final = GetRoomOccupancy(objRoom[j].RoomType, Occupancy);

                                lstRoom_Final.Add(objRoom[j]);




                                //objDOTWHotelDetailList.Room.Add(objRoom[j]);
                                //objDOTWHotelDetailList.Room[j].RoomType = lstRoomType_Final;

                                //objRoom[j] = new Room();
                                //objRoom[j].RoomType = new List<RoomType>();
                                //objRoom[j].RoomType = objDOTWHotelDetails[i].Room[j].RoomType;
                                //RoomType[] objRoomType = new RoomType[objRoom[j].RoomType.Count];
                                //for (int k = 0; k < objDOTWHotelDetails[i].Room[j].RoomType.Count; k++)
                                //{
                                //    objRoomType[k] = new RoomType();
                                //    objRoomType[k].cancellationRules = objDOTWHotelDetails[i].Room[j].RoomType[k].cancellationRules;
                                //    cancellationRules[] objcancellationRules = new cancellationRules[objDOTWHotelDetails[i].Room[j].RoomType[k].cancellationRules.Count];
                                //    for (int l = 0; l < objDOTWHotelDetails[i].Room[j].RoomType[k].cancellationRules.Count; l++)
                                //    {
                                //        objcancellationRules[l] = new cancellationRules();
                                //        objcancellationRules[l].cancelCharge = objDOTWHotelDetails[i].Room[j].RoomType[k].cancellationRules[l].cancelCharge;
                                //        objcancellationRules[l].fromDate = objDOTWHotelDetails[i].Room[j].RoomType[k].cancellationRules[l].fromDate;
                                //        objcancellationRules[l].CUTcancelCharge = objDOTWHotelDetails[i].Room[j].RoomType[k].cancellationRules[l].CUTcancelCharge;
                                //        objcancellationRules[l].AgentMarkup = objDOTWHotelDetails[i].Room[j].RoomType[k].cancellationRules[l].AgentMarkup;
                                //        objRoomType[k].cancellationRules.Add(objcancellationRules[l]);
                                //    }
                                //    objRoom[j].RoomType.Add(objRoomType[k]);
                                //}
                                //objDOTWHotelDetailList.Room.Add(objRoom[j]);
                            }
                            //start from here...
                            List<Room> lstRoom_F = new List<Room>();
                            lstRoom_F = GetRoomOccupancy(lstRoom_Final, Occupancy);
                            if (lstRoom_F.Count != 0)
                            {

                                var Room_Final = lstRoom_F.OrderBy(data => data.CUTRoomPrice).FirstOrDefault();
                                objDOTWHotelDetailList.RoomPrice = Room_Final.RoomPrice;
                                objDOTWHotelDetailList.CUTPrice = Room_Final.CUTRoomPrice;
                                objDOTWHotelDetailList.AgentMarkup = Room_Final.AgentMarkup;
                                objDOTWHotelDetailList.Room = lstRoom_F;
                                lstHotels_Final.Add(objDOTWHotelDetailList);

                                //Context.Session["HotelPrice" + i] = null;
                                //Context.Session["HotelPrice" + i] = lstHotels_Final[lstHotels_Final.Count - 1];



                            }
                            //else
                            //{
                            //    Context.Session["HotelPrice" + i] = null;
                            //}
                            //}//........................
                        }
                    }
                    Context.Session["HotelPrice"] = lstHotels_Final;
                }
                catch
                {
                    return nStatus = false;
                }


                //foreach (DOTWLib.Response.DOTWHotelDetails objDOTWHotelDetail in objDOTWHotel.HotelDetails)
                //{
                //    var objDOTWHotelDetailList = objDOTWHotelDetails.Where(data => data.HotelId == objDOTWHotelDetail.HotelId).ToList();
                //    m++;
                //}
                return nStatus;
            }
            else
                return nStatus;

        }
        #endregion Parsepricexml

        public static float Getgreatervalue(float Percentage, float Ammount)
        {
            if (Percentage > Ammount)
            {
                return Percentage;
            }
            else
            {
                return Ammount;
            }
        }

        public List<DOTWLib.Response.Facility> Facility
        {
            get { return List_Facility; }
        }
        public List<DOTWLib.Response.Location> Location
        {
            get { return List_Location; }
        }
        public List<DOTWLib.Response.Category> Category
        {
            get { return List_Category; }
        }

        public List<DOTWHotelDetails> GetServiceHotel()
        {
            List<DOTWHotelDetails> lstHotels = new List<DOTWHotelDetails>();
            List<DOTWHotelDetails> DOTWHotelDetails = new List<DOTWHotelDetails>();
            //if (Context.Session["DoTWNumberOfHotels"] != null)
            //{
            //    int hotelCount = Convert.ToInt32(Context.Session["DoTWNumberOfHotels"]);
            //    for (int i = 0; i < hotelCount; i++)
            //    {
            //        DOTWHotelDetails objDOTWHotelDetails = (DOTWHotelDetails)Context.Session["Hotel" + i];
            //        if (objDOTWHotelDetails != null)
            //        {
            //            lstHotels.Add(objDOTWHotelDetails);
            //        }
            //    }
            //}

            if (Context.Session["DoTWNumberOfHotels"] != null)
            {
                int hotelCount = Convert.ToInt32(Context.Session["DoTWNumberOfHotels"]);
                DOTWHotelDetails = (List<DOTWHotelDetails>)Context.Session["Hotel"];
                for (int i = 0; i < hotelCount; i++)
                {
                    if (DOTWHotelDetails[i] != null)
                    {
                        lstHotels.Add(DOTWHotelDetails[i]);
                    }
                }
            }

            return lstHotels;
        }

        //public List<DOTWHotelDetails> GetServiceHotel(List<DOTWLib.Request.Room> Occupancy)
        //{
        //    List<DOTWHotelDetails> lstHotels = new List<DOTWHotelDetails>();
        //    if (Context.Session["DoTWNumberOfHotels"] != null)
        //    //if (Context.Session["DoTWNumberOfHotels"] != null)
        //    {
        //        int hotelCount = Convert.ToInt32(Context.Session["DoTWNumberOfHotels"]);
        //        //int hotelCount = Convert.ToInt32(Context.Session["DoTWNumberOfHotels"]);
        //        for (int i = 0; i < hotelCount; i++)
        //        {
        //            DOTWHotelDetails objDOTWHotelDetails = (DOTWHotelDetails)Context.Session["Hotel" + i];
        //            if (objDOTWHotelDetails != null)
        //            {
        //                lstHotels.Add(objDOTWHotelDetails);
        //            }
        //        }
        //    }
        //    return lstHotels;
        //}

        public List<DOTWHotelDetails> GetServiceHotelPrice(List<DOTWLib.Request.Room> Occupancy)
        {
            List<DOTWHotelDetails> lstHotels = new List<DOTWHotelDetails>();
            List<DOTWHotelDetails> DOTWHotelDetails = new List<DOTWHotelDetails>();
            //if (Context.Session["DoTWNumberOfHotels"] != null)
            //if (Context.Session["DoTWNumberOfHotelsPrice"] != null)
            //{
            //    //int hotelCount = Convert.ToInt32(Context.Session["DoTWNumberOfHotels"]);
            //    int hotelCount = Convert.ToInt32(Context.Session["DoTWNumberOfHotelsPrice"]);
            //    for (int i = 0; i < hotelCount; i++)
            //    {
            //        if (Context.Session["HotelPrice" + i] != null)
            //        {
            //            DOTWHotelDetails objDOTWHotelDetails = (DOTWHotelDetails)Context.Session["HotelPrice" + i];
            //            lstHotels.Add(objDOTWHotelDetails);
            //        }

            //    }
            //}

            if (Context.Session["DoTWNumberOfHotelsPrice"] != null)
            {
                int hotelCount = Convert.ToInt32(Context.Session["DoTWNumberOfHotelsPrice"]);
                DOTWHotelDetails = (List<DOTWHotelDetails>)Context.Session["HotelPrice"];
                for (int i = 0; i < DOTWHotelDetails.Count; i++)
                {
                    if (DOTWHotelDetails[i] != null)
                    {
                        lstHotels.Add(DOTWHotelDetails[i]);
                    }
                }
            }

            return lstHotels;
            //return GetRoomOccupancy(lstHotels, Occupancy);
        }

        //private List<RoomOccupancy> GetRoomOccupancy(List<Room> List_Room, string HotelID)
        private List<Room> GetRoomOccupancy(List<Room> List_Room, List<DOTWLib.Request.Occupancy> Occupancy)
        {
            List<Room> List_RoomOccupancy = new List<Room>();
            List<RoomType> List_RoomType;
            List<rateBasis> Final_rateBasis;
            //List<RoomType> List_RoomType = new List<RoomType>();
            //int s = 0;



            List<RoomType>[] Array_RoomType = new List<RoomType>[Occupancy.Count];
            int min = 0;
            int i = 0;
            float price;
            float cutprice;
            float AgentMarkup;
            int l = 0;
            RoomType objRoomType;
            RoomType objRoomType_S;
            rateBasis objrateBasis;
            rateBasis objrateBasis_S;
            foreach (Room objRoom in List_Room)
            {
                //foreach (DOTWLib.Request.Occupancy objOccupancy in Occupancy)
                //{
                //    var occupancy_room = objRoom.RoomType.Where(data => data.Adults == objOccupancy.AdultCount && data.Children == objOccupancy.ChildCount).ToList();
                //    Array_RoomType[i] = occupancy_room;
                //    if (occupancy_room.Count == 0)
                //        return List_RoomOccupancy;
                //    if (min == 0 || min > occupancy_room.Count)
                //        min = occupancy_room.Count;
                //    i = i + 1;
                //}


                var occupancy_room = objRoom.RoomType;
                Array_RoomType[i] = occupancy_room;
                if (occupancy_room.Count == 0)
                    return List_RoomOccupancy;
                if (min == 0 || min > occupancy_room.Count)
                    min = occupancy_room.Count;
                i = i + 1;
            }

            //for test purpose 14 Mar 2016

            //trial #2
            int length = Array_RoomType.Length;
            for (int m = 0; m < Array_RoomType[0].Count; m++)
            {
                List<RoomType> Final_Room = new List<RoomType>();
                //price = 0;
                //cutprice = 0;
                //AgentMarkup = 0;
                objRoomType = (RoomType)Array_RoomType[0][m];
                Final_Room.Add(objRoomType);
                for (int n = 0; n < objRoomType.rateBasis.Count; n++)
                {
                    if (Final_Room.Count != 0)
                    {
                        Final_rateBasis = new List<rateBasis>();
                        List_RoomType = new List<RoomType>();
                        price = 0;
                        cutprice = 0;
                        AgentMarkup = 0;
                        objrateBasis = (rateBasis)objRoomType.rateBasis[n];
                        price = price + objrateBasis.Total;
                        cutprice = cutprice + objrateBasis.CUTPrice;
                        AgentMarkup = AgentMarkup + objrateBasis.AgentMarkup;
                        List_RoomType.Add(new RoomType
                        {
                            RoomTypeCode = objRoomType.RoomTypeCode,
                            RoomTypeName = objRoomType.RoomTypeName,
                            RoomDescription = objrateBasis.RoomDescription,
                            //RoomDescriptionId = objrateBasis.RoomDescriptionId + "_" + m + "_" + n,
                            RoomDescriptionId = objrateBasis.RoomDescriptionId,
                            RoomRateType = objrateBasis.RoomRateType,
                            RoomRateTypeCurrency = objrateBasis.RoomRateTypeCurrency,
                            RoomRateTypeCurrencyId = objrateBasis.RoomRateTypeCurrencyId,
                            RoomAllocationDetails = objrateBasis.RoomAllocationDetails,
                            RoomAmenities = objRoomType.RoomAmenities,
                            RoomInfo = objRoomType.RoomInfo,
                            special = objRoomType.special,
                            RoomCapacityInfo = objRoomType.RoomCapacityInfo,
                            cancellationRules = objrateBasis.cancellationRules,
                            tariffNotes = objrateBasis.tariffNotes,
                            Total = objrateBasis.Total,
                            LeftToSell = objrateBasis.LeftToSell,
                            CUTPrice = objrateBasis.CUTPrice,
                            AgentMarkup = objrateBasis.AgentMarkup,
                            ChildrenAges = objRoomType.ChildrenAges,
                            Children = objRoomType.Children,
                            Adults = objRoomType.Adults,
                            roomNumber = objRoomType.roomNumber,
                            specialsApplied = objrateBasis.specialsApplied,
                            minStay = objrateBasis.minStay,
                            dateApplyMinStay = objrateBasis.dateApplyMinStay,
                            status = objrateBasis.status,
                            passengerNamesRequiredForBooking = objrateBasis.passengerNamesRequiredForBooking,
                            validForOccupancy = objrateBasis.validForOccupancy,
                            changedOccupancy = objrateBasis.changedOccupancy
                        });
                        //List_RoomType.Add(new RoomType { RoomTypeCode = objRoomType.RoomTypeCode, RoomTypeName = objRoomType.RoomTypeName, RoomAmenities = objRoomType.RoomAmenities, RoomInfo = objRoomType.RoomInfo, special = objRoomType.special, RoomCapacityInfo = objRoomType.RoomCapacityInfo, ChildrenAges = objRoomType.ChildrenAges, Children = objRoomType.Children, Adults = objRoomType.Adults, roomNumber = objRoomType.roomNumber });
                        Final_rateBasis.Add(objrateBasis);
                        //Final_Room = List_RoomType;
                        //......

                        if (length > 1)
                        {
                            for (int x = 0; x < length - 1; x++)
                            {
                                for (int p = 0; p < Array_RoomType[x + 1].Count; p++)
                                {

                                    objRoomType_S = (RoomType)Array_RoomType[x + 1][p];
                                    if (Final_Room[Final_Room.IndexOf(Final_Room.FirstOrDefault())].RoomTypeCode == objRoomType_S.RoomTypeCode)
                                    {
                                        for (int y = 0; y < objRoomType_S.rateBasis.Count; y++)
                                        {
                                            objrateBasis_S = (rateBasis)objRoomType_S.rateBasis[y];
                                            if (Final_rateBasis[Final_rateBasis.IndexOf(Final_rateBasis.FirstOrDefault())].RoomDescriptionId == objrateBasis_S.RoomDescriptionId && Final_rateBasis[Final_rateBasis.IndexOf(Final_rateBasis.FirstOrDefault())].CUTPrice == objrateBasis_S.CUTPrice && Final_rateBasis.Count <= (x + 1) && objRoomType_S.Adults == Occupancy[x + 1].AdultCount && objRoomType_S.Children == Occupancy[x + 1].ChildCount)
                                            {
                                                price = price + objrateBasis_S.Total;
                                                cutprice = cutprice + objrateBasis_S.CUTPrice;
                                                AgentMarkup = AgentMarkup + objrateBasis_S.AgentMarkup;
                                                Final_rateBasis.Add(objrateBasis_S);

                                                List_RoomType.Add(new RoomType
                                                {
                                                    RoomTypeCode = objRoomType_S.RoomTypeCode,
                                                    RoomTypeName = objRoomType_S.RoomTypeName,
                                                    RoomDescription = objrateBasis_S.RoomDescription,
                                                    //RoomDescriptionId = objrateBasis_S.RoomDescriptionId + "_" + m + "_" + n,
                                                    RoomDescriptionId = objrateBasis_S.RoomDescriptionId,
                                                    RoomRateType = objrateBasis_S.RoomRateType,
                                                    RoomRateTypeCurrency = objrateBasis_S.RoomRateTypeCurrency,
                                                    RoomRateTypeCurrencyId = objrateBasis_S.RoomRateTypeCurrencyId,
                                                    RoomAllocationDetails = objrateBasis_S.RoomAllocationDetails,
                                                    RoomAmenities = objRoomType_S.RoomAmenities,
                                                    RoomInfo = objRoomType_S.RoomInfo,
                                                    special = objRoomType_S.special,
                                                    RoomCapacityInfo = objRoomType_S.RoomCapacityInfo,
                                                    cancellationRules = objrateBasis_S.cancellationRules,
                                                    tariffNotes = objrateBasis_S.tariffNotes,
                                                    Total = objrateBasis_S.Total,
                                                    LeftToSell = objrateBasis_S.LeftToSell,
                                                    CUTPrice = objrateBasis_S.CUTPrice,
                                                    AgentMarkup = objrateBasis_S.AgentMarkup,
                                                    ChildrenAges = objRoomType_S.ChildrenAges,
                                                    Children = objRoomType_S.Children,
                                                    Adults = objRoomType_S.Adults,
                                                    roomNumber = objRoomType_S.roomNumber,
                                                    specialsApplied = objrateBasis_S.specialsApplied,
                                                    minStay = objrateBasis_S.minStay,
                                                    dateApplyMinStay = objrateBasis_S.dateApplyMinStay,
                                                    status = objrateBasis_S.status,
                                                    passengerNamesRequiredForBooking = objrateBasis_S.passengerNamesRequiredForBooking,
                                                    validForOccupancy = objrateBasis_S.validForOccupancy,
                                                    changedOccupancy = objrateBasis_S.changedOccupancy
                                                });

                                                //List_RoomType.Add(new RoomType { RoomTypeCode = objRoomType.RoomTypeCode, RoomTypeName = objRoomType.RoomTypeName, RoomAmenities = objRoomType.RoomAmenities, RoomInfo = objRoomType.RoomInfo, special = objRoomType.special, RoomCapacityInfo = objRoomType.RoomCapacityInfo, ChildrenAges = objRoomType.ChildrenAges, Children = objRoomType.Children, Adults = objRoomType.Adults, roomNumber = objRoomType.roomNumber, rateBasis = Final_rateBasis });
                                            }
                                            //break;
                                        }
                                        //Final_Room.Add(objRoomType_S);

                                        Final_Room = List_RoomType;
                                    }
                                }
                            }
                            if (Final_Room.Count != length)
                            {
                                Final_Room.Clear();
                            }
                            else
                            {
                                List_RoomOccupancy.Add(new Room { RoomType = Final_Room, RoomPrice = Convert.ToSingle(Math.Round(price, 2)), CUTRoomPrice = cutprice, AgentMarkup = AgentMarkup });
                            }
                        }
                        else
                        {
                            Final_Room = List_RoomType;
                            //List_RoomOccupancy.Add(new Room { RoomType = Array_RoomType[0], RoomPrice = Array_RoomType[0][m].Total, CUTRoomPrice = Array_RoomType[0][m].CUTPrice, AgentMarkup = Array_RoomType[0][m].AgentMarkup });
                            List_RoomOccupancy.Add(new Room { RoomType = Final_Room, RoomPrice = Convert.ToSingle(Math.Round(Array_RoomType[0][m].rateBasis[n].Total, 2)), CUTRoomPrice = Array_RoomType[0][m].rateBasis[n].CUTPrice, AgentMarkup = Array_RoomType[0][m].rateBasis[n].AgentMarkup });
                        }

                        //Final_Room.Add(objRoomType);
                    }
                }
            }

            #region Commented last implemented logic
            ////trial #2
            //int length = Array_RoomType.Length;
            //for (int m = 0; m < Array_RoomType[0].Count; m++)
            //{
            //    List<RoomType> Final_Room = new List<RoomType>();
            //    price = 0;
            //    cutprice = 0;
            //    AgentMarkup = 0;
            //    objRoomType = (RoomType)Array_RoomType[0][m];
            //    price = price + objRoomType.Total;
            //    cutprice = cutprice + objRoomType.CUTPrice;
            //    AgentMarkup = AgentMarkup + objRoomType.AgentMarkup;
            //    Final_Room.Add(objRoomType);
            //    if (length > 1)
            //    {
            //        for (int n = 0; n < length - 1; n++)
            //        {
            //            for (int p = 0; p < Array_RoomType[n + 1].Count; p++)
            //            {

            //                objRoomType = (RoomType)Array_RoomType[n + 1][p];
            //                if (Final_Room[Final_Room.IndexOf(Final_Room.FirstOrDefault())].RoomTypeCode == objRoomType.RoomTypeCode)
            //                {
            //                    Final_Room.Add(objRoomType);
            //                    price = price + objRoomType.Total;
            //                    cutprice = cutprice + objRoomType.CUTPrice;
            //                    AgentMarkup = AgentMarkup + objRoomType.AgentMarkup;
            //                }
            //            }
            //        }
            //        if (Final_Room.Count != length)
            //        {
            //            Final_Room.Clear();
            //        }
            //        else
            //        {
            //            List_RoomOccupancy.Add(new Room { RoomType = Final_Room, RoomPrice = price, CUTRoomPrice = cutprice, AgentMarkup = AgentMarkup });
            //        }
            //    }
            //    else
            //    {
            //        //List_RoomOccupancy.Add(new Room { RoomType = Array_RoomType[0], RoomPrice = Array_RoomType[0][m].Total, CUTRoomPrice = Array_RoomType[0][m].CUTPrice, AgentMarkup = Array_RoomType[0][m].AgentMarkup });
            //        List_RoomOccupancy.Add(new Room { RoomType = Final_Room, RoomPrice = Array_RoomType[0][m].Total, CUTRoomPrice = Array_RoomType[0][m].CUTPrice, AgentMarkup = Array_RoomType[0][m].AgentMarkup });
            //    }

            //}
            #endregion Commented last implemented logic

            //trial #1
            //int length = Array_RoomType.Length;
            //for (int m = 0; m < length; m++)
            //{
            //    List<RoomType> Final_Room = new List<RoomType>();
            //    for (int n = 0; n < Array_RoomType[m].Count; n++)
            //    {
            //        objRoomType = (RoomType)Array_RoomType[m][n];
            //        Final_Room.Add(objRoomType);
            //    }
            //    for (int j = (m + 1); j < length; )
            //    {
            //        if (Array_RoomType[m] == Array_RoomType[j])
            //        {
            //            for (int k = j; k < length - 1; k++)
            //                Array_RoomType[k] = Array_RoomType[k + 1];
            //            length--;
            //        }
            //        else
            //            j++;
            //    }
            //}
            //.........................


            //for (int j = 0; j < min; j++)
            //{
            //    int count = 0;
            //    List<RoomType> Final_Room = new List<RoomType>();
            //    //List<RoomType> Final_RoomType = new List<RoomType>();
            //    price = 0;
            //    cutprice = 0;
            //    AgentMarkup = 0;
            //    //l = j;

            //    while (Array_RoomType.Length > count)
            //    {
            //        //RoomType objRoomType = (RoomType)Array_RoomType[count][l];  //original line
            //        //Room objRoom = (Room)Array_Room[count][l];
            //        if (count == 0)
            //        {
            //            //l = j;
            //            objRoomType = (RoomType)Array_RoomType[count][l];
            //            Final_Room.Add(objRoomType);
            //            count = count + 1;
            //            price = price + objRoomType.Total;
            //            cutprice = cutprice + objRoomType.CUTPrice;
            //            AgentMarkup = AgentMarkup + objRoomType.AgentMarkup;
            //        }
            //        else
            //        {
            //            for (int k = 0; k < Array_RoomType[count].Count; k++)
            //            {
            //                objRoomType = (RoomType)Array_RoomType[count][k];
            //                if (Final_Room[count - 1].RoomTypeCode == objRoomType.RoomTypeCode)
            //                {
            //                    Final_Room.Add(objRoomType);
            //                    count = count + 1;
            //                    price = price + objRoomType.Total;
            //                    cutprice = cutprice + objRoomType.CUTPrice;
            //                    AgentMarkup = AgentMarkup + objRoomType.AgentMarkup;
            //                    l++;
            //                    break;
            //                }
            //                else
            //                {
            //                    if (k == Array_RoomType[count].Count - 1)
            //                    {
            //                        Final_Room.Clear();
            //                        count = 0;
            //                        price = 0;
            //                        cutprice = 0;
            //                        AgentMarkup = 0;
            //                        l++;
            //                        break;
            //                    }
            //                }
            //            }

            //            //if (Final_Room[count - 1].RoomTypeCode == objRoomType.RoomTypeCode)
            //            //{
            //            //    Final_Room.Add(objRoomType);
            //            //    count = count + 1;
            //            //    price = price + objRoomType.Total;
            //            //    cutprice = cutprice + objRoomType.CUTPrice;
            //            //    AgentMarkup = AgentMarkup + objRoomType.AgentMarkup;
            //            //}
            //            //else
            //            //{
            //            //    if (Array_RoomType[count][l] == Array_RoomType[count].Last())
            //            //    {
            //            //        Final_Room.Clear();
            //            //        count = 0;
            //            //        price = 0;
            //            //        cutprice = 0;
            //            //        AgentMarkup = 0;
            //            //        //l = j;
            //            //        //break;
            //            //    }
            //            //    else
            //            //        l++;
            //            //}
            //        }
            //    }
            //    if (Final_Room.Count != 0)
            //    {
            //        NoofRooms = Final_Room.Count;
            //        List_RoomOccupancy.Add(new Room { RoomType = Final_Room, RoomPrice = price, CUTRoomPrice = cutprice, AgentMarkup = AgentMarkup });
            //    }
            //}
            ////Final_Room.Clear();
            ////s++;
            List_RoomOccupancy = List_RoomOccupancy.OrderBy(data => data.RoomType[0].RoomDescriptionId).ToList();




            #region Comment
            //...........

            //for (int e = 0; e < List_Room.Count; e++)
            //{
            //    i = 0;
            //    //Array_Room = new List<Room>[Occupancy.Count];
            //    int k = 0;
            //foreach (DOTWLib.Request.Room objOccupancy in Occupancy)
            //{
            //    //var occupancy_room = List_Room.Where(data => data.Room[k].Adults == objOccupancy.AdultCount && data.Room[k].Children == objOccupancy.ChildCount && data.Room[k].RoomCount == objOccupancy.RoomCount).ToList();
            //    var occupancy_room = List_Room[k].Room.Where(data => data.Adults == objOccupancy.adults && data.Children == objOccupancy.children.Count).ToList();
            //    Array_Room[i] = occupancy_room;
            //    if (occupancy_room.Count == 0)
            //        return List_RoomOccupancy;
            //    if (min == 0 || min > occupancy_room.Count)
            //        min = occupancy_room.Count;
            //    i = i + 1;
            //    k++;
            //    //occupancy_room.Clear();
            //}

            //for (int x = 0; x < min; x++)
            //{
            //    int count = 0;
            //    List<Room> Final_Room = new List<Room>();
            //    price = 0;
            //    cutprice = 0;
            //    AgentMarkup = 0;
            //    Room objRoom = (Room)Array_Room[x][0];
            //    for (int y = 0; y < objRoom.RoomType.Count; y++)
            //    {
            //        if (y == 0)
            //        {
            //            Final_Room.Add(objRoom);
            //            price = price + objRoom.RoomType[y].Total;
            //            cutprice = cutprice + objRoom.RoomType[y].CUTPrice;
            //            AgentMarkup = AgentMarkup + objRoom.RoomType[y].AgentMarkup;
            //        }
            //        else
            //        {
            //            objRoom = (Room)Array_Room[y][0];
            //            Final_Room.Add(objRoom);
            //            if (Final_Room[x].RoomType[y].RoomTypeCode == objRoom.RoomType[y].RoomTypeCode)
            //            {
            //                if (Final_Room[count - 1] != objRoom)
            //                {
            //                    Final_Room.Add(objRoom);
            //                    count = count + 1;
            //                    price = price + objRoom.RoomType[y].Total;
            //                    cutprice = cutprice + objRoom.RoomType[y].CUTPrice;
            //                    AgentMarkup = AgentMarkup + objRoom.RoomType[y].AgentMarkup;
            //                }
            //                else
            //                {
            //                    Final_Room.Add(objRoom);
            //                    count = count + 1;
            //                }
            //            }
            //            else
            //            {
            //                count = count + 1;
            //                //Final_Room.RemoveAt(0);
            //            }
            //        }
            //    }
            //}


            //.....................................................


            ////for (int j = 0; j < Array_Room.Length; j++)
            //for (int j = 0; j < min; j++)
            //{
            //    int count = 0;
            //    List<Room> Final_Room = new List<Room>();
            //    price = 0;
            //    cutprice = 0;
            //    AgentMarkup = 0;
            //    while (Array_Room.Length > count)
            //    {
            //        Room objRoom = (Room)Array_Room[count][j];
            //        if (count == 0)
            //        {
            //            Final_Room.Add(objRoom);
            //            count = count + 1;
            //            price = price + objRoom.RoomType[j].Total;
            //            cutprice = cutprice + objRoom.RoomType[j].CUTPrice;
            //            AgentMarkup = AgentMarkup + objRoom.RoomType[j].AgentMarkup;
            //        }
            //        else
            //        {
            //            //for (int x = 0; x < objRoom.RoomType.Count; x++)
            //            //{
            //            if (Final_Room[count - 1].RoomType[j].RoomTypeCode == objRoom.RoomType[j].RoomTypeCode)
            //            {
            //                if (Final_Room[count - 1] != objRoom)
            //                {
            //                    Final_Room.Add(objRoom);
            //                    count = count + 1;
            //                    price = price + objRoom.RoomType[j].Total;
            //                    cutprice = cutprice + objRoom.RoomType[j].CUTPrice;
            //                    AgentMarkup = AgentMarkup + objRoom.RoomType[j].AgentMarkup;
            //                }
            //                else
            //                {
            //                    Final_Room.Add(objRoom);
            //                    count = count + 1;
            //                }
            //            }
            //            else
            //            {
            //                count = count + 1;
            //                Final_Room.RemoveAt(0);
            //            }
            //            //}
            //        }
            //    }
            //    if (Final_Room.Count != 0)
            //    {
            //        MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();
            //        Models.Hotel objHotel;
            //        NoofRooms = Final_Room.Count;
            //        //List_RoomOccupancy.Add(new DOTWHotelDetails { RoomID = HotelID + "_" + (j + 1), sRoom = Final_Room, RoomPrice = price, CUTRoomPrice = cutprice, AgentMarkup = AgentMarkup });
            //    }

            //    }
            //}
            #endregion Comment

            return List_RoomOccupancy;
        }

        public bool ParseRoomDetails(DOTWLib.Response.DOTWHotelDetails objDOTWHotelDetails, string xmlPriceString, out DOTWLib.Response.DOTWHotelDetails OutobjDOTWHotelDetails, List<DOTWLib.Request.Occupancy> Occupancy, int Night)
        {
            DBHelper.DBReturnCode retCode;
            DataTable dtResult;
            XmlDocument doc = new XmlDocument();
            bool nStatus;
            float BaseAmt;

            ////.................test................//
            //float GlobalMarkupAmtS = 0;
            //float GlobalMarkupPerS = 0;
            //float GlobalCommAmtS = 0;
            //float GlobalCommPerS = 0;

            //float GroupMarkupAmtS = 0;
            //float GroupMarkupPerS = 0;
            //float GroupCommAmtS = 0;
            //float GroupCommPerS = 0;

            //float IndividualMarkupAmtS = 0;
            //float IndividualMarkupPerS = 0;
            //float IndividualCommAmtS = 0;
            //float IndividualCommPerS = 0;

            //float AgentOwnMarkupPerS = 0;
            //float AgentOwnMarkupAmtS = 0;

            //float PerServiceTaxS = 0;
            //float TimeGapS = 0;

            //DataSet ds;
            //DBHelper.DBReturnCode retCode1 = DefaultManager.GetMarkupsAndTaxesforDoTW(out ds);
            //if (DBHelper.DBReturnCode.SUCCESS == retCode1)
            //{
            //    DataTable dtIndividualMarkup, dtGroupMarkup, dtGlobalMarkup, dtAgentOwnMarkp, dtServiceTax;
            //    dtIndividualMarkup = ds.Tables[2];
            //    dtGroupMarkup = ds.Tables[1];
            //    dtGlobalMarkup = ds.Tables[0];
            //    dtAgentOwnMarkp = ds.Tables[3];
            //    dtServiceTax = ds.Tables[4];
            //    MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();
            //    if (dtIndividualMarkup.Rows.Count != 0)
            //    {
            //        IndividualMarkupPerS = Convert.ToSingle(dtIndividualMarkup.Rows[0]["MarkupPercentage"]);
            //        IndividualMarkupAmtS = Convert.ToSingle(dtIndividualMarkup.Rows[0]["MarkupAmmount"]);
            //        IndividualCommPerS = Convert.ToSingle(dtIndividualMarkup.Rows[0]["CommessionPercentage"]);
            //        IndividualCommAmtS = Convert.ToSingle(dtIndividualMarkup.Rows[0]["CommessionAmmount"]);
            //    }
            //    if (dtGroupMarkup.Rows.Count != 0)
            //    {
            //        GroupMarkupPerS = Convert.ToSingle(dtGroupMarkup.Rows[0]["MarkupPercentage"]);
            //        GroupMarkupAmtS = Convert.ToSingle(dtGroupMarkup.Rows[0]["MarkupAmmount"]);
            //        GroupCommPerS = Convert.ToSingle(dtGroupMarkup.Rows[0]["CommessionPercentage"]);
            //        GroupCommAmtS = Convert.ToSingle(dtGroupMarkup.Rows[0]["CommessionAmmount"]);
            //    }
            //    if (dtGlobalMarkup.Rows.Count != 0)
            //    {
            //        GlobalMarkupPerS = Convert.ToSingle(dtGlobalMarkup.Rows[0]["MarkupPercentage"]);
            //        GlobalMarkupAmtS = Convert.ToSingle(dtGlobalMarkup.Rows[0]["MarkupAmmount"]);
            //        GlobalCommPerS = Convert.ToSingle(dtGlobalMarkup.Rows[0]["CommessionPercentage"]);
            //        GlobalCommAmtS = Convert.ToSingle(dtGlobalMarkup.Rows[0]["CommessionAmmount"]);
            //    }
            //    if (dtAgentOwnMarkp.Rows.Count != 0)
            //    {
            //        AgentOwnMarkupPerS = Convert.ToSingle(dtAgentOwnMarkp.Rows[0]["Percentage"]);
            //        AgentOwnMarkupAmtS = Convert.ToSingle(dtAgentOwnMarkp.Rows[0]["Amount"]);
            //    }
            //    if (dtServiceTax.Rows.Count != 0)
            //    {
            //        PerServiceTaxS = Convert.ToSingle(dtServiceTax.Rows[0]["ServiceTax"]);
            //        TimeGapS = Convert.ToInt32(dtServiceTax.Rows[0]["TimeGap"]);
            //    }
            //}
            ////.................test end............//

            float CutRoomAmountAmt = 0;

            float CutGlobalMarkupAmt = 0;
            float CutGlobalMarkupPer = 0;
            float ActualGlobalMarkupAmt = 0;

            float CutGroupMarkupAmt = 0;
            float CutGroupMarkupPer = 0;
            float ActualGroupMarkupAmt = 0;

            float CutIndividualMarkupAmt = 0;
            float CutIndividualMarkupPer = 0;
            float ActualIndividualMarkupAmt = 0;

            float ActualAgentRoomMarkup = 0;
            float CutRoomAmount = 0;
            float AgentRoomMarkup = 0;
            float AgentOwnMarkupPer = 0;
            float AgentOwnMarkupAmt = 0;

            string currency = "";
            OutobjDOTWHotelDetails = new DOTWHotelDetails();

            float GlobalMarkup = 0;
            float GroupMarkup = 0;
            float IndividualMarkup = 0;

            string allowBook = "";
            List<Room> lstRoom_Final = new List<Room>();

            doc.LoadXml(xmlPriceString);
            XmlNode ndResponseStatus = doc.SelectSingleNode("result/successful");
            currency = doc.SelectSingleNode("result/currencyShort").InnerText;
            ExchangeRateManager.GetForeignExchangeRate(currency, out dtResult);
            float dolarRate = (float)Convert.ToDecimal(dtResult.Rows[0]["INR"].ToString(), CultureInfos);
            nStatus = Convert.ToBoolean(ndResponseStatus.InnerText);
            if (nStatus == true)
            {
                XmlNode ndHotel = doc.SelectSingleNode("result/hotel");
                allowBook = ndHotel.SelectSingleNode("allowBook").InnerText;
                if (allowBook == "yes")
                {
                    //nNumberOfHotels = Convert.ToInt32(ndHotels.Attributes.GetNamedItem("count").Value);
                    //Context.Session["DoTWNumberOfHotelsPrice"] = nNumberOfHotels;
                    //DOTWHotelDetails[] objDOTWHotelDetails = new DOTWHotelDetails[nNumberOfHotels];
                    MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();
                    if (Context.Session["markups"] != null)
                        objMarkupsAndTaxes = (MarkupsAndTaxes)Context.Session["markups"];
                    else if (Context.Session["AgentMarkupsOnAdmin"] != null)
                        objMarkupsAndTaxes = (MarkupsAndTaxes)Context.Session["AgentMarkupsOnAdmin"];
                    //for (int i = 0; i < nNumberOfHotels; i++)
                    //{
                    //    objDOTWHotelDetails[i] = new DOTWHotelDetails();
                    //    //XmlNode ndHotel = doc.SelectSingleNode("result/hotels/hotel").ChildNodes[i];
                    //    XmlNode ndHotel = doc.SelectSingleNode("result/hotels").ChildNodes[i];
                    //    objDOTWHotelDetails[i].HotelId = Convert.ToInt64(ndHotel.Attributes.GetNamedItem("hotelid").Value);
                    //    objDOTWHotelDetails[i].Room = new List<Room>();
                    XmlNodeList ndRoom = ndHotel.SelectNodes("rooms/room");
                    Room[] objRoom = new Room[ndRoom.Count];
                    for (int j = 0; j < ndRoom.Count; j++)
                    {
                        objRoom[j] = new Room();
                        objRoom[j].Count = Convert.ToInt64(ndRoom[j].Attributes.GetNamedItem("count").Value);
                        objRoom[j].RoomType = new List<RoomType>();
                        XmlNodeList ndRoomType = ndRoom[j].SelectNodes("roomType");
                        RoomType[] objRoomType = new RoomType[ndRoomType.Count];
                        for (int k = 0; k < ndRoomType.Count; k++)
                        {
                            objRoomType[k] = new RoomType();
                            objRoomType[k].RoomTypeName = ndRoomType[k].SelectSingleNode("name").InnerText;
                            objRoomType[k].special = new List<special>();
                            objRoomType[k].rateBasis = new List<rateBasis>();
                            XmlNode ndspecials = ndRoomType[k].SelectSingleNode("specials");
                            if (Convert.ToInt32(ndspecials.Attributes.GetNamedItem("count").Value) > 0)
                            {
                                XmlNodeList ndspecial = ndRoomType[k].SelectNodes("specials/special");
                                special[] objspecial = new special[ndspecial.Count];
                                for (int l = 0; l < ndspecial.Count; l++)
                                {
                                    objspecial[l] = new special();
                                    objspecial[l].runno = l;
                                    if (ndspecial[l].SelectSingleNode("type") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("type").InnerText != "")
                                        {
                                            objspecial[l].type = ndspecial[l].SelectSingleNode("type").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("specialName") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("specialName").InnerText != "")
                                        {
                                            objspecial[l].specialName = ndspecial[l].SelectSingleNode("specialName").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("upgradeToRoomId") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("upgradeToRoomId").InnerText != "")
                                        {
                                            objspecial[l].upgradeToRoomId = ndspecial[l].SelectSingleNode("upgradeToRoomId").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("upgradeToMealId") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("upgradeToMealId").InnerText != "")
                                        {
                                            objspecial[l].upgradeToMealId = ndspecial[l].SelectSingleNode("upgradeToMealId").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("condition") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("condition").InnerText != "")
                                        {
                                            objspecial[l].condition = ndspecial[l].SelectSingleNode("condition").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("description") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("description").InnerText != "")
                                        {
                                            objspecial[l].description = ndspecial[l].SelectSingleNode("description").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("notes") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("notes").InnerText != "")
                                        {
                                            objspecial[l].notes = ndspecial[l].SelectSingleNode("notes").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("stay") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("stay").InnerText != "")
                                        {
                                            objspecial[l].stay = ndspecial[l].SelectSingleNode("stay").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("discount") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("discount").InnerText != "")
                                        {
                                            objspecial[l].discount = ndspecial[l].SelectSingleNode("discount").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("discountedNights") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("discountedNights").InnerText != "")
                                        {
                                            objspecial[l].discountedNights = ndspecial[l].SelectSingleNode("discountedNights").InnerText;
                                        }
                                    }
                                    if (ndspecial[l].SelectSingleNode("pay") != null)
                                    {
                                        if (ndspecial[l].SelectSingleNode("pay").InnerText != "")
                                        {
                                            objspecial[l].pay = ndspecial[l].SelectSingleNode("pay").InnerText;
                                        }
                                    }
                                    objRoomType[k].special.Add(objspecial[l]);
                                }
                            }
                            XmlNode ndrateBasis = ndRoomType[k].SelectSingleNode("rateBases");
                            if (Convert.ToInt32(ndrateBasis.Attributes.GetNamedItem("count").Value) > 0)
                            {
                                XmlNodeList nrateBasis = ndRoomType[k].SelectNodes("rateBases/rateBasis");
                                rateBasis[] objrateBasis = new rateBasis[nrateBasis.Count];
                                for (int l = 0; l < nrateBasis.Count; l++)
                                {
                                    objrateBasis[l] = new rateBasis();
                                    objrateBasis[l].RoomDescription = nrateBasis[l].Attributes.GetNamedItem("description").Value;
                                    objrateBasis[l].RoomDescriptionId = nrateBasis[l].Attributes.GetNamedItem("id").Value;
                                    objrateBasis[l].RoomRateType = nrateBasis[l].SelectSingleNode("rateType").InnerText;
                                    objrateBasis[l].RoomRateTypeCurrency = nrateBasis[l].SelectSingleNode("rateType").Attributes.GetNamedItem("currencyshort").Value;
                                    objrateBasis[l].RoomRateTypeCurrencyId = Convert.ToInt64(nrateBasis[l].SelectSingleNode("rateType").Attributes.GetNamedItem("currencyid").Value);
                                    objrateBasis[l].RoomAllocationDetails = nrateBasis[l].SelectSingleNode("allocationDetails").InnerText;
                                    objrateBasis[l].minStay = nrateBasis[l].SelectSingleNode("minStay").InnerText;
                                    objrateBasis[l].dateApplyMinStay = nrateBasis[l].SelectSingleNode("dateApplyMinStay").InnerText;
                                    objrateBasis[l].status = nrateBasis[l].SelectSingleNode("status").InnerText;
                                    objrateBasis[l].passengerNamesRequiredForBooking = Convert.ToInt32(nrateBasis[l].SelectSingleNode("passengerNamesRequiredForBooking").InnerText);
                                    if (nrateBasis[l].SelectSingleNode("validForOccupancy") != null)
                                    {
                                        objrateBasis[l].validForOccupancy = new validForOccupancy();
                                        if (nrateBasis[l].SelectSingleNode("validForOccupancy/adults") != null)
                                        {
                                            objrateBasis[l].validForOccupancy.adults = Convert.ToInt32(nrateBasis[l].SelectSingleNode("validForOccupancy/adults").InnerText);
                                        }
                                        else
                                        {
                                            objrateBasis[l].validForOccupancy.adults = 0;
                                        }
                                        if (nrateBasis[l].SelectSingleNode("validForOccupancy/children") != null)
                                        {
                                            objrateBasis[l].validForOccupancy.children = Convert.ToInt32(nrateBasis[l].SelectSingleNode("validForOccupancy/children").InnerText);
                                        }
                                        else
                                        {
                                            objrateBasis[l].validForOccupancy.children = 0;
                                        }
                                        if (nrateBasis[l].SelectSingleNode("validForOccupancy/childrenAges") != null)
                                        {
                                            objrateBasis[l].validForOccupancy.childrenAges = nrateBasis[l].SelectSingleNode("validForOccupancy/childrenAges").InnerText;
                                        }
                                        else
                                        {
                                            objrateBasis[l].validForOccupancy.childrenAges = "";
                                        }
                                        if (nrateBasis[l].SelectSingleNode("validForOccupancy/extraBed") != null)
                                        {
                                            objrateBasis[l].validForOccupancy.extraBed = Convert.ToInt32(nrateBasis[l].SelectSingleNode("validForOccupancy/extraBed").InnerText);
                                        }
                                        else
                                        {
                                            objrateBasis[l].validForOccupancy.adults = 0;
                                        }
                                        if (nrateBasis[l].SelectSingleNode("validForOccupancy/extraBedOccupant") != null)
                                        {
                                            objrateBasis[l].validForOccupancy.extraBedOccupant = nrateBasis[l].SelectSingleNode("validForOccupancy/extraBedOccupant").InnerText;
                                        }
                                        else
                                        {
                                            objrateBasis[l].validForOccupancy.extraBedOccupant = "";
                                        }
                                        //objrateBasis[l].validForOccupancy.adults = Convert.ToInt32(nrateBasis[l].SelectSingleNode("validForOccupancy/adult").InnerText);
                                        //objrateBasis[l].validForOccupancy.children = Convert.ToInt32(nrateBasis[l].SelectSingleNode("validForOccupancy/children").InnerText);
                                        //objrateBasis[l].validForOccupancy.childrenAges = nrateBasis[l].SelectSingleNode("validForOccupancy/childrenAges").InnerText;
                                        //objrateBasis[l].validForOccupancy.extraBed = Convert.ToInt32(nrateBasis[l].SelectSingleNode("validForOccupancy/extraBed").InnerText);
                                        //objrateBasis[l].validForOccupancy.extraBedOccupant = nrateBasis[l].SelectSingleNode("validForOccupancy/extraBedOccupant").InnerText;
                                    }
                                    if (nrateBasis[l].SelectSingleNode("changedOccupancy") != null)
                                    {
                                        objrateBasis[l].changedOccupancy = nrateBasis[l].SelectSingleNode("changedOccupancy").InnerText;
                                    }


                                    objrateBasis[l].cancellationRules = new List<cancellationRules>();

                                    if (nrateBasis[l].SelectSingleNode("tariffNotes") != null)
                                    {
                                        objrateBasis[l].tariffNotes = nrateBasis[l].SelectSingleNode("tariffNotes").InnerText;
                                    }

                                    GlobalDefault objGlobalDefault = (GlobalDefault)Context.Session["LoginUser"];
                                    string DoTWcurrency = objGlobalDefault.Currency;
                                    ExchangeRateManager.GetForeignExchangeRate(currency, out dtResult);
                                    float ExchangeRate = 0;
                                    if (DoTWcurrency == "INR")
                                    {
                                        ExchangeRate = 1;
                                    }
                                    else
                                    {
                                        ExchangeRate = (float)Convert.ToSingle(dtResult.Rows[0]["INR"].ToString(), CultureInfos);
                                    }
                                    objrateBasis[l].LeftToSell = Convert.ToInt32(nrateBasis[l].SelectSingleNode("leftToSell").InnerText) + 1;
                                    objrateBasis[l].Total = Convert.ToSingle(nrateBasis[l].SelectSingleNode("total/formatted").InnerText, CultureInfos) * dolarRate;
                                    BaseAmt = objrateBasis[l].Total / ExchangeRate;
                                    CutRoomAmountAmt = 0;
                                    CutGlobalMarkupAmt = 0;
                                    CutGlobalMarkupPer = 0;
                                    ActualGlobalMarkupAmt = 0;
                                    CutGroupMarkupAmt = 0;
                                    CutGroupMarkupPer = 0;
                                    ActualGroupMarkupAmt = 0;
                                    CutIndividualMarkupAmt = 0;
                                    CutIndividualMarkupPer = 0;
                                    ActualIndividualMarkupAmt = 0;
                                    ActualAgentRoomMarkup = 0;
                                    CutRoomAmount = 0;
                                    AgentRoomMarkup = 0;
                                    AgentOwnMarkupPer = 0;
                                    AgentOwnMarkupAmt = 0;
                                    if (DoTWcurrency == "INR")
                                    {
                                        // CutGlobalMarkup //
                                        float GlobalMarkupPer = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == "Dotw").GlobalMarkupPer;
                                        CutGlobalMarkupPer = ((GlobalMarkupPer / 100) * BaseAmt);
                                        //CutGlobalMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.GlobalMarkAmt;
                                        ActualGlobalMarkupAmt = Getgreatervalue(CutGlobalMarkupPer, CutGlobalMarkupAmt);
                                        GlobalMarkup = ActualGlobalMarkupAmt + BaseAmt;

                                        // CutGroupMarkup //
                                        float GroupMarkupPer = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == "Dotw").GroupMarkupPer;
                                        CutGroupMarkupPer = ((GroupMarkupPer / 100) * GlobalMarkup);
                                        //CutGroupMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.GroupMarkAmt;
                                        ActualGroupMarkupAmt = Getgreatervalue(CutGroupMarkupPer, CutGroupMarkupAmt);
                                        GroupMarkup = ActualGroupMarkupAmt + GlobalMarkup;

                                        // CutIndividualMarkup //
                                        if (objMarkupsAndTaxes.listIndividualMarkup != null)
                                        {
                                            float IndMarkupPer = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == "Dotw").IndMarkupPer;
                                            CutIndividualMarkupPer = ((IndMarkupPer / 100) * GroupMarkup);
                                            //CutIndividualMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.IndMarkAmt;
                                            ActualIndividualMarkupAmt = Getgreatervalue(CutIndividualMarkupPer, CutIndividualMarkupAmt);
                                            IndividualMarkup = ActualIndividualMarkupAmt + GroupMarkup;
                                        }


                                        CutRoomAmountAmt = ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt;
                                        // Start Agent Markup //

                                        //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                                        float AgentMarkupPer = objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupPer;
                                        AgentOwnMarkupPer = ((AgentMarkupPer / 100) * IndividualMarkup);
                                        AgentOwnMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupAmt;
                                        ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                                        //End Agent Markup // 

                                        CutRoomAmount = (BaseAmt + CutRoomAmountAmt);
                                        AgentRoomMarkup = ActualAgentRoomMarkup;

                                       
                                    }
                                    else
                                    {
                                        // CutGlobalMarkup //
                                        float GlobalMarkupPer = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == "Dotw").GlobalMarkupPer;
                                        CutGlobalMarkupPer = ((GlobalMarkupPer / 100) * BaseAmt);
                                        //CutGlobalMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.GlobalMarkAmt;
                                        ActualGlobalMarkupAmt = Getgreatervalue(CutGlobalMarkupPer, CutGlobalMarkupAmt);
                                        GlobalMarkup = ActualGlobalMarkupAmt + BaseAmt;

                                        // CutGroupMarkup //
                                        float GroupMarkupPer = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == "Dotw").GroupMarkupPer;
                                        CutGroupMarkupPer = ((GroupMarkupPer / 100) * GlobalMarkup);
                                        //CutGroupMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.GroupMarkAmt;
                                        ActualGroupMarkupAmt = Getgreatervalue(CutGroupMarkupPer, CutGroupMarkupAmt);
                                        GroupMarkup = ActualGroupMarkupAmt + GlobalMarkup;

                                        // CutIndividualMarkup //
                                        if (objMarkupsAndTaxes.listIndividualMarkup != null)
                                        {
                                            float IndMarkupPer = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == "Dotw").IndMarkupPer;
                                            CutIndividualMarkupPer = ((IndMarkupPer / 100) * GroupMarkup);
                                            //CutIndividualMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.IndMarkAmt;
                                            ActualIndividualMarkupAmt = Getgreatervalue(CutIndividualMarkupPer, CutIndividualMarkupAmt);
                                            IndividualMarkup = ActualIndividualMarkupAmt + GroupMarkup;
                                        }


                                        CutRoomAmountAmt = ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt;
                                        // Start Agent Markup //

                                        //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                                        float AgentMarkupPer = objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupPer;
                                        AgentOwnMarkupPer = ((AgentMarkupPer / 100) * IndividualMarkup);
                                        //AgentOwnMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.AgentOwnMarkupAmt;
                                        ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                                        //End Agent Markup // 

                                        CutRoomAmount = (BaseAmt + CutRoomAmountAmt);
                                        AgentRoomMarkup = ActualAgentRoomMarkup;

                                        
                                    }
                                    if (nrateBasis[l].SelectSingleNode("specialsApplied") != null)
                                    {
                                        objrateBasis[l].specialsApplied = Convert.ToInt32(nrateBasis[l].SelectSingleNode("specialsApplied/special").InnerText);
                                    }
                                    else
                                    {
                                        objrateBasis[l].specialsApplied = -1;
                                    }
                                    objrateBasis[l].CUTPrice = CutRoomAmount;
                                    objrateBasis[l].AgentMarkup = AgentRoomMarkup;
                                    XmlNode ncancellation = nrateBasis[l].SelectSingleNode("cancellationRules");
                                    Int32 ncancellationRules = Convert.ToInt32(ncancellation.Attributes.GetNamedItem("count").Value);
                                    XmlNodeList ndcancellationRules = nrateBasis[l].SelectNodes("cancellationRules/rule");
                                    cancellationRules[] objcancellationRules = new cancellationRules[ncancellationRules];
                                    for (int m = 0; m < ncancellationRules; m++)
                                    {
                                        objcancellationRules[m] = new cancellationRules();
                                        if (ndcancellationRules[m].SelectSingleNode("noShowPolicy") == null && ndcancellationRules[m].SelectSingleNode("fromDate") == null)
                                        {
                                            objcancellationRules[m] = null;
                                        }
                                        else if (ndcancellationRules[m].SelectSingleNode("noShowPolicy") != null && ndcancellationRules[m].SelectSingleNode("fromDate") == null)
                                        {
                                            objcancellationRules[m] = null;
                                        }
                                        else if (ndcancellationRules[m].SelectSingleNode("noShowPolicy") != null && ndcancellationRules[m].SelectSingleNode("fromDate") != null)
                                        {
                                            objcancellationRules[m] = null;
                                        }
                                        else
                                        {
                                            if (ndcancellationRules[m].SelectSingleNode("amendRestricted") != null && ndcancellationRules[m].SelectSingleNode("cancelRestricted") != null)
                                            {
                                                if (ndcancellationRules[m].SelectSingleNode("amendRestricted").InnerText == "true" && ndcancellationRules[m].SelectSingleNode("cancelRestricted").InnerText == "true")
                                                {
                                                    objcancellationRules[m].fromDate = ndcancellationRules[m].SelectSingleNode("fromDate").InnerText;
                                                    objcancellationRules[m].cancelCharge = BaseAmt;
                                                    objcancellationRules[m].CUTcancelCharge = objrateBasis[l].CUTPrice;
                                                    objcancellationRules[m].AgentMarkup = objrateBasis[l].AgentMarkup;
                                                    objcancellationRules[m].AmendRestricted = true;
                                                    objcancellationRules[m].CancelRestricted = true;
                                                }
                                                else if (ndcancellationRules[m].SelectSingleNode("amendRestricted").InnerText == "false" && ndcancellationRules[m].SelectSingleNode("cancelRestricted").InnerText == "true")
                                                {
                                                    objcancellationRules[m].fromDate = ndcancellationRules[m].SelectSingleNode("fromDate").InnerText;
                                                    objcancellationRules[m].cancelCharge = BaseAmt;
                                                    objcancellationRules[m].CUTcancelCharge = objrateBasis[l].CUTPrice;
                                                    objcancellationRules[m].AgentMarkup = objrateBasis[l].AgentMarkup;
                                                    objcancellationRules[m].AmendRestricted = false;
                                                    objcancellationRules[m].CancelRestricted = true;
                                                }
                                                else
                                                {
                                                    objcancellationRules[m] = null;
                                                }
                                            }
                                            else
                                            {
                                                objcancellationRules[m].fromDate = ndcancellationRules[m].SelectSingleNode("fromDate").InnerText;
                                                objcancellationRules[m].cancelCharge = Convert.ToSingle(ndcancellationRules[m].SelectSingleNode("cancelCharge/formatted").InnerText, CultureInfos) * dolarRate;
                                                CutRoomAmountAmt = 0;
                                                CutGlobalMarkupAmt = 0;
                                                CutGlobalMarkupPer = 0;
                                                ActualGlobalMarkupAmt = 0;
                                                CutGroupMarkupAmt = 0;
                                                CutGroupMarkupPer = 0;
                                                ActualGroupMarkupAmt = 0;
                                                CutIndividualMarkupAmt = 0;
                                                CutIndividualMarkupPer = 0;
                                                ActualIndividualMarkupAmt = 0;
                                                ActualAgentRoomMarkup = 0;
                                                CutRoomAmount = 0;
                                                AgentRoomMarkup = 0;
                                                AgentOwnMarkupPer = 0;
                                                AgentOwnMarkupAmt = 0;
                                                BaseAmt = objcancellationRules[m].cancelCharge / ExchangeRate;
                                                if (DoTWcurrency == "INR")
                                                {
                                                    // CutGlobalMarkup //
                                                    float GlobalMarkupPer = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == "Dotw").GlobalMarkupPer;
                                                    CutGlobalMarkupPer = ((GlobalMarkupPer / 100) * BaseAmt);
                                                    //CutGlobalMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.GlobalMarkAmt;
                                                    ActualGlobalMarkupAmt = Getgreatervalue(CutGlobalMarkupPer, CutGlobalMarkupAmt);
                                                    GlobalMarkup = ActualGlobalMarkupAmt + BaseAmt;

                                                    // CutGroupMarkup //
                                                    float GroupMarkupPer = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == "Dotw").GroupMarkupPer;
                                                    CutGroupMarkupPer = ((GroupMarkupPer / 100) * GlobalMarkup);
                                                    //CutGroupMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.GroupMarkAmt;
                                                    ActualGroupMarkupAmt = Getgreatervalue(CutGroupMarkupPer, CutGroupMarkupAmt);
                                                    GroupMarkup = ActualGroupMarkupAmt + GlobalMarkup;

                                                    // CutIndividualMarkup //
                                                    if (objMarkupsAndTaxes.listIndividualMarkup != null)
                                                    {
                                                        float IndMarkupPer = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == "Dotw").IndMarkupPer;
                                                        CutIndividualMarkupPer = ((IndMarkupPer / 100) * GroupMarkup);
                                                        //CutIndividualMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.IndMarkAmt;
                                                        ActualIndividualMarkupAmt = Getgreatervalue(CutIndividualMarkupPer, CutIndividualMarkupAmt);
                                                        IndividualMarkup = ActualIndividualMarkupAmt + GroupMarkup;
                                                    }


                                                    CutRoomAmountAmt = ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt;
                                                    // Start Agent Markup //

                                                    //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                                                    float AgentMarkupPer = objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupPer;
                                                    AgentOwnMarkupPer = ((AgentMarkupPer / 100) * IndividualMarkup);
                                                    AgentOwnMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupAmt;
                                                    ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                                                    //End Agent Markup // 

                                                    CutRoomAmount = (BaseAmt + CutRoomAmountAmt);
                                                    AgentRoomMarkup = ActualAgentRoomMarkup;

                                                   
                                                }
                                                else
                                                {
                                                    // CutGlobalMarkup //
                                                    float GlobalMarkupPer = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == "Dotw").GlobalMarkupPer;
                                                    CutGlobalMarkupPer = ((GlobalMarkupPer / 100) * BaseAmt);
                                                    //CutGlobalMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.GlobalMarkAmt;
                                                    ActualGlobalMarkupAmt = Getgreatervalue(CutGlobalMarkupPer, CutGlobalMarkupAmt);
                                                    GlobalMarkup = ActualGlobalMarkupAmt + BaseAmt;

                                                    // CutGroupMarkup //
                                                    float GroupMarkupPer = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == "Dotw").GroupMarkupPer;
                                                    CutGroupMarkupPer = ((GroupMarkupPer / 100) * GlobalMarkup);
                                                    //CutGroupMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.GroupMarkAmt;
                                                    ActualGroupMarkupAmt = Getgreatervalue(CutGroupMarkupPer, CutGroupMarkupAmt);
                                                    GroupMarkup = ActualGroupMarkupAmt + GlobalMarkup;

                                                    // CutIndividualMarkup //
                                                    if (objMarkupsAndTaxes.listIndividualMarkup != null)
                                                    {
                                                        float IndMarkupPer = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == "Dotw").IndMarkupPer;
                                                        CutIndividualMarkupPer = ((IndMarkupPer / 100) * GroupMarkup);
                                                        //CutIndividualMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.IndMarkAmt;
                                                        ActualIndividualMarkupAmt = Getgreatervalue(CutIndividualMarkupPer, CutIndividualMarkupAmt);
                                                        IndividualMarkup = ActualIndividualMarkupAmt + GroupMarkup;
                                                    }


                                                    CutRoomAmountAmt = ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt;
                                                    // Start Agent Markup //

                                                    //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                                                    float AgentMarkupPer = objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupPer;
                                                    AgentOwnMarkupPer = ((AgentMarkupPer / 100) * IndividualMarkup);
                                                    //AgentOwnMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.AgentOwnMarkupAmt;
                                                    ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                                                    //End Agent Markup // 

                                                    CutRoomAmount = (BaseAmt + CutRoomAmountAmt);
                                                    AgentRoomMarkup = ActualAgentRoomMarkup;

                                                   
                                                }

                                                CutRoomAmount = (BaseAmt + CutRoomAmountAmt);
                                                AgentRoomMarkup = ActualAgentRoomMarkup;

                                                objcancellationRules[m].CUTcancelCharge = CutRoomAmount;
                                                objcancellationRules[m].AgentMarkup = AgentRoomMarkup;
                                                objcancellationRules[m].AmendRestricted = false;
                                                objcancellationRules[m].CancelRestricted = false;
                                            }
                                        }
                                        objrateBasis[l].cancellationRules.Add(objcancellationRules[m]);
                                    }
                                    objRoomType[k].rateBasis.Add(objrateBasis[l]);
                                }
                            }
                            objRoomType[k].ChildrenAges = ndRoom[j].Attributes.GetNamedItem("childrenages").Value;
                            objRoomType[k].Children = Convert.ToInt64(ndRoom[j].Attributes.GetNamedItem("children").Value);
                            objRoomType[k].Adults = Convert.ToInt64(ndRoom[j].Attributes.GetNamedItem("adults").Value);
                            objRoomType[k].roomNumber = j + 1;
                            objRoomType[k].RoomTypeCode = Convert.ToInt32(ndRoomType[k].Attributes.GetNamedItem("roomtypecode").Value);
                            objRoom[j].RoomType.Add(objRoomType[k]);
                        }
                        lstRoom_Final.Add(objRoom[j]);
                    }
                    List<Room> lstRoom_F = new List<Room>();
                    lstRoom_F = GetRoomOccupancy(lstRoom_Final, Occupancy);
                    //lstRoom_Final = GetRoomOccupancy(lstRoom_Final, Occupancy);
                    if (lstRoom_F.Count != 0)
                    {
                        var Room_Final = lstRoom_F.OrderBy(data => data.CUTRoomPrice).FirstOrDefault();
                        objDOTWHotelDetails.RoomPrice = Room_Final.RoomPrice;
                        objDOTWHotelDetails.CUTPrice = Room_Final.CUTRoomPrice;
                        objDOTWHotelDetails.AgentMarkup = Room_Final.AgentMarkup;
                        objDOTWHotelDetails.Room = lstRoom_Final;
                        OutobjDOTWHotelDetails = objDOTWHotelDetails;
                    }
                    else
                    {
                        OutobjDOTWHotelDetails = null;
                    }
                    //}
                    return nStatus;
                }
                else
                {
                    nStatus = false;
                    return nStatus;
                }
            }
            else
                return nStatus;

        }
    }
}