﻿using CutAdmin.DataLayer;
using GRNLib.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CutAdmin.Common
{
    public class ParseGRNResponse
    {
        public HttpContext Context { get; set; }

        public bool ParseJson(CutAdmin.Models.GRNStatusCode objGRNStatusCode, out GRNLib.Response.HotelList objHotelListResponse)
        {
            objHotelListResponse = new HotelList();
            try
            {
                Int64 HotelCount;
                Int64 RateCount;
                List<string> FacilitiesLists = new List<string>();
                var Respose = JsonConvert.DeserializeObject<dynamic>(objGRNStatusCode.Response);
                HotelCount = Convert.ToInt64(Respose.no_of_hotels.Value);
                objHotelListResponse.checkin = objGRNStatusCode.TDate.ToString();
                objHotelListResponse.checkout = objGRNStatusCode.FDate.ToString();
                objHotelListResponse.search_id = Respose.search_id.Value.ToString();
                objHotelListResponse.no_of_rooms = Convert.ToInt16(Respose.no_of_rooms.Value);
                objHotelListResponse.no_of_nights = Convert.ToInt16(Respose.no_of_nights.Value);
                if (Respose.no_of_children != null)
                    objHotelListResponse.no_of_children = Convert.ToInt16(Respose.no_of_children.Value);
                objHotelListResponse.no_of_adults = Convert.ToInt16(Respose.no_of_adults.Value);

                #region Hotels
                objHotelListResponse.hotels = new List<Hotel>();
                for (int i = 0; i < HotelCount; i++)
                {
                    Hotel ObjHotel = new Hotel();
                    if (Respose.hotels[0].rates[0].rooms.Count != Convert.ToInt16(Respose.no_of_rooms.Value) || Convert.ToInt16(Respose.no_of_rooms.Value) == 1)
                    {
                        ObjHotel.name = Respose.hotels[i].name.Value.ToString();
                        ObjHotel.hotel_code = Respose.hotels[i].hotel_code.Value.ToString();
                        ObjHotel.description = Respose.hotels[i].description.Value.ToString();
                        ObjHotel.country = Respose.hotels[i].country.Value.ToString();
                        ObjHotel.city_code = Respose.hotels[i].city_code.Value.ToString();
                        if (Respose.hotels[i].category != null)
                            ObjHotel.category = Convert.ToInt16(Respose.hotels[i].category.Value);
                        else
                            ObjHotel.category = 0;
                        ObjHotel.address = Respose.hotels[i].address.Value.ToString();
                        if (Respose.hotels[i].recommended != null)
                            ObjHotel.recommended = Convert.ToBoolean(Respose.hotels[i].recommended.Value);
                        else
                            ObjHotel.recommended = false;
                        #region Facilities
                        if (Respose.hotels[i].facilities != null)
                        {
                            ObjHotel.facilities = Respose.hotels[i].facilities.Value.ToString();
                            string[] Splitter = ObjHotel.facilities.Split(';').Select(S => S.Trim()).ToArray();//emails.Split(',').Select(email => email.Trim()).ToArray()
                            List<string> FacilityList = new List<string>();
                            for (int k = 0; k < Splitter.Length; k++)
                            {
                                if (i != 0)
                                {
                                    if (FacilitiesLists.Count > 0)
                                    {
                                        var Facility = FacilitiesLists.Where(s => s == Splitter[k]).ToList();
                                        if (Facility.Count == 0)
                                            FacilitiesLists.Add(Splitter[k]);
                                    }
                                }
                                else
                                    FacilitiesLists.Add(Splitter[k]);
                                FacilityList.Add(Splitter[k]);
                            }
                            ObjHotel.FacilitiesList = FacilityList;
                        }
                        #endregion Facilities
                        #region Rates
                        RateCount = Convert.ToInt64(Respose.hotels[i].rates.Count);
                        ObjHotel.rates = new List<Rate>();
                        for (int j = 0; j < RateCount; j++)
                        {
                            Rate ObjRate = new Rate();
                            ObjRate.supports_cancellation = Convert.ToBoolean(Respose.hotels[i].rates[j].supports_cancellation.Value);
                            ObjRate.supports_amendment = Convert.ToBoolean(Respose.hotels[i].rates[j].supports_amendment.Value);
                            ObjRate.room_code = Respose.hotels[i].rates[j].room_code.Value;
                            ObjRate.rate_type = Respose.hotels[i].rates[j].rate_type.Value;
                            ObjRate.rate_key = Respose.hotels[i].rates[j].rate_key.Value;
                            ObjRate.price = Convert.ToSingle(Respose.hotels[i].rates[j].price.Value);
                            float AgentPrice = 0;
                            float ExchangeRate = ExchangeRateManager.GetExchange(Context);

                            ObjRate.CUTRoomPrice = MarkupTaxManager.GetCutRoomAmount(ObjRate.price / ExchangeRate, ExchangeRateManager.GetCurentCurrency(Context), objHotelListResponse.no_of_rooms, objHotelListResponse.no_of_nights, "Mgh", out AgentPrice);
                            ObjRate.AgentPrice = AgentPrice;
                            if (Respose.hotels[i].rates[j].non_refundable != null)
                                ObjRate.non_refundable = Convert.ToBoolean(Respose.hotels[i].rates[j].non_refundable.Value);
                            ObjRate.no_of_rooms = Convert.ToInt16(Respose.hotels[i].rates[j].no_of_rooms.Value);
                            ObjRate.includes_boarding = Convert.ToBoolean(Respose.hotels[i].rates[j].includes_boarding.Value);
                            ObjRate.has_promotions = Convert.ToBoolean(Respose.hotels[i].rates[j].has_promotions.Value);
                            ObjRate.group_code = Respose.hotels[i].rates[j].group_code.Value;
                            ObjRate.currency = Respose.hotels[i].rates[j].currency.Value;
                            if (Respose.hotels[i].rates[j].allotment != null)
                                ObjRate.allotment = Convert.ToInt16(Respose.hotels[i].rates[j].allotment.Value);
                            #region Cancellation Policy
                            if (Respose.hotels[i].rates[j].cancellation_policy_code != null)
                                ObjRate.cancellation_policy_code = Respose.hotels[i].rates[j].cancellation_policy_code.Value;
                            else
                                ObjRate.cancellation_policy_code = "";
                            if (Respose.hotels[i].rates[j].cancellation_policy != null)
                            {
                                ObjRate.cancellation_policy = new CancellationPolicy();
                                ObjRate.cancellation_policy.under_cancellation = Convert.ToBoolean(Respose.hotels[i].rates[j].cancellation_policy.under_cancellation.Value);
                                if (Respose.hotels[i].rates[j].cancellation_policy.cancel_by_date != null)
                                    ObjRate.cancellation_policy.cancel_by_date = Respose.hotels[i].rates[j].cancellation_policy.cancel_by_date.Value.ToString();
                                ObjRate.cancellation_policy.amount_type = Respose.hotels[i].rates[j].cancellation_policy.amount_type.Value;
                                if (Respose.hotels[i].rates[j].cancellation_policy.details != null)
                                {
                                    Int64 CancellationDetailsCount = Convert.ToInt64(Respose.hotels[i].rates[j].cancellation_policy.details.Count);
                                    ObjRate.cancellation_policy.details = new List<Detail>();
                                    for (int k = 0; k < CancellationDetailsCount; k++)
                                    {
                                        Detail ObjDetail = new Detail();
                                        ObjDetail.from = Respose.hotels[i].rates[j].cancellation_policy.details[k].from.Value.ToString();
                                        ObjDetail.currency = Respose.hotels[i].rates[j].cancellation_policy.details[k].currency.Value;
                                        float AgentCancellationPrice = 0;
                                        if (Respose.hotels[i].rates[j].cancellation_policy.details[k].flat_fee != null)
                                        {
                                            ObjDetail.flat_fee = Convert.ToSingle(Respose.hotels[i].rates[j].cancellation_policy.details[k].flat_fee.Value);
                                            ObjDetail.CUTCancellationPrice = MarkupTaxManager.GetCutRoomAmount(ObjDetail.flat_fee / ExchangeRate, ExchangeRateManager.GetCurentCurrency(Context), objHotelListResponse.no_of_rooms, objHotelListResponse.no_of_nights, "Mgh", out AgentCancellationPrice);
                                            ObjDetail.AgentCancellationPrice = AgentCancellationPrice;
                                        }
                                        else if (Respose.hotels[i].rates[j].cancellation_policy.details[k].percent != null)
                                        {
                                            ObjDetail.percent = Convert.ToInt64(Respose.hotels[i].rates[j].cancellation_policy.details[k].percent.Value);
                                            float CUTCancellationPrice = (ObjRate.price * Convert.ToSingle(ObjDetail.percent)) / 100;
                                            ObjDetail.CUTCancellationPrice = MarkupTaxManager.GetCutRoomAmount(CUTCancellationPrice / ExchangeRate, ExchangeRateManager.GetCurentCurrency(Context), objHotelListResponse.no_of_rooms, objHotelListResponse.no_of_nights, "Mgh", out AgentCancellationPrice);
                                            //ObjDetail.CUTCancellationPrice = MarkupTaxManager.GetCutRoomAmount(CUTCancellationPrice, "INR", objHotelListResponse.no_of_rooms, objHotelListResponse.no_of_nights, "Mgh", out AgentCancellationPrice);
                                            ObjDetail.AgentCancellationPrice = AgentCancellationPrice;
                                        }
                                        else if (Respose.hotels[i].rates[j].cancellation_policy.details[k].nights != null)
                                        {
                                            ObjDetail.nights = Convert.ToInt64(Respose.hotels[i].rates[j].cancellation_policy.details[k].nights.Value);
                                            float CUTCancellationPrice = 0;
                                            if (ObjDetail.nights == objHotelListResponse.no_of_nights)
                                                CUTCancellationPrice = ObjRate.price;
                                            else
                                            {
                                                float PerNightRate = ObjRate.price / objHotelListResponse.no_of_nights;
                                                CUTCancellationPrice = PerNightRate * ObjDetail.nights;
                                            }
                                            ObjDetail.CUTCancellationPrice = MarkupTaxManager.GetCutRoomAmount(CUTCancellationPrice / ExchangeRate, ExchangeRateManager.GetCurentCurrency(Context), objHotelListResponse.no_of_rooms, objHotelListResponse.no_of_nights, "Mgh", out AgentCancellationPrice);
                                            ObjDetail.AgentCancellationPrice = AgentCancellationPrice;
                                        }
                                        ObjRate.cancellation_policy.details.Add(ObjDetail);
                                    }
                                }
                            }
                            #endregion Cancellation Policy
                            #region Rate Comment
                            if (Respose.hotels[i].rates[j].rate_comments != null)
                            {
                                Int64 RateCommentCount = Convert.ToInt64(Respose.hotels[i].rates[j].rate_comments.Count);
                                ObjRate.rate_comments = new List<RateComments>();
                                for (int k = 0; k < RateCommentCount; k++)
                                {
                                    RateComments ObjRateComments = new RateComments();
                                    ObjRateComments.comments = Respose.hotels[i].rates[j].rate_comments[k].comments.Value;
                                    ObjRateComments.checkinInstruction = Respose.hotels[i].rates[j].rate_comments[k].checkinInstruction.Value;
                                    ObjRate.rate_comments.Add(ObjRateComments);
                                }
                            }
                            #endregion Rate Comment
                            #region Promotion Details
                            if (Respose.hotels[i].rates[j].promotions_details != null)
                            {
                                Int64 PromotionDetailsCount = Convert.ToInt64(Respose.hotels[i].rates[j].promotions_details.Count);
                                List<string> BoardindDetailsList = new List<string>();
                                for (int m = 0; m < PromotionDetailsCount; m++)
                                {
                                    BoardindDetailsList.Add(Respose.hotels[i].rates[j].promotions_details[m].Value);
                                }
                                ObjRate.promotions_details = BoardindDetailsList;
                            }
                            #endregion Promotion Details
                            #region Rooms
                            ObjRate.rooms = new List<HotelRoom>();
                            Int64 NoOfRoom = Convert.ToInt64(Respose.hotels[i].rates[j].rooms.Count);
                            for (int l = 0; l < NoOfRoom; l++)
                            {
                                HotelRoom ObjHotelRoom = new HotelRoom();
                                ObjHotelRoom.room_type = Respose.hotels[i].rates[j].rooms[l].room_type.Value.ToString();
                                ObjHotelRoom.no_of_rooms = Convert.ToInt16(Respose.hotels[i].rates[j].rooms[l].no_of_rooms.Value);
                                ObjHotelRoom.no_of_children = Convert.ToInt16(Respose.hotels[i].rates[j].rooms[l].no_of_children.Value);
                                ObjHotelRoom.no_of_adults = Convert.ToInt16(Respose.hotels[i].rates[j].rooms[l].no_of_adults.Value);
                                if (Respose.hotels[i].rates[j].rooms[l].description != null)
                                    ObjHotelRoom.description = Respose.hotels[i].rates[j].rooms[l].description.Value;
                                int ChildCount = ObjHotelRoom.no_of_children;
                                #region Child Age List
                                List<int> ChildAge = new List<int>();
                                for (int m = 0; m < ChildCount; m++)
                                {
                                    ChildAge.Add(Convert.ToInt16(Respose.hotels[i].rates[j].rooms[l].children_ages[m]));
                                }
                                #endregion Child Age List
                                ObjHotelRoom.children_ages = ChildAge;
                                ObjRate.rooms.Add(ObjHotelRoom);
                            }

                            #endregion Rooms
                            #region Price Detils
                            ObjRate.price_details = new PriceDetails();
                            #region Net Rate
                            Int64 NetCount = Convert.ToInt64(Respose.hotels[i].rates[j].price_details.net.Count);
                            ObjRate.price_details.NetRate = new List<Net>();
                            for (int n = 0; n < NetCount; n++)
                            {
                                Net ObjNetRate = new Net();
                                ObjNetRate.name = Respose.hotels[i].rates[j].price_details.net[n].name.Value;
                                ObjNetRate.included = Convert.ToBoolean(Respose.hotels[i].rates[j].price_details.net[n].included.Value);
                                ObjNetRate.currency = Respose.hotels[i].rates[j].price_details.net[n].currency.Value;
                                ObjNetRate.amount_type = Respose.hotels[i].rates[j].price_details.net[n].amount_type.Value;
                                ObjNetRate.amount = Convert.ToSingle(Respose.hotels[i].rates[j].price_details.net[n].amount.Value);
                                ObjRate.price_details.NetRate.Add(ObjNetRate);
                            }
                            #endregion Net Rate
                            #region GST
                            Int64 GSTCount = Convert.ToInt64(Respose.hotels[i].rates[j].price_details.GST.Count);
                            ObjRate.price_details.GST = new List<GST>();
                            for (int n = 0; n < GSTCount; n++)
                            {
                                GST ObjGST = new GST();
                                ObjGST.name = Respose.hotels[i].rates[j].price_details.GST[n].name.Value;
                                ObjGST.included = Convert.ToBoolean(Respose.hotels[i].rates[j].price_details.GST[n].included.Value);
                                ObjGST.currency = Respose.hotels[i].rates[j].price_details.GST[n].currency.Value;
                                ObjGST.amount_type = Respose.hotels[i].rates[j].price_details.GST[n].amount_type.Value;
                                ObjGST.amount = Convert.ToSingle(Respose.hotels[i].rates[j].price_details.GST[n].amount.Value);
                                ObjRate.price_details.GST.Add(ObjGST);
                            }
                            #endregion GST
                            #endregion Price Detils
                            #region Payment Type
                            Int64 PaymentTypeCount = Convert.ToInt64(Respose.hotels[i].rates[j].payment_type.Count);
                            List<string> PaymentTypeList = new List<string>();
                            for (int m = 0; m < PaymentTypeCount; m++)
                            {
                                PaymentTypeList.Add(Respose.hotels[i].rates[j].payment_type[m].Value);
                            }
                            ObjRate.payment_type = PaymentTypeList;
                            #endregion Payment Type
                            if (Respose.hotels[i].rates[j].boarding_details != null)
                            {
                                Int64 BoardindDetailsCount = Convert.ToInt64(Respose.hotels[i].rates[j].boarding_details.Count);
                                List<string> BoardindDetailsList = new List<string>();
                                for (int m = 0; m < BoardindDetailsCount; m++)
                                {
                                    BoardindDetailsList.Add(Respose.hotels[i].rates[j].boarding_details[m].Value);
                                }
                                ObjRate.boarding_details = BoardindDetailsList;
                            }
                            ObjHotel.rates.Add(ObjRate);
                        }
                        float MaxCutPrice = ObjHotel.rates.OrderByDescending(d => d.CUTRoomPrice).Select(rate => rate.CUTRoomPrice).FirstOrDefault();
                        ObjHotel.MaxCutPrice = MaxCutPrice;
                        float AgentMarkup = ObjHotel.rates.OrderByDescending(d => d.AgentMarkup).Select(rate => rate.AgentMarkup).FirstOrDefault();
                        ObjHotel.AgentMarkup = AgentMarkup;
                        #endregion Rates
                        #region Image
                        ObjHotel.images = new Images();
                        ObjHotel.images.main_image = Respose.hotels[i].images.main_image.Value.ToString();
                        #endregion Image
                        #region GeoLocation
                        ObjHotel.geolocation = new Geolocation();
                        ObjHotel.geolocation.latitude = Convert.ToSingle(Respose.hotels[i].geolocation.latitude.Value);
                        ObjHotel.geolocation.longitude = Convert.ToSingle(Respose.hotels[i].geolocation.longitude.Value);
                        // ObjHotel.geolocation.
                        #endregion GeoLocation
                        objHotelListResponse.FacilitiesList = new List<string>();
                        objHotelListResponse.FacilitiesList = (FacilitiesLists);
                        objHotelListResponse.hotels.Add(ObjHotel);
                    }
                }
                #endregion  Hotels


                return true;
            }
            catch (Exception ex)
            {
                string LineNum = ex.StackTrace;
                return false;
            }
        }

        public bool BookingResponse(string Json, out GRNLib.Response.Booking.BookingResponse ObjBookingResponse)
        {
            ObjBookingResponse = new GRNLib.Response.Booking.BookingResponse();
            var Response = JsonConvert.DeserializeObject<dynamic>(Json);
            bool isResponse = false;
            try
            {
                ObjBookingResponse.supports_cancellation = Convert.ToBoolean(Response.supports_cancellation.Value);
                ObjBookingResponse.supports_amendment = Convert.ToBoolean(Response.supports_amendment.Value);
                ObjBookingResponse.status = Response.status.Value;
                ObjBookingResponse.payment_type = Response.payment_type.Value;
                ObjBookingResponse.payment_status = Response.payment_status.Value;
                ObjBookingResponse.currency = Response.currency.Value;
                ObjBookingResponse.checkin = Response.checkin.Value;
                ObjBookingResponse.checkout = Response.checkout.Value;
                ObjBookingResponse.booking_reference = Response.booking_reference.Value;
                ObjBookingResponse.booking_id = Response.booking_id.Value;
                ObjBookingResponse.booking_date = Response.booking_date.Value;
                ObjBookingResponse.booking_comment = Response.booking_comment.Value;
                #region Price
                ObjBookingResponse.price = new GRNLib.Response.Booking.Price();
                ObjBookingResponse.price.total = Response.price.total.Value;
                ObjBookingResponse.price.breakdown = new GRNLib.Response.Booking.Breakdown();
                ObjBookingResponse.price.breakdown.net = new List<Net>();
                Int64 NetCount = Convert.ToInt64(Response.price.breakdown.net.Count);
                for (int i = 0; i < NetCount; i++)
                {
                    Net ObjNet = new Net();
                    ObjNet.name = Response.price.breakdown.net[i].name.Value;
                    ObjNet.included = Convert.ToBoolean(Response.price.breakdown.net[i].included.Value);
                    ObjNet.currency = Response.price.breakdown.net[i].currency.Value;
                    ObjNet.amount_type = Response.price.breakdown.net[i].amount_type.Value;
                    ObjNet.amount = Convert.ToSingle(Response.price.breakdown.net[i].amount.Value);
                    ObjBookingResponse.price.breakdown.net.Add(ObjNet);
                }
                #endregion Price
                #region Hotel
                ObjBookingResponse.hotel = new GRNLib.Response.Booking.Hotel();
                ObjBookingResponse.hotel.name = Response.hotel.name.Value;
                ObjBookingResponse.hotel.hotel_code = Response.hotel.hotel_code.Value;
                ObjBookingResponse.hotel.description = Response.hotel.description.Value;
                ObjBookingResponse.hotel.city_code = Response.hotel.city_code.Value;
                ObjBookingResponse.hotel.category = Convert.ToSingle(Response.hotel.category.Value);
                ObjBookingResponse.hotel.address = Response.hotel.address.Value;
                #region Pax
                ObjBookingResponse.hotel.paxes = new List<GRNLib.Response.Booking.Pax>();
                Int64 PaxCount = Convert.ToInt64(Response.hotel.paxes.Count);
                for (int i = 0; i < PaxCount; i++)
                {
                    GRNLib.Response.Booking.Pax ObjPax = new GRNLib.Response.Booking.Pax();
                    ObjPax.type = Response.hotel.paxes[i].type.Value;
                    ObjPax.surname = Response.hotel.paxes[i].surname.Value;
                    ObjPax.pax_id = Convert.ToSingle(Response.hotel.paxes[i].pax_id.Value);
                    ObjPax.name = Response.hotel.paxes[i].name.Value;
                    ObjBookingResponse.hotel.paxes.Add(ObjPax);
                }
                #endregion
                #region GeoLocation
                ObjBookingResponse.hotel.geolocation = new Geolocation();
                ObjBookingResponse.hotel.geolocation.latitude = Convert.ToSingle(Response.hotel.geolocation.latitude.Value);
                ObjBookingResponse.hotel.geolocation.longitude = Convert.ToSingle(Response.hotel.geolocation.longitude.Value);
                // ObjHotel.geolocation.
                #endregion GeoLocation
                #region BookingItem
                ObjBookingResponse.hotel.booking_items = new List<GRNLib.Response.Booking.BookingItem>();
                Int64 BookingItemCount = Convert.ToInt64(Response.hotel.booking_items.Count);
                for (int i = 0; i < BookingItemCount; i++)
                {
                    GRNLib.Response.Booking.BookingItem ObjBookingItem = new GRNLib.Response.Booking.BookingItem();
                    ObjBookingItem.price = Convert.ToSingle(Response.hotel.booking_items[i].price.Value);
                    ObjBookingItem.non_refundable = Convert.ToBoolean(Response.hotel.booking_items[i].non_refundable.Value);
                    ObjBookingItem.includes_boarding = Convert.ToBoolean(Response.hotel.booking_items[i].includes_boarding.Value);
                    ObjBookingItem.currency = Response.hotel.booking_items[i].currency.Value;
                    #region Rooms
                    ObjBookingItem.rooms = new List<GRNLib.Response.Booking.Room>();
                    Int64 RoomCount = Convert.ToInt64(Response.hotel.booking_items.Count);
                    for (int j = 0; j < RoomCount; j++)
                    {
                        GRNLib.Response.Booking.Room ObjRoom = new GRNLib.Response.Booking.Room();
                        ObjRoom.room_type = Response.hotel.booking_items[i].rooms[j].room_type.Value;
                        ObjRoom.room_reference = Response.hotel.booking_items[i].rooms[j].room_reference.Value;
                        ObjRoom.no_of_children = Convert.ToSingle(Response.hotel.booking_items[i].rooms[j].no_of_children.Value);
                        ObjRoom.no_of_adults = Convert.ToSingle(Response.hotel.booking_items[i].rooms[j].no_of_adults.Value);
                        ObjRoom.description = Response.hotel.booking_items[i].rooms[j].description.Value;
                        #region Pax Ids
                        ObjRoom.pax_ids = new List<int>();
                        Int64 PaxIdCount = Convert.ToInt64(Response.hotel.booking_items[i].rooms[j].pax_ids.Count);
                        List<int> PaxIdList = new List<int>();
                        for (int k = 0; k < PaxIdCount; k++)
                        {
                            PaxIdList.Add(Convert.ToSingle(Response.hotel.booking_items[i].rooms[j].pax_ids[k]));
                        }
                        ObjRoom.pax_ids = PaxIdList;
                        #endregion

                        ObjBookingItem.rooms.Add(ObjRoom);
                    }
                    #endregion

                    ObjBookingResponse.hotel.booking_items.Add(ObjBookingItem);
                }
                #endregion
                #endregion Hotel
                #region Holder
                ObjBookingResponse.holder = new GRNLib.Request.Booking.Holder();
                ObjBookingResponse.holder.title = Response.holder.title.Value;
                ObjBookingResponse.holder.surname = Response.holder.title.surname;
                ObjBookingResponse.holder.phone_number = Response.holder.phone_number.Value;
                ObjBookingResponse.holder.name = Response.holder.name.Value;
                ObjBookingResponse.holder.email = Response.holder.email.Value;
                ObjBookingResponse.holder.name = Response.holder.name.Value;
                ObjBookingResponse.holder.client_nationality = Response.holder.client_nationality.Value;
                #endregion
                #region Additional Info
                ObjBookingResponse.additional_info = new GRNLib.Response.Booking.AdditionalInfo();
                ObjBookingResponse.additional_info.Msg = Response.additional_info.Msg.Value;
                #endregion
                isResponse = true;
            }
            catch (Exception ex)
            {
                string LineNo = ex.StackTrace.ToString();
            }
            return isResponse;
        }

        public bool CancellResponse(string Json, out GRNLib.Response.Cancellation.CancellationResponse ObjCancellationResponse)
        {
            ObjCancellationResponse = new GRNLib.Response.Cancellation.CancellationResponse();
            try
            {
                var Response = JsonConvert.DeserializeObject<dynamic>(Json);
                ObjCancellationResponse.status = Response.status.Value;
                ObjCancellationResponse.cancellation_reference = Response.cancellation_reference.Value;
                ObjCancellationResponse.cancellation_comments = Response.cancellation_comments.Value;
                ObjCancellationResponse.booking_reference = Response.booking_reference.Value;
                ObjCancellationResponse.booking_id = Response.booking_id.Value;
                //Cancellation Charges
                ObjCancellationResponse.cancellation_charges = new GRNLib.Response.Cancellation.CancellationCharges();
                ObjCancellationResponse.cancellation_charges.amount = Convert.ToSingle(Response.cancellation_charges.amount.Value);
                ObjCancellationResponse.cancellation_charges.currency = Response.cancellation_charges.currency.Value;
                //Booking Price
                ObjCancellationResponse.booking_price = new GRNLib.Response.Cancellation.BookingPrice();
                ObjCancellationResponse.booking_price.amount = Convert.ToSingle(Response.booking_price.amount.Value);
                ObjCancellationResponse.booking_price.currency = Response.booking_price.currency.Value;
                return true;
            }
            catch (Exception ex)
            {
                string LineNo = ex.StackTrace.ToString();
                return false;
            }
        }

        public bool ImagesReseponse(string Json, out  GRNLib.Response.ImagesRes.ImageList ObjImageList)
        {
            ObjImageList = new GRNLib.Response.ImagesRes.ImageList();
            try
            {
                var Response = JsonConvert.DeserializeObject<dynamic>(Json);
                ObjImageList.images = new GRNLib.Response.ImagesRes.Images();
                #region Small
                if (Response.images.small != null)
                {
                    Int64 SmallCount = Convert.ToInt64(Response.images.small.Count);
                    ObjImageList.images.small = new List<GRNLib.Response.ImagesRes.Small>();
                    for (int i = 0; i < SmallCount; i++)
                    {
                        GRNLib.Response.ImagesRes.Small ObjSmall = new GRNLib.Response.ImagesRes.Small();
                        ObjSmall.path = Response.images.small[i].path;
                        ObjImageList.images.small.Add(ObjSmall);
                    }
                }

                #endregion
                #region Regular
                Int64 RegularCount = Convert.ToInt64(Response.images.regular.Count);
                ObjImageList.images.regular = new List<GRNLib.Response.ImagesRes.Regular>();
                for (int i = 0; i < RegularCount; i++)
                {
                    GRNLib.Response.ImagesRes.Regular ObjRegular = new GRNLib.Response.ImagesRes.Regular();
                    ObjRegular.path = Response.images.regular[i].path;
                    ObjImageList.images.regular.Add(ObjRegular);
                }
                #endregion
                #region Large
                if (Response.images.large != null)
                {
                    Int64 LargeCount = Convert.ToInt64(Response.images.large.Count);
                    ObjImageList.images.large = new List<GRNLib.Response.ImagesRes.Large>();
                    for (int i = 0; i < LargeCount; i++)
                    {
                        GRNLib.Response.ImagesRes.Large ObjLarge = new GRNLib.Response.ImagesRes.Large();
                        ObjLarge.path = Response.images.large[i].path;
                        ObjImageList.images.large.Add(ObjLarge);
                    }
                }
                #endregion
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool CancellationPolicyCodeRes(string Json, out  GRNLib.Response.CancellationPolicyCode.CancellationPolicyCode ObjCanPolicyCode)
        {
            ObjCanPolicyCode = new GRNLib.Response.CancellationPolicyCode.CancellationPolicyCode();
            try
            {
                var Response = JsonConvert.DeserializeObject<dynamic>(Json);
                ObjCanPolicyCode.under_cancellation = Convert.ToBoolean(Response.under_cancellation.Value);
                ObjCanPolicyCode.cancellation_policy_code = Response.cancellation_policy_code.Value;
                ObjCanPolicyCode.amount_type = Response.amount_type.Value;
                ObjCanPolicyCode.no_show_fee.flat_fee = Convert.ToSingle(Response.no_show_fee.flat_fee.Value);
                ObjCanPolicyCode.no_show_fee.currency = Response.no_show_fee.currency.Value;
                ObjCanPolicyCode.no_show_fee.amount_type = Response.no_show_fee.amount_type.Value;
                ObjCanPolicyCode.details = new List<GRNLib.Response.CancellationPolicyCode.Detail>();
                Int64 DetailCount = Convert.ToInt64(Response.details.Count);
                for (int i = 0; i < DetailCount; i++)
                {
                    GRNLib.Response.CancellationPolicyCode.Detail ObjDetail = new GRNLib.Response.CancellationPolicyCode.Detail();
                    ObjDetail.from = Response.details[i].from.Value;
                    ObjDetail.flat_fee = Convert.ToSingle(Response.details[i].flat_fee.Value);
                    ObjDetail.currency = Response.details[i].currency.Value;
                    ObjCanPolicyCode.details.Add(ObjDetail);
                }
                return true;
            }
            catch (Exception ex)
            {
                string LineNo = ex.StackTrace.ToString();
                return false;
            }
        }
    }
}