﻿using CutAdmin.DataLayer;
using HotelLib.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CutAdmin.Common
{
    public class XMLHelper
    {
        public bool isResponse { get; set; }
        public string session { get; set; }
        public Models.StatusCode objStatusCode { get; set; }
        public GlobalDefault objGlobalDefault { get; set; }
        public Models.Hotel objHotel { get; set; }

        public static HttpContext context { get; set; }
        public System.Globalization.CultureInfo CultureInfo { get; set; }
        public AgentDetailsOnAdmin objAgentDetailsOnAdmin { get; set; }
        //public List<HotelLib.Response.HotelDetail> List_HotelDetail { get; set; }
        // public static bool AvailRequest(string session, out CUT.Models.StatusCode objStatusCode)
        public void AvailRequest()
        {
            try
            {
                //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                HotelValuedAvailRQ objHotelValuedAvailRQ = new HotelValuedAvailRQ();
                Models.AvailRequest objAvail = new Models.AvailRequest();
                List<HotelOccupancy> list_Occupancy = new List<HotelOccupancy>();
                string[] array_input = session.Split('_');
                objHotelValuedAvailRQ.EchoToken = Guid.NewGuid().ToString().Replace("-", "");
                objHotelValuedAvailRQ.Language = "ENG";
                objHotelValuedAvailRQ.PageNumber = 1;
                objHotelValuedAvailRQ.SessionID = Common.ToUnixTimestamp(DateTime.Now).ToString();
                objHotelValuedAvailRQ.UserName = System.Configuration.ConfigurationManager.AppSettings["APIUserName"];
                objHotelValuedAvailRQ.Password = System.Configuration.ConfigurationManager.AppSettings["APIPassword"];
                objHotelValuedAvailRQ.DestinationCode = array_input[0].ToString();
                objAvail.Location = array_input[1].ToString();
                objAvail.HotelName = array_input[7].ToString();
                objHotelValuedAvailRQ.DestinationType = "SIMPLE";
                DateTime fDate = Convert.ToDateTime(array_input[2].ToString(), new System.Globalization.CultureInfo("en-GB"));
                DateTime tDate = Convert.ToDateTime(array_input[3].ToString(), new System.Globalization.CultureInfo("en-GB"));
                objHotelValuedAvailRQ.CheckInDate = fDate.ToString("yyyyMMdd");
                objAvail.CheckIn = fDate.ToString("dd/MM/yyyy");
                objHotelValuedAvailRQ.CheckOutDate = tDate.ToString("yyyyMMdd");
                objAvail.CheckOut = tDate.ToString("dd/MM/yyyy");
                string occpancy = array_input[5].ToString();
                objAvail.DestinationCode = array_input[0].ToString();
                try
                {
                    objAvail.HotelCode = array_input[6].ToString();
                    objAvail.HotelName = array_input[7].ToString();
                    objAvail.StarRating = array_input[8].ToString();
                    objAvail.NationalityCode = array_input[9].ToString();
                }
                catch { }
                //objAvail.NationalityCountry = array_input[10].ToString();
                string[] array_occupancy = occpancy.Split('$');
                int adult = 0;
                int child = 0;
                foreach (string str in array_occupancy)
                {
                    string[] m_array = str.Split('|');
                    adult = adult + Convert.ToInt32(m_array[0]);
                    string[] n_array = m_array[1].ToString().Split('^');
                    child = child + Convert.ToInt32(n_array[0]);
                    List<Customer> list_Customer = new List<Customer>();
                    for (int i = 0; i < n_array.Length; i++)
                    {
                        if (i > 0)
                        {
                            //...............................under test by maqsood...........................................//
                            int Age = Convert.ToInt32(n_array[i]);
                            //...............................under test by maqsood...........................................//
                            if (Age > 0)
                                list_Customer.Add(new Customer { Age = Age, type = "CH" });
                        }
                    }
                    list_Occupancy.Add(new HotelOccupancy { RoomCount = 1, AdultCount = Convert.ToInt32(m_array[0]), ChildCount = Convert.ToInt32(n_array[0]), GuestList = list_Customer });
                }
                //...............................under test by maqsood...........................................//
                List<Occupancy> DISTINST_OCCUPANCY = new List<Occupancy>();
                foreach (HotelOccupancy Check_Duplicate_Occupancy in list_Occupancy)
                {
                    if (!DISTINST_OCCUPANCY.Exists(data => data.RoomCount == Check_Duplicate_Occupancy.RoomCount && data.AdultCount == Check_Duplicate_Occupancy.AdultCount && data.ChildCount == Check_Duplicate_Occupancy.ChildCount))
                        DISTINST_OCCUPANCY.Add(new Occupancy { RoomCount = Check_Duplicate_Occupancy.RoomCount, AdultCount = Check_Duplicate_Occupancy.AdultCount, ChildCount = Check_Duplicate_Occupancy.ChildCount });
                }
            
                //...............................under test by maqsood...........................................//
                List<HotelOccupancy> LIST_FINAL_OCCUPANCY = new List<HotelOccupancy>();
                //List<HotelOccupancy> LIST_F_OCCUPANCY = new List<HotelOccupancy>();
                int k = 0;
                foreach (Occupancy objOccupancy in DISTINST_OCCUPANCY)
                {
                    //int k = 0;
                    var Occupancy_List = list_Occupancy.Where(data => data.AdultCount == objOccupancy.AdultCount && data.ChildCount == objOccupancy.ChildCount).ToList();
                    int RoomCount = 0;
                    int AdultCount = 0;
                    int ChildCount = 0;
                    List<Customer> ListCustomer = new List<Customer>();
                    foreach (HotelOccupancy objHotelOccupancy in Occupancy_List)
                    {
                        RoomCount = RoomCount + objHotelOccupancy.RoomCount;
                        ChildCount = objHotelOccupancy.ChildCount;
                        AdultCount = objHotelOccupancy.AdultCount;
                        foreach (Customer objCustomer in objHotelOccupancy.GuestList)
                        {
                            ListCustomer.Add(objCustomer);
                        }
                    }
                    LIST_FINAL_OCCUPANCY.Add(new HotelOccupancy { RoomCount = RoomCount, AdultCount = AdultCount, ChildCount = ChildCount, GuestList = ListCustomer });
                }
               


                objAvail.Room = list_Occupancy.Sum(data => data.RoomCount);
                objAvail.Adult = list_Occupancy.Sum(data => data.AdultCount);
                objAvail.Child = list_Occupancy.Sum(data => data.ChildCount);
                objAvail.List_Occupancy = LIST_FINAL_OCCUPANCY;
                objAvail.List_ActualOccupancy = list_Occupancy;
                string HotelCode = array_input[6].ToString();
                objHotelValuedAvailRQ.HotelOccupancy = LIST_FINAL_OCCUPANCY;
                if (!String.IsNullOrEmpty(HotelCode))
                    objHotelValuedAvailRQ.sHotelCodeList = new HotelCodeList { withinResults = "Y", ProductCode = HotelCode };

                string xml = objHotelValuedAvailRQ.GenerateXML();
                string m_response = "";
                string RequestHeader = "";
                string ResponseHeader = "";
                int Status = 0;
                List<Occupancy> m_List_Hotel_Occupancy = new List<Occupancy>();
                foreach (HotelOccupancy objOccupancy in LIST_FINAL_OCCUPANCY)
                {
                    m_List_Hotel_Occupancy.Add(new Occupancy { RoomCount = objOccupancy.RoomCount, AdultCount = objOccupancy.AdultCount, ChildCount = objOccupancy.ChildCount });
                }

                List<Occupancy> m_ListF_Hotel_Occupancy = new List<Occupancy>();
                foreach (HotelOccupancy objOccupancy in list_Occupancy)
                {
                    m_ListF_Hotel_Occupancy.Add(new Occupancy { RoomCount = objOccupancy.RoomCount, AdultCount = objOccupancy.AdultCount, ChildCount = objOccupancy.ChildCount });
                }

               
                bool bResponse = objHotelValuedAvailRQ.Post(xml, out m_response, out RequestHeader, out ResponseHeader, out Status);
                isResponse = bResponse;
                /*.......................*/
                objAvail.Night = Convert.ToInt32((tDate - fDate).TotalDays);
                objStatusCode = new Models.StatusCode { Request = xml, Response = m_response, RequestHeader = RequestHeader, ResponseHeader = ResponseHeader, Status = Status, DisplayRequest = objAvail, FDate = fDate, TDate = tDate, Occupancy = m_List_Hotel_Occupancy, ActualOccupancy = m_ListF_Hotel_Occupancy };
                //com.Common.Session(out objGlobalDefault);
              //  LogManager.Add(0, objStatusCode.RequestHeader, objStatusCode.Request, objStatusCode.ResponseHeader, objStatusCode.Response, objGlobalDefault.sid, objStatusCode.Status);
                HotelList();
                //return bResponse;
            }
            catch
            {
                objStatusCode = null;
                isResponse = false;
                //return false;
            }
        }

        public void HotelList()
        {
            if (isResponse)
            {
                int count;
                float min_price;
                float max_price;
                int m_counthotel;
                List<HotelLib.Response.HotelDetail> List_HotelDetail;
                count = objStatusCode.DisplayRequest.Night;
                ParseAvailResponse objParseAvailResponse = new ParseAvailResponse(objStatusCode.Response, count, objGlobalDefault.sid, objStatusCode.DisplayRequest.Room, objStatusCode.DisplayRequest.Night, objStatusCode.Occupancy, objStatusCode.ActualOccupancy);
                
                List_HotelDetail = objParseAvailResponse.GetServiceHotel(context);
               
                List_HotelDetail = objParseAvailResponse.GetSortedList(List_HotelDetail);
                List_HotelDetail = objParseAvailResponse.RemoveHotelWithoutCancellationPolicy(List_HotelDetail);
               
                min_price = List_HotelDetail.OrderBy(data => data.CUTPrice).Select(data => data.CUTPrice).FirstOrDefault();
                max_price = List_HotelDetail.OrderByDescending(data => data.CUTPrice).Select(data => data.CUTPrice).FirstOrDefault();
                m_counthotel = List_HotelDetail.Where(data => data.CUTPrice == min_price).Count();
                objHotel = new Models.Hotel { MinPrice = min_price, MaxPrice = max_price, Facility = objParseAvailResponse.Facility, HotelDetail = List_HotelDetail, CountHotel = m_counthotel, Location = objParseAvailResponse.Location, Category = objParseAvailResponse.Category, DisplayRequest = objStatusCode.DisplayRequest };

            }

        }
    }
}