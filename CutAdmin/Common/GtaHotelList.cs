﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CommonLib.Response;
using CutAdmin.BL;
namespace CutAdmin.Common
{
    public class GtaHotelList
    {
        DBHandlerDataContext db = new DBHandlerDataContext(); 
        public string Session { get; set; }
        public List<CommonLib.Response.CommonHotelDetails> Hotels { get; set; }
        public  void GetGtaHotels()
        {
              Hotels = new List<CommonHotelDetails>();
              string[] array_input = Session.Split('_');
              var GtaHotelList = (from obj in db.tbl_GTAProperties where obj.City_Code == array_input[0] select obj).ToList();
              foreach(tbl_GTAProperty objHotel in GtaHotelList)
              {
                  //string Facility="";
                  string Address="";
                  if(objHotel.Address1 !=null)
                      Address += objHotel.Address1;
                  if(objHotel.Address2 !=null)
                      Address += " ," +objHotel.Address2;
                  if (objHotel.Address3 != null)
                      Address += " ," + objHotel.Address3;
                  if (objHotel.Address4 != null)
                      Address += " ," + objHotel.Address4;
                  Hotels.Add(new CommonHotelDetails
                  {
                      HotelName=objHotel.HotelName,
                      Category = objHotel.StarRating.ToString(),
                     // Currency = "INR",
                      DateFrom="",
                      DotwCode="",
                      HotelBedsCode="",
                      MGHCode="",
                      GRNCode="",
                      ExpediaCode="",
                      Description = GetDescription(objHotel.HotelCode).ToString(),
                      HotelId=objHotel.HotelCode,
                      Location = GetLocations(objHotel.HotelCode),
                      Address=Address,
                      Facility = GetFacility(objHotel.HotelCode),
                      Image=GetImage(objHotel.HotelCode),
                      Langitude=objHotel.Longitude.ToString(),
                      Latitude=objHotel.Latitude.ToString(),
                      GTACode ="GTA",
                      Supplier = "GTA",
                  });
              }
        }
        public List<Location> GetLocations(string HotelId)
        {
            //List<tbl_CommonHotelMaster> Locations = new List<tbl_CommonHotelMaster>();
            List<Location> Locations = new List<Location>();
            var GtaLocationList= (from obj in db.tbl_GTANearByLocations where obj.HotelID == HotelId select obj).ToList();
            foreach (var objLocation in GtaLocationList)
              {
                 Locations.Add(new Location{HotelCode=HotelId,Name=objLocation.Location,Supplier="GTA",Count=0,});
               }
             return Locations;
        }
        public List<String> GetFacility(string HotelId)
         {
             List<String> Facilities = new List<String>();
             var GtaFacilitiesList = (from obj in db.tbl_GTAHotelFacilities  where obj.HotelID == HotelId select obj).ToList();
             foreach (var objFacility in GtaFacilitiesList)
             {
                 Facilities.Add(objFacility.FacilityName);
             }
             return Facilities;
         }
        public List<Image> GetImage(string HotelId)
        {
            List<Image> Images = new List<Image>();
            var GtaImage = (from obj in db.tbl_GTAHotelImages where obj.HotelID == HotelId select obj).ToList();
            foreach (var objImage in GtaImage)
            {
                Images.Add(new Image { Title=objImage.Title,Url=objImage.Path,Count=0,});
            }
            return Images;
        }
        public String GetDescription(string HotelId)
        {
            String Descriptions =String.Empty;
            try
            {
                Descriptions = (from obj in db.tbl_GTAReports
                                where (obj.HotelID == HotelId && obj.ReportType == "general")
                                select obj.ReportText).FirstOrDefault();

                if (Descriptions == null)
                    Descriptions = "";
                return Descriptions;
            }
            catch
            {

            }
            
            return Descriptions;
        }
       
    }
   
} 



