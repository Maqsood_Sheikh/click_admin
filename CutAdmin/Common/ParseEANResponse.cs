﻿using CutAdmin.DataLayer;
using CutAdmin.Models;
using EANLib.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;

namespace CutAdmin.Common
{
    public class ParseEANResponse
    {
        public HttpContext Context { get; set; }
        #region Common Mathod & Variables
        public List<string> List_Facility = new List<string>();
        List<HotelLib.Response.Location> List_Location = new List<HotelLib.Response.Location>();
        List<EANLib.Response.Category> List_Category = new List<EANLib.Response.Category>();
        public static float Getgreatervalue(float Percentage, float Ammount)
        {
            if (Percentage > Ammount)
            {
                return Percentage;
            }
            else
            {
                return Ammount;
            }
        }
        #region Methods
        public DateTime CencelletionDate(string ArrivelDate, Int64 CencelletionHours)
        {
            DateTime CencelationDate = new DateTime();
            CencelationDate = DateTime.ParseExact(ArrivelDate, "MM/dd/yyyy", CultureInfo.InvariantCulture).AddDays(-(CencelletionHours / 24) - 1);
            return CencelationDate;
        }
        public List<CancellationInfo> GetCancellationInfo(List<CancellationInfo> listCancellationInfo, List<NightlyRate> listNightlyRate, float ExpediaAmount, Int64 Night, Int64 Rooms, bool nonRefundable)
        {
            List<CancellationInfo> CancelletionAmt = new List<CancellationInfo>();
            GlobalDefault objGlobalDefault = (GlobalDefault)Context.Session["LoginUser"];
            MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();
            if (Context.Session["markups"] != null)
                objMarkupsAndTaxes = (MarkupsAndTaxes)Context.Session["markups"];
            else if (Context.Session["AgentMarkupsOnAdmin"] != null)
                objMarkupsAndTaxes = (MarkupsAndTaxes)Context.Session["AgentMarkupsOnAdmin"];
            bool TaxOnMarkup = objMarkupsAndTaxes.listGroupMarkup.Single(data => data.Supplier == "HotelBeds").TaxOnMarkup;

            CancelletionAmt = listCancellationInfo;
            if (nonRefundable == false)
            {
                if (CancelletionAmt[0].amount != null)
                {
                    ExpediaAmount = ExpediaAmount - Convert.ToSingle(CancelletionAmt[0].amount);
                    CancelletionAmt[1].CancellationPrice = ExpediaAmount;
                }
                else if (CancelletionAmt[0].Percentage != null)
                {
                    ExpediaAmount = ExpediaAmount - (Convert.ToSingle(CancelletionAmt[0].Percentage) / 100);
                    CancelletionAmt[1].CancellationPrice = ExpediaAmount;
                }
                else if (CancelletionAmt[0].NightCount != null)
                {
                    ExpediaAmount = ExpediaAmount - (Convert.ToSingle(listNightlyRate[0].baseRate) / 100);
                    CancelletionAmt[1].CancellationPrice = ExpediaAmount;
                }
            }

            float DollerRate = objGlobalDefault.ExchangeRate.Single(data => data.Currency == "USD").Rate;
            ExpediaAmount = ExpediaAmount * DollerRate; // amount In Doller //
            #region Cut Net Ammount
            float GlobalMarkup = 0;
            float GroupMarkup = 0;
            float IndividualMarkup = 0;

            float CutGlobalMarkupAmt = 0;
            float CutGlobalMarkupPer = 0;
            float ActualGlobalMarkupAmt = 0;

            float CutGroupMarkupAmt = 0;
            float CutGroupMarkupPer = 0;
            float ActualGroupMarkupAmt = 0;

            float CutIndividualMarkupAmt = 0;
            float CutIndividualMarkupPer = 0;
            float ActualIndividualMarkupAmt = 0;

            float ActualAgentRoomMarkup = 0;
            float CutRoomAmount = 0;
            float AgentRoomMarkup = 0;
            float AgentOwnMarkupPer = 0;
            float AgentOwnMarkupAmt = 0;
            float CutRoomAmountAmt = 0;
            //int Night = Convert.ToInt16(results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.ChargeableRateInfo.NightlyRatesPerRoom.size.Value);
            string currency = objGlobalDefault.Currency;
            //CUT.Admin.DataLayer.ExchangeRateManager.GetForeignExchangeRate(currency, out dtResult);
            float ExchangeRate = 0;
            if (currency == "INR")
            {
                ExchangeRate = 1;
            }
            else
            {
                //ExchangeRate = (float)Convert.ToDecimal(dtResult.Rows[0]["INR"].ToString());
                ExchangeRate = objGlobalDefault.ExchangeRate.Single(CurrencyType => CurrencyType.Currency == currency).Rate;
            }
            ExpediaAmount = ExpediaAmount / ExchangeRate;
            if (currency == "INR")
            {

                float GlobalMarkupPer = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GlobalMarkupPer;
                CutGlobalMarkupPer = ((GlobalMarkupPer / 100) * ExpediaAmount);
                float GlobalMarkAmt = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GlobalMarkAmt;
                CutGlobalMarkupAmt = (Rooms * Night) * GlobalMarkAmt;
                ActualGlobalMarkupAmt = Getgreatervalue(CutGlobalMarkupPer, CutGlobalMarkupAmt);
                GlobalMarkup = ActualGlobalMarkupAmt + ExpediaAmount;
                float GroupMarkupPer = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GroupMarkupPer;
                CutGroupMarkupPer = ((GroupMarkupPer / 100) * GlobalMarkup);
                float GroupMarkAmt = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GroupMarkAmt;
                CutGroupMarkupAmt = (Rooms * Night) * GroupMarkAmt;
                ActualGroupMarkupAmt = Getgreatervalue(CutGroupMarkupPer, CutGroupMarkupAmt);
                GroupMarkup = ActualGroupMarkupAmt + GlobalMarkup;

                if (objMarkupsAndTaxes.listIndividualMarkup != null)
                {
                    float IndMarkupPer = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").IndMarkupPer;
                    CutIndividualMarkupPer = ((IndMarkupPer / 100) * GroupMarkup);
                    float IndMarkAmt = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").IndMarkAmt;
                    CutIndividualMarkupAmt = (Rooms * Night) * IndMarkAmt;
                    ActualIndividualMarkupAmt = Getgreatervalue(CutIndividualMarkupPer, CutIndividualMarkupAmt);
                    IndividualMarkup = ActualIndividualMarkupAmt + GroupMarkup;
                }

                CutRoomAmountAmt = ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt;

                //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                float AgentMarkupPer = objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupPer;
                AgentOwnMarkupPer = ((AgentMarkupPer / 100) * IndividualMarkup);
                //AgentOwnMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupAmt;
                ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                //End Agent Markup //

                //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                //AgentOwnMarkupAmt = (noRooms * Night) * objMarkupsAndTaxes.AgentOwnMarkupAmt;
                ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                if (objMarkupsAndTaxes.listGroupMarkup.Single(GroupDetails => GroupDetails.Supplier == "HotelBeds").TaxOnMarkup == false)
                {
                    CutRoomAmountAmt = CutRoomAmountAmt + (objMarkupsAndTaxes.PerServiceTax * CutRoomAmountAmt / 100);
                }
                //else
                //{
                //    CutRoomAmount = (ExpediaAmount + CutRoomAmountAmt);
                //}
                CancelletionAmt[1].CUTCancellationPrice = CutRoomAmount + ExpediaAmount;
                AgentRoomMarkup = ActualAgentRoomMarkup;

                //objChargeableRateInfo.CutRoomAmmount = CutRoomAmount.ToString();
                CancelletionAmt[1].AgentCancellationPrice = AgentRoomMarkup;
            }
            else
            {
                float GlobalMarkupPer = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GlobalMarkupPer;
                CutGlobalMarkupPer = ((GlobalMarkupPer / 100) * ExpediaAmount);
                ActualGlobalMarkupAmt = Getgreatervalue(CutGlobalMarkupPer, CutGlobalMarkupAmt);
                GlobalMarkup = ActualGlobalMarkupAmt + ExpediaAmount;

                float GroupMarkupPer = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GroupMarkupPer;
                CutGroupMarkupPer = ((GroupMarkupPer / 100) * GlobalMarkup);
                ActualGroupMarkupAmt = Getgreatervalue(CutGroupMarkupPer, CutGroupMarkupAmt);
                GroupMarkup = ActualGroupMarkupAmt + GlobalMarkup;

                if (objMarkupsAndTaxes.listIndividualMarkup != null)
                {
                    float IndMarkupPer = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").IndMarkupPer;
                    CutIndividualMarkupPer = ((IndMarkupPer / 100) * GroupMarkup);
                }


                ActualIndividualMarkupAmt = Getgreatervalue(CutIndividualMarkupPer, CutIndividualMarkupAmt);
                IndividualMarkup = ActualIndividualMarkupAmt + GroupMarkup;
                CutRoomAmountAmt = ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt;
                if (objMarkupsAndTaxes.listGroupMarkup.Single(GroupDetails => GroupDetails.Supplier == "Expedia").TaxOnMarkup == false)
                {
                    CutRoomAmountAmt = CutRoomAmountAmt + (objMarkupsAndTaxes.PerServiceTax * CutRoomAmountAmt / 100);
                }
                else
                {
                    CutRoomAmount = (ExpediaAmount + CutRoomAmountAmt);
                }

                //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                float AgentMarkupPer = objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupPer;
                AgentOwnMarkupPer = ((AgentMarkupPer / 100) * IndividualMarkup);
                //AgentOwnMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupAmt;
                ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                CancelletionAmt[1].AgentCancellationPrice = CutRoomAmount;
                //End Agent Markup //

                //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                //ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);

                CutRoomAmount = (ExpediaAmount + CutRoomAmountAmt);
                CancelletionAmt[1].CUTCancellationPrice = CutRoomAmount;
                AgentRoomMarkup = ActualAgentRoomMarkup;
                CancelletionAmt[1].AgentCancellationPrice = AgentRoomMarkup;
            }
            #endregion  Cut Net Ammount
            return CancelletionAmt;

        }
        #endregion
        #endregion Common Mathod

        #region Hotel Search
        public bool ParseXML(string xmlResponse, EANStatusCode objEANStatusCode, out EANLib.Response.HotelListResponse objHotelListResponse)
        {
            objHotelListResponse = new HotelListResponse();
            int noRooms = objEANStatusCode.noRooms;
            cachedSupplierResponse objcachedSupplierResponse = new cachedSupplierResponse();
            var serializer = new JavaScriptSerializer();
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)Context.Session["LoginUser"];
                MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();
                if (Context.Session["markups"] != null)
                    objMarkupsAndTaxes = (MarkupsAndTaxes)Context.Session["markups"];
                else if (Context.Session["AgentMarkupsOnAdmin"] != null)
                    objMarkupsAndTaxes = (MarkupsAndTaxes)Context.Session["AgentMarkupsOnAdmin"];
                // var dict = serializer.Deserialize<Dictionary<string, dynamic>>(xmlResponse);
                xmlResponse = xmlResponse.Replace("@", "");
                var dict = JsonConvert.DeserializeObject<dynamic>(xmlResponse);
                string Catch = Convert.ToString(dict.HotelListResponse.customerSessionId.Value.ToString());
                objHotelListResponse.customerSessionId = Convert.ToString(dict.HotelListResponse.customerSessionId.Value.ToString());
                objHotelListResponse.numberOfRoomsRequested = Convert.ToInt16(dict.HotelListResponse.numberOfRoomsRequested.Value);
                objHotelListResponse.moreResultsAvailable = Convert.ToBoolean(dict.HotelListResponse.moreResultsAvailable.Value);
                bool TaxOnMarkup = objMarkupsAndTaxes.listGroupMarkup.Single(data => data.Supplier == "HotelBeds").TaxOnMarkup;
                float HotelTDSPer = objMarkupsAndTaxes.SupplierCommision.Single(data => data.Supplier == "Expedia").TDS;
                float HotelCommPer = objMarkupsAndTaxes.SupplierCommision.Single(data => data.Supplier == "Expedia").Commision;
                float NetPayble = 0;
                decimal TDS = 0;
                //objHotelListResponse.cacheKey = Convert.ToString(dict["HotelListResponse"]["cacheKey"]);
                //objHotelListResponse.cacheLocation = Convert.ToString(dict["HotelListResponse"]["cacheLocation"]);
                //objcachedSupplierResponse.@supplierCacheTolerance = Convert.ToString(dict["HotelListResponse"]["cachedSupplierResponse"]["@supplierCacheTolerance"]);
                //objcachedSupplierResponse = dict["HotelListResponse"]["cachedSupplierResponsee"].ToString();
                int noofHotel = dict.HotelListResponse.HotelList.HotelSummary.Count;
                EANLib.Response.Category objCategory = new EANLib.Response.Category();

                var results = JsonConvert.DeserializeObject<dynamic>(xmlResponse);
                var id = results.HotelListResponse.customerSessionId.Value;
                //var name = results.Name;
                List<HotelSummary> HotelDetails = new List<HotelSummary>();
                HotelList objHotelList = new HotelList();
                for (int i = 0; i < noofHotel; i++)
                {
                    HotelSummary objHotelSummary = new HotelSummary();
                    List<string> Address = new List<string>();
                    List<ValueAdd> Services = new List<ValueAdd>();
                    RoomRateDetailsList objRoomRateDetailsList = new RoomRateDetailsList();
                    RoomRateDetails objRoomRateDetails = new RoomRateDetails();
                    BedTypes objBedTypes = new BedTypes();
                    List<BedType> listBedType = new List<BedType>();
                    objHotelSummary.Supplier = "Expedia";
                    objHotelSummary.DateTo = objEANStatusCode.TDate.ToString();
                    objHotelSummary.DateFrom = objEANStatusCode.FDate.ToString();
                    //List<RoomRateDetails> RoomList = new List<RoomRateDetails>();
                    int Night = Convert.ToInt16(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.NightlyRatesPerRoom.size.Value);
                    try
                    {
                        objHotelSummary.order = dict.HotelListResponse.HotelList.HotelSummary[i].order.Value.ToString();
                        objHotelSummary.ubsScore = dict.HotelListResponse.HotelList.HotelSummary[i].ubsScore.Value.ToString();
                        objHotelSummary.hotelId = dict.HotelListResponse.HotelList.HotelSummary[i].hotelId.Value.ToString();
                        objHotelSummary.name = dict.HotelListResponse.HotelList.HotelSummary[i].name.Value.ToString();
                        Address.Add(dict.HotelListResponse.HotelList.HotelSummary[i].address1.Value.ToString());
                        try
                        {
                            Address.Add(dict.HotelListResponse.HotelList.HotelSummary[i].address2.Value.ToString());
                        }
                        catch
                        {

                        }
                        objHotelSummary.address1 = Address;
                        objHotelSummary.city = dict.HotelListResponse.HotelList.HotelSummary[i].city.Value.ToString();
                        try
                        {
                            objHotelSummary.postalCode = dict.HotelListResponse.HotelList.HotelSummary[i].postalCode.Value.ToString();
                        }
                        catch
                        {
                            objHotelSummary.postalCode = "";
                        }
                        objHotelSummary.countryCode = dict.HotelListResponse.HotelList.HotelSummary[i].countryCode.Value.ToString();
                        objHotelSummary.airportCode = dict.HotelListResponse.HotelList.HotelSummary[i].airportCode.Value.ToString();
                        //objHotelSummary.supplierType = dict.HotelListResponse.HotelList.HotelSummary[i]["supplierType"].ToString();
                        objHotelSummary.propertyCategory = dict.HotelListResponse.HotelList.HotelSummary[i].propertyCategory.Value.ToString();
                        objHotelSummary.hotelRating = dict.HotelListResponse.HotelList.HotelSummary[i].hotelRating.Value.ToString();
                        objHotelSummary.confidenceRating = dict.HotelListResponse.HotelList.HotelSummary[i].confidenceRating.Value.ToString();
                        //objHotelSummary.amenityMask = Convert.ToInt32(dict["HotelListResponse"]["HotelList"]["HotelSummary"[i]]["amenityMask"].ToString());
                        objHotelSummary.tripAdvisorRating = dict.HotelListResponse.HotelList.HotelSummary[i].tripAdvisorRating.Value.ToString();
                        objHotelSummary.tripAdvisorReviewCount = dict.HotelListResponse.HotelList.HotelSummary[i].tripAdvisorReviewCount.Value.ToString();
                        objHotelSummary.tripAdvisorRatingUrl = dict.HotelListResponse.HotelList.HotelSummary[i].tripAdvisorRatingUrl.Value.ToString();
                        objHotelSummary.locationDescription = dict.HotelListResponse.HotelList.HotelSummary[i].locationDescription.Value.ToString();
                        objHotelSummary.shortDescription = dict.HotelListResponse.HotelList.HotelSummary[i].shortDescription.Value.ToString();
                        objHotelSummary.highRate = dict.HotelListResponse.HotelList.HotelSummary[i].highRate.Value.ToString();
                        objHotelSummary.lowRate = dict.HotelListResponse.HotelList.HotelSummary[i].lowRate.Value.ToString();
                        objHotelSummary.rateCurrencyCode = dict.HotelListResponse.HotelList.HotelSummary[i].rateCurrencyCode.Value.ToString();
                        objHotelSummary.latitude = dict.HotelListResponse.HotelList.HotelSummary[i].latitude.Value.ToString();
                        objHotelSummary.longitude = dict.HotelListResponse.HotelList.HotelSummary[i].longitude.Value.ToString();
                        objHotelSummary.proximityDistance = Convert.ToDecimal(dict.HotelListResponse.HotelList.HotelSummary[i].proximityDistance.Value);
                        objHotelSummary.hotelInDestination = dict.HotelListResponse.HotelList.HotelSummary[i].hotelInDestination.Value.ToString();
                        objHotelSummary.thumbNailUrl = dict.HotelListResponse.HotelList.HotelSummary[i].thumbNailUrl.Value.ToString();
                        objHotelSummary.deepLink = dict.HotelListResponse.HotelList.HotelSummary[i].deepLink.Value.ToString();

                        #region Rate Details

                        objRoomRateDetails.roomTypeCode = Convert.ToInt64(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.roomTypeCode.Value);
                        objRoomRateDetails.rateCode = Convert.ToInt64(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.rateCode.Value);
                        objRoomRateDetails.maxRoomOccupancy = Convert.ToInt64(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.maxRoomOccupancy.Value);
                        objRoomRateDetails.quotedRoomOccupancy = Convert.ToInt64(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.quotedRoomOccupancy.Value.ToString());
                        objRoomRateDetails.minGuestAge = Convert.ToInt64(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.minGuestAge.Value.ToString());
                        objRoomRateDetails.roomDescription = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.roomDescription.Value.ToString();
                        objRoomRateDetails.propertyAvailable = Convert.ToBoolean(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.propertyAvailable.Value);
                        objRoomRateDetails.propertyRestricted = Convert.ToBoolean(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.propertyRestricted.Value);
                        objRoomRateDetails.expediaPropertyId = Convert.ToBoolean(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.expediaPropertyId.Value);
                        objRoomRateDetails.smokingPreferences = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.smokingPreferences.Value.ToString();
                        #region objRateInfos
                        RateInfos objRateInfos = new RateInfos();
                        RateInfo objRateInfo = new RateInfo();
                        objRateInfo.@priceBreakdown = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.priceBreakdown.Value.ToString();
                        objRateInfo.@promo = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.promo.Value.ToString();
                        objRateInfo.@rateChange = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.rateChange.Value.ToString();

                        #region Room Group
                        RoomGroup objRoomGroup = new RoomGroup();
                        List<Room> Rooms = new List<Room>();
                        if (noRooms == 1)
                        {
                            Room objRoom = new Room();
                            objRoom.numberOfAdults = Convert.ToInt64(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.RoomGroup.Room.numberOfAdults.Value);
                            objRoom.numberOfChildren = Convert.ToInt64(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.RoomGroup.Room.numberOfChildren.Value);
                            if (objRoom.numberOfChildren != 0)
                            {
                                objRoom.childAges = Convert.ToInt64(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.RoomGroup.Room.childAges.Value);
                            }
                            Rooms.Add(objRoom);
                        }
                        else
                        {
                            //int noofRooms = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.RoomGroup.Count;
                            for (int j = 0; j < noRooms; j++)
                            {
                                Room objRoom = new Room();
                                objRoom.numberOfAdults = Convert.ToInt64(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.RoomGroup.Room[j].numberOfAdults.Value);
                                objRoom.numberOfChildren = Convert.ToInt64(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.RoomGroup.Room[j].numberOfChildren.Value);
                                if (objRoom.numberOfChildren != 0)
                                {
                                    objRoom.childAges = Convert.ToInt64(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.RoomGroup.Room[j].childAges.Value);
                                }
                                Rooms.Add(objRoom);
                            }

                        }

                        objRoomGroup.Room = Rooms;
                        objRateInfo.RoomGroup = objRoomGroup;
                        #endregion

                        #region Chargeable Info
                        float TotalAmt = Convert.ToSingle(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.total.Value);
                        float commissionableUsdTotal = Convert.ToSingle(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.commissionableUsdTotal.Value);
                        float surchargeTotal = Convert.ToSingle(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.surchargeTotal.Value);
                        string HotelCurrency = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.currencyCode.Value.ToString();
                        float dolarRate = 1;
                        if (HotelCurrency != "INR")
                        {
                            dolarRate = (float)objGlobalDefault.ExchangeRate.Single(CurrencyType => CurrencyType.Currency == HotelCurrency).Rate; ;
                        }

                        TotalAmt = TotalAmt * dolarRate;
                        commissionableUsdTotal = commissionableUsdTotal * dolarRate;
                        surchargeTotal = surchargeTotal * dolarRate;
                        string currency = objGlobalDefault.Currency;
                        //CUT.Admin.DataLayer.ExchangeRateManager.GetForeignExchangeRate(currency, out dtResult);
                        float ExchangeRate = 0;
                        if (currency == "INR")
                        {
                            ExchangeRate = 1;
                        }
                        else
                        {
                            ExchangeRate = objGlobalDefault.ExchangeRate.Single(CurrencyType => CurrencyType.Currency == currency).Rate;
                        }

                        #region Commission Logic
                        float GlobalMarkupPer = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GlobalCommPer; // Given Commission (i.e. 7%)
                        float SupplierCommision = commissionableUsdTotal * HotelCommPer;                                                                // Supplier Comm (5775.38 * 9% = 519.78)          
                        float GivenCommision = commissionableUsdTotal * (GlobalMarkupPer / 100);                                                         //  (5775.38 *7%  - 519.78) 
                        float ServiceTax = (SupplierCommision - GivenCommision) * Convert.ToSingle((0.15));                                // ServiceTax //   
                        float LessCommisionEarned = 0;
                        if (GivenCommision == 0)
                            LessCommisionEarned = ServiceTax;// Less Commision Earned // 
                        else
                            LessCommisionEarned = GivenCommision - ServiceTax;
                        TDS = Convert.ToDecimal(LessCommisionEarned * HotelTDSPer);                                                                       // TDS //
                        NetPayble = TotalAmt - (LessCommisionEarned + Convert.ToSingle(TDS));
                        TotalAmt = TotalAmt / ExchangeRate;
                        ServiceTax = ServiceTax / ExchangeRate;
                        commissionableUsdTotal = commissionableUsdTotal / ExchangeRate;
                        LessCommisionEarned = LessCommisionEarned / ExchangeRate;
                        TDS = TDS / Convert.ToDecimal(ExchangeRate);
                        #endregion

                        string AgentRoomAmt = "";
                        ChargeableRateInfo objChargeableRateInfo = new ChargeableRateInfo();
                        objChargeableRateInfo.@averageBaseRate = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.averageBaseRate.Value.ToString();
                        objChargeableRateInfo.@averageRate = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.averageRate.Value.ToString();
                        objChargeableRateInfo.@commissionableUsdTotal = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.commissionableUsdTotal.Value.ToString();
                        objChargeableRateInfo.@currencyCode = HotelCurrency;
                        objChargeableRateInfo.@maxNightlyRate = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.maxNightlyRate.Value.ToString();
                        objChargeableRateInfo.@nightlyRateTotal = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.nightlyRateTotal.Value.ToString();
                        try
                        {
                            objChargeableRateInfo.@surchargeTotal = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.surchargeTotal.Value.ToString();
                        }
                        catch
                        {

                        }


                        //objChargeableRateInfo.@grossProfitOffline = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["@grossProfitOffline"].ToString();
                        //objChargeableRateInfo.@grossProfitOnline = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["@grossProfitOnline"].ToString();
                        //objChargeableRateInfo.@surchargeTotal = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["@surchargeTotal"].ToString();

                        //TotalAmt = Convert.ToSingle(dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["@total"]);
                        //float dolarRate = 1;
                        //if(currency != "INR")
                        //{
                        // .dolarRate = (float)objGlobalDefault.ExchangeRate.Single(CurrencyType => CurrencyType.Currency == currency).Rate; ;
                        //}

                        TotalAmt = NetPayble / ExchangeRate;
                        objChargeableRateInfo.@total = TotalAmt.ToString();
                        objChargeableRateInfo.ServiceTax = ServiceTax;
                        objChargeableRateInfo.TDS = Convert.ToSingle(TDS);
                        objChargeableRateInfo.LessCommEarned = LessCommisionEarned;
                        #region NightlyRatesPerRoom Info
                        int noNights = Night;
                        NightlyRatesPerRoom objNightlyRatesPerRoom = new NightlyRatesPerRoom();
                        List<NightlyRate> listNightlyRate = new List<NightlyRate>();
                        if (noNights == 1)
                        {
                            NightlyRate objNightlyRate = new NightlyRate();
                            objNightlyRate.@baseRate = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.NightlyRatesPerRoom.NightlyRate.baseRate.Value.ToString();
                            objNightlyRate.@rate = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.NightlyRatesPerRoom.NightlyRate.rate.Value.ToString();
                            objNightlyRate.@promo = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.NightlyRatesPerRoom.NightlyRate.promo.Value.ToString();
                            //objNightlyRatesPerRoom.NightlyRate = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["NightlyRatesPerRoom"]["NightlyRate"]["NightlyRate"].ToString();
                            listNightlyRate.Add(objNightlyRate);
                        }
                        else
                        {
                            for (int n = 0; n < noNights; n++)
                            {
                                NightlyRate objNightlyRate = new NightlyRate();
                                try
                                {

                                    objNightlyRate.@baseRate = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.NightlyRatesPerRoom.NightlyRate[n].baseRate.Value.ToString();
                                    objNightlyRate.@rate = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.NightlyRatesPerRoom.NightlyRate[n].rate.Value.ToString();
                                    objNightlyRate.@promo = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.NightlyRatesPerRoom.NightlyRate[n].promo.Value.ToString();
                                    //objNightlyRatesPerRoom.NightlyRate = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["NightlyRatesPerRoom"]["NightlyRate"]["NightlyRate"].ToString();
                                    listNightlyRate.Add(objNightlyRate);
                                }
                                catch
                                {
                                    continue;
                                }
                            }
                        }

                        objNightlyRatesPerRoom.size = noNights.ToString();
                        objNightlyRatesPerRoom.NightlyRate = listNightlyRate;
                        objChargeableRateInfo.NightlyRatesPerRoom = objNightlyRatesPerRoom;
                        #endregion

                        #region Surcharges Info
                        Surcharges objSurcharges = new Surcharges();
                        int noSurcharges = Convert.ToInt16(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.Surcharges.size.Value.ToString());
                        List<Surcharge> listSurcharge = new List<Surcharge>();
                        for (int n = 0; n < noSurcharges; n++)
                        {
                            try
                            {
                                Surcharge objSurcharge = new Surcharge();
                                objSurcharge.@type = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.Surcharges.Surcharge[n].type.Value.ToString();
                                objSurcharge.@amount = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.Surcharges.Surcharge[n].amount.Value.ToString();
                                listSurcharge.Add(objSurcharge);
                            }
                            catch
                            {
                                continue;
                            }
                        }
                        objSurcharges.Surcharge = listSurcharge;
                        objChargeableRateInfo.Surcharges = objSurcharges;
                        objRateInfo.ChargeableRateInfo = objChargeableRateInfo;
                        #endregion Surcharges

                        #endregion  Chargeable Info

                        try
                        {
                            objRateInfo.nonRefundable = Convert.ToBoolean(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.nonRefundable.Value);
                            objRateInfo.rateType = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.rateType.Value.ToString();

                            objRateInfo.currentAllotment = Convert.ToInt64(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.currentAllotment.Value);
                            var PromoDec = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.promoDescription;
                            if (PromoDec != null)
                            {
                                objRateInfo.promoDescription = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.promoDescription.Value;
                                objRateInfo.promoId = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.promoId.Value;
                            }

                        }
                        catch
                        {

                        }

                        try
                        {
                            objRateInfo.MandatoryTax = Convert.ToSingle(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.HotelFees.HotelFee.amount.Value) * dolarRate / ExchangeRate;

                        }
                        catch
                        {

                        }
                        //RoomList.Add(objRoomRateDetails);

                        #region Cut Net Ammount
                        float GlobalMarkup = 0;
                        float GroupMarkup = 0;
                        float IndividualMarkup = 0;

                        float CutGlobalMarkupAmt = 0;
                        float CutGlobalMarkupPer = 0;
                        float ActualGlobalMarkupAmt = 0;

                        float CutGroupMarkupAmt = 0;
                        float CutGroupMarkupPer = 0;
                        float ActualGroupMarkupAmt = 0;

                        float CutIndividualMarkupAmt = 0;
                        float CutIndividualMarkupPer = 0;
                        float ActualIndividualMarkupAmt = 0;

                        float ActualAgentRoomMarkup = 0;
                        float CutRoomAmount = 0;
                        float AgentRoomMarkup = 0;
                        float AgentOwnMarkupPer = 0;
                        float AgentOwnMarkupAmt = 0;
                        float CutRoomAmountAmt = 0;



                        if (currency == "INR")
                        {

                            GlobalMarkupPer = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GlobalMarkupPer;
                            CutGlobalMarkupPer = ((GlobalMarkupPer / 100) * TotalAmt);
                            float GlobalMarkAmt = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GlobalMarkAmt;
                            CutGlobalMarkupAmt = (noRooms * Night) * GlobalMarkAmt;
                            ActualGlobalMarkupAmt = Getgreatervalue(CutGlobalMarkupPer, CutGlobalMarkupAmt);
                            GlobalMarkup = ActualGlobalMarkupAmt + TotalAmt;
                            float GroupMarkupPer = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GroupMarkupPer;
                            CutGroupMarkupPer = ((GroupMarkupPer / 100) * GlobalMarkup);
                            float GroupMarkAmt = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GroupMarkAmt;
                            CutGroupMarkupAmt = (noRooms * Night) * GroupMarkAmt;
                            ActualGroupMarkupAmt = Getgreatervalue(CutGroupMarkupPer, CutGroupMarkupAmt);
                            GroupMarkup = ActualGroupMarkupAmt + GlobalMarkup;

                            if (objMarkupsAndTaxes.listIndividualMarkup != null)
                            {
                                float IndMarkupPer = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").IndMarkupPer;
                                CutIndividualMarkupPer = ((IndMarkupPer / 100) * GroupMarkup);
                                float IndMarkAmt = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").IndMarkAmt;
                                CutIndividualMarkupAmt = (noRooms * Night) * IndMarkAmt;
                                ActualIndividualMarkupAmt = Getgreatervalue(CutIndividualMarkupPer, CutIndividualMarkupAmt);
                                IndividualMarkup = ActualIndividualMarkupAmt + GroupMarkup;
                            }

                            CutRoomAmountAmt = ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt;

                            //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                            float AgentMarkupPer = objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupPer;
                            AgentOwnMarkupPer = ((AgentMarkupPer / 100) * IndividualMarkup);
                            //AgentOwnMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupAmt;
                            ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                            //End Agent Markup //



                            //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup); //comment by maqsood
                            //AgentOwnMarkupAmt = (noRooms * Night) * objMarkupsAndTaxes.AgentOwnMarkupAmt;
                            //ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);


                            if (objMarkupsAndTaxes.listGroupMarkup.Single(GroupDetails => GroupDetails.Supplier == "Expedia").TaxOnMarkup == true)
                            {
                                CutRoomAmountAmt = CutRoomAmountAmt + (objMarkupsAndTaxes.PerServiceTax * CutRoomAmountAmt / 100);
                            }
                            //else
                            //{
                            //    CutRoomAmountAmt = (TotalAmt + CutRoomAmountAmt);
                            //}

                            CutRoomAmount = (TotalAmt + CutRoomAmountAmt);
                            AgentRoomMarkup = ActualAgentRoomMarkup;
                            objChargeableRateInfo.CutRoomAmmount = CutRoomAmount.ToString();
                            objChargeableRateInfo.AgentRoomMarkup = AgentRoomMarkup.ToString();
                        }
                        else
                        {
                            GlobalMarkupPer = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GlobalMarkupPer;
                            CutGlobalMarkupPer = ((GlobalMarkupPer / 100) * TotalAmt);
                            ActualGlobalMarkupAmt = Getgreatervalue(CutGlobalMarkupPer, CutGlobalMarkupAmt);
                            GlobalMarkup = ActualGlobalMarkupAmt + TotalAmt;

                            float GroupMarkupPer = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GroupMarkupPer;
                            CutGroupMarkupPer = ((GroupMarkupPer / 100) * GlobalMarkup);
                            ActualGroupMarkupAmt = Getgreatervalue(CutGroupMarkupPer, CutGroupMarkupAmt);
                            GroupMarkup = ActualGroupMarkupAmt + GlobalMarkup;

                            if (objMarkupsAndTaxes.listIndividualMarkup != null)
                            {
                                float IndMarkupPer = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").IndMarkupPer;
                                CutIndividualMarkupPer = ((IndMarkupPer / 100) * GroupMarkup);
                            }


                            ActualIndividualMarkupAmt = Getgreatervalue(CutIndividualMarkupPer, CutIndividualMarkupAmt);
                            IndividualMarkup = ActualIndividualMarkupAmt + GroupMarkup;
                            CutRoomAmountAmt = ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt;
                            if (objMarkupsAndTaxes.listGroupMarkup.Single(GroupDetails => GroupDetails.Supplier == "HotelBeds").TaxOnMarkup == true)
                            {
                                CutRoomAmountAmt = CutRoomAmountAmt + (objMarkupsAndTaxes.PerServiceTax * CutRoomAmountAmt / 100);
                            }
                            else
                            {
                                CutRoomAmount = (TotalAmt + CutRoomAmountAmt);
                            }

                            //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                            float AgentMarkupPer = objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupPer;
                            AgentOwnMarkupPer = ((AgentMarkupPer / 100) * IndividualMarkup);
                            //AgentOwnMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupAmt;
                            ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                            //End Agent Markup //

                            //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                            //ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);


                            CutRoomAmount = (TotalAmt + CutRoomAmountAmt);
                            AgentRoomMarkup = ActualAgentRoomMarkup;
                            objChargeableRateInfo.CutRoomAmmount = CutRoomAmount.ToString();
                            objChargeableRateInfo.AgentRoomMarkup = AgentRoomMarkup.ToString();
                        }
                        #endregion  Cut Net Ammount

                        #region Cancellation Policy
                        objRateInfo.CancellationPolicy = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["cancellationPolicy"].ToString();
                        List<CancellationInfo> CancellationInfo = new List<EANLib.Response.CancellationInfo>();
                        EANLib.Response.CancellationInfo ObjCancellationInfo = new CancellationInfo();
                        EANLib.Response.CancellationInfoList ObjCancellationInfos = new CancellationInfoList();
                        try
                        {
                            ObjCancellationInfo.VersionId = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][0]["versionId"].ToString();
                            ObjCancellationInfo.CancelTime = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][0]["cancelTime"].ToString();
                            ObjCancellationInfo.StartWindowHours = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][0]["startWindowHours"].ToString();
                            ObjCancellationInfo.NightCount = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][0]["nightCount"].ToString();
                            ObjCancellationInfo.CurrencyCode = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][0]["currencyCode"].ToString();
                            ObjCancellationInfo.CancelDate = CencelletionDate(objEANStatusCode.TDate.ToString(), Convert.ToInt64(ObjCancellationInfo.StartWindowHours));
                            try
                            {
                                ObjCancellationInfo.TimeZoneDesc = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][0]["timeZoneDescription"].ToString();
                                ObjCancellationInfo.Percentage = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][0]["percent"].ToString();
                            }
                            catch
                            {

                            }
                            CancellationInfo.Add(ObjCancellationInfo);
                            ObjCancellationInfo = new CancellationInfo();
                            ObjCancellationInfo.VersionId = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][1]["versionId"].ToString();
                            ObjCancellationInfo.CancelTime = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][1]["cancelTime"].ToString();
                            ObjCancellationInfo.StartWindowHours = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][1]["startWindowHours"].ToString();
                            ObjCancellationInfo.NightCount = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][1]["nightCount"].ToString();
                            ObjCancellationInfo.CurrencyCode = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][1]["currencyCode"].ToString();
                            ObjCancellationInfo.CancelDate = CencelletionDate(objEANStatusCode.TDate.ToString(), Convert.ToInt64(ObjCancellationInfo.StartWindowHours));
                            #region CencellationPrice

                            #endregion
                            try
                            {
                                ObjCancellationInfo.TimeZoneDesc = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][1]["timeZoneDescription"].ToString();
                                ObjCancellationInfo.Percentage = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][1]["percent"].ToString();
                                string Amt = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][1]["percent"].ToString();
                            }
                            catch
                            {

                            }
                            CancellationInfo.Add(ObjCancellationInfo);
                        }
                        catch
                        {

                        }
                        ObjCancellationInfos.CancellationInfo = GetCancellationInfo(CancellationInfo, listNightlyRate, (commissionableUsdTotal), noNights, noRooms, objRateInfo.nonRefundable);
                        objRateInfo.CancellationInfoList = ObjCancellationInfos;
                        #endregion

                        objRateInfos.RateInfo = objRateInfo;
                        objRoomRateDetails.RateInfos = objRateInfos;
                        #endregion objRateInfos


                        #region Value Added Service
                        ValueAdds objValueAdds = new ValueAdds();
                        try
                        {
                            objValueAdds.Size = Convert.ToInt64(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.ValueAdds.ValueAdd.Count);
                        }
                        catch
                        {
                            objValueAdds.Size = 0;
                        }
                        if (objValueAdds.Size == 1)
                        {
                            try
                            {

                                ValueAdd objValueAdd = new ValueAdd();
                                objValueAdd.@id = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.ValueAdds.ValueAdd["id"].ToString();
                                objValueAdd.Service = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.ValueAdds.ValueAdd["description"].ToString();
                                objCategory.id = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.ValueAdds.ValueAdd["id"].ToString();
                                objCategory.Ratings = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.ValueAdds.ValueAdd["id"].ToString();
                                Services.Add(objValueAdd);
                                //if (!List_Facility.Contains(objValueAdd.Service))
                                //{
                                List_Facility.Add(objValueAdd.Service);
                                //}
                                //List_Facility.Add(objValueAdd);
                            }
                            catch
                            {
                                ValueAdd objValueAdd = new ValueAdd();
                                objValueAdd.@id = "";
                                objValueAdd.Service = "";
                                Services.Add(objValueAdd);
                            }
                        }
                        else
                        {
                            for (int j = 0; j < objValueAdds.Size; j++)
                            {
                                string ValueAdds = "";
                                try
                                {
                                    ValueAdd objValueAdd = new ValueAdd();
                                    objValueAdd.@id = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.ValueAdds.ValueAdd[j]["id"].ToString();
                                    objValueAdd.Service = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.ValueAdds.ValueAdd[j]["description"].ToString();
                                    //if (!List_Facility.Contains(objValueAdd.Service))
                                    //{
                                    //    List_Facility.Add(objValueAdd.Service);
                                    //}
                                    List_Facility.Add(objValueAdd.Service);
                                    Services.Add(objValueAdd);
                                    objCategory.id = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.ValueAdds.ValueAdd["id"].ToString();
                                    objCategory.Ratings = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.ValueAdds.ValueAdd["id"].ToString();

                                }
                                catch
                                {
                                    ValueAdd objValueAdd = new ValueAdd();
                                    objValueAdd.@id = "";
                                    objValueAdd.Service = "";
                                    Services.Add(objValueAdd);
                                }

                            }
                        }


                        objValueAdds.ValueAdd = Services;
                        objRoomRateDetails.ValueAdds = objValueAdds;
                        #endregion Value Added Service

                        #region BedType

                        try
                        {

                            objBedTypes.size = Convert.ToInt64(dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["BedTypes"]["@size"]);
                        }
                        catch
                        {
                            objBedTypes.size = 0;
                        }
                        if (objBedTypes.size == 1)
                        {
                            try
                            {

                                BedType objBedType = new BedType();
                                objBedType.Id = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["BedTypes"]["BedType"]["@id"].ToString();
                                objBedType.Description = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["BedTypes"]["BedType"]["description"].ToString();
                                listBedType.Add(objBedType);
                            }
                            catch
                            {
                                BedType objBedType = new BedType();
                                objBedType.Id = "";
                                objBedType.Description = "";
                                listBedType.Add(objBedType);
                            }
                        }
                        else
                        {
                            for (int j = 0; j < objValueAdds.Size; j++)
                            {

                                try
                                {
                                    BedType objBedType = new BedType();
                                    objBedType.Id = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["BedTypes"]["BedType"][j]["@id"].ToString();
                                    objBedType.Description = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["BedTypes"]["BedType"][j]["description"].ToString();
                                    listBedType.Add(objBedType);
                                }
                                catch
                                {
                                    ValueAdd objValueAdd = new ValueAdd();
                                    objValueAdd.@id = "";
                                    objValueAdd.Service = "";
                                    Services.Add(objValueAdd);
                                }

                            }
                        }

                        objBedTypes.BedType = listBedType;
                        objRoomRateDetails.BedTypes = objBedTypes;
                        objValueAdds.ValueAdd = Services;
                        objRoomRateDetails.ValueAdds = objValueAdds;
                        #endregion

                        objRoomRateDetailsList.RoomRateDetails = objRoomRateDetails;
                        objHotelSummary.RoomRateDetailsList = objRoomRateDetailsList;
                        HotelDetails.Add(objHotelSummary);

                        #endregion Rate Details



                    }
                    catch
                    {
                        continue;
                    }
                }
                objHotelList.HotelSummary = HotelDetails;
                objHotelListResponse.HotelList = objHotelList;
                return true;
            }
            catch
            {
                return false;
            }

        }


        #endregion

        #region Parse Hotel Details
        public bool ParseHotelSummery(string HotelSummerylResponse, string HotelRoomResponse, EANStatusCode objEANStatusCode, out EANLib.Response.HotelListResponse objHotelListResponse)
        {
            objHotelListResponse = new HotelListResponse();
            int noRooms = objEANStatusCode.noRooms;
            cachedSupplierResponse objcachedSupplierResponse = new cachedSupplierResponse();
            var serializer = new JavaScriptSerializer();
            try
            {
                XmlDocument doc = JsonConvert.DeserializeXmlNode(HotelRoomResponse);
                GlobalDefault objGlobalDefault = (GlobalDefault)Context.Session["LoginUser"];
                MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();
                if (Context.Session["markups"] != null)
                    objMarkupsAndTaxes = (MarkupsAndTaxes)Context.Session["markups"];
                else if (Context.Session["AgentMarkupsOnAdmin"] != null)
                    objMarkupsAndTaxes = (MarkupsAndTaxes)Context.Session["AgentMarkupsOnAdmin"];
                //var dict = serializer.Deserialize<Dictionary<string, dynamic>>(HotelSummerylResponse);
                HotelSummerylResponse = HotelSummerylResponse.Replace("@", "");
                var dict = JsonConvert.DeserializeObject<dynamic>(HotelSummerylResponse);
                string Catch = Convert.ToString(dict["HotelListResponse"]["customerSessionId"]);
                objHotelListResponse.customerSessionId = Convert.ToString(dict["HotelListResponse"]["customerSessionId"]);
                objHotelListResponse.numberOfRoomsRequested = Convert.ToInt16(dict["HotelListResponse"]["numberOfRoomsRequested"]);
                objHotelListResponse.moreResultsAvailable = Convert.ToBoolean(dict["HotelListResponse"]["moreResultsAvailable"]);
                //objHotelListResponse.cacheKey = Convert.ToString(dict["HotelListResponse"]["cacheKey"]);
                //objHotelListResponse.cacheLocation = Convert.ToString(dict["HotelListResponse"]["cacheLocation"]);
                //objcachedSupplierResponse.@supplierCacheTolerance = Convert.ToString(dict["HotelListResponse"]["cachedSupplierResponse"]["@supplierCacheTolerance"]);
                //objcachedSupplierResponse = dict["HotelListResponse"]["cachedSupplierResponsee"].ToString();
                int noofHotel = dict["HotelListResponse"]["HotelList"]["HotelSummary"].Count;
                EANLib.Response.Category objCategory = new EANLib.Response.Category();


                List<HotelSummary> HotelDetails = new List<HotelSummary>();
                HotelList objHotelList = new HotelList();
                for (int i = 0; i < noofHotel; i++)
                {
                    HotelSummary objHotelSummary = new HotelSummary();
                    List<string> Address = new List<string>();
                    List<ValueAdd> Services = new List<ValueAdd>();
                    RoomRateDetailsList objRoomRateDetailsList = new RoomRateDetailsList();
                    RoomRateDetails objRoomRateDetails = new RoomRateDetails();
                    objHotelSummary.Supplier = "Expedia";
                    objHotelSummary.DateTo = objEANStatusCode.TDate.ToString();
                    objHotelSummary.DateFrom = objEANStatusCode.FDate.ToString();
                    //List<RoomRateDetails> RoomList = new List<RoomRateDetails>();
                    try
                    {
                        objHotelSummary.order = dict.HotelListResponse.HotelList.HotelSummary[i].@order.Value.ToString();
                        objHotelSummary.ubsScore = dict.HotelListResponse.HotelList.HotelSummary[i].ubsScore.Value.ToString();
                        objHotelSummary.hotelId = dict.HotelListResponse.HotelList.HotelSummary[i].hotelId.Value.ToString();
                        objHotelSummary.name = dict.HotelListResponse.HotelList.HotelSummary[i].name.Value.ToString();
                        Address.Add(dict.HotelListResponse.HotelList.HotelSummary[i].address1.Value.ToString());
                        try
                        {
                            Address.Add(dict.HotelListResponse.HotelList.HotelSummary[i].address2.Value.ToString());
                        }
                        catch
                        {

                        }
                        objHotelSummary.address1 = Address;
                        objHotelSummary.city = dict.HotelListResponse.HotelList.HotelSummary[i].city.Value.ToString();
                        try
                        {
                            objHotelSummary.postalCode = dict.HotelListResponse.HotelList.HotelSummary[i].postalCode.Value.ToString();
                        }
                        catch
                        {
                            objHotelSummary.postalCode = "";
                        }
                        objHotelSummary.countryCode = dict.HotelListResponse.HotelList.HotelSummary[i].countryCode.Value.ToString();
                        objHotelSummary.airportCode = dict.HotelListResponse.HotelList.HotelSummary[i].airportCode.Value.ToString();
                        //objHotelSummary.supplierType = dict.HotelListResponse.HotelList.HotelSummary[i]["supplierType"].ToString();
                        objHotelSummary.propertyCategory = dict.HotelListResponse.HotelList.HotelSummary[i].propertyCategory.Value.ToString();
                        objHotelSummary.hotelRating = dict.HotelListResponse.HotelList.HotelSummary[i].hotelRating.Value.ToString();
                        objHotelSummary.confidenceRating = dict.HotelListResponse.HotelList.HotelSummary[i].confidenceRating.Value.ToString();
                        //objHotelSummary.amenityMask = Convert.ToInt32(dict["HotelListResponse"]["HotelList"]["HotelSummary"[i]]["amenityMask"].ToString());
                        objHotelSummary.tripAdvisorRating = dict.HotelListResponse.HotelList.HotelSummary[i].tripAdvisorRating.Value.ToString();
                        objHotelSummary.tripAdvisorReviewCount = dict.HotelListResponse.HotelList.HotelSummary[i].tripAdvisorReviewCount.Value.ToString();
                        objHotelSummary.tripAdvisorRatingUrl = dict.HotelListResponse.HotelList.HotelSummary[i].tripAdvisorRatingUrl.Value.ToString();
                        objHotelSummary.locationDescription = dict.HotelListResponse.HotelList.HotelSummary[i].locationDescription.Value.ToString();
                        objHotelSummary.shortDescription = dict.HotelListResponse.HotelList.HotelSummary[i].shortDescription.Value.ToString();
                        objHotelSummary.highRate = dict.HotelListResponse.HotelList.HotelSummary[i].highRate.Value.ToString();
                        objHotelSummary.lowRate = dict.HotelListResponse.HotelList.HotelSummary[i].lowRate.Value.ToString();
                        objHotelSummary.rateCurrencyCode = dict.HotelListResponse.HotelList.HotelSummary[i]["rateCurrencyCode"].ToString();
                        objHotelSummary.latitude = dict.HotelListResponse.HotelList.HotelSummary[i]["latitude"].ToString();
                        objHotelSummary.longitude = dict.HotelListResponse.HotelList.HotelSummary[i]["longitude"].ToString();
                        objHotelSummary.proximityDistance = Convert.ToDecimal(dict.HotelListResponse.HotelList.HotelSummary[i]["proximityDistance"]);
                        objHotelSummary.hotelInDestination = dict.HotelListResponse.HotelList.HotelSummary[i]["hotelInDestination"].ToString();
                        objHotelSummary.thumbNailUrl = dict.HotelListResponse.HotelList.HotelSummary[i]["thumbNailUrl"].ToString();
                        objHotelSummary.deepLink = dict.HotelListResponse.HotelList.HotelSummary[i]["deepLink"].ToString();

                        #region Rate Details

                        objRoomRateDetails.roomTypeCode = Convert.ToInt64(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.roomTypeCode.Value);
                        objRoomRateDetails.rateCode = Convert.ToInt64(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.rateCode.Value);
                        objRoomRateDetails.maxRoomOccupancy = Convert.ToInt64(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.maxRoomOccupancy.Value);
                        objRoomRateDetails.quotedRoomOccupancy = Convert.ToInt64(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.quotedRoomOccupancy.Value.ToString());
                        objRoomRateDetails.minGuestAge = Convert.ToInt64(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.minGuestAge.Value.ToString());
                        objRoomRateDetails.roomDescription = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.roomDescription.Value.ToString();
                        objRoomRateDetails.propertyAvailable = Convert.ToBoolean(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.propertyAvailable.Value);
                        objRoomRateDetails.propertyRestricted = Convert.ToBoolean(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.propertyRestricted.Value);
                        objRoomRateDetails.expediaPropertyId = Convert.ToBoolean(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.expediaPropertyId.Value);

                        #region objRateInfos
                        RateInfos objRateInfos = new RateInfos();
                        RateInfo objRateInfo = new RateInfo();
                        objRateInfo.@priceBreakdown = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["@priceBreakdown"].ToString();
                        objRateInfo.@promo = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["@promo"].ToString();
                        objRateInfo.@rateChange = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["@rateChange"].ToString();

                        #region Room Group
                        RoomGroup objRoomGroup = new RoomGroup();
                        List<Room> Rooms = new List<Room>();
                        if (noRooms == 1)
                        {
                            Room objRoom = new Room();
                            objRoom.numberOfAdults = Convert.ToInt64(dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["RoomGroup"]["Room"]["numberOfAdults"]);
                            objRoom.numberOfChildren = Convert.ToInt64(dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["RoomGroup"]["Room"]["numberOfChildren"]);
                            Rooms.Add(objRoom);
                        }
                        else
                        {
                            int noofRooms = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["RoomGroup"].Count;
                            for (int j = 0; j < noRooms; j++)
                            {
                                Room objRoom = new Room();
                                objRoom.numberOfAdults = Convert.ToInt64(dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["RoomGroup"]["Room"][j]["numberOfAdults"]);
                                objRoom.numberOfChildren = Convert.ToInt64(dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["RoomGroup"]["Room"][j]["numberOfChildren"]);
                                Rooms.Add(objRoom);
                            }

                        }

                        objRoomGroup.Room = Rooms;
                        objRateInfo.RoomGroup = objRoomGroup;
                        #endregion

                        #region Chargeable Info
                        float TotalAmt = Convert.ToSingle(dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["@total"]);
                        string HotelCurrency = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["@currencyCode"].ToString();
                        float dolarRate = 1;
                        if (HotelCurrency != "INR")
                        {
                            dolarRate = (float)objGlobalDefault.ExchangeRate.Single(CurrencyType => CurrencyType.Currency == HotelCurrency).Rate; ;
                        }

                        TotalAmt = TotalAmt * dolarRate;
                        string AgentRoomAmt = "";
                        ChargeableRateInfo objChargeableRateInfo = new ChargeableRateInfo();
                        objChargeableRateInfo.@averageBaseRate = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["@averageBaseRate"].ToString();
                        objChargeableRateInfo.@averageRate = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["@averageRate"].ToString();
                        objChargeableRateInfo.@commissionableUsdTotal = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["@commissionableUsdTotal"].ToString();
                        objChargeableRateInfo.@currencyCode = HotelCurrency;
                        objChargeableRateInfo.@maxNightlyRate = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["@maxNightlyRate"].ToString();
                        objChargeableRateInfo.@nightlyRateTotal = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["@nightlyRateTotal"].ToString();
                        try
                        {
                            objChargeableRateInfo.@surchargeTotal = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["@surchargeTotal"].ToString();
                        }
                        catch
                        {

                        }


                        //objChargeableRateInfo.@grossProfitOffline = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["@grossProfitOffline"].ToString();
                        //objChargeableRateInfo.@grossProfitOnline = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["@grossProfitOnline"].ToString();
                        //objChargeableRateInfo.@surchargeTotal = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["@surchargeTotal"].ToString();
                        #region Cut Net Ammount
                        float GlobalMarkup = 0;
                        float GroupMarkup = 0;
                        float IndividualMarkup = 0;

                        float CutGlobalMarkupAmt = 0;
                        float CutGlobalMarkupPer = 0;
                        float ActualGlobalMarkupAmt = 0;

                        float CutGroupMarkupAmt = 0;
                        float CutGroupMarkupPer = 0;
                        float ActualGroupMarkupAmt = 0;

                        float CutIndividualMarkupAmt = 0;
                        float CutIndividualMarkupPer = 0;
                        float ActualIndividualMarkupAmt = 0;

                        float ActualAgentRoomMarkup = 0;
                        float CutRoomAmount = 0;
                        float AgentRoomMarkup = 0;
                        float AgentOwnMarkupPer = 0;
                        float AgentOwnMarkupAmt = 0;
                        float CutRoomAmountAmt = 0;
                        int Night = Convert.ToInt16(dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["NightlyRatesPerRoom"]["@size"]);
                        string currency = objGlobalDefault.Currency;
                        //CUT.Admin.DataLayer.ExchangeRateManager.GetForeignExchangeRate(currency, out dtResult);
                        float ExchangeRate = 0;
                        if (currency == "INR")
                        {
                            ExchangeRate = 1;
                        }
                        else
                        {
                            //ExchangeRate = (float)Convert.ToDecimal(dtResult.Rows[0]["INR"].ToString());
                            ExchangeRate = objGlobalDefault.ExchangeRate.Single(CurrencyType => CurrencyType.Currency == currency).Rate;
                        }
                        TotalAmt = TotalAmt / ExchangeRate;
                        if (currency == "INR")
                        {

                            float GlobalMarkupPer = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == "HotelBeds").GlobalMarkupPer;
                            CutGlobalMarkupPer = ((GlobalMarkupPer / 100) * TotalAmt);
                            float GlobalMarkAmt = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == "HotelBeds").GlobalMarkAmt;
                            CutGlobalMarkupAmt = (noRooms * Night) * GlobalMarkAmt;
                            ActualGlobalMarkupAmt = Getgreatervalue(CutGlobalMarkupPer, CutGlobalMarkupAmt);
                            GlobalMarkup = ActualGlobalMarkupAmt + TotalAmt;
                            float GroupMarkupPer = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == "HotelBeds").GroupMarkupPer;
                            CutGroupMarkupPer = ((GroupMarkupPer / 100) * GlobalMarkup);
                            float GroupMarkAmt = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == "HotelBeds").GroupMarkAmt;
                            CutGroupMarkupAmt = (noRooms * Night) * GroupMarkAmt;
                            ActualGroupMarkupAmt = Getgreatervalue(CutGroupMarkupPer, CutGroupMarkupAmt);
                            GroupMarkup = ActualGroupMarkupAmt + GlobalMarkup;

                            if (objMarkupsAndTaxes.listIndividualMarkup != null)
                            {
                                float IndMarkupPer = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == "HotelBeds").IndMarkupPer;
                                CutIndividualMarkupPer = ((IndMarkupPer / 100) * GroupMarkup);
                                float IndMarkAmt = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == "HotelBeds").IndMarkAmt;
                                CutIndividualMarkupAmt = (noRooms * Night) * IndMarkAmt;
                                ActualIndividualMarkupAmt = Getgreatervalue(CutIndividualMarkupPer, CutIndividualMarkupAmt);
                                IndividualMarkup = ActualIndividualMarkupAmt + GroupMarkup;
                            }

                            CutRoomAmountAmt = ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt;

                            //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                            float AgentMarkupPer = objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupPer;
                            AgentOwnMarkupPer = ((AgentMarkupPer / 100) * IndividualMarkup);
                            //AgentOwnMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupAmt;
                            ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                            //End Agent Markup //

                            //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                            //AgentOwnMarkupAmt = (noRooms * Night) * objMarkupsAndTaxes.AgentOwnMarkupAmt;
                            ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                            if (objMarkupsAndTaxes.listGroupMarkup.Single(GroupDetails => GroupDetails.Supplier == "HotelBeds").TaxOnMarkup)
                            {
                                CutRoomAmountAmt = CutRoomAmountAmt + (objMarkupsAndTaxes.PerServiceTax * CutRoomAmountAmt / 100);
                            }
                            else
                            {
                                CutRoomAmount = (TotalAmt + CutRoomAmountAmt);
                            }

                            AgentRoomMarkup = ActualAgentRoomMarkup;
                            objChargeableRateInfo.CutRoomAmmount = CutRoomAmount.ToString();
                            objChargeableRateInfo.AgentRoomMarkup = AgentRoomMarkup.ToString();
                        }
                        else
                        {
                            float GlobalMarkupPer = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == "HotelBeds").GlobalMarkupPer;
                            CutGlobalMarkupPer = ((GlobalMarkupPer / 100) * TotalAmt);
                            ActualGlobalMarkupAmt = Getgreatervalue(CutGlobalMarkupPer, CutGlobalMarkupAmt);
                            GlobalMarkup = ActualGlobalMarkupAmt + TotalAmt;

                            float GroupMarkupPer = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == "HotelBeds").GroupMarkupPer;
                            CutGroupMarkupPer = ((GroupMarkupPer / 100) * GlobalMarkup);
                            ActualGroupMarkupAmt = Getgreatervalue(CutGroupMarkupPer, CutGroupMarkupAmt);
                            GroupMarkup = ActualGroupMarkupAmt + GlobalMarkup;

                            if (objMarkupsAndTaxes.listIndividualMarkup != null)
                            {
                                float IndMarkupPer = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == "HotelBeds").IndMarkupPer;
                                CutIndividualMarkupPer = ((IndMarkupPer / 100) * GroupMarkup);
                            }


                            ActualIndividualMarkupAmt = Getgreatervalue(CutIndividualMarkupPer, CutIndividualMarkupAmt);
                            IndividualMarkup = ActualIndividualMarkupAmt + GroupMarkup;
                            CutRoomAmountAmt = ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt;
                            if (objMarkupsAndTaxes.listGroupMarkup.Single(GroupDetails => GroupDetails.Supplier == "HotelBeds").TaxOnMarkup)
                            {
                                CutRoomAmountAmt = CutRoomAmountAmt + (objMarkupsAndTaxes.PerServiceTax * CutRoomAmountAmt / 100);
                            }
                            else
                            {
                                CutRoomAmount = (TotalAmt + CutRoomAmountAmt);
                            }

                            //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                            float AgentMarkupPer = objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupPer;
                            AgentOwnMarkupPer = ((AgentMarkupPer / 100) * IndividualMarkup);
                            //AgentOwnMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupAmt;
                            ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                            //End Agent Markup //

                            //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                            //ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);

                            CutRoomAmount = (TotalAmt + CutRoomAmountAmt);
                            AgentRoomMarkup = ActualAgentRoomMarkup;
                            objChargeableRateInfo.CutRoomAmmount = CutRoomAmount.ToString();
                            objChargeableRateInfo.AgentRoomMarkup = AgentRoomMarkup.ToString();
                        }
                        #endregion  Cut Net Ammount
                        try
                        {
                            objRateInfo.MandatoryTax = Convert.ToSingle(dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["HotelFees"]["HotelFee"]["amount"]) * dolarRate / ExchangeRate;

                        }
                        catch
                        {

                        }
                        //TotalAmt = Convert.ToSingle(dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["@total"]);
                        //float dolarRate = 1;
                        //if(currency != "INR")
                        //{
                        // .dolarRate = (float)objGlobalDefault.ExchangeRate.Single(CurrencyType => CurrencyType.Currency == currency).Rate; ;
                        //}

                        //TotalAmt = TotalAmt * dolarRate;
                        objChargeableRateInfo.@total = TotalAmt.ToString();

                        #region NightlyRatesPerRoom Info
                        int noNights = Night;
                        NightlyRatesPerRoom objNightlyRatesPerRoom = new NightlyRatesPerRoom();
                        List<NightlyRate> listNightlyRate = new List<NightlyRate>();
                        if (noNights == 1)
                        {
                            NightlyRate objNightlyRate = new NightlyRate();
                            objNightlyRate.@baseRate = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["NightlyRatesPerRoom"]["NightlyRate"]["@baseRate"].ToString();
                            objNightlyRate.@rate = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["NightlyRatesPerRoom"]["NightlyRate"]["@rate"].ToString();
                            objNightlyRate.@promo = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["NightlyRatesPerRoom"]["NightlyRate"]["@promo"].ToString();
                            //objNightlyRatesPerRoom.NightlyRate = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["NightlyRatesPerRoom"]["NightlyRate"]["NightlyRate"].ToString();
                            listNightlyRate.Add(objNightlyRate);
                        }
                        else
                        {
                            for (int n = 0; n < noNights; n++)
                            {
                                NightlyRate objNightlyRate = new NightlyRate();
                                try
                                {

                                    objNightlyRate.@baseRate = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["NightlyRatesPerRoom"]["NightlyRate"][n]["@baseRate"].ToString();
                                    objNightlyRate.@rate = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["NightlyRatesPerRoom"]["NightlyRate"][n]["@rate"].ToString();
                                    objNightlyRate.@promo = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["NightlyRatesPerRoom"]["NightlyRate"][n]["@promo"].ToString();
                                    //objNightlyRatesPerRoom.NightlyRate = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["NightlyRatesPerRoom"]["NightlyRate"]["NightlyRate"].ToString();
                                    listNightlyRate.Add(objNightlyRate);
                                }
                                catch
                                {
                                    continue;
                                }
                            }
                        }

                        objNightlyRatesPerRoom.size = noNights.ToString();
                        objNightlyRatesPerRoom.NightlyRate = listNightlyRate;
                        objChargeableRateInfo.NightlyRatesPerRoom = objNightlyRatesPerRoom;
                        #endregion

                        #region Surcharges Info
                        Surcharges objSurcharges = new Surcharges();
                        int noSurcharges = Convert.ToInt16(dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["Surcharges"]["@size"].ToString());
                        List<Surcharge> listSurcharge = new List<Surcharge>();
                        for (int n = 0; n < noSurcharges; n++)
                        {
                            try
                            {
                                Surcharge objSurcharge = new Surcharge();
                                objSurcharge.@type = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["Surcharges"]["Surcharge"][n]["@type"].ToString();
                                objSurcharge.@amount = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["Surcharges"]["Surcharge"][n]["@amount"].ToString();
                                listSurcharge.Add(objSurcharge);
                            }
                            catch
                            {
                                continue;
                            }
                        }
                        objSurcharges.Surcharge = listSurcharge;
                        objChargeableRateInfo.Surcharges = objSurcharges;
                        objRateInfo.ChargeableRateInfo = objChargeableRateInfo;
                        #endregion Surcharges

                        #endregion  Chargeable Info

                        try
                        {
                            objRateInfo.nonRefundable = Convert.ToBoolean(dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["nonRefundable"]);
                            objRateInfo.rateType = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["rateType"].ToString();

                            objRateInfo.currentAllotment = Convert.ToInt64(dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["currentAllotment"]);
                        }
                        catch
                        {

                        }


                        //RoomList.Add(objRoomRateDetails);
                        #region Cancellation Policy
                        objRateInfo.CancellationPolicy = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["cancellationPolicy"].ToString();
                        List<CancellationInfo> CancellationInfo = new List<EANLib.Response.CancellationInfo>();
                        EANLib.Response.CancellationInfo ObjCancellationInfo = new CancellationInfo();
                        EANLib.Response.CancellationInfoList ObjCancellationInfos = new CancellationInfoList();
                        try
                        {
                            ObjCancellationInfo.VersionId = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][0]["versionId"].ToString();
                            ObjCancellationInfo.CancelTime = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][0]["cancelTime"].ToString();
                            ObjCancellationInfo.StartWindowHours = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][0]["startWindowHours"].ToString();
                            ObjCancellationInfo.NightCount = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][0]["nightCount"].ToString();
                            ObjCancellationInfo.CurrencyCode = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][0]["currencyCode"].ToString();

                            try
                            {
                                ObjCancellationInfo.TimeZoneDesc = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][0]["timeZoneDescription"].ToString();
                                ObjCancellationInfo.Percentage = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][0]["percent"].ToString();
                            }
                            catch
                            {

                            }
                            CancellationInfo.Add(ObjCancellationInfo);
                            ObjCancellationInfo = new CancellationInfo();
                            ObjCancellationInfo.VersionId = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][1]["versionId"].ToString();
                            ObjCancellationInfo.CancelTime = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][1]["cancelTime"].ToString();
                            ObjCancellationInfo.StartWindowHours = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][1]["startWindowHours"].ToString();
                            ObjCancellationInfo.NightCount = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][1]["nightCount"].ToString();
                            ObjCancellationInfo.CurrencyCode = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][1]["currencyCode"].ToString();

                            try
                            {
                                ObjCancellationInfo.TimeZoneDesc = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][1]["timeZoneDescription"].ToString();
                                ObjCancellationInfo.Percentage = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["CancelPolicyInfoList"]["CancelPolicyInfo"][0]["percent"].ToString();
                            }
                            catch
                            {

                            }
                            CancellationInfo.Add(ObjCancellationInfo);
                        }
                        catch
                        {

                        }
                        ObjCancellationInfos.CancellationInfo = CancellationInfo;
                        objRateInfo.CancellationInfoList = ObjCancellationInfos;
                        #endregion
                        objRateInfos.RateInfo = objRateInfo;
                        objRoomRateDetails.RateInfos = objRateInfos;
                        #endregion objRateInfos


                        #region Value Added Service
                        ValueAdds objValueAdds = new ValueAdds();
                        try
                        {
                            objValueAdds.Size = Convert.ToInt64(dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.ValueAdds.size.Value);
                        }
                        catch
                        {
                            objValueAdds.Size = 0;
                        }
                        if (objValueAdds.Size == 1)
                        {
                            try
                            {

                                ValueAdd objValueAdd = new ValueAdd();
                                objValueAdd.@id = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.ValueAdds.ValueAdd.id.Value.ToString();
                                objValueAdd.Service = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.ValueAdds.ValueAdd.description.Value.ToString();
                                objCategory.id = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.ValueAdds.ValueAdd.id.Value.ToString();
                                objCategory.Ratings = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.ValueAdds.ValueAdd.id.Value.ToString();
                                Services.Add(objValueAdd);
                                if (!List_Facility.Contains(objValueAdd.Service))
                                {
                                    List_Facility.Add(objValueAdd.Service);
                                }
                                //List_Facility.Add(objValueAdd);
                            }
                            catch
                            {
                                ValueAdd objValueAdd = new ValueAdd();
                                objValueAdd.@id = "";
                                objValueAdd.Service = "";
                                Services.Add(objValueAdd);
                            }
                        }
                        else
                        {
                            for (int j = 0; j < objValueAdds.Size; j++)
                            {

                                try
                                {
                                    ValueAdd objValueAdd = new ValueAdd();
                                    objValueAdd.@id = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.ValueAdds.ValueAdd[j].id.Vale.ToString();
                                    objValueAdd.Service = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.ValueAdds.ValueAdd[j].description.Value.ToString();
                                    Services.Add(objValueAdd);
                                    objCategory.id = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.ValueAdds.ValueAdd.id.Value.ToString();
                                    objCategory.Ratings = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.ValueAdds.ValueAdd.description.Value.ToString();
                                    if (!List_Facility.Contains(objValueAdd.Service))
                                    {
                                        List_Facility.Add(objValueAdd.Service);
                                    }
                                }
                                catch
                                {
                                    ValueAdd objValueAdd = new ValueAdd();
                                    objValueAdd.@id = "";
                                    objValueAdd.Service = "";
                                    Services.Add(objValueAdd);
                                }

                            }
                        }


                        objValueAdds.ValueAdd = Services;
                        objRoomRateDetails.ValueAdds = objValueAdds;
                        #endregion Value Added Service
                        objRoomRateDetailsList.RoomRateDetails = objRoomRateDetails;
                        objHotelSummary.RoomRateDetailsList = objRoomRateDetailsList;
                        HotelDetails.Add(objHotelSummary);

                        #endregion Rate Details



                    }
                    catch
                    {
                        continue;
                    }
                }
                objHotelList.HotelSummary = HotelDetails;
                objHotelListResponse.HotelList = objHotelList;
                return true;
            }
            catch
            {
                return false;
            }

        }

        #endregion


        #region Parse HotelRooms
        public bool ParseHotelRooms(string response, Models.EANHotels objEANHotels, string HotelId, out  HotelRoomAvailabilityResponse objHotelRoomAvailability)
        {
            bool bresponse = false;
            objHotelRoomAvailability = new HotelRoomAvailabilityResponse();
            GlobalDefault objGlobalDefault = (GlobalDefault)Context.Session["LoginUser"];
            MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();
            if (Context.Session["markups"] != null)
                objMarkupsAndTaxes = (MarkupsAndTaxes)Context.Session["markups"];
            else if (Context.Session["AgentMarkupsOnAdmin"] != null)
                objMarkupsAndTaxes = (MarkupsAndTaxes)Context.Session["AgentMarkupsOnAdmin"];
            bool TaxOnMarkup = objMarkupsAndTaxes.listGroupMarkup.Single(data => data.Supplier == "HotelBeds").TaxOnMarkup;
            float HotelTDSPer = objMarkupsAndTaxes.SupplierCommision.Single(data => data.Supplier == "Expedia").TDS;
            float HotelCommPer = objMarkupsAndTaxes.SupplierCommision.Single(data => data.Supplier == "Expedia").Commision;
            float NetPayble = 0;
            decimal TDS = 0;
            EANLib.Response.Category objCategory = new EANLib.Response.Category();
            List<string> Services = new List<string>();
            try
            {
                response = response.Replace("@", "");
                XmlDocument doc = JsonConvert.DeserializeXmlNode(response);
                var results = JsonConvert.DeserializeObject<dynamic>(response);
                List<string> Images = new List<string>();
                //results= results.Replace("@", "");
                objHotelRoomAvailability.hotelRating = GetRatings(objEANHotels.HotelDetail.Single(Details => Details.hotelId == HotelId).hotelRating);
                objHotelRoomAvailability.latitude = objEANHotels.HotelDetail.Single(listHotel => listHotel.hotelId == HotelId).latitude;
                objHotelRoomAvailability.longitude = objEANHotels.HotelDetail.Single(listHotel => listHotel.hotelId == HotelId).longitude;
                objHotelRoomAvailability.noNight = objEANHotels.noNights;
                Int64 nRooms = Convert.ToInt64(results.HotelRoomAvailabilityResponse.size.Value);
                objHotelRoomAvailability.size = Convert.ToInt64(results.HotelRoomAvailabilityResponse.HotelRoomResponse.Count);
                objHotelRoomAvailability.customerSessionId = results.HotelRoomAvailabilityResponse.customerSessionId.Value;
                objHotelRoomAvailability.hotelId = results.HotelRoomAvailabilityResponse.hotelId.Value.ToString();
                objHotelRoomAvailability.arrivalDate = results.HotelRoomAvailabilityResponse.arrivalDate.Value;
                objHotelRoomAvailability.departureDate = results.HotelRoomAvailabilityResponse.departureDate.Value;
                objHotelRoomAvailability.hotelName = results.HotelRoomAvailabilityResponse.hotelName.Value;
                objHotelRoomAvailability.Discription = objEANHotels.HotelDetail.Single(details => details.hotelId == HotelId).shortDescription;
                objHotelRoomAvailability.hotelAddress = results.HotelRoomAvailabilityResponse.hotelAddress.Value;
                objHotelRoomAvailability.hotelCountry = results.HotelRoomAvailabilityResponse.hotelCountry.Value;
                objHotelRoomAvailability.hotelCity = results.HotelRoomAvailabilityResponse.hotelCity.Value;
                objHotelRoomAvailability.numberOfRoomsRequested = Convert.ToInt64(results.HotelRoomAvailabilityResponse.numberOfRoomsRequested.Value);
                objHotelRoomAvailability.checkInInstructions = results.HotelRoomAvailabilityResponse.checkInInstructions.Value;
                var SpCheckIn = results.HotelRoomAvailabilityResponse.specialCheckInInstructions;
                if (SpCheckIn != null)
                    objHotelRoomAvailability.specialCheckInInstructions = results.HotelRoomAvailabilityResponse.specialCheckInInstructions.Value;
                objHotelRoomAvailability.tripAdvisorRating = results.HotelRoomAvailabilityResponse.tripAdvisorRating.Value.ToString();
                objHotelRoomAvailability.tripAdvisorRatingUrl = results.HotelRoomAvailabilityResponse.tripAdvisorRatingUrl.Value;
                //objHotelRoomAvailability.HotelRoomResponse = results.HotelRoomAvailabilityResponse.HotelRoomResponse.Value;
                List<HotelRoomResponse> listHotelRoomResponse = new List<HotelRoomResponse>();
                if (objHotelRoomAvailability.size == 0)
                {
                    #region Single Room
                    // for (int i = 0; i < objHotelRoomAvailability.size; i++)
                    // {
                    HotelRoomResponse objHotelRoomResponse = new HotelRoomResponse();
                    objHotelRoomResponse.rateCode = results.HotelRoomAvailabilityResponse.HotelRoomResponse.rateCode.Value.ToString();
                    objHotelRoomResponse.roomTypeCode = results.HotelRoomAvailabilityResponse.HotelRoomResponse.roomTypeCode.Value.ToString();
                    objHotelRoomResponse.roomTypeDescription = results.HotelRoomAvailabilityResponse.HotelRoomResponse.roomTypeDescription.Value.ToString();
                    objHotelRoomResponse.rateDescription = results.HotelRoomAvailabilityResponse.HotelRoomResponse.rateDescription.Value.ToString();
                    objHotelRoomResponse.supplierType = results.HotelRoomAvailabilityResponse.HotelRoomResponse.supplierType.Value.ToString();
                    objHotelRoomResponse.smokingPreferences = results.HotelRoomAvailabilityResponse.HotelRoomResponse.smokingPreferences.Value.ToString();
                    #region BedTypes
                    BedTypes objBedTypes = new BedTypes();
                    List<BedType> listBedType = new List<BedType>();
                    objBedTypes.size = Convert.ToInt64(results.HotelRoomAvailabilityResponse.HotelRoomResponse.BedTypes.size.Value);
                    if (objBedTypes.size == 1)
                    {
                        BedType objBedType = new BedType();
                        objBedType.Id = results.HotelRoomAvailabilityResponse.HotelRoomResponse.BedTypes.BedType.id.Value.ToString();
                        objBedType.Description = results.HotelRoomAvailabilityResponse.HotelRoomResponse.BedTypes.BedType.description.Value.ToString();
                        listBedType.Add(objBedType);
                        objBedTypes.BedType = listBedType;
                        objHotelRoomResponse.BedTypes = objBedTypes;
                    }
                    else
                    {
                        for (int j = 0; j < objBedTypes.size; j++)
                        {
                            BedType objBedType = new BedType();
                            objBedType.Id = results.HotelRoomAvailabilityResponse.HotelRoomResponse.BedTypes.BedType[j].id.Value.ToString();
                            objBedType.Description = results.HotelRoomAvailabilityResponse.HotelRoomResponse.BedTypes.BedType[j].description.Value.ToString();
                            listBedType.Add(objBedType);
                        }
                        objBedTypes.BedType = listBedType;
                        objHotelRoomResponse.BedTypes = objBedTypes;
                    }

                    #endregion

                    #region Rates Info
                    RateInfos objRateInfos = new RateInfos();
                    RateInfo objRateInfo = new RateInfo();
                    objRateInfos.size = Convert.ToString((results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.RoomGroup.Room.Count));
                    //Int64 noRooms = Convert.ToInt64(objRateInfos.size);
                    float TotalAmt = Convert.ToSingle(results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.ChargeableRateInfo.total.Value);
                    float ExpediaAmt = Convert.ToSingle(results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.ChargeableRateInfo.total.Value);
                    float commissionableUsdTotal = Convert.ToSingle(results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.ChargeableRateInfo.commissionableUsdTotal.Value);
                    float surchargeTotal = Convert.ToSingle(results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.ChargeableRateInfo.surchargeTotal.Value);
                    //string HotelCurrency = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.ChargeableRateInfo.Value.ToString();

                    nRooms = Convert.ToInt64(objRateInfos.size);
                    RoomGroup objRoomGroup = new RoomGroup();
                    List<Room> Rooms = new List<Room>();
                    List<CancellationInfo> CancellationInfo = new List<EANLib.Response.CancellationInfo>();
                    #region Room Group
                    objRateInfo.nonRefundable = Convert.ToBoolean(results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.nonRefundable.Value);
                    if (nRooms == 0)
                    {
                        Room objRoom = new Room();
                        objRoom.numberOfAdults = Convert.ToInt64(results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.RoomGroup.Room.numberOfAdults.Value);
                        objRoom.numberOfChildren = Convert.ToInt64(results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.RoomGroup.Room.numberOfChildren.Value);
                        objRoom.rateKey = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.RoomGroup.Room.rateKey.Value.ToString();
                        if (objRoom.numberOfChildren != 0)
                        {
                            objRoom.numberOfChildren = Convert.ToInt64(results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.RoomGroup.Room.childAges.Value);
                        }
                        objRoom.CancellationInfo = CancellationInfo;
                        Rooms.Add(objRoom);

                    }
                    else
                    {
                        for (int j = 0; j < nRooms; j++)
                        {
                            Room objRoom = new Room();
                            objRoom.numberOfAdults = Convert.ToInt64(results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.RoomGroup.Room[j].numberOfAdults.Value);
                            objRoom.numberOfChildren = Convert.ToInt64(results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.RoomGroup.Room[j].numberOfChildren.Value);
                            objRoom.rateKey = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.RoomGroup.Room[j].rateKey.Value.ToString();
                            if (objRoom.numberOfChildren != 0)
                            {
                                objRoom.numberOfChildren = Convert.ToInt64(results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.RoomGroup.Room[j].childAges.Value);
                            }
                            objRoom.CancellationInfo = CancellationInfo;
                            Rooms.Add(objRoom);
                        }

                    }

                    objRoomGroup.Room = Rooms;
                    objRateInfo.RoomGroup = objRoomGroup;
                    objRateInfos.RateInfo = objRateInfo;
                    objHotelRoomResponse.RateInfos = objRateInfos;
                    #endregion

                    #region Chargeable Info

                    string HotelCurrency = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.ChargeableRateInfo.currencyCode.ToString();
                    float dolarRate = 1;
                    if (HotelCurrency != "INR")
                    {
                        dolarRate = (float)objGlobalDefault.ExchangeRate.Single(CurrencyType => CurrencyType.Currency == HotelCurrency).Rate; ;
                    }

                    TotalAmt = TotalAmt * dolarRate;
                    commissionableUsdTotal = commissionableUsdTotal * dolarRate;
                    surchargeTotal = surchargeTotal * dolarRate;
                    #region Commission Logic
                    float GlobalMarkupPer = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GlobalCommPer; // Given Commission (i.e. 7%)
                    float SupplierCommision = commissionableUsdTotal * HotelCommPer;                                                                // Supplier Comm (5775.38 * 9% = 519.78)          
                    float GivenCommision = commissionableUsdTotal * (GlobalMarkupPer / 100);                                                         //  (5775.38 *7%  - 519.78) 
                    float ServiceTax = (SupplierCommision - GivenCommision) * Convert.ToSingle((0.15)); float LessCommisionEarned = 0;                              // ServiceTax //   
                    if (GivenCommision == 0)
                        LessCommisionEarned = ServiceTax;                                                                         // Less Commision Earned // 
                    else
                        LessCommisionEarned = GivenCommision - ServiceTax;
                    TDS = Convert.ToDecimal(LessCommisionEarned * HotelTDSPer);                                                                       // TDS //
                    NetPayble = TotalAmt - (LessCommisionEarned + Convert.ToSingle(TDS));
                    string currency = objGlobalDefault.Currency;
                    float ExchangeRate = 0;
                    if (currency == "INR")
                    {
                        ExchangeRate = 1;
                    }
                    else
                    {
                        ExchangeRate = objGlobalDefault.ExchangeRate.Single(CurrencyType => CurrencyType.Currency == currency).Rate;
                    }
                    TotalAmt = NetPayble / ExchangeRate;
                    ServiceTax = ServiceTax / ExchangeRate;
                    LessCommisionEarned = LessCommisionEarned / ExchangeRate;
                    commissionableUsdTotal = commissionableUsdTotal / ExchangeRate;
                    surchargeTotal = surchargeTotal / ExchangeRate;
                    TDS = TDS / Convert.ToDecimal(ExchangeRate);
                    #endregion
                    string AgentRoomAmt = "";
                    ChargeableRateInfo objChargeableRateInfo = new ChargeableRateInfo();
                    objChargeableRateInfo.@averageBaseRate = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.ChargeableRateInfo.averageBaseRate.Value.ToString();

                    objChargeableRateInfo.@averageRate = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.ChargeableRateInfo.averageRate.Value.ToString();
                    objChargeableRateInfo.@commissionableUsdTotal = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.ChargeableRateInfo.commissionableUsdTotal.Value.ToString();
                    objChargeableRateInfo.@currencyCode = HotelCurrency;
                    objChargeableRateInfo.@maxNightlyRate = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.ChargeableRateInfo.maxNightlyRate.Value.ToString();
                    objChargeableRateInfo.@nightlyRateTotal = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.ChargeableRateInfo.nightlyRateTotal.Value.ToString();

                    objChargeableRateInfo.LessCommEarned = LessCommisionEarned;
                    objChargeableRateInfo.ServiceTax = ServiceTax;
                    objChargeableRateInfo.TDS = Convert.ToSingle(TDS);
                    objChargeableRateInfo.total = TotalAmt.ToString();
                    try
                    {
                        objChargeableRateInfo.@surchargeTotal = surchargeTotal.ToString();
                    }
                    catch
                    {

                    }


                    #region Cut Net Ammount
                    float GlobalMarkup = 0;
                    float GroupMarkup = 0;
                    float IndividualMarkup = 0;

                    float CutGlobalMarkupAmt = 0;
                    float CutGlobalMarkupPer = 0;
                    float ActualGlobalMarkupAmt = 0;

                    float CutGroupMarkupAmt = 0;
                    float CutGroupMarkupPer = 0;
                    float ActualGroupMarkupAmt = 0;

                    float CutIndividualMarkupAmt = 0;
                    float CutIndividualMarkupPer = 0;
                    float ActualIndividualMarkupAmt = 0;

                    float ActualAgentRoomMarkup = 0;
                    float CutRoomAmount = 0;
                    float AgentRoomMarkup = 0;
                    float AgentOwnMarkupPer = 0;
                    float AgentOwnMarkupAmt = 0;
                    float CutRoomAmountAmt = 0;
                    int Night = Convert.ToInt16(results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.ChargeableRateInfo.NightlyRatesPerRoom.size.Value);

                    if (currency == "INR")
                    {

                        GlobalMarkupPer = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GlobalMarkupPer;
                        CutGlobalMarkupPer = ((GlobalMarkupPer / 100) * TotalAmt);
                        float GlobalMarkAmt = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GlobalMarkAmt;
                        CutGlobalMarkupAmt = (Rooms.Count * Night) * GlobalMarkAmt;
                        ActualGlobalMarkupAmt = Getgreatervalue(CutGlobalMarkupPer, CutGlobalMarkupAmt);
                        GlobalMarkup = ActualGlobalMarkupAmt + TotalAmt;
                        float GroupMarkupPer = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GroupMarkupPer;
                        CutGroupMarkupPer = ((GroupMarkupPer / 100) * GlobalMarkup);
                        float GroupMarkAmt = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GroupMarkAmt;
                        CutGroupMarkupAmt = (Rooms.Count * Night) * GroupMarkAmt;
                        ActualGroupMarkupAmt = Getgreatervalue(CutGroupMarkupPer, CutGroupMarkupAmt);
                        GroupMarkup = ActualGroupMarkupAmt + GlobalMarkup;

                        if (objMarkupsAndTaxes.listIndividualMarkup != null)
                        {
                            float IndMarkupPer = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").IndMarkupPer;
                            CutIndividualMarkupPer = ((IndMarkupPer / 100) * GroupMarkup);
                            float IndMarkAmt = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").IndMarkAmt;
                            CutIndividualMarkupAmt = (Rooms.Count * Night) * IndMarkAmt;
                            ActualIndividualMarkupAmt = Getgreatervalue(CutIndividualMarkupPer, CutIndividualMarkupAmt);
                            IndividualMarkup = ActualIndividualMarkupAmt + GroupMarkup;
                        }

                        CutRoomAmountAmt = ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt;

                        //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                        float AgentMarkupPer = objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupPer;
                        AgentOwnMarkupPer = ((AgentMarkupPer / 100) * IndividualMarkup);
                        //AgentOwnMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupAmt;
                        ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                        //End Agent Markup //

                        //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                        //AgentOwnMarkupAmt = (noRooms * Night) * objMarkupsAndTaxes.AgentOwnMarkupAmt;
                        ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                        if (objMarkupsAndTaxes.listGroupMarkup.Single(GroupDetails => GroupDetails.Supplier == "HotelBeds").TaxOnMarkup == false)
                        {
                            CutRoomAmountAmt = CutRoomAmountAmt + (objMarkupsAndTaxes.PerServiceTax * CutRoomAmountAmt / 100);
                        }
                        //else
                        //{
                        //    CutRoomAmount = (TotalAmt + CutRoomAmountAmt);
                        //}

                        AgentRoomMarkup = ActualAgentRoomMarkup;
                        objChargeableRateInfo.CutRoomAmmount = (TotalAmt + CutRoomAmountAmt).ToString();
                        objChargeableRateInfo.AgentRoomMarkup = AgentRoomMarkup.ToString();
                    }
                    else
                    {
                        GlobalMarkupPer = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GlobalMarkupPer;
                        CutGlobalMarkupPer = ((GlobalMarkupPer / 100) * TotalAmt);
                        ActualGlobalMarkupAmt = Getgreatervalue(CutGlobalMarkupPer, CutGlobalMarkupAmt);
                        GlobalMarkup = ActualGlobalMarkupAmt + TotalAmt;

                        float GroupMarkupPer = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GroupMarkupPer;
                        CutGroupMarkupPer = ((GroupMarkupPer / 100) * GlobalMarkup);
                        ActualGroupMarkupAmt = Getgreatervalue(CutGroupMarkupPer, CutGroupMarkupAmt);
                        GroupMarkup = ActualGroupMarkupAmt + GlobalMarkup;

                        if (objMarkupsAndTaxes.listIndividualMarkup != null)
                        {
                            float IndMarkupPer = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").IndMarkupPer;
                            CutIndividualMarkupPer = ((IndMarkupPer / 100) * GroupMarkup);
                        }


                        ActualIndividualMarkupAmt = Getgreatervalue(CutIndividualMarkupPer, CutIndividualMarkupAmt);
                        IndividualMarkup = ActualIndividualMarkupAmt + GroupMarkup;
                        CutRoomAmountAmt = ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt;
                        //if (objMarkupsAndTaxes.listGroupMarkup.Single(GroupDetails => GroupDetails.Supplier == "HotelBeds").TaxOnMarkup == false)
                        //{
                        //    CutRoomAmountAmt = CutRoomAmountAmt + (objMarkupsAndTaxes.PerServiceTax * CutRoomAmountAmt / 100);
                        //}
                        //else
                        //{
                        //    CutRoomAmount = (TotalAmt + CutRoomAmountAmt);
                        //}

                        //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                        float AgentMarkupPer = objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupPer;
                        AgentOwnMarkupPer = ((AgentMarkupPer / 100) * IndividualMarkup);
                        //AgentOwnMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupAmt;
                        ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                        //End Agent Markup //

                        //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                        //ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);

                        CutRoomAmount = (TotalAmt + CutRoomAmountAmt);
                        AgentRoomMarkup = ActualAgentRoomMarkup;
                        objChargeableRateInfo.CutRoomAmmount = CutRoomAmount.ToString();
                        objChargeableRateInfo.AgentRoomMarkup = AgentRoomMarkup.ToString();
                    }
                    #endregion  Cut Net Ammount

                    try
                    {
                        objRateInfo.MandatoryTax = Convert.ToSingle(results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.HotelFees.HotelFee.amount.Value) * dolarRate / ExchangeRate;

                    }
                    catch
                    {

                    }


                    #region NightlyRatesPerRoom Info
                    //noNights = Night;
                    NightlyRatesPerRoom objNightlyRatesPerRoom = new NightlyRatesPerRoom();
                    List<NightlyRate> listNightlyRate = new List<NightlyRate>();
                    if (Night == 1)
                    {
                        NightlyRate objNightlyRate = new NightlyRate();
                        objNightlyRate.@baseRate = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.ChargeableRateInfo.NightlyRatesPerRoom.NightlyRate.baseRate.Value.ToString();
                        objNightlyRate.@rate = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.ChargeableRateInfo.NightlyRatesPerRoom.NightlyRate.rate.Value.ToString();
                        objNightlyRate.@promo = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.ChargeableRateInfo.NightlyRatesPerRoom.NightlyRate.promo.ToString();
                        listNightlyRate.Add(objNightlyRate);
                    }
                    else
                    {
                        for (int n = 0; n < Night; n++)
                        {
                            NightlyRate objNightlyRate = new NightlyRate();
                            try
                            {

                                objNightlyRate.@baseRate = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.ChargeableRateInfo.NightlyRatesPerRoom.NightlyRate[n].baseRate.ToString();
                                objNightlyRate.@rate = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.ChargeableRateInfo.NightlyRatesPerRoom.NightlyRate[n].rate.ToString();
                                objNightlyRate.@promo = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.ChargeableRateInfo.NightlyRatesPerRoom.NightlyRate[n].promo.ToString();
                                //objNightlyRatesPerRoom.NightlyRate = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["NightlyRatesPerRoom"]["NightlyRate"]["NightlyRate"].ToString();
                                listNightlyRate.Add(objNightlyRate);
                            }
                            catch
                            {
                                continue;
                            }
                        }
                    }

                    objNightlyRatesPerRoom.size = Night.ToString();
                    objNightlyRatesPerRoom.NightlyRate = listNightlyRate;
                    objChargeableRateInfo.NightlyRatesPerRoom = objNightlyRatesPerRoom;
                    #endregion

                    #region Surcharges Info
                    Surcharges objSurcharges = new Surcharges();
                    int noSurcharges = Convert.ToInt16(results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.ChargeableRateInfo.Surcharges.size.Value.ToString());
                    List<Surcharge> listSurcharge = new List<Surcharge>();
                    for (int n = 0; n < noSurcharges; n++)
                    {
                        try
                        {
                            Surcharge objSurcharge = new Surcharge();
                            //objSurcharge.@type = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.ChargeableRateInfo.Surcharges.Surcharge[n].type.Value.ToString();
                            objSurcharge.@amount = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.ChargeableRateInfo.Surcharges.Surcharge[n].amount.Value.ToString();
                            listSurcharge.Add(objSurcharge);
                        }
                        catch
                        {
                            continue;
                        }
                    }
                    objSurcharges.Surcharge = listSurcharge;
                    objChargeableRateInfo.Surcharges = objSurcharges;
                    objRateInfo.ChargeableRateInfo = objChargeableRateInfo;
                    #endregion Surcharges

                    #endregion  Chargeable Info

                    #region Cancellation Policy
                    objRateInfo.CancellationPolicy = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.cancellationPolicy.Value.ToString();

                    EANLib.Response.CancellationInfo ObjCancellationInfo = new CancellationInfo();
                    EANLib.Response.CancellationInfoList ObjCancellationInfos = new CancellationInfoList();
                    try
                    {

                        try
                        {
                            ObjCancellationInfo.VersionId = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[0].versionId.Value.ToString();
                            ObjCancellationInfo.CancelTime = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[0].cancelTime.Value.ToString();
                            ObjCancellationInfo.StartWindowHours = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[0].startWindowHours.Value.ToString();
                            ObjCancellationInfo.NightCount = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[0].nightCount.Value.ToString();
                            ObjCancellationInfo.CurrencyCode = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[0].currencyCode.Value.ToString();
                            ObjCancellationInfo.CancelDate = CencelletionDate(objHotelRoomAvailability.arrivalDate, Convert.ToInt64(ObjCancellationInfo.StartWindowHours));
                            //ObjCancellationInfo.TimeZoneDesc = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[0].timeZoneDescription.Value.ToString();
                            //ObjCancellationInfo.Percentage = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[0].percent.Value.ToString();
                            var TimeZone = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[0].timeZoneDescription;
                            if (TimeZone != null)
                                ObjCancellationInfo.TimeZoneDesc = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[0].timeZoneDescription.Value.ToString();
                            var Percentage = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[0].percent;
                            if (Percentage != null)
                                ObjCancellationInfo.Percentage = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[0].percent.Value.ToString();
                            var Amt = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[0].amount;
                            if (Amt != null)
                                ObjCancellationInfo.amount = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[0].amount;
                        }
                        catch
                        {

                        }
                        CancellationInfo.Add(ObjCancellationInfo);
                        ObjCancellationInfo = new CancellationInfo();
                        try
                        {
                            ObjCancellationInfo.VersionId = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[1].versionId.Value.ToString();
                            ObjCancellationInfo.CancelTime = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[1].cancelTime.Value.ToString();
                            ObjCancellationInfo.StartWindowHours = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[1].startWindowHours.Value.ToString();
                            ObjCancellationInfo.NightCount = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[1].nightCount.Value.ToString();
                            ObjCancellationInfo.CurrencyCode = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[1].currencyCode.Value.ToString();
                            ObjCancellationInfo.CancelDate = CencelletionDate(objHotelRoomAvailability.arrivalDate, Convert.ToInt64(ObjCancellationInfo.StartWindowHours));
                            //    ObjCancellationInfo.TimeZoneDesc = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[1].timeZoneDescription.Value.ToString();
                            //    ObjCancellationInfo.Percentage = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[0].percent.Value.ToString();
                            var TimeZone = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[1].timeZoneDescription;
                            if (TimeZone != null)
                                ObjCancellationInfo.TimeZoneDesc = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[1].timeZoneDescription.Value.ToString();
                            var Percentage = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[1].percent;
                            if (Percentage != null)
                                ObjCancellationInfo.Percentage = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[1].percent.Value.ToString();
                            var Amt = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[1].amount;
                            if (Amt != null)
                                ObjCancellationInfo.amount = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[1].amount;
                        }
                        catch
                        {

                        }
                        CancellationInfo.Add(ObjCancellationInfo);
                    }
                    catch
                    {

                    }
                    ObjCancellationInfos.CancellationInfo = GetCancellationInfo(CancellationInfo, listNightlyRate, commissionableUsdTotal, Night, Rooms.Count, objRateInfo.nonRefundable);
                    objRateInfo.CancellationInfoList = ObjCancellationInfos;
                    #endregion





                    try
                    {
                        objRateInfo.nonRefundable = Convert.ToBoolean(results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.nonRefundable.Value);
                        objRateInfo.rateType = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.rateType.Value.ToString();

                        objRateInfo.currentAllotment = Convert.ToInt64(results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.currentAllotment.Value);
                    }
                    catch
                    {

                    }


                    //RoomList.Add(objRoomRateDetails);

                    objRateInfos.RateInfo = objRateInfo;
                    objHotelRoomResponse.RateInfos = objRateInfos;

                    #endregion  Rates Info

                    #region Value Added Service
                    ValueAdds objValueAdds = new ValueAdds();
                    try
                    {
                        objValueAdds.Size = Convert.ToInt64(results.HotelRoomAvailabilityResponse.HotelRoomResponse.ValueAdds.size.Value);
                    }
                    catch
                    {
                        objValueAdds.Size = 0;
                    }
                    if (objValueAdds.Size == 1)
                    {
                        try
                        {

                            ValueAdd objValueAdd = new ValueAdd();
                            //objValueAdd.@id = results.HotelRoomAvailabilityResponse.HotelRoomResponse.ValueAdds.ValueAdd.id.ToString();
                            objValueAdd.Service = results.HotelRoomAvailabilityResponse.HotelRoomResponse.ValueAdds.ValueAdd.description.Value.ToString();
                            //objCategory.id =      results.HotelRoomAvailabilityResponse.HotelRoomResponse.ValueAdds.ValueAdd.id.Value.ToString();
                            //objCategory.Ratings = results.HotelRoomAvailabilityResponse.HotelRoomResponse.ValueAdds.ValueAdd.id.id.Value.ToString();
                            //Services.Add(objValueAdd);
                            if (!Services.Contains(objValueAdd.Service))
                            {
                                Services.Add(objValueAdd.Service);
                            }
                            //List_Facility.Add(objValueAdd);
                        }
                        catch
                        {
                            ValueAdd objValueAdd = new ValueAdd();
                            objValueAdd.@id = "";
                            objValueAdd.Service = "";
                            //Services.Add(objValueAdd);
                        }
                    }
                    else
                    {
                        for (int j = 0; j < objValueAdds.Size; j++)
                        {

                            try
                            {
                                ValueAdd objValueAdd = new ValueAdd();
                                //objValueAdd.@id = results.HotelRoomAvailabilityResponse.HotelRoomAvailabilityResponse.HotelRoomResponse[i].ValueAdds.ValueAdd[j].id.Value.ToString();
                                objValueAdd.Service = results.HotelRoomAvailabilityResponse.HotelRoomResponse.ValueAdds.ValueAdd[j].description.Value.ToString();
                                //Services.Add(objValueAdd);
                                if (!Services.Contains(objValueAdd.Service))
                                {
                                    Services.Add(objValueAdd.Service);
                                }
                            }
                            catch
                            {
                                ValueAdd objValueAdd = new ValueAdd();
                                objValueAdd.@id = "";
                                objValueAdd.Service = "";
                                //Services.Add(objValueAdd);
                            }

                        }
                    }


                    //objValueAdds.ValueAdd = Services;
                    objHotelRoomResponse.ValueAdds = objValueAdds;
                    #endregion Value Added Service

                    #region deepLink
                    objHotelRoomResponse.deeplink = results.HotelRoomAvailabilityResponse.HotelRoomResponse.deepLink.Value.ToString();
                    #endregion

                    #region Hotel Images
                    List<HotelImage> listHotelImage = new List<HotelImage>();
                    int noImages = Convert.ToInt16(results.HotelRoomAvailabilityResponse.HotelRoomResponse.RoomImages.size.Value);
                    HotelImage objHotelImage = new HotelImage();
                    objHotelImage.thumbnailUrl = "http://media.expedia.com" + objEANHotels.HotelDetail.Single(Details => Details.hotelId == HotelId).thumbNailUrl.Replace("_t", "_b");
                    Images.Add(objHotelImage.thumbnailUrl);
                    listHotelImage.Add(objHotelImage);
                    objHotelRoomResponse.HotelImage = listHotelImage;
                    if (noImages != 1)
                    {
                        for (int j = 0; j < noImages; j++)
                        {
                            objHotelImage = new HotelImage();
                            objHotelImage.thumbnailUrl = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RoomImages.RoomImage[j].url.Value.ToString().Replace("_s", "_b");
                            Images.Add(objHotelImage.thumbnailUrl);
                            //objHotelRoomAvailability.HotelImages.Add(results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RoomImages.RoomImage[j].url.Value.ToString());
                            listHotelImage.Add(objHotelImage);
                        }
                    }

                    objHotelRoomResponse.HotelImage = listHotelImage;
                    #endregion

                    listHotelRoomResponse.Add(objHotelRoomResponse);
                    //}
                    #endregion
                }
                else
                {
                    #region Multipule Rooms
                    for (int i = 0; i < objHotelRoomAvailability.size; i++)
                    {
                        HotelRoomResponse objHotelRoomResponse = new HotelRoomResponse();
                        objHotelRoomResponse.rateCode = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].rateCode.Value.ToString();
                        objHotelRoomResponse.roomTypeCode = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].roomTypeCode.Value.ToString();
                        objHotelRoomResponse.roomTypeDescription = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].roomTypeDescription.Value.ToString();
                        objHotelRoomResponse.rateDescription = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].rateDescription.Value.ToString();
                        objHotelRoomResponse.supplierType = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].supplierType.Value.ToString();
                        objHotelRoomResponse.smokingPreferences = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].smokingPreferences.Value.ToString();
                        #region BedTypes
                        BedTypes objBedTypes = new BedTypes();
                        List<BedType> listBedType = new List<BedType>();
                        objBedTypes.size = Convert.ToInt64(results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].BedTypes.size.Value);
                        if (objBedTypes.size == 1)
                        {
                            BedType objBedType = new BedType();
                            objBedType.Id = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].BedTypes.BedType.id.Value.ToString();
                            objBedType.Description = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].BedTypes.BedType.description.Value.ToString();
                            listBedType.Add(objBedType);
                            objBedTypes.BedType = listBedType;
                            objHotelRoomResponse.BedTypes = objBedTypes;
                        }
                        else
                        {
                            for (int j = 0; j < objBedTypes.size; j++)
                            {
                                BedType objBedType = new BedType();
                                objBedType.Id = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].BedTypes.BedType[j].id.Value.ToString();
                                objBedType.Description = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].BedTypes.BedType[j].description.Value.ToString();
                                listBedType.Add(objBedType);
                            }
                            objBedTypes.BedType = listBedType;
                            objHotelRoomResponse.BedTypes = objBedTypes;
                        }

                        #endregion

                        #region Rates Info
                        RateInfos objRateInfos = new RateInfos();
                        RateInfo objRateInfo = new RateInfo();
                        objRateInfos.size = Convert.ToString((results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.RoomGroup.Room.Count));
                        Int64 noRateInfo = Convert.ToInt64(objRateInfos.size);
                        float TotalAmt = Convert.ToSingle(results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.ChargeableRateInfo.total.Value);
                        float commissionableUsdTotal = Convert.ToSingle(results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.ChargeableRateInfo.commissionableUsdTotal.Value);
                        float surchargeTotal = Convert.ToSingle(results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.ChargeableRateInfo.surchargeTotal.Value);
                        //string HotelCurrency = dict.HotelListResponse.HotelList.HotelSummary[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.currencyCode.Value.ToString();

                        float ExpediaAmt = Convert.ToSingle(results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.ChargeableRateInfo.total.Value);
                        nRooms = Convert.ToInt64(objRateInfos.size);
                        RoomGroup objRoomGroup = new RoomGroup();
                        List<Room> Rooms = new List<Room>();
                        List<CancellationInfo> CancellationInfo = new List<EANLib.Response.CancellationInfo>();
                        #region Room Group

                        if (nRooms == 0)
                        {
                            Room objRoom = new Room();
                            objRoom.numberOfAdults = Convert.ToInt64(results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.RoomGroup.Room.numberOfAdults.Value);
                            objRoom.numberOfChildren = Convert.ToInt64(results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.RoomGroup.Room.numberOfChildren.Value);
                            objRoom.rateKey = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.RoomGroup.Room.rateKey.Value.ToString();
                            if (objRoom.numberOfChildren != 0)
                            {
                                objRoom.numberOfChildren = Convert.ToInt64(results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.RoomGroup.Room.childAges.Value);
                            }
                            objRoom.CancellationInfo = CancellationInfo;
                            Rooms.Add(objRoom);
                        }
                        else
                        {
                            for (int k = 0; k < nRooms; k++)
                            {
                                Room objRoom = new Room();
                                objRoom.numberOfAdults = Convert.ToInt64(results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.RoomGroup.Room[k].numberOfAdults.Value);
                                objRoom.numberOfChildren = Convert.ToInt64(results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.RoomGroup.Room[k].numberOfChildren.Value);
                                objRoom.rateKey = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.RoomGroup.Room[k].rateKey.Value.ToString();
                                if (objRoom.numberOfChildren != 0)
                                {
                                    objRoom.childAges = Convert.ToInt64(results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.RoomGroup.Room[k].childAges.Value);
                                }
                                objRoom.CancellationInfo = CancellationInfo;
                                Rooms.Add(objRoom);
                            }

                        }

                        objRoomGroup.Room = Rooms;
                        objRateInfo.RoomGroup = objRoomGroup;
                        objRateInfos.RateInfo = objRateInfo;
                        objHotelRoomResponse.RateInfos = objRateInfos;
                        #endregion

                        #region Chargeable Info
                        objRateInfo.nonRefundable = Convert.ToBoolean(results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.nonRefundable.Value);
                        string HotelCurrency = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.ChargeableRateInfo.currencyCode.ToString();
                        float dolarRate = 1;
                        if (HotelCurrency != "INR")
                        {
                            dolarRate = (float)objGlobalDefault.ExchangeRate.Single(CurrencyType => CurrencyType.Currency == HotelCurrency).Rate; ;
                        }

                        TotalAmt = TotalAmt * dolarRate;
                        commissionableUsdTotal = commissionableUsdTotal * dolarRate;
                        surchargeTotal = surchargeTotal * dolarRate;
                        #region Commission Logic
                        float GlobalMarkupPer = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GlobalCommPer; // Given Commission (i.e. 7%)
                        float SupplierCommision = commissionableUsdTotal * HotelCommPer;                                                                // Supplier Comm (5775.38 * 9% = 519.78)          
                        float GivenCommision = commissionableUsdTotal * (GlobalMarkupPer / 100);                                                         //  (5775.38 *7%  - 519.78) 
                        float ServiceTax = (SupplierCommision - GivenCommision) * Convert.ToSingle((0.15));                                // ServiceTax //   
                        float LessCommisionEarned = 0;                              // ServiceTax //   
                        if (GivenCommision == 0)
                            LessCommisionEarned = ServiceTax;                                                                         // Less Commision Earned // 
                        else
                            LessCommisionEarned = GivenCommision - ServiceTax;                                                                       // Less Commision Earned // 
                        TDS = Convert.ToDecimal(LessCommisionEarned * HotelTDSPer);                                                                       // TDS //
                        NetPayble = TotalAmt - (LessCommisionEarned + Convert.ToSingle(TDS));
                        string currency = objGlobalDefault.Currency;
                        float ExchangeRate = 0;
                        if (currency == "INR")
                        {
                            ExchangeRate = 1;
                        }
                        else
                        {
                            ExchangeRate = objGlobalDefault.ExchangeRate.Single(CurrencyType => CurrencyType.Currency == currency).Rate;
                        }
                        TotalAmt = NetPayble / ExchangeRate;
                        ServiceTax = ServiceTax / ExchangeRate;
                        LessCommisionEarned = LessCommisionEarned / ExchangeRate;
                        commissionableUsdTotal = commissionableUsdTotal / ExchangeRate;
                        TDS = TDS / Convert.ToDecimal(ExchangeRate);
                        surchargeTotal = surchargeTotal / ExchangeRate;
                        #endregion
                        string AgentRoomAmt = "";
                        ChargeableRateInfo objChargeableRateInfo = new ChargeableRateInfo();
                        objChargeableRateInfo.@averageBaseRate = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.ChargeableRateInfo.averageBaseRate.Value.ToString();

                        objChargeableRateInfo.@averageRate = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.ChargeableRateInfo.averageRate.Value.ToString();
                        objChargeableRateInfo.@commissionableUsdTotal = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.ChargeableRateInfo.commissionableUsdTotal.Value.ToString();
                        objChargeableRateInfo.@currencyCode = HotelCurrency;
                        objChargeableRateInfo.@maxNightlyRate = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.ChargeableRateInfo.maxNightlyRate.Value.ToString();
                        objChargeableRateInfo.@nightlyRateTotal = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.ChargeableRateInfo.nightlyRateTotal.Value.ToString();
                        objChargeableRateInfo.@total = TotalAmt.ToString();
                        objChargeableRateInfo.TDS = Convert.ToSingle(TDS);
                        objChargeableRateInfo.commissionableUsdTotal = commissionableUsdTotal.ToString();
                        objChargeableRateInfo.ServiceTax = ServiceTax;
                        objChargeableRateInfo.LessCommEarned = LessCommisionEarned;
                        objChargeableRateInfo.TDS = Convert.ToSingle(TDS);

                        try
                        {
                            objChargeableRateInfo.@surchargeTotal = surchargeTotal.ToString();
                        }
                        catch
                        {

                        }



                        #region Cut Net Ammount
                        float GlobalMarkup = 0;
                        float GroupMarkup = 0;
                        float IndividualMarkup = 0;

                        float CutGlobalMarkupAmt = 0;
                        float CutGlobalMarkupPer = 0;
                        float ActualGlobalMarkupAmt = 0;

                        float CutGroupMarkupAmt = 0;
                        float CutGroupMarkupPer = 0;
                        float ActualGroupMarkupAmt = 0;

                        float CutIndividualMarkupAmt = 0;
                        float CutIndividualMarkupPer = 0;
                        float ActualIndividualMarkupAmt = 0;

                        float ActualAgentRoomMarkup = 0;
                        float CutRoomAmount = 0;
                        float AgentRoomMarkup = 0;
                        float AgentOwnMarkupPer = 0;
                        float AgentOwnMarkupAmt = 0;
                        float CutRoomAmountAmt = 0;
                        int Night = Convert.ToInt16(results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.ChargeableRateInfo.NightlyRatesPerRoom.size.Value);
                        if (currency == "INR")
                        {

                            GlobalMarkupPer = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GlobalMarkupPer;
                            CutGlobalMarkupPer = ((GlobalMarkupPer / 100) * TotalAmt);
                            float GlobalMarkAmt = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GlobalMarkAmt;
                            CutGlobalMarkupAmt = (Rooms.Count * Night) * GlobalMarkAmt;
                            ActualGlobalMarkupAmt = Getgreatervalue(CutGlobalMarkupPer, CutGlobalMarkupAmt);
                            GlobalMarkup = ActualGlobalMarkupAmt + TotalAmt;
                            float GroupMarkupPer = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GroupMarkupPer;
                            CutGroupMarkupPer = ((GroupMarkupPer / 100) * GlobalMarkup);
                            float GroupMarkAmt = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GroupMarkAmt;
                            CutGroupMarkupAmt = (Rooms.Count * Night) * GroupMarkAmt;
                            ActualGroupMarkupAmt = Getgreatervalue(CutGroupMarkupPer, CutGroupMarkupAmt);
                            GroupMarkup = ActualGroupMarkupAmt + GlobalMarkup;

                            if (objMarkupsAndTaxes.listIndividualMarkup != null)
                            {
                                float IndMarkupPer = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").IndMarkupPer;
                                CutIndividualMarkupPer = ((IndMarkupPer / 100) * GroupMarkup);
                                float IndMarkAmt = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").IndMarkAmt;
                                CutIndividualMarkupAmt = (Rooms.Count * Night) * IndMarkAmt;
                                ActualIndividualMarkupAmt = Getgreatervalue(CutIndividualMarkupPer, CutIndividualMarkupAmt);
                                IndividualMarkup = ActualIndividualMarkupAmt + GroupMarkup;
                            }
                            CutRoomAmountAmt = ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt;
                            if (TaxOnMarkup == true)
                            {
                                CutRoomAmountAmt = CutRoomAmountAmt + (CutRoomAmountAmt * objMarkupsAndTaxes.PerServiceTax / 100);
                            }


                            //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                            float AgentMarkupPer = objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupPer;
                            AgentOwnMarkupPer = ((AgentMarkupPer / 100) * IndividualMarkup);
                            //AgentOwnMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupAmt;
                            ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                            //End Agent Markup //

                            //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                            //AgentOwnMarkupAmt = (noRooms * Night) * objMarkupsAndTaxes.AgentOwnMarkupAmt;
                            ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                            if (objMarkupsAndTaxes.listGroupMarkup.Single(GroupDetails => GroupDetails.Supplier == "Expedia").TaxOnMarkup == false)
                            {
                                CutRoomAmountAmt = TotalAmt + (objMarkupsAndTaxes.PerServiceTax * TotalAmt / 100);
                            }
                            else
                            {
                                CutRoomAmountAmt = (TotalAmt + CutRoomAmountAmt);
                            }

                            AgentRoomMarkup = ActualAgentRoomMarkup;
                            objChargeableRateInfo.CutRoomAmmount = CutRoomAmountAmt.ToString();
                            objChargeableRateInfo.AgentRoomMarkup = AgentRoomMarkup.ToString();
                        }
                        else
                        {
                            GlobalMarkupPer = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GlobalMarkupPer;
                            CutGlobalMarkupPer = ((GlobalMarkupPer / 100) * TotalAmt);
                            ActualGlobalMarkupAmt = Getgreatervalue(CutGlobalMarkupPer, CutGlobalMarkupAmt);
                            GlobalMarkup = ActualGlobalMarkupAmt + TotalAmt;

                            float GroupMarkupPer = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").GroupMarkupPer;
                            CutGroupMarkupPer = ((GroupMarkupPer / 100) * GlobalMarkup);
                            ActualGroupMarkupAmt = Getgreatervalue(CutGroupMarkupPer, CutGroupMarkupAmt);
                            GroupMarkup = ActualGroupMarkupAmt + GlobalMarkup;

                            if (objMarkupsAndTaxes.listIndividualMarkup != null)
                            {
                                float IndMarkupPer = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == "Expedia").IndMarkupPer;
                                CutIndividualMarkupPer = ((IndMarkupPer / 100) * GroupMarkup);
                            }


                            ActualIndividualMarkupAmt = Getgreatervalue(CutIndividualMarkupPer, CutIndividualMarkupAmt);
                            IndividualMarkup = ActualIndividualMarkupAmt + GroupMarkup;
                            CutRoomAmountAmt = ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt;
                            //if (TaxOnMarkup == true)
                            //{
                            //    CutRoomAmountAmt = CutRoomAmountAmt + (objMarkupsAndTaxes.PerServiceTax * CutRoomAmountAmt / 100);
                            //}
                            //else
                            //{
                            //    CutRoomAmount = (TotalAmt + CutRoomAmountAmt);
                            //}

                            //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                            float AgentMarkupPer = objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupPer;
                            AgentOwnMarkupPer = ((AgentMarkupPer / 100) * IndividualMarkup);
                            //AgentOwnMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupAmt;
                            ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                            //End Agent Markup //

                            //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                            //ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                            //if (objMarkupsAndTaxes.listGroupMarkup.Single(GroupDetails => GroupDetails.Supplier == "Expedia").TaxOnMarkup == false)
                            //{
                            //    CutRoomAmountAmt = TotalAmt + (objMarkupsAndTaxes.PerServiceTax * TotalAmt / 100);
                            //}
                            //else
                            //{
                            //    CutRoomAmountAmt = (TotalAmt + CutRoomAmountAmt);
                            //}
                            CutRoomAmount = (TotalAmt + CutRoomAmountAmt);
                            AgentRoomMarkup = ActualAgentRoomMarkup;
                            objChargeableRateInfo.CutRoomAmmount = CutRoomAmount.ToString();
                            objChargeableRateInfo.AgentRoomMarkup = AgentRoomMarkup.ToString();
                        }
                        #endregion  Cut Net Ammount

                        try
                        {
                            objRateInfo.MandatoryTax = Convert.ToSingle(results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.HotelFees.HotelFee.amount.Value) * dolarRate / ExchangeRate;

                        }
                        catch
                        {

                        }
                        #region NightlyRatesPerRoom Info
                        //noNights = Night;
                        NightlyRatesPerRoom objNightlyRatesPerRoom = new NightlyRatesPerRoom();
                        List<NightlyRate> listNightlyRate = new List<NightlyRate>();
                        if (Night == 1)
                        {
                            NightlyRate objNightlyRate = new NightlyRate();
                            objNightlyRate.@baseRate = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.ChargeableRateInfo.NightlyRatesPerRoom.NightlyRate.baseRate.Value.ToString();
                            objNightlyRate.@rate = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.ChargeableRateInfo.NightlyRatesPerRoom.NightlyRate.rate.Value.ToString();
                            objNightlyRate.@promo = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.ChargeableRateInfo.NightlyRatesPerRoom.NightlyRate.promo.ToString();
                            listNightlyRate.Add(objNightlyRate);
                        }
                        else
                        {
                            for (int n = 0; n < Night; n++)
                            {
                                NightlyRate objNightlyRate = new NightlyRate();
                                try
                                {

                                    objNightlyRate.@baseRate = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.ChargeableRateInfo.NightlyRatesPerRoom.NightlyRate[n].baseRate.ToString();
                                    objNightlyRate.@rate = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.ChargeableRateInfo.NightlyRatesPerRoom.NightlyRate[n].rate.ToString();
                                    objNightlyRate.@promo = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.ChargeableRateInfo.NightlyRatesPerRoom.NightlyRate[n].promo.ToString();
                                    //objNightlyRatesPerRoom.NightlyRate = dict.HotelListResponse.HotelList.HotelSummary[i]["RoomRateDetailsList"]["RoomRateDetails"]["RateInfos"]["RateInfo"]["ChargeableRateInfo"]["NightlyRatesPerRoom"]["NightlyRate"]["NightlyRate"].ToString();
                                    listNightlyRate.Add(objNightlyRate);
                                }
                                catch
                                {
                                    continue;
                                }
                            }
                        }

                        objNightlyRatesPerRoom.size = Night.ToString();
                        objNightlyRatesPerRoom.NightlyRate = listNightlyRate;
                        objChargeableRateInfo.NightlyRatesPerRoom = objNightlyRatesPerRoom;
                        #endregion

                        #region Surcharges Info
                        Surcharges objSurcharges = new Surcharges();
                        int noSurcharges = Convert.ToInt16(results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.ChargeableRateInfo.Surcharges.size.Value.ToString());
                        List<Surcharge> listSurcharge = new List<Surcharge>();
                        if (noSurcharges == 1)
                        {
                            Surcharge objSurcharge = new Surcharge();
                            var SurviceCharges = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.ChargeableRateInfo.Surcharges.Surcharge.amount.Value.ToString();
                            objSurcharge.@type = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.ChargeableRateInfo.Surcharges.Surcharge.type.Value.ToString();
                            objSurcharge.@amount = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.ChargeableRateInfo.Surcharges.Surcharge.amount.Value.ToString();
                            listSurcharge.Add(objSurcharge);
                        }
                        else
                        {
                            for (int n = 0; n < noSurcharges; n++)
                            {
                                try
                                {
                                    Surcharge objSurcharge = new Surcharge();
                                    //objSurcharge.@type = results.HotelRoomAvailabilityResponse.HotelRoomResponse.RateInfos.RateInfo.ChargeableRateInfo.Surcharges.Surcharge[n].type.Value.ToString();
                                    objSurcharge.@amount = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.ChargeableRateInfo.Surcharges.Surcharge[n].amount.Value.ToString();
                                    listSurcharge.Add(objSurcharge);
                                }
                                catch
                                {
                                    continue;
                                }
                            }
                        }

                        objSurcharges.Surcharge = listSurcharge;
                        objChargeableRateInfo.Surcharges = objSurcharges;
                        objRateInfo.ChargeableRateInfo = objChargeableRateInfo;
                        #endregion Surcharges

                        #endregion  Chargeable Info

                        try
                        {
                            objRateInfo.nonRefundable = Convert.ToBoolean(results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.nonRefundable.Value);
                            objRateInfo.rateType = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.rateType.Value.ToString();

                            objRateInfo.currentAllotment = Convert.ToInt64(results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.currentAllotment.Value);
                        }
                        catch
                        {

                        }
                        #region Cancellation Policy
                        objRateInfo.CancellationPolicy = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.cancellationPolicy.Value.ToString();

                        EANLib.Response.CancellationInfo ObjCancellationInfo = new CancellationInfo();
                        EANLib.Response.CancellationInfoList ObjCancellationInfos = new CancellationInfoList();
                        try
                        {
                            //ObjCancellationInfo.VersionId = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.CancelPolicyInfo[0].versionId.Value.ToString();
                            ObjCancellationInfo.CancelTime = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[0].cancelTime.Value.ToString();
                            ObjCancellationInfo.StartWindowHours = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[0].startWindowHours.Value.ToString();
                            ObjCancellationInfo.NightCount = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[0].nightCount.Value.ToString();
                            ObjCancellationInfo.CurrencyCode = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[0].currencyCode.Value.ToString();
                            ObjCancellationInfo.CancelDate = CencelletionDate(objHotelRoomAvailability.arrivalDate.ToString(), Convert.ToInt64(ObjCancellationInfo.StartWindowHours));
                            try
                            {
                                var TimeZone = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[0].timeZoneDescription;
                                if (TimeZone != null)
                                    ObjCancellationInfo.TimeZoneDesc = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[0].timeZoneDescription.Value.ToString();
                                var Percentage = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[0].percent;
                                if (Percentage != null)
                                    ObjCancellationInfo.Percentage = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[0].percent.Value.ToString();
                                var Amt = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[0].amount;
                                if (Amt != null)
                                    ObjCancellationInfo.amount = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[0].amount;
                            }
                            catch
                            {

                            }
                            CancellationInfo.Add(ObjCancellationInfo);
                            ObjCancellationInfo = new CancellationInfo();
                            ObjCancellationInfo.VersionId = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[1].versionId.Value.ToString();
                            ObjCancellationInfo.CancelTime = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[1].cancelTime.Value.ToString();
                            ObjCancellationInfo.StartWindowHours = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[1].startWindowHours.Value.ToString();
                            ObjCancellationInfo.NightCount = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[1].nightCount.Value.ToString();
                            ObjCancellationInfo.CurrencyCode = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[1].currencyCode.Value.ToString();
                            ObjCancellationInfo.CancelDate = CencelletionDate(objHotelRoomAvailability.arrivalDate.ToString(), Convert.ToInt64(ObjCancellationInfo.StartWindowHours));

                            try
                            {
                                var TimeZone = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[1].timeZoneDescription;
                                if (TimeZone != null)
                                    ObjCancellationInfo.TimeZoneDesc = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[1].timeZoneDescription.Value.ToString();
                                var Percentage = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[1].percent;
                                if (Percentage != null)
                                    ObjCancellationInfo.Percentage = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[1].percent.Value.ToString();
                                var Amt = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[1].amount;
                                if (Amt != null)
                                    ObjCancellationInfo.amount = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RateInfos.RateInfo.CancelPolicyInfoList.CancelPolicyInfo[1].amount;
                            }
                            catch
                            {

                            }
                            CancellationInfo.Add(ObjCancellationInfo);
                        }
                        catch
                        {

                        }
                        ObjCancellationInfos.CancellationInfo = GetCancellationInfo(CancellationInfo, listNightlyRate, commissionableUsdTotal, Night, Rooms.Count, objRateInfo.nonRefundable);
                        objRateInfo.CancellationInfoList = ObjCancellationInfos;
                        #endregion








                        //RoomList.Add(objRoomRateDetails);

                        objRateInfos.RateInfo = objRateInfo;
                        objHotelRoomResponse.RateInfos = objRateInfos;

                        #endregion  Rates Info

                        #region Value Added Service
                        ValueAdds objValueAdds = new ValueAdds();
                        try
                        {
                            objValueAdds.Size = Convert.ToInt64(results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].ValueAdds.size.Value);
                        }
                        catch
                        {
                            objValueAdds.Size = 0;
                        }
                        if (objValueAdds.Size == 1)
                        {
                            try
                            {

                                ValueAdd objValueAdd = new ValueAdd();
                                //objValueAdd.@id = results.HotelRoomAvailabilityResponse.HotelRoomResponse.ValueAdds.ValueAdd.id.ToString();
                                objValueAdd.Service = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].ValueAdds.ValueAdd.description.Value.ToString();
                                //objCategory.id =      results.HotelRoomAvailabilityResponse.HotelRoomResponse.ValueAdds.ValueAdd.id.Value.ToString();
                                //objCategory.Ratings = results.HotelRoomAvailabilityResponse.HotelRoomResponse.ValueAdds.ValueAdd.id.id.Value.ToString();
                                //Services.Add(objValueAdd);
                                if (!Services.Contains(objValueAdd.Service))
                                {
                                    Services.Add(objValueAdd.Service);
                                }
                                // Services.Add(objValueAdd);
                                //List_Facility.Add(objValueAdd);
                            }
                            catch
                            {
                                ValueAdd objValueAdd = new ValueAdd();
                                objValueAdd.@id = "";
                                objValueAdd.Service = "";
                                //Services.Add(objValueAdd);
                            }
                        }
                        else
                        {
                            for (int j = 0; j < objValueAdds.Size; j++)
                            {

                                try
                                {
                                    ValueAdd objValueAdd = new ValueAdd();
                                    //objValueAdd.@id = results.HotelRoomAvailabilityResponse.HotelRoomAvailabilityResponse.HotelRoomResponse[i].ValueAdds.ValueAdd[j].id.Value.ToString();
                                    objValueAdd.Service = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].ValueAdds.ValueAdd[j].description.Value.ToString();
                                    //Services.Add(objValueAdd);
                                    if (!Services.Contains(objValueAdd.Service))
                                    {
                                        Services.Add(objValueAdd.Service);
                                    }
                                }
                                catch
                                {
                                    ValueAdd objValueAdd = new ValueAdd();
                                    objValueAdd.@id = "";
                                    objValueAdd.Service = "";
                                    //Services.Add(objValueAdd);
                                }

                            }
                        }


                        // objValueAdds.ValueAdd = Services;
                        objHotelRoomResponse.ValueAdds = objValueAdds;
                        #endregion Value Added Service

                        #region deepLink
                        objHotelRoomResponse.deeplink = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].deepLink.Value.ToString();
                        #endregion

                        #region Hotel Images
                        List<HotelImage> listHotelImage = new List<HotelImage>();
                        var RoomImage = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RoomImages;
                        if (RoomImage != null)
                        {
                            int noImages = Convert.ToInt16(results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RoomImages.size.Value);
                            HotelImage objHotelImage = new HotelImage();
                            objHotelImage.thumbnailUrl = "http://media.expedia.com" + objEANHotels.HotelDetail.Single(Details => Details.hotelId == HotelId).thumbNailUrl.Replace("_t", "_b");
                            Images.Add(objHotelImage.thumbnailUrl);
                            listHotelImage.Add(objHotelImage);
                            objHotelRoomResponse.HotelImage = listHotelImage;
                            if (noImages != 1)
                            {
                                for (int j = 0; j < noImages; j++)
                                {
                                    objHotelImage = new HotelImage();
                                    objHotelImage.thumbnailUrl = results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RoomImages.RoomImage[j].url.Value.ToString().Replace("_s", "_b");
                                    Images.Add(objHotelImage.thumbnailUrl);
                                    //objHotelRoomAvailability.HotelImages.Add(results.HotelRoomAvailabilityResponse.HotelRoomResponse[i].RoomImages.RoomImage[j].url.Value.ToString());
                                    listHotelImage.Add(objHotelImage);
                                }
                            }

                            objHotelRoomResponse.HotelImage = listHotelImage;
                        }
                        else
                        {
                            HotelImage objHotelImage = new HotelImage();
                            objHotelImage.thumbnailUrl = "http://media.expedia.com" + objEANHotels.HotelDetail.Single(Details => Details.hotelId == HotelId).thumbNailUrl.Replace("_t", "_b");
                            Images.Add(objHotelImage.thumbnailUrl);
                            listHotelImage.Add(objHotelImage);
                            objHotelRoomResponse.HotelImage = listHotelImage;
                        }


                        #endregion

                        listHotelRoomResponse.Add(objHotelRoomResponse);
                    }
                    #endregion
                }

                objHotelRoomAvailability.HotelRoomResponse = listHotelRoomResponse;
                objHotelRoomAvailability.HotelImages = Images;
                objHotelRoomAvailability.ValueAdd = Services;
                objEANHotels.HotelRoomAvailabilityResponse = objHotelRoomAvailability;
                Context.Session["EanAvail"] = objEANHotels;


            }
            catch
            {
                bresponse = false;
            }


            return bresponse;
        }

        #endregion

        #region Genrate Ratings
        private static string GetRatings(string RatingId)
        {
            string Ratings = "0";
            if (RatingId == "1.0" || RatingId == "1.5")
            {
                RatingId = "1";
            }
            if (RatingId == "2.0" || RatingId == "2.5")
            {
                RatingId = "2";
            }
            else if (RatingId == "3.0" || RatingId == "3.5")
            {
                RatingId = "3";
            }
            else if (RatingId == "4.0" || RatingId == "4.5")
            {
                RatingId = "4";
            }
            return Ratings = RatingId;
        }
        #endregion




    }
}