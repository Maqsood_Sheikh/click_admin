﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Config = System.Configuration.ConfigurationManager;

namespace CutAdmin.Common
{
    public class ConfigHelper
    {

        public static string GetConnectionString()
        {
            return Config.AppSettings["ConnectionString"].ToString();
        }
        public static string GetConnectionPwd()
        {
            return Config.AppSettings["Connection.Password"].ToString();
        }
        public static string[] CommonPages { get { return GetFromConfig("Pages"); } }
        public static bool SecurePages { get { return Convert.ToBoolean(GetConfigValue("SecurePages", "True")); } }
        private static string[] GetFromConfig(string key)
        {
            string[] arr = Config.AppSettings[key].ToString().Split('|');
            return arr;
        }

        public static string GetConfigValue(string key, string defaultValue)
        {
            string val = Config.AppSettings[key].ToString();
            return String.IsNullOrEmpty(val) ? defaultValue : val;
        }
    }
}