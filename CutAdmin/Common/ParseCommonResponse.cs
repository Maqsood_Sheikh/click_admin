﻿using CommonLib.Response;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace CutAdmin.Common
{
    public class ParseCommonResponse
    {
        //List<CommonLib.Response.Facility> lstFacility = new List<CommonLib.Response.Facility>();
        //List<CommonLib.Response.Category> lstCategory = new List<CommonLib.Response.Category>();
        //List<CommonLib.Response.Location> lstLocation = new List<CommonLib.Response.Location>();

        List<Location> Location_List = new List<Location>();
        List<Facility> Facility_List = new List<Facility>();

        public List<CommonLib.Response.CommonHotelDetails> GetCommon(Models.Hotel objHotel, Models.MGHHotel objMGHHotel, Models.DoTWHotel objDoTWHotel, Models.EANHotels objEANHotels, Models.GRNHotels objGRNHotels, out List<CommonLib.Response.Facility> lstFacility, out List<CommonLib.Response.Category> lstCategory, out List<CommonLib.Response.Location> lstLocation)
        {
            List<CommonLib.Response.CommonHotelDetails> lstHotels = new List<CommonLib.Response.CommonHotelDetails>();
            CommonLib.Response.CommonHotelDetails[] objCommonHotelDetails;
            Int64 CommonHotel = 0;
            if (objHotel.HotelDetail != null)
                CommonHotel += objHotel.HotelDetail.Count;
            if (objMGHHotel.HotelDetail != null)
                CommonHotel += objMGHHotel.HotelDetail.Count;
            if (objDoTWHotel.HotelDetails != null)
                CommonHotel += objDoTWHotel.HotelDetails.Count;
            if (objEANHotels.HotelDetail != null)
                CommonHotel += objEANHotels.HotelDetail.Count;
            if (objGRNHotels.HotelDetail != null)
                CommonHotel += objGRNHotels.HotelDetail.hotels.Count;
            objCommonHotelDetails = new CommonLib.Response.CommonHotelDetails[CommonHotel];
            //CommonLib.Response.CommonHotelDetails[] objCommonHotelDetails = new CommonLib.Response.CommonHotelDetails[objDoTWHotel.HotelDetails.Count];
            //CommonLib.Response.CommonHotelDetails[] objCommonHotelDetails = new CommonLib.Response.CommonHotelDetails[objHotel.HotelDetail.Count + objMGHHotel.HotelDetail.Count];
            //CommonLib.Response.CommonHotelDetails[] objCommonHotelDetails = new CommonLib.Response.CommonHotelDetails[objHotel.HotelDetail.Count + objMGHHotel.HotelDetail.Count + objDoTWHotel.HotelDetails.Count + objEANHotels.HotelDetail.Count];
            lstFacility = new List<CommonLib.Response.Facility>();
            lstCategory = new List<CommonLib.Response.Category>();
            lstLocation = new List<CommonLib.Response.Location>();

            #region HotelBeds
            if (objHotel.HotelDetail != null)
            {
                for (int i = 0; i < objHotel.HotelDetail.Count; i++)
                {
                    objCommonHotelDetails[i] = new CommonLib.Response.CommonHotelDetails();
                    objCommonHotelDetails[i].Address = objHotel.HotelDetail[i].Address;
                    //#3
                    CommonLib.Response.Category objCategory = new CommonLib.Response.Category();
                    objCategory.Name = objHotel.HotelDetail[i].Category;
                    objCommonHotelDetails[i].Category = objHotel.HotelDetail[i].Category;
                    //HotelCategory(objHotel.HotelDetail[i].Category);
                    string code = AllowCategory(objCommonHotelDetails[i].Category);
                    var Main_Hotel_Category = lstCategory.Where(data => data.Name == code).FirstOrDefault();
                    if (lstCategory.Count > 0)
                    {
                        if (Main_Hotel_Category != null)
                            lstCategory.Remove(Main_Hotel_Category);
                        else
                            Main_Hotel_Category = new CommonLib.Response.Category { Name = code, Count = 0, Supplier = "HotelBeds" };
                        Main_Hotel_Category.Count = Main_Hotel_Category.Count + 1;
                        lstCategory.Add(Main_Hotel_Category);
                    }
                    else
                    {
                        Main_Hotel_Category = new CommonLib.Response.Category { Name = code, Count = 0, Supplier = "HotelBeds" };
                        Main_Hotel_Category.Count = Main_Hotel_Category.Count + 1;
                        lstCategory.Add(Main_Hotel_Category);
                    }
                    objCommonHotelDetails[i].Currency = objHotel.HotelDetail[i].Currency;
                    objCommonHotelDetails[i].DateFrom = objHotel.HotelDetail[i].DateFrom;
                    objCommonHotelDetails[i].DateTo = objHotel.HotelDetail[i].DateTo;
                    objCommonHotelDetails[i].Description = objHotel.HotelDetail[i].Description;
                    //#1
                    objCommonHotelDetails[i].Facility = objHotel.HotelDetail[i].Facility;
                    foreach (string Fname in objCommonHotelDetails[i].Facility)
                    {
                        //CommonLib.Response.Facilities objFacilities = new CommonLib.Response.Facilities();
                        //objFacilities.Name = Fname;
                        //objFacilities.HotelCode = objHotel.HotelDetail[i].HotelID;
                        //objFacilities.Count = 1;
                        //lstFacilities.Add(objFacilities);
                        CommonLib.Response.Facility objFacility = new CommonLib.Response.Facility();
                        objFacility.FacilitiesName = Fname;
                        objFacility.HotelCode = objHotel.HotelDetail[i].HotelID;
                        objFacility.Supplier = "HotelBeds";
                        objFacility.Count = 1;
                        //HotelFacility(objFacility);
                        if (lstFacility.Count > 0)
                        {
                            var Main_List_Location = lstFacility.Where(data => data.FacilitiesName == objFacility.FacilitiesName).FirstOrDefault();
                            if (Main_List_Location != null)
                                lstFacility.Remove(Main_List_Location);
                            else
                                Main_List_Location = new CommonLib.Response.Facility { FacilitiesName = objFacility.FacilitiesName, Count = 0, Supplier = "HotelBeds" };
                            Main_List_Location.Count = Main_List_Location.Count + 1;
                            lstFacility.Add(Main_List_Location);
                        }
                        else
                        {
                            lstFacility.Add(objFacility);
                        }
                    }

                    objCommonHotelDetails[i].HotelId = objHotel.HotelDetail[i].HotelID;
                    objCommonHotelDetails[i].HotelName = objHotel.HotelDetail[i].Name;
                    objCommonHotelDetails[i].Image = new List<CommonLib.Response.Image>();
                    objCommonHotelDetails[i].Room = new List<CommonLib.Response.Room>();
                    objCommonHotelDetails[i].Location = new List<CommonLib.Response.Location>();
                    CommonLib.Response.Image[] objImage = new CommonLib.Response.Image[objHotel.HotelDetail[i].Image.Count];
                    for (int j = 0; j < objHotel.HotelDetail[i].Image.Count; j++)
                    {
                        objImage[j] = new CommonLib.Response.Image();
                        objImage[j].Url = objHotel.HotelDetail[i].Image[j].Url;
                        objImage[j].Title = objHotel.HotelDetail[i].Image[j].Type;
                        objCommonHotelDetails[i].Image.Add(objImage[j]);
                    }
                    objCommonHotelDetails[i].Langitude = objHotel.HotelDetail[i].Langitude;
                    objCommonHotelDetails[i].Latitude = objHotel.HotelDetail[i].Latitude;
                    objCommonHotelDetails[i].Supplier = "HotelBeds";
                    CommonLib.Response.Location[] objLocation = new CommonLib.Response.Location[objHotel.HotelDetail[i].Location.Count];
                    //#2
                    for (int j = 0; j < objHotel.HotelDetail[i].Location.Count; j++)
                    {
                        objLocation[j] = new CommonLib.Response.Location();
                        objLocation[j].Name = objHotel.HotelDetail[i].Location[j];
                        objLocation[j].HotelCode = objHotel.HotelDetail[i].HotelID;
                        objLocation[j].Supplier = "HotelBeds";
                        objLocation[j].Count = 1;
                        objCommonHotelDetails[i].Location.Add(objLocation[j]);
                        if (lstLocation.Count > 0)
                        {
                            var Main_List_Location = lstLocation.Where(data => data.Name == objLocation[j].Name).FirstOrDefault();
                            if (Main_List_Location != null)
                                lstLocation.Remove(Main_List_Location);
                            else
                            {
                                Main_List_Location = new CommonLib.Response.Location { Name = objLocation[j].Name, Count = 0, Supplier = "HotelBeds" };
                            }
                            Main_List_Location.Count = Main_List_Location.Count + 1;
                            lstLocation.Add(Main_List_Location);
                        }
                        else
                        {
                            lstLocation.Add(objLocation[j]);
                        }
                    }
                    CommonLib.Response.Room[] objRoom = new CommonLib.Response.Room[objHotel.HotelDetail[i].sRooms.Count];
                    CommonLib.Response.RoomOccupancy[] objRoomOccupancy = new CommonLib.Response.RoomOccupancy[objHotel.HotelDetail[i].sRooms.Count];
                    objCommonHotelDetails[i].sRooms = new List<CommonLib.Response.RoomOccupancy>();
                    for (int j = 0; j < objHotel.HotelDetail[i].sRooms.Count; j++)
                    {
                        objRoomOccupancy[j] = new CommonLib.Response.RoomOccupancy();
                        objRoomOccupancy[j].RoomID = objHotel.HotelDetail[i].sRooms[j].RoomID;
                        objRoomOccupancy[j].sRoom = new List<CommonLib.Response.sRoom>();
                        objRoomOccupancy[j].RoomPrice = objHotel.HotelDetail[i].sRooms[j].RoomPrice;
                        objRoomOccupancy[j].CUTRoomPrice = objHotel.HotelDetail[i].sRooms[j].CUTRoomPrice;
                        objRoomOccupancy[j].AgentMarkup = objHotel.HotelDetail[i].sRooms[j].AgentMarkup;
                        objRoomOccupancy[j].StaffMarkup = objHotel.HotelDetail[i].sRooms[j].StaffMarkup;
                        CommonLib.Response.sRoom[] objsRoom = new CommonLib.Response.sRoom[objHotel.HotelDetail[i].sRooms[j].sRoom.Count];
                        for (int k = 0; k < objHotel.HotelDetail[i].sRooms[j].sRoom.Count; k++)
                        {
                            objsRoom[k] = new CommonLib.Response.sRoom();
                            objsRoom[k].onRequest = objHotel.HotelDetail[i].sRooms[j].sRoom[k].onRequest;
                            objsRoom[k].availCount = objHotel.HotelDetail[i].sRooms[j].sRoom[k].availCount;
                            objsRoom[k].SHRUI = objHotel.HotelDetail[i].sRooms[j].sRoom[k].SHRUI;
                            objsRoom[k].Boardtype = objHotel.HotelDetail[i].sRooms[j].sRoom[k].Boardtype;
                            objsRoom[k].Boardcode = objHotel.HotelDetail[i].sRooms[j].sRoom[k].Boardcode;
                            objsRoom[k].Boardshortname = objHotel.HotelDetail[i].sRooms[j].sRoom[k].Boardshortname;
                            objsRoom[k].Boardtext = objHotel.HotelDetail[i].sRooms[j].sRoom[k].Boardtext;
                            objsRoom[k].Roomtype = objHotel.HotelDetail[i].sRooms[j].sRoom[k].Roomtype;
                            objsRoom[k].Roomcode = objHotel.HotelDetail[i].sRooms[j].sRoom[k].Roomcode;
                            objsRoom[k].Roomcharacteristic = objHotel.HotelDetail[i].sRooms[j].sRoom[k].Roomcharacteristic;
                            objsRoom[k].Roomtext = objHotel.HotelDetail[i].sRooms[j].sRoom[k].Roomtext;
                            objsRoom[k].RoomAmount = objHotel.HotelDetail[i].sRooms[j].sRoom[k].RoomAmount;
                            objsRoom[k].CUTRoomAmount = objHotel.HotelDetail[i].sRooms[j].sRoom[k].CUTRoomAmount;
                            objsRoom[k].AgentRoomMarkup = objHotel.HotelDetail[i].sRooms[j].sRoom[k].AgentRoomMarkup;
                            objsRoom[k].Roomdate = objHotel.HotelDetail[i].sRooms[j].sRoom[k].Roomdate;
                            objsRoom[k].Roomtime = objHotel.HotelDetail[i].sRooms[j].sRoom[k].Roomtime;
                            objsRoom[k].CancellationAmount = objHotel.HotelDetail[i].sRooms[j].sRoom[k].CancellationAmount;
                            objsRoom[k].CUTCancellationAmount = objHotel.HotelDetail[i].sRooms[j].sRoom[k].CUTCancellationAmount;
                            objsRoom[k].AgentCancellationMarkup = objHotel.HotelDetail[i].sRooms[j].sRoom[k].AgentCancellationMarkup;
                            objsRoom[k].NoChargeDate = objHotel.HotelDetail[i].sRooms[j].sRoom[k].NoChargeDate;
                            objsRoom[k].ChargeDate = objHotel.HotelDetail[i].sRooms[j].sRoom[k].ChargeDate;
                            objsRoom[k].sNoChargeDate = objHotel.HotelDetail[i].sRooms[j].sRoom[k].sNoChargeDate;
                            objsRoom[k].sChargeDate = objHotel.HotelDetail[i].sRooms[j].sRoom[k].sChargeDate;
                            objsRoom[k].RoomCount = objHotel.HotelDetail[i].sRooms[j].sRoom[k].RoomCount;
                            objsRoom[k].AdultCount = objHotel.HotelDetail[i].sRooms[j].sRoom[k].AdultCount;
                            objsRoom[k].ChildCount = objHotel.HotelDetail[i].sRooms[j].sRoom[k].ChildCount;
                            objsRoom[k].StaffRoomMarkup = objHotel.HotelDetail[i].sRooms[j].sRoom[k].StaffRoomMarkup;
                            objsRoom[k].StaffCancellationMarkup = objHotel.HotelDetail[i].sRooms[j].sRoom[k].StaffCancellationMarkup;
                            objRoomOccupancy[j].sRoom.Add(objsRoom[k]);
                        }
                        objCommonHotelDetails[i].sRooms.Add(objRoomOccupancy[j]);
                    }
                    ////..........
                    //for (int j = 0; j < objHotel.HotelDetail[i].sRooms.Count; j++)
                    //{

                    //    objRoom[j] = new CommonLib.Response.Room();
                    //    objRoom[j].CancellationPolicy = new List<CommonLib.Response.CancellationPolicy>();
                    //    objRoom[j].RoomType = new CommonLib.Response.RoomType();
                    //    objRoom[j].RoomType.RoomTypeId = objHotel.HotelDetail[i].sRooms[j].RoomID;
                    //    objRoom[j].RoomType.RoomTypeName = objHotel.HotelDetail[i].sRooms[j].sRoom[0].Roomtext;
                    //    objRoom[j].RoomType.RoomDescription = objHotel.HotelDetail[i].sRooms[j].sRoom[0].Boardtext;
                    //    objRoom[j].Price = objHotel.HotelDetail[i].Price;
                    //    objRoom[j].CutPrice = objHotel.HotelDetail[i].sRooms[j].CUTRoomPrice;
                    //    objRoom[j].AgentMarkup = objHotel.HotelDetail[i].sRooms[j].AgentMarkup;
                    //    objRoom[j].AvailCount = Convert.ToInt64(objHotel.HotelDetail[i].sRooms[j].sRoom[0].availCount);
                    //    CommonLib.Response.CancellationPolicy[] objCancellationPolicy = new CommonLib.Response.CancellationPolicy[objHotel.HotelDetail[i].sRooms[j].sRoom.Count];
                    //    for (int k = 0; k < objHotel.HotelDetail[i].sRooms[j].sRoom.Count; k++)
                    //    {
                    //        objCancellationPolicy[k] = new CommonLib.Response.CancellationPolicy();
                    //        objCancellationPolicy[k].AgentCancellationMarkup = objHotel.HotelDetail[i].sRooms[j].sRoom[k].AgentCancellationMarkup[0];
                    //        objCancellationPolicy[k].AgentRoomMarkup = objHotel.HotelDetail[i].sRooms[j].sRoom[k].AgentRoomMarkup;
                    //        objCancellationPolicy[k].CancellationAmount = objHotel.HotelDetail[i].sRooms[j].sRoom[k].CancellationAmount[0];
                    //        objCancellationPolicy[k].CUTCancellationAmount = objHotel.HotelDetail[i].sRooms[j].sRoom[k].CUTCancellationAmount[0];
                    //        objCancellationPolicy[k].CutRoomAmount = objHotel.HotelDetail[i].sRooms[j].sRoom[k].CUTRoomAmount;
                    //        objCancellationPolicy[k].dayMax = objHotel.HotelDetail[i].sRooms[j].sRoom[k].ChargeDate[0].Date.ToString();
                    //        objCancellationPolicy[k].deduction = objHotel.HotelDetail[i].sRooms[j].sRoom[k].CancellationAmount[0];
                    //        //objCancellationPolicy[k].StaffCancellationMarkup = objHotel.HotelDetail[i].sRooms[j].sRoom[0].StaffCancellationMarkup[k];
                    //        objCancellationPolicy[k].unit = "";
                    //        objRoom[j].CancellationPolicy.Add(objCancellationPolicy[k]);
                    //    }
                    //    objCommonHotelDetails[i].Room.Add(objRoom[j]);
                    //}
                    objCommonHotelDetails[i].CUTPrice = objHotel.HotelDetail[i].CUTPrice;
                    objCommonHotelDetails[i].AgentMarkup = objHotel.HotelDetail[i].AgentMarkup;
                    objCommonHotelDetails[i].UserRating = objHotel.HotelDetail[i].UserRating;
                    lstHotels.Add(objCommonHotelDetails[i]);
                }
            }

            #endregion HotelBeds

            #region MGH
            if (objMGHHotel.HotelDetail != null)
            {
                for (int i = 0; i < objMGHHotel.HotelDetail.Count; i++)
                {
                    List<Offers> MyOffer = new List<Offers>();
                    Offers ObjOffer = new Offers();

                    objCommonHotelDetails[i] = new CommonLib.Response.CommonHotelDetails();
                    objCommonHotelDetails[i].Address = objMGHHotel.HotelDetail[i].Location[0].LocationName + " ," + objMGHHotel.HotelDetail[i].Location[0].Country;
                    //objCommonHotelDetails[i].Category = objMGHHotel.HotelDetail[i].Category[0].CategoryName;
                    objCommonHotelDetails[i].PreferedHotel = true;
                    if (objMGHHotel.HotelDetail[i].Category.Count != 0)
                    {
                        objCommonHotelDetails[i].Category = AllowMghCategory(objMGHHotel.HotelDetail[i].Category[0].CategoryName);
                    }
                    else
                    {
                        objCommonHotelDetails[i].Category = "Other";
                    }
                    //objCommonHotelDetails[i].Category = AllowMghCategory(objMGHHotel.HotelDetail[i].Category[0].CategoryName);
                    //HotelCategory(objMGHHotel.HotelDetail[i].Category[0].CategoryName);
                    //string code = AllowCategory(objCommonHotelDetails[i].Category);
                    string code = (objCommonHotelDetails[i].Category).ToString();
                    var Main_Hotel_Category = lstCategory.Where(data => data.Name == code).FirstOrDefault();
                    if (lstCategory.Count > 0)
                    {
                        if (Main_Hotel_Category != null)
                            lstCategory.Remove(Main_Hotel_Category);
                        else
                            Main_Hotel_Category = new CommonLib.Response.Category { Name = code, Count = 0, Supplier = "MGH" };
                        Main_Hotel_Category.Count = Main_Hotel_Category.Count + 1;
                        lstCategory.Add(Main_Hotel_Category);
                    }
                    else
                    {
                        Main_Hotel_Category = new CommonLib.Response.Category { Name = code, Count = 0, Supplier = "MGH" };
                        Main_Hotel_Category.Count = Main_Hotel_Category.Count + 1;
                        lstCategory.Add(Main_Hotel_Category);
                    }
                    objCommonHotelDetails[i].Currency = objMGHHotel.HotelDetail[i].Currency;
                    objCommonHotelDetails[i].DateFrom = objMGHHotel.HotelDetail[i].DateFrom;
                    objCommonHotelDetails[i].DateTo = objMGHHotel.HotelDetail[i].DateTo;
                    objCommonHotelDetails[i].Description = objMGHHotel.HotelDetail[i].Description;
                    //objCommonHotelDetails[i].Facility = objMGHHotel.HotelDetail[i].Facilities[0];
                    objCommonHotelDetails[i].HotelId = objMGHHotel.HotelDetail[i].HotelId.ToString();

                    objCommonHotelDetails[i].HotelName = objMGHHotel.HotelDetail[i].HotelName;
                    objCommonHotelDetails[i].Facility = new List<String>();
                    objCommonHotelDetails[i].Image = new List<CommonLib.Response.Image>();
                    objCommonHotelDetails[i].Room = new List<CommonLib.Response.Room>();
                    objCommonHotelDetails[i].Location = new List<CommonLib.Response.Location>();
                    CommonLib.Response.Image[] objImage = new CommonLib.Response.Image[objMGHHotel.HotelDetail[i].MediaList.Count];
                    for (int j = 0; j < objMGHHotel.HotelDetail[i].MediaList.Count; j++)
                    {
                        objImage[j] = new CommonLib.Response.Image();
                        objImage[j].Url = objMGHHotel.HotelDetail[i].MediaList[j].Url;
                        objImage[j].Title = objMGHHotel.HotelDetail[i].MediaList[j].Type;
                        objCommonHotelDetails[i].Image.Add(objImage[j]);
                    }
                    CommonLib.Response.Facility[] objFacilities = new CommonLib.Response.Facility[objMGHHotel.HotelDetail[i].Facilities.Count];
                    for (int j = 0; j < objMGHHotel.HotelDetail[i].Facilities.Count; j++)
                    {
                        objFacilities[j] = new CommonLib.Response.Facility();
                        objFacilities[j].FacilitiesName = objMGHHotel.HotelDetail[i].Facilities[j].FacilitiesName;
                        objFacilities[j].Supplier = "MGH";
                        objCommonHotelDetails[i].Facility.Add(objFacilities[j].FacilitiesName);
                        //HotelFacility(objFacilities[j]);
                        if (lstFacility.Count > 0)
                        {
                            var Main_List_Location = lstFacility.Where(data => data.FacilitiesName == objFacilities[j].FacilitiesName).FirstOrDefault();
                            if (Main_List_Location != null)
                                lstFacility.Remove(Main_List_Location);
                            else
                                Main_List_Location = new CommonLib.Response.Facility { FacilitiesName = objFacilities[j].FacilitiesName, Count = 0, Supplier = "MGH" };
                            Main_List_Location.Count = Main_List_Location.Count + 1;
                            lstFacility.Add(Main_List_Location);
                        }
                        else
                        {
                            lstFacility.Add(objFacilities[j]);
                        }
                    }
                    CommonLib.Response.Category[] objCategory = new CommonLib.Response.Category[objMGHHotel.HotelDetail[i].Category.Count];
                    for (int j = 0; j < objMGHHotel.HotelDetail[i].Category.Count; j++)
                    {
                        objCategory[j] = new CommonLib.Response.Category();
                    }
                    objCommonHotelDetails[i].Langitude = objMGHHotel.HotelDetail[i].Longitude.ToString();
                    objCommonHotelDetails[i].Latitude = objMGHHotel.HotelDetail[i].Latitude.ToString();
                    objCommonHotelDetails[i].Supplier = "MGH";
                    CommonLib.Response.Location[] objLocation = new CommonLib.Response.Location[objMGHHotel.HotelDetail[i].Location.Count];
                    for (int j = 0; j < objMGHHotel.HotelDetail[i].Location.Count; j++)
                    {
                        objLocation[j] = new CommonLib.Response.Location();
                        objLocation[j].Name = objMGHHotel.HotelDetail[i].Location[j].LocationName;
                        objLocation[j].HotelCode = objMGHHotel.HotelDetail[i].HotelId.ToString();
                        objLocation[j].Supplier = "MGH";
                        objLocation[j].Count = 1;
                        objCommonHotelDetails[i].Location.Add(objLocation[j]);

                        if (lstLocation.Count > 0)
                        {
                            var Main_List_Location = lstLocation.Where(data => data.Name == objLocation[j].Name).FirstOrDefault();
                            if (Main_List_Location != null)
                                lstLocation.Remove(Main_List_Location);
                            else
                            {
                                Main_List_Location = new CommonLib.Response.Location { Name = objLocation[j].Name, Count = 0, Supplier = "MGH" };
                            }
                            Main_List_Location.Count = Main_List_Location.Count + 1;
                            lstLocation.Add(Main_List_Location);
                        }
                        else
                        {
                            lstLocation.Add(objLocation[j]);
                        }
                    }
                    CommonLib.Response.Room[] objRoom = new CommonLib.Response.Room[objMGHHotel.HotelDetail[i].rate.Count];
                    for (int j = 0; j < objMGHHotel.HotelDetail[i].rate.Count; j++)
                    {
                        objRoom[j] = new CommonLib.Response.Room();
                        objRoom[j].CancellationPolicy = new List<CommonLib.Response.CancellationPolicy>();
                        objRoom[j].RoomType = new CommonLib.Response.RoomType();
                        objRoom[j].RoomType.RoomTypeId = objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].RoomCategoryId.ToString();
                        objRoom[j].RoomType.RoomTypeName = objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].RoomCategoryName;
                        objRoom[j].RoomType.RoomDescription = objMGHHotel.HotelDetail[i].rate[j].mealPlan[0].text;
                        objRoom[j].AvailCount = Convert.ToInt64(objMGHHotel.HotelDetail[i].rate[0].availCount);
                        #region Applying Offer
                        if (objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList.Count != 0)
                        {
                            MyOffer = new List<Offers>();
                            var List = objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList.Where(x => x.Hotel_Id == Convert.ToInt64(objCommonHotelDetails[i].HotelId) && x.Room_Id == Convert.ToInt64(objRoom[j].RoomType.RoomTypeId)).ToList();
                            if (List.Count > 0)
                            {
                                for (int x = 0; x < objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList.Count; x++)
                                {
                                    ObjOffer = new Offers();
                                    ObjOffer.IsOffer = true;
                                    ObjOffer.ApplyingOffer = objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].ApplyingOffer;
                                    ObjOffer.Sid = Convert.ToInt64(objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].Sid);
                                    ObjOffer.Room_Id = Convert.ToInt64(objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].Room_Id);
                                    ObjOffer.Offer_Id = Convert.ToInt64(objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].Offer_Id);
                                    ObjOffer.Hotel_Id = Convert.ToInt64(objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].Hotel_Id);
                                    ObjOffer.BookBefore = objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].BookBefore;
                                    ObjOffer.DateType = objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].DateType;
                                    ObjOffer.DaysPrior = objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].DaysPrior;
                                    ObjOffer.DiscountAmount = Convert.ToDecimal(objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].DiscountAmount);
                                    ObjOffer.DiscountPer = Convert.ToDecimal(objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].DiscountPer);
                                    ObjOffer.FreebiItem = objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].FreebiItem;
                                    ObjOffer.FreebiItemDetail = objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].FreebiItemDetail;
                                    ObjOffer.HotelOfferCode = objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].HotelOfferCode;
                                    ObjOffer.MealPlan = objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].MealPlan;
                                    ObjOffer.MinNights = objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].MinNights;
                                    ObjOffer.NewRate = Convert.ToDecimal(objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].NewRate);
                                    ObjOffer.OfferCode = objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].OfferCode;
                                    ObjOffer.OfferNote = objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].OfferNote;
                                    ObjOffer.OfferOn = objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].OfferOn;
                                    ObjOffer.OfferTerms = objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].OfferTerms;
                                    ObjOffer.OfferType = objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].OfferType;
                                    ObjOffer.SeasonName = objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].SeasonName;
                                    ObjOffer.ValidFrom = objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].ValidFrom;
                                    ObjOffer.Supplier = objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].Supplier;
                                    ObjOffer.ValidTo = objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].ValidTo;
                                    ObjOffer.RoomPrice = Convert.ToDecimal(objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].RoomPrice);
                                    ObjOffer.AgentMarkup = Convert.ToDecimal(objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].AgentMarkup);
                                    ObjOffer.AgentMarkupOffer = Convert.ToDecimal(objMGHHotel.HotelDetail[i].rate[j].roomCategory[0].OfferList[x].AgentMarkupOffer);
                                    MyOffer.Add(ObjOffer);
                                }
                                objRoom[j].OfferList = MyOffer;
                            }
                        }
                        #endregion
                        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        //objRoom[j].Price = objMGHHotel.HotelDetail[i].rate[j].refundPolicy[0].CutRoomAmount;
                        //objRoom[j].CutPrice = objMGHHotel.HotelDetail[i].rate[j].refundPolicy[0].CutRoomAmount;
                        ////objRoom[j].Price = objMGHHotel.HotelDetail[i].rate[j].roomInfo[0].roomRate[0].amount;
                        ////objRoom[j].CutPrice = objMGHHotel.HotelDetail[i].rate[j].roomInfo[0].roomRate[0].amount;
                        //objRoom[j].AgentMarkup = objMGHHotel.HotelDetail[i].rate[j].refundPolicy[0].AgentRoomMarkup;
                        //CommonLib.Response.CancellationPolicy[] objCancellationPolicy = new CommonLib.Response.CancellationPolicy[objMGHHotel.HotelDetail[i].rate[j].refundPolicy.Count];
                        ////CommonLib.Response.RoomType[] objRoomType = new CommonLib.Response.RoomType[objMGHHotel.HotelDetail[i].rate[0].roomCategory.Count];
                        //for (int k = 0; k < objMGHHotel.HotelDetail[i].rate[j].refundPolicy.Count; k++)
                        //{
                        //    objCancellationPolicy[k] = new CommonLib.Response.CancellationPolicy();
                        //    objCancellationPolicy[k].AgentCancellationMarkup = objMGHHotel.HotelDetail[i].AgentMarkup;
                        //    objCancellationPolicy[k].AgentRoomMarkup = objMGHHotel.HotelDetail[i].AgentMarkup;
                        //    objCancellationPolicy[k].CancellationAmount = objMGHHotel.HotelDetail[i].rate[j].refundPolicy[k].CancellationAmount;
                        //    objCancellationPolicy[k].CUTCancellationAmount = objMGHHotel.HotelDetail[i].rate[j].refundPolicy[k].CUTCancellationAmount;
                        //    objCancellationPolicy[k].CutRoomAmount = objMGHHotel.HotelDetail[i].rate[j].refundPolicy[k].CutRoomAmount;
                        //    objCancellationPolicy[k].dayMax = objMGHHotel.HotelDetail[i].rate[j].refundPolicy[k].dayMax;
                        //    objCancellationPolicy[k].deduction = objMGHHotel.HotelDetail[i].rate[j].refundPolicy[k].CancellationAmount;
                        //    //objCancellationPolicy[k].StaffCancellationMarkup = objHotel.HotelDetail[i].sRooms[j].sRoom[0].StaffCancellationMarkup[k];
                        //    objCancellationPolicy[k].unit = "";
                        //    objRoom[j].CancellationPolicy.Add(objCancellationPolicy[k]);
                        //}
                        #region test purpose
                        objRoom[j].Price = objMGHHotel.HotelDetail[i].rate[j].Price;
                        objRoom[j].CutPrice = objMGHHotel.HotelDetail[i].rate[j].CUTPrice;
                        objRoom[j].AgentMarkup = objMGHHotel.HotelDetail[i].rate[j].AgentMarkup;
                        //CommonLib.Response.CancellationPolicy[] objCancellationPolicy = new CommonLib.Response.CancellationPolicy[objMGHHotel.HotelDetail[i].rate[j].refundPolicy.Count];
                        for (int k = 0; k < objMGHHotel.HotelDetail[i].rate[j].roomInfo.Count; k++)
                        {
                            for (int l = 0; l < objMGHHotel.HotelDetail[i].rate[j].roomInfo[k].roomRate.Count; l++)
                            {
                                CommonLib.Response.CancellationPolicy[] objCancellationPolicy = new CommonLib.Response.CancellationPolicy[objMGHHotel.HotelDetail[i].rate[j].roomInfo[k].roomRate[l].refundPolicy.Count];
                                for (int m = 0; m < objMGHHotel.HotelDetail[i].rate[j].roomInfo[k].roomRate[l].refundPolicy.Count; m++)
                                {
                                    objCancellationPolicy[m] = new CommonLib.Response.CancellationPolicy();
                                    if (objMGHHotel.HotelDetail[i].rate[j].roomInfo[k].roomRate[l].refundPolicy[m].nonRefundable)
                                        objCancellationPolicy[m].nonRefundable = true;
                                    objCancellationPolicy[m].AgentCancellationMarkup = objMGHHotel.HotelDetail[i].rate[j].roomInfo[k].roomRate[l].refundPolicy[m].AgentCancellationMarkup;
                                    objCancellationPolicy[m].AgentRoomMarkup = objMGHHotel.HotelDetail[i].AgentMarkup;
                                    objCancellationPolicy[m].CancellationAmount = objMGHHotel.HotelDetail[i].rate[j].roomInfo[k].roomRate[l].refundPolicy[m].CancellationAmount;
                                    objCancellationPolicy[m].CUTCancellationAmount = objMGHHotel.HotelDetail[i].rate[j].roomInfo[k].roomRate[l].refundPolicy[m].CUTCancellationAmount;
                                    objCancellationPolicy[m].CutRoomAmount = objMGHHotel.HotelDetail[i].rate[j].roomInfo[k].roomRate[l].refundPolicy[m].CutRoomAmount;
                                    objCancellationPolicy[m].dayMax = objMGHHotel.HotelDetail[i].rate[j].roomInfo[k].roomRate[l].refundPolicy[m].dayMax;
                                    objCancellationPolicy[m].deduction = objMGHHotel.HotelDetail[i].rate[j].roomInfo[k].roomRate[l].refundPolicy[m].CancellationAmount;
                                    objCancellationPolicy[m].unit = "";
                                    objRoom[j].CancellationPolicy.Add(objCancellationPolicy[m]);
                                }
                            }

                        }
                        #endregion test purpose

                        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        objCommonHotelDetails[i].Room.Add(objRoom[j]);
                    }
                    objCommonHotelDetails[i].CUTPrice = objMGHHotel.HotelDetail[i].CUTPrice;
                    objCommonHotelDetails[i].Price = objMGHHotel.HotelDetail[i].Price;
                    objCommonHotelDetails[i].AgentMarkup = objMGHHotel.HotelDetail[i].AgentMarkup;
                    objCommonHotelDetails[i].UserRating = Convert.ToSingle(objMGHHotel.HotelDetail[i].rating);
                    lstHotels.Add(objCommonHotelDetails[i]);
                }
            }

            #endregion MGH
            //.......................

            #region DoTW
            if (objDoTWHotel.HotelDetails != null)
            {
                for (int i = 0; i < objDoTWHotel.HotelDetails.Count; i++)
                {
                    objCommonHotelDetails[i] = new CommonLib.Response.CommonHotelDetails();
                    objCommonHotelDetails[i].Address = objDoTWHotel.HotelDetails[i].FullAddress.hotelStreetAddress + "," + objDoTWHotel.HotelDetails[i].FullAddress.hotelCity + "," + objDoTWHotel.HotelDetails[i].FullAddress.hotelZipCode;
                    //#3
                    CommonLib.Response.Category objCategory = new CommonLib.Response.Category();
                    objCategory.Name = objDoTWHotel.HotelDetails[i].Rating;
                    objCommonHotelDetails[i].Category = objDoTWHotel.HotelDetails[i].Rating;
                    //HotelCategory(objHotel.HotelDetail[i].Category);
                    string code = AllowDoTWCategory(objCommonHotelDetails[i].Category);
                    var Main_Hotel_Category = lstCategory.Where(data => data.Name == code).FirstOrDefault();
                    if (lstCategory.Count > 0)
                    {
                        if (Main_Hotel_Category != null)
                            lstCategory.Remove(Main_Hotel_Category);
                        else
                            Main_Hotel_Category = new CommonLib.Response.Category { Name = code, Count = 0, Supplier = "DoTW" };
                        Main_Hotel_Category.Count = Main_Hotel_Category.Count + 1;
                        lstCategory.Add(Main_Hotel_Category);
                    }
                    else
                    {
                        Main_Hotel_Category = new CommonLib.Response.Category { Name = code, Count = 0, Supplier = "DoTW" };
                        Main_Hotel_Category.Count = Main_Hotel_Category.Count + 1;
                        lstCategory.Add(Main_Hotel_Category);
                    }
                    objCommonHotelDetails[i].Currency = objDoTWHotel.HotelDetails[i].Currency;
                    objCommonHotelDetails[i].DateFrom = objDoTWHotel.HotelDetails[i].HotelCheckIn;
                    objCommonHotelDetails[i].DateTo = objDoTWHotel.HotelDetails[i].HotelCheckOut;
                    objCommonHotelDetails[i].Description = objDoTWHotel.HotelDetails[i].Description;
                    //#1



                    objCommonHotelDetails[i].HotelId = objDoTWHotel.HotelDetails[i].HotelId.ToString();
                    objCommonHotelDetails[i].HotelName = objDoTWHotel.HotelDetails[i].HotelName;
                    objCommonHotelDetails[i].Facility = new List<String>();
                    objCommonHotelDetails[i].Image = new List<CommonLib.Response.Image>();
                    objCommonHotelDetails[i].Room = new List<CommonLib.Response.Room>();
                    objCommonHotelDetails[i].Location = new List<CommonLib.Response.Location>();

                    CommonLib.Response.Facility[] objFacilities = new CommonLib.Response.Facility[objDoTWHotel.HotelDetails[i].Amenitie.Count];
                    for (int j = 0; j < objDoTWHotel.HotelDetails[i].Amenitie.Count; j++)
                    {
                        objFacilities[j] = new CommonLib.Response.Facility();
                        objFacilities[j].FacilitiesName = objDoTWHotel.HotelDetails[i].Amenitie[j].AmenitieName;
                        objFacilities[j].Supplier = "DoTW";
                        objCommonHotelDetails[i].Facility.Add(objFacilities[j].FacilitiesName);
                        //HotelFacility(objFacilities[j]);
                        if (lstFacility.Count > 0)
                        {
                            var Main_List_Location = lstFacility.Where(data => data.FacilitiesName == objFacilities[j].FacilitiesName).FirstOrDefault();
                            if (Main_List_Location != null)
                                lstFacility.Remove(Main_List_Location);
                            else
                                Main_List_Location = new CommonLib.Response.Facility { FacilitiesName = objFacilities[j].FacilitiesName, Count = 0, Supplier = "DoTW" };
                            Main_List_Location.Count = Main_List_Location.Count + 1;
                            lstFacility.Add(Main_List_Location);
                        }
                        else
                        {
                            lstFacility.Add(objFacilities[j]);
                        }
                    }

                    if (objDoTWHotel.HotelDetails[i].HotelImages != null)
                    {
                        CommonLib.Response.Image[] objImage = new CommonLib.Response.Image[objDoTWHotel.HotelDetails[i].HotelImages.Count];
                        for (int j = 0; j < objDoTWHotel.HotelDetails[i].HotelImages.Count; j++)
                        {
                            objImage[j] = new CommonLib.Response.Image();
                            objImage[j].Url = objDoTWHotel.HotelDetails[i].HotelImages[j].Url;
                            objImage[j].Title = objDoTWHotel.HotelDetails[i].HotelImages[j].CategoryName;
                            objCommonHotelDetails[i].Image.Add(objImage[j]);
                        }
                    }

                    objCommonHotelDetails[i].Langitude = objDoTWHotel.HotelDetails[i].Lng.ToString();
                    objCommonHotelDetails[i].Latitude = objDoTWHotel.HotelDetails[i].Lat.ToString();
                    objCommonHotelDetails[i].Supplier = "DoTW";
                    CommonLib.Response.Location[] objLocation = new CommonLib.Response.Location[objDoTWHotel.HotelDetails[i].Attraction.Count];
                    //#2
                    for (int j = 0; j < objDoTWHotel.HotelDetails[i].Attraction.Count; j++)
                    {
                        objLocation[j] = new CommonLib.Response.Location();
                        objLocation[j].Name = objDoTWHotel.HotelDetails[i].Attraction[j].Name;
                        objLocation[j].HotelCode = objDoTWHotel.HotelDetails[i].HotelId.ToString();
                        objLocation[j].Supplier = "DoTW";
                        objLocation[j].Count = 1;
                        objCommonHotelDetails[i].Location.Add(objLocation[j]);
                        if (lstLocation.Count > 0)
                        {
                            var Main_List_Location = lstLocation.Where(data => data.Name == objLocation[j].Name).FirstOrDefault();
                            if (Main_List_Location != null)
                                lstLocation.Remove(Main_List_Location);
                            else
                            {
                                Main_List_Location = new CommonLib.Response.Location { Name = objLocation[j].Name, Count = 0, Supplier = "DoTW" };
                            }
                            Main_List_Location.Count = Main_List_Location.Count + 1;
                            lstLocation.Add(Main_List_Location);
                        }
                        else
                        {
                            lstLocation.Add(objLocation[j]);
                        }
                    }
                    CommonLib.Response.Room[] objRoom = new CommonLib.Response.Room[objDoTWHotel.HotelDetails[i].Room.Count];
                    for (int j = 0; j < objDoTWHotel.HotelDetails[i].Room.Count; j++)
                    {

                        objRoom[j] = new CommonLib.Response.Room();
                        objRoom[j].CancellationPolicy = new List<CommonLib.Response.CancellationPolicy>();
                        objRoom[j].RoomType = new CommonLib.Response.RoomType();
                        objRoom[j].RoomType.RoomTypeId = objDoTWHotel.HotelDetails[i].Room[j].RoomType[0].RoomTypeCode.ToString();
                        objRoom[j].RoomType.RoomTypeName = objDoTWHotel.HotelDetails[i].Room[j].RoomType[0].RoomTypeName;
                        objRoom[j].RoomType.RoomDescriptionId = objDoTWHotel.HotelDetails[i].Room[j].RoomType[0].RoomDescriptionId.ToString();
                        objRoom[j].RoomType.RoomDescription = objDoTWHotel.HotelDetails[i].Room[j].RoomType[0].RoomDescription;
                        //objRoom[j].RoomType = objDoTWHotel.HotelDetails[i].Room[j].RoomType;
                        objRoom[j].Price = objDoTWHotel.HotelDetails[i].Room[j].RoomPrice;
                        objRoom[j].CutPrice = objDoTWHotel.HotelDetails[i].Room[j].CUTRoomPrice;
                        objRoom[j].AgentMarkup = objDoTWHotel.HotelDetails[i].Room[j].AgentMarkup;
                        objRoom[j].AvailCount = Convert.ToInt64(objDoTWHotel.HotelDetails[i].Room[j].RoomType[0].LeftToSell);
                        //CommonLib.Response.CancellationPolicy[] objCancellationPolicy = new CommonLib.Response.CancellationPolicy[objDoTWHotel.HotelDetails[i].Room[j].RoomType.Count];


                        objRoom[j].LRoomType = new List<CommonLib.Response.RoomType>();
                        CommonLib.Response.RoomType[] objLRoomType = new CommonLib.Response.RoomType[objDoTWHotel.HotelDetails[i].Room[j].RoomType.Count];
                        for (int k = 0; k < objDoTWHotel.HotelDetails[i].Room[j].RoomType.Count; k++)
                        {
                            objLRoomType[k] = new CommonLib.Response.RoomType();
                            objLRoomType[k].RoomTypeId = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].RoomTypeCode.ToString();
                            objLRoomType[k].RoomTypeName = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].RoomTypeName;
                            objLRoomType[k].RoomDescriptionId = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].RoomDescriptionId.ToString();
                            objLRoomType[k].RoomDescription = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].RoomDescription;
                            objLRoomType[k].AvailCount = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].LeftToSell;
                            objLRoomType[k].CancellationPolicy = new List<CommonLib.Response.CancellationPolicy>();
                            objLRoomType[k].special = new List<CommonLib.Response.special>();
                            objLRoomType[k].specialsApplied = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].specialsApplied;
                            objLRoomType[k].minStay = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].minStay;
                            objLRoomType[k].dateApplyMinStay = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].dateApplyMinStay;
                            objLRoomType[k].status = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].status;
                            objLRoomType[k].passengerNamesRequiredForBooking = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].passengerNamesRequiredForBooking;
                            if (objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].validForOccupancy != null)
                            {
                                objLRoomType[k].validForOccupancy = new CommonLib.Response.validForOccupancy();
                                objLRoomType[k].validForOccupancy.adults = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].validForOccupancy.adults;
                                objLRoomType[k].validForOccupancy.children = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].validForOccupancy.children;
                                objLRoomType[k].validForOccupancy.childrenAges = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].validForOccupancy.childrenAges;
                                objLRoomType[k].validForOccupancy.extraBed = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].validForOccupancy.extraBed;
                                objLRoomType[k].validForOccupancy.extraBedOccupant = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].validForOccupancy.extraBedOccupant;
                            }
                            if (objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].changedOccupancy != null)
                            {
                                objLRoomType[k].changedOccupancy = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].changedOccupancy;
                            }
                            CommonLib.Response.CancellationPolicy[] objCancellationPolicy = new CommonLib.Response.CancellationPolicy[objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules.Count];
                            for (int l = 0; l < objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules.Count; l++)
                            {
                                objCancellationPolicy[l] = new CommonLib.Response.CancellationPolicy();
                                if (objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[l] != null)
                                {
                                    //objCancellationPolicy[l] = new CommonLib.Response.CancellationPolicy();
                                    objCancellationPolicy[l].dayMax = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[l].fromDate;
                                    objCancellationPolicy[l].AgentCancellationMarkup = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[l].AgentMarkup;
                                    objCancellationPolicy[l].AgentRoomMarkup = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].AgentMarkup;
                                    objCancellationPolicy[l].CancellationAmount = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[l].cancelCharge;
                                    objCancellationPolicy[l].CUTCancellationAmount = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[l].CUTcancelCharge;
                                    objCancellationPolicy[l].CutRoomAmount = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].CUTPrice;
                                    objCancellationPolicy[l].AmendRestricted = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[l].AmendRestricted;
                                    objCancellationPolicy[l].CancelRestricted = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[l].CancelRestricted;


                                    //if (objCancellationPolicy[l].AmendRestricted == true || objCancellationPolicy[l].CancelRestricted == true)
                                    //{
                                    //    objCancellationPolicy[l].dayMax = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[l].fromDate;
                                    //    objCancellationPolicy[l].AgentCancellationMarkup = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[l].AgentMarkup;
                                    //    objCancellationPolicy[l].AgentRoomMarkup = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].AgentMarkup;
                                    //    objCancellationPolicy[l].CancellationAmount = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[l].cancelCharge;
                                    //    objCancellationPolicy[l].CUTCancellationAmount = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[l].CUTcancelCharge;
                                    //    objCancellationPolicy[l].CutRoomAmount = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].CUTPrice;
                                    //    objCancellationPolicy[l].NoShowPolicy = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[l].NoShowPolicy;
                                    //}
                                    //else
                                    //{
                                    //    objCancellationPolicy[l].dayMax = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[l].fromDate;
                                    //    objCancellationPolicy[l].AgentCancellationMarkup = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[l].AgentMarkup;
                                    //    objCancellationPolicy[l].AgentRoomMarkup = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].AgentMarkup;
                                    //    objCancellationPolicy[l].CancellationAmount = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[l].cancelCharge;
                                    //    objCancellationPolicy[l].CUTCancellationAmount = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[l].CUTcancelCharge;
                                    //    objCancellationPolicy[l].CutRoomAmount = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].CUTPrice;
                                    //    objCancellationPolicy[l].NoShowPolicy = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[l].NoShowPolicy;
                                    //}


                                    objCancellationPolicy[l].deduction = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[l].cancelCharge;
                                    objCancellationPolicy[l].unit = "";
                                    //objRoom[j].CancellationPolicy.Add(objCancellationPolicy[l]);

                                }
                                else
                                {
                                    objCancellationPolicy[l] = null;
                                }
                                objLRoomType[k].CancellationPolicy.Add(objCancellationPolicy[l]);
                            }
                            //.......ttttt
                            CommonLib.Response.special[] objspecial = new CommonLib.Response.special[objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].special.Count];
                            for (int l = 0; l < objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].special.Count; l++)
                            {
                                objspecial[l] = new CommonLib.Response.special();
                                if (objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].special[l] != null)
                                {
                                    objspecial[l].runno = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].special[l].runno;
                                    objspecial[l].type = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].special[l].type;
                                    objspecial[l].specialName = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].special[l].specialName;
                                    objspecial[l].upgradeToRoomId = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].special[l].upgradeToRoomId;
                                    objspecial[l].upgradeToMealId = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].special[l].upgradeToMealId;
                                    objspecial[l].condition = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].special[l].condition;
                                    objspecial[l].description = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].special[l].description;
                                    objspecial[l].notes = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].special[l].notes;
                                    objspecial[l].stay = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].special[l].stay;
                                    objspecial[l].discount = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].special[l].discount;
                                    objspecial[l].discountedNights = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].special[l].discountedNights;
                                    objspecial[l].pay = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].special[l].pay;
                                }
                                else
                                {
                                    objspecial[l] = null;
                                }
                                objLRoomType[k].special.Add(objspecial[l]);
                            }
                            //.......tttttt


                            //objCancellationPolicy[k] = new CommonLib.Response.CancellationPolicy();
                            //if (objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules != null)
                            //{
                            //    objCancellationPolicy[k].AmendRestricted = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[0].AmendRestricted;
                            //    objCancellationPolicy[k].CancelRestricted = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[0].CancelRestricted;
                            //    if (objCancellationPolicy[k].AmendRestricted == true || objCancellationPolicy[k].CancelRestricted == true)
                            //    {
                            //        objCancellationPolicy[k].dayMax = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[0].fromDate;
                            //        objCancellationPolicy[k].AgentCancellationMarkup = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[0].AgentMarkup;
                            //        objCancellationPolicy[k].AgentRoomMarkup = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].AgentMarkup;
                            //        objCancellationPolicy[k].CancellationAmount = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[0].cancelCharge;
                            //        objCancellationPolicy[k].CUTCancellationAmount = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[0].CUTcancelCharge;
                            //        objCancellationPolicy[k].CutRoomAmount = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].CUTPrice;
                            //        objCancellationPolicy[k].NoShowPolicy = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[0].NoShowPolicy;
                            //    }
                            //    else
                            //    {
                            //        objCancellationPolicy[k].dayMax = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[1].fromDate;
                            //        objCancellationPolicy[k].AgentCancellationMarkup = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[1].AgentMarkup;
                            //        objCancellationPolicy[k].AgentRoomMarkup = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].AgentMarkup;
                            //        objCancellationPolicy[k].CancellationAmount = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[1].cancelCharge;
                            //        objCancellationPolicy[k].CUTCancellationAmount = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[1].CUTcancelCharge;
                            //        objCancellationPolicy[k].CutRoomAmount = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].CUTPrice;
                            //        objCancellationPolicy[k].NoShowPolicy = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[0].NoShowPolicy;
                            //    }

                            //    objCancellationPolicy[k].deduction = objDoTWHotel.HotelDetails[i].Room[j].RoomType[k].cancellationRules[0].cancelCharge;

                            //    //objCancellationPolicy[k].StaffCancellationMarkup = objHotel.HotelDetail[i].sRooms[j].sRoom[0].StaffCancellationMarkup[k];
                            //    objCancellationPolicy[k].unit = "";
                            //}
                            //objRoom[j].CancellationPolicy.Add(objCancellationPolicy[k]);

                            objRoom[j].LRoomType.Add(objLRoomType[k]);
                        }
                        objCommonHotelDetails[i].Room.Add(objRoom[j]);
                    }
                    objCommonHotelDetails[i].CUTPrice = objDoTWHotel.HotelDetails[i].CUTPrice;
                    objCommonHotelDetails[i].AgentMarkup = objDoTWHotel.HotelDetails[i].AgentMarkup;
                    //objCommonHotelDetails[i].UserRating = Convert.ToSingle(objDoTWHotel.HotelDetails[i].Rating);
                    lstHotels.Add(objCommonHotelDetails[i]);
                }
            }

            #endregion DoTW

            #region Xpedia
            if (objEANHotels.HotelDetail != null)
            {
                for (int i = 0; i < objEANHotels.HotelDetail.Count; i++)
                {
                    objCommonHotelDetails[i] = new CommonLib.Response.CommonHotelDetails();
                    objCommonHotelDetails[i].Address = objEANHotels.HotelDetail[i].address1[0];
                    //#3
                    CommonLib.Response.Category objCategory = new CommonLib.Response.Category();
                    objCategory.Name = objEANHotels.HotelDetail[i].hotelRating;
                    objCommonHotelDetails[i].Category = AllowExpediaCategory(objEANHotels.HotelDetail[i].hotelRating);
                    objCommonHotelDetails[i].HotelId = objEANHotels.HotelDetail[i].hotelId;
                    objCommonHotelDetails[i].HotelName = objEANHotels.HotelDetail[i].name;
                    objCommonHotelDetails[i].Supplier = "Expedia";
                    objCommonHotelDetails[i].CUTPrice = Convert.ToSingle(objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.CutRoomAmmount);
                    objCommonHotelDetails[i].FeeAtHotel = Convert.ToSingle(objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.MandatoryTax);
                    objCommonHotelDetails[i].AgentMarkup = Convert.ToSingle(objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.AgentRoomMarkup);
                    objCommonHotelDetails[i].Currency = "USD";
                    objCommonHotelDetails[i].DateFrom = objEANHotels.HotelDetail[i].DateFrom;
                    objCommonHotelDetails[i].DateTo = objEANHotels.HotelDetail[i].DateTo;
                    objCommonHotelDetails[i].Description = objEANHotels.HotelDetail[i].shortDescription;
                    //#1
                    List<string> Facility = new List<string>();
                    for (int j = 0; j < objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.ValueAdds.ValueAdd.Count; j++)
                    {
                        if (objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.ValueAdds.ValueAdd[j].Service != "")
                            Facility.Add(objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.ValueAdds.ValueAdd[j].Service);
                    }
                    objCommonHotelDetails[i].Facility = Facility;
                    foreach (string Fname in objCommonHotelDetails[i].Facility)
                    {
                        //CommonLib.Response.Facilities objFacilities = new CommonLib.Response.Facilities();
                        //objFacilities.Name = Fname;
                        //objFacilities.HotelCode = objHotel.HotelDetail[i].HotelID;
                        //objFacilities.Count = 1;
                        //lstFacilities.Add(objFacilities);
                        CommonLib.Response.Facility objFacility = new CommonLib.Response.Facility();
                        objFacility.FacilitiesName = Fname;
                        objCommonHotelDetails[i].HotelId = objEANHotels.HotelDetail[i].hotelId;
                        objFacility.Supplier = "Expedia";
                        objFacility.Count = 1;
                        //HotelFacility(objFacility);
                        if (lstFacility.Count > 0)
                        {
                            var Main_List_Location = lstFacility.Where(data => data.FacilitiesName == objFacility.FacilitiesName).FirstOrDefault();
                            if (Main_List_Location != null)
                                lstFacility.Remove(Main_List_Location);
                            else
                                Main_List_Location = new CommonLib.Response.Facility { FacilitiesName = objFacility.FacilitiesName, Count = 0, Supplier = "Expedia" };
                            Main_List_Location.Count = Main_List_Location.Count + 1;
                            lstFacility.Add(Main_List_Location);
                        }
                        else
                        {
                            lstFacility.Add(objFacility);
                        }
                    }
                    string code = AllowExpediaCategory(objEANHotels.HotelDetail[i].hotelRating);
                    var Main_Hotel_Category = lstCategory.Where(data => data.Name == code).FirstOrDefault();
                    if (lstCategory.Count > 0)
                    {
                        if (Main_Hotel_Category != null)
                            lstCategory.Remove(Main_Hotel_Category);
                        else
                            Main_Hotel_Category = new CommonLib.Response.Category { Name = code, Count = 0, Supplier = "Expedia" };
                        Main_Hotel_Category.Count = Main_Hotel_Category.Count + 1;
                        lstCategory.Add(Main_Hotel_Category);
                    }
                    else
                    {
                        Main_Hotel_Category = new CommonLib.Response.Category { Name = code, Count = 0, Supplier = "Expedia" };
                        Main_Hotel_Category.Count = Main_Hotel_Category.Count + 1;
                        lstCategory.Add(Main_Hotel_Category);
                    }
                    // Location 
                    objCommonHotelDetails[i].Langitude = objEANHotels.HotelDetail[i].longitude;
                    objCommonHotelDetails[i].Latitude = objEANHotels.HotelDetail[i].latitude;
                    objCommonHotelDetails[i].Room = new List<CommonLib.Response.Room>();
                    CommonLib.Response.Location[] objLocation = new CommonLib.Response.Location[objEANHotels.HotelDetail[i].address1.Count];
                    for (int j = 0; j < objEANHotels.HotelDetail[i].address1.Count; j++)
                    {
                        string LOC = objEANHotels.HotelDetail[i].address1[j];
                        objLocation[j] = new CommonLib.Response.Location();
                        objLocation[j].Name = LOC;
                        objLocation[j].HotelCode = objEANHotels.HotelDetail[i].hotelId;
                        objLocation[j].Supplier = "Expedia";
                        objLocation[j].Count = 1;
                        List<CommonLib.Response.Location> listLocation = new List<CommonLib.Response.Location>();
                        listLocation.Add(objLocation[j]);
                        objCommonHotelDetails[i].Location = listLocation;
                        if (lstLocation.Count > 0)
                        {
                            var Main_List_Location = lstLocation.Where(data => data.Name == objLocation[j].Name).FirstOrDefault();
                            if (Main_List_Location != null)
                                lstLocation.Remove(Main_List_Location);
                            else
                            {
                                Main_List_Location = new CommonLib.Response.Location { Name = objLocation[j].Name, Count = 0, Supplier = "Expedia" };
                            }
                            Main_List_Location.Count = Main_List_Location.Count + 1;
                            lstLocation.Add(Main_List_Location);
                        }
                        else
                        {
                            lstLocation.Add(objLocation[j]);
                        }
                    }
                    // End lOCATION

                    // Hotel Image //
                    CommonLib.Response.Image objImage = new CommonLib.Response.Image();
                    objImage = new CommonLib.Response.Image();
                    objImage.Url = "http://media.expedia.com/" + objEANHotels.HotelDetail[i].thumbNailUrl.Replace("_t", "_b");
                    objImage.Title = objEANHotels.HotelDetail[i].name;
                    List<CommonLib.Response.Image> listImage = new List<CommonLib.Response.Image>();
                    listImage.Add(objImage);
                    objCommonHotelDetails[i].Image = listImage;

                    // End Image
                    List<EANLib.Response.ValueAdd> ValueAddist = objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.ValueAdds.ValueAdd;
                    // List<string> Facilities = new List<string>();
                    //objCommonHotelDetails[i].Facility = objEANHotels.Facility;

                    // Rooms  //
                    CommonLib.Response.Room[] objRoom = new CommonLib.Response.Room[1];
                    CommonLib.Response.RoomOccupancy[] objRoomOccupancy = new CommonLib.Response.RoomOccupancy[1];
                    int Rooms = Convert.ToInt16(objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.RoomGroup.Room.Count);
                    objCommonHotelDetails[i].sRooms = new List<CommonLib.Response.RoomOccupancy>();
               
                    objRoomOccupancy[0] = new CommonLib.Response.RoomOccupancy();
                    objRoomOccupancy[0].RoomID = objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.roomTypeCode.ToString();
                    objRoomOccupancy[0].sRoom = new List<CommonLib.Response.sRoom>();
                    objRoomOccupancy[0].RoomPrice = Convert.ToSingle(objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.total);
                    objRoomOccupancy[0].CUTRoomPrice = Convert.ToSingle(objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.CutRoomAmmount);
                    objRoomOccupancy[0].AgentMarkup = Convert.ToSingle((objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.AgentRoomMarkup));
                    objRoomOccupancy[0].StaffMarkup = 0;
                    CommonLib.Response.sRoom objsRoom = new CommonLib.Response.sRoom();
                    objsRoom.onRequest = "";
                    objsRoom.availCount = objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.currentAllotment.ToString();
                    objsRoom.SHRUI = "";
                    objsRoom.Boardtype = "";
                    objsRoom.Boardcode = objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.expediaPropertyId.ToString();
                    objsRoom.Boardshortname = objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.roomDescription;
                    objsRoom.Boardtext = objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.roomDescription;
                    //objsRoom.Roomtype = objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.BedTypes.BedType[0].Description;
                    objsRoom.Roomcode = objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.roomTypeCode.ToString();
                    objsRoom.Roomcharacteristic = objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.roomTypeCode.ToString();
                    objsRoom.Roomtext = objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.roomDescription;
                    objsRoom.RoomAmount = Convert.ToSingle(objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.total);
                    objsRoom.CUTRoomAmount = Convert.ToSingle(objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.CutRoomAmmount);
                    objsRoom.AgentRoomMarkup = Convert.ToSingle(objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.AgentRoomMarkup);
                    objsRoom.Roomdate = "";
                    objsRoom.Roomtime = "";
                    objsRoom.CancellationAmount = null;
                    objsRoom.CUTCancellationAmount = null;
                    objsRoom.AgentCancellationMarkup = null;
                    objsRoom.NoChargeDate = DateTime.Now;
                    objsRoom.ChargeDate = null;
                    objsRoom.sNoChargeDate = null;
                    objsRoom.sChargeDate = null;
                    objsRoom.RoomCount = Rooms;
                    objsRoom.AdultCount = Convert.ToUInt16(objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.RoomGroup.Room[0].numberOfAdults);
                    objsRoom.ChildCount = Convert.ToUInt16(objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.RoomGroup.Room[0].numberOfChildren);
                    objsRoom.StaffRoomMarkup = null;
                    objsRoom.StaffCancellationMarkup = null;
                    objRoomOccupancy[0].sRoom.Add(objsRoom);
                    objCommonHotelDetails[i].sRooms.Add(objRoomOccupancy[0]);


                    // End  Rooms //


                    lstHotels.Add(objCommonHotelDetails[i]);
                }
            }


            #endregion
            #region GRN
            if (objGRNHotels.HotelDetail != null)
            {
                for (int i = 0; i < objGRNHotels.HotelDetail.hotels.Count; i++)
                {
                    objCommonHotelDetails[i] = new CommonLib.Response.CommonHotelDetails();
                    objCommonHotelDetails[i].Address = objGRNHotels.HotelDetail.hotels[i].address;
                    CommonLib.Response.Category objCategory = new CommonLib.Response.Category();
                    objCategory.Name = objGRNHotels.HotelDetail.hotels[i].category.ToString();
                    objCommonHotelDetails[i].Category = AllowExpediaCategory(objGRNHotels.HotelDetail.hotels[i].category.ToString());
                    objCommonHotelDetails[i].HotelId = objGRNHotels.HotelDetail.hotels[i].hotel_code;
                    objCommonHotelDetails[i].HotelName = objGRNHotels.HotelDetail.hotels[i].name;
                    objCommonHotelDetails[i].Supplier = "GRN";
                    objCommonHotelDetails[i].CUTPrice = objGRNHotels.HotelDetail.hotels[i].MaxCutPrice;
                    //objCommonHotelDetails[i].FeeAtHotel = Convert.ToSingle(objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.MandatoryTax);
                    objCommonHotelDetails[i].AgentMarkup = objGRNHotels.HotelDetail.hotels[i].AgentMarkup;
                    objCommonHotelDetails[i].Currency = "INR";
                    objCommonHotelDetails[i].DateFrom = objGRNHotels.FromDate;
                    objCommonHotelDetails[i].DateTo = objGRNHotels.ToDate;
                    objCommonHotelDetails[i].Description = objGRNHotels.HotelDetail.hotels[i].description;
                    //#1
                    /*List<string> Facility = new List<string>();
                    
                    for (int j = 0; j < objGRNHotels.Facility.Count; j++)
                    {
                        if (objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.ValueAdds.ValueAdd[j].Service != "")
                            Facility.Add(objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.ValueAdds.ValueAdd[j].Service);
                    } */
                    if (objGRNHotels.HotelDetail.hotels[i].facilities != null)
                    {
                        objCommonHotelDetails[i].Facility = objGRNHotels.HotelDetail.hotels[i].facilities.Split(';').ToList();
                        foreach (string Fname in objCommonHotelDetails[i].Facility)
                        {
                            //CommonLib.Response.Facilities objFacilities = new CommonLib.Response.Facilities();
                            //objFacilities.Name = Fname;
                            //objFacilities.HotelCode = objHotel.HotelDetail[i].HotelID;
                            //objFacilities.Count = 1;
                            //lstFacilities.Add(objFacilities);
                            CommonLib.Response.Facility objFacility = new CommonLib.Response.Facility();
                            objFacility.FacilitiesName = Fname;
                            objCommonHotelDetails[i].HotelId = objGRNHotels.HotelDetail.hotels[i].hotel_code;
                            objFacility.Supplier = "GRN";
                            objFacility.Count = 1;
                            //HotelFacility(objFacility);
                            if (lstFacility.Count > 0)
                            {
                                var MyFacility = lstFacility.Where(data => data.FacilitiesName == objFacility.FacilitiesName).FirstOrDefault();
                                if (MyFacility != null)
                                    lstFacility.Remove(MyFacility);
                                else
                                    MyFacility = new CommonLib.Response.Facility { FacilitiesName = objFacility.FacilitiesName, Count = 0, Supplier = "GRN" };
                                MyFacility.Count = MyFacility.Count + 1;
                                lstFacility.Add(MyFacility);
                            }
                            else
                                lstFacility.Add(objFacility);
                        }
                    }
                    string code = AllowExpediaCategory(objGRNHotels.HotelDetail.hotels[i].category.ToString());
                    var Main_Hotel_Category = lstCategory.Where(data => data.Name == code).FirstOrDefault();
                    if (lstCategory.Count > 0)
                    {
                        if (Main_Hotel_Category != null)
                            lstCategory.Remove(Main_Hotel_Category);
                        else
                            Main_Hotel_Category = new CommonLib.Response.Category { Name = code, Count = 0, Supplier = "GRN" };
                        Main_Hotel_Category.Count = Main_Hotel_Category.Count + 1;
                        lstCategory.Add(Main_Hotel_Category);
                    }
                    else
                    {
                        Main_Hotel_Category = new CommonLib.Response.Category { Name = code, Count = 0, Supplier = "GRN" };
                        Main_Hotel_Category.Count = Main_Hotel_Category.Count + 1;
                        lstCategory.Add(Main_Hotel_Category);
                    }
                    // Location 
                    objCommonHotelDetails[i].Langitude = objGRNHotels.HotelDetail.hotels[i].geolocation.longitude.ToString();
                    objCommonHotelDetails[i].Latitude = objGRNHotels.HotelDetail.hotels[i].geolocation.latitude.ToString();
                    objCommonHotelDetails[i].Room = new List<CommonLib.Response.Room>();
                    CommonLib.Response.Location[] objLocation = new CommonLib.Response.Location[1];
                    for (int j = 0; j < 1; j++)
                    {
                        string LOC = objGRNHotels.HotelDetail.hotels[i].address;
                        objLocation[j] = new CommonLib.Response.Location();
                        objLocation[j].Name = LOC;
                        objLocation[j].HotelCode = objGRNHotels.HotelDetail.hotels[i].hotel_code;
                        objLocation[j].Supplier = "GRN";
                        objLocation[j].Count = 1;
                        List<CommonLib.Response.Location> listLocation = new List<CommonLib.Response.Location>();
                        listLocation.Add(objLocation[j]);
                        objCommonHotelDetails[i].Location = listLocation;
                        if (lstLocation.Count > 0)
                        {
                            var Main_List_Location = lstLocation.Where(data => data.Name == objLocation[j].Name).FirstOrDefault();
                            if (Main_List_Location != null)
                                lstLocation.Remove(Main_List_Location);
                            else
                            {
                                Main_List_Location = new CommonLib.Response.Location { Name = objLocation[j].Name, Count = 0, Supplier = "GRN" };
                            }
                            Main_List_Location.Count = Main_List_Location.Count + 1;
                            lstLocation.Add(Main_List_Location);
                        }
                        else
                        {
                            lstLocation.Add(objLocation[j]);
                        }
                    }
                    // End lOCATION

                    // Hotel Image //
                    CommonLib.Response.Image objImage = new CommonLib.Response.Image();
                    objImage = new CommonLib.Response.Image();
                    objImage.Url = "http://cdn.grnconnect.com/" + objGRNHotels.HotelDetail.hotels[i].images.main_image.ToString();
                    objImage.Title = objGRNHotels.HotelDetail.hotels[i].name;
                    List<CommonLib.Response.Image> listImage = new List<CommonLib.Response.Image>();
                    listImage.Add(objImage);
                    objCommonHotelDetails[i].Image = listImage;

                    // End Image
                    //List<GRNLib.Response.ValueAdd> ValueAddist = objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.ValueAdds.ValueAdd;
                    // List<string> Facilities = new List<string>();
                    //objCommonHotelDetails[i].Facility = objEANHotels.Facility;

                    // Rooms  //
                    CommonLib.Response.Room[] objRoom = new CommonLib.Response.Room[1];
                    CommonLib.Response.RoomOccupancy[] objRoomOccupancy = new CommonLib.Response.RoomOccupancy[objGRNHotels.HotelDetail.hotels[i].rates.Count];
                    int Rate = Convert.ToInt16(objGRNHotels.HotelDetail.hotels[i].rates.Count);
                    objCommonHotelDetails[i].sRooms = new List<CommonLib.Response.RoomOccupancy>();
                    for (int j = 0; j < Rate; j++)
                    {
                        objRoomOccupancy[j] = new CommonLib.Response.RoomOccupancy();
                        objRoomOccupancy[j].RoomID = objGRNHotels.HotelDetail.hotels[i].rates[j].rate_key.ToString();
                        objRoomOccupancy[j].sRoom = new List<CommonLib.Response.sRoom>();
                        objRoomOccupancy[j].RoomPrice = Convert.ToSingle(objGRNHotels.HotelDetail.hotels[i].rates[j].price);
                        objRoomOccupancy[j].CUTRoomPrice = Convert.ToSingle(objGRNHotels.HotelDetail.hotels[i].rates[j].CUTRoomPrice);
                        objRoomOccupancy[j].AgentMarkup = Convert.ToSingle((objGRNHotels.HotelDetail.hotels[i].rates[j].AgentMarkup));
                        objRoomOccupancy[j].StaffMarkup = 0;
                        CommonLib.Response.sRoom objsRoom = new CommonLib.Response.sRoom();
                        int Room = Convert.ToInt16(objGRNHotels.HotelDetail.hotels[i].rates[j].rooms.Count);
                        for (int k = 0; k < Room; k++)
                        {
                            objsRoom = new CommonLib.Response.sRoom();
                            objsRoom.onRequest = "";
                            objsRoom.availCount = objGRNHotels.HotelDetail.hotels[i].rates[j].allotment.ToString();
                            objsRoom.SHRUI = "";
                            objsRoom.Boardtype = "";
                            //objsRoom.Boardcode = objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.expediaPropertyId.ToString();
                            //objsRoom.Boardshortname = objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.roomDescription;
                            //objsRoom.Boardtext = objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.roomDescription;
                            objsRoom.Roomtype = objGRNHotels.HotelDetail.hotels[i].rates[j].rooms[k].room_type;
                            objsRoom.Roomcode = objGRNHotels.HotelDetail.hotels[i].rates[j].room_code;
                            //objsRoom.Roomcharacteristic = objEANHotels.HotelDetail[i].RoomRateDetailsList.RoomRateDetails.roomTypeCode.ToString();
                            if (objGRNHotels.HotelDetail.hotels[i].rates[j].rooms[k].description != null)
                                objsRoom.Roomtext = objGRNHotels.HotelDetail.hotels[i].rates[j].rooms[k].description;
                            else
                                objsRoom.Roomtext = "";

                            objsRoom.RoomAmount = objGRNHotels.HotelDetail.hotels[i].rates[j].price;
                            objsRoom.CUTRoomAmount = objGRNHotels.HotelDetail.hotels[i].rates[j].CUTRoomPrice;
                            objsRoom.AgentRoomMarkup = objGRNHotels.HotelDetail.hotels[i].rates[j].AgentMarkup;
                            objsRoom.Roomdate = "";
                            objsRoom.Roomtime = "";
                            objsRoom.CancellationAmount = null;
                            objsRoom.CUTCancellationAmount = null;
                            objsRoom.AgentCancellationMarkup = null;
                            objsRoom.NoChargeDate = DateTime.Now;
                            objsRoom.ChargeDate = null;
                            objsRoom.sNoChargeDate = null;
                            objsRoom.sChargeDate = null;
                            objsRoom.RoomCount = Rate;
                            objsRoom.AdultCount = objGRNHotels.HotelDetail.hotels[i].rates[j].rooms[k].no_of_adults;
                            objsRoom.ChildCount = objGRNHotels.HotelDetail.hotels[i].rates[j].rooms[k].no_of_children;
                            objsRoom.StaffRoomMarkup = null;
                            objsRoom.StaffCancellationMarkup = null;
                            objRoomOccupancy[j].sRoom.Add(objsRoom);
                        }
                        objCommonHotelDetails[i].sRooms.Add(objRoomOccupancy[j]);
                    }
                    // End  Rooms //
                    lstHotels.Add(objCommonHotelDetails[i]);
                }

            }
            #endregion
            lstHotels = lstHotels.OrderBy(data => data.CUTPrice).ToList();


            //..............................
            return lstHotels;
        }

        public static List<string> GetCommonFacilities(List<string> oldFacilities)
        {
            List<string> CommonFacilities = new List<string>();
            foreach (string Facility in oldFacilities)
            {
                // if(Facility.Contains("Fridge"))



            }
            return CommonFacilities;
        }
        public static List<CommonLib.Response.CommonHotelDetails> GetPreferred(List<CommonLib.Response.CommonHotelDetails> lstHotels)
        {
            CommonLib.Response.CommonHotelDetails[] lstPreferedHotel = new CommonLib.Response.CommonHotelDetails[lstHotels.OrderBy(data => data.CUTPrice).ToList().Where(data => data.PreferedHotel == true).ToList().Count];
            lstPreferedHotel = lstHotels.OrderBy(data => data.CUTPrice).ToList().Where(data => data.PreferedHotel == true).ToList().ToArray();

            //Non Prefered Hotel List //
            CommonLib.Response.CommonHotelDetails[] LstWithOutPrefered = new CommonLib.Response.CommonHotelDetails[lstHotels.OrderBy(data => data.CUTPrice).ToList().Where(data => data.PreferedHotel == false).ToList().Count];
            LstWithOutPrefered = lstHotels.OrderBy(data => data.CUTPrice).ToList().Where(data => data.PreferedHotel == false).ToList().ToArray();

            var result = lstPreferedHotel.ToList().Union((LstWithOutPrefered.ToList()));
            lstHotels = result.ToList();
            return lstHotels;
        }

       
        private string AllowCategory(string code)
        {
            if (code == "1EST")
                return code;
            else if (code == "2EST")
                return code;
            else if (code == "3EST")
                return code;
            else if (code == "4EST")
                return code;
            else if (code == "5EST")
                return code;
            else
                return "Other";
        }

        private string AllowDoTWCategory(string code)
        {
            if (code == "559")
                return code;
            else if (code == "560")
                return code;
            else if (code == "561")
                return code;
            else if (code == "562")
                return code;
            else if (code == "563")
                return code;
            else
                return "48055";
        }
        private string AllowMghCategory(string code)
        {
            if (code == "1 Star Hotel")
                return code = "1";
            else if (code == "2 Star Hotel")
                return code = "2";
            else if (code == "3 Star Hotel")
                return code = "3";
            else if (code == "4 Star Hotel")
                return code = "4";
            else if (code == "5 Star Hotel")
                return code = "5";
            else
                return "Other";
        }
        private string AllowExpediaCategory(string code)
        {
            if (code == "0")
                return code = "0";
            if (code == "1" || code == "1.0")
                return code = "1";
            else if (code == "2" || code == "2.5")
                return code = "2";
            else if (code == "3" || code == "3.5")
                return code = "3";
            else if (code == "4" || code == "4.5")
                return code = "4";
            else if (code == "5" || code == "5")
                return code = "5";
            else
                return "Other";
        }
        #region Distinct Serach Occupancy
        public static List<HotelOccupancy> GetSearchOccupancy()
        {
            List<HotelOccupancy> DISTINST_OCCUPANCY = new List<HotelOccupancy>();
            List<CommonLib.Response.Room> list_CommonOcupancyRoom = new System.Collections.Generic.List<CommonLib.Response.Room>();
            string[] array_input = HttpContext.Current.Session["session"].ToString().Split('_');
            string[] array_occupancy = array_input[5].ToString().Split('$');
            List<CommonLib.Response.HotelOccupancy> list_Occupancy = new System.Collections.Generic.List<CommonLib.Response.HotelOccupancy>();
            int adult = 0;
            int child = 0;
            foreach (string str in array_occupancy)
            {
                string[] m_array = str.Split('|');
                adult = adult + Convert.ToInt32(m_array[0]);
                string[] n_array = m_array[1].ToString().Split('^');
                child = child + Convert.ToInt32(n_array[0]);
                List<CommonLib.Response.Customers> list_Customer = new List<CommonLib.Response.Customers>();
                string ChildAges = "";
                for (int i = 0; i < n_array.Length; i++)
                {
                    if (i > 0)
                    {
                        if (n_array[i] == "")
                            continue;
                        //...............................under test by maqsood...........................................//
                        int Age = Convert.ToInt32(n_array[i]);
                        //...............................under test by maqsood...........................................//
                        if (Age > 0)
                        {
                            ChildAges += Age.ToString();
                            if (n_array.Length - 1 != i)
                                ChildAges += ",";
                            list_Customer.Add(new CommonLib.Response.Customers { Age = Age, type = "CH" });
                        }

                    }
                }
                list_Occupancy.Add(new HotelOccupancy { RoomCount = 1, AdultCount = Convert.ToInt32(m_array[0]), ChildCount = Convert.ToInt32(n_array[0]), GuestList = list_Customer, ChildAges = ChildAges });
            }
           
            return list_Occupancy;
        }
        public static List<HotelOccupancy> GetDISTINST_OCCUPANCY(List<HotelOccupancy> list_Occupancy)
        {
            List<HotelOccupancy> DISTINST_OCCUPANCY = new List<HotelOccupancy>();
            int r = 1;
            List<int> RoomNo = new List<int>();
            foreach (CommonLib.Response.HotelOccupancy Check_Duplicate_Occupancy in list_Occupancy)
            {

                if (!DISTINST_OCCUPANCY.Exists(data => data.AdultCount == Check_Duplicate_Occupancy.AdultCount && data.ChildCount == Check_Duplicate_Occupancy.ChildCount && data.ChildAges == Check_Duplicate_Occupancy.ChildAges))
                {
                    RoomNo = new List<int>();
                    RoomNo.Add(r);
                    int noRoom = list_Occupancy.Where(d => d.AdultCount == Check_Duplicate_Occupancy.AdultCount && d.ChildCount == Check_Duplicate_Occupancy.ChildCount && d.ChildAges == Check_Duplicate_Occupancy.ChildAges).ToList().Count;
                    DISTINST_OCCUPANCY.Add(new HotelOccupancy
                    {
                        RoomCount = noRoom,
                        RoomNo = RoomNo,
                        AdultCount = Check_Duplicate_Occupancy.AdultCount,
                        ChildCount = Check_Duplicate_Occupancy.ChildCount,
                        ChildAges = Check_Duplicate_Occupancy.ChildAges,
                    });
                }
                else
                {
                    DISTINST_OCCUPANCY.Where(d => d.AdultCount == Check_Duplicate_Occupancy.AdultCount && d.ChildCount == Check_Duplicate_Occupancy.ChildCount && d.ChildAges == Check_Duplicate_Occupancy.ChildAges).FirstOrDefault().RoomNo.Add(r);
                }
                r++;
            }
            return DISTINST_OCCUPANCY;
        }
        #endregion
    }
}