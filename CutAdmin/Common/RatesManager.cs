﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CommonLib.Response;
using System.Globalization;
using CutAdmin.BL;
using CutAdmin.DataLayer;
namespace CutAdmin.Common
{
    public class RatesManager
    {
        public static string CheckInDate, CheckOutDate;
        static DBHandlerDataContext db = new DBHandlerDataContext();
        static dbTaxHandlerDataContext dbTax = new dbTaxHandlerDataContext();

        #region Lib
        public class RatePrice
        {
            public string Currency { get; set; }
            public List<DateTime> Dates { get; set; }
            public string RateType { get; set; } /* Special  Date / Genral Date*/
            public string Daywise { get; set; }
            public decimal RRRate { get; set; }
            public decimal EBRate { get; set; }
            public decimal CWBRate { get; set; }
            public decimal CNBRate { get; set; }
            public decimal MonRate { get; set; }
            public decimal TueRate { get; set; }
            public decimal WedRate { get; set; }
            public decimal ThuRate { get; set; }
            public decimal FriRate { get; set; }
            public decimal SatRate { get; set; }
            public decimal SunRate { get; set; }
            public string CancelationPolicy { get; set; }
            public string CheckinDate { get; set; }
            public string CheckoutDate { get; set; }
            public string OfferID { get; set; }
            public string RateID { get; set; }
        }

        public class OfferRate
        {
            public List<DateTime> Dates { get; set; }
            public string Daywise { get; set; }
            public string OfferID { get; set; }
        }

        public class OfferSeason
        {
            public DateTime Dates { get; set; }
            public string OfferType { get; set; }
            public float DisountAmount { get; set; }
            public float DisountPercent { get; set; }
            public float NewRate { get; set; }
        }

        public class Supplier
        {
            public string Name { get; set; }
            public string HotelCode { get; set; }
            public string HotelName { get; set; }
            public string Nationality { get; set; }
            public string NationalityName { get; set; }
            public int noRooms { get; set; }
            public List<Details> Details { get; set; }

        }

        public class Details
        {
            public int RoomIndex { get; set; }
            public int RateTypeID { get; set; }
            public string RateType { get; set; }
            public int MealID { get; set; }
            public string MealName { get; set; }
            public int noRooms { get; set; }
            public Rate Rate { get; set; }

        }

        public class Rate
        {
            public string Currency { get; set; }
            public int RateID { get; set; }
            public float EBRate { get; set; }
            public float CNBRate { get; set; }
            public float CWBRate { get; set; }
            public Int64 MaxOccupancy { get; set; }
            public Int64 RoomOccupancy { get; set; }
            public Int64 MaxEB { get; set; }
            public Int64 MaxCWB { get; set; }
            public List<ListDates> ListDates { get; set; }
            public float SupplierMarkup { get; set; }
            public float CutMarkup { get; set; }
            public float TotalPrice { get; set; }
            public ChargeRate TotalCharge { get; set; }
            public List<CommonLib.Response.CancellationPolicy> ListCancellation { get; set; }
            public List<string> ListCancel { get; set; }
            public List<CommonLib.Request.Customer> LisCustumer { get; set; }
        }
        public class ListDates
        {

            public string RateID { get; set; }
            public string Date { get; set; }
            public ChargeRate Rate { get; set; }
            public ChargeRate EBRate { get; set; }
            public ChargeRate CWBRate { get; set; }
            public ChargeRate CNBRate { get; set; }
            public float TotalPrice { get; set; }
            public string Type { get; set; }
            public List<CutAdmin.BookingHandler.Addons> ListCommAddons { get; set; }

        }
        #endregion

        #region GetRateList
        public static List<TaxRate> GetRates(Int64 HotelCode)
        {
            List<TaxRate> arrRates = new List<TaxRate>();
            try
            {
                var TaxRates = dbTax.Comm_TaxMappings.Where(d => d.HotelID == HotelCode && d.IsAddOns == false).ToList();
                foreach (var objRate in TaxRates.Select(d => d.TaxID).Distinct().ToList())
                {
                    var ListTaxes = TaxRates.Where(r => r.TaxID == objRate).ToList().Select(d => d.TaxOnID).Distinct().ToList();
                    List<Tax> arrTax = new List<Tax>();
                    foreach (var objTax in ListTaxes)
                    {
                        if (dbTax.Comm_Taxes.Where(d => d.ID == objTax).ToList().Count != 0)
                        {
                            arrTax.Add(new Tax
                            {
                                ID = Convert.ToInt64(objTax),
                                TaxName = dbTax.Comm_Taxes.Where(d => d.ID == objTax).FirstOrDefault().Name,
                                TaxPer = Convert.ToSingle(dbTax.Comm_Taxes.Where(d => d.ID == objTax).FirstOrDefault().Value),
                                TaxRate = 0,
                            });
                        }
                        else if (objTax == 0)
                        {
                            arrTax.Add(new Tax
                            {
                                TaxName = "Base Rate",
                                TaxPer = 0,
                                TaxRate = 0,
                            });
                        }

                    }
                    if (dbTax.Comm_Taxes.Where(d => d.ID == objRate).ToList().Count != 0)
                    {
                        arrRates.Add(new TaxRate
                        {
                            ID = Convert.ToInt64(objRate),
                            RateName = dbTax.Comm_Taxes.Where(d => d.ID == objRate).FirstOrDefault().Name + " " + dbTax.Comm_Taxes.Where(d => d.ID == objRate).FirstOrDefault().Value + " (%)",
                            Per = Convert.ToSingle(dbTax.Comm_Taxes.Where(d => d.ID == objRate).FirstOrDefault().Value),
                            TaxOn = arrTax
                        });
                    }
                }
            }
            catch
            {

            }
            return arrRates;
        }

        public static date GetTaxRate(date objDate, Int64 HotelCode)
        {
            date arrDate = objDate;
            float SupplierMarkup = 0,S2SMarkup=0;
            MarkupTaxManager.objMarkupCommission = objMarckupCommission;
            objDate.AdminMarkup = MarkupTaxManager.GetRoomAmount(arrDate.Total, out SupplierMarkup, out S2SMarkup);
            objDate.Markup = SupplierMarkup;
            objDate.S2SMarkup = S2SMarkup;
            arrDate.ListRates = new List<TaxRate>();
            arrDate.ListRates =new List<TaxRate>();
            if (arrDate.ListRates != null )
            {
                arrDate.ListRates = TaxManager.GetRacRates(ListRateTypes, arrDate.Total);
            }
            arrDate.Total = arrDate.Total + arrDate.AdminMarkup + arrDate.Markup + S2SMarkup;
            return arrDate;
        }

        public static ChargeRate GetRate(float Rate, string Name)
        {
            ChargeRate objChageRate = new ChargeRate();

            float SupplierMarkup = 0, S2SMarkup=0;
            MarkupTaxManager.objMarkupCommission = objMarckupCommission;
            float AdminMarkup = MarkupTaxManager.GetRoomAmount(Rate, out SupplierMarkup, out S2SMarkup);
            float Markup = SupplierMarkup;
            objChageRate.BaseRate = Rate;
            objChageRate.Charge = TaxManager.GetRacRates(ListRateTypes, Rate); ;
            objChageRate.TotalRate = objChageRate.BaseRate + objChageRate.Charge.Select(d => d.TotalRate).ToList().Sum() + AdminMarkup + Markup + S2SMarkup;
            return objChageRate;
        }

        public static ChargeRate GetRateByCancellation(float Rate, out float SupplierMarkup, out float AdminMarkup,out float S2SMarkup)
        {
            ChargeRate objChageRate = new ChargeRate();
            SupplierMarkup = 0; AdminMarkup = 0; S2SMarkup = 0;
            try
            {
            MarkupTaxManager.objMarkupCommission = objMarckupCommission;
            SupplierMarkup = MarkupTaxManager.GetRoomAmount(Rate, out AdminMarkup, out S2SMarkup);
            float Markup = AdminMarkup;
            objChageRate.BaseRate = Rate;
            objChageRate.Charge = TaxManager.GetRacRates(ListRateTypes, Rate); ;
            objChageRate.TotalRate = objChageRate.BaseRate + objChageRate.Charge.Select(d => d.TotalRate).ToList().Sum() + SupplierMarkup + Markup;
            }
            catch{

            }
            return objChageRate;
        }

        public static List<TaxRate> ListRateTypes { get; set; }
        public static Int64 UserID { get; set; }
        public static  Int64 noDaysGap { get; set; }
        public static MarkupCommission objMarckupCommission { get; set; }
        /// <summary>
        ///  Get Rate By Hotels
        /// </summary>
        public static List<RateGroup> GetRateList(Int64 HotelCode, string CheckIn, string CheckOut, string[] Nationality, List<tbl_commonRoomDetail> ListRoom, string SearchValid)
        {
            List<DateTime> List_SearchDate = new List<DateTime>();
            List<RoomType> ListRate = new List<RoomType>();
            List<RateGroup> ListRateGroup = new List<RateGroup>();
            try
            {
                objMarckupCommission = new MarkupCommission();
                #region Assign Markup
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired ,Please Login and try Again!!");
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 uid = AccountManager.GetSupplierByUser();
                objMarckupCommission = MarkupTaxManager.GetMarkupTax(uid, HotelCode);
                using (var Context = new DBHandlerDataContext() )
                {
                    if (Context.Comm_CancellationDays.Where(d => d.UserID == uid).FirstOrDefault()!= null)
                        noDaysGap = Convert.ToInt64( Context.Comm_CancellationDays.Where(d => d.UserID == uid).FirstOrDefault().DaysBefore);
                }
                #endregion

                ListRateTypes = GetRates(HotelCode);

                CheckInDate = CheckIn;
                CheckOutDate = CheckOut;

                var SupplierID = new List<Int64>();
                using (var db = new DBHandlerDataContext())
                {
                    SupplierID = (from obj in db.tbl_commonRoomRates
                                  where obj.HotelID == HotelCode
                                  select Convert.ToInt64(obj.SupplierId)).Distinct().ToList();
                }
                foreach (var Supplier in SupplierID)
                {
                    List<CommonLib.Response.HotelOccupancy> ListRates = GetRate(HotelCode, Convert.ToInt64(Supplier), CheckIn, CheckOut, ListRoom, SearchValid, Nationality[0]);
                    if (objGlobalDefault.UserType == "Agent")
                    {
                        ListRateGroup.Add(new RateGroup
                        {
                            AgentMarkup = 0,
                            AvailToken = HotelCode.ToString(),
                            Charge = new ServiceCharge(),
                            CutPrice = 0,
                            //Name = (from obj in db.tbl_APIDetails where obj.Hotel == true && obj.sid == Supplier select obj).FirstOrDefault().Supplier + "_" + SupplierID.IndexOf(Supplier),
                            Name = Supplier.ToString(),
                            RoomOccupancy = ListRates,
                            Nationality = Nationality[0].ToString(),
                        });
                        if(ListRateGroup.LastOrDefault().RoomOccupancy.Count !=0)
                        {
                            ListRateGroup.LastOrDefault().Total = GetTotal(ListRates);
                        }
                    }
                       
                    else
                    {
                        foreach (var objNation in Nationality)
                        {
                            ListRates = GetRate(HotelCode, Convert.ToInt64(Supplier), CheckIn, CheckOut, ListRoom, SearchValid, objNation);
                            ListRateGroup.Add(new RateGroup
                            {
                                AgentMarkup = 0,
                                AvailToken = HotelCode.ToString(),
                                Charge = new ServiceCharge(),
                                CutPrice = 0,
                                //Name = (from obj in db.tbl_APIDetails where obj.Hotel == true && obj.sid == Supplier select obj).FirstOrDefault().Supplier +"_" +SupplierID.IndexOf(Supplier),
                                Name = Supplier.ToString(),
                                RoomOccupancy = ListRates,
                                Nationality = objNation.ToString(),
                            });
                            if (ListRateGroup.LastOrDefault().RoomOccupancy.Count != 0)
                            {
                                ListRateGroup.LastOrDefault().Total = GetTotal(ListRates);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
          
            return ListRateGroup;
        }

        public static float GetTotal(List<CommonLib.Response.HotelOccupancy> arrRates)
        {
            float TotalRate = 0;
            try
            {
                foreach (var objPrice in arrRates.Select(d=>d.MinPrice).ToList())
                {
                    var arrRate = arrRates.Where(d => d.MinPrice == objPrice).FirstOrDefault();
                    TotalRate += arrRate.MinPrice * arrRate.RoomCount;
                }
            }
            catch (Exception)
            {
            }
            return TotalRate;
        }
        #endregion

        #region GetRate
        public static List<CommonLib.Response.HotelOccupancy> GetRate(Int64 HotelCode, Int64 SupplierID, string Checkin, string Checkout, List<tbl_commonRoomDetail> rooms, string SearchValid, string Nationality)
        {
            List<CommonLib.Response.HotelOccupancy> ListSearch = ParseCommonResponse.GetSearchOccupancy();
            List<CommonLib.Response.HotelOccupancy> DistinctSearch = ParseCommonResponse.GetDISTINST_OCCUPANCY(ListSearch);
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            try
            {
                DateTime dtCheckIn = DateTime.ParseExact(Checkin, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                DateTime dtCheckOut = DateTime.ParseExact(Checkout, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-1);
                foreach (var Search in DistinctSearch)
                {
                    List<CommonLib.Response.RoomType> ListRoomType = new List<RoomType>();
                    Search.Rooms = new List<RoomType>();
                    var arrRate = new List<tbl_commonRoomRate>();
                    var sRate = new List<tbl_commonRoomRate>();
                    // ** Room Availbility
                    foreach (var objRoom in rooms)
                    {
                        using (var db = new DBHandlerDataContext())
                        {
                            arrRate = new List<tbl_commonRoomRate>();
                            List<CommonLib.Response.date> Dates = new List<date>();
                            List<CommonLib.Response.RoomType> OfferRATE = new List<CommonLib.Response.RoomType>();
                            List<CommonLib.Response.CancellationPolicy> CancellationPolicy = new List<CommonLib.Response.CancellationPolicy>();


                            if (objGlobalDefault.UserType == "Agent")
                            {

                                arrRate = (from obj in db.tbl_commonRoomRates where obj.SupplierId == SupplierID && obj.HotelID == HotelCode && obj.Status == "Active" && obj.RateType == "GN" && (obj.ValidNationality.Contains("AllCountry")) && obj.RoomId == objRoom.RoomTypeId && obj.CurrencyCode == objGlobalDefault.Currency && obj.SupplierId==SupplierID select obj).ToList();
                                if (arrRate.Count == 0)
                                {
                                    arrRate = (from obj in db.tbl_commonRoomRates where obj.SupplierId == SupplierID && obj.HotelID == HotelCode && obj.Status == "Active" && obj.RateType == "GN" && (obj.ValidNationality.Contains(Nationality + "^")) && obj.RoomId == objRoom.RoomTypeId && obj.CurrencyCode == objGlobalDefault.Currency && obj.SupplierId == SupplierID select obj).ToList();
                                }
                            }
                            else
                                arrRate = (from obj in db.tbl_commonRoomRates where obj.SupplierId == SupplierID && obj.HotelID == HotelCode && obj.Status == "Active" && obj.RateType == "GN" && (obj.ValidNationality.Contains(Nationality + "^")) && obj.RoomId == objRoom.RoomTypeId select obj).ToList();
                            foreach (var Rate in arrRate)
                            {
                                var calendar = new List<DateTime>();
                                DateTime RateCheckin = DateTime.ParseExact(Rate.Checkin, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                DateTime RateCheckOut = DateTime.ParseExact(Rate.Checkout, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                while (RateCheckin <= RateCheckOut)
                                {
                                    calendar.Add(RateCheckin);
                                    RateCheckin = RateCheckin.AddDays(1);
                                }

                                if (calendar.Contains(dtCheckIn) || calendar.Contains(dtCheckOut))
                                {
                                    List<CommonLib.Response.date> rateDates = new List<date>();
                                    // ** Occpancy Availbility
                                    if (ValidOccpancy(Convert.ToInt64(Rate.RoomId), Convert.ToInt64(Search.AdultCount), Convert.ToInt64(Search.ChildCount), Search.ChildAges) == true || SearchValid == "General")
                                    {
                                        sRate.Add(Rate);
                                        rateDates = GetCharges(Search, Rate, out CancellationPolicy, out OfferRATE, objRoom, SearchValid, Search);
                                        foreach (var obj in rateDates)
                                        {
                                            Dates.Add(obj);
                                        }
                                    }
                                }

                            }
                            if (Dates.Count == 0)
                                continue;
                            foreach (var sMealPlan in Dates.Select(d => d.MealPlan).Distinct().ToList())
                            {
                                string RoomTypeId = "";
                                List<date> sRateDate = Dates.Where(d => d.MealPlan == sMealPlan).Distinct().ToList()
                                                        .GroupBy(d => d.datetime).ToList().Select(g => g.First()).ToList(); ;
                                List<string> arrRateID = sRateDate.Select(r => r.RateTypeId.ToString()).Distinct().ToList();
                                for (int i = 0; i < arrRateID.Count; i++)
                                {
                                    RoomTypeId += arrRateID[i] + "_";
                                }
                                bool ValidDate = true;
                                if (objGlobalDefault.UserType == "Agent")
                                {
                                    List<DateTime> col = sRateDate.Select(d => DateTime.ParseExact(d.datetime, "dd-MM-yyyy", CultureInfo.InvariantCulture)).ToList();
                                    DateTime currentDate = dtCheckIn;
                                    var calendar = new List<DateTime>();
                                    var targetDate = dtCheckIn;
                                    while (targetDate <= dtCheckOut)
                                    {
                                        calendar.Add(targetDate);
                                        targetDate = targetDate.AddDays(1);
                                    }

                                    var missingDates = (from date in calendar
                                                        where !col.Contains(date)
                                                        select date).ToList();
                                    if (missingDates.Count != 0)
                                        ValidDate = false;
                                }
                                if (ValidDate)
                                {

                                    // List<string> CancellationID = ListCancellation(sRateDate);
                                    CancellationPolicy = GetCancellationPolicyByRate(sRateDate);
                                    List<TaxRate> otherRate = GetOtherRate(sRateDate);
                                    float Total = sRateDate.Select(d => d.Total).Sum() + otherRate.Select(d => d.TotalRate).Sum();
                                    ListRoomType.Add(new RoomType
                                    {
                                        Total = Total,
                                        OtherRates = otherRate,
                                        Dates = sRateDate,
                                        RoomTypeId = objRoom.RoomTypeId.ToString(),
                                        RoomDescriptionId = RoomTypeId.TrimEnd('_'),
                                        RoomTypeName = (from obj in db.tbl_commonRoomTypes where obj.RoomTypeID == objRoom.RoomTypeId select obj).FirstOrDefault().RoomType,
                                        CancellationPolicy = CancellationPolicy,
                                        RoomDescription = sMealPlan,
                                        ListCancel = CutAdmin.Common.RatesManager.CancellationPolicies(CancellationPolicy)
                                    });
                                }

                            }
                            foreach (var RATE in OfferRATE)
                            {
                                ListRoomType.Add(RATE);
                            }
                            Search.Rooms = ListRoomType.OrderBy(d => d.Total).ToList();
                            if (ListRoomType.Count != 0)
                            {
                                Search.MinPrice = ListRoomType.Select(d => d.Total).ToList().Min();
                                Search.MaxPrice = ListRoomType.Select(d => d.Total).ToList().Max();
                            }

                            //}
                        }
                        
                       
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return DistinctSearch;
        }

        public static List<string> ListCancellation(List<CommonLib.Response.date> Dates)
        {
            DBHandlerDataContext db = new DBHandlerDataContext();
            List<string> sCancellationId = new List<string>();
            try
            {
                foreach (var RateID in Dates.Select(d => d.RateTypeId).Distinct().ToList())
                {
                    var arrRate = (from obj in db.tbl_commonRoomRates where obj.HotelRateID == RateID select obj).FirstOrDefault();
                    if (arrRate != null)
                    {
                        foreach (var sCancellation in arrRate.CancellationPolicyId.Split(',').ToList())
                        {
                            sCancellationId.Add(sCancellation);
                        }
                    }
                }
            }
            catch
            {

            }
            return sCancellationId.Distinct().ToList();
        }
        #endregion

        #region ValidOccpancy
        public static bool ValidOccpancy(Int64 RoomID, Int64 AdultCount, Int64 ChildCount, string ChildAges)
        {
            bool valid = false; /* MaxOccpancy = RoomOccpancy + ChildWithoutBeds + ExtraBeds*/
            try
            {
                var sRooms = (from obj in db.tbl_commonRoomDetails where obj.RoomTypeId == RoomID select obj).FirstOrDefault();
                if (sRooms != null)
                {
                    if ((sRooms.RoomOccupancy + sRooms.NoOfChildWithoutBed + sRooms.MaxExtrabedAllowed) >= AdultCount + ChildCount)
                        valid = true;
                }

            }
            catch
            {

            }
            return valid;
        }

        public static date GetRateByOccupancy(date sDate, float EBRate, float CWBRate, float CNBRate, CommonLib.Response.HotelOccupancy Search, string HotelCode)
        {
            date sNewDateRate = new date();
            try
            {
                var sChildAges = (from obj in db.tbl_CommonHotelMasters where obj.sid == Convert.ToInt64(HotelCode) select new { obj.ChildAgeTo, obj.ChildAgeFrom }).FirstOrDefault();
                var sRates = (from obj in db.tbl_commonRoomDetails where obj.RoomTypeId == Convert.ToInt64(sDate.RoomTypeId) && obj.HotelId == Convert.ToInt64(HotelCode) select obj).FirstOrDefault();
                Int64 MaxEB = Convert.ToInt64(sRates.MaxExtrabedAllowed);
                Int64 MaxCNB = Convert.ToInt64(sRates.NoOfChildWithoutBed);
                Int64 RoomOccupancy = Convert.ToInt64(sRates.RoomOccupancy);
                Int64 MaxOccupancy = (Convert.ToInt64(sRates.RoomOccupancy) + Convert.ToInt64(MaxEB) + Convert.ToInt64(MaxCNB));
                /*Validate Child Ages*/
                foreach (var sAges in Search.ChildAges.Split(',').ToList())
                {
                    if (sAges == "")
                        continue;
                    if (sChildAges.ChildAgeFrom >= Convert.ToInt16(sAges))
                    {
                        Search.ChildCount = Search.ChildCount - 1;

                    }
                    else if (sChildAges.ChildAgeTo <= Convert.ToInt16(sAges))
                    {
                        Search.AdultCount = Search.AdultCount + 1;
                        Search.ChildCount = Search.ChildCount - 1;
                    }
                }

                if (MaxOccupancy >= Search.AdultCount + Search.ChildCount)
                {
                    if (Convert.ToInt64(sRates.RoomOccupancy) + Convert.ToInt64(sRates.MaxExtrabedAllowed) < Search.AdultCount)
                        throw new Exception("Maximum adults exceed");
                    Int64 noAdultEB = 0;
                    RoomOccupancy = RoomOccupancy - Search.AdultCount;
                    if (RoomOccupancy < 0)
                    {
                        if (RoomOccupancy + Convert.ToInt64(sRates.MaxExtrabedAllowed) < 0)
                            throw new Exception("Room Ocuupancy No Valid");
                        noAdultEB = (-RoomOccupancy);
                        RoomOccupancy = RoomOccupancy + Convert.ToInt64(sRates.MaxExtrabedAllowed);
                        if (RoomOccupancy != 0)
                            throw new Exception("No Extrabeds exceed");
                        sDate.EBRate = noAdultEB * Convert.ToSingle(EBRate);
                        sDate.noEXB = noAdultEB;

                    }
                    if (Search.ChildCount != 0)
                    {
                        Int64 CWB = 0, CNB = 0;
                        if (noAdultEB == 0)
                        {
                            if (Convert.ToInt64(sRates.MaxExtrabedAllowed) == 1)
                            {
                                CWB = Search.ChildCount + (Convert.ToInt64(sRates.MaxExtrabedAllowed) - Search.ChildCount);
                                // CWB =  (objRate.MaxEB - ChildCount);
                                sDate.ChidWBedRate = CWB * Convert.ToSingle(CWBRate);
                                sDate.noCWB = CWB;
                            }
                            else if (Convert.ToInt64(sRates.MaxExtrabedAllowed) > 1 && Search.ChildCount == 1)
                            {
                                CWB = (Convert.ToInt64(sRates.MaxExtrabedAllowed) - Search.ChildCount);
                                sDate.ChidWBedRate = CWB * Convert.ToSingle(CWBRate);
                                sDate.noCWB = CWB;
                            }
                            else
                            {
                                CWB = Search.ChildCount + (MaxEB - Search.ChildCount);
                                sDate.ChidWBedRate = CWB * Convert.ToSingle(CWBRate);
                                sDate.noCWB = CWB;
                            }
                            if ((MaxEB - Search.ChildCount) < 0)
                            {
                                CNB = (-(MaxEB - Search.ChildCount));
                                sDate.CNBRate = CNB * Convert.ToSingle(CNBRate);
                                sDate.noCNB = CNB;
                            }
                        }
                        else
                        {
                            RoomOccupancy = RoomOccupancy - Search.ChildCount;
                            if (RoomOccupancy < 0)
                            {
                                CNB = -(RoomOccupancy);
                                sDate.CNBRate = CNB * Convert.ToSingle(CNBRate);
                                sDate.noCNB = CNB;
                                sDate.ChidWBedRate = 0 * Convert.ToSingle(CWBRate);
                                sDate.noCWB = 0;

                            }
                        }
                    }

                    sDate.Total = (sDate.RoomRate + sDate.EBRate + sDate.CNBRate + sDate.ChidWBedRate);
                    sNewDateRate = sDate;

                }
                else
                {
                    throw new Exception("Room Ocuupancy No Valid");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return sNewDateRate;
        }
        #endregion

        #region GetCharges
        public static List<TaxRate> GetOtherRate(List<CommonLib.Response.date> Dates)
        {
            List<TaxRate> ListRates = new List<TaxRate>();
            try
            {
                foreach (var objDate in Dates)
                {
                    foreach (var objRate in objDate.ListRates)
                    {
                        if (ListRates.Where(d => d.RateName == objRate.RateName).ToList().Count == 0)
                        {
                            ListRates.Add(new TaxRate { BaseRate = objRate.BaseRate, Per = objRate.Per, RateName = objRate.RateName, TaxOn = objRate.TaxOn, TotalRate = objRate.TotalRate, Type = objRate.Type });
                        }
                        else
                        {
                            ListRates.Where(d => d.RateName != objRate.RateName).FirstOrDefault().BaseRate += objRate.BaseRate;
                            ListRates.Where(d => d.RateName != objRate.RateName).FirstOrDefault().TotalRate += objRate.TotalRate;

                        }
                    }
                }
            }
            catch
            {

            }
            return ListRates;
        }


        public static List<CommonLib.Response.date> GetCharges(CommonLib.Response.HotelOccupancy RoomSearch, tbl_commonRoomRate Rate, out List<CommonLib.Response.CancellationPolicy> ListCancellation, out List<CommonLib.Response.RoomType> ListOfferRate, tbl_commonRoomDetail room, string SearchValid, CommonLib.Response.HotelOccupancy objSerach)
        {

            List<CommonLib.Response.date> ListOfDay = new List<date>();
            ListCancellation = new List<CancellationPolicy>();
            ListOfferRate = new List<CommonLib.Response.RoomType>();
            List<string> CancellationID = new List<string>();
            List<string> OfferID = new List<string>();
            float TotalRate = 0;
            DateTime sCheckIn, sCheckOut;
           DateTime  dtCheckIn = DateTime.ParseExact(CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
           DateTime dtCheckOut = DateTime.ParseExact(CheckOutDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-1); 
            try
            {
                var arrSpecialRate = (from obj in db.tbl_commonRoomRates where obj.HotelID == Rate.HotelID && obj.RateType == "SL" && obj.RoomId == Rate.RoomId && obj.MealPlan == Rate.MealPlan && obj.ValidNationality == Rate.ValidNationality && obj.Status == "Active" select obj).ToList();

                var sSpecialRate = new List<RatePrice>();
                foreach (var sRate in arrSpecialRate)
                {
                    //if ((DateTime.ParseExact(sRate.Checkin, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture) >= ))
                    //|| (DateTime.ParseExact(sRate.Checkout, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture) <= (DateTime.ParseExact(CheckOutDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture)))
                    //|| (DateTime.ParseExact(Rate.Checkin, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture) <= (DateTime.ParseExact(CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture)))
                    //   || (DateTime.ParseExact(Rate.Checkout, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture) >= (DateTime.ParseExact(CheckOutDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture))))
                    var calendar = new List<DateTime>();
                    DateTime RateCheckin = DateTime.ParseExact(sRate.Checkin, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    DateTime RateCheckOut = DateTime.ParseExact(sRate.Checkout, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    while (RateCheckin <= RateCheckOut)
                    {
                        calendar.Add(RateCheckin.AddDays(1));
                        RateCheckin = RateCheckin.AddDays(1);
                    }
                    if (calendar.Contains(dtCheckIn) || calendar.Contains(dtCheckOut))
                    {

                        if (ValidOccpancy(Convert.ToInt64(sRate.RoomId), Convert.ToInt64(RoomSearch.AdultCount), Convert.ToInt64(RoomSearch.ChildCount), RoomSearch.ChildAges) == true || SearchValid == "General")
                        {
                            sCheckIn = DateTime.ParseExact(sRate.Checkin, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            sCheckOut = DateTime.ParseExact(sRate.Checkout, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            double noDays = (sCheckOut - sCheckIn).TotalDays;
                            List<DateTime> ListDates = new List<DateTime>();
                            for (int i = 0; i <= noDays; i++)
                            {
                                ListDates.Add(sCheckIn.AddDays(i));

                            }
                            sSpecialRate.Add(new RatePrice
                            {
                                Currency = Rate.CurrencyCode,
                                RateType ="SP",
                                Dates = ListDates,
                                Daywise = Rate.DayWise,
                                MonRate = Convert.ToDecimal(sRate.MonRR),
                                TueRate = Convert.ToDecimal(sRate.TueRR),
                                WedRate = Convert.ToDecimal(sRate.WedRR),
                                ThuRate = Convert.ToDecimal(sRate.ThuRR),
                                FriRate = Convert.ToDecimal(sRate.FriRR),
                                SatRate = Convert.ToDecimal(sRate.SatRR),
                                SunRate = Convert.ToDecimal(sRate.SunRR),
                                RRRate = Convert.ToDecimal(sRate.RR),
                                CNBRate = Convert.ToDecimal(sRate.CNB),
                                CWBRate = Convert.ToDecimal(sRate.CWB),
                                EBRate = Convert.ToDecimal(sRate.EB),
                                CancelationPolicy = sRate.CancellationPolicyId,
                                OfferID = sRate.OfferId,
                                RateID = Convert.ToString(sRate.HotelRateID),
                            });

                        }
                    }

                }



                var sGeneralRate = new List<RatePrice>();

                sCheckIn = DateTime.ParseExact(Rate.Checkin, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                sCheckOut = DateTime.ParseExact(Rate.Checkout, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                double noGDays = (sCheckOut - sCheckIn).TotalDays;
                List<DateTime> ListGDates = new List<DateTime>();
                for (int i = 0; i <= noGDays; i++)
                {
                    ListGDates.Add(sCheckIn.AddDays(i));

                }
                sGeneralRate.Add(new RatePrice
                {
                    Currency = Rate.CurrencyCode,
                    RateType = "GN",
                    Dates = ListGDates,
                    Daywise = Rate.DayWise,
                    MonRate = Convert.ToDecimal(Rate.MonRR),
                    TueRate = Convert.ToDecimal(Rate.TueRR),
                    WedRate = Convert.ToDecimal(Rate.WedRR),
                    ThuRate = Convert.ToDecimal(Rate.ThuRR),
                    FriRate = Convert.ToDecimal(Rate.FriRR),
                    SatRate = Convert.ToDecimal(Rate.SatRR),
                    SunRate = Convert.ToDecimal(Rate.SunRR),
                    RRRate = Convert.ToDecimal(Rate.RR),
                    CNBRate = Convert.ToDecimal(Rate.CNB),
                    CWBRate = Convert.ToDecimal(Rate.CWB),
                    EBRate = Convert.ToDecimal(Rate.EB),
                    CancelationPolicy = Rate.CancellationPolicyId,
                    OfferID = Rate.OfferId,
                    RateID = Convert.ToString(Rate.HotelRateID),

                });


                sCheckIn = DateTime.ParseExact(CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                sCheckOut = DateTime.ParseExact(CheckOutDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-1);
                double noCDays = (sCheckOut - sCheckIn).TotalDays;
                for (int i = 0; i <= noCDays; i++)
                {

                    var sData = (from obj in sSpecialRate where obj.Dates.Contains(sCheckIn.AddDays(i)) select obj).FirstOrDefault();
                    if (sData == null)
                    {
                        sData = (from obj in sGeneralRate where obj.Dates.Contains(sCheckIn.AddDays(i)) select obj).FirstOrDefault();
                    }
                    decimal RRate = 0;
                    decimal CWBRate = 0;
                    decimal EBRate = 0;
                    decimal CNBRate = 0;
                    decimal RateID = 0;
                    if (sData != null)
                    {
                        if (sData.Daywise == "Yes")
                        {
                            if (sCheckIn.AddDays(i).DayOfWeek == DayOfWeek.Sunday)
                                RRate = sData.SunRate;
                            else if (sCheckIn.AddDays(i).DayOfWeek == DayOfWeek.Monday)
                                RRate = sData.MonRate;
                            else if (sCheckIn.AddDays(i).DayOfWeek == DayOfWeek.Tuesday)
                                RRate = sData.TueRate;
                            else if (sCheckIn.AddDays(i).DayOfWeek == DayOfWeek.Wednesday)
                                RRate = sData.WedRate;
                            else if (sCheckIn.AddDays(i).DayOfWeek == DayOfWeek.Thursday)
                                RRate = sData.ThuRate;
                            else if (sCheckIn.AddDays(i).DayOfWeek == DayOfWeek.Friday)
                                RRate = sData.FriRate;
                            else if (sCheckIn.AddDays(i).DayOfWeek == DayOfWeek.Saturday)
                                RRate = sData.SatRate;

                        }
                        else
                        {
                            RRate = sData.RRRate;
                        }
                        CWBRate = sData.CWBRate;
                        EBRate = sData.EBRate;
                        CNBRate = sData.CNBRate;
                        RateID = Convert.ToDecimal(sData.RateID);
                        List<CommonLib.Response.InventoryType> Inventory = new List<InventoryType>();
                        //List<CommonLib.Response.InventoryType> Inventory = GetInventory(Convert.ToInt64(RateID), Convert.ToInt64(Rate.HotelID), Convert.ToInt64(Rate.SupplierId), sCheckIn.AddDays(i).ToString("dd-MM-yyyy"));
                        if (Inventory == null)
                        {

                        }
                        GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                        Int64 AvailBility = 0;
                        if (objGlobalDefault.UserType != "Agent")
                            AvailBility = InventoryManager.GetInventoryCount(Rate.HotelID.ToString(), RateID, Rate.RoomId.ToString(), sCheckIn.AddDays(i).ToString("dd-MM-yyyy"),AccountManager.GetSupplierByUser());

                        date Chk_Date = new date
                            {
                                Type = Rate.Type,
                                MealPlan = Rate.MealPlan,
                                Currency = sData.Currency,
                                datetime = sCheckIn.AddDays(i).ToString("dd-MM-yyyy"),
                                NoOfCount = AvailBility,
                                day = sCheckIn.AddDays(i).DayOfWeek.ToString(),
                                RoomRate = Convert.ToSingle(RRate),
                                ChidWBedRate = Convert.ToSingle(0),
                                EBRate = Convert.ToSingle(0),
                                CNBRate = Convert.ToSingle(0),
                                RoomTypeId = Convert.ToInt32(Rate.RoomId),
                                RateTypeId = Convert.ToInt32(RateID),
                                //  NoOfInventory = GetInventory(Convert.ToInt64(RateID), Convert.ToInt64(Rate.HotelID), Convert.ToInt64(Rate.SupplierId), sCheckIn.AddDays(i).ToString("dd-MM-yyyy")),
                                // Total = Convert.ToSingle(RRate) + Convert.ToSingle(ChildRate * RoomSearch.ChildCount)
                                NoOfInventory = Inventory,
                                Total = Convert.ToSingle(RRate),
                                ListRates = new List<TaxRate>(),
                                offerID = sData.OfferID.Split(',').Distinct().ToList(),
                            };
                        try
                        {
                            if (SearchValid == "")
                                ListOfDay.Add(GetRateByOccupancy(Chk_Date, Convert.ToSingle(EBRate), Convert.ToSingle(CWBRate), Convert.ToSingle(CNBRate), objSerach, Rate.HotelID.ToString()));
                            else
                                ListOfDay.Add(Chk_Date);

                        }
                        catch
                        {

                        }
                        ListOfDay.LastOrDefault().Equals(GetTaxRate(ListOfDay.Last(), Convert.ToInt64( Rate.HotelID)));
                        CancellationID = sData.CancelationPolicy.Split(',').Distinct().ToList();
                        OfferID = sData.OfferID.Split(',').Distinct().ToList();
                    }

                }
                float TotalCharge = ListOfDay.Select(d => d.Total).Sum();
                int night = ListOfDay.Count;
                //if (ListOfDay.Count != 0)
                //    ListCancellation = GetCancellationPolicy(CancellationID, TotalCharge, ListOfDay.Count);
                ListOfferRate = GetOffer(OfferID, ListOfDay, Rate, room);
            }
            catch
            {

            }

            return ListOfDay;
        }

        public static ChargeRate GetTotalChargeTax(ListDates objDateCharge)
        {
            ChargeRate objChargeRate = new ChargeRate();
            objChargeRate.Charge = new List<TaxRate>();
            if (objDateCharge.CNBRate.BaseRate != 0)
            {
                objChargeRate.TotalRate += objDateCharge.CNBRate.TotalRate;
                foreach (var objCharge in objDateCharge.CNBRate.Charge)
                {
                    if (!objChargeRate.Charge.Exists(d => d.RateName == objCharge.RateName))
                    {
                        objChargeRate.Charge.Add(new TaxRate
                        {
                            BaseRate = objCharge.BaseRate,
                            RateName = objCharge.RateName,
                            TaxOn = objCharge.TaxOn,
                            TotalRate = objCharge.TotalRate

                        });
                    }
                }
            }
            if (objDateCharge.CWBRate.BaseRate != 0)
            {
                foreach (var objCharge in objDateCharge.CWBRate.Charge)
                {
                    if (!objChargeRate.Charge.Exists(d => d.RateName == objCharge.RateName))
                    {
                        objChargeRate.Charge.Add(new TaxRate
                        {
                            BaseRate = objCharge.BaseRate,
                            RateName = objCharge.RateName,
                            TaxOn = objCharge.TaxOn,
                            TotalRate = objCharge.TotalRate

                        });
                    }
                }
            }
            if (objDateCharge.EBRate.BaseRate != 0)
            {

            }
            return objChargeRate;
        }
        #endregion

        #region Get Inventory Details
        public static List<date> GetAvailibility(string Serach, string RoomID, string RoomDescID, int RoomNo)
        {
            List<date> ListDates = new List<date>();
            try
            {
                string HotelID = "";
                HotelOccupancy objOccupancy = GenralManager.GetRateInfo(Serach, out HotelID).RoomOccupancy.Where(d => d.RoomNo.Contains(RoomNo)).FirstOrDefault();
                RoomType objRoom = objOccupancy.Rooms.Where(d => d.RoomTypeId == RoomID && d.RoomDescription == RoomDescID).FirstOrDefault();
                foreach (var objDate in objRoom.Dates)
                {
                    objDate.NoOfCount = InventoryManager.GetInventoryCount(HotelID, objDate.RateTypeId, RoomID, objDate.datetime,AccountManager.GetSupplierByUser());
                    ListDates.Add(objDate);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return ListDates;
        }

        public static List<date> GetAvailibility(string Serach, string RoomID, string RoomDescID, int RoomNo,string HotelCode,string Supplier)
        {
            List<date> ListDates = new List<date>();
            try
            {
                HotelOccupancy objOccupancy = GenralManager.GetRateInfo(Serach, Supplier, HotelCode).RoomOccupancy.Where(d => d.RoomNo.Contains(RoomNo)).FirstOrDefault();
                RoomType objRoom = objOccupancy.Rooms.Where(d => d.RoomTypeId == RoomID && d.RoomDescription == RoomDescID).FirstOrDefault();
                foreach (var objDate in objRoom.Dates)
                {
                    objDate.NoOfCount = InventoryManager.GetInventoryCount(HotelCode, objDate.RateTypeId, RoomID, objDate.datetime, Convert.ToInt64(Supplier));
                    ListDates.Add(objDate);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return ListDates;
        }
        #endregion

        #region Get Room Image
        public static List<Image> GetRoomImage(Int64 RoomTypeID, Int64 HotelId)
        {
            List<Image> arrImages = new List<Image>();
            try
            {
                var arrRoomImage = (from obj in db.tbl_commonRoomDetails
                                    where obj.HotelId == HotelId &&
                                    obj.RoomTypeId == RoomTypeID
                                    select new
                                    {
                                        obj.RoomImage,
                                        obj.SubImages
                                    }).FirstOrDefault();
                string URL = System.Configuration.ConfigurationManager.AppSettings["URL"];
                arrImages.Add(new Image
                {
                    Count = 1,
                    IsDefault = true,
                    Title = arrRoomImage.RoomImage,
                    Url = URL + "/RoomImages/" + arrRoomImage.RoomImage
                });
                foreach (var Image in arrRoomImage.SubImages.Split('^').ToList())
                {
                    if (Image == "")
                        continue;
                    arrImages.Add(new Image
                    {
                        Count = 1,
                        IsDefault = true,
                        Title = Image,
                        Url = URL + "/RoomImages/" + Image
                    });
                }

            }
            catch
            {

            }
            return arrImages;
        }
        #endregion

        #region Create  Cancellation
        public static List<CommonLib.Response.CancellationPolicy> GetCancellationPolicyByRate(List<date> arrDates)
        {
            List<CancellationPolicy> Cancellation = new List<CancellationPolicy>();

            try
            {
                foreach (var RateID in arrDates.Select(d => d.RateTypeId).Distinct().ToList())
                {
                    List<string> CancellationID = new List<string>();
                    float TotalCancellation = 0;
                    DateTime CancelDate = DateTime.Now;
                    bool nonRefundable = false;
                    var arrRate = (from obj in db.tbl_commonRoomRates where obj.HotelRateID == RateID select obj).FirstOrDefault();
                    if (arrRate != null)
                    {
                        foreach (var sCancellation in arrRate.CancellationPolicyId.Split(',').ToList())
                        {
                            if (sCancellation == "")
                                CancellationID.Add("1");
                            else
                             CancellationID.Add(sCancellation);
                        }
                    }
                    List<date> arrChargeDate = arrDates.Where(d => d.RateTypeId == RateID).ToList();
                    float Markups = arrDates.Where(d => d.RateTypeId == RateID).ToList().Select(r => r.AdminMarkup).ToList().Sum() +
                                          arrDates.Where(d => d.RateTypeId == RateID).ToList().Select(r => r.Markup).ToList().Sum();
                    float TotalCharge = arrDates.Where(d => d.RateTypeId == RateID).ToList().Select(r => r.Total).ToList().Sum() - Markups;
                    int noNights = arrDates.Where(d => d.RateTypeId == RateID).ToList().Count;
                    foreach (var ID in CancellationID)
                    {
                        if (ID == "")
                        {
                            nonRefundable = true;
                            CancelDate = DateTime.Now;
                            TotalCancellation = TotalCharge;
                        }
                        var sCancellation = (from obj in db.tbl_CommonCancelationPolicies where obj.CancelationID == Convert.ToInt64(ID) select obj).FirstOrDefault();

                        if (sCancellation.RefundType == "Refundable")
                        {

                            if (sCancellation.PolicyType == "DaysPrior" && (DateTime.ParseExact(CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-Convert.ToInt16(sCancellation.DaysPrior)) >= DateTime.Now))
                            {
                                CancelDate = DateTime.ParseExact(CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-Convert.ToInt16(sCancellation.DaysPrior));
                                TotalCancellation = GetCancellation(sCancellation, TotalCharge);
                            }
                            else if (sCancellation.PolicyType == "DaysPrior" && (DateTime.ParseExact(CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddMinutes(-Convert.ToInt16(1)) <= DateTime.Now))
                            {
                                CancelDate = DateTime.ParseExact(CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                TotalCancellation = TotalCharge;
                                nonRefundable = true;
                            }
                            else if (sCancellation.PolicyType == "Date" && (DateTime.ParseExact(sCancellation.Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture) >= DateTime.Now))
                            {
                                CancelDate = DateTime.ParseExact(sCancellation.Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                TotalCancellation = GetCancellation(sCancellation, TotalCharge);
                            }

                        }
                        else if (sCancellation.RefundType == "NoShow")
                        {
                            if (sCancellation.PolicyType == "DaysPrior" && (DateTime.ParseExact(CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-Convert.ToInt16(sCancellation.DaysPrior)) >= DateTime.Now))
                            {
                                CancelDate = DateTime.ParseExact(CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-Convert.ToInt16(sCancellation.DaysPrior));
                                TotalCancellation = GetCancellation(sCancellation, TotalCharge);
                            }
                            else if (sCancellation.PolicyType == "Date" && (DateTime.ParseExact(sCancellation.Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture) >= DateTime.Now))
                            {
                                CancelDate = DateTime.ParseExact(sCancellation.Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                TotalCancellation = GetCancellation(sCancellation, TotalCharge);
                            }
                        }
                        else
                        {
                            nonRefundable = true;
                            CancelDate = DateTime.Now;
                            TotalCancellation = TotalCharge;

                        }

                        Cancellation.Add(new CancellationPolicy
                        {
                            AmendRestricted = nonRefundable,
                            CancellationAmount = TotalCancellation,
                            CancellationDateTime = CancelDate.AddHours(-noDaysGap).AddSeconds(-1).ToString("dd/MM/yyy hh:mm", CultureInfo.CurrentCulture),
                            CancelRestricted = nonRefundable,
                            CUTCancellationAmount = TotalCancellation,
                            nonRefundable = nonRefundable,
                            arrChargeDate = arrChargeDate
                        });
                    }

                }

            }
            catch
            {

            }
            float SupplierMarkup = 0, AgentMarkup = 0, S2SMarkup=0;
            List<CancellationPolicy> arrCancellations = new List<CancellationPolicy>();
            arrCancellations.Add(Cancellation[0]);
            arrCancellations.Last().objCharges = new ChargeRate();
            arrCancellations.Last().objCharges = GetRateByCancellation(arrCancellations.Last().CancellationAmount, out SupplierMarkup, out AgentMarkup, out S2SMarkup);
            arrCancellations.Last().SupplierMarkup = SupplierMarkup;
            arrCancellations.Last().AgentCancellationMarkup = AgentMarkup;
            arrCancellations.Last().S2SMarkup = S2SMarkup;
            for (int i = 1; i < Cancellation.Count; i++)
            {
                SupplierMarkup = 0; AgentMarkup = 0; S2SMarkup = 0;
                arrCancellations.Add(Cancellation[i]);
                arrCancellations.Last().CancellationAmount = arrCancellations.Last().CancellationAmount + Cancellation[i - 1].CancellationAmount;
                arrCancellations.Last().CUTCancellationAmount = arrCancellations.Last().CUTCancellationAmount + Cancellation[i - 1].CUTCancellationAmount;
                arrCancellations.Last().CutRoomAmount = arrCancellations.Last().CutRoomAmount + Cancellation[i - 1].CUTCancellationAmount;
                arrCancellations.Last().objCharges = new ChargeRate();
                arrCancellations.Last().objCharges = GetRateByCancellation(arrCancellations.Last().CancellationAmount, out SupplierMarkup, out AgentMarkup, out S2SMarkup);
                arrCancellations.Last().SupplierMarkup = SupplierMarkup;
                arrCancellations.Last().AgentCancellationMarkup = AgentMarkup;
                arrCancellations.Last().S2SMarkup = S2SMarkup;
            }

            return arrCancellations;
        }

        public static DateTime  OnHoldDate(List<RoomType> ListRoom)
        {
            DateTime CompareDate = new DateTime();
            List<DateTime> onHoldDate = new List<DateTime>();
             List<bool> nonRefund = new List<bool>();
            try 
	        {	      
                foreach (var objRoom in ListRoom)
	            {
                    nonRefund.Add(objRoom.CancellationPolicy.Any(d => d.CancelRestricted == true || d.AmendRestricted == true && d != null));
                     int l = 0;
                     foreach (CommonLib.Response.CancellationPolicy objCancellation in objRoom.CancellationPolicy)
                     {
                         if (objCancellation.CancelRestricted == false && DateTime.ParseExact(objCancellation.CancellationDateTime,"dd-MM-yyyy hh:mm", CultureInfo.InvariantCulture).AddDays(-2) > DateTime.Now)
                         {
                             onHoldDate.Add(DateTime.Now.AddHours(-48));
                         }
                         nonRefund.Add(objCancellation.CancelRestricted);
                     }
	            }
            bool  nonRefundable = nonRefund.Any(d => d == true);
            if (onHoldDate.Count != 0 && nonRefundable == false)
            {
                DateTime date = Convert.ToDateTime(Convert.ToDateTime(onHoldDate.Min()).ToShortDateString());
                CompareDate = new DateTime(date.Year, date.Month, date.Day, 00, 00, 00).AddMinutes(-1).AddSeconds(-1);
                return CompareDate;
            }
               
            else
                return CompareDate;
		         
	        }
	        catch (Exception)
	        {
		
		          return CompareDate;
	        }
         
        }
        #endregion

        #region Utilities



        public static List<CommonLib.Response.CancellationPolicy> GetCancellationPolicy(List<string> CancellationID, float TotalCharge, int noNights)
        {
            List<CancellationPolicy> Cancellation = new List<CancellationPolicy>();
            try
            {
                noNights = noNights - 1;
                float TotalCancellation = 0;
                DateTime CancelDate = DateTime.Now;
                bool nonRefundable = false;
                foreach (var ID in CancellationID)
                {
                    if (ID == "")
                    {
                        nonRefundable = true;
                        CancelDate = DateTime.Now;
                        TotalCancellation = TotalCharge;
                    }
                    var sCancellation = (from obj in db.tbl_CommonCancelationPolicies where obj.CancelationID == Convert.ToInt64(ID) select obj).FirstOrDefault();

                    if (sCancellation.RefundType == "Refundable")
                    {

                        if (sCancellation.PolicyType == "DaysPrior" && (DateTime.ParseExact(CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-Convert.ToInt16(sCancellation.DaysPrior)) >= DateTime.Now))
                        {
                            CancelDate = DateTime.ParseExact(CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-Convert.ToInt16(sCancellation.DaysPrior));
                            TotalCancellation = GetCancellation(sCancellation, TotalCharge);
                        }
                        else if (sCancellation.PolicyType == "DaysPrior" && (DateTime.ParseExact(CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddMinutes(-Convert.ToInt16(1)) <= DateTime.Now))
                        {
                            CancelDate = DateTime.ParseExact(CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            TotalCancellation = TotalCharge;
                            nonRefundable = true;
                        }
                        else if (sCancellation.PolicyType == "Date" && (DateTime.ParseExact(sCancellation.Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture) >= DateTime.Now))
                        {
                            CancelDate = DateTime.ParseExact(sCancellation.Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            TotalCancellation = GetCancellation(sCancellation, TotalCharge);
                        }

                    }
                    else if (sCancellation.RefundType == "NoShow")
                    {
                        if (sCancellation.PolicyType == "DaysPrior" && (DateTime.ParseExact(CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-Convert.ToInt16(sCancellation.DaysPrior)) >= DateTime.Now))
                        {
                            CancelDate = DateTime.ParseExact(CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-Convert.ToInt16(sCancellation.DaysPrior));
                            TotalCancellation = GetCancellation(sCancellation, TotalCharge);
                        }
                        else if (sCancellation.PolicyType == "Date" && (DateTime.ParseExact(sCancellation.Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture) >= DateTime.Now))
                        {
                            CancelDate = DateTime.ParseExact(sCancellation.Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            TotalCancellation = GetCancellation(sCancellation, TotalCharge);
                        }
                    }
                    else
                    {
                        nonRefundable = true;
                        CancelDate = DateTime.Now;
                        TotalCancellation = TotalCharge * noNights;
                    }

                    Cancellation.Add(new CancellationPolicy
                    {
                        AmendRestricted = nonRefundable,
                        CancellationAmount = TotalCancellation,
                        //CancellationDateTime = CancelDate.ToShortDateString(),
                        CancellationDateTime = CancelDate.AddSeconds(-1).ToString("dd/MM/yyy hh:mm", CultureInfo.CurrentCulture),
                        CancelRestricted = nonRefundable,
                        CUTCancellationAmount = TotalCancellation,
                        nonRefundable = nonRefundable,
                    });
                }
            }
            catch
            {

            }

            return Cancellation;
        }

        public static float GetCancellation(tbl_CommonCancelationPolicy Cancellation, float Total)
        {
            float CancellationTotal = 0;
            if (Cancellation.ChargesType == "Amount")
            {
                CancellationTotal = Convert.ToSingle(Cancellation.AmountToCharge);
            }
            else if (Cancellation.ChargesType == "Nights")
            {
                CancellationTotal = (Total / Convert.ToSingle(Cancellation.NightsToCharge));
            }
            else if (Cancellation.ChargesType == "Percentile")
            {
                CancellationTotal = (Total * Convert.ToSingle(Cancellation.PercentageToCharge) / 100);
            }
            return CancellationTotal;
        }


        public static List<string> CancellationPolicies(List<CommonLib.Response.CancellationPolicy> ListCancellation)
        {
            List<string> Cancellation = new List<string>();

            foreach (CommonLib.Response.CancellationPolicy objCancellation in ListCancellation)
            {
                string CurrencyClass = "";
                switch (objCancellation.arrChargeDate[0].Currency)
                {
                    case "AED":
                        CurrencyClass = "Currency-AED";

                        break;
                    case "SAR":
                        CurrencyClass = "Currency-SAR";
                        break;
                    case "EUR":
                        CurrencyClass = "fa fa-eur";
                        break;
                    case "GBP":
                        CurrencyClass = "fa fa-gbp";
                        break;
                    case "USD":
                        CurrencyClass = "fa fa-dollar";
                        break;
                    case "INR":
                        CurrencyClass = "fa fa-inr";
                        break;
                }
                DateTime date = Convert.ToDateTime(Convert.ToDateTime(objCancellation.CancellationDateTime).ToString("dd/MM/yyy hh:mm", CultureInfo.CurrentCulture));
                // Convert.ToDateTime(objCancellation.dayMax);                
                DateTime time = Convert.ToDateTime(objCancellation.CancellationDateTime);
                /// time = time.Add(Cancelstart);              
                DateTime dtCancel = new DateTime(date.Year, date.Month, date.Day, 00, 00, 00).AddSeconds(-1);
                //if (objCancellation != null && objCancellation.nonRefundable == false)
               //{
                    //if (objCancellation.AmendRestricted == true || objCancellation.CancelRestricted == true)
                    //{
                    //    Cancellation.Add("In the event of cancellation after " + DateTime.Now.ToString("dd/MM/yyy hh:mm", CultureInfo.CurrentCulture) + " no amendment/cancellation refund will be applicable after booking .");
                    //    break;
                    //}
                   // else
                    //{
                        string CancelAmt = Convert.ToSingle(Math.Round((decimal)objCancellation.objCharges.TotalRate, 2, MidpointRounding.AwayFromZero)).ToString("N");
                        Cancellation.Add("<i class=" + CurrencyClass + "> </i> " + CancelAmt + " charge will apply if cancelled after " + date.AddSeconds(-1).ToString("dd/MM/yyy hh:mm", CultureInfo.CurrentCulture) + "PM");
                    //}
                //}//
               // else
               // {
                   // Cancellation.Add("This is Non-Refundable  booking, there will be no cancellation or amendment possible once booked");
               // }
            }


            return Cancellation;


        }

        public static List<CommonLib.Response.RoomType> GetOffer(List<string> OfferID, List<date> ListNight, tbl_commonRoomRate ListRate, tbl_commonRoomDetail room)
        {
            //DateTime sCheckIn, sCheckOut, sOfferValidFrom, sOfferValidTo;

            List<CommonLib.Response.RoomType> ListRoomType = new List<CommonLib.Response.RoomType>();
            List<CommonLib.Response.date> DATES = new List<CommonLib.Response.date>();
            List<CommonLib.Response.date> ListOfDay = new List<date>();
            List<CommonLib.Response.CancellationPolicy> CancellationPolicy = new List<CommonLib.Response.CancellationPolicy>();

            var sOffer = new List<RoomType>();
            try
            {
                foreach (var IDs in OfferID)
                {
                    if (IDs != "")
                    {
                        List<OfferSeason> OfferRate = GetValidOffer(IDs);
                        foreach (var objDate in ListNight)
                        {
                            if (OfferRate.Select(obj => obj.Dates).ToList().Contains(DateTime.ParseExact(objDate.datetime, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture)))
                            {
                                OfferSeason Season = OfferRate.Where(d => d.Dates == DateTime.ParseExact(objDate.datetime, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture)).FirstOrDefault();
                                objDate.OfferRate = GetOfferRate(Season, objDate.Total);
                            }
                        }
                        if (OfferRate.Count != 0)
                        {
                            float TotalCharge = ListOfDay.Select(d => d.Total).Sum();
                            List<string> CancellationID = ListRate.CancellationPolicyId.Split(',').Distinct().ToList();
                            if (ListOfDay.Count != 0)
                                CancellationPolicy = GetCancellationPolicy(CancellationID, TotalCharge, ListOfDay.Count);
                            ListRoomType.Add(new RoomType
                            {
                                Total = DATES.Select(d => d.Total).Sum(),
                                Dates = DATES,
                                RoomTypeId = (from obj in db.tbl_commonRoomDetails where obj.RoomId == ListRate.RoomId select obj).FirstOrDefault().RoomTypeId.ToString(),
                                RoomTypeName = (from obj in db.tbl_commonRoomTypes where obj.RoomTypeID == ListRate.RoomId select obj).FirstOrDefault().RoomType.ToString(),
                                CancellationPolicy = CancellationPolicy,
                                RoomDescription = ListRate.MealPlan,
                            });
                        }
                    }
                }
            }

            catch
            {
            }

            return ListRoomType;
        }

        public static List<OfferSeason> GetValidOffer(string OfferID)
        {
            List<OfferSeason> ListSeason = new List<OfferSeason>();
            List<OfferSeason> OfferSDate = new List<OfferSeason>();
            List<DateTime> OfferBDate = new List<DateTime>();
            if (OfferID != "")
            {
                var sOfferRate = (from obj in db.tbl_CommonHotelOffers where obj.OfferID == Convert.ToInt64(OfferID) select obj).ToList();

                foreach (var OfferValid in sOfferRate)
                {

                    if (DateTime.ParseExact(OfferValid.ValidFrom, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture) >= (DateTime.ParseExact(CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture)))
                    {
                        DateTime ValidTill = DateTime.Now;


                        // var offerDate = DateTime.ParseExact(CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-Convert.ToInt16(OfferValid.DaysPrior));
                        if (OfferValid.DateType == "Season Date")
                        {
                            DateTime ValidFrom = DateTime.ParseExact(OfferValid.ValidFrom, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            DateTime ValidTo = DateTime.ParseExact(OfferValid.ValidTo, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            double noCDays = (ValidTo - ValidFrom).TotalDays;
                            for (int i = 0; i <= noCDays; i++)
                            {
                                List<string> Days = OfferValid.BlockDay.Split('^').ToList();
                                bool validDay = false; /*/To check Day Valid for Current Offer Day*/
                                foreach (var Day in Days)
                                {
                                    if (Day != "")
                                    {
                                        if (Day == "Sunday" && DateTime.ParseExact(OfferValid.ValidFrom, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(i).DayOfWeek == DayOfWeek.Sunday)
                                        {
                                            validDay = true;
                                        }
                                        if (Day == "Monday" && DateTime.ParseExact(OfferValid.ValidFrom, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(i).DayOfWeek == DayOfWeek.Monday)
                                        {
                                            validDay = true;
                                        }
                                        if (Day == "Tuesday" && DateTime.ParseExact(OfferValid.ValidFrom, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(i).DayOfWeek == DayOfWeek.Tuesday)
                                        {
                                            validDay = true;
                                        }
                                        if (Day == "Wednesday" && DateTime.ParseExact(OfferValid.ValidFrom, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(i).DayOfWeek == DayOfWeek.Wednesday)
                                        {
                                            validDay = true;
                                        }
                                        if (Day == "Thursday" && DateTime.ParseExact(OfferValid.ValidFrom, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(i).DayOfWeek == DayOfWeek.Thursday)
                                        {
                                            validDay = true;
                                        }
                                        if (Day == "Friday" && DateTime.ParseExact(OfferValid.ValidFrom, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(i).DayOfWeek == DayOfWeek.Friday)
                                        {
                                            validDay = true;
                                        }
                                        if (Day == "Saterday" && DateTime.ParseExact(OfferValid.ValidFrom, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(i).DayOfWeek == DayOfWeek.Saturday)
                                        {
                                            validDay = true;
                                        }
                                    }
                                }
                                if (OfferValid.DaysPrior != null)
                                    ValidTill = DateTime.ParseExact(OfferValid.ValidFrom, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(i).AddDays(-Convert.ToInt16(OfferValid.DaysPrior));
                                else if (OfferValid.BookBefore != null)
                                    ValidTill = DateTime.ParseExact(OfferValid.BookBefore, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                if (Days.Count == 0 || validDay == false && ValidTill >= DateTime.Now)

                                    OfferSDate.Add(new OfferSeason
                                    {
                                        Dates = DateTime.ParseExact(OfferValid.ValidFrom, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(i),
                                        DisountAmount = Convert.ToSingle(OfferValid.DiscountAmount),
                                        DisountPercent = Convert.ToSingle(OfferValid.DiscountPercent),
                                        NewRate = Convert.ToSingle(OfferValid.NewRate),
                                        OfferType = OfferValid.OfferType
                                    });
                            }

                        }
                        else if (OfferValid.DateType == "Block Date" && ValidTill >= DateTime.Now)
                        {
                            DateTime ValidFrom = DateTime.ParseExact(OfferValid.ValidFrom, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            DateTime ValidTo = DateTime.ParseExact(OfferValid.ValidTo, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            double noCDays = (ValidTo - ValidFrom).TotalDays;
                            for (int i = 0; i < noCDays; i++)
                            {
                                OfferBDate.Add(DateTime.ParseExact(OfferValid.ValidFrom, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(i));
                            }

                        }
                    }
                }
            }


            foreach (var obj in OfferSDate)
            {
                if (!OfferBDate.Contains(obj.Dates))
                {
                    ListSeason.Add(obj);
                }

            }
            return ListSeason;
        }

        public static float GetOfferRate(OfferSeason Offer, float ActualRate)
        {
            float OfferTotal = 0;
            if (Offer.OfferType == "Discount")
            {
                if (Offer.DisountAmount != 0)
                {
                    OfferTotal = ActualRate - Convert.ToSingle(Offer.DisountAmount);
                }
                if (Offer.DisountPercent != 0)
                {
                    OfferTotal = ActualRate - (ActualRate * Convert.ToSingle(Offer.DisountPercent) / 100);
                }

            }
            if (Offer.OfferType == "Deal")
            {
                OfferTotal = Convert.ToSingle(Offer.NewRate);
            }

            return OfferTotal;
        }


        #endregion

        #region Inventory Update
        public static List<CommonLib.Response.InventoryType> GetInventory(Int64 HotelRateID, Int64 HotelID, Int64 Supplier, string ChekIn)
        {
            db = new DBHandlerDataContext();
            List<CommonLib.Response.InventoryType> ListInventory = new List<CommonLib.Response.InventoryType>();
            DateTime ChekInDt = DateTime.ParseExact(ChekIn, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
            var sdata = (from obj in db.tbl_CommonInventories where obj.RateID == Convert.ToString(HotelRateID) && obj.HotelID == Convert.ToString(HotelID) && obj.SupplierID == Convert.ToString(Supplier) && obj.Date == Convert.ToString(ChekInDt) select obj).FirstOrDefault();
            //  foreach (var data in sdata)
            //  {
            if (sdata != null)
            {
                ListInventory.Add(new CommonLib.Response.InventoryType
                {
                    InvNoOfRoom = Convert.ToInt32(sdata.INV_NoOfRoom),
                    InventoryName = sdata.InventoryType,
                    InvDate = Convert.ToString(sdata.Date),
                    Sold = Convert.ToInt32(sdata.Sold),
                    IsStop = Convert.ToBoolean(sdata.IsStop)
                });
            }
            //  }
            return ListInventory;
        }

        #endregion
           
    }
}