﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CutAdmin
{
    public partial class ViewVoucher : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string ReservationID = Request.QueryString["ReservationID"];
            string Uid = Request.QueryString["Uid"];
            string Status = Request.QueryString["Status"];
            if (ReservationID != "")
            {
                maincontainer.InnerHtml = VoucherManager.GenrateVoucher(ReservationID, Uid, Status);
            }

        }

        private static bool UrlExists(string url)
        {
            try
            {
                new System.Net.WebClient().DownloadData(url);
                return true;
            }
            catch (System.Net.WebException e)
            {
                if (((System.Net.HttpWebResponse)e.Response).StatusCode == System.Net.HttpStatusCode.NotFound)
                    return false;
                else
                    throw;
            }
        }
    }
}