﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="EditRate.aspx.cs" Inherits="CutAdmin.EditRate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script src="Scripts/EditRate.js?v=1.7"></script>
   <%-- <script src="Scripts/AddRate2.js?v=1.0"></script>--%>
    <script src="Scripts/MastelHotelOffer.js"></script>
    <script src="Scripts/ChargesMapping.js"></script>
    <script src="Scripts/AddOnsCharges.js?v=1.14"></script>
    <!-- Additional styles -->
    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">

    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

    <link href="css/dynamicDiv.css" rel="stylesheet" />

    <script src="Scripts/moments.js"></script>
    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
    <style>
        .deleteMe {
            float: right;
            background: #bb3131;
            color: white;
        }
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <!-- Main content -->
    <section role="main" id="main">

        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        
        <hgroup id="main-title" class="thin">
			<h1>Update Rate Details</h1>
            <h2><b class="grey"><strong>HOTEL</strong>: <strong><label id="lblHotel"></label></strong></b></h2>
            <hr/>			
		</hgroup>


                <div class="with-padding">
                    <div class="columns">
                        <div class="three-columns twelve-columns-mobile" id="Supplier">
                             <label class="input-info">Supplier</label>
                            <select id="sel_Supplier" name="validation-select" class="select full-width">
                                   <option selected="selected">Select Supplier</option>
                                <option value="25" >From Hotel</option>
                            </select>
                        </div>
                         <div class="three-columns twelve-columns-mobile" id="drp_Nationality">
                            <label >Nationality</label>
                                <select id="sel_Nationality" onchange="CheckNationality(this.value);" class="select multiple-as-single easy-multiple-selection allow-empty check-list chkNationality full-width" multiple>
                                    <option value="All">All&nbsp;/&nbsp;Unselect</option>     
                                </select>                               
                        </div>

                        <div class="three-columns twelve-columns-mobile SelBox" id="Currency">
                            <label >Currency</label>
                            <select id="sel_CurrencyCode" name="validation-select" class="select full-width">
                                <option value="-" selected="selected" disabled>Please select</option>
                            </select>
                        </div>
                        <div class="three-columns twelve-columns-mobile" id="MealPlan">
                            <label >Meal Plan</label>
                            <select id="sel_MealPlan" name="validation-select" class="select full-width">
                                <option value="-" selected="selected" disabled>Please select</option>
                            </select>
                        </div>
                    </div>

                            
             <h4>Rate Validity</h4>
                     <hr/>


                    <label id="clickRV" style="display:none" none">1</label>
                    <div id="idRateValidity" class="cRateValidity">
                        <div class="columns columnsRV" id="divRV0">

                            <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                                <label>Valid From</label>
                                <span class="input full-width">
                                    <span class="icon-calendar"></span>
                                    <input type="text" id="txtFromRV0" class="input-unstyled cInputs cRateValidFrom" style="cursor: pointer" value="">
                                </span>

                            </div>

                             <div class="three-columns ten-columns-mobile four-columns-tablet bold">
                                <label>Valid To</label>
                                <span class="input full-width">
                                    <span class="icon-calendar"></span>
                                    <input class="input-unstyled cInputs cRateValidTo" type="text" id="txtToRV0" style="cursor: pointer" value="" />
                                    <%--<input type="text" id="txtToRV0" class="input-unstyled cRateValidTo" value="">--%>
                                </span>
                            </div>
   
                        </div>
                         <br />
                    </div>

                    <div class="columns" style="display:none"none;">
                          <div class="twelve-columns cRVDays" id="iRVDays0" >
                                <small class="input-info">Days</small>
                                <label for="chkAllDays0" class="label">
                                    <input type="checkbox" class="cDayRV" id="chkAllDays0" value="All" onchange="CheckRVAllDays(0);">All</label>
                                <label for="chkSun0" class="label">
                                    <input type="checkbox" class="cDayRV" id="chkSun0" value="Sun" onchange="CheckRVSingle(0);">Sun</label>
                                <label for="chkMon0" class="label">
                                    <input type="checkbox" class="cDayRV" id="chkMon0" value="Mon" onchange="CheckRVSingle(0);">Mon</label>
                                <label for="chkTue0" class="label">
                                    <input type="checkbox" class="cDayRV" id="chkTue0" value="Tue" onchange="CheckRVSingle(0);">Tue</label>
                                <label for="chkWed0" class="label">
                                    <input type="checkbox" class="cDayRV" id="chkWed0" value="Wed" onchange="CheckRVSingle(0);">Wed</label>
                                <label for="chkThu0" class="label">
                                    <input type="checkbox" class="cDayRV" id="chkThu0" value="Thu" onchange="CheckRVSingle(0);">Thu</label>
                                <label for="chkFri0" class="label">
                                    <input type="checkbox" class="cDayRV" id="chkFri0" value="Fri" onchange="CheckRVSingle(0);">Fri</label>
                                <label for="chkSat0" class="label">
                                    <input type="checkbox" class="cDayRV" id="chkSat0" value="Sat" onchange="CheckRVSingle(0);">Sat</label>

                            </div>
                        <br />
                    </div>
                   <%-- <h6 class="underline margin-bottom">
                        <input type="checkbox" id="chkSpecialDate" onchange="CheckSpecialDate()" /><label for="chkSpecialDate" class="label">SPECIAL DATES</label></h6>--%>
                    <%-- special Dates--%>

                    <label id="clickSD" style="display:none" none">1</label>
                    <div id="idSpecialDate" class="cSpecialDate" style="display:none" none">

                        <div class="columns" id="divSD0">

                            <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                                <label>Valid From</label>
                                <span class="input">
                                    <span class="icon-calendar"></span>
                                    <input type="text" id="txtFromSD0" class="input-unstyled ctxtfromSD" placeholder="Select Date">
                                </span>

                            </div>
                            <div class="three-columns ten-columns-mobile four-columns-tablet bold">
                                <label> Valid To</label>
                                <span class="input">
                                    <span class="icon-calendar"></span>
                                    <input type="text" id="txtToSD0" class="input-unstyled ctxttoSD" placeholder="Select Date" >
                                </span>
                            </div>
                          
                            <div class="three-column plusminus">
                                <br />
                                <a id="lnkAddSD0" onclick="AddSpecialDates(0);" class="button">
                                    <span class="icon-plus-round blue"></span>
                                </a>

                            </div>
                        </div>
                        <br />
                    </div>
                    <div class="columns" id="dispSD" style="display:none">
                          <div class="twelve-columns cSDDays" id="iSDDays0">
                                <small class="input-info">Days:</small>
                                <label for="chkAllSD0" class="label">
                                    <input type="checkbox" id="chkAllSD0" value="All" class="cDaySD" onclick="CheckSDAllDays(0);">All</label>
                                <label for="chkSunSD0" class="label">
                                    <input type="checkbox" id="chkSunSD0" value="Sun" class="cDaySD" onclick="CheckSDSingle(0);">Sun</label>
                                <label for="chkMonSD0" class="label">
                                    <input type="checkbox" id="chkMonSD0" value="Mon" class="cDaySD" onclick="CheckSDSingle(0);">Mon</label>
                                <label for="chkTueSD0" class="label">
                                    <input type="checkbox" id="chkTueSD0" value="Tue" class="cDaySD" onclick="CheckSDSingle(0);">Tue</label>
                                <label for="chkWedSD0" class="label">
                                    <input type="checkbox" id="chkWedSD0" value="Wed" class="cDaySD" onclick="CheckSDSingle(0);">Wed</label>
                                <label for="chkThuSD0" class="label">
                                    <input type="checkbox" id="chkThuSD0" value="Thu" class="cDaySD" onclick="CheckSDSingle(0);">Thu</label>
                                <label for="chkFriSD0" class="label">
                                    <input type="checkbox" id="chkFriSD0" value="Fri" class="cDaySD" onclick="CheckSDSingle(0);">Fri</label>
                                <label for="chkSatSD0" class="label">
                                    <input type="checkbox" id="chkSatSD0" value="Sat" class="cDaySD" onclick="CheckSDSingle(0);">Sat</label>

                            </div>
                    </div>
                    <%--End special Dates--%>

                    <hr/><br />

                     <p><a href="#" class="button anthracite-gradient float-right" id="btn_AddDates" style="cursor:pointer" onClick="AddDates();">Add Dates</a><br />

                         <div id="DivDates">
                             <h4>Rooms</h4>
                             <hr />
                             <br />

                             <ul class="standard-tabs margin-bottom" id="TabRooms">
                             </ul>
                             <div class="tabs-content NestesDiv" id="hdndiv1" style="display: none">
                             </div>
                         </div>

                   
                 <p class="text-textright"><button type="button" onclick="UpdateRates()" class="button anthracite-gradient ">Updates</button></p>           
                </div>
                  
                <%--<div class="button-height with-mid-padding silver-gradient no-margin-top">
                    <a  class="button blue-gradient float-right" onclick="UpdateRates()" style="cursor:pointer">Update</a>
                    <br />
                </div>--%>

            </div>
        

    </section>
    <!-- End main content -->


</asp:Content>
