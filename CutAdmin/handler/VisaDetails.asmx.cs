﻿using CutAdmin.DataLayer;
using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for VisaDetails
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class VisaDetails : System.Web.Services.WebService
    {

        private string escapejosndata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        dbAdminHelperDataContext DB = new dbAdminHelperDataContext();
        GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        string json = "";
        int i;
        [WebMethod(EnableSession = true)]
        public string GetVisaChargesByNationality(string Type, string Nationality)
        {
            try
            {
                var List = (from obj in DB.tbl_VisaChargesDetails where obj.VisaType==Type && obj.Nationality==Nationality select obj ).ToList();
                if (List.Count != 0)
                {
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, tbl_Visa = List });
                }
                else
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetVisaChargesByCurrency(string Type, string Nationality, string Currency)
        {
            try
            {
                var List = (from obj in DB.tbl_VisaChargesDetails where obj.VisaType == Type && obj.Nationality == Nationality && obj.Currency == Currency select obj).ToList();
                if (List.Count != 0)
                {
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, tbl_Visa = List });
                }
                else
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                }
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        
        [WebMethod(EnableSession = true)]
        public string UpdateVisa(string Type, string Nationality, string Currency, decimal Fee, decimal Urgent)
        {
            try
            {
                GlobalDefault objGlobalDefault = new GlobalDefault();
                objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 uid = objGlobalDefault.sid;

                tbl_VisaChargesDetail updVisaStatus = DB.tbl_VisaChargesDetails.Single(y => y.VisaType == Type && y.Nationality == Nationality);
                updVisaStatus.Fees = Fee;
                updVisaStatus.Currency = Currency;
                updVisaStatus.UrgentChg = Urgent;
                updVisaStatus.UpdateBy = uid;
                DB.SubmitChanges();

                //if (List.Count != 0)
                //{
                //    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, tbl_Visa = List });
                //}
                //else
                //{
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                //}
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string AddVisaCharge(string Type, string Nationality, string Currency, decimal Fee, decimal Urgent)
        {
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 UserId = objGlobalDefault.sid;

                var List = (from obj in DB.tbl_VisaChargesDetails where obj.VisaType == Type && obj.Nationality == Nationality && obj.Currency == Currency select obj).ToList();
                if (List.Count != 0)
                {
                    try
                    {
                        tbl_VisaChargesDetail Vissa = DB.tbl_VisaChargesDetails.Single(d => d.VisaType == Type && d.Nationality == Nationality && d.Currency == Currency);
                        Vissa.Fees = Fee;
                        Vissa.UrgentChg = Urgent;
                        DB.SubmitChanges();

                        return "{\"Session\":\"1\",\"retCode\":\"1\"}";
                    }
                    catch
                    {
                        return "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }
                }
                else
                {
                    try
                    {
                        tbl_VisaChargesDetail Visa = new tbl_VisaChargesDetail();
                        Visa.VisaType = Type;
                        Visa.Nationality = Nationality;
                        Visa.Required = "Y";
                        Visa.Currency = Currency;
                        Visa.Fees = Fee;
                        Visa.UrgentChg = Urgent;
                        Visa.UpdateBy = UserId;

                        DB.tbl_VisaChargesDetails.InsertOnSubmit(Visa);
                        DB.SubmitChanges();

                        return "{\"Session\":\"1\",\"retCode\":\"1\"}";
                    }
                    catch
                    {
                        return "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }
                }
               
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        //[WebMethod(EnableSession = true)]
        //public string UpdateVisaStatus(Int64 sid, string status, Int64 UserId, string RefrenceNo)
        //{
        //    try
        //    {
        //        var OldHistory = (from obj in DB.tbl_VisaDetails where obj.sid == UserId select obj.History);

        //        tbl_VisaDetail updVisaStatus = DB.tbl_VisaDetails.Single(y => y.sid == UserId);
        //        updVisaStatus.Status = status;
        //        updVisaStatus.History = OldHistory + "^" + status + ": " + DateTime.Now;
        //        DB.SubmitChanges();

        //        //if (List.Count != 0)
        //        //{
        //        //    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, tbl_Visa = List });
        //        //}
        //        //else
        //        //{
        //        json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
        //        //}
        //    }
        //    catch
        //    {
        //        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return json;
        //}
    }
}
