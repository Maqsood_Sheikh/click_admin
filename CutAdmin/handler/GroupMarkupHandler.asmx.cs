﻿using CutAdmin;
using CutAdmin.DataLayer;
using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for GroupMarkupHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class GroupMarkupHandler : System.Web.Services.WebService
    {
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        dbAdminHelperDataContext DB = new dbAdminHelperDataContext();
        string json = "";
        #region Group Markup
        [WebMethod(EnableSession = true)]
        public string GetGroup()
        {
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                List<string> ListDetail = new List<string>();
                if (objGlobalDefault.UserType == "Supplier")
                {
                    var List = (from obj in DB.tbl_GroupMarkups select obj).ToList();

                    if (List.Any())
                    {
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                    }
                    else
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }
                }
                if (objGlobalDefault.UserType == "SupplierStaff")
                {
                    var List = (from obj in DB.tbl_GroupMarkups where obj.ParentID == objGlobalDefault.ParentId select obj).ToList();

                    if (List.Any())
                    {
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                    }
                    else
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GroupMarkupByGroupId(Int64 GroupId, Int64 Type)
        {
            try
            {
                var List = (from obj in DB.tbl_GroupMarkupDetails
                            where obj.GroupId == GroupId && obj.Type ==Type
                            select new
                            {
                                obj.sid,
                                obj.Supplier,
                                obj.Type,
                                obj.MarkupPercentage,
                                obj.MarkupAmmount,
                                obj.CommessionPercentage,
                                obj.CommessionAmmount,
                                obj.GroupId,
                                obj.TaxApplicable,

                            }).ToList();

                if (List.Any())
                {
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                }
                else
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetFlightSupplier()
        {
            string jsonString = "";

            var list = (from obj in DB.tbl_APIDetails where obj.Flight == true select obj.Supplier).ToList();

            if (list.Count > 0)
            {
                jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, list = list });
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }


            return jsonString;
        }
        [WebMethod(EnableSession = true)]
        public string GetGroupMarkup(Int64 Id)
        {
            try
            {
                var List = (from obj in DB.tbl_GroupMarkups
                            join mark in DB.tbl_GroupMarkupDetails on obj.sid equals mark.GroupId
                            where obj.sid == Id
                            select new
                            {
                                obj.sid,
                                obj.GroupName,
                                obj.ParentID,
                                mark.MarkupPercentage,
                                mark.MarkupAmmount,
                                mark.CommessionAmmount,
                                mark.CommessionPercentage,
                                mark.TaxApplicable

                            }).ToList();

                if (List.Any())
                {
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                }
                else
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string AddGroupDetails(string GroupName, Int64 MarkPercentage, Int64 MarkUpAmount, Int64 CommPercentage, Int64 CommAmount)
        {
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                CutAdmin.HotelAdmin.tbl_GroupMarkup Mark = new CutAdmin.HotelAdmin.tbl_GroupMarkup();
                Mark.GroupName = GroupName;
                if (objGlobalDefault.UserType == "Supplier")
                {
                    Mark.ParentID = objGlobalDefault.sid;
                }
                else if (objGlobalDefault.UserType == "SupplierStaff")
                {
                    Mark.ParentID = objGlobalDefault.ParentId;
                }

                //Mark.ParentID = 232;
                //DB.tbl_GroupMarkups.InsertOnSubmit(Mark);
                DB.SubmitChanges();

                CutAdmin.HotelAdmin.tbl_GroupMarkupDetail Det = new CutAdmin.HotelAdmin.tbl_GroupMarkupDetail();
                Det.Type = 1;
                Det.MarkupPercentage = MarkPercentage;
                Det.MarkupAmmount = MarkUpAmount;
                Det.CommessionAmmount = CommAmount;
                Det.CommessionPercentage = CommPercentage;
                Det.GroupId = Mark.sid;
                Det.TaxApplicable = false;
                //DB.tbl_GroupMarkupDetails.InsertOnSubmit(Det);
                DB.SubmitChanges();


                json = jsSerializer.Serialize(new { Session = 1, retCode = 1 });

            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string Update(Int64 GroupId, Int64 MarkPercentage, Int64 MarkUpAmount, Int64 CommPercentage, Int64 CommAmount)
        {
            try
            {
                //CutAdmin.HotelAdmin.tbl_GroupMarkupDetail Det = DB.tbl_GroupMarkupDetails.Where(d => d.GroupId == GroupId).FirstOrDefault();

                //Det.MarkupPercentage = MarkPercentage;
                //Det.MarkupAmmount = MarkUpAmount;
                //Det.CommessionAmmount = CommAmount;
                //Det.CommessionPercentage = CommPercentage;
                //DB.SubmitChanges();

                json = jsSerializer.Serialize(new { Session = 1, retCode = 1 });

            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        #endregion

    }
}
