﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using CutAdmin.HotelAdmin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.dbml;
namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for MarkUpHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class MarkUpHandler : System.Web.Services.WebService
    {
        AdminDBHandlerDataContext DB = new AdminDBHandlerDataContext();
        //dbTaxHandlerDataContext dbTax = new dbTaxHandlerDataContext();
        DBHandlerDataContext db = new DBHandlerDataContext();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        string json = "";

        [WebMethod(EnableSession = true)]
        public string SaveMarkUp(string Perc, string Amt)
        {
            try
            {
                Int64 uid = 0;
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                if (objGlobalDefault.UserType != "Supplier")
                {
                    using (var dbTax = new dbTaxHandlerDataContext())
                    {
                        uid = objGlobalDefault.ParentId;
                        tbl_AgentMarkup Add = new tbl_AgentMarkup();
                        Add.ServiceType = 1;
                        Add.uid = Convert.ToInt64(uid);
                        Add.Percentage = Convert.ToDecimal(Perc);
                        Add.Amount = Convert.ToDecimal(Amt);
                        dbTax.tbl_AgentMarkups.InsertOnSubmit(Add);
                        dbTax.SubmitChanges();
                    }
                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
                else if (objGlobalDefault.UserType == "Supplier")
                {
                    using (var dbTax = new dbTaxHandlerDataContext())
                    {
                        CutAdmin.BL.tbl_GlobalMarkup Add = new CutAdmin.BL.tbl_GlobalMarkup();
                        Add.MarkupPercentage = Convert.ToDecimal(Perc);
                        Add.MarkupAmmount = Convert.ToDecimal(Amt);
                        Add.CommessionPercentage = 0;
                        Add.CommessionAmmount = 0;
                        Add.Type = 1;
                        Add.ParentID = objGlobalDefault.sid;
                        dbTax.tbl_GlobalMarkups.InsertOnSubmit(Add);
                        dbTax.SubmitChanges();
                    }
                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string LoadMarkUp()
        {
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 Uid = objGlobalDefault.sid;
                using (var dbTax = new dbTaxHandlerDataContext())
                {
                    if (objGlobalDefault.UserType != "Supplier")
                    {
                        Uid = objGlobalDefault.ParentId;
                        var List = (from obj in dbTax.tbl_AgentMarkups where obj.uid == Uid select obj).ToList();

                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, AgentMarkUps = List });
                    }
                    else if (objGlobalDefault.UserType == "Supplier")
                    {
                        var List = (from obj in dbTax.tbl_GlobalMarkups where obj.ParentID == Uid select obj).ToList();

                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, AgentMarkUps = List });
                    }
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateMarkUp(string Perc, string Amt)
        {
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 Uid = objGlobalDefault.sid;
                using (var dbTax = new dbTaxHandlerDataContext())
                {
                    if (objGlobalDefault.UserType != "Supplier")
                    {
                        Uid = objGlobalDefault.ParentId;
                        tbl_AgentMarkup Update = dbTax.tbl_AgentMarkups.Where(x => x.uid == Uid).FirstOrDefault();
                        Update.Percentage = Convert.ToDecimal(Perc);
                        Update.Amount = Convert.ToDecimal(Amt);
                        dbTax.SubmitChanges();
                        json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                    }
                    else if (objGlobalDefault.UserType == "Supplier")
                    {
                        CutAdmin.BL.tbl_GlobalMarkup Update = dbTax.tbl_GlobalMarkups.Where(x => x.ParentID == Uid).FirstOrDefault();
                        Update.MarkupPercentage = Convert.ToDecimal(Perc);
                        Update.MarkupAmmount = Convert.ToDecimal(Amt);
                        dbTax.SubmitChanges();
                        json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                    }
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        #region Group Markup
        [WebMethod(EnableSession = true)]
        public string GetGroup()
        {
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                List<string> ListDetail = new List<string>();
                using (var db = new dbAdminHelperDataContext())
                {
                    if (objGlobalDefault.UserType == "Admin")
                    {
                        var List = (from obj in db.tbl_GroupMarkups  select obj).ToList();

                        if (List.Any())
                        {
                            json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                        }
                        else
                        {
                            json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                        }
                    }
                    if (objGlobalDefault.UserType == "AdminStaff")
                    {
                        var List = (from obj in db.tbl_GroupMarkups where obj.ParentID == objGlobalDefault.ParentId select obj).ToList();

                        if (List.Any())
                        {
                            json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                        }
                        else
                        {
                            json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                        }
                    }
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetGroupMarkup(Int64 Id)
        {
            try
            {
                var arrMarkups = MarkupManger.GetService();
                List<Service> arrService = new List<Service>();
                using (var db = new dbAdminHelperDataContext())
                {
                    foreach (var objService in arrMarkups)
                    {
                        var arrSupplier = new List<Supplier>();
                        foreach (var objSupplier in objService.ListSupplier)
                        {
                            var arrMarkup = (from obj in db.tbl_GroupMarkupDetails
                                               where obj.Type == Convert.ToInt64(arrMarkups.IndexOf(objService) + 1)
                                               && obj.GroupId == Id && obj.Supplier == objSupplier.Name
                                               select new Supplier
                                               {
                                                   Name = obj.Supplier,
                                                   MarkupAmt = Convert.ToDecimal(obj.MarkupAmmount),
                                                   MarkupPer = Convert.ToDecimal(obj.MarkupPercentage),
                                                   CommAmt = Convert.ToDecimal(obj.CommessionAmmount),
                                                   CommPer = Convert.ToDecimal(obj.CommessionPercentage),
                                                   MarkupID = obj.sid
                                               }).FirstOrDefault();
                            if (arrMarkup== null)
                            {
                                arrSupplier.Add(new Supplier
                                {
                                    Name = objSupplier.Name,
                                    MarkupAmt = 0,
                                    MarkupPer = 0,
                                    CommAmt =0,
                                    CommPer = 0,
                                    MarkupID = 0
                                });
                            }
                            else
                            {
                                arrSupplier.Add(arrMarkup);
                            }
                           
                        }
                        arrService.Add(new Service { Name = objService.Name, ListSupplier = arrSupplier }); 
                    }
                    json = jsSerializer.Serialize(new { retCode = 1, arrDetails = arrService });
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string SaveGroupMarkup(List<Service> arrService, Int64 GroupId)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

            try
            {
                using (var db = new dbAdminHelperDataContext())
                {
                    foreach (var objService in arrService)
                    {

                        foreach (var objSupplier in objService.ListSupplier)
                        {
                            var arrMarkup = (from obj in db.tbl_GroupMarkupDetails
                                             where obj.Type == Convert.ToInt64(arrService.IndexOf(objService) + 1)
                                                 && obj.Supplier == objSupplier.Name && obj.GroupId==GroupId
                                             select obj).FirstOrDefault();
                            if (arrMarkup != null)
                            {
                                arrMarkup.MarkupAmmount = objSupplier.MarkupAmt;
                                arrMarkup.MarkupPercentage = objSupplier.MarkupPer;
                                arrMarkup.CommessionAmmount = objSupplier.CommAmt;
                                arrMarkup.CommessionPercentage = objSupplier.CommPer;
                                db.SubmitChanges();
                            }
                            else
                            {
                                arrMarkup = new dbml.tbl_GroupMarkupDetail
                                {
                                    GroupId = GroupId,
                                    MarkupAmmount = objSupplier.MarkupAmt,
                                    MarkupPercentage = objSupplier.MarkupPer,
                                    ParentID=0,
                                    Supplier = objSupplier.Name,
                                    TaxApplicable=false,
                                    CommessionPercentage = 0,
                                    CommessionAmmount=0,
                                    Type = Convert.ToInt64(arrService.IndexOf(objService) + 1)
                                };
                                db.tbl_GroupMarkupDetails.InsertOnSubmit(arrMarkup);
                                db.SubmitChanges();

                            }
                           
                        }

                    }
                }
                return jsSerializer.Serialize(new { retCode = 1 });
            }
            catch (Exception)
            {
                return jsSerializer.Serialize(new { retCode = 0 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string AddGroupDetails(string GroupName, Int64 MarkPercentage, Int64 MarkUpAmount, Int64 CommPercentage, Int64 CommAmount)
        {
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                CutAdmin.HotelAdmin.tbl_GroupMarkup Mark = new CutAdmin.HotelAdmin.tbl_GroupMarkup();
                Mark.GroupName = GroupName;
                if (objGlobalDefault.UserType == "Supplier")
                {
                    Mark.ParentID = objGlobalDefault.sid;
                }
                else if (objGlobalDefault.UserType == "SupplierStaff")
                {
                    Mark.ParentID = objGlobalDefault.ParentId;
                }

                //Mark.ParentID = 232;
                DB.tbl_GroupMarkups.InsertOnSubmit(Mark);
                DB.SubmitChanges();

                CutAdmin.HotelAdmin.tbl_GroupMarkupDetail Det = new CutAdmin.HotelAdmin.tbl_GroupMarkupDetail();
                Det.Type = 1;
                Det.MarkupPercentage = MarkPercentage;
                Det.MarkupAmmount = MarkUpAmount;
                Det.CommessionAmmount = CommAmount;
                Det.CommessionPercentage = CommPercentage;
                Det.GroupId = Mark.sid;
                Det.TaxApplicable = false;
                DB.tbl_GroupMarkupDetails.InsertOnSubmit(Det);
                DB.SubmitChanges();


                json = jsSerializer.Serialize(new { Session = 1, retCode = 1 });

            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string Update(Int64 GroupId, Int64 MarkPercentage, Int64 MarkUpAmount, Int64 CommPercentage, Int64 CommAmount)
        {
            try
            {
                CutAdmin.HotelAdmin.tbl_GroupMarkupDetail Det = DB.tbl_GroupMarkupDetails.Where(d => d.GroupId == GroupId).FirstOrDefault();

                Det.MarkupPercentage = MarkPercentage;
                Det.MarkupAmmount = MarkUpAmount;
                Det.CommessionAmmount = CommAmount;
                Det.CommessionPercentage = CommPercentage;
                DB.SubmitChanges();

                json = jsSerializer.Serialize(new { Session = 1, retCode = 1 });

            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        //******************************************************************************************************

        [WebMethod(EnableSession = true)]
        public string GetGuropName(string GetgName)
        {
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                List<string> ListDetail = new List<string>();
                using (var db = new dbAdminHelperDataContext())
                {
                    if (objGlobalDefault.UserType == "Supplier")
                    {
                        var List = (from obj in db.tbl_GroupMarkups where obj.ParentID == objGlobalDefault.sid && obj.GroupName == GetgName select obj).ToList();

                        if (List.Any())
                        {
                            json = jsSerializer.Serialize(new { Session = 1, retCode = 1, GetgnameArr = List });
                        }
                        else
                        {
                            json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                        }
                    }
                    if (objGlobalDefault.UserType == "SupplierStaff")
                    {
                        var List = (from obj in db.tbl_GroupMarkups where obj.ParentID == objGlobalDefault.ParentId && obj.GroupName == GetgName select obj).ToList();
                        if (List.Any())
                        {
                            json = jsSerializer.Serialize(new { Session = 1, retCode = 1, GetgnameArr = List });
                        }
                        else
                        {
                            json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                        }
                    }
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        #endregion


        #region Individual Markup
        [WebMethod(EnableSession = true)]
        public string GetIndividualMarkup(Int64 Id)
        {
            try
            {
                var List = (from obj in DB.tbl_IndividualMarkups where obj.AgentId == Id select obj).ToList();

                if (List.Count == 0)
                {
                    CutAdmin.HotelAdmin.tbl_IndividualMarkup Det = new CutAdmin.HotelAdmin.tbl_IndividualMarkup();
                    Det.Type = 1;
                    Det.MarkupPercentage = 0;
                    Det.MarkupAmmount = 0;
                    Det.CommessionAmmount = 0;
                    Det.CommessionPercentage = 0;
                    Det.AgentId = Id;
                    DB.tbl_IndividualMarkups.InsertOnSubmit(Det);
                    DB.SubmitChanges();
                }

                if (List.Any())
                {
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                }
                else
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateIndividualMarkupDetails(Int64 Sid, Int64 IndMarkPercentage, Int64 IndMarkUpAmount, Int64 IndCommPercentage, Int64 IndCommAmount)
        {
            try
            {
                CutAdmin.HotelAdmin.tbl_IndividualMarkup Det = DB.tbl_IndividualMarkups.Where(d => d.AgentId == Sid).FirstOrDefault();

                Det.Type = 1;
                Det.MarkupPercentage = IndMarkPercentage;
                Det.MarkupAmmount = IndMarkUpAmount;
                Det.CommessionAmmount = IndCommAmount;
                Det.CommessionPercentage = IndCommPercentage;
                DB.SubmitChanges();

                json = jsSerializer.Serialize(new { Session = 1, retCode = 1 });

            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        #endregion


        #region Global Markup
        [WebMethod(EnableSession = true)]
        public string GetGlobalMarkup()
        {
            try
            {
                List<Service> arrService = new List<Service>();
                var arrMarkups = MarkupManger.GetService();
                using (var db = new dbAdminHelperDataContext())
                {
                    foreach (var objService in arrMarkups)
                    {
                        var arrSupplier = (from obj in db.tbl_GlobalMarkups
                                           where obj.Type == Convert.ToInt64(arrMarkups.IndexOf(objService) + 1)
                                           select new Supplier
                                           {
                                               Name = obj.Supplier,
                                               MarkupAmt = Convert.ToDecimal(obj.MarkupAmmount),
                                               MarkupPer = Convert.ToDecimal(obj.MarkupPercentage),
                                               CommAmt = Convert.ToDecimal(obj.CommessionAmmount),
                                               CommPer = Convert.ToDecimal(obj.CommessionPercentage),
                                               MarkupID = obj.sid
                                           }).ToList();
                        arrService.Add(new Service { Name = objService.Name, ListSupplier = arrSupplier });
                    }
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = arrService });
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string SaveGlobalMarkup(List<Service> arrService)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            
            try
            {
                using (var db = new dbAdminHelperDataContext())
                {
                    foreach (var objService in arrService)
                    {
                      
                        foreach (var objSupplier in objService.ListSupplier)
                        {
                            var arrMarkup = (from obj in db.tbl_GlobalMarkups
                                             where obj.Type == Convert.ToInt64(arrService.IndexOf(objService) + 1)
                                                 && obj.Supplier == objSupplier.Name
                                             select obj).FirstOrDefault();
                            arrMarkup.MarkupAmmount = objSupplier.MarkupAmt;
                            arrMarkup.MarkupPercentage = objSupplier.MarkupPer;
                            arrMarkup.CommessionAmmount = objSupplier.CommAmt;
                            arrMarkup.CommessionPercentage = objSupplier.CommPer;
                            db.SubmitChanges();
                        }
                     
                    }
              }
                return jsSerializer.Serialize(new { retCode = 1 });
            }
            catch (Exception)
            {
                return jsSerializer.Serialize(new { retCode = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string UpdateGlobalMarkupDetails(Int64 MarkPercentage, Int64 MarkUpAmount, Int64 CommPercentage, Int64 CommAmount)
        {
            try
            {
                CutAdmin.HotelAdmin.tbl_GlobalMarkup Det = DB.tbl_GlobalMarkups.FirstOrDefault();

                Det.Type = 1;
                Det.MarkupPercentage = MarkPercentage;
                Det.MarkupAmmount = MarkUpAmount;
                Det.CommessionAmmount = CommAmount;
                Det.CommessionPercentage = CommPercentage;
                DB.SubmitChanges();

                json = jsSerializer.Serialize(new { Session = 1, retCode = 1 });

            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetAgentGroup(Int64 AgentId)
        {
            try
            {
                var List = (from obj in DB.tbl_AgentGroupMarkupMappings where obj.AgentId == AgentId select obj).ToList();
                if (List.Any())
                {
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                }
                else
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        #endregion


    }
}
