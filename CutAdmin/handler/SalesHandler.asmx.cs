﻿using CutAdmin.BL;
using CutAdmin.Common;
using CutAdmin.DataLayer;
using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for SalesHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class SalesHandler : System.Web.Services.WebService
    {

        [WebMethod(true)]
        public string AddSalesPerson(string sFirstName, string sLastName, string Gender, string nMobile, string sEmail, string DOB, string sAddress, string sCountry, string State, string sCity, string nPinCode, string SalesTerritory, string TerCountry, string TerState, string TerCityCountry, string TerCity, string TerAgency)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            dbAdminHelperDataContext DB = new dbAdminHelperDataContext();
            string jsonString = "";
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 ParentID = objGlobalDefault.sid;

            Random generator = new Random();
            String Password = generator.Next(0, 999999).ToString("D6");
            Password = Cryptography.EncryptText(Password);
            try
            {

                CutAdmin.dbml.tbl_Contact objcont = new CutAdmin.dbml.tbl_Contact();

                objcont.Mobile = nMobile;

                objcont.email = sEmail;
                objcont.Address = sAddress;
                objcont.sCountry = sCountry;
                objcont.StateID = State;
                objcont.Code = sCity;
                objcont.PinCode = nPinCode;
                DB.tbl_Contacts.InsertOnSubmit(objcont);
                DB.SubmitChanges();

                CutAdmin.dbml.tbl_StaffLogin objSales = new tbl_StaffLogin();
                objSales.uid = sEmail;
                objSales.UserType = "SalesStaff";
                objSales.password = Password;
                objSales.ContactPerson = sFirstName;
                objSales.Last_Name = sLastName;
                objSales.Gender = Gender;
                objSales.DOB = DOB;
                objSales.ContactID = objcont.ContactID;
                objSales.ParentId = ParentID;
                objSales.TerritoryType = SalesTerritory;

                if (SalesTerritory == "City")
                {
                    objSales.CountryID = TerCityCountry;
                }
                else
                {
                    objSales.CountryID = TerCountry;
                }

                //objSales.CountryID = TerCountry;
                objSales.CitiesID = TerCity;
                objSales.StateID = TerState;
                objSales.Agents = TerAgency;

                DB.tbl_StaffLogins.InsertOnSubmit(objSales);
                DB.SubmitChanges();
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch (Exception)
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return jsonString;
        }
        [WebMethod(true)]
        public string GetAllSalesPerson()
        {
            string jsonString = "";
            //DBHandlerDataContext DB = new DBHandlerDataContext();
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            var Territory = "";
            string Conutry = "";
            string states = "";
            string Cities = "";
            string Agents = "";
            string Password = "";
            try
            {
                using (var DB = new dbAdminHelperDataContext())
                {
                    var List = (from obj in DB.tbl_StaffLogins
                                join meta in DB.tbl_Contacts on obj.ContactID equals meta.ContactID
                                where obj.UserType == "SalesStaff"
                                //join city in DB.tbl_HCities on obj.CityAllocated equals city.Code
                                select new
                                {
                                    obj.sid,
                                    obj.ContactPerson,
                                    Name = obj.ContactPerson + " " + obj.Last_Name,
                                    meta.email,
                                    obj.password,

                                    Territory = obj.TerritoryType,
                                    meta.ContactID,
                                    obj.StateID,
                                    obj.CountryID,
                                    obj.CitiesID,
                                    obj.Agents,

                                }).Distinct().ToList();

                    for (int i = 0; i < List.Count; i++)
                    {
                        string sEncryptedPassword = Cryptography.DecryptText(List[i].password);
                        Password += sEncryptedPassword + ',';
                    }

                    for (int i = 0; i < List.Count; i++)
                    {
                        if (List[i].Territory == "Country")
                        {
                            String[] countries = List[i].CountryID.Split(',');

                            for (int j = 0; j < countries.Length; j++)
                            {
                                var countrylist = (from obj in DB.tbl_HCities
                                                   where obj.Country == countries[j]
                                                   select new
                                                   {
                                                       obj.Countryname,


                                                   }).Distinct().ToList();

                                Conutry += countrylist[0].Countryname + ",";

                            }


                        }
                        else if (List[i].Territory == "State")
                        {
                            String[] state = List[i].StateID.Split(',');

                            for (int k = 0; k < state.Length; k++)
                            {
                                var Statelist = (from obj in DB.tbl_GstStates
                                                 where obj.StateID == state[k]
                                                 select new
                                                 {
                                                     obj.SateName,


                                                 }).Distinct().ToList();

                                states += Statelist[0].SateName + ",";
                            }
                        }
                        else if (List[i].Territory == "City")
                        {
                            String[] City = List[i].CitiesID.Split(',');

                            for (int l = 0; l < City.Length; l++)
                            {
                                var Statelist = (from obj in DB.tbl_HCities
                                                 where obj.Code == City[l]
                                                 select new
                                                 {
                                                     obj.Description,


                                                 }).Distinct().ToList();

                                Cities += Statelist[0].Description + ",";
                            }


                        }

                        else
                        {
                            String[] Agent = List[i].Agents.Split(',');

                            for (int m = 0; m < Agent.Length; m++)
                            {
                                var Statelist = (from obj in DB.tbl_AdminLogins
                                                 where obj.sid.ToString() == Agent[m]
                                                 select new
                                                 {
                                                     obj.AgencyName,


                                                 }).Distinct().ToList();

                                Agents += Statelist[0].AgencyName + ",";
                            }


                        }
                    }


                    ListtoDataTable lsttodt = new ListtoDataTable();
                    DataTable dt = lsttodt.ToDataTable(List);
                    //DataTable Tissues = new DataTable();
                    Session.Add("Sales", dt);

                    jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List, TConutry = Conutry, Tstates = states, TCities = Cities, TAgents = Agents, Password = Password });

                }
            }
            catch (Exception)
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        public class ListtoDataTable
        {
            public DataTable ToDataTable<T>(List<T> items)
            {
                DataTable dataTable = new DataTable(typeof(T).Name);
                //Get all the properties by using reflection   
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names  
                    dataTable.Columns.Add(prop.Name);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {

                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }

                return dataTable;
            }
        }

        [WebMethod(true)]
        public string SearchSales(string Name, string Territory)
        {

            DataTable dtResult = (DataTable)Session["Sales"];
            Int32 Get = 20;
            string jsonString = "";
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            DataTable dtTable = dtResult.Clone();
            foreach (DataRow dr in dtResult.Rows)
            {

                dtTable.ImportRow(dr);

            }


            // DataTable SorteddtResult = dtResult.Clone();
            DataView myDataView = dtResult.DefaultView;
            myDataView.Sort = "sid DESC";
            DataTable SorteddtResult = myDataView.ToTable();

            DataRow[] rows = null;

            if (Territory == "-")
            {
                Territory = "";
            }
            //if (sTerCountry == "-")
            //{
            //    sTerCountry = "";
            //}

            if (Name != "")
            {
                rows = SorteddtResult.Select("(Name like '%" + Name + "%')");

                if (rows.Length != 0)
                {
                    SorteddtResult = rows.CopyToDataTable();
                }
                else
                {
                    SorteddtResult = null;

                    SorteddtResult = dtResult.Clone();
                }
            }
            if (Territory != "")
            {
                rows = SorteddtResult.Select("(Territory like '%" + Territory + "%')");

                if (rows.Length != 0)
                {
                    SorteddtResult = rows.CopyToDataTable();
                }
                else
                {
                    SorteddtResult = null;

                    SorteddtResult = dtResult.Clone();
                }
            }
            //if (sTerCountry != "")
            //{
            //    rows = SorteddtResult.Select("(Country like '%" + sTerCountry + "%')");

            //    if (rows.Length != 0)
            //    {
            //        SorteddtResult = rows.CopyToDataTable();
            //    }
            //    else
            //    {
            //        SorteddtResult = null;

            //        SorteddtResult = dtResult.Clone();
            //    }
            //}

            try
            {
                IEnumerable<DataRow> allButFirst = SorteddtResult.AsEnumerable().Skip(0).Take(Get);
                SorteddtResult = allButFirst.CopyToDataTable();
                jsonString = "";

                List<Dictionary<string, object>> List_Sales = new List<Dictionary<string, object>>();

                List_Sales = JsonStringManager.ConvertDataTable(SorteddtResult);
                var Password = "";
                for (int i = 0; i < List_Sales.Count; i++)
                {
                    // var Pass = List_Sales.Where(d => d.Keys.ToString() == "password").FirstOrDefault().Values.ToString();
                    string sEncryptedPassword = Cryptography.DecryptText(SorteddtResult.Rows[i]["password"].ToString());
                    Password += sEncryptedPassword + ',';
                }
                dtResult.Dispose();
                objSerializer.MaxJsonLength = Int32.MaxValue;

                //jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"ReservationDetails\":[" + jsonString.Trim(',') + "]}";
                SorteddtResult.Dispose();
                objSerializer.MaxJsonLength = Int32.MaxValue;
                return objSerializer.Serialize(new { retCode = 1, Session = 1, List_Sales = List_Sales, Password = Password, });

            }
            catch
            {
                SorteddtResult = dtTable.Clone();
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";

            }
            return jsonString;

        }

        [WebMethod(true)]
        public string GetSalesPersonByID(Int64 Id)
        {
            string jsonString = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

            try
            {
                using (var DB = new dbAdminHelperDataContext())
                {
                    var List = (from obj in DB.tbl_StaffLogins
                                join meta in DB.tbl_Contacts on obj.ContactID equals meta.ContactID
                                //join city in DB.tbl_HCities on obj.CityAllocated equals city.Code

                                where obj.sid == Id
                                select new
                                {
                                    obj.sid,
                                    obj.ContactPerson,
                                    obj.Last_Name,
                                    obj.TerritoryType,
                                    obj.CountryID,
                                    obj.StateID,
                                    obj.CitiesID,
                                    obj.Agents,
                                    obj.Gender,
                                    obj.DOB,
                                    meta.Mobile,
                                    meta.email,
                                    meta.Address,
                                    meta.PinCode,
                                    meta.Code,
                                    meta.ContactID,
                                    state = meta.StateID,
                                    meta.sCountry,

                                    //city.Description,
                                    //city.Country,
                                    //city.Countryname,

                                    //contry.Country,
                                    //contry.Countryname,


                                }).Distinct().ToList();

                  

                    jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List});


                }
            }
            catch (Exception)
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        [WebMethod(true)]
        public string DeleteSalesPerson(Int64 uid, Int64 contactId)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            //DBHandlerDataContext DB = new DBHandlerDataContext();
            string jsonString = "";

            try
            {
                using (var DB = new dbAdminHelperDataContext())
                {
                    tbl_StaffLogin Delete = DB.tbl_StaffLogins.Single(x => x.sid == uid);
                    DB.tbl_StaffLogins.DeleteOnSubmit(Delete);
                    DB.SubmitChanges();
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";

                    //tbl_Contact Condlt = DB.tbl_Contacts.Single(y => y.ContactID == contactId);
                    //DB.tbl_Contacts.DeleteOnSubmit(Condlt);
                    //DB.SubmitChanges();
                    //jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        [WebMethod(true)]
        public string UpdateSalesPerson(Int64 id, Int64 contactID, string sFirstName, string sLastName, string Gender, string nMobile, string sEmail, string DOB, string sAddress, string sCountry, string State, string sCity, string nPinCode, string SalesTerritory, string TerCountry, string TerState, string TerCityCountry, string TerCity, string TerAgency)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            //dbAdminHelperDataContext DB = new dbAdminHelperDataContext();
            string jsonString = "";
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 ParentID = objGlobalDefault.sid;

            Random generator = new Random();
            String Password = generator.Next(0, 999999).ToString("D6");
            Password = Cryptography.EncryptText(Password);
            try
            {
                using (var DB = new dbAdminHelperDataContext())
                {
                    CutAdmin.dbml.tbl_Contact updcon = new CutAdmin.dbml.tbl_Contact();

                    //tbl_Contact updcon = DB.tbl_Contacts.Single(x => x.ContactID == contactID);


                    updcon.Mobile = nMobile;

                    updcon.email = sEmail;
                    updcon.Address = sAddress;
                    updcon.sCountry = sCountry;
                    updcon.StateID = State;
                    updcon.Code = sCity;
                    updcon.PinCode = nPinCode;
                    DB.SubmitChanges();

                    tbl_StaffLogin updSales = DB.tbl_StaffLogins.Single(y => y.sid == id);
                    //string sEncryptedPassword = CUT.Common.Cryptography.EncryptText(updSales.password);
                    updSales.ContactPerson = sFirstName;

                    updSales.Last_Name = sLastName;
                    updSales.Gender = Gender;
                    updSales.DOB = DOB;
                    updSales.TerritoryType = SalesTerritory;
                    //updSales.password = sEncryptedPassword;
                    if (SalesTerritory == "City")
                    {
                        updSales.CountryID = TerCityCountry;
                    }
                    else
                    {
                        updSales.CountryID = TerCountry;
                    }

                    updSales.StateID = TerState;
                    updSales.CitiesID = TerCity;
                    updSales.Agents = TerAgency;

                    updSales.ContactID = contactID;
                    updSales.ParentId = ParentID;

                    DB.SubmitChanges();

                    jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return jsonString;
        }

       
        [WebMethod(EnableSession = true)]
        public string GetCountry()
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var DB = new dbAdminHelperDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

                    var CountryList = (from obj in DB.tbl_HCities
                                        select new
                                        {
                                            obj.Countryname,
                                            obj.Country,
                                        }).ToList().Distinct();

                    return jsSerializer.Serialize(new { Session = 1, retCode = 1, CountryList = CountryList });
                }
            }

            catch (Exception)
            {

                return jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }

        }

        [WebMethod(EnableSession = true)]
        public string GetCity(string country)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var DB = new dbAdminHelperDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

                    var CityList = (from obj in DB.tbl_HCities
                                    where obj.Country == country
                                       select new
                                       {
                                           obj.Description,
                                           obj.Code,
                                       }).ToList().Distinct();

                    return jsSerializer.Serialize(new { Session = 1, retCode = 1, CityList = CityList });
                }
            }

            catch (Exception)
            {

                return jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }

        }
        [WebMethod(EnableSession = true)]
        public string GetState()
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var DB = new dbAdminHelperDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

                    var List = (from obj in DB.tbl_GstStates
                                       select new
                                       {
                                           obj.SateName,
                                           obj.StateID,
                                       }).ToList().Distinct();

                    return jsSerializer.Serialize(new { Session = 1, retCode = 1, List = List });
                }
            }

            catch (Exception)
            {

                return jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }

        }

        [WebMethod(EnableSession = true)]
        public string GetTeritoryCity( string country)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var DB = new dbAdminHelperDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

                    var CityList = (from obj in DB.tbl_HCities
                                    where obj.Country == country
                                    select new
                                    {
                                        obj.Description,
                                        obj.Code,
                                    }).ToList().Distinct();

                    return jsSerializer.Serialize(new { Session = 1, retCode = 1, City = CityList });
                }
            }

            catch (Exception)
            {

                return jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }

        }

        [WebMethod(true)]
        public string GetTeritoryAgency()
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var DB = new dbAdminHelperDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                     var List = (from obj in DB.tbl_AdminLogins
                                    where obj.UserType == "Agent"
                                    select new
                                    {
                                        obj.sid,
                                        obj.AgencyName,
                                    }).ToList().Distinct().OrderBy(data => data.AgencyName);

                  
                    return jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                }
            }

            catch (Exception)
            {

                return jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
        }
        [WebMethod(true)]
        public string CheckForUniqueSales(string sEmail)
        {
            string json="";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var DB = new dbAdminHelperDataContext())
                {

                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    var List = (from obj in DB.tbl_Saleslogins
                                where obj.UID == sEmail
                                select obj);
                    if (List.Count() > 0)
                    {
                         json= jsSerializer.Serialize(new {Session=1, retCode=1});
                    }

                    else if (List.Count()==0)
                    {
                        json= jsSerializer.Serialize(new { Session = 1, retCode = 2 });
                    }
                    return json;
                }
            }

            catch (Exception)
            {

                return jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
           
        }

        [WebMethod(true)]
        public string GetTerritoryDetail(string Territory, string TerritoryType)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            //DBHandlerDataContext DB = new DBHandlerDataContext();
            string jsonString = "";
            string Conutry = "";
            string states = "";
            string Cities = "";
            string Agents = "";

            try
            {
                using (var DB = new dbAdminHelperDataContext())
                {
                    if (true)
                    {
                        if (TerritoryType == "Country")
                        {
                            String[] countries = Territory.Split(',');

                            for (int j = 0; j < countries.Length; j++)
                            {
                                var countrylist = (from obj in DB.tbl_HCities
                                                   where obj.Country == countries[j]
                                                   select new
                                                   {
                                                       obj.Countryname,


                                                   }).Distinct().ToList();

                                Conutry += countrylist[0].Countryname + ",";

                            }


                        }
                        else if (TerritoryType == "State")
                        {
                            String[] state = Territory.Split(',');

                            for (int k = 0; k < state.Length; k++)
                            {
                                var Statelist = (from obj in DB.tbl_GstStates
                                                 where obj.StateID == state[k]
                                                 select new
                                                 {
                                                     obj.SateName,


                                                 }).Distinct().ToList();

                                states += Statelist[0].SateName + ",";
                            }
                        }
                        else if (TerritoryType == "City")
                        {
                            String[] City = Territory.Split(',');

                            for (int l = 0; l < City.Length; l++)
                            {
                                var Statelist = (from obj in DB.tbl_HCities
                                                 where obj.Code == City[l]
                                                 select new
                                                 {
                                                     obj.Description,


                                                 }).Distinct().ToList();

                                Cities += Statelist[0].Description + ",";
                            }


                        }

                        else
                        {
                            String[] Agent = Territory.Split(',');

                            for (int m = 0; m < Agent.Length; m++)
                            {
                                var Statelist = (from obj in DB.tbl_AdminLogins
                                                 where obj.sid.ToString() == Agent[m]
                                                 select new
                                                 {
                                                     obj.AgencyName,


                                                 }).Distinct().ToList();

                                Agents += Statelist[0].AgencyName + ",";
                            }


                        }
                    }
                    jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, TConutry = Conutry, Tstates = states, TCities = Cities, TAgents = Agents });

                }
            }
            catch
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";

            }
            return jsonString;
        }
        [WebMethod(true)]
        public string SalesChangePassword(int sid, string password)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            //DBHandlerDataContext DB = new DBHandlerDataContext();
            string Password = Cryptography.EncryptText(password);
            string jsonString = "";
            try
            {
                using (var DB = new dbAdminHelperDataContext())
                {
                    tbl_StaffLogin Update = DB.tbl_StaffLogins.Single(x => x.sid == sid);
                    Update.password = Password;
                    DB.SubmitChanges();
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";

                }
            }
            catch
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";

            }
            return jsonString;
        }
        [WebMethod(true)]
        public string MailPassword(string sEmail, string sTo)
        {
            string sJsonString = "{\"retCode\":\"0\"}";
            DataSet dsResult;
            DataTable dtFirstResult;
            DataTable dtSecondResult;
            DBHelper.DBReturnCode retCode = DefaultManager.isAgentValid(sEmail, out dsResult);
            dtFirstResult = dsResult.Tables[0];
            dtSecondResult = dsResult.Tables[1];
            if (retCode == DBHelper.DBReturnCode.SUCCESS && dtFirstResult.Rows.Count > 0)
            {
                string sDecryptedPassword = Cryptography.DecryptText(dtFirstResult.Rows[0]["password"].ToString());
                if (DefaultManager.SendEmail(sTo, "Your Password Detail", "Information Received", dtFirstResult.Rows[0]["ContactPerson"].ToString(), sEmail, dtFirstResult.Rows[0]["Mobile"].ToString(), sDecryptedPassword) == true)//info@redhillindia.com
                {
                    sJsonString = "{\"retCode\":\"1\",}";
                }
            }
            if (retCode == DBHelper.DBReturnCode.SUCCESS && dtSecondResult.Rows.Count > 0)
            {
                string sDecryptedPassword = Cryptography.DecryptText(dtSecondResult.Rows[0]["password"].ToString());
                if (DefaultManager.SendEmail(sEmail, "Your Password Detail", "Information Received", dtSecondResult.Rows[0]["ContactPerson"].ToString(), sEmail, dtSecondResult.Rows[0]["Mobile"].ToString(), sDecryptedPassword) == true)//info@redhillindia.com
                {
                    sJsonString = "{\"retCode\":\"1\",}";
                }
            }
            return sJsonString;
        }
        [WebMethod(EnableSession = true)]
        public string GetCountryname(string countrId)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var DB = new dbAdminHelperDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

                    var CountryList = (from obj in DB.tbl_HCities
                                       select new
                                       {
                                           obj.Countryname,
                                           obj.Country,
                                       }).ToList().Distinct();

                    return jsSerializer.Serialize(new { Session = 1, retCode = 1, CountryList = CountryList });
                }
            }

            catch (Exception)
            {

                return jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }

        }
    }
}
