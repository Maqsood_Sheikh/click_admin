﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for ViewPackageHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ViewPackageHandler : System.Web.Services.WebService
    {
        public string GetPackageType(Int64 id)
        {
            if (id == 1)
            {
                return "Holidays Package";
            }
            else if (id == 2)
            {
                return "Umrah Package";
            }
            else if (id == 3)
            {
                return "Hajj Package";
            }
            else if (id == 4)
            {
                return "Honeymoon Package";
            }
            else if (id == 5)
            {
                return "Summer Package";
            }
            else if (id == 6)
            {
                return "Adventure Package";
            }
            else if (id == 7)
            {
                return "Deluxe Package";
            }
            else if (id == 8)
            {
                return "Business Package";
            }
            else if (id == 9)
            {
                return "Premium Package";
            }
            else if (id == 10)
            {
                return "Wildlife Package";
            }
            else if (id == 11)
            {
                return "Weekend Package";

            }
            else if (id == 12)
            {
                return "New Year Package";
            }
            else
            {
                return "No Package";
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetPackageDetails()
        {
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //if (!Utils.ValidateSession())
            if (objGlobalDefaults == null)
            {
                return objSerializer.Serialize(new { Session = 0 });
            }
            DataTable dtResult = new DataTable();
            DBHelper.DBReturnCode retCode = PackageManager.GetPackageDetails(out dtResult); 
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                var List_Packages = dtResult.AsEnumerable()
                    .Select(data => new
                    {
                        nID = data.Field<Int64>("nID"),
                        sPackageName = data.Field<string>("sPackageName"),
                        sPackageDestination = data.Field<string>("sPackageDestination"),
                        sPackageCategory = data.Field<string>("sPackageCategory"),
                        sPackageThemes = data.Field<string>("sPackageThemes"),
                        nDuration = data.Field<Int64>("nDuration"),
                        sPackageDescription = data.Field<string>("sPackageDescription"),
                        dValidityFrom = data.Field<string>("dValidityFrom"),
                        dValidityTo = data.Field<string>("dValidityTo"),
                        dTax = data.Field<Decimal>("dTax"),
                        nCancelDays = data.Field<Int64>("nCancelDays"),
                        dCancelCharge = data.Field<Decimal>("dCancelCharge"),
                        sTermsCondition = data.Field<String>("sTermsCondition")
                    }).ToList();
                return objSerializer.Serialize(new { Session = 1, retCode = 1, List_Packages = List_Packages });
            }
            else
            {
                return objSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
        }
    }
}
