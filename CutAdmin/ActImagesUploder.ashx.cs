﻿using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
namespace CutAdmin
{
    /// <summary>
    /// Summary description for ActImagesUploder
    /// </summary>
    public class ActImagesUploder : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            
            DataManager.DBReturnCode retCode = DataManager.DBReturnCode.EXCEPTION;
            string sid = System.Convert.ToString(context.Request.QueryString["sid"]);
            string No = System.Convert.ToString(context.Request.QueryString["ImagePath"]);
            if (context.Request.Files.Count > 0)
            {
                List<string> Images = No.Split('^').ToList();
                HttpFileCollection files = context.Request.Files;
                for (int i = 0; i < Images.Count-1; i++)
                {

                    string sFolderName = context.Server.MapPath("~/activityImages/" + sid);
                    if (!Directory.Exists(sFolderName))
                    {
                        Directory.CreateDirectory(sFolderName);
                    }
                    else
                    {
                        string[] Files = Directory.GetFiles(sFolderName);
                        foreach (string fileName in Files)
                        {
                            if (fileName.Contains(Images[i]))
                            {
                                continue;
                            }
                        }
                    }
                    HttpPostedFile file = files[i];
                    //string extt = Path.GetExtension(Images[i]);
                    //string myFilePath="";
                    //if (extt ==""||extt==".jpg")
                    //{
                    //     myFilePath = Images[i].Replace(".jpg","") + ".png";
                    //}
                    //else
                    //{
                    //    myFilePath = Images[i];
                    //}



                    string ext = Path.GetExtension(Images[i]);
                    if (ext != ".pdf")
                    {
                        string FileName = No;
                        FileName = Path.Combine(context.Server.MapPath("activityImages/" + sid + '/'), Images[i]);
                        file.SaveAs(FileName);
                        context.Response.Write("1");
                    }
                    else
                    {
                        context.Response.Write("0");

                    }
                }
                retCode = ActivityManager.UpdateImages(No, sid);
            }
            else
            {
                //DBHelper.DBReturnCode retCode = ImageManager.InsertSliderImage(sid, sFileName, facility, service, start, details, Notes, "");
                if (retCode == DataManager.DBReturnCode.SUCCESS)
                {
                    context.Response.Write("1");
                }
                else
                {
                    context.Response.Write("0");
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}