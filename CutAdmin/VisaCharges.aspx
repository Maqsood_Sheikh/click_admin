﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="VisaCharges.aspx.cs" Inherits="CutAdmin.VisaCharges" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/VisaCharges.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">


        <hgroup id="main-title" class="thin">
            <h1>Visa ->  <a  href="VisaDetails.aspx" title="Visa Details">Visa Details</a>-> Visa Charges</h1>
        </hgroup>

        <div class="with-padding">

           <%-- <h4>Visa Service</h4>--%>
            <hr />

            <div class="columns">
                <div class="four-columns four-columns-tablet twelve-columns-mobile">
                    <label>Visa Type:</label>
                    <div class="full-width button-height">
                        <select id="selService" class="select">
                             <option value="-" selected="selected">--Select Visa Type--</option>
                            <option value="6">96hrs VISA (Compulsory Confirm Ticket Copy)</option>
                            <option value="1">14 days Service VISA</option>
                            <option value="2">30 days Tourist Single Entry</option>
                            <option value="3">30 days Tourist Multiple Entry</option>
                            <option value="4">90 days Tourist Single Entry</option>
                            <option value="5">90 days Tourist Multiple Entry</option>
                            <option value="7">90 Days Tourist Single Entry Convertible</option>
                        </select>
                    </div>
                </div>
                <div class="four-columns four-columns-tablet twelve-columns-mobile">
                    <label>Nationality:</label>
                    <div class="full-width button-height">
                        <select id="selVisaCountry" class="select" onchange="GetCharges()" >
                            <option value="-" selected="selected">--Salect Visa Country--</option>
                            <option value="Indian">Indian</option>
                            <option value="Pakistani">Pakistani</option>
                            <option value="Sri Lankan">Sri Lankan</option>
                            <option value="South African">South African</option>
                            <option value="Mozambican">Mozambican</option>
                            <option value="Malaysian">Malaysian</option>
                            <option value="Indonesian">Indonesian</option>
                            <option value="Philippines">Philippines</option>
                        </select>
                    </div>
                </div>
            </div>

            <h4>Visa Charges</h4>
            <hr />

            <div class="columns">
                <div class="four-columns four-columns-tablet twelve-columns-mobile">
                    <label>Currency:</label>
                    <div class="full-width button-height" id="DivCurrencyType">
                        <select id="selCurrencyType"  class="select" onchange="Notification()">
                            <option value="-" selected="selected">--Salect Visa Currency--</option>
                            <option value="INR">INR</option>
                            <option value="AED">AED</option>
                            <option value="SAR">SAR</option>
                            <option value="EUR">EUR</option>
                            <option value="GBP">GBP</option>
                            <option value="USD">USD</option>
                        </select>
                    </div>
                </div>

                <div class="four-columns four-columns-tablet twelve-columns-mobile">
                    <label>Visa Fee*</label>
                    <div class="input full-width">
                        <input type="text" class="input-unstyled full-width" id="txtVisaFee" onkeyup="ADD()" placeholder="0.00"></div>
                </div>
                <div class="four-columns four-columns-tablet twelve-columns-mobile">
                    <label>Urgent Charge*</label>
                    <div class="input full-width" >
                        <input type="text" class="input-unstyled full-width" id="txtOtherFee" onkeyup="ADD()" placeholder="0.00"></div>
                </div>
            </div>

            <p class="text-alignright">
                <button type="button" id="btnAddStaff" onclick="UpdateCharges();" class="button anthracite-gradient UpdateMarkup">Update Charges</button>
                 <button type="button" id="btnAddCharge" onclick="AddCharges();" class="button anthracite-gradient UpdateMarkup" style="display:none">Save Charges</button>
            </p>
        </div>

    </section>
    <!-- End main content -->
</asp:Content>
