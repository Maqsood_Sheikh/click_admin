﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web;
using System.Transactions;
using System.Globalization;
using System.Text;
using System.Transactions.Workflows;
using System.Net.Mail;
using System.Net;

namespace CutAdmin.DataLayer
{
    public class DefaultManager
    {
        #region Variable
        const string SERVER = "EvidentNet";
        const string URL = "http://www.evidentnet.com";
        const string EmailTemplate = "<html><body>" +
                                    "<div align=\"center\">" +
                                    "<table style=\"width:602px;border:silver 1px solid\" cellspacing=\"0\" cellpadding=\"0\">" +
                                    "<tr>" +
                                    "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;color:White; FONT-SIZE: 14px;background-color:Black\"><span>#title#</span></td>" +
                                    "</tr>" +

                                    "<tr>" +
                                    "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\">Name: #name#</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                    "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\">Mobile No: #phone#</td>" +
                                    "</tr>" +

                                    "<tr>" +
                                    "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\">Email: #email#</td>" +
                                    "</tr>" +

                                    "<tr>" +
                                    "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\">Your Password: #text#</td>" +
                                    "</tr>" +

                                    "<tr>" +
                                    "<td width=\"100%\" style=\"vertical-align:middle;text-align:left;padding:20px 15px 20px 15px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\"><b>Thank You</b><br/>Adminstrator</td>" +
                                    "</tr>" +

                                    "<tr>" +
                                    "<td width=\"100%\" style=\"padding:8px 5px 8px 5px;text-align:center;FONT-SIZE: 8pt;color:#007CC2;FONT-FAMILY: Tahoma;background-color:Silver\">  © 2014 - 2015 ClickurTrip.com</td>" +
                                    "</tr>" +

                                    "</table>" +
                                    "</div>" +
                                    "</html></body>";
        #endregion
        public static DBHelper.DBReturnCode UserLogin(string sUserName, string sPassword)
        {

            string sEncryptedPassword = CutAdmin.Common.Cryptography.EncryptText(sPassword);
            HttpContext.Current.Session["LoginUser"] = null;
            DataSet dsResult;
            DataTable dtResult;
            GlobalDefault objGlobalDefault;
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@uid", sUserName);
            sqlParams[1] = new SqlParameter("@password", sEncryptedPassword);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_tbl_AdminLoginLoadByKey", out dsResult, sqlParams);
            DataTable firstTable = dsResult.Tables[0];
            DataTable secondTable = dsResult.Tables[1];
            DataTable thirdTable = dsResult.Tables[2];
            DataTable dtExchangeRate = dsResult.Tables[3];
            string CurrencyClass = "";
            if (DBHelper.DBReturnCode.SUCCESS == retCode)
            {
                if (firstTable.Rows.Count > 0)
                {
                    objGlobalDefault = new GlobalDefault();
                    objGlobalDefault.sid = Convert.ToInt64(firstTable.Rows[0]["sid"]);
                    objGlobalDefault.uid = firstTable.Rows[0]["uid"].ToString();
                    objGlobalDefault.UserType = firstTable.Rows[0]["UserType"].ToString();
                    string DecryptedPassword = CutAdmin.Common.Cryptography.DecryptText(firstTable.Rows[0]["password"].ToString());
                    objGlobalDefault.password = DecryptedPassword;
                    objGlobalDefault.AgencyName = firstTable.Rows[0]["AgencyName"].ToString();
                    objGlobalDefault.AgencyLogo = firstTable.Rows[0]["AgencyLogo"].ToString();
                    objGlobalDefault.AgencyType = firstTable.Rows[0]["AgencyType"].ToString();
                    objGlobalDefault.MerchantID = firstTable.Rows[0]["MerchantID"].ToString();
                    objGlobalDefault.ContactPerson = firstTable.Rows[0]["ContactPerson"].ToString();
                    objGlobalDefault.Last_Name = firstTable.Rows[0]["Last_Name"].ToString();
                    objGlobalDefault.Designation = firstTable.Rows[0]["Designation"].ToString();
                    objGlobalDefault.ContactID = Convert.ToInt64(firstTable.Rows[0]["ContactID"]);
                    if (firstTable.Rows[0]["RoleID"].ToString() != "")
                    {
                        objGlobalDefault.RoleID = Convert.ToInt64(firstTable.Rows[0]["RoleID"]);
                    }
                    objGlobalDefault.Agentuniquecode = firstTable.Rows[0]["Agentuniquecode"].ToString();
                    objGlobalDefault.LoginFlag = Convert.ToBoolean(firstTable.Rows[0]["LoginFlag"]);
                    objGlobalDefault.IsFirstLogIn = Convert.ToBoolean(firstTable.Rows[0]["IsFirstLogIn"]);
                    objGlobalDefault.Currency = firstTable.Rows[0]["CurrencyCode"].ToString();
                    objGlobalDefault.ParentId = Convert.ToInt64(firstTable.Rows[0]["ParentID"]);
                    objGlobalDefault.Franchisee = firstTable.Rows[0]["FranchiseeId"].ToString();
                    objGlobalDefault.SupplierVisible = (bool)firstTable.Rows[0]["SupplierVisible"];
                    CurrencyClass = firstTable.Rows[0]["CurrencyCode"].ToString();
                    if (objGlobalDefault.UserType != "Admin")
                    {
                        objGlobalDefault.agentCategory = firstTable.Rows[0]["agentCategory"].ToString();

                    }
                    if (thirdTable.Rows.Count > 0)
                    {
                        objGlobalDefault.AvailableCrdit = Convert.ToSingle(thirdTable.Rows[0]["AvailableCredit"]);
                        bool CreditLmit = false;
                        DateTime LimitDate = DateTime.Now;
                        DateTime Today = DateTime.Now;
                        // objGlobalDefault.CreditLimit = Convert.ToSingle(thirdTable.Rows[0]["CreditAmount"]);
                        //objGlobalDefault.OTC = Convert.ToSingle(thirdTable.Rows[0]["OTC"]);
                        // objGlobalDefault.AvailableCrdit = Convert.ToSingle(thirdTable.Rows[0]["AvailableCredit"]) + Convert.ToSingle(thirdTable.Rows[0]["CreditAmount"]); 
                        try
                        {
                            CreditLmit = Convert.ToBoolean(thirdTable.Rows[0]["Credit_Flag"]);
                            Int64 limitDays = 0;
                            if (CreditLmit == true)
                            {
                                LimitDate = ConvertDateTime(thirdTable.Rows[0]["Spn_DateLimit"].ToString());
                                limitDays = Convert.ToInt64(thirdTable.Rows[0]["Spn_NoDay"].ToString());
                                LimitDate = LimitDate.AddDays(limitDays);
                                if (LimitDate < Today)
                                {
                                    ResetPriviouslimit(objGlobalDefault.sid);

                                }
                            }
                        }
                        catch
                        {

                        }
                        if (objGlobalDefault.LoginFlag == true && (objGlobalDefault.AvailableCrdit <= 1000) == true)
                        {
                            string Email = objGlobalDefault.uid;
                            float Balance = objGlobalDefault.AvailableCrdit;
                            string sAgencyName = objGlobalDefault.AgencyName;
                            //DefaultManager.LowBalanceEmail(Email, sAgencyName, Balance);

                        }
                    }

                    if (dtExchangeRate.Rows.Count != 0)
                    {
                        List<ExchangeRate> listExchangeRate = new List<ExchangeRate>();
                        for (int i = 0; i < dtExchangeRate.Rows.Count; i++)
                        {

                            ExchangeRate objExchangeRate = new ExchangeRate();
                            objExchangeRate.Currency = Convert.ToString(dtExchangeRate.Rows[i]["Currency"]);
                            objExchangeRate.Rate = Convert.ToSingle(dtExchangeRate.Rows[i]["INR"]);
                            objExchangeRate.LastUpdateDt = Convert.ToString(dtExchangeRate.Rows[i]["LastUpdateDt"]);
                            objExchangeRate.MailFlag = Convert.ToBoolean(dtExchangeRate.Rows[i]["MailFlag"]);
                            //objExchangeRate.Status = Convert.ToBoolean(dtExchangeRate.Rows[i]["Status"]);
                            listExchangeRate.Add(objExchangeRate);
                        }
                        //List<ExchangeRate> listExchangeRateForEmail = new List<ExchangeRate>();
                        //for (int i = 0; i < listExchangeRate.Count; i++)
                        //{
                        //    if (DateTime.ParseExact(listExchangeRate[i].LastUpdateDt, "dd-MM-yyyy hh:mm", CultureInfo.InvariantCulture).AddHours(48) < DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy hh:mm"), "dd-MM-yyyy hh:mm", CultureInfo.InvariantCulture) && listExchangeRate[i].MailFlag == true)
                        //    {
                        //        listExchangeRateForEmail.Add(listExchangeRate[i]);
                        //    }
                        //    //else if (DateTime.ParseExact(listExchangeRate[i].LastUpdateDt, "dd-MM-yyyy hh:mm", CultureInfo.InvariantCulture).AddHours(96) < DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy hh:mm"), "dd-MM-yyyy hh:mm", CultureInfo.InvariantCulture) && listExchangeRate[i].MailFlag == false)
                        //    //{
                        //    //    int row = 0;
                        //    //    SqlParameter[] SQLParams = new SqlParameter[2];
                        //    //    SQLParams[0] = new SqlParameter("@Currency", listExchangeRate[i].Currency);
                        //    //    SQLParams[1] = new SqlParameter("@Status", "1");
                        //    //    retCode = DBHelper.ExecuteNonQuery("Proc_tbl_ExChangeUpdateMailFlag", out row, SQLParams);
                        //    //}
                        //}
                        //if (listExchangeRateForEmail.Count > 0)
                        //{
                        //    retCode = EmailManager.ExchangeRateNotificationEmail(listExchangeRateForEmail);
                        //    if (DBHelper.DBReturnCode.SUCCESS == retCode)
                        //    {
                        //        int row = 0;
                        //        for (int i = 0; i < listExchangeRateForEmail.Count; i++)
                        //        {
                        //            SqlParameter[] SQLParams = new SqlParameter[2];
                        //            SQLParams[0] = new SqlParameter("@Currency", listExchangeRateForEmail[i].Currency);
                        //            SQLParams[1] = new SqlParameter("@Status", "0");
                        //            retCode = DBHelper.ExecuteNonQuery("Proc_tbl_ExChangeUpdateMailFlag", out row, SQLParams);
                        //        }
                        //    }
                        //}

                        objGlobalDefault.ExchangeRate = listExchangeRate;
                    }
                    // ... Switch on the string.
                    switch (CurrencyClass)
                    {
                        case "AED":
                            CurrencyClass = "Currency-AED";

                            break;
                        case "SAR":
                            CurrencyClass = "Currency-SAR";
                            break;
                        case "EUR":
                            CurrencyClass = "fa fa-eur";
                            break;
                        case "GBP":
                            CurrencyClass = "fa fa-gbp";
                            break;
                        case "USD":
                            CurrencyClass = "fa fa-dollar";
                            break;
                        case "INR":
                            CurrencyClass = "fa fa-inr";
                            break;
                    }
                    HttpContext.Current.Session["CurrencyClass"] = CurrencyClass;
                    HttpContext.Current.Session["LoginUser"] = objGlobalDefault;

                    if (objGlobalDefault.UserType == "Admin")
                    {
                        GetAuthorizedFormList();
                    }
                    else if (objGlobalDefault.UserType == "Franchisee")
                    {
                        GetAuthorizedFranchiseeFormList();
                    }
                    else if (objGlobalDefault.UserType == "Agent")
                    {
                        GetAgentFormList();
                    }
                }
                else if (secondTable.Rows.Count > 0)
                {
                    objGlobalDefault = new GlobalDefault();
                    objGlobalDefault.sid = Convert.ToInt64(secondTable.Rows[0]["sid"]);
                    objGlobalDefault.uid = secondTable.Rows[0]["uid"].ToString();
                    objGlobalDefault.UserType = secondTable.Rows[0]["UserType"].ToString();
                    string DecryptedPassword = CutAdmin.Common.Cryptography.DecryptText(secondTable.Rows[0]["password"].ToString());
                    objGlobalDefault.password = DecryptedPassword;
                    objGlobalDefault.ContactPerson = secondTable.Rows[0]["ContactPerson"].ToString();
                    objGlobalDefault.Last_Name = secondTable.Rows[0]["Last_Name"].ToString();
                    objGlobalDefault.Designation = secondTable.Rows[0]["Designation"].ToString();
                    objGlobalDefault.ContactID = Convert.ToInt64(secondTable.Rows[0]["ContactID"]);
                    if (secondTable.Rows[0]["RoleID"].ToString() != "")
                        objGlobalDefault.RoleID = Convert.ToInt64(secondTable.Rows[0]["RoleID"]);
                    objGlobalDefault.Staffuniquecode = secondTable.Rows[0]["StaffUniquecode"].ToString();
                    objGlobalDefault.LoginFlag = Convert.ToBoolean(secondTable.Rows[0]["LoginFlag"]);
                    objGlobalDefault.ParentId = Convert.ToInt64(secondTable.Rows[0]["ParentID"]);
                    SqlParameter[] sqlParams1 = new SqlParameter[1];
                    sqlParams1[0] = new SqlParameter("@sId", objGlobalDefault.ParentId);
                    DBHelper.DBReturnCode retCode2 = DBHelper.GetDataTable("Proc_tbl_AdminLoginLoadAllById", out dtResult, sqlParams1);
                    objGlobalDefault.AgencyName = dtResult.Rows[0]["AgencyName"].ToString();
                    objGlobalDefault.AgencyType = dtResult.Rows[0]["AgencyType"].ToString();
                    objGlobalDefault.Agentuniquecode = dtResult.Rows[0]["Agentuniquecode"].ToString();
                    HttpContext.Current.Session["LoginUser"] = objGlobalDefault;
                    if (objGlobalDefault.UserType == "AdminStaff")
                    {
                        GetAuthorizedFormFoAdminStaff();
                    }
                    else if (objGlobalDefault.UserType == "FranchiseeStaff")
                    {
                        GetAuthorizedFormFoFranchiseeStaff();
                    }
                }
                else
                    retCode = DBHelper.DBReturnCode.EXCEPTION;
            }
            return retCode;
        }

        #region ResetAvaillimit
        public static DateTime ConvertDateTime(string Date)
        {
            DateTime date = new DateTime();
            try
            {
                string CurrentPattern = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
                string[] Split = new string[] { "-", "/", @"\", "." };
                string[] Patternvalue = CurrentPattern.Split(Split, StringSplitOptions.None);
                string[] DateSplit = Date.Split(Split, StringSplitOptions.None);
                string NewDate = "";
                if (Patternvalue[0].ToLower().Contains("d") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[0] + "/" + DateSplit[1] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("m") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[0] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("d") == true)
                {
                    NewDate = DateSplit[2] + "/" + DateSplit[1] + "/" + DateSplit[0];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("m") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[2] + "/" + DateSplit[0];
                }
                date = DateTime.Parse(NewDate, Thread.CurrentThread.CurrentCulture);
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

            return date;

        }
        public static DBHelper.DBReturnCode ResetPriviouslimit(Int64 Uid)
        {
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            int Rowseffected = 0;
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@uid", Uid);
            sqlParams[1] = new SqlParameter("@LastUpdatedDate", DateTime.Now.ToString("dd-mm-yyyy"));
            retCode = DBHelper.ExecuteNonQuery("Proc_tbl_AdminCreditLimitUpdateByCreditLimit", out Rowseffected, sqlParams);
            return retCode;

        }
        #endregion ResetAvaillimit

        public static DBHelper.DBReturnCode GetMarkupsAndTaxes(out DataSet ds)
        {
            //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //SqlParameter[] sqlParams = new SqlParameter[1];
            //sqlParams[0] = new SqlParameter("@uid", objGlobalDefault.sid);
            //DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_GetMarkupsAndTaxes", out ds, sqlParams);
            //return retCode;
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@AgentId", 226);
            sqlParams[1] = new SqlParameter("@Supplier", "HotelBeds");
            //DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_GetAllAgentMarkpDetailsByAgentId", out ds, sqlParams);   //temporarily commented for test purpose
            DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_GetAgentMarkpDetailsByTravel", out ds, sqlParams);
            return retCode;
        }

        private static void GetAuthorizedFormList()
        {
            if (HttpContext.Current.Session["LoginUser"] != null)
            {
                DataTable dtResult;
                Forms objForms = new Forms();
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                SqlParameter[] sqlParams = new SqlParameter[1];
                sqlParams[0] = new SqlParameter("@RoleId", objGlobalDefault.RoleID);
                DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_RoleManagerGetFormList", out dtResult, sqlParams);
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {
                    objForms.strAuthorizedFormCollection = new string[dtResult.Rows.Count];
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        objForms.strAuthorizedFormCollection[i] = dtResult.Rows[i]["sFormName"].ToString();
                    }
                    HttpContext.Current.Session["AthorizedFormList"] = objForms;
                }
            }
        }

        private static void GetAuthorizedFranchiseeFormList()
        {
            if (HttpContext.Current.Session["LoginUser"] != null)
            {
                DataTable dtResult;
                Forms objForms = new Forms();
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                SqlParameter[] sqlParams = new SqlParameter[2];
                sqlParams[0] = new SqlParameter("@sid", objGlobalDefault.sid);
                sqlParams[1] = new SqlParameter("@ParentID", objGlobalDefault.ParentId);
                DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_RoleManagerGetFranchiseeFormList", out dtResult, sqlParams);
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {
                    objForms.strAuthorizedFormCollection = new string[dtResult.Rows.Count];
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        objForms.strAuthorizedFormCollection[i] = dtResult.Rows[i]["sFormName"].ToString();
                    }
                    HttpContext.Current.Session["AthorizedFormList"] = objForms;
                }
            }
        }

        private static void GetAuthorizedFormFoAdminStaff()
        {
            if (HttpContext.Current.Session["LoginUser"] != null)
            {
                DataTable dtResult;
                Forms objForms = new Forms();
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                SqlParameter[] sqlParams = new SqlParameter[1];
                sqlParams[0] = new SqlParameter("@nUid", objGlobalDefault.sid);
                DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_tblStaffRoleManagerGetFormList", out dtResult, sqlParams);
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {
                    objForms.strAuthorizedFormCollection = new string[dtResult.Rows.Count];
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        objForms.strAuthorizedFormCollection[i] = dtResult.Rows[i]["sFormName"].ToString();
                    }
                    HttpContext.Current.Session["AthorizedFormList"] = objForms;
                }
            }
        }

        private static void GetAuthorizedFormFoFranchiseeStaff()
        {
            if (HttpContext.Current.Session["LoginUser"] != null)
            {
                DataTable dtResult;
                Forms objForms = new Forms();
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                SqlParameter[] sqlParams = new SqlParameter[1];
                sqlParams[0] = new SqlParameter("@nUid", objGlobalDefault.sid);
                DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_StaffFranchiseeRoleManagerGetFormList", out dtResult, sqlParams);
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {
                    objForms.strAuthorizedFormCollection = new string[dtResult.Rows.Count];
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        objForms.strAuthorizedFormCollection[i] = dtResult.Rows[i]["sFormName"].ToString();
                    }
                    HttpContext.Current.Session["AthorizedFormList"] = objForms;
                }
            }
        }

        private static void GetAgentFormList()
        {
            if (HttpContext.Current.Session["LoginUser"] != null)
            {
                DataTable dtResult;
                Forms objForms = new Forms();
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                SqlParameter[] sqlParams = new SqlParameter[1];
                sqlParams[0] = new SqlParameter("@nUid", objGlobalDefault.sid);
                DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_AgentRoleManagerGetFormList", out dtResult, sqlParams);
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {
                    objForms.strAuthorizedFormCollection = new string[dtResult.Rows.Count];
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        objForms.strAuthorizedFormCollection[i] = dtResult.Rows[i]["sFormName"].ToString();
                    }
                    HttpContext.Current.Session["AthorizedFormList"] = objForms;
                }
            }
        }
        public static DBHelper.DBReturnCode isAgentValid(string sEmail, out DataSet dsResult)
        {
            dsResult = null;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@email", sEmail);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_tbl_AgentDetailForgetPassword", out dsResult, sqlParams);
            return retCode;
        }



        public static DBHelper.DBReturnCode CheckForUniqueAgent(string sEmail, out DataSet dsResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@uid", sEmail);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_tbl_AdminLoginCheckUser", out dsResult, sqlParams);
            //DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_AdminLoginCheckUser", out dtResult, sqlParams);
            return retCode;
        }


        public static DBHelper.DBReturnCode GetAgentDetails(string sUserName, out DataTable ds)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@uid", sUserName);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_AdminLoginTEMPDetailsLoadByKey", out ds, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode RegisterUser(string sAgencyName, string sEmail, string sFirstName, string sLastName, string sDesignation, string sAddress, string sCity, string nPinCode, string nIATA, string nPAN, string nPhone, string nMobile, string nFax, string sServiceTaxNumber, string sWebsite, string sReferral, string sCurrency, string sRemarks)
        {
            DataTable dtResult;
            DataSet dsResult;
            int rowsAffected = 0;
            int sid = 0;
            string sUserType = "Agent";
            string AgencyLogoPath = "";
            string AgencyType = "A";
            string sPassword = GenerateRandomString(8);
            string sEncryptedPassword = CutAdmin.Common.Cryptography.EncryptText(sPassword);
            int RoleId = 2;
            string MerchantID = "CUT-" + GenerateRandomString(4);
            DateTime Validity = DateTime.Now.AddYears(1);
            DateTime LastAccess = DateTime.Now;
            string ValidationCode = "VC-" + GenerateRandomNumber();
            string LoginFlag = "0";
            string ReferenceNumber = "";
            string Donecarduser = "";
            int TDSFlag = 0;
            int EnableDeposite = 1;
            int EnableCard = 0;
            int EnableCheckAccount = 1;
            string AgentUniqueCode = "AG-" + GenerateRandomString(4);
            DateTime? UpdateDate = null;
            string FailedReports = "";
            string BookingDate = "";
            string Bankdetails = "";
            string DistAgencyName = "";
            string agentCategory = "Default- Visa";
            Int64 ParentID = 127;
            using (TransactionScope objTransactionScope = new TransactionScope())
            {
                SqlParameter[] sqlParams = new SqlParameter[45];
                sqlParams[0] = new SqlParameter("@uid", sEmail);
                sqlParams[1] = new SqlParameter("@UserType", sUserType);
                sqlParams[2] = new SqlParameter("@password", sEncryptedPassword);
                sqlParams[3] = new SqlParameter("@AgencyName", sAgencyName);
                sqlParams[4] = new SqlParameter("@AgencyLogo", AgencyLogoPath);
                sqlParams[5] = new SqlParameter("@AgencyType", AgencyType);
                sqlParams[6] = new SqlParameter("@MerchantID", MerchantID);
                sqlParams[7] = new SqlParameter("@ContactPerson", sFirstName + " " + sLastName);
                sqlParams[8] = new SqlParameter("@Last_Name", sLastName);
                sqlParams[9] = new SqlParameter("@Designation", sDesignation);
                sqlParams[10] = new SqlParameter("@phone", nPhone);
                sqlParams[11] = new SqlParameter("@Mobile", nMobile);
                sqlParams[12] = new SqlParameter("@Fax", nFax);
                sqlParams[13] = new SqlParameter("@email", sEmail);
                sqlParams[14] = new SqlParameter("@Website", sWebsite);
                sqlParams[15] = new SqlParameter("@Address", sAddress);
                sqlParams[16] = new SqlParameter("@Code", sCity);
                sqlParams[17] = new SqlParameter("@PinCode", nPinCode);
                sqlParams[18] = new SqlParameter("@Validity", Validity);
                sqlParams[19] = new SqlParameter("@dtLastAccess", LastAccess);
                sqlParams[20] = new SqlParameter("@Remarks", sRemarks);
                sqlParams[21] = new SqlParameter("@ValidationCode", ValidationCode);
                sqlParams[22] = new SqlParameter("@LoginFlag", LoginFlag);
                sqlParams[23] = new SqlParameter("@RefAgency", ReferenceNumber);
                sqlParams[24] = new SqlParameter("@PANNo", nPAN);
                sqlParams[25] = new SqlParameter("@ServiceTaxNO", sServiceTaxNumber);
                sqlParams[26] = new SqlParameter("@Donecarduser", Donecarduser);
                sqlParams[27] = new SqlParameter("@MerchantURL", sWebsite);
                sqlParams[28] = new SqlParameter("@TDSFlag", TDSFlag);
                sqlParams[29] = new SqlParameter("@EnableDeposit", EnableDeposite);
                sqlParams[30] = new SqlParameter("@EnableCard", EnableCard);
                sqlParams[31] = new SqlParameter("@EnableCheckAccount", EnableCheckAccount);
                sqlParams[32] = new SqlParameter("@Agentuniquecode", AgentUniqueCode);
                sqlParams[33] = new SqlParameter("@Updatedate", UpdateDate);
                sqlParams[34] = new SqlParameter("@FailedReports", FailedReports);
                sqlParams[35] = new SqlParameter("@BookingDate", BookingDate);
                sqlParams[36] = new SqlParameter("@Bankdetails", Bankdetails);
                sqlParams[37] = new SqlParameter("@IATANumber", nIATA);
                sqlParams[38] = new SqlParameter("@DistAgencyName", DistAgencyName);
                sqlParams[39] = new SqlParameter("@agentCategory", agentCategory);
                sqlParams[40] = new SqlParameter("@sid", sid);
                sqlParams[41] = new SqlParameter("@RoleID", RoleId);
                sqlParams[42] = new SqlParameter("@CurrencyCode", sCurrency);
                sqlParams[43] = new SqlParameter("@Referral", sReferral);
                sqlParams[44] = new SqlParameter("@ParentID", ParentID);
                DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_AdminLoginAddUpdate", out rowsAffected, sqlParams);
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {
                    string UniqueCodeUpdate = "";
                    //dtResult = new DataTable();                   
                    SqlParameter[] sqlParamsUniqueCode = new SqlParameter[1];
                    sqlParamsUniqueCode[0] = new SqlParameter("@Agentuniquecode", AgentUniqueCode);
                    retCode = DBHelper.GetDataTable("Proc_tbl_AdminLoginLoadsid", out dtResult, sqlParamsUniqueCode);
                    if (retCode == DBHelper.DBReturnCode.SUCCESS)
                    {
                        Int64 nReceivedSid = Convert.ToInt64(dtResult.Rows[0]["sid"]);
                        Int64 UniqueCodeLength = nReceivedSid.ToString().Length;

                        if (UniqueCodeLength <= 4)
                        {
                            UniqueCodeUpdate = "AG-" + nReceivedSid.ToString("D" + 4);
                        }
                        else
                            UniqueCodeUpdate = "AG-" + nReceivedSid.ToString("D" + 6);
                        SqlParameter[] sqlParamsCodeUpdate = new SqlParameter[2];
                        sqlParamsCodeUpdate[0] = new SqlParameter("@sid", nReceivedSid);
                        sqlParamsCodeUpdate[1] = new SqlParameter("@Agentuniquecode", UniqueCodeUpdate);
                        retCode = DBHelper.ExecuteNonQuery("Proc_tbl_AdminLoginUpdateUniqueCode", out rowsAffected, sqlParamsCodeUpdate);
                        if (retCode == DBHelper.DBReturnCode.SUCCESS)
                        {
                            //    decimal MarkupPer = Convert.ToDecimal(dtResult.Rows[0]["AgentDefaultMarkup"]);
                            SqlParameter[] sqlParamsGrpName = new SqlParameter[2];
                            sqlParamsGrpName[0] = new SqlParameter("@sid", nReceivedSid);
                            sqlParamsGrpName[1] = new SqlParameter("@agentCategory", "Default");
                            retCode = DBHelper.ExecuteNonQuery("Proc_tbl_AdminLoginUpdateGroup", out rowsAffected, sqlParamsGrpName);
                            if (retCode == DBHelper.DBReturnCode.SUCCESS)
                            {
                                SqlParameter[] sqlParamsGroup = new SqlParameter[3];

                                sqlParamsGroup[0] = new SqlParameter("@AgentId", nReceivedSid);
                                sqlParamsGroup[1] = new SqlParameter("@UpadteBy", nReceivedSid);
                                sqlParamsGroup[2] = new SqlParameter("@GroupId", 5);

                                retCode = DBHelper.ExecuteNonQuery("Proc_tbl_MarkupGroupMappingNewAdd", out rowsAffected, sqlParamsGroup);
                                //if (retCode == DBHelper.DBReturnCode.SUCCESS)
                                //{
                                //    SqlParameter[] sqlParamsVisaGroup = new SqlParameter[8];
                                //    sqlParamsVisaGroup[0] = new SqlParameter("@GroupName", "Visa");
                                //    sqlParamsVisaGroup[1] = new SqlParameter("@uid", nReceivedSid);
                                //    sqlParamsVisaGroup[2] = new SqlParameter("@UpdateDate", DateTime.Now);
                                //    sqlParamsVisaGroup[3] = new SqlParameter("@UpdatedBy", nReceivedSid);
                                //    sqlParamsVisaGroup[4] = new SqlParameter("@GroupId", 34);
                                //    sqlParamsVisaGroup[5] = new SqlParameter("@RoleId", RoleId);
                                //    sqlParamsVisaGroup[6] = new SqlParameter("@UserType", sUserType);
                                //    sqlParamsVisaGroup[7] = new SqlParameter("@Supplier", "ClickUrTrip");
                                //    retCode = DBHelper.ExecuteNonQuery("Proc_tbl_MarkupGroupMappingAddVisaGroup", out rowsAffected, sqlParamsVisaGroup);
                                //if (retCode == DBHelper.DBReturnCode.SUCCESS)
                                //{
                                //    //....
                                //    SqlParameter[] sqlParamsMrkUpStfDefault = new SqlParameter[8];
                                //    sqlParamsMrkUpStfDefault[0] = new SqlParameter("@sid", sid);
                                //    sqlParamsMrkUpStfDefault[1] = new SqlParameter("@GroupName", "Default");
                                //    sqlParamsMrkUpStfDefault[2] = new SqlParameter("@Supplier", "HotelBeds");
                                //    sqlParamsMrkUpStfDefault[3] = new SqlParameter("@MarkupPercentage", 0);
                                //    sqlParamsMrkUpStfDefault[4] = new SqlParameter("@MarkupAmount", 0);
                                //    sqlParamsMrkUpStfDefault[5] = new SqlParameter("@CommisionPercentage", 0);
                                //    sqlParamsMrkUpStfDefault[6] = new SqlParameter("@CommisionAmount", 0);
                                //    sqlParamsMrkUpStfDefault[7] = new SqlParameter("@ParentId", nReceivedSid);
                                //    retCode = DBHelper.ExecuteNonQuery("Proc_tbl_MarkUpGroupDetailsStaffAddUpdate", out rowsAffected, sqlParamsMrkUpStfDefault);
                                //....
                                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                                {
                                    SqlParameter[] sqlParamsCreditLimit = new SqlParameter[2];
                                    sqlParamsCreditLimit[0] = new SqlParameter("@uid", nReceivedSid);
                                    sqlParamsCreditLimit[1] = new SqlParameter("@LastUpdateDate", DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
                                    retCode = DBHelper.ExecuteNonQuery("Proc_tbl_AdminCreditLimitAdd", out rowsAffected, sqlParamsCreditLimit);
                                    if (retCode == DBHelper.DBReturnCode.SUCCESS)
                                    {
                                        decimal MarkupPer = 0;
                                        SqlParameter[] sqlParamsMarkupPer = new SqlParameter[2];
                                        sqlParamsMarkupPer[0] = new SqlParameter("@uid", nReceivedSid);
                                        sqlParamsMarkupPer[1] = new SqlParameter("@MarkupPer", MarkupPer);
                                        retCode = DBHelper.ExecuteNonQuery("Proc_tbl_MarkUpAgentAddUpdate", out rowsAffected, sqlParamsMarkupPer);
                                        if (retCode == DBHelper.DBReturnCode.SUCCESS)
                                        {
                                            DefaultManager.isAgentValid(sEmail, out dsResult);
                                            dtResult = dsResult.Tables[0];
                                            HttpContext.Current.Session["AgentUniqueCode"] = UniqueCodeUpdate;
                                            DefaultManager.VerificationEmail(sEmail, "Account Verification", "Information Received", dtResult.Rows[0]["ContactPerson"].ToString(), sEmail + "$" + UniqueCodeUpdate, dtResult.Rows[0]["Mobile"].ToString(), sPassword, sAgencyName);

                                            objTransactionScope.Complete();
                                            DefaultManager.SendEmailAdmin(sAgencyName, UniqueCodeUpdate, nMobile, sEmail);

                                        }
                                        else
                                        {
                                            objTransactionScope.Dispose();
                                            return DBHelper.DBReturnCode.EXCEPTION;
                                        }
                                    }
                                    else
                                    {
                                        objTransactionScope.Dispose();
                                        return DBHelper.DBReturnCode.EXCEPTION;
                                    }

                                }
                                else
                                {
                                    objTransactionScope.Dispose();
                                    return DBHelper.DBReturnCode.EXCEPTION;
                                }
                                //}
                                //else
                                //{
                                //    objTransactionScope.Dispose();
                                //    return DBHelper.DBReturnCode.EXCEPTION;
                                //}
                                //}

                            }
                            else
                            {
                                objTransactionScope.Dispose();
                                return DBHelper.DBReturnCode.EXCEPTION;
                            }
                        }
                        //.....
                        else
                        {
                            objTransactionScope.Dispose();
                            return DBHelper.DBReturnCode.EXCEPTION;
                        }
                    }
                    //.....
                    else
                    {
                        objTransactionScope.Dispose();
                        return DBHelper.DBReturnCode.EXCEPTION;
                    }
                }
                else
                {
                    objTransactionScope.Dispose();
                    return DBHelper.DBReturnCode.EXCEPTION;
                }
                //return retCode;
                return DBHelper.DBReturnCode.SUCCESS;
                //if (retCode == DBHelper.DBReturnCode.SUCCESS)
                //{
                //    HttpContext.Current.Session["AgentUniqueCode"] = AgentUniqueCode;
                //}
                //return retCode;
            }
        }

        public static DBHelper.DBReturnCode VerifyAccount(string Email)
        {
            string[] spliter = Email.Split('$');
            string AgentEmail = spliter[0];
            string sAgentCode = spliter[1];
            int rows = 0;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Email", AgentEmail);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_AdminLoginVerifyAccount", out rows, sqlParams);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                DataTable dtResult;
                SqlParameter[] sqlParams1 = new SqlParameter[1];
                sqlParams[0] = new SqlParameter("@sAgentCode", sAgentCode);
                DBHelper.DBReturnCode retCode1 = DBHelper.GetDataTable("Proc_GetAgentMailDetails", out dtResult, sqlParams);
                string sAgencyName = (dtResult.Rows[0]["AgencyName"]).ToString();
                string UniqueCodeUpdate = sAgentCode;
                string nMobile = (dtResult.Rows[0]["Mobile"]).ToString();
                string sEmail = (dtResult.Rows[0]["uid"]).ToString();
                string sPassword = Common.Cryptography.DecryptText((dtResult.Rows[0]["password"]).ToString());
                CutAdmin.DataLayer.DefaultManager.SendEmail(sEmail, "Your Password Detail", "Information Received", dtResult.Rows[0]["ContactPerson"].ToString(), sEmail, dtResult.Rows[0]["Mobile"].ToString(), sPassword);
            }



            return retCode;
        }
        public static bool SendEmail(string sTo, string sSubject, string sMessageTitle, string sName, string sEmail, string sPhone, string sMessage)
        {
            //Work start
            StringBuilder sb = new StringBuilder();
            sb.Append("<!DOCTYPE html>");
            sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
            sb.Append("<head>");
            sb.Append("<meta charset=\"utf-8\" />");
            sb.Append("</head>");
            sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">");
            //sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
            //sb.Append("</div>");
            sb.Append("<div style=\"height:50px;width:auto\">");
            sb.Append("<br />");
            //sb.Append("<img src=\"http://www.clickurtrip.com/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" />");
            sb.Append("</div>");
            //sb.Append("<img src=\"http://www.clickurtrip.com/unnamed.jpg\" style=\"padding-left:-2%;width:100%;height:auto\" />");
            sb.Append("<div>");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Hello " + sName + ",</span><br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Your Username is : <b>" + sEmail + "</b> and Password is : <b>" + sMessage + "</b></span><br />");
            sb.Append("<br />");
            sb.Append("<br />");
            sb.Append("<span style=\"margin-left:2%\">");
            sb.Append("<b>Thank You,</b><br />");
            sb.Append("</span>");
            sb.Append("<span style=\"margin-left:2%\">");
            sb.Append("Administrator");
            sb.Append("</span>");
            sb.Append("</div>");
            //sb.Append("<img src=\"http://www.clickurtrip.com/images/unnamed.jpg\" style=\"width:100%;height:auto\" />");
            sb.Append("<div>");
            sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
            sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
            sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">ClickurTrip.com</div></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</div>");
            //sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
            //sb.Append("</div>");
            sb.Append("</body>");
            sb.Append("</html>");

            sb.ToString();

            try
            {

                int rowsAffected = 0;
                SqlParameter[] sqlParamsGrpName = new SqlParameter[3];
                sqlParamsGrpName[0] = new SqlParameter("@sTo", sTo);
                sqlParamsGrpName[1] = new SqlParameter("@sSubject", "Your Password Detail");
                sqlParamsGrpName[2] = new SqlParameter("@VarBody", sb.ToString());
                DBHelper.DBReturnCode RetEmail = DBHelper.ExecuteNonQuery("Proc_RegistrationMail", out rowsAffected, sqlParamsGrpName);
                //string sMailMessage = EmailTemplate.Replace("#title#", sMessageTitle);
                //sMailMessage = sMailMessage.Replace("#name#", sName);
                //sMailMessage = sMailMessage.Replace("#CompanyName#", sCompanyName);
                //sMailMessage = sMailMessage.Replace("#CompanyAddress#", sCompanyAddress);
                //sMailMessage = sMailMessage.Replace("#City#", sCity);
                //sMailMessage = sMailMessage.Replace("#State#", sState);
                //sMailMessage = sMailMessage.Replace("#Desgn#", sDesgn);
                //sMailMessage = sMailMessage.Replace("#email#", sEmail);
                //sMailMessage = sMailMessage.Replace("#phone#", sPhone);
                //sMailMessage = sMailMessage.Replace("#selectoption#", sSelectProducts);
                //sMailMessage = sMailMessage.Replace("#text#", sMessage);
                //System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                //mailMsg.From = new MailAddress("support@royalsheets.asia");
                //mailMsg.To.Add(sTo);
                //mailMsg.Subject = sSubject;
                //mailMsg.IsBodyHtml = true;
                //mailMsg.Body = sb.ToString(); ;
                //SmtpClient mailObj = new SmtpClient("smtp.gmail.com", 587);
                //mailObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["EmailID"], System.Configuration.ConfigurationManager.AppSettings["PassWord"]);
                //mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                //mailObj.EnableSsl = true;
                //mailObj.Send(mailMsg);
                //mailMsg.Dispose();
                return true;

            }
            catch
            {
                return false;
            }
        }
        public static bool VerificationEmail(string sTo, string sSubject, string sMessageTitle, string sName, string sEmail, string sPhone, string sMessage, string sAgencyName)
        {
            //Work start
            string Url = "http://www.clickurtrip.com/login.aspx?Email=" + sEmail;
            StringBuilder sb = new StringBuilder();
            sb.Append("<!DOCTYPE html>");
            sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
            sb.Append("<head>");
            sb.Append("<meta charset=\"utf-8\" />");
            sb.Append("</head>");
            sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">");
            //sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
            //sb.Append("</div>");
            sb.Append("<div id=\"yui_3_16_0_ym19_1_1489469507252_52741\"><div class=\"yiv6464921239WordSection1\" id=\"yui_3_16_0_ym19_1_1489469507252_52740\">");
            sb.Append("<p class=\"yiv6464921239MsoNormal\" style=\"margin-right:30.0pt;\" id=\"yui_3_16_0_ym19_1_1489469507252_52778\">");
            sb.Append("<span lang=\"EN\" style=\"font-family:&quot;Segoe UI&quot;, sans-serif;\" id=\"yui_3_16_0_ym19_1_1489469507252_52777\">Hello " + sName + " " + ",</span>");
            sb.Append("</p>");
            sb.Append("");
            sb.Append("<p class=\"yiv6464921239MsoNormal\" style=\"margin-right:30.0pt;\" id=\"yui_3_16_0_ym19_1_1489469507252_52775\">");
            sb.Append("<span lang=\"EN\" style=\"font-family:&quot;Segoe UI&quot;, sans-serif;\" id=\"yui_3_16_0_ym19_1_1489469507252_52828\">Welcome to ");
            sb.Append("<b id=\"yui_3_16_0_ym19_1_1489469507252_52827\">ClickUrTrip</b></span></p>");
            sb.Append("");
            sb.Append("<p class=\"yiv6464921239MsoNormal\" style=\"margin-right:30.0pt;\" id=\"yui_3_16_0_ym19_1_1489469507252_52854\"><span lang=\"EN\" style=\"font-family:&quot;Segoe UI&quot;, sans-serif;\" id=\"yui_3_16_0_ym19_1_1489469507252_52853\">");
            sb.Append("We have received your registration request as a  " + " " + sAgencyName + " </span></p><p class=\"yiv6464921239MsoNormal\" id=\"yui_3_16_0_ym19_1_1489469507252_52823\">");
            sb.Append("<span lang=\"EN\" style=\"font-family:&quot;Segoe UI&quot;, sans-serif;\">");
            sb.Append("To complete registration, kindly <b><u><a href=\"" + Url + "\" target=\"_blank\">click here</a></u></b>, you will receive your temporary password on next mail</span>");
            sb.Append("</p>");
            sb.Append("");
            sb.Append("<p class=\"yiv6464921239MsoNormal\"><b><span lang=\"EN\" style=\"font-family:&quot;Segoe UI&quot;, sans-serif;\">Thanking You,</span>");
            sb.Append("</b><span lang=\"EN\" style=\"font-family:&quot;Segoe UI&quot;, sans-serif;\"><br>Administrator</span></p>");
            sb.Append("<p class=\"yiv6464921239MsoNormal\"><b><span lang=\"EN\" style=\"font-family:&quot;Segoe UI&quot;, sans-serif;\">ClickUrTrip.com Pvt. Ltd.</span></b></p>");
            sb.Append("<div id=\"yui_3_16_0_ym19_1_1489469507252_52802\">");
            sb.Append("</div>");
            sb.Append("</div>");
            sb.Append("</div>");
            //sb.Append("<div style=\"height:50px;width:auto\">");
            //sb.Append("<br />");
            //sb.Append("<img src=\"http://www.clickurtrip.com/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" />");
            //sb.Append("</div>");
            ////sb.Append("<img src=\"http://www.clickurtrip.com/unnamed.jpg\" style=\"padding-left:-2%;width:100%;height:auto\" />");
            //sb.Append("<div>");
            //sb.Append("<span style=\"margin-left:2%;font-weight:400\">Hello " + sName + ",</span><br />");
            //sb.Append("<br />");
            //sb.Append("<span style=\"margin-left:2%;font-weight:400\">To complete registration process, <a href=\"" + Url + "\" target=\"_blank\">Click here. </a></span><br />");
            ////sb.Append("<span style=\"margin-left:2%;font-weight:400\">Your Username is : <b>" + sEmail + "</b> and Password is : <b>" + sMessage + "</b></span><br />");
            //sb.Append("<br />");
            //sb.Append("<br />");
            //sb.Append("<span style=\"margin-left:2%\">");
            //sb.Append("<b>Thank You,</b><br />");
            //sb.Append("</span>");
            //sb.Append("<span style=\"margin-left:2%\">");
            //sb.Append("Administrator");
            //sb.Append("</span>");
            //sb.Append("</div>");
            ////sb.Append("<img src=\"http://www.clickurtrip.com/images/unnamed.jpg\" style=\"width:100%;height:auto\" />");
            //sb.Append("<div>");
            //sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
            //sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
            //sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">  © 2015 - 2016 ClickurTrip.com</div></td>");
            //sb.Append("</tr>");
            //sb.Append("</table>");
            //sb.Append("</div>");
            //sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
            //sb.Append("</div>");
            sb.Append("</body>");
            sb.Append("</html>");



            try
            {
                int rowsAffected = 0;
                SqlParameter[] sqlParamsGrpName = new SqlParameter[3];
                sqlParamsGrpName[0] = new SqlParameter("@sTo", sTo);
                sqlParamsGrpName[1] = new SqlParameter("@sSubject", "Your Password Detail");
                sqlParamsGrpName[2] = new SqlParameter("@VarBody", sb.ToString());
                DBHelper.DBReturnCode RetEmail = DBHelper.ExecuteNonQuery("Proc_RegistrationMail", out rowsAffected, sqlParamsGrpName);
                //System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                //mailMsg.From = new MailAddress("support@royalsheets.asia");
                //mailMsg.To.Add(sTo);
                //mailMsg.CC.Add(System.Configuration.ConfigurationManager.AppSettings["EmailID"]);
                //mailMsg.Subject = sSubject;
                //mailMsg.IsBodyHtml = true;
                //mailMsg.Body = sb.ToString(); ;
                //SmtpClient mailObj = new SmtpClient("smtp.gmail.com", 587);
                //mailObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["EmailID"], System.Configuration.ConfigurationManager.AppSettings["PassWord"]);
                //mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                //mailObj.EnableSsl = true;
                //mailObj.Send(mailMsg);
                //mailMsg.Dispose();
                return true;

            }
            catch
            {
                return false;
            }
        }

        public static bool SendEmailAdmin(string sAgencyName, string AgentUniqueCode, string nMobile, string sEmail)
        {
            //Work start
            StringBuilder sb = new StringBuilder();
            sb.Append("<!DOCTYPE html>");
            sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
            sb.Append("<head>");
            sb.Append("<meta charset=\"utf-8\" />");
            sb.Append("</head>");
            sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">");
            //sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
            //sb.Append("</div>");
            sb.Append("<div style=\"height:50px;width:auto\">");
            sb.Append("<br />");
            sb.Append("<img src=\"http://www.clickurtrip.com/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" />");
            sb.Append("</div>");
            sb.Append("<div>");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Hello Admin,</span><br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">A New Agent is Registered With following Details. Activate Agent</b></span><br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Agency Name : <b>" + sAgencyName + "</b></span><br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Agent Unique Code : <b>" + AgentUniqueCode + "</b></span><br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Email-ID : <b>" + sEmail + "</b></span><br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Mobile Number : <b>" + nMobile + "</b></span><br />");
            sb.Append("<br />");
            sb.Append("<br />");
            sb.Append("<span style=\"margin-left:2%\">");
            sb.Append("<b>Thank You,</b><br />");
            sb.Append("</span>");
            sb.Append("<span style=\"margin-left:2%\">");
            sb.Append("Administrator");
            sb.Append("</span>");
            sb.Append("</div>");
            sb.Append("<div>");
            sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
            sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
            sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">  © 2015 - 2016 ClickurTrip.com</div></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</div>");
            //sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
            //sb.Append("</div>");
            sb.Append("</body>");
            sb.Append("</html>");



            try
            {
                string sTo = "support@clickurtrip.com";
                string sSubject = "New Agent is Registered";
                int rowsAffected = 0;
                SqlParameter[] sqlParamsGrpName = new SqlParameter[3];
                sqlParamsGrpName[0] = new SqlParameter("@sTo", sTo);
                sqlParamsGrpName[1] = new SqlParameter("@sSubject", sSubject);
                sqlParamsGrpName[2] = new SqlParameter("@VarBody", sb.ToString());
                DBHelper.DBReturnCode RetEmail = DBHelper.ExecuteNonQuery("Proc_Mail", out rowsAffected, sqlParamsGrpName);


                //System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                //mailMsg.From = new MailAddress("support@royalsheets.asia");
                //mailMsg.To.Add(sTo);
                //mailMsg.Subject = sSubject;
                //mailMsg.IsBodyHtml = true;
                //mailMsg.Body = sb.ToString();
                //SmtpClient mailObj = new SmtpClient("smtp.gmail.com", 587);
                //mailObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["EmailID"], System.Configuration.ConfigurationManager.AppSettings["PassWord"]);
                //mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                //mailObj.EnableSsl = true;
                //mailObj.Send(mailMsg);
                //mailMsg.Dispose();
                return true;

            }
            catch
            {
                return false;
            }
        }

        public static bool EmailSending(string sTo, string sSubject, string sMessageTitle, string sName, string sEmail, string sPhone, string sMessage)
        {

            try
            {
                string sMailMessage = EmailTemplate.Replace("#title#", sMessageTitle);
                sMailMessage = sMailMessage.Replace("#name#", sName);
                //sMailMessage = sMailMessage.Replace("#CompanyName#", sCompanyName);
                //sMailMessage = sMailMessage.Replace("#CompanyAddress#", sCompanyAddress);
                //sMailMessage = sMailMessage.Replace("#City#", sCity);
                //sMailMessage = sMailMessage.Replace("#State#", sState);
                //sMailMessage = sMailMessage.Replace("#Desgn#", sDesgn);
                sMailMessage = sMailMessage.Replace("#email#", sEmail);
                sMailMessage = sMailMessage.Replace("#phone#", sPhone);
                //sMailMessage = sMailMessage.Replace("#selectoption#", sSelectProducts);
                sMailMessage = sMailMessage.Replace("#text#", sMessage);
                System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                mailMsg.From = new MailAddress(sEmail);
                mailMsg.To.Add(sTo);
                mailMsg.Subject = sSubject;
                mailMsg.IsBodyHtml = true;
                mailMsg.Body = sMailMessage;
                SmtpClient mailObj = new SmtpClient("smtp.tmauto.in", 587);
                mailObj.Credentials = new NetworkCredential("support@tmauto.in", "admin123");
                mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                mailObj.EnableSsl = false;
                mailObj.Send(mailMsg);
                mailMsg.Dispose();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public static bool LowBalanceEmail(string Email, string sAgencyName, float Balance)
        {
            //Work start
            StringBuilder sb = new StringBuilder();
            sb.Append("<!DOCTYPE html>");
            sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
            sb.Append("<head>");
            sb.Append("<meta charset=\"utf-8\" />");
            sb.Append("</head>");
            sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto; border:2px solid gray\">");
            sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
            sb.Append("</div>");
            sb.Append("<div style=\"height:50px;width:auto\">");
            sb.Append("<br />");
            sb.Append("<img src=\"http://www.clickurtrip.com/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" />");
            sb.Append("</div>");
            sb.Append("<div>");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Hello " + sAgencyName + ",</span><br />");
            sb.Append("<br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Your Account Balance is Below Rs 1000. Kindly credit your acoount for further booking.</b></span><br />");

            sb.Append("<br />");
            sb.Append("<br />");
            sb.Append("<span style=\"margin-left:2%\">");
            sb.Append("<b>Thank You,</b><br />");
            sb.Append("</span>");
            sb.Append("<span style=\"margin-left:2%\">");
            sb.Append("Administrator");
            sb.Append("</span>");
            sb.Append("</div>");
            sb.Append("<div>");
            sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
            sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
            sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">  © 2015 - 2016 ClickurTrip.com</div></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</div>");
            sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
            sb.Append("</div>");
            sb.Append("</body>");
            sb.Append("</html>");



            try
            {

                int rowsAffected = 0;
                SqlParameter[] sqlParamsGrpName = new SqlParameter[3];
                sqlParamsGrpName[0] = new SqlParameter("@sTo", Email);
                sqlParamsGrpName[1] = new SqlParameter("@sSubject", "Low Balance Notification");
                sqlParamsGrpName[2] = new SqlParameter("@VarBody", sb.ToString());
                DBHelper.DBReturnCode RetEmail = DBHelper.ExecuteNonQuery("Proc_RegistrationMail", out rowsAffected, sqlParamsGrpName);


                //string sTo = Email;
                //string sSubject = "Low Balance Notification";
                //System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                //mailMsg.From = new MailAddress("support@royalsheets.asia");
                //mailMsg.To.Add(sTo);
                //mailMsg.Subject = sSubject;
                //mailMsg.IsBodyHtml = true;
                //mailMsg.Body = sb.ToString();
                //SmtpClient mailObj = new SmtpClient("smtp.gmail.com", 587);
                //mailObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["EmailID"], System.Configuration.ConfigurationManager.AppSettings["PassWord"]);
                //mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                //mailObj.EnableSsl = true;
                //mailObj.Send(mailMsg);
                //mailMsg.Dispose();
                return true;

            }
            catch
            {
                return false;
            }
        }
        public static DBHelper.DBReturnCode GetCountry(out DataTable dtResult)
        {
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_HCityLoadAllCountry", out dtResult);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetCity(string country, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Country", country);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_HCityLoadAllCityByCountry", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode UpdateAgentMarkup(Int64 sid, float MarkupPer, out int rows)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@uid", sid);
            sqlParams[1] = new SqlParameter("@MarkupPer", MarkupPer);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_MarkUpAgentAddUpdate", out rows, sqlParams);
            return retCode;
        }


        public static DBHelper.DBReturnCode GetAgentMarkup(Int64 sid, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@uid", sid);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_MarkUpGetAgentMarkup", out dtResult, sqlParams);
            return retCode;
        }

        public static string GenerateRandomString(int length)
        {
            string rndstring = "";
            bool IsRndlength = false;
            Random rnd = new Random((int)DateTime.Now.Ticks);
            if (IsRndlength)
                length = rnd.Next(4, length);
            for (int i = 0; i < length; i++)
            {
                int toss = rnd.Next(1, 10);
                if (toss > 5)
                    rndstring += (char)rnd.Next((int)'A', (int)'Z');
                else
                    rndstring += rnd.Next(0, 9).ToString();
            }
            return rndstring;
        }

        public static int GenerateRandomNumber()
        {
            int rndnumber = 0;
            Random rnd = new Random((int)DateTime.Now.Ticks);
            rndnumber = rnd.Next();
            return rndnumber;
        }

        public static bool UrlExists(string url)
        {
            try
            {
                new System.Net.WebClient().DownloadData(url);
                return true;
            }
            catch (System.Net.WebException e)
            {
                if (((System.Net.HttpWebResponse)e.Response).StatusCode == System.Net.HttpStatusCode.NotFound)
                    return false;
                else
                    return false;
            }
        }

    }
}