﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace CutAdmin.DataLayer
{
    public class DataManager
    {
        #region core ***********************************************
        public enum DBReturnCode
        {
            SUCCESS = 0,
            CONNECTIONFAILURE = -1,
            SUCCESSNORESULT = -2,
            SUCCESSNOAFFECT = -3,
            DUPLICATEENTRY = -4,
            EXCEPTION = -5,
            INVALIDXML = -11,
            INPUTPARAMETEREMPTY = -6,
            UNIQUEKEYVIOLATION = -7,
            REFERENCECONFLICT = -8,
            NOQUESTIONS = -9,
            INTERMISSION = -10,
            TESTCOMPLETED = -12,
            TESTTIMEOUT = -13,
            QUESTIONALREADYANSWERED = -14,
            TESTNOTFOUND = -15
        }
        public enum CountryReturnCode
        {
            SUCCESS = 0,
            EXCEPTION = -1,
            NORECORDSFOUND = -2,
            COUNTRYNOTFOUND = -3,
            COUNTRYEXISTS = -4,
        }
        private static SqlConnection OpenConnection()
        {
            SqlConnection conn = null;
            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ClickurTripConnectionString"].ToString());
                conn.Open();
            }
            catch
            {
                conn = null;
            }
            return conn;
        }
        private static void CloseConnection(SqlConnection conn)
        {
            try
            {
                conn.Close();
                conn.Dispose();
            }
            catch { }
        }
        public static DBReturnCode ExecuteQuery(string sQuery, out DataTable dtResult)
        {

            DBReturnCode retcode = DBReturnCode.SUCCESS;
            dtResult = null;
            SqlDataAdapter dataAdapter = null;
            SqlConnection conn = OpenConnection();
            if (conn == null)
            {
                retcode = DBReturnCode.CONNECTIONFAILURE;
            }
            else
            {
                try
                {
                    dataAdapter = new SqlDataAdapter(sQuery, conn);
                    DataSet dsTmp = new DataSet();
                    dataAdapter.Fill(dsTmp);
                    dtResult = dsTmp.Tables[0];
                    if (dtResult.Rows.Count == 0)
                        return DBReturnCode.SUCCESSNORESULT;
                    else
                        return DBReturnCode.SUCCESS;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    retcode = DBReturnCode.EXCEPTION;
                }
                finally
                {
                    dataAdapter.Dispose();
                    CloseConnection(conn);
                }
            }
            return retcode;
        }
        public static DBReturnCode ExecuteNonQuery(string sQuery)
        {
            DBReturnCode retcode = DBReturnCode.SUCCESS;
            SqlCommand cmd = null;
            SqlConnection conn = OpenConnection();
            if (conn == null)
            {
                retcode = DBReturnCode.CONNECTIONFAILURE;
            }
            else
            {
                try
                {
                    cmd = new SqlCommand(sQuery, conn);
                    if (cmd.ExecuteNonQuery() == 0)
                        return DBReturnCode.SUCCESSNOAFFECT;
                    else
                        return DBReturnCode.SUCCESS;
                }
                catch (SqlException ex)
                {
                    if (ex.Number == 2627 || ex.Number == 2601) // PRIMARY or UNIQUE KEY KEY VIOLATION
                    {
                        retcode = DBReturnCode.DUPLICATEENTRY;
                    }
                    else
                        retcode = DBReturnCode.EXCEPTION;
                }
                finally
                {
                    cmd.Dispose();
                    CloseConnection(conn);
                }
            }
            return retcode;
        }
        public static DBReturnCode ExecuteScaler(string sQuery, out Int64 nRecordID)
        {
            nRecordID = 0;
            /*HttpContext.Current.Response.Write(sQuery);
            HttpContext.Current.Response.Write("<br/>");*/
            DBReturnCode retcode = DBReturnCode.SUCCESS;
            SqlCommand cmd = null;
            SqlConnection conn = OpenConnection();
            if (conn == null)
            {
                retcode = DBReturnCode.CONNECTIONFAILURE;
            }
            else
            {
                try
                {
                    sQuery += "; SELECT SCOPE_IDENTITY();";
                    cmd = new SqlCommand(sQuery, conn);
                    nRecordID = Convert.ToInt64(cmd.ExecuteScalar());
                    if (nRecordID <= 0)
                        return DBReturnCode.SUCCESSNOAFFECT;
                    else
                        return DBReturnCode.SUCCESS;
                }
                catch //(Exception e)
                {
                    retcode = DBReturnCode.EXCEPTION;
                    //HttpContext.Current.Response.Write(e.Message); 
                }
                finally
                {
                    cmd.Dispose();
                    CloseConnection(conn);
                }
            }
            return retcode;
        }

        #region " DB Opration With Transection "

        public static DBReturnCode ExecuteQueryT(SqlConnection Conn, String sSQL, out DataTable dtResult)
        {
            DBReturnCode retCode = DBReturnCode.EXCEPTION;
            dtResult = null;

            try
            {
                using (SqlCommand Cmd = new SqlCommand(sSQL, Conn))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(sSQL, Conn))
                    {
                        da.Fill(dtResult);

                        if (dtResult.Rows.Count > 0)
                            retCode = DBReturnCode.SUCCESS;
                        else
                            retCode = DBReturnCode.SUCCESSNORESULT;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {

            }

            return retCode;
        }

        public static DBReturnCode ExecuteNonQueryT(SqlConnection Conn, SqlTransaction Tran, String SQL)
        {
            DBReturnCode retCode = DBReturnCode.SUCCESS;
            SqlCommand Cmd = null;
            try
            {
                Cmd = new SqlCommand();
                Cmd.Connection = Conn;
                Cmd.Transaction = Tran;
                Cmd.CommandText = SQL;

                if (Cmd.ExecuteNonQuery() == 0)
                    retCode = DBReturnCode.SUCCESSNOAFFECT;
                else
                    retCode = DBReturnCode.SUCCESS;

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);

                if (ex.Number == 2627) // PRIMARY KEY KEY VIOLATION
                {
                    retCode = DBReturnCode.DUPLICATEENTRY;
                }
                else if (ex.Number == 2601) //UNIQUE KEY VIOLATION
                {
                    retCode = DBReturnCode.UNIQUEKEYVIOLATION;
                }
                else if (ex.Number == 547)   // FORENIGN KEY VIOLATION
                {
                    retCode = DBReturnCode.REFERENCECONFLICT;
                }
                else
                {
                    retCode = DBReturnCode.EXCEPTION;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (Cmd != null)
                    Cmd.Dispose();
            }

            return retCode;
        }

        public static DBReturnCode ExecuteNonQueryT(SqlConnection Conn, SqlTransaction Tran, String SQL, SqlParameter[] ParameterList)
        {
            DBReturnCode retCode = DBReturnCode.SUCCESS;
            SqlCommand Cmd = null;
            try
            {
                Cmd = new SqlCommand();
                Cmd.Connection = Conn;
                Cmd.Transaction = Tran;
                Cmd.CommandText = SQL;

                foreach (SqlParameter par in ParameterList)
                {
                    if (par != null)
                        Cmd.Parameters.Add(par);
                }

                if (Cmd.ExecuteNonQuery() == 0)
                    retCode = DBReturnCode.SUCCESSNOAFFECT;
                else
                    retCode = DBReturnCode.SUCCESS;

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);

                if (ex.Number == 2627) // PRIMARY KEY VIOLATION
                {
                    retCode = DBReturnCode.DUPLICATEENTRY;
                }
                else if (ex.Number == 2601) //UNIQUE KEY VIOLATION
                {
                    retCode = DBReturnCode.UNIQUEKEYVIOLATION;
                }
                else if (ex.Number == 547)   // FORENIGN KEY VIOLATION
                {
                    retCode = DBReturnCode.REFERENCECONFLICT;
                }
                else
                {
                    retCode = DBReturnCode.EXCEPTION;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (Cmd != null)
                    Cmd.Dispose();
            }

            return retCode;
        }

        public static DBReturnCode ExecuteScalerT(SqlConnection Conn, SqlTransaction Tran, String SQL, out Int64 RecordID, out String ErrorMessage)
        {
            DBReturnCode retCode = DBReturnCode.SUCCESS;
            SqlCommand Cmd = null;
            RecordID = 0;
            ErrorMessage = String.Empty;
            try
            {
                Cmd = new SqlCommand();
                Cmd.Connection = Conn;
                Cmd.Transaction = Tran;

                SQL += "; SELECT SCOPE_IDENTITY();";

                Cmd.CommandText = SQL;

                RecordID = Convert.ToInt64(Cmd.ExecuteScalar());

                if (RecordID == 0)
                    retCode = DBReturnCode.SUCCESSNOAFFECT;
                else
                    retCode = DBReturnCode.SUCCESS;

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                ErrorMessage = "FROM DataManger Class \n\r " + ex.Message;
                ErrorMessage += "\n\r " + SQL;

                if (ex.Number == 2627) // PRIMARY KEY VIOLATION
                {
                    retCode = DBReturnCode.DUPLICATEENTRY;
                }
                else if (ex.Number == 2601) //UNIQUE KEY VIOLATION
                {
                    retCode = DBReturnCode.UNIQUEKEYVIOLATION;
                }
                else if (ex.Number == 547)   // FORENIGN KEY VIOLATION
                {
                    retCode = DBReturnCode.REFERENCECONFLICT;
                }
                else
                {
                    retCode = DBReturnCode.EXCEPTION;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (Cmd != null)
                    Cmd.Dispose();
            }

            return retCode;
        }

        public static DBReturnCode ExecuteScalerT(SqlConnection Conn, SqlTransaction Tran, String SQL, SqlParameter[] ParameterList, out Int64 RecordID, out String ErrorMessage)
        {
            DBReturnCode retCode = DBReturnCode.SUCCESS;
            SqlCommand Cmd = null;
            RecordID = 0;
            ErrorMessage = String.Empty;
            try
            {
                Cmd = new SqlCommand();
                Cmd.Connection = Conn;
                Cmd.Transaction = Tran;

                SQL += "; SELECT SCOPE_IDENTITY();";

                Cmd.CommandText = SQL;

                foreach (SqlParameter par in ParameterList)
                {
                    if (par != null)
                        Cmd.Parameters.Add(par);
                }

                RecordID = Convert.ToInt64(Cmd.ExecuteScalar());

                if (RecordID == 0)
                    retCode = DBReturnCode.SUCCESSNOAFFECT;
                else
                    retCode = DBReturnCode.SUCCESS;

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                ErrorMessage = "FROM DataManger Class \n\r " + ex.Message;
                ErrorMessage += "\n\r " + SQL;

                if (ex.Number == 2627) // PRIMARY KEY VIOLATION
                {
                    retCode = DBReturnCode.DUPLICATEENTRY;
                }
                else if (ex.Number == 2601) //UNIQUE KEY VIOLATION
                {
                    retCode = DBReturnCode.UNIQUEKEYVIOLATION;
                }
                else if (ex.Number == 547)   // FORENIGN KEY VIOLATION
                {
                    retCode = DBReturnCode.REFERENCECONFLICT;
                }
                else
                {
                    retCode = DBReturnCode.EXCEPTION;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (Cmd != null)
                    Cmd.Dispose();
            }

            return retCode;
        }

        #endregion " DB Opration With Transection "

        #endregion core

        public static DBReturnCode GetPackageDetails(out DataTable dtResult)
        {
            StringBuilder sSQL = new StringBuilder();
            sSQL.Append("SELECT tbl_Package.*, tbl_PackageImages.ImageArray,tbl_PackagePricing.dSingleAdult FROM tbl_Package INNER JOIN  tbl_PackageImages ON tbl_Package.nID = tbl_PackageImages.nPackageID INNER JOIN tbl_PackagePricing ON tbl_Package.nID = tbl_PackagePricing.nPackageID ORDER BY tbl_PackagePricing.dSingleAdult");
            return ExecuteQuery(sSQL.ToString(), out dtResult);
        }

        public static DBReturnCode SinglePackageDetails(Int64 nID, out DataTable dtResult)
        {
            StringBuilder sSQL = new StringBuilder();
            sSQL.Append("SELECT tbl_Package.*, tbl_Itinerary.*  FROM tbl_Package INNER JOIN tbl_Itinerary ON tbl_Package.nID = tbl_Itinerary.nPackageID  WHERE tbl_Package.nID= ").Append(nID);
            return ExecuteQuery(sSQL.ToString(), out dtResult);
        }

        public static DBReturnCode PackageDetails(Int64 nID, out DataTable dtResult)
        {
            StringBuilder sSQL = new StringBuilder();
            sSQL.Append("SELECT tbl_Package.*,tbl_PackageImages.ImageArray, tbl_Itinerary.nCategoryID, tbl_Itinerary.nCategoryName, tbl_Itinerary.sItinerary_1, tbl_Itinerary.sItinerary_2, tbl_Itinerary.sItinerary_3, tbl_Itinerary.sItinerary_4, tbl_Itinerary.sItinerary_5, tbl_Itinerary.sItinerary_6, tbl_Itinerary.sItinerary_7,")
            .Append(" tbl_Itinerary.sItinerary_8, tbl_Itinerary.sItinerary_9, tbl_Itinerary.sItinerary_10, tbl_Itinerary.sItinerary_11, tbl_Itinerary.sItinerary_12, tbl_Itinerary.sItinerary_13, tbl_Itinerary.sItinerary_14, tbl_Itinerary.sItinerary_15, tbl_HotelImages.sHotelImage1,tbl_HotelImages.sHotelImage2,tbl_HotelImages.sHotelImage3,")
            .Append(" tbl_HotelImages.sHotelImage4,tbl_HotelImages.sHotelImage5,tbl_HotelImages.sHotelImage6,tbl_HotelImages.sHotelImage7,tbl_HotelImages.sHotelImage8,tbl_HotelImages.sHotelImage9,tbl_HotelImages.sHotelImage10,tbl_HotelImages.sHotelImage11,tbl_HotelImages.sHotelImage12,tbl_HotelImages.sHotelImage13,tbl_HotelImages.sHotelImage14,")
            .Append(" tbl_PackagePricing.dSingleAdult,tbl_PackagePricing.dCouple,tbl_PackagePricing.dExtraAdult,tbl_PackagePricing.dInfantKid,tbl_PackagePricing.dKidWBed,tbl_PackagePricing.dKidWOBed,tbl_PackagePricing.sStaticInclusions,tbl_PackagePricing.sDynamicInclusion,tbl_PackagePricing.sStaticExclusion,tbl_PackagePricing.sDynamicExclusion,")
            .Append(" tbl_ItineraryHotel.sHotelName_1,tbl_ItineraryHotel.sHotelName_2,tbl_ItineraryHotel.sHotelName_3,tbl_ItineraryHotel.sHotelName_4,tbl_ItineraryHotel.sHotelName_5,tbl_ItineraryHotel.sHotelName_6,tbl_ItineraryHotel.sHotelName_7,tbl_ItineraryHotel.sHotelName_8,tbl_ItineraryHotel.sHotelName_9,tbl_ItineraryHotel.sHotelName_10,")
            .Append(" tbl_ItineraryHotel.sHotelName_11,tbl_ItineraryHotel.sHotelName_12,tbl_ItineraryHotel.sHotelName_13,tbl_ItineraryHotel.sHotelName_14,tbl_ItineraryHotel.sHotelDescrption_1,tbl_ItineraryHotel.sHotelDescrption_2,tbl_ItineraryHotel.sHotelDescrption_3,tbl_ItineraryHotel.sHotelDescrption_4,tbl_ItineraryHotel.sHotelDescrption_5,")
            .Append(" tbl_ItineraryHotel.sHotelDescrption_6,tbl_ItineraryHotel.sHotelDescrption_7,tbl_ItineraryHotel.sHotelDescrption_8,tbl_ItineraryHotel.sHotelDescrption_9,tbl_ItineraryHotel.sHotelDescrption_10,tbl_ItineraryHotel.sHotelDescrption_11,tbl_ItineraryHotel.sHotelDescrption_12,tbl_ItineraryHotel.sHotelDescrption_13,")
            .Append(" tbl_ItineraryHotel.sHotelDescrption_14 FROM tbl_Package INNER JOIN tbl_PackageImages ON tbl_Package.nID = tbl_PackageImages.nPackageID INNER JOIN tbl_Itinerary ON tbl_Itinerary.nPackageID = tbl_Package.nID INNER JOIN tbl_ItineraryHotel ON (tbl_Itinerary.nPackageID = tbl_ItineraryHotel.nPackageID AND tbl_Itinerary.nCategoryID = tbl_ItineraryHotel.nCategoryID)")
            .Append(" INNER JOIN tbl_PackagePricing ON (tbl_ItineraryHotel.nPackageID = tbl_PackagePricing.nPackageID AND tbl_PackagePricing.nCategoryID = tbl_ItineraryHotel.nCategoryID) INNER JOIN tbl_HotelImages ON (tbl_PackagePricing.nPackageID = tbl_HotelImages.nPackageID  AND tbl_PackagePricing.nCategoryID = tbl_HotelImages.nCategoryID) WHERE tbl_Package.nID=").Append(nID);
            return ExecuteQuery(sSQL.ToString(), out dtResult);
        }

        public static DBReturnCode GetProductImages(Int64 nPackageID, out DataTable dtResult)
        {
            StringBuilder sSQL = new StringBuilder();
            sSQL.Append("SELECT * FROM tbl_PackageImages WHERE nPackageID=").Append(nPackageID);
            return ExecuteQuery(sSQL.ToString(), out dtResult);
        }

        public static DBReturnCode GetStaticPackages(out DataTable dtResult)
        {
            StringBuilder sSQL = new StringBuilder();
            sSQL.Append("Select top 9 * from tbl_StaticPackages where Currency='USD' ORDER BY tbl_StaticPackages.sid DESC");
            return ExecuteQuery(sSQL.ToString(), out dtResult);
        }
        public static List<Dictionary<string, object>> ConvertDataTable(DataTable dtResult)
        {
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dtResult.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dtResult.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return parentRow;
        }
        public static DBReturnCode BookingMails(string Email, string Subject, string MailDetails)
        {
            StringBuilder sSQL = new StringBuilder();
            sSQL.Append("EXEC msdb.dbo.sp_send_dbmail @profile_name = 'CUT_MailProfile', @recipients='" + Email + "',@subject = '" + Subject + "',@body = '" + MailDetails + "',@body_format= 'HTML';");
            return ExecuteNonQuery(sSQL.ToString());
        }
    }
}