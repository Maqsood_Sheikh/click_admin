﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CutAdmin.BL;
using System.Globalization;
namespace CutAdmin.DataLayer
{
    public class CommissionReports
    {
        public class InvoiceCategory
        {
            public string Category { get; set; }
            public List<string > InvoiceNo { get; set; }
            public decimal Total { get; set; }
        }
        //static DBHandlerDataContext db = new DBHandlerDataContext();

        #region Old report
        public static void GenrateReports(List<Int64> SupplierId)
        {
            try
            {
                foreach (var UserID in SupplierId)
                {
                    //dbTaxHandlerDataContext dbTax = new dbTaxHandlerDataContext();
                    using (var dbTax = new dbTaxHandlerDataContext())
                    {
                        Comm_CommissionReport objReport = new Comm_CommissionReport();
                        var arrInvoice = (from obj in dbTax.tbl_Invoices where obj.ParentID == UserID select obj).ToList().OrderBy(d => d.ID).ToList();
                        var arrReport = (from obj in dbTax.Comm_CommissionReports where obj.SupplierID == UserID select obj).ToList().OrderByDescending(d => d.ID).ToList();
                        using (var db = new DBHandlerDataContext())
                        {
                            int Cycle = Convert.ToInt16((from obj in db.tbl_AdminLogins where obj.sid == UserID select obj).FirstOrDefault().CommssionsDays);
                            bool RefundCancel = Convert.ToBoolean((from obj in db.tbl_AdminLogins where obj.sid == UserID select obj).FirstOrDefault().RefundCancel);
                            bool onCheckIn = Convert.ToBoolean((from obj in db.tbl_AdminLogins where obj.sid == UserID select obj).FirstOrDefault().CheckInCommission);
                            if (Cycle == 0)
                                continue;
                            Int64 InvoiceID;
                            if (arrReport.Count == 0)
                            {
                                DateTime LastDate = DateTime.ParseExact(arrInvoice.FirstOrDefault().InvoiceDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                if (LastDate.AddDays(Cycle) < DateTime.Now)
                                {
                                    decimal Comission = GetCycleInvoice(arrInvoice, LastDate.AddDays(Cycle), 0, out InvoiceID);
                                    objReport = new Comm_CommissionReport
                                    {
                                        Commission = Comission,
                                        FullPaid = false,
                                        InsertDate = DateTime.Now.ToString("dd-MM-yyyy"),
                                        LastCycle = Cycle,
                                        PartialPaid = false,
                                        SupplierID = UserID,
                                        UnPaidAmunt = arrInvoice.Select(d => d.Commission).ToList().Sum(),
                                        InvoiceID = InvoiceID

                                    };
                                    dbTax.Comm_CommissionReports.InsertOnSubmit(objReport);
                                    dbTax.SubmitChanges();


                                    /////////// For Commission Invoice  ///////////////////////

                                    var ID = arrInvoice.Where(d => d.AgentID == UserID && d.ParentID == 232).ToList().Count;
                                    string LastCommiissionInvoiceID = "CQ-" + (ID + 1).ToString("D" + 6);

                                    string Currency = (from obj in db.tbl_AdminLogins where obj.sid == UserID select obj.CurrencyCode).FirstOrDefault();

                                    tbl_Invoice Invc = new tbl_Invoice();
                                    Invc.InvoiceNo = LastCommiissionInvoiceID;
                                    Invc.InvoiceDate = DateTime.Now.ToString("dd-MM-yyyy");
                                    Invc.Particulars = LastDate.ToString() + "-" + DateTime.Now.ToString("dd-MM-yyyy");
                                    Invc.ServiceType = "Hotel";
                                    Invc.InvoiceType = "Hotel Commission";
                                    Invc.Currency = Currency;
                                    Invc.InvoiceAmount = Comission;
                                    Invc.TaxCharges = 0;
                                    Invc.Commission = 0;
                                    Invc.TDS = 0;
                                    Invc.ServiceFee = 0;
                                    Invc.GstDetails = 0;
                                    Invc.AgentID = UserID;
                                    Invc.ParentID = 232;
                                    Invc.Status = 1;

                                    dbTax.tbl_Invoices.InsertOnSubmit(Invc);
                                    dbTax.SubmitChanges();

                                    /////////// End Commission Invoice  ///////////////////////

                                }
                            }
                            else
                            {
                                DateTime LastDate = DateTime.ParseExact(arrReport.FirstOrDefault().InsertDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                if (LastDate.AddDays(Cycle) < DateTime.Now)
                                {
                                    decimal Commission = GetCycleInvoice(arrInvoice, LastDate.AddDays(1), Convert.ToInt64(arrReport.FirstOrDefault().InvoiceID), out InvoiceID);
                                    if (Commission == 0)
                                        continue;
                                    var Report = (from obj in dbTax.Comm_CommissionReports
                                                  where obj.InsertDate == DateTime.Now.ToString("dd-MM-yyyy") && obj.SupplierID == UserID
                                                  select obj).FirstOrDefault();
                                    if (Report != null)
                                    {
                                        Report.Commission = Commission;
                                        Report.UnPaidAmunt = Commission + arrReport.Where(d => d.FullPaid == false).Select(d => d.UnPaidAmunt).ToList().Sum();
                                        Report.InvoiceID = InvoiceID;
                                    }
                                    else
                                    {
                                        objReport = new Comm_CommissionReport
                                        {
                                            Commission = Commission,
                                            FullPaid = false,
                                            InsertDate = DateTime.Now.ToString("dd-MM-yyyy"),
                                            LastCycle = Cycle,
                                            PartialPaid = false,
                                            SupplierID = UserID,
                                            UnPaidAmunt = Commission + arrReport.Where(d => d.FullPaid == false).Select(d => d.UnPaidAmunt).ToList().Sum(),
                                            InvoiceID = InvoiceID,
                                        };
                                        dbTax.Comm_CommissionReports.InsertOnSubmit(objReport);
                                    }

                                    dbTax.SubmitChanges();


                                    /////////// For Commission Invoice  ///////////////////////

                                    var ID = arrInvoice.Where(d => d.AgentID == UserID && d.ParentID == 232).ToList().Count;
                                    string LastCommiissionInvoiceID = "CQ-" + (ID + 1).ToString("D" + 6);

                                    string Currency = (from obj in db.tbl_AdminLogins where obj.sid == UserID select obj.CurrencyCode).FirstOrDefault();

                                    tbl_Invoice Invc = new tbl_Invoice();
                                    Invc.InvoiceNo = LastCommiissionInvoiceID;
                                    Invc.InvoiceDate = DateTime.Now.ToString("dd-MM-yyyy");
                                    Invc.Particulars = LastDate.ToString() + "-" + DateTime.Now.ToString("dd-MM-yyyy");
                                    Invc.ServiceType = "Hotel";
                                    Invc.InvoiceType = "Hotel Commission";
                                    Invc.Currency = Currency;
                                    Invc.InvoiceAmount = Commission;
                                    Invc.TaxCharges = 0;
                                    Invc.Commission = 0;
                                    Invc.TDS = 0;
                                    Invc.ServiceFee = 0;
                                    Invc.GstDetails = 0;
                                    Invc.AgentID = UserID;
                                    Invc.ParentID = 232;
                                    Invc.Status = 1;

                                    dbTax.tbl_Invoices.InsertOnSubmit(Invc);
                                    dbTax.SubmitChanges();

                                    /////////// End Commission Invoice  ///////////////////////

                                }
                            }
                        }
                    }
                }
            }
            catch
            {

            }
        }

        public static decimal GetCycleInvoice(List<tbl_Invoice> arrInvoice, DateTime LastDate, Int64 PriviousID, out Int64 LastInvoiceID)
        {
            decimal Commission = 0;
            LastInvoiceID = 0;
            List<tbl_Invoice> arrNewInvoce = new List<tbl_Invoice>();
            try
            {
                foreach (var objInvoice in arrInvoice)
                {
                    DateTime InvoiceDate = DateTime.ParseExact(objInvoice.InvoiceDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    if (InvoiceDate >= LastDate || PriviousID == 0)
                    {
                        if (objInvoice.ID > PriviousID)
                            arrNewInvoce.Add(objInvoice);
                    }
                }
                Commission = arrNewInvoce.Select(d => Convert.ToDecimal(d.Commission)).ToList().Sum();
                LastInvoiceID = arrNewInvoce.OrderByDescending(d => d.ID).FirstOrDefault().ID;
            }
            catch
            {

            }
            return Commission;
        }

        public static tbl_Invoice GetCommission(Int64 Supplier_ID)
        {
            using (var db = new DBHandlerDataContext())
            {
                using (var dbTax = new dbTaxHandlerDataContext())
                {
                    tbl_Invoice objComission = new tbl_Invoice();
                    //dbTaxHandlerDataContext dbTax = new dbTaxHandlerDataContext();
                    List<InvoiceCategory> ListCategory = new List<InvoiceCategory>();
                    try
                    {
                        bool OnCheckIn = (from obj in db.tbl_AdminLogins where obj.sid == Supplier_ID select obj.CheckInCommission).FirstOrDefault().Value;
                        bool RefundCancel = (from obj in db.tbl_AdminLogins where obj.sid == Supplier_ID select obj.RefundCancel).FirstOrDefault().Value;
                        var arrInvoice = (from obj in dbTax.tbl_Invoices
                                          where obj.ParentID == Supplier_ID
                                          select new
                                          {
                                              obj.ID,
                                              obj.InvoiceNo,
                                              dtInvoiceDate = DateTime.ParseExact(obj.InvoiceDate, "dd-MM-yyyy", CultureInfo.InstalledUICulture),
                                              Commssion = obj.Commission,
                                          }).ToList().OrderBy(d => d.ID).ToList();
                        foreach (var objInvoice in arrInvoice)
                        {
                            var objHotel = (from obj in db.tbl_CommonHotelReservations
                                            join
                                            objHotelDeatails in db.tbl_CommonHotelMasters on Convert.ToInt64(obj.HotelCode) equals objHotelDeatails.sid
                                            select new { objHotelDeatails.HotelCategory }).FirstOrDefault();
                            if (ListCategory.Where(d => d.Category.Contains(objHotel.HotelCategory)).ToList().Count == 0)
                            {
                                List<string> sNo = new List<string> { objInvoice.InvoiceNo };
                                ListCategory.Add(new InvoiceCategory { Category = objHotel.HotelCategory, InvoiceNo = new List<string> { objInvoice.InvoiceNo }, Total = Convert.ToDecimal(objInvoice.Commssion) });
                            }
                            else
                            {
                                ListCategory.Where(d => d.Category.Contains(objHotel.HotelCategory)).FirstOrDefault().InvoiceNo.Add(objInvoice.InvoiceNo);
                                ListCategory.Where(d => d.Category.Contains(objHotel.HotelCategory)).FirstOrDefault().Total += Convert.ToDecimal(objInvoice.Commssion);

                            }
                        }

                    }
                    catch (Exception ex)
                    {
                    }
                    return objComission;
                }
            }
        }
        #endregion

        #region new Report
        public static void GenrateCommision(Int64 UserID)
        {
            try
            {
                using(var dbTax= new dbTaxHandlerDataContext())
	            {
                    int Cycle; bool RefundCancel, onCheckIn; DateTime CycleDate;
                    using (var db = new DBHandlerDataContext())
                    {
                         Cycle = Convert.ToInt16((from obj in db.tbl_AdminLogins where obj.sid == UserID select obj).FirstOrDefault().CommssionsDays);
                         RefundCancel = Convert.ToBoolean((from obj in db.tbl_AdminLogins where obj.sid == UserID select obj).FirstOrDefault().RefundCancel);
                         onCheckIn = Convert.ToBoolean((from obj in db.tbl_AdminLogins where obj.sid == UserID select obj).FirstOrDefault().CheckInCommission);
                    
                    var arrInvoice = (from obj in dbTax.tbl_Invoices
                                      where obj.ParentID == UserID && obj.CycleId == 0 && obj.OnCheckIn == onCheckIn && obj.RefundCancel == RefundCancel
                                      select new
                                      {
                                          obj.InvoiceNo,
                                          obj.ID,
                                          obj.InvoiceDate,
                                          obj.Commission,
                                          Date = DateTime.ParseExact(obj.CommissionDate, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                                          obj.OnCheckIn
                                      }).ToList();
                    var arrCommision = (from obj in dbTax.Comm_CommissionReports where obj.SupplierID == UserID select obj).OrderByDescending(d => d.ID).FirstOrDefault();
                    if(arrCommision != null)
                        CycleDate = DateTime.ParseExact(arrCommision.InsertDate, "dd-MM-yyyy", CultureInfo.InvariantCulture).AddDays(Cycle);
                    else
                        CycleDate = DateTime.Now.AddDays(Cycle);
                    var arrCommInvoice = arrInvoice.Where(d => d.Date <= CycleDate).ToList();
                    if(arrCommInvoice.Count!=0)
                    {
                        decimal? Commission = arrCommInvoice.Select(d => d.Commission).ToList().Sum();
                       var objReport = new Comm_CommissionReport
                        {
                            Commission = Commission,
                            FullPaid = false,
                            InsertDate = DateTime.Now.ToString("dd-MM-yyyy"),
                            LastCycle = Cycle,
                            PartialPaid = false,
                            SupplierID = UserID,
                            UnPaidAmunt = Commission ,
                            InvoiceID = arrCommInvoice.OrderByDescending(d=>d.ID).FirstOrDefault().ID,
                        };
                        dbTax.Comm_CommissionReports.InsertOnSubmit(objReport);
                        dbTax.SubmitChanges();
                        foreach (var objInvoice in arrCommInvoice)
                        {
                            var Invoice = (from obj in dbTax.tbl_Invoices
                                           where obj.ID == objInvoice.ID
                                           select obj).FirstOrDefault();
                            Invoice.CycleId = objReport.ID;
                            dbTax.SubmitChanges();
                        }
                        /////////// For Commission Invoice  ///////////////////////

                        var ID = dbTax.tbl_Invoices.Where(d => d.AgentID  == UserID && d.ParentID == 232).ToList().Count;
                        string LastCommiissionInvoiceID = "CQ-" + (ID + 1).ToString("D" + 6);

                        string Currency = (from obj in db.tbl_AdminLogins where obj.sid == UserID select obj.CurrencyCode).FirstOrDefault();

                        tbl_Invoice Invc = new tbl_Invoice();
                        Invc.InvoiceNo = LastCommiissionInvoiceID;
                        Invc.InvoiceDate = DateTime.Now.ToString("dd-MM-yyyy");
                        if (arrCommision != null)
                         Invc.Particulars = arrCommision.InsertDate + " to " + DateTime.Now.ToString("dd-MM-yyyy");
                        else
                            Invc.Particulars = arrCommInvoice.OrderByDescending(d => d.ID).ToList().FirstOrDefault().InvoiceDate + " to " + DateTime.Now.ToString("dd-MM-yyyy"); 
                        Invc.ServiceType = "Hotel";
                        Invc.InvoiceType = "Hotel Commission";
                        Invc.Currency = Currency;
                        Invc.InvoiceAmount = Commission;
                        Invc.TaxCharges = 0;
                        Invc.Commission = 0;
                        Invc.TDS = 0;
                        Invc.ServiceFee = 0;
                        Invc.GstDetails = 0;
                        Invc.AgentID = UserID;
                        Invc.ParentID = 232;
                        Invc.Status = 1;
                        Invc.CycleId = objReport.ID;
                        dbTax.tbl_Invoices.InsertOnSubmit(Invc);
                        dbTax.SubmitChanges();
                    }
                    }
	            }
            }
            catch
            {
                
            }
        }
        #endregion
    }
}