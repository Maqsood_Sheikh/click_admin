﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace CutAdmin.DataLayer
{
    public class GlobalDefaultTransfers
    {
        public Int64 Sid { get; set; }
        public string sFirstName { get; set; }
        public string sLastName { get; set; }
        public string sGender { get; set; }
        public string sEducation { get; set; }
        public string sMarried { get; set; }
        public string sSpousName { get; set; }
        public string sPAddress { get; set; }
        public string sPLandmark { get; set; }
        public string sPPincode { get; set; }
        public string sPCountry { get; set; }
        public string sCAddress { get; set; }
        public string sPCity { get; set; }
        public string sCLandmark { get; set; }
        public string sCCity { get; set; }
        public string sCPincode { get; set; }
        public string sCCountry { get; set; }
        public string sMobile { get; set; }
        public string sPhone { get; set; }
        public string sEmail { get; set; }
        public string sPassword { get; set; }
        public string sIntroducerName { get; set; }
        public string dRegistrationDate { get; set; }
        public string sUserType { get; set; }
        public string sRoleId { get; set; }
        public string sUniqueCode { get; set; }
        public string sCategory { get; set; }
        public string sRefMail { get; set; }
        public bool sIsActive { get; set; }
        public string sUserLimit { get; set; }
        public string DOB { get; set; }
        public Int64 Sr_No { get; set; }
        public string UserType { get; set; }
        public string Username { get; set; }

       // public List<Trans_Reservation> ReservationList { get; set; }
       // public DataTable dtReservationList { get; set; }
    }
}