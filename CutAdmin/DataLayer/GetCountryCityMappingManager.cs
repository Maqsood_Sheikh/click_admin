﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CutAdmin.DataLayer
{
    public class GetCountryCityMappingManager
    {
        public static DBHelper.DBReturnCode GetCountryCityMapping(string Countryname, string CityName, out DataSet dsResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@Countryname", Countryname);
            sqlParams[1] = new SqlParameter("@CityName", CityName);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("usp_Proc_CountryCityMappingWithCode", out dsResult, sqlParams);
            return retCode;
        }
    }
}