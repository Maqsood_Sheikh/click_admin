﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CutAdmin.DataLayer
{
    public class FacilityManager
    {
        public static DBHelper.DBReturnCode AddFacility(string HotelFacilityName, string sPhoto)
        {
            int rows = 0;
            //string UniqueCode = CountryCode + GenerateRandomString(4);
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@HotelFacilityName", HotelFacilityName);
            sqlParams[1] = new SqlParameter("@sPhoto", sPhoto);

            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_InsertHotelFacility", out rows, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetHotelFacility(out DataTable dtResult)
        {
            dtResult = null;
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("proc_GetHotelFacility", out dtResult);
            return retCode;
        }

        public static DBHelper.DBReturnCode DeleteHotelFacility(string HotelFacilityID)
        {
            int rows;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@HotelFacilityID", HotelFacilityID);

            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("proc_DeleteHotelFacility", out rows, sqlParams);
            return retCode;
        }


        public static DBHelper.DBReturnCode GetDetails(string HotelFacilityID, out DataTable dtResult)
        {
            int rows;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@HotelFacilityID", HotelFacilityID);

            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("proc_GetFacilityDetails", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode UpdateHotelFacility(string HotelFacilityName, string FacilityIcon, Int64 id)
        {
            int rows = 0;
            //string UniqueCode = CountryCode + GenerateRandomString(4);
            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@id", id);
            sqlParams[1] = new SqlParameter("@HotelFacilityName", HotelFacilityName);
            sqlParams[2] = new SqlParameter("@FacilityIcon", FacilityIcon);

            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("proc_updateFacility", out rows, sqlParams);
            return retCode;
        }
    }
}