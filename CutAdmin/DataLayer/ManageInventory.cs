﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CommonLib.Response;
using CutAdmin.BL;
using System.Globalization;
using System.Data;
namespace CutAdmin.DataLayer
{
    public class ManageInventory : InventoryManager
    {
        public class Room
        {
            public string RoomName { get; set; }
            public Int64 RoomTypeID { get; set; }
        }
        public class Inventory
        {
            public string HotelCode { get; set; }
            public string HotelName { get; set; }
            public string Address { get; set; }
            public List<date> Dates { get; set; }
            public List<RoomType> ListRoom { get; set; }
        }
       public static List<DateTime> RateDate{ get; set; }
       public static List<Inventory> GetInventory(string CheckIn,string CheckOut)
       {
           List<Inventory> ListInventory = new List<Inventory>();
           try
           {
               using(var db  = new DBHandlerDataContext())
               {
                   DateTime From = DateTime.ParseExact(CheckIn, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                   DateTime To = DateTime.ParseExact(CheckOut, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                   Int64 SupplierID = AccountManager.GetSupplierByUser();
                   RateDate = new List<DateTime>();
                   DateTime targetDate = From;
                   while (targetDate <= To)
                   {
                       RateDate.Add(targetDate);
                       targetDate = targetDate.AddDays(1);
                   }
                   var arrHotels = (from objHotel in db.tbl_CommonHotelMasters
                                    join objInvent in db.tbl_CommonHotelInventories on objHotel.sid equals Convert.ToInt64(objInvent.HotelCode)
                                    where objInvent.SupplierId == SupplierID.ToString()
                                    select new
                                    {
                                        objInvent.HotelCode,
                                        objHotel.HotelName,
                                        Address = objHotel.HotelAddress,
                                        Rating = objHotel.HotelCategory
                                    }).Distinct().ToList();
                   foreach (var objHotel in arrHotels)
                   {

                       var arrRooms = (from objRoom in db.tbl_commonRoomDetails
                                       join objRoomType in db.tbl_commonRoomTypes on objRoom.RoomTypeId equals objRoomType.RoomTypeID
                                       where objRoom.HotelId == Convert.ToInt64(objHotel.HotelCode)
                                       select new Room
                                       {
                                           RoomTypeID =  objRoomType.RoomTypeID,
                                           RoomName = objRoomType.RoomType,
                                       }).ToList();
                       List<RoomType> ListRooms = new List<RoomType>();
                       double noGDays = (To - From).TotalDays;
                       foreach (var objRoom in arrRooms)
                       {
                           List<date> ListDate = new List<date>();
                           for (int i = 0; i <= noGDays; i++)
                           {
                               string Type="";
                               string Date = From.AddDays(i).ToString("dd-MM-yyyy");
                               var listInventory = (from obj in db.tbl_CommonHotelInventories where obj.HotelCode == objHotel.HotelCode.ToString() && obj.Month == Date.Split('-')[1] && obj.Year == Date.Split('-')[2] && obj.RoomType == objRoom.RoomTypeID.ToString() && obj.SupplierId== SupplierID.ToString()  select obj).ToList();
                               Int64 Count = 0;
                               string InventoryType = "";
                               if(listInventory.Count !=0 )
                               {
                                   ListtoDataTable lsttodt = new ListtoDataTable();
                                   DataTable dt = lsttodt.ToDataTable(listInventory);
                               
                                   string date = "";
                                   if (From.AddDays(i).ToString("dd").StartsWith("0"))
                                       date = "Date_" + From.AddDays(i).ToString("dd").TrimStart('0');
                                   else
                                       date = "Date_" + From.AddDays(i).ToString("dd");
                                   string noRooms = dt.Rows[0][date].ToString();
                                   if (noRooms != "")
                                   {
                                       string noTotalRooms = noRooms.Split('_')[1];
                                       if (noTotalRooms != "")
                                       {
                                           Count = Count + Convert.ToInt64(noTotalRooms);
                                       }
                                   }
                               }
                               if(listInventory.Count !=0)
                               {
                                   Type = listInventory.FirstOrDefault().RateType;
                                   InventoryType = listInventory.FirstOrDefault().InventoryType;
                               }
                             
                               ListDate.Add(new date
                               {
                                   datetime = From.AddDays(i).ToString("dd-MM-yyyy"),
                                   day = From.AddDays(0).Day.ToString(),
                                   NoOfCount = Count,
                                   Type = Type,
                                   InventoryType = InventoryType,
                               });
                           }
                           ListRooms.Add(new RoomType
                           {
                               RoomTypeName = objRoom.RoomName,
                               RoomTypeId = objRoom.RoomTypeID.ToString(),
                               Dates = ListDate
                           });
                       }
              
                       if (ListRooms.Count != 0)
                       {
                           ListInventory.Add(new Inventory
                           {
                               HotelCode = objHotel.HotelCode,
                               HotelName = objHotel.HotelName,
                               Address = objHotel.Address,
                               ListRoom = ListRooms,
                           });
                       }
                   }
               }
           }
           catch (Exception ex)
           {
               
               throw new Exception(ex.Message);
           }
           return ListInventory;
       }

    }
}