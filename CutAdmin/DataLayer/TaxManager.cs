﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CutAdmin.BL;
using CommonLib.Response;
using CutAdmin.dbml;
namespace CutAdmin.DataLayer
{
    public class TaxManager
    {
        //static dbTaxHandlerDataContext dbTax = new dbTaxHandlerDataContext();
        //static DBHandlerDataContext db = new DBHandlerDataContext();
        public static bool SaveTaxDetails(List<Comm_TaxMapping> ListTax)
        {
            using (var dbTax = new dbTaxHandlerDataContext())
            {
                bool retCode = false;
                var oldRates = (from obj in dbTax.Comm_TaxMappings
                                where obj.HotelID == ListTax.First().HotelID
                                    && obj.ServiceID == ListTax.First().ServiceID
                                    && obj.IsAddOns == ListTax.First().IsAddOns
                                select obj).ToList();
                if (oldRates.Count != 0)
                {
                    dbTax.Comm_TaxMappings.DeleteAllOnSubmit(oldRates);
                    dbTax.SubmitChanges();
                }
                dbTax.Comm_TaxMappings.InsertAllOnSubmit(ListTax);
                dbTax.SubmitChanges();
                return retCode;
            }
        }

        public static bool SaveTaxDetails(List<Comm_AddOnsRate> ListAddOns)
        {
            using (var dbTax = new dbTaxHandlerDataContext())
            {
                bool retCode = false;
                var oldAddOns = (from obj in dbTax.Comm_AddOnsRates
                                 where obj.HotelID == ListAddOns.First().HotelID
                                     && obj.RateID == ListAddOns.First().RateID
                                 select obj).ToList();
                if (oldAddOns.Count != 0)
                {
                    dbTax.Comm_AddOnsRates.DeleteAllOnSubmit(oldAddOns);
                    dbTax.SubmitChanges();
                }
                dbTax.Comm_AddOnsRates.InsertAllOnSubmit(ListAddOns);
                dbTax.SubmitChanges();
                return retCode;
            }
        }


        public static List<TaxRate> GetRacRates(List<TaxRate> ListRates, float Total)
        {
            List<TaxRate> arrRate = new List<TaxRate>();
            try
            {
                foreach (var objRate in ListRates)
                {
                    List<Tax> arrTax = new List<Tax>();
                    float TotalRate = 0;
                    float BaseRate = Total;
                    foreach (var obj in objRate.TaxOn)
                    {
                        Tax sTax = new Tax();

                        if (objRate.TaxOn.Count == 1)
                        {
                            sTax.TaxName = obj.TaxName;
                            sTax.TaxRate = objRate.Per * Total / 100;
                        }
                        else
                        {
                            if (obj.ID == 0)
                            {
                                 sTax.TaxRate = Total;
                                 sTax.TaxName = obj.TaxName;
                            }
                               
                            else
                            {
                                sTax.ID = obj.ID;
                                sTax.TaxName = obj.TaxName;
                                BaseRate = (Total + arrRate.FirstOrDefault().TotalRate);
                                sTax.TaxRate = objRate.Per * BaseRate / 100;
                            }

                        }
                        arrTax.Add(sTax);
                    }
                    if (objRate.TaxOn.Count == 1)
                    {
                        TotalRate = arrTax.Select(d => d.TaxRate).ToList().Sum();
                    }
                    else
                        TotalRate = arrTax.Where(d => d.ID != 0).Select(r => r.TaxRate).ToList().Sum();

                    arrRate.Add(new TaxRate { 
                        BaseRate = BaseRate, 
                        ID = objRate.ID, 
                        Per = objRate.Per, 
                        RateName = 
                        objRate.RateName, 
                        TaxOn = arrTax,
                        TotalRate = TotalRate,
                        Type = objRate.Type
                    });
                }
            }
            catch
            {

            }
            return arrRate;

        }
//******************************************* Code for AddTaxces.aspx *********************************************************        
        public static object GetTaxByService(string Services)
        {
            object data = new object();
            try
            {
                using (var db = new dbAdminHelperDataContext())
                {

                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    data = (from obj in db.tbl_TAXes where obj.Service == Services select obj).ToList().OrderBy(d => d.tID).FirstOrDefault(); //&& obj.ParentID == objGlobalDefault.sid
                }
            }
            catch
            {
                throw new Exception("Not Valid Service");
            }
            return data;
        }
    }
}