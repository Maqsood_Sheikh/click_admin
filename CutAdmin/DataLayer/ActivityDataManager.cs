﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CutAdmin.DataLayer
{
    public class ActivityDataManager
    {
        public static List<Activity> listActivity { get; set; }
    }
    public class Activity
    {
        public string Aid { get; set; }
        public string Name { get; set; }

        public string Sub_Title { get; set; }

        public string Country { get; set; }
        public string City { get; set; }
        public string Location { get; set; }
        public string Desc { get; set; }
        public float MinPrice { get; set; }

        public float MaxPrice { get; set; }

        public List<string> TourType { get; set; }

        public List<ModeDetails> Mode { get; set; }

        public string ValidFrom { get; set; }

        public string ValidTo { get; set; }

        public string Status { get; set; }

        public List<string> Attractions { get; set; }

        public string sImages { get; set; }

        
    }
    public class ModeDetails
    {
        public string Type { get; set; }
        public string Priority { get; set; }

        public string Start { get; set; }

        public string End { get; set; }

        public string Pickup { get; set; }

        public string Duration { get; set; }

        public string PickupFrom { get; set; }

        public string sDays { get; set; }

        public string AdultRate { get; set; }

        public string ChildRate { get; set; }

        public string Inclusions { get; set; }

        public string Exclusions { get; set; }

        public string PricesNote { get; set; }

        public string ImportantNote { get; set; }

    }
}