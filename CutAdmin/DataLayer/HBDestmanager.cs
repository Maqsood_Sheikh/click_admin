﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CutAdmin
{
    public class HBDestmanager
    {
        public static DBHelper.DBReturnCode Get(string name, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@CountryCode", name);

            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_GetHotelBedDestination", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetDOT(string name, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Country", name);

            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_DoTwCountryCodeMap", out dtResult, sqlParams);
            return retCode;
        }


        public static List<Dictionary<string, object>> ConvertDataTable(DataTable dtResult)
        {
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dtResult.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dtResult.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return parentRow;
        }
    }
}