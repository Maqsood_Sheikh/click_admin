﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace CutAdmin.DataLayer
{
    public class RoomsManager
    {
        public static DBHelper.DBReturnCode GetRoomType(out DataTable dtResult)
        {
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_GetRoomType", out dtResult);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetRoomAmenities(out DataTable dtResult)
        {
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("proc_GetRoomAmunities", out dtResult);
            return retCode;
        }

    }
}