﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
namespace CutAdmin.DataLayer
{
    public class PropertyManager
    {

        public static DBHelper.DBReturnCode AddPropertyType(string PropertyType, string Description)
        {
            int rows = 0;
            //string UniqueCode = CountryCode + GenerateRandomString(4);
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@PropertyType", PropertyType);
            sqlParams[1] = new SqlParameter("@Description", Description);

            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_InsertPropertyType", out rows, sqlParams);
            return retCode;
        }



        public static DBHelper.DBReturnCode GetPropertyType(out DataTable dtResult)
        {
            dtResult = null;
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("proc_GetPropertyType", out dtResult);
            return retCode;
        }


        public static DBHelper.DBReturnCode DeletePropertyType(string PropertyTypeID)
        {
            int rows;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@PropertyTypeID", PropertyTypeID);

            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("proc_DeletePropertyType", out rows, sqlParams);
            return retCode;
        }


        public static DBHelper.DBReturnCode GetDetails(string PropertyTypeID, out DataTable dtResult)
        {
            int rows;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@PropertyTypeID", PropertyTypeID);

            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("proc_GetPropertyDetails", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode UpdatePropertyType(string PropertyType, string Description, Int64 id)
        {
            int rows = 0;
            //string UniqueCode = CountryCode + GenerateRandomString(4);
            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@id", id);
            sqlParams[1] = new SqlParameter("@PropertyType", PropertyType);
            sqlParams[2] = new SqlParameter("@Description", Description);

            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("proc_updatePropertyType", out rows, sqlParams);
            return retCode;
        }


    }
}