﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CommonLib.Response;

namespace CutAdmin.DataLayer
{
    public class RoomFilter
    {
        public List<string> RoomType { get; set; }

        public List<string> MealType { get; set; }

        public float MinPrice { get; set; }

        public float MaxPrice { get; set; }


        public static RoomFilter GenrateFilter(string Search)
        {
            List<CommonLib.Response.HotelOccupancy> ListRates = GenralManager.GetOccupancyInfo(Search);
            RoomFilter objFilter = new RoomFilter();
            objFilter.RoomType = new List<string>();
            objFilter.MealType = new List<string>();
            List<float> Rates = new List<float>();
            try
            {
                foreach (HotelOccupancy objOccupancy in ListRates)
                {
                    var ListRoomType = objOccupancy.Rooms.Select(d => d.RoomTypeName).ToList();
                    foreach (var RoomType in ListRoomType)
                    {
                        if (!objFilter.RoomType.Contains(RoomType))
                            objFilter.RoomType.Add(RoomType);
                    }

                    var ListMealType = objOccupancy.Rooms.Select(d => d.RoomDescription).ToList();
                    foreach (var MealType in ListMealType)
                    {
                        if (!objFilter.MealType.Contains(MealType))
                            objFilter.MealType.Add(MealType);
                    }

                    var sRates = objOccupancy.Rooms.Select(d => d.Total).ToList();
                    foreach (var objrate in sRates)
                    {
                        Rates.Add(objrate);
                    }

                }
                objFilter.MinPrice = Rates.Min();
                objFilter.MaxPrice = Rates.Max();
            }
            catch
            {

            }
            return objFilter;
        }

        public static List<HotelOccupancy> FilterRate(string Search, List<string> RoomType, List<string> MealType, float MinPrice, float MaxPrice)
        {
            List<HotelOccupancy> SearchRates = GenralManager.GetOccupancyInfo(Search);
            List<HotelOccupancy> ListRates = new List<HotelOccupancy>();
            try
            { 
                    List<RoomType> ListRoom = new List<RoomType>();
                    foreach (HotelOccupancy obj in SearchRates)
                    {
                        foreach (var objRoom in obj.Rooms)
                        {
                            ListRoom.Add(objRoom);
                            ListRoom.Last().AdultCount = obj.AdultCount;
                            ListRoom.Last().ChildCount = obj.ChildCount;
                            ListRoom.Last().ChildAges = obj.ChildAges;
                        }
                        ListRates.Add(new HotelOccupancy
                        {
                            AdultCount = obj.AdultCount,
                            ChildAges = obj.ChildAges,
                            ChildCount = obj.ChildCount,
                            GuestList = new List<Customers>(),
                            RoomCount = obj.RoomCount,
                            RoomNo = obj.RoomNo,
                            Rooms = new List<RoomType>(),
                        });
                    }
                /*Rate Group*/
                    List<RoomType> SearhRoom = new List<RoomType>();
                    SearhRoom = ListRoom;
                    if (RoomType.Count != 0)
                    {
                        List<RoomType> SearhRoomType = new List<RoomType>();
                        for (int i = 0; i < RoomType.Count; i++)
                        {
                            var Rooms = SearhRoom.Where(d => d.RoomTypeName == RoomType[i]).ToList();
                            foreach (var Room in Rooms)
                            {
                                SearhRoomType.Add(Room);
                            }
                        }
                        SearhRoom = SearhRoomType;
                    }
                    if (MealType.Count != 0)
                    {
                        List<RoomType> SearhRoomMeal = new List<RoomType>();
                        for (int i = 0; i < MealType.Count; i++)
                        {
                            var Rooms = SearhRoom.Where(d => d.RoomDescription == MealType[i]).ToList();
                            foreach (var Room in Rooms)
                            {
                                SearhRoomMeal.Add(Room);
                            }
                        }
                        SearhRoom = SearhRoomMeal;
                    }
                    if (MinPrice != 0)
                    {
                        List<RoomType> SearhRoomMinPrice = new List<RoomType>();
                        var Rooms = SearhRoom.Where(d => d.Total >= MinPrice).ToList();
                        foreach (var Room in Rooms)
                        {
                            SearhRoomMinPrice.Add(Room);
                        }
                        SearhRoom = SearhRoomMinPrice;
                    }
                    if (MaxPrice != 0)
                    {
                        List<RoomType> SearhRoomMaxPrice = new List<RoomType>();
                        var Rooms = SearhRoom.Where(d => d.Total <= MaxPrice).ToList();
                        foreach (var Room in Rooms)
                        {
                            SearhRoomMaxPrice.Add(Room);
                        }
                        SearhRoom = SearhRoomMaxPrice;
                    }

                    foreach (var obj in ListRates)
                    {
                         var arrRate = SearhRoom.Where(d => d.AdultCount == obj.AdultCount && d.ChildCount == obj.ChildCount && d.ChildAges == obj.ChildAges).Distinct().ToList();
                         obj.Rooms = arrRate;
                    }

            }
            catch
            {

            }
            return ListRates;
        }
    }
}