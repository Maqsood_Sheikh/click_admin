﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CutAdmin.DataLayer
{
    public class DOTWManager
    {
        public static DBHelper.DBReturnCode GetRatingById(int id, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@RatingId", id);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_DOTWGetRatingById", out dtResult, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetDOTWCityCode(string city, out DataTable dtResult)
        {
            //string[] CityName = city.Split(' ');
            string[] CityName = city.Replace("-", " ").Split(' ');
            SqlParameter[] SQLParams = new SqlParameter[1];
            SQLParams[0] = new SqlParameter("@City", CityName[0]);
            //SQLParams[1] = new SqlParameter("@countryName", Country);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_tbl_DoTWAllCitiesLoadByCity", out dtResult, SQLParams);
            if (dtResult.Rows.Count > 1)
            {
                DataRow[] Row = null; Row = dtResult.Select("name='" + city + "'");
                if (Row.Length != 0)
                {
                    dtResult = Row.CopyToDataTable();
                }
                else
                {
                    city = "";
                    for (int i = 0; i < CityName.Length; i++)
                    {
                        city += CityName[i] + " ";
                        if (i == 1)
                        {
                            Row = dtResult.Select("name like '%" + city + "%'");
                            if (Row.Length != 0)
                            {
                                dtResult = Row.CopyToDataTable();
                            }
                            break;
                        }
                    }
                }
            }
            return retCode;
        }

        public static DBHelper.DBReturnCode GetDOTWNationalityCode(string Nationality, out DataTable dtResult)
        {
            SqlParameter[] SQLParams = new SqlParameter[1];
            SQLParams[0] = new SqlParameter("@Country", Nationality);

            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_DoTwCountryCode", out dtResult, SQLParams);
            return retCode;
        }
    }
}