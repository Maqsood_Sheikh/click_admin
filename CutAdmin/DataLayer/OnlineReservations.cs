﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace CutAdmin.DataLayer
{
    public class OnlineReservations
    {
        public int Sid { get; set; }
        public string ReservationID { get; set; }
        public string ReservationDate { get; set; }
        public string AgencyName { get; set; }
        public string Status { get; set; }
        public string bookingname { get; set; }
        public decimal? TotalFare { get; set; }
        public int TotalRooms { get; set; }
        public string HotelName { get; set; }
        public string sCheckIn { get; set; }
        public string CheckIn { get; set; }
        public string CheckOut { get; set; }
        public string City { get; set; }
        public int? Children { get; set; }
        public string BookingStatus { get; set; }
        public int? NoOfAdults { get; set; }
        public string Source { get; set; }
        public Int64 Uid { get; set; }
        public string LatitudeMGH { get; set; }
        public string LongitudeMGH { get; set; }
        public string CurrencyCode { get; set; }
        public string DeadLine { get; set; }
        public string InvoiceID { get; set; }
        public bool? IsConfirm { get; set; }

        //static   DBHandlerDataContext DB = new DBHandlerDataContext();
        public static List<OnlineReservations> OnlineBookingList()
        {
            List<OnlineReservations> ListReservation = new List<OnlineReservations>();
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = AccountManager.GetUserByLogin();
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    ListReservation = (from obj in DB.tbl_HotelReservations
                                       join AgName in DB.tbl_AdminLogins on obj.Uid equals AgName.sid
                                       where obj.Uid == Uid
                                       select new OnlineReservations
                                       {
                                           AgencyName = AgName.AgencyName,
                                           bookingname = obj.bookingname,
                                           BookingStatus = obj.BookingStatus,
                                           CurrencyCode = AgName.CurrencyCode,
                                           IsConfirm = obj.IsConfirm,
                                           CheckIn = obj.CheckIn,
                                           //sCheckIn = DateTime.ParseExact(obj.CheckIn, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"),
                                           CheckOut = obj.CheckOut,
                                           Children = obj.Children,
                                           City = obj.City,
                                           HotelName = obj.HotelName,
                                           LatitudeMGH = obj.LatitudeMGH,
                                           LongitudeMGH = obj.LongitudeMGH,
                                           NoOfAdults = obj.NoOfAdults,
                                           ReservationDate = obj.ReservationDate,
                                           ReservationID = obj.ReservationID,
                                           Sid = obj.Sid,
                                           Source = obj.Source,
                                           Status = obj.Status,
                                           TotalFare = obj.TotalFare,
                                           DeadLine = obj.DeadLine,
                                           TotalRooms = Convert.ToInt16(obj.TotalRooms),
                                           Uid = Convert.ToInt64(obj.Uid)

                                       }).Distinct().ToList();
                    if (objGlobalDefault.UserType != "Agent")
                    {
                        var arrResrvation = (from obj in DB.tbl_HotelReservations
                                             join AgName in DB.tbl_AdminLogins on obj.Uid equals AgName.sid
                                             where AgName.ParentID == Uid && obj.BookingStatus != "GroupRequest"
                                             select new OnlineReservations
                                             {
                                                 AgencyName = AgName.AgencyName,
                                                 bookingname = obj.bookingname,
                                                 BookingStatus = obj.BookingStatus,
                                                 CurrencyCode = AgName.CurrencyCode,
                                                 CheckIn = obj.CheckIn,
                                                 CheckOut = obj.CheckOut,
                                                 //sCheckIn = DateTime.ParseExact(obj.CheckIn, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"),
                                                 Children = obj.Children,
                                                 City = obj.City,
                                                 IsConfirm = obj.IsConfirm,
                                                 HotelName = obj.HotelName,
                                                 LatitudeMGH = obj.LatitudeMGH,
                                                 LongitudeMGH = obj.LongitudeMGH,
                                                 NoOfAdults = obj.NoOfAdults,
                                                 ReservationDate = obj.ReservationDate,
                                                 ReservationID = obj.ReservationID,
                                                 Sid = obj.Sid,
                                                 Source = obj.Status,
                                                 Status = obj.Status,
                                                 TotalFare = obj.TotalFare,
                                                 TotalRooms = Convert.ToInt16(obj.TotalRooms),
                                                 Uid = Convert.ToInt64(obj.Uid),
                                                 DeadLine = obj.DeadLine,
                                                 InvoiceID=obj.InvoiceID

                                             }).ToList().Distinct();
                        foreach (var obj in arrResrvation)
                        {
                            ListReservation.Add(obj);
                        }
                    }
                    ListReservation = ListReservation.OrderByDescending(s => s.ReservationID).ToList();
                }
            }
            catch (Exception)
            {

                throw;
            }
            return ListReservation;
        }

        public static List<OnlineReservations> OnlineGroupBookingList()
        {
            List<OnlineReservations> ListReservation = new List<OnlineReservations>();
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = AccountManager.GetUserByLogin();
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    ListReservation = (from obj in DB.tbl_HotelReservations
                                       join AgName in DB.tbl_AdminLogins on obj.Uid equals AgName.sid
                                       where obj.Uid == Uid && obj.BookingStatus == "GroupRequest"
                                       select new OnlineReservations
                                       {
                                           AgencyName = AgName.AgencyName,
                                           bookingname = obj.bookingname,
                                           BookingStatus = obj.BookingStatus,
                                           CurrencyCode = AgName.CurrencyCode,
                                           IsConfirm = obj.IsConfirm,
                                           CheckIn = obj.CheckIn,
                                           sCheckIn = DateTime.ParseExact(obj.CheckIn, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"),
                                           CheckOut = obj.CheckOut,
                                           Children = obj.Children,
                                           City = obj.City,
                                           HotelName = obj.HotelName,
                                           LatitudeMGH = obj.LatitudeMGH,
                                           LongitudeMGH = obj.LongitudeMGH,
                                           NoOfAdults = obj.NoOfAdults,
                                           ReservationDate = obj.ReservationDate,
                                           ReservationID = obj.ReservationID,
                                           Sid = obj.Sid,
                                           Source = obj.Status,
                                           Status = obj.Status,
                                           TotalFare = obj.TotalFare,
                                           TotalRooms = Convert.ToInt16(obj.TotalRooms),
                                           Uid = Convert.ToInt64(obj.Uid)

                                       }).Distinct().ToList();
                    if (objGlobalDefault.UserType != "Agent")
                    {
                        var arrResrvation = (from obj in DB.tbl_HotelReservations
                                             join AgName in DB.tbl_AdminLogins on obj.Uid equals AgName.sid
                                             where AgName.ParentID == Uid && obj.BookingStatus == "GroupRequest"
                                             select new OnlineReservations
                                             {
                                                 AgencyName = AgName.AgencyName,
                                                 bookingname = obj.bookingname,
                                                 BookingStatus = obj.BookingStatus,
                                                 CurrencyCode = AgName.CurrencyCode,
                                                 CheckIn = obj.CheckIn,
                                                 CheckOut = obj.CheckOut,
                                                 sCheckIn = DateTime.ParseExact(obj.CheckIn, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"),
                                                 Children = obj.Children,
                                                 City = obj.City,
                                                 IsConfirm = obj.IsConfirm,
                                                 HotelName = obj.HotelName,
                                                 LatitudeMGH = obj.LatitudeMGH,
                                                 LongitudeMGH = obj.LongitudeMGH,
                                                 NoOfAdults = obj.NoOfAdults,
                                                 ReservationDate = obj.ReservationDate,
                                                 ReservationID = obj.ReservationID,
                                                 Sid = obj.Sid,
                                                 Source = obj.Status,
                                                 Status = obj.Status,
                                                 TotalFare = obj.TotalFare,
                                                 TotalRooms = Convert.ToInt16(obj.TotalRooms),
                                                 Uid = Convert.ToInt64(obj.Uid)

                                             }).ToList().Distinct();
                        foreach (var obj in arrResrvation)
                        {
                            ListReservation.Add(obj);
                        }
                    }
                    ListReservation = ListReservation.OrderByDescending(s => s.ReservationID).ToList();
                }
            }
            catch (Exception)
            {

                throw;
            }
            return ListReservation;
        }
    }


}