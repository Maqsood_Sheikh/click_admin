﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CutAdmin.DataLayer
{
    public class DestinationManager
    {
        public static DBHelper.DBReturnCode Get(string name, string lang, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@Destination", name);
            sqlParams[1] = new SqlParameter("@LanguageCode", lang);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_GetDestination", out dtResult, sqlParams);
            return retCode;
        }
        #region Get Active Supplier
        public static DBHelper.DBReturnCode GetActiveSupplier(out DataTable dtResult)
        {
            dtResult = null;
            return DBHelper.GetDataTable("Proc_tbl_APIDetailsLoadBySupplier", out dtResult);
        }
        #endregion

        public static DBHelper.DBReturnCode GetNationalityCOR(out DataTable dtResult)
        {
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_NationalityCOR", out dtResult);
            return retCode;
        }
    }
}