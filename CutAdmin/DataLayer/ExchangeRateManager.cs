﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Transactions;
using System.Web;
using OpenQA.Selenium;
using OpenQA.Selenium.PhantomJS;

namespace CutAdmin.DataLayer
{
    public class CurrencyList
    {
        public Int64 Sid { get; set; }
        public string ExchageCurrency { get; set; }
        public string PreferredCurrency { get; set; }
        public decimal Amount { get; set; }
        public decimal Percentage { get; set; }
        public ExchaneList ExchaneList { get; set; }
    }
    public class ExchaneList
    {
        public string Currency { get; set; }
        public string Preferred { get; set; }
        public decimal ExchaneRate { get; set; }
        public decimal MarkupRate { get; set; }
        public decimal TotalExchange { get; set; }
    }
    public class ExchangeRateManager
    {
        #region Add Update ExcgangeRate
        public static DBHelper.DBReturnCode GetLastRecordDate(out DataTable dtResult)
        {
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_ExChangeRateLoadAll", out dtResult);
            return retCode;
        }

        public static DBHelper.DBReturnCode InsertExchangeRate(List<string> Currency, string ApplicableDate, List<decimal> INR)
        {
            Int64 ID = 0;
            int rowsAffected = 0;
            decimal FCY = 1;
            string MerchantID = "";
            bool Flag = true;
            string MerchantURL = "";
            DateTime dt2 = DateTime.ParseExact(ApplicableDate, "d/M/yyyy", CultureInfo.InvariantCulture);
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.SUCCESS;
            //DateTime date = DateTime.ParseExact(ApplicableDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            for (int i = 0; i < Currency.Count; i++)
            {
                SqlParameter[] sqlParams = new SqlParameter[8];
                sqlParams[0] = new SqlParameter("@ID", ID);
                sqlParams[1] = new SqlParameter("@Currency", Currency[i]);
                sqlParams[2] = new SqlParameter("@FCY", FCY);
                sqlParams[3] = new SqlParameter("@INR", INR[i]);
                sqlParams[4] = new SqlParameter("@ApplicableDate", dt2);
                sqlParams[5] = new SqlParameter("@MerchantID", MerchantID);
                sqlParams[6] = new SqlParameter("@Flag", Flag);
                sqlParams[7] = new SqlParameter("@MerchantURL", MerchantURL);
                retCode = DBHelper.ExecuteNonQuery("Proc_tbl_ExChangeRateAddUpdate", out rowsAffected, sqlParams);
            }
            return retCode;
        }

        //public static DBHelper.DBReturnCode InsertExchangeRate(string Currency, string ApplicableDate, decimal INR)
        //{
        //    Int64 ID = 0;
        //    int rowsAffected = 0;
        //    decimal FCY = 1;
        //    string MerchantID = "";
        //    bool Flag = true;
        //    string MerchantURL = "";
        //    //DateTime date = DateTime.ParseExact(ApplicableDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //    SqlParameter[] sqlParams = new SqlParameter[8];
        //    sqlParams[0] = new SqlParameter("@ID", ID);
        //    sqlParams[1] = new SqlParameter("@Currency", Currency);
        //    sqlParams[2] = new SqlParameter("@FCY", FCY);
        //    sqlParams[3] = new SqlParameter("@INR", INR);
        //    sqlParams[4] = new SqlParameter("@ApplicableDate", ApplicableDate);
        //    sqlParams[5] = new SqlParameter("@MerchantID", MerchantID);
        //    sqlParams[6] = new SqlParameter("@Flag", Flag);
        //    sqlParams[7] = new SqlParameter("@MerchantURL", MerchantURL);
        //    DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_ExChangeRateAddUpdate", out rowsAffected, sqlParams);
        //    return retCode;
        //}

        public static DBHelper.DBReturnCode UpdateExchangeRate(Int64 ID, decimal INR)
        {
           GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

            string Role = (objGlobalDefaults.RoleID).ToString();
            string UpdateBy = objGlobalDefaults.uid;


            //if (Role == "1")
            //{
            //    UpdateBy = objGlobalDefaults.ContactPerson;
            //}
            //if (Role == "2")
            //{
            //    UpdateBy = objGlobalDefaults.ContactPerson;
            //}
            //if (Role == "3")
            //{
            //    UpdateBy = objGlobalDefaults.ContactPerson;
            //}
            //if (Role == "4")
            //{
            //    UpdateBy = objGlobalDefaults.ContactPerson;
            //}
            string Currency = "";
            int rowsAffected = 0;
            decimal FCY = 1;
            string ApplicableDate = "";
            string MerchantID = "";
            bool Flag = true;
            string MerchantURL = "";
            SqlParameter[] sqlParams = new SqlParameter[10];
            sqlParams[0] = new SqlParameter("@ID", ID);
            sqlParams[1] = new SqlParameter("@Currency", Currency);
            sqlParams[2] = new SqlParameter("@FCY", FCY);
            sqlParams[3] = new SqlParameter("@INR", INR);
            sqlParams[4] = new SqlParameter("@ApplicableDate", ApplicableDate);
            sqlParams[5] = new SqlParameter("@MerchantID", MerchantID);
            sqlParams[6] = new SqlParameter("@Flag", Flag);
            sqlParams[7] = new SqlParameter("@MerchantURL", MerchantURL);
            sqlParams[8] = new SqlParameter("@UpdateBy", UpdateBy);
            sqlParams[9] = new SqlParameter("@LastUpdateDt", DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_ExChangeRateAddUpdate", out rowsAffected, sqlParams);
            return retCode;
        }

        //public static DBHelper.DBReturnCode UpdateExchangeRate(List<Int64> ID, List<decimal> INR)
        //{
        //  GlobalDefault objGlobalDefaults = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];

        //   string UpdateBy = (objGlobalDefaults.sid).ToString();
        //    string Currency = "";
        //    int rowsAffected = 0;
        //    decimal FCY = 1;
        //    string ApplicableDate = "";
        //    string MerchantID = "";
        //    bool Flag = true;
        //    string MerchantURL = "";
        //    DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.SUCCESS;


        //    SqlParameter[] sqlParams = new SqlParameter[9];
        //    for (int i = 0; i < ID.Count; i++)
        //    {
        //        sqlParams[0] = new SqlParameter("@ID", ID[i]);
        //        sqlParams[1] = new SqlParameter("@Currency", Currency);
        //        sqlParams[2] = new SqlParameter("@FCY", FCY);
        //        sqlParams[3] = new SqlParameter("@INR", INR[i]);
        //        sqlParams[4] = new SqlParameter("@ApplicableDate", ApplicableDate);
        //        sqlParams[5] = new SqlParameter("@MerchantID", MerchantID);
        //        sqlParams[6] = new SqlParameter("@Flag", Flag);
        //        sqlParams[7] = new SqlParameter("@MerchantURL", MerchantURL);
        //        sqlParams[8] = new SqlParameter("@UpdateBy", UpdateBy);

        //        retCode = DBHelper.ExecuteNonQuery("Proc_tbl_ExChangeRateAddUpdate", out rowsAffected, sqlParams);
        //    }

        //    return retCode;
        //}

        public static DBHelper.DBReturnCode GetForeignExchange(string ApplicableDate, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];

            //DateTime dt = Convert.ToDateTime(ApplicableDate);
            DateTime dt = DateTime.ParseExact(ApplicableDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            sqlParams[0] = new SqlParameter("@ApplicableDate", dt);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_ExChangeRateLoadByKey", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetForeignExchangeRate(string currency, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Currency", currency);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_ExChangeRateLoadByRate", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetCurrentForeignExchangeRate(out DataTable dtResult)
        {
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_ExChangeRateLoadByRecent", out dtResult);
            return retCode;
        }

        #endregion

        #region Exchange Log
        public static DBHelper.DBReturnCode GeExchangLog(out DataSet dsResult)
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@sid", objGlobalDefault.sid);

            DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_tbl_ExchangeRateLogLoadAll", out dsResult, sqlParams);
            DataRow[] row = null;

            return retCode;
        }
        public static DBHelper.DBReturnCode UpdateExchangeRate(Int64[] MarkupSid, Int64[] LogSid, string[] Currency, decimal[] ExchangeRate, decimal[] MarkupAmt, decimal[] MarkupPer)
        {
            int Rowseffected = 0;
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string Update = DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);

            using (TransactionScope objTransactionScope = new TransactionScope())
            {
                for (int i = 0; i < MarkupAmt.Length; i++)
                {
                    SqlParameter[] sqlParams = new SqlParameter[4];
                    sqlParams[0] = new SqlParameter("@Amount", MarkupAmt[i]);
                    sqlParams[1] = new SqlParameter("@Percent", MarkupPer[i]);
                    sqlParams[2] = new SqlParameter("@UpdateBy", objGlobalDefault.uid);
                    sqlParams[3] = new SqlParameter("@sid", MarkupSid[i]);
                    retCode = DBHelper.ExecuteNonQuery("Proc_tbl_ExchangeMarkupUpdate", out Rowseffected, sqlParams);
                }
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {
                    for (int i = 0; i < ExchangeRate.Length; i++)
                    {
                        decimal TotalAmt = Math.Round((ExchangeRate[i] + MarkupRate(ExchangeRate[i], MarkupAmt[i], MarkupPer[i])), 2, MidpointRounding.AwayFromZero);
                        SqlParameter[] sqlParams = new SqlParameter[6];
                        sqlParams[0] = new SqlParameter("@Currency", Currency[i]);
                        sqlParams[1] = new SqlParameter("@sid", LogSid[i]);
                        sqlParams[2] = new SqlParameter("@MarkupRate", MarkupRate(ExchangeRate[i], MarkupAmt[i], MarkupPer[i]));
                        sqlParams[3] = new SqlParameter("@TotalExchange", TotalAmt);
                        sqlParams[4] = new SqlParameter("@UpdateBy", objGlobalDefault.uid);
                        sqlParams[5] = new SqlParameter("@UpdateDate", Update);
                        retCode = DBHelper.ExecuteNonQuery("Proc_ExchangeRateLogUpdate", out Rowseffected, sqlParams);
                    }
                    if (retCode == DBHelper.DBReturnCode.SUCCESS)
                    {
                        for (int i = 0; i < ExchangeRate.Length; i++)
                        {
                            decimal TotalAmt = Math.Round((ExchangeRate[i] + MarkupRate(ExchangeRate[i], MarkupAmt[i], MarkupPer[i])), 2, MidpointRounding.AwayFromZero);
                            SqlParameter[] sqlParams = new SqlParameter[5];
                            sqlParams[0] = new SqlParameter("@Currency", Currency[i]);
                            sqlParams[1] = new SqlParameter("@INR", TotalAmt);
                            sqlParams[2] = new SqlParameter("@ApplicableDate", "");
                            sqlParams[3] = new SqlParameter("@UpdateBy", objGlobalDefault.uid);
                            sqlParams[4] = new SqlParameter("@LastUpdateDt", Update);
                            retCode = DBHelper.ExecuteNonQuery("Proc_tbl_ExChangeRateAddUpdateByMarkupSet", out Rowseffected, sqlParams);
                        }
                        if (retCode == DBHelper.DBReturnCode.SUCCESS)
                            objTransactionScope.Complete();
                        else
                            objTransactionScope.Dispose();
                    }
                    else
                    {
                        objTransactionScope.Dispose();
                    }

                }
                else
                {
                    objTransactionScope.Dispose();
                }

            }
            return retCode;
        }
        public static void GetMarkupAmt(decimal[] ExchangeRate, decimal[] MarkupAmt, decimal[] MarkupPer, out decimal[] Amt, out decimal[] TotalExchange)
        {
            Amt = new decimal[ExchangeRate.Length]; TotalExchange = new decimal[ExchangeRate.Length];
            for (int i = 0; i < ExchangeRate.Length; i++)
            {
                Amt[i] = MarkupRate(ExchangeRate[i], MarkupAmt[i], MarkupPer[i]);
                TotalExchange[i] = Math.Round((ExchangeRate[i] + Amt[i]), 2, MidpointRounding.AwayFromZero);

            }
        }
        public static decimal MarkupRate(decimal Amt, decimal MarkupAmt, decimal MarkupPer)
        {
            decimal Rate = 0;
            decimal ActualAmt = MarkupAmt;
            decimal ActualPerAmt = (Amt * MarkupPer / 100);
            if (ActualAmt > ActualPerAmt)
                Rate = ActualAmt;
            else
                Rate = ActualPerAmt;
            return Rate;
        }
        #endregion

        #region GetOnline Rate
        public static CurrencyList[] arrCurrency { get; set; }
        public static IWebDriver driver { get; set; }
        public static string Path { get; set; }
        public static DBHelper.DBReturnCode GetExchangeRate()
        {
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = DBHelper.DBReturnCode.EXCEPTION;
            retcode = DBHelper.GetDataTable("Proc_ExchangeMarkupAll", out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                arrCurrency = new CurrencyList[dtResult.Rows.Count]; int i = 0;
                Path = HttpContext.Current.Server.MapPath(@"Browser\ExchangeRate\");
                driver = new PhantomJSDriver(Path);
                foreach (DataRow Row in dtResult.Rows)
                {
                    CurrencyList objCurrencyList = new CurrencyList();
                    objCurrencyList.Sid = Convert.ToInt64(Row["sid"]);
                    objCurrencyList.ExchageCurrency = Row["Currency"].ToString();
                    objCurrencyList.PreferredCurrency = Row["Preferred"].ToString();
                    objCurrencyList.Amount = Convert.ToDecimal(Row["Amount"]);
                    objCurrencyList.Percentage = Convert.ToDecimal(Row["Percentage"]);
                    objCurrencyList.ExchaneList = GetExchaneList(objCurrencyList.ExchageCurrency, objCurrencyList.PreferredCurrency, objCurrencyList.Amount, objCurrencyList.Percentage);
                    arrCurrency[i] = objCurrencyList; i++;
                }
                UpdateExchangeRate();
                driver.Dispose();
                //driver.Close();
            }
            return retcode;
        }
        public static ExchaneList GetExchaneList(string ExchangeCurrency, string Preferred, decimal MarkupAmt, decimal MarkupPerentage)
        {
            ExchaneList objExchaneList = new ExchaneList();

            string Url = "http://www.xe.com/currencyconverter/convert/?Amount=1&From=" + ExchangeCurrency + "&To=" + Preferred;
            driver.Navigate().GoToUrl(Url);
            while (driver.Url != Url) { }
            IWebElement Status = driver.FindElement(By.ClassName("uccResultAmount"));
            objExchaneList.ExchaneRate = Convert.ToDecimal(Status.Text);
            objExchaneList.Currency = ExchangeCurrency;
            objExchaneList.Preferred = Preferred;
            objExchaneList.MarkupRate = MarkupRate(objExchaneList.ExchaneRate, MarkupAmt, MarkupPerentage);
            return objExchaneList;

        }
        public static DBHelper.DBReturnCode UpdateExchangeRate()
        {
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            string Update = DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);
            foreach (CurrencyList objCurrencyList in arrCurrency)
            {
                int Rowseffected = 0;
                decimal TotalAmt = Math.Round((objCurrencyList.ExchaneList.ExchaneRate + objCurrencyList.ExchaneList.MarkupRate), 2, MidpointRounding.AwayFromZero);
                if (TotalAmt != 0)
                {
                    SqlParameter[] sqlParams = new SqlParameter[7];
                    sqlParams[0] = new SqlParameter("@Currency", objCurrencyList.ExchageCurrency);
                    sqlParams[1] = new SqlParameter("@Preferred", objCurrencyList.PreferredCurrency);
                    sqlParams[2] = new SqlParameter("@ExchangeRate", objCurrencyList.ExchaneList.ExchaneRate);
                    sqlParams[3] = new SqlParameter("@MarkupRate", objCurrencyList.ExchaneList.MarkupRate);
                    sqlParams[4] = new SqlParameter("@TotalExchange", TotalAmt);
                    sqlParams[5] = new SqlParameter("@UpdateBy", "System");
                    sqlParams[6] = new SqlParameter("@UpdateDate", Update);
                    retCode = DBHelper.ExecuteNonQuery("Proc_ExchangeRateLogAdd", out Rowseffected, sqlParams);
                    if (retCode == DBHelper.DBReturnCode.SUCCESS)
                    {
                        SqlParameter[] sqlParams1 = new SqlParameter[5];
                        sqlParams1[0] = new SqlParameter("@Currency", objCurrencyList.ExchageCurrency);
                        sqlParams1[1] = new SqlParameter("@INR", TotalAmt);
                        sqlParams1[2] = new SqlParameter("@ApplicableDate", "");
                        sqlParams1[3] = new SqlParameter("@UpdateBy", "System");
                        sqlParams1[4] = new SqlParameter("@LastUpdateDt", Update);
                        retCode = DBHelper.ExecuteNonQuery("Proc_tbl_ExChangeRateAddUpdateByMarkupSet", out Rowseffected, sqlParams1);
                    }
                }

            }
            return retCode;

        }
        #endregion

        //from Main datalayer ... Azhar
        public static float GetExchange()
        {
            float ExchangeRate = 0;
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string PreferedCurrency = objGlobalDefault.Currency;
            AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
            if (HttpContext.Current.Session["AgentDetailsOnAdmin"] != null)
            {
                if (objAgentDetailsOnAdmin.Currency == "INR")
                {
                    ExchangeRate = 1;
                }
                else
                {
                    ExchangeRate = (float)objGlobalDefault.ExchangeRate.Single(CurrencyType => CurrencyType.Currency == objAgentDetailsOnAdmin.Currency).Rate;
                }
            }
            else
            {
                if (PreferedCurrency == "INR")
                {
                    ExchangeRate = 1;
                }
                else
                {
                    ExchangeRate = (float)objGlobalDefault.ExchangeRate.Single(CurrencyType => CurrencyType.Currency == PreferedCurrency).Rate;
                }
            }
            return ExchangeRate;
        }

        public static float GetExchange(HttpContext Context)
        {
            float ExchangeRate = 0;
            GlobalDefault objGlobalDefault = (GlobalDefault)Context.Session["LoginUser"];
            string PreferedCurrency = objGlobalDefault.Currency;
            AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
            if (Context.Session["AgentDetailsOnAdmin"] != null)
            {
                if (objAgentDetailsOnAdmin.Currency == "INR")
                {
                    ExchangeRate = 1;
                }
                else
                {
                    ExchangeRate = (float)objGlobalDefault.ExchangeRate.Single(CurrencyType => CurrencyType.Currency == objAgentDetailsOnAdmin.Currency).Rate;
                }
            }
            else
            {
                if (PreferedCurrency == "INR")
                {
                    ExchangeRate = 1;
                }
                else
                {
                    ExchangeRate = (float)objGlobalDefault.ExchangeRate.Single(CurrencyType => CurrencyType.Currency == PreferedCurrency).Rate;
                }
            }
            return ExchangeRate;
        }

        public static string GetCurentCurrency(HttpContext Context)
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)Context.Session["LoginUser"];
            string CurrencyClass = objGlobalDefault.Currency;
            AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
            if (Context.Session["AgentDetailsOnAdmin"] != null)
            {
                objAgentDetailsOnAdmin = (AgentDetailsOnAdmin)Context.Session["AgentDetailsOnAdmin"];
                CurrencyClass = objAgentDetailsOnAdmin.Currency;
            }
            return CurrencyClass;
        }
        public static string GetCurentCurrency()
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string CurrencyClass = objGlobalDefault.Currency;
            AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
            if (HttpContext.Current.Session["AgentDetailsOnAdmin"] != null)
            {
                objAgentDetailsOnAdmin = (AgentDetailsOnAdmin)HttpContext.Current.Session["AgentDetailsOnAdmin"];
                CurrencyClass = objAgentDetailsOnAdmin.Currency;
            }
            return CurrencyClass;
        }
    }
}