﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="HotelList.aspx.cs" Inherits="CutAdmin.MappedHotelsList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/HotelList.js?v=2.3"></script>
    <link href="css/styles/switches.css?v=1" rel="stylesheet" />
    <script src="js/setup.js"></script>
    <script src="js/libs/formValidator/jquery.validationEngine.js?v=1"></script>
    <script src="js/libs/formValidator/languages/jquery.validationEngine-en.js?v=1"></script>
    <script src="js/libs/jquery.tablesorter.min.js"></script>
    <meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Hotels List</h1>
            <hr />
        </hgroup>

        <div class="with-padding">
            <%-- <table class="table responsive-table"  id="tbl_HotelList" >
				<thead>
					<tr>
						<th scope="col" width="35%">Hotels</th>
						<th scope="col" width="10%">Country</th>
						<th scope="col" width="10%">City</th>
                        <th scope="col" width="10%">Ratings</th>
                        <th scope="col" width="10%">Edit</th>
                        <th scope="col" width="10%">Manage</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>--%>

            <table class="table responsive-table" id="tbl_HotelList">

                <thead>
                    <tr>
                        <th scope="col">Hotels</th>
                        <th scope="col" class="align-center">Country</th>
                        <th scope="col" class="align-center">City</th>
                        <th scope="col" class="align-center">Ratings</th>
                        <%--<th scope="col" width="100" class="align-center">Offers</th>--%>
                        <th scope="col" class="align-center">Update</th>
                        <th scope="col" class="align-center">Activate</th>
                        <th scope="col" class="align-center">Inventory</th>
                        <%-- <th scope="col" width="50" class="align-center">Delete</th>--%>
                    </tr>
                </thead>
                <tbody>
                    <%--<tr style="border: none;">
                        <td colspan="7">
                            <img src="loader.gif" style="padding-left: 40%; padding-right: 20%;" id="img_file_attach" alt=""  />
                        </td>
                    </tr>--%>
                </tbody>
            </table>
        </div>
    </section>
    <!-- Scripts -->



    <script>

        // Call template init (optional, but faster if called manually)
        $.template.init();


        // Table sort - styled
        $('#tbl_HotelList').tablesorter({
            headers: {
                0: { sorter: false },
                6: { sorter: false }
            }
        }).on('click', 'tbody td', function (event) {
            var drophotelId = this.id;

            // Do not process if something else has been clicked
            if (event.target !== this) {
                return;
            }

            var tr = $(this).parent(),
				row = tr.next('.row-drop'),
				rows;

            // If click on a special row
            if (tr.hasClass('row-drop')) {
                return;
            }

            // If there is already a special row
            if (row.length > 0) {

                // Un-style row
                tr.children().removeClass('anthracite-gradient glossy');

                // Remove row
                row.remove();

                return;
            }

            // Remove existing special rows
            rows = tr.siblings('.row-drop');
            if (rows.length > 0) {
                // Un-style previous rows
                rows.prev().children().removeClass('anthracite-gradient glossy');

                // Remove rows
                rows.remove();
            }

            // Style row
            tr.children().addClass('anthracite-gradient glossy');


            // Add fake row
            $('<tr  class="row-drop">' +
				'<td  colspan="' + tr.children().length + '">' +
                  '<div class="columns">' +
                    '<div id="DropLeft"  class="five-columns"> </div>' +
                    '<div id="Dropmiddle"  class="four-columns align-center"> </div>' +
                    '<div id="DropRight"  class="three-columns"> </div>' +
                  '</div>' +
				'</td>' +
			'</tr>').insertAfter(tr);
            DropRowwithId(drophotelId)
        }).on('sortStart', function () {
            var rows = $(this).find('.row-drop');
            if (rows.length > 0) {
                // Un-style previous rows
                rows.prev().children().removeClass('anthracite-gradient glossy');

                // Remove rows
                rows.remove();
            }
        });



    </script>
    <script>

        // Call template init (optional, but faster if called manually)
        $.template.init();

        // Color
        $('#anthracite-inputs').change(function () {
            $('#main')[this.checked ? 'addClass' : 'removeClass']('black-inputs');
        });

        // Switches mode
        $('#switch-mode').change(function () {
            $('#switch-wrapper')[this.checked ? 'addClass' : 'removeClass']('reversed-switches');
        });

        // Disabled switches
        $('#switch-enable').change(function () {
            $('#disabled-switches').children()[this.checked ? 'enableInput' : 'disableInput']();
        });

        // Tooltip menu
        $('#select-tooltip').menuTooltip($('#select-context').hide(), {
            classes: ['no-padding']
        });

        // Date picker
        $('.datepicker').glDatePicker({ zIndex: 100 });

        // Form validation
        $('form').validationEngine();

    </script>
</asp:Content>
