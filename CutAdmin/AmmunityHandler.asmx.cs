﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for AmmunityHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class AmmunityHandler : System.Web.Services.WebService
    {

        private string escapejosndata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }



        [WebMethod(EnableSession = true)]
        public string AddRoomAmunities(string RoomAmunityName, string sPhoto)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DBHelper.DBReturnCode retcode = AmmunityManager.AddRoomAmunities(RoomAmunityName, sPhoto);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }

        }


        [WebMethod(EnableSession = true)]
        public string GetRoomAmunities()
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = AmmunityManager.GetRoomAmunities(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"Staff\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }


        [WebMethod(EnableSession = true)]
        public string DeleteRoomAmunities(string RoomAmunityID)
        {
            string jsonString = "";
            DBHelper.DBReturnCode retcode = AmmunityManager.DeleteRoomAmunities(RoomAmunityID);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }


        [WebMethod(EnableSession = true)]
        public string GetDetails(string RoomAmunityID)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = AmmunityManager.GetDetails(RoomAmunityID, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"dttable\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateRoomAmunity(string RoomAmunityName, string AmunityIcon, Int64 id)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DBHelper.DBReturnCode retcode = AmmunityManager.UpdateRoomAmunity(RoomAmunityName, AmunityIcon, id);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }

        }
    }
}
