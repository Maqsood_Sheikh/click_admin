﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;


namespace CutAdmin
{
    /// <summary>
    /// Summary description for FacilityHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class FacilityHandler : System.Web.Services.WebService
    {

        private string escapejosndata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }



        [WebMethod(EnableSession = true)]
        public string AddFacility(string HotelFacilityName, string sPhoto)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DBHelper.DBReturnCode retcode = FacilityManager.AddFacility(HotelFacilityName, sPhoto);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }

        }

        [WebMethod(EnableSession = true)]
        public string GetHotelFacility()
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = FacilityManager.GetHotelFacility(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"Staff\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string DeleteHotelFacility(string HotelFacilityID)
        {
            string jsonString = "";
            DBHelper.DBReturnCode retcode = FacilityManager.DeleteHotelFacility(HotelFacilityID);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetDetails(string HotelFacilityID)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = FacilityManager.GetDetails(HotelFacilityID, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"dttable\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateHotelFacility(string HotelFacilityName, string FacilityIcon, Int64 id)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DBHelper.DBReturnCode retcode = FacilityManager.UpdateHotelFacility(HotelFacilityName, FacilityIcon, id);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }

        }

    }
}
