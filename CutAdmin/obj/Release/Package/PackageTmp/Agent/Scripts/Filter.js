﻿var arrFilter = new Array();
function GenerateFilter() {
    try {
        arrFilter = arrFilter;
        if (arrFilter.RoomType.length > 0) {
            $("#RoomType").empty();
            var ddlRequest = '<option value="-">Select Room Type</option>';
            for (i = 0; i < arrFilter.RoomType.length; i++) {
                ddlRequest += '<option value="' + arrFilter.RoomType[i] + '">' + arrFilter.RoomType[i] + '</option>';
            }
            $("#RoomType").append(ddlRequest);
        }

        if (arrFilter.MealType.length > 0) {
            $("#MealType").empty();
            var ddlRequest = '<option value="-">Select Room Type</option>';
            for (i = 0; i < arrFilter.MealType.length; i++) {
                ddlRequest += '<option value="' + arrFilter.MealType[i] + '">' + arrFilter.MealType[i] + '</option>';
            }
            $("#MealType").append(ddlRequest);
        }
        $("#Price_Filter").empty();
        var htlml = '<span class="demo-slider" data-slider-options="{"size":false,"values":["' + arrFilter.MinPrice + '","' + arrFilter.MaxPrice + '"],"tooltip":["left","right"],"tooltipOnHover":false,"topLabelAlign":"right","barClasses":["red-gradient","glossy"]}"></span>'
        $("#Price_Filter").append(htlml);
        var Remain = arrFilter.MinPrice.toString().split('.')[1];
        $('.demo-slider').slider({
            values: [arrFilter.MinPrice, arrFilter.MaxPrice],
            tooltipOnHover: false,
            round: false,
            stickToRound: true,
            max: arrFilter.MaxPrice,
            min: arrFilter.MinPrice,
            tooltip: ["left", "right"],
            stripesSize: 'big',
            tooltipFormat: null,
            tooltipBiggerOnDrag: true,
            tooltipBiggerOnDrag: false,
            tooltipClass: ['compact', 'black-gradient', 'glossy'],
            barClasses: ["red-gradient", "glossy"],
        });
        $(".slider").click(function () {
            FilterRate()
        });
    }
    catch (ex) {

    }

}

function FilterRate() {
    var RoomType = [];
    var MealType = [];
    var ndPrice = $(".tooltip-value");
    RoomType = $('#RoomType').val();
    MealType = $('#MealType').val();
    var MinPrice = $(ndPrice[0]).text();
    var MaxPrice = $(ndPrice[1]).text();
    if (RoomType == "" || RoomType == null) {
        RoomType = [];
    }
    if (MealType == "" || MealType == null) {
        MealType = [];
    }
    if (MinPrice == "" || MinPrice == null) {
        MinPrice = 0;
    }
    if (MaxPrice == "" || MaxPrice == null) {
        MaxPrice = 0;
    }
    var data =
         {

             RoomType: RoomType,
             MealType: MealType,
             MinPrice: MinPrice,
             MaxPrice: MaxPrice,

         }
    $.ajax({
        type: "POST",
        url: "../Agent/Handler/RoomHandler.asmx/FilterRate",

        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrRoomRates = result.objRateGroup;
                GenrateB2bRates();
            }
        }
    });
}