﻿$(document).ready(function () {
    BookingRequests();
    BookingOnHold();
    BookingReconfirmation();
    BookingGroupRequest();
    GetCount();
});

function GetCount() {
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/GetCount",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            $("#listHotels").empty();
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var HotelList = result.HotelList;
                var AgentCount = result.AgentCount;
                $('#hotel_Count').text(HotelList);
                $('#Agents').text(AgentCount);

            }
        },

    });
}


function BookingRequests() {
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/BookingRequests",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {

                var Pending = result.BookingPending;
                var Confirmed = result.BookingConfirmed;
                var Rejected = result.BookingRejected;

                var html = "";
                html += '<div class="block-title">'
                html += '     <h3><i class="icon-pencil icon-size2"></i> Requests</h3>'
                html += '</div>'
                html += '<ul class="events">'
                html += '    <li>'
                html += '        <span class="event-date orange" style="font-size: 12px;">Pending</span>'
                html += '        <a href="bookinglist.aspx?Type=BookingPending&Status=Vouchered" class="event-description">'
                html += '            <br />'
                html += '            <h4>' + Pending + '</h4>'
                html += '        </a>'
                html += '    </li>'
                html += '    <li>'
                html += '        <span class="event-date orange" style="font-size: 12px;">Confirmed</span>'
                html += '       <a href="bookinglist.aspx?Type=BookingConfirmed&Status=Vouchered" class="event-description">'
                html += '            <br />'
                html += '            <h4>' + Confirmed + '</h4>'
                html += '        </a>'
                html += '    </li>'
                html += '    <li>'
                html += '        <span class="event-date orange" style="font-size: 12px;">Rejected</span>'
                html += '        <a href="bookinglist.aspx?Type=BookingRejected&Status=Vouchered" class="event-description">'
                html += '            <br />'
                html += '            <h4>' + Rejected + '</h4>'
                html += '        </a>'
                html += '    </li>'
                html += '</ul>'
                $("#BookingsRequests").append(html);
            }
        },
    });
}

function BookingOnHold() {
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/BookingOnHold",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {

                var Bookings = result.OnHoldBookings;
                var Requested = result.OnHoldRequested;
                var Confirmed = result.OnHoldConfirmed;
                GetCalender(result.arrBookings);
                var html = "";
                html += '<div class="block-title">'
                html += '     <h3><i class="icon-pencil icon-size2"></i> On Hold</h3>'
                html += '</div>'
                html += '<ul class="events">'
                html += '    <li>'
                html += '        <span class="event-date orange" style="font-size: 12px;">Pending</span>'
                html += '        <a href="bookinglist.aspx?Type=Bookings&Status=OnRequest" class="event-description">'
                html += '            <br />'
                html += '            <h4>' + Bookings + '</h4>'
                html += '        </a>'
                html += '    </li>'
                html += '    <li>'
                html += '        <span class="event-date orange" style="font-size: 12px;">Confirmed</span>'
                html += '          <a href="bookinglist.aspx?Type=Requested&Status=OnRequest" class="event-description">'
                html += '            <br />'
                html += '            <h4>' + Requested + '</h4>'
                html += '        </a>'
                html += '    </li>'
                html += '    <li>'
                html += '        <span class="event-date orange" style="font-size: 12px;">Rejected</span>'
                html += '          <a href="bookinglist.aspx?Type=Confirmed&Status=OnRequest" class="event-description">'
                html += '            <br />'
                html += '            <h4>' + Confirmed + '</h4>'
                html += '        </a>'
                html += '    </li>'
                html += '</ul>'
                $("#BookingOnHold").append(html);
            }
        },
    });
}

function BookingReconfirmation() {
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/BookingReconfirmation",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {

                var Pending = result.ReconfirmPending;
                var Confirmed = result.ReconfirmRequested;
                var Rejected = result.ReconfirmReject;

                GetCalender(result.arrBookings);
                var html = "";
                html += '<div class="block-title">'
                html += '     <h3><i class="icon-pencil icon-size2"></i>Reconfirmation</h3>'
                html += '</div>'
                html += '<ul class="events">'
                html += '    <li>'
                html += '        <span class="event-date orange" style="font-size: 12px;">Pending</span>'
                html += '        <a href="bookinglist.aspx?Type=ReconfirmPending&Status=Vouchered" class="event-description">'
                html += '            <br />'
                html += '            <h4>' + Pending + '</h4>'
                html += '        </a>'
                html += '    </li>'
                html += '    <li>'
                html += '        <span class="event-date orange" style="font-size: 12px;">Confirmed</span>'
                html += '          <a href="bookinglist.aspx?Type=ReconfirmRequested&Status=Vouchered" class="event-description">'
                html += '            <br />'
                html += '            <h4>' + Confirmed + '</h4>'
                html += '        </a>'
                html += '    </li>'
                html += '    <li>'
                html += '        <span class="event-date orange" style="font-size: 12px;">Rejected</span>'
                html += '          <a href="bookinglist.aspx?Type=ReconfirmReject&Status=Vouchered" class="event-description">'
                html += '            <br />'
                html += '            <h4>' + Rejected + '</h4>'
                html += '        </a>'
                html += '    </li>'
                html += '</ul>'
                $("#BookingsReconfirm").append(html);
            }
        },
    });
}

function BookingGroupRequest() {
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/BookingGroupRequest",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {

                var Pending = result.GroupRequestPending;
                var Confirmed = result.GroupRequestRequested;
                var Rejected = result.GroupRequestReject;

                GetCalender(result.arrBookings);
                var html = "";
                html += '<div class="block-title">'
                html += '     <h3><i class="icon-pencil icon-size2"></i>Group Request</h3>'
                html += '</div>'
                html += '<ul class="events">'
                html += '    <li>'
                html += '        <span class="event-date orange" style="font-size: 12px;">Pending</span>'
                html += '        <a href="groupbookinglist.aspx?Type=GroupRequestPending&Status=GroupRequest" class="event-description">'
                html += '            <br />'
                html += '            <h4>' + Pending + '</h4>'
                html += '        </a>'
                html += '    </li>'
                html += '    <li>'
                html += '        <span class="event-date orange" style="font-size: 12px;">Confirmed</span>'
                html += '          <a href="groupbookinglist.aspx?Type=GroupRequestRequested&Status=GroupRequest" class="event-description">'
                html += '            <br />'
                html += '            <h4>' + Confirmed + '</h4>'
                html += '        </a>'
                html += '    </li>'
                html += '    <li>'
                html += '        <span class="event-date orange" style="font-size: 12px;">Rejected</span>'
                html += '          <a href="groupbookinglist.aspx?Type=GroupRequestReject&Status=GroupRequest" class="event-description">'
                html += '            <br />'
                html += '            <h4>' + Rejected + '</h4>'
                html += '        </a>'
                html += '    </li>'
                html += '</ul>'
                $("#GroupRequest").append(html);
            }
        },
    });
}

function GetCalender(arrBookings) {
    try {
        var arrEvents = [];
        for (var i = 0; i < arrBookings.length; i++) {
            arrBookings[i].sCheckIn = arrBookings[i].sCheckIn + "T22:30"
            var Event = {
                id: arrBookings[i].ReservationID,
                title: "<a  onclick='GetDetails(\"" + arrBookings[i].ReservationID + "\")'><h5 class='green underline'>" + arrBookings[i].HotelName + "<br/> <small class=''>(" + arrBookings[i].bookingname + ")</small></h5></a>",
                start: arrBookings[i].sCheckIn,
                end: arrBookings[i].sCheckIn,
                html: true,
                color: '#FFF',
                textEscape: false,
            }
            arrEvents.push(Event)
        }
        var calendar = $('.datepicker').fullCalendar({
            editable: true,
            header: {
                left: 'prev',
                center: 'title',
                right: 'next'
            },
            events: arrEvents,
            // Convert the allDay from string to boolean
            eventRender: function (event, element, view) {
                element.find('.fc-title').html("<b>" + event.title + "</b>");
            },
            selectable: true,
            selectHelper: true,
            editable: true,
            eventAfterAllRender: function (view) {
                //$(".fc-event-container").append("<span class='closon'>X</span>");
            },
            eventDrop: function (event, delta) {
            },
            eventResize: function (event) {
            },
            eventClick: function (event) {
                //$(".closon").click(function () {
                //    $('#fullCalendar').fullCalendar('removeEvents', event._id);
                //});
            },
        });
        $(".fc-toolbar").css({
            "width": "90%",
            "margin-left": "6%",
        })
    }
    catch (e) {
        alert(e)
    }
}


function GetDetails(ReservationIID) {



    var data = {
        ReservationID: ReservationIID
    }
    $.ajax({
        type: "POST",
        url: "../handler/BookingHandler.asmx/GetDetail",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var Cancle = "";
            var Cancleamnt = "";
            var Policy = "";
            if (result.retCode == 1) {
                try {
                    var Detail = result.Detail;

                    $.modal({
                        content:

                      '<div class="modal-body">' +
                      '' +
                      '<table class="table table-hover table-responsive" id="tbl_Confirmation" style="width: 90%">' +
                      '<tr>' +
                      '<td>' +
                      '<span class="text-left">Hotel:&nbsp;&nbsp;<b>' + Detail[0].HotelName + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                       '<td>' +
                      '<span class="text-left">CheckIn:&nbsp;&nbsp;<b>' + Detail[0].CheckIn + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                       '<td>' +
                      '<span class="text-left">CheckOut:&nbsp;&nbsp;<b>' + Detail[0].CheckOut + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                      '</tr>' +
                      '<tr>' +
                      '<td>' +
                      '<span class="text-left">Passenger: &nbsp;&nbsp;<b>' + Detail[0].Name + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                       '<td>' +
                      '<span class="text-left">Location:&nbsp;&nbsp;<b> ' + Detail[0].City + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                       '<td>' +
                      '<span class="text-left">Nights:&nbsp;&nbsp; <b>' + Detail[0].NoOfDays + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                      '</tr>' +
                      '<tr>' +
                      '<td>' +
                      '<span class="text-left">Booking Id:&nbsp;&nbsp; <b>' + Detail[0].ReservationID + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                       '<td>' +
                      '<span class="text-left">Booiking Date:&nbsp;&nbsp;<b> ' + Detail[0].ReservationDate + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                       '<td>' +
                      '<span class="text-left">Amount:&nbsp;&nbsp;<b>' + Detail[0].TotalFare + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                      '</tr>' +
                      '</table>' +



                       '<br/>' +
                      '</div>',
                        title: 'Booking Details',
                        width: 600,
                        height: 200,
                        scrolling: true,
                        actions: {
                            'Close': {
                                color: 'red',
                                click: function (win) { win.closeModal(); }
                            }
                        },
                        buttons: {
                            'Close': {
                                classes: 'huge anthracite-gradient displayNone',
                                click: function (win) { win.closeModal(); }
                            }
                        },
                        buttonsLowPadding: true

                    });
                }
                catch (ex) {

                }


            }
            else if (result.retCode == 0) {
                $('#SpnMessege').text('Something Went Wrong');
                $('#ModelMessege').modal('show')
                // alert("error occured while getting cancellation details")
            }
        },
        error: function (xhr, status, error) {
            $('#SpnMessege').text("Getting Error");
            $('#ModelMessege').modal('show')
            // alert("Error on cancellation popup:" + " " + xhr.readyState + " " + xhr.status);
        }
    });
}