﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddSeason.aspx.cs" Inherits="CutAdmin.AddSeason" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


       <!-- Additional styles -->
	<link rel="stylesheet" href="css/styles/form.css?v=1">
	<link rel="stylesheet" href="css/styles/switches.css?v=1">
	<link rel="stylesheet" href="css/styles/table.css?v=1">

	<!-- DataTables -->
	<link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">
   
   
	<!-- Microsoft clear type rendering -->
	<meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
       
        <div class="with-padding">
            <div class="large-box-shadow white-gradient with-border">

                <div class="blue-gradient panel-control">
                    <h3  style="color:white">Add Rates</h3>
                </div>

                <div class="with-padding">
                    <div class="columns">
                        <div class="three-columns">

                            <small class="input-info">Valid From:</small><br />
                             <input type="number" id="txtValidFrom" class="input full-width" >
                        </div>
                        <%--<div class="four-columns">
                                          <small class="input-info">Number of Rooms:</small>
                                          <input type="number" id="QtyRooms" class="input full-width" value="">
                                      </div>--%>
                        <div class="four-columns">
                            <small class="input-info">Maximum Occupacy</small>
                            <input type="number" id="QtyOccupacy" class="input full-width" value="0">
                        </div>

                        <div class="four-columns">
                            <small class="input-info">Room Size:</small>
                            <input type="text" id="RoomSize" class="input full-width" placeholder="Sq x Ft">
                        </div>

                        <div class="new-row four-columns">
                            <small class="input-info">Maximum Children Allowed:</small>
                            <input type="number" id="MaxChildsAllowed" class="input full-width" value="0">
                        </div>
                        <div class="four-columns">
                            <small class="input-info">Adults with Childs:</small>
                            <input type="number" id="AdultsWithChilds" class="input full-width" value="0">
                        </div>
                        <div class="four-columns">
                            <small class="input-info">Adults without Childs</small>
                            <input type="number" id="AdultsWithoutChilds" class="input full-width" value="0">
                        </div>

                        <div class="new-row twelve-columns">
                            <small class="input-info">Room Description:</small>
                            <textarea id="RoomDescription" class="input full-width" style="min-height: 100px;"></textarea>
                        </div>

                    </div>
                </div>
                <div class="button-height with-mid-padding silver-gradient no-margin-top">
                    <a href="#" class="button blue-gradient float-right with-tooltip" title="Submit">Submit</a>
                    <span class="button-group children-tooltip"></span>

                </div>

            </div>
        </div>

	</section>
	<!-- End main content -->



	<!-- JavaScript at the bottom for fast page loading -->
	<!-- Scripts -->
	<script src="js/libs/jquery-1.10.2.min.js"></script>
	<script src="js/setup.js"></script>

	<!-- Template functions -->
	<script src="js/developr.input.js"></script>
	<script src="js/developr.navigable.js"></script>
	<script src="js/developr.notify.js"></script>
	<script src="js/developr.scroll.js"></script>
	<script src="js/developr.tooltip.js"></script>
	<script src="js/developr.table.js"></script>
    <script src="js/developr.accordions.js"></script>
    <script src="js/developr.wizard.js"></script>

	<!-- Plugins -->
	<script src="js/libs/jquery.tablesorter.min.js"></script>
	<script src="js/libs/DataTables/jquery.dataTables.min.js"></script>

</asp:Content>
