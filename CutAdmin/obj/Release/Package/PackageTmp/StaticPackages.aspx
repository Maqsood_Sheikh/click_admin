﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="StaticPackages.aspx.cs" Inherits="CutAdmin.StaticPackages" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/StaticPackges.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">


        <hgroup id="main-title" class="thin">
            <h1>Static Packages</h1>

        </hgroup>

        <div class="with-padding">
            <div class="columns">
                <div class="six-columns six-columns-mobile">
                    <button type="button" class="button compact anthracite-gradient" onclick="NewPakages()">New Package</button></div>
                <div class="six-columns  six-columns-mobile text-alignright selCurrency">
                    <div class="full-width button-height">
                        <select id="selCurrency" class="select" onchange="GetPackages()">
                            <option selected="selected" value="USD">USD</option>
                            <option value="INR">INR</option>
                            <option value="AED">AED</option>
                        </select>
                    </div>
                </div>
            </div>



            <%--<div class="table-header button-height anthracite-gradient">
                <div class="float-right">
                    Search&nbsp;<input type="text" name="table_search" id="table_search" value="" class="input mid-margin-left">
                </div>

                Show&nbsp;<select name="range" class="select anthracite-gradient glossy">
                    <option value="1">10</option>
                    <option value="2">20</option>
                    <option value="3" selected="selected">40</option>
                    <option value="4">100</option>
                </select>
                entries
            </div>--%>
            <div class="respTable">
                <table class="table responsive-table" id="tbl_Package">

                    <thead>
                        <tr>
                            <th scope="col">S.No</th>
                            <th scope="col">Package Name</th>
                            <th scope="col">Location</th>
                            <th scope="col">Night</th>
                            <th scope="col">Price</th>
                            <th scope="col">Star Rating</th>
                            <th scope="col">Valid Upto</th>
                            <th scope="col">Active / Deactive</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                        </tr>
                    </thead>


                    <tbody>
                        <%--<tr>
                            <td>1</td>
                            <td>Appart.s City Paris Boblgny</td>
                            <td>London - KY, UNITED STATES - USA</td>
                            <td>1 Night</td>
                            <td>$ 25.28</td>
                            <td>
                                <img src="img/smallrating-4.png" alt="" /></td>
                            <td>1-11-2018</td>
                            <td class="align-center">
                                <input type="checkbox" name="switch-tiny" id="switch-tiny" class="switch tiny mid-margin-right" value="1"></td>
                            <td class="align-center"><a href="#" class="button" title="Update" onclick="NewPakages()"><span class="icon-pencil"></span></a></td>
                            <td class="align-center"><span class="button-group children-tooltip">
                                <a href="#" class="button" title="trash" onclick="deletTrush()"><span class="icon-trash"></span></a></span></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>aliah Talbah</td>
                            <td>Madinah, SAUDI ARABIA</td>
                            <td>1 Night</td>
                            <td>$ 25.28</td>
                            <td>
                                <img src="img/smallrating-5.png" alt="" /></td>
                            <td>1-11-2018</td>
                            <td class="align-center">
                                <input type="checkbox" name="switch-tiny" id="switch-tiny" class="switch tiny mid-margin-right" value="1"></td>
                            <td class="align-center"><a href="#" class="button" title="Update" onclick="NewPakages()"><span class="icon-pencil"></span></a></td>
                            <td class="align-center"><span class="button-group children-tooltip">
                                <a href="#" class="button" title="trash" onclick="deletTrush()"><span class="icon-trash"></span></a></span></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>City Max Bur Dubai</td>
                            <td>Dubai, UNITED ARAB EMIRATE</td>
                            <td>1 Night</td>
                            <td>$ 25.28</td>
                            <td>
                                <img src="img/smallrating-3.png" alt="" /></td>
                            <td>1-11-2018</td>
                            <td class="align-center">
                                <input type="checkbox" name="switch-tiny" id="switch-tiny" class="switch tiny mid-margin-right" value="1"></td>
                            <td class="align-center"><a href="#" class="button" title="Update" onclick="NewPakages()"><span class="icon-pencil"></span></a></td>
                            <td class="align-center"><span class="button-group children-tooltip">
                                <a href="#" class="button" title="trash" onclick="deletTrush()"><span class="icon-trash"></span></a></span></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>rabian Countyard</td>
                            <td>Dubai, UNITED ARAB EMIRATES</td>
                            <td>1 Night</td>
                            <td>$ 25.28</td>
                            <td>
                                <img src="img/smallrating-4.png" alt="" /></td>
                            <td>1-11-2018</td>
                            <td class="align-center">
                                <input type="checkbox" name="switch-tiny" id="switch-tiny" class="switch tiny mid-margin-right" value="1"></td>
                            <td class="align-center"><a href="#" class="button" title="Update" onclick="NewPakages()"><span class="icon-pencil"></span></a></td>
                            <td class="align-center"><span class="button-group children-tooltip">
                                <a href="#" class="button" title="trash" onclick="deletTrush()"><span class="icon-trash"></span></a></span></td>
                        </tr>--%>

                    </tbody>

                </table>

            </div>
            <%--<form method="post" action="" class="table-footer button-height large-margin-bottom anthracite-gradient">
                <div class="float-right">
                    <div class="button-group">
                        <a href="#" title="First page" class="button anthracite-gradient glossy"><span class="icon-previous"></span></a>
                        <a href="#" title="Previous page" class="button anthracite-gradient glossy"><span class="icon-backward"></span></a>
                    </div>

                    <div class="button-group">
                        <a href="#" title="Page 1" class="button anthracite-gradient glossy">1</a>
                        <a href="#" title="Page 2" class="button anthracite-gradient glossy active">2</a>
                        <a href="#" title="Page 3" class="button anthracite-gradient glossy">3</a>
                        <a href="#" title="Page 4" class="button anthracite-gradient glossy">4</a>
                    </div>

                    <div class="button-group">
                        <a href="#" title="Next page" class="button anthracite-gradient glossy"><span class="icon-forward"></span></a>
                        <a href="#" title="Last page" class="button anthracite-gradient glossy"><span class="icon-next"></span></a>
                    </div>
                </div>

                <div class="dataTables_info" id="sorting-advanced_info">Showing 1 to 10 of 18 entries</div>
            </form>--%>




        </div>

    </section>
    <script>

        // Call template init (optional, but faster if called manually)
        $.template.init();

        // Favicon count
        Tinycon.setBubble(2);

        // If the browser support the Notification API, ask user for permission (with a little delay)
        if (notify.hasNotificationAPI() && !notify.isNotificationPermissionSet()) {
            setTimeout(function () {
                notify.showNotificationPermission('Your browser supports desktop notification, click here to enable them.', function () {
                    // Confirmation message
                    if (notify.hasNotificationPermission()) {
                        notify('Notifications API enabled!', 'You can now see notifications even when the application is in background', {
                            icon: 'img/demo/icon.png',
                            system: true
                        });
                    }
                    else {
                        notify('Notifications API disabled!', 'Desktop notifications will not be used.', {
                            icon: 'img/demo/icon.png'
                        });
                    }
                });

            }, 2000);
        }

        /*
		 * Handling of 'other actions' menu
		 */

        var otherActions = $('#otherActions'),
			current = false;

        // Other actions
        $('.list .button-group a:nth-child(2)').menuTooltip('Loading...', {

            classes: ['with-mid-padding'],
            ajax: 'ajax-demo/tooltip-content.html',

            onShow: function (target) {
                // Remove auto-hide class
                target.parent().removeClass('show-on-parent-hover');
            },

            onRemove: function (target) {
                // Restore auto-hide class
                target.parent().addClass('show-on-parent-hover');
            }
        });

        // Delete button
        $('.list .button-group a:last-child').data('confirm-options', {

            onShow: function () {
                // Remove auto-hide class
                $(this).parent().removeClass('show-on-parent-hover');
            },

            onConfirm: function () {
                // Remove element
                $(this).closest('li').fadeAndRemove();

                // Prevent default link behavior
                return false;
            },

            onRemove: function () {
                // Restore auto-hide class
                $(this).parent().addClass('show-on-parent-hover');
            }

        });

        $(function () {
         
            $("#txt_ValidUpto").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy"
            });
        })
        // New Pakages modal
        function NewPakages() {
            $.modal({
                content: '<div class="modal-body" id="PackageModal">' +
				'<div class="columns">' +
'<div class="six-columns twelve-columns-mobile"><label>Package Name:</label> <div class="input full-width">' +
'<input name="prompt-value" id="txt_Name" placeholder="Package Name" value="" class="input-unstyled full-width"  type="text">' +
'</div></div>' +
'<div class="six-columns twelve-columns-mobile"><label>Location: </label> <div class="input full-width">' +
'<input name="prompt-value" id="txt_Location" placeholder="Location"  class="input-unstyled full-width"  type="text">' +
'</div></div></div>' +
'<div class="columns">' +
'<div class="four-columns twelve-columns-mobile"><label>Currency: </label><div class="full-width button-height">' +
'<select id="selPakageCurrency" class="select">' +
'<option selected="selected" value="USD">USD</option><option value="INR">INR</option><option value="AED">AED</option> </select> ' +
'</div></div>' +
'<div class="four-columns twelve-columns-mobile"><label>Stay: </label><div class="full-width button-height">' +
'<select id="selStay" class="select">' +
'<option selected="selected" value="1">1 Night</option><option value="2">2 Night</option><option value="3">3 Night</option>' +
'<option value="4">4 Night</option><option value="5">5 Night</option> </select> ' +
'</div></div>' +
'<div class="four-columns twelve-columns-mobile"><label>Star Rating: </label><div class="full-width button-height">' +
'<select id="selStarRating" class="select">' +
'<option selected="selected" value="0">No Star</option><option value="1">1 Star</option><option value="2">2 Star</option>' +
'<option value="3">3 Star</option><option value="4">4 Star</option><option value="5">5 Star</option></select> ' +
'</div></div>' +
'</div><div class="columns">' +
'<div class="six-columns twelve-columns-mobile"><label>Price: </label> <div class="input full-width">' +
'<input name="prompt-value" id="txt_Price" placeholder="Price" value="" class="input-unstyled full-width" type="text">' +
'</div></div>' +
'<div class="six-columns twelve-columns-mobile"><label>Valid Upto: </label> <div class="input full-width">' +
'<input name="datepicker" id="txt_ValidUpto" placeholder="Valid Upto" value="" class="input-unstyled" type="text">' +
'</div></div></div>' +
'<p class="text-alignright"><input type="button" value="Add" onclick="AddPackage();" title="Submit Details" class="button anthracite-gradient"/></p>' +
'</div>',

                title: 'Add Package Details',
                width: 500,
                scrolling: true,
                actions: {
                    'Close': {
                        color: 'red',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttons: {
                    'Close': {
                        classes: 'huge anthracite-gradient displayNone',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttonsLowPadding: true
            });
        };


        // deletTrush modal
        function deletTrush() {
            $.modal({
                content: '<p class="avtiveDea">Are you sure you want to Delete this package</strong></p>' +
'<p class="text-alignright text-popBtn"><button type="submit" class="button anthracite-gradient">OK</button></p>',


                width: 300,
                scrolling: false,
                actions: {
                    'Close': {
                        color: 'red',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttons: {
                    'Close': {
                        classes: 'anthracite-gradient glossy',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttonsLowPadding: false
            });
        };

    </script>
</asp:Content>
