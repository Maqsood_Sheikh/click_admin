﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="MailSettings.aspx.cs" Inherits="CutAdmin.MailSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    <script src="../Scripts/MailSettings.js?v1.1"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Mail Settings</h1>
            <hr />
        </hgroup>

        <div class="with-padding">
            <div class="respTable">
                <table class="table responsive-table" id="tbl_HotelDetails">
                    <thead>
                        <tr>
                            <th scope="col">Activity</th>
                            <th scope="col" class="align-center">Mail Id  </th>
                            <th scope="col" class="align-center">CC Mail Id </th>
                            <th scope="col" class="align-center">Error Message </th>
                            <th scope="col" class="align-center">Add | Edit </th>
                        </tr>
                    </thead>

                </table>
            </div>
        </div>

        <%--<div class="with-padding">
            <div class="standard-tabs margin-bottom" id="add-tabs">
                <ul class="tabs">
                    <li class="active"><a href="#VisaMails">Visa Mails</a></li>
                    <li><a href="#OTBMails">OTB Mails</a></li>
                    <li class="active"><a href="#HotelMails">Hotel Mails</a></li>
                </ul>
                <div class="tabs-content">
                    <div id="HotelMails">
                        
                    </div>
                    
                </div>
            </div>

        </div>--%>
    </section>
    <!-- End main content -->
</asp:Content>
