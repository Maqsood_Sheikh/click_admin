﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="PromoImage.aspx.cs" Inherits="CutAdmin.PromoImage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/PromoImage.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Promo Image</h1>
            <h2>
                <button type="button" class="button compact anthracite-gradient" onclick="Imageupdate();"><span class="icon-user"></span>&nbsp;Add New</button></h2>
        </hgroup>
        <div class="with-padding">



            <div class="respTable">
                <table class="table responsive-table" id="tbl_PromoImageDetails">

                    <thead>
                        <tr>
                            <th scope="col" class="align-center">Image Name</th>
                            <th scope="col" class="align-center">Update Date</th>
                            <th scope="col" class="align-center">Selected Image</th>
                            <th scope="col" class="align-center">Action</th>
                        </tr>
                    </thead>


                    <tbody>
                        <%--<tr>
                            <td>At The Top</td>
                            <td>17-02-2018</td>
                            <td>
                                <input type="radio" name="radio" id="radio-2" value="2" class="radio mid-margin-left"></td>
                            <td class="align-center"><span class="button-group children-tooltip actiontab">
                                <a href="#" class="button" title="Image" onclick="ViewPromoImage();"><span class="fa fa-picture-o"></span></a>
                                <a href="#" class="button" title="Edit" onclick="Imageupdate();"><span class="icon-pencil"></span></a>
                                <a href="#" class="button" title="trash" onclick="deletTrush();"><span class="icon-trash"></span></a>
                            </span></td>

                        </tr>
                        <tr>
                            <td>At The Top</td>
                            <td>17-02-2018</td>
                            <td>
                                <input type="radio" name="radio" id="radio-2" value="2" class="radio mid-margin-left"></td>
                            <td class="align-center"><span class="button-group children-tooltip actiontab">
                                <a href="#" class="button" title="Image" onclick="ImageView();"><span class="fa fa-picture-o"></span></a>
                                <a href="#" class="button" title="Edit" onclick="Imageupdate();"><span class="icon-pencil"></span></a>
                                <a href="#" class="button" title="trash" onclick="deletTrush();"><span class="icon-trash"></span></a>
                            </span></td>

                        </tr>--%>
                    </tbody>

                </table>

            </div>



        </div>
    </section>
    <!-- End main content -->

    <script>

        // Call template init (optional, but faster if called manually)
        $.template.init();

        // Favicon count
        Tinycon.setBubble(2);

        // If the browser support the Notification API, ask user for permission (with a little delay)
        if (notify.hasNotificationAPI() && !notify.isNotificationPermissionSet()) {
            setTimeout(function () {
                notify.showNotificationPermission('Your browser supports desktop notification, click here to enable them.', function () {
                    // Confirmation message
                    if (notify.hasNotificationPermission()) {
                        notify('Notifications API enabled!', 'You can now see notifications even when the application is in background', {
                            icon: 'img/demo/icon.png',
                            system: true
                        });
                    }
                    else {
                        notify('Notifications API disabled!', 'Desktop notifications will not be used.', {
                            icon: 'img/demo/icon.png'
                        });
                    }
                });

            }, 2000);
        }

        /*
		 * Handling of 'other actions' menu
		 */

        var otherActions = $('#otherActions'),
			current = false;

        // Other actions
        $('.list .button-group a:nth-child(2)').menuTooltip('Loading...', {

            classes: ['with-mid-padding'],
            ajax: 'ajax-demo/tooltip-content.html',

            onShow: function (target) {
                // Remove auto-hide class
                target.parent().removeClass('show-on-parent-hover');
            },

            onRemove: function (target) {
                // Restore auto-hide class
                target.parent().addClass('show-on-parent-hover');
            }
        });

        // Delete button
        $('.list .button-group a:last-child').data('confirm-options', {

            onShow: function () {
                // Remove auto-hide class
                $(this).parent().removeClass('show-on-parent-hover');
            },

            onConfirm: function () {
                // Remove element
                $(this).closest('li').fadeAndRemove();

                // Prevent default link behavior
                return false;
            },

            onRemove: function () {
                // Restore auto-hide class
                $(this).parent().addClass('show-on-parent-hover');
            }

        });

        // Password modal
        function Imageupdate() {
            $.modal({
                content: '<div class="modal-body">' +
                            '<div class="scrollingDiv">' +
                                '<div class="columns">' +
                                    '<div class="six-columns bold"><label>Upload Image:<span class="red">*</span></label><p class="button-height"><input type="file" name="special-input-1" id="Upload" value="" onchange="readURL(this)" class="file" multiple></p></div>' +
                                 '</div>' +
                                 '<div class="columns" id="demo">' +
                                     '<img  id="img_prev" /><br>' +
                                    '<div class="six-columns bold"><label>Name:<span class="red">*</span></label><div class="input full-width"><input name="prompt-value" value="" id="txt_Name" placeholder="Promo Name" class="input-unstyled full-width" type="text"></div></div>' +
                                    '<div class="six-columns bold"><label>Facility:<span class="red">*</span></label><div class="input full-width"><input name="prompt-value" value="" id="txt_Facility" placeholder="Facility" class="input-unstyled full-width" type="text" placeholder="Facility"></div></div>' +
                                    '<div class="six-columns bold"><label>Service:</label><div class="input full-width"><input name="prompt-value" value="" id="txt_service" placeholder="Service" class="input-unstyled full-width" type="text" placeholder="124th Floor (non peak)"></div></div>' +
                                    '<div class="six-columns bold"><label>Start Date:<span class="red">*</span></label><div class="input full-width"><input name="prompt-value" value="" id="txt_Start" placeholder="" class="input-unstyled full-width" type="text" ></div></div>' +
                                    '<div class="six-columns bold"><label>Details:<span class="red">*</span></label><div class="input full-width"><input name="prompt-value" value="" id="txt_details" placeholder="Details" class="input-unstyled full-width" type="text"></div></div>' +
                                    '<div class="six-columns bold"><label>Note:<span class="red">*</span></label><div class="input full-width"><input name="prompt-value" value="" id="txt_Note" placeholder="" class="input-unstyled full-width" type="text"></div>' +
                                '</div>' +
                            '</div>' +
                            '<p class="text-alignright"><button type="submit" value="Add" id="btn_AddPromo" class="button anthracite-gradient" onclick="UploadSliderImage();"></button> <button type="reset" class="button anthracite-gradient">Reset</button>' +
                            '</p>' +
                        '</div>',

                title: 'Upload Image',
                width: 500,
                scrolling: true,
                actions: {
                    'Close': {
                        color: 'red',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttons: {
                    'Close': {
                        classes: 'huge anthracite-gradient displayNone',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttonsLowPadding: true
            });
        };
        // deletTrush alert
        function deletTrush() {
            $.modal({
                content: '<p class="avtiveDea">Are you sure you want to Delete this package</strong></p>' +
'<p class="text-alignright text-popBtn"><button type="submit" class="button anthracite-gradient">OK</button></p>',


                width: 300,
                scrolling: false,
                actions: {
                    'Close': {
                        color: 'red',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttons: {
                    'Close': {
                        classes: 'anthracite-gradient glossy',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttonsLowPadding: false
            });
        };



    </script>
</asp:Content>
