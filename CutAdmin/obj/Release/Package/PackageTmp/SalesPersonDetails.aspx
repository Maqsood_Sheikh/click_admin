﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="SalesPersonDetails.aspx.cs" Inherits="CutAdmin.SalesPersonDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/SalesDetails.js?v=1.4"></script>
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <!-- Main content -->
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>All Sales Persons</h1>
            <hr />
            <h2><a href="AddSalesPerson.aspx" class="addnew"><i class="fa fa-user-plus"></i></a>
                <a href="#" class="addnew"><i class="fa fa-filter"></i></a></h2>
            <br />
            <div id="filter" class="with-padding anthracite-gradient" style="display: none;">
                <form class="form-horizontal">
                    <div class="columns">
                        <div class="three-columns four-columns-tablet twelve-columns-mobile">
                            <label>Name</label>
                            <div class="input full-width">
                                <input type="text" id="txt_Name" class="input-unstyled full-width">
                                <input type="hidden" id="hdnDCode">
                            </div>
                        </div>

                        <div class="two-columns four-columns-tablet twelve-columns-mobile">
                            <label>Territory</label>
                            <div class="full-width button-height">
                                <select id="selTerritory" class="select">
                                    <option selected="selected" value="">Select Territory</option>
                                    <option value="Country">Country</option>
                                    <option value="State">State</option>
                                    <option value="City">City</option>
                                    <option value="Agency">Agency</option>
                                </select>
                            </div>
                        </div>
                         <div class="two-columns twelve-columns-mobile formBTn">
                            <br />
                            <button type="button" class="button anthracite-gradient" onclick="Search()">Search</button>
                            <button type="button" class="button anthracite-gradient" onclick="reset()">Reset</button>

                        </div>
                        <div class="two-columns four-columns-tablet twelve-columns-mobile">
                            <br />
                            <span class="icon-pdf right" onclick="ExportAgentDetailsToExcel('PDF')">
                                <img src="../img/PDF-Viewer-icon.png" style="cursor: pointer" title="Export To Pdf" height="35" width="35">
                            </span>
                            <span class="icon-excel right" onclick="ExportAgentDetailsToExcel('excel')">
                                <img src="../img/Excel-2-icon.png" style="cursor: pointer" title="Export To Excel" id="btnPDF" height="35" width="35"></span>
                        </div>
                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
        </hgroup>
        <div class="with-padding">
            <div class="respTable">
                <table class="table responsive-table" id="tbl_SalesDetails">
                    <thead>
                        <tr>
                            <th scope="col">S.No</th>
                            <th scope="col">Name</th>
                            <th scope="col" class="align-center" >Login Details</th>
                            <th scope="col" class="align-center">Territory</th>
                            <th scope="col" class="align-center">Update</th>
                            <th scope="col" class="align-center">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <script>
        jQuery(document).ready(function () {
            jQuery('.fa-filter').click(function () {
                jQuery(' #filter').slideToggle();
                jQuery('.searchBox li a ').on('click', function () {
                    jQuery('.filterBox #filter').slideUp();
                });
            });
        });
    </script>
    
</asp:Content>
