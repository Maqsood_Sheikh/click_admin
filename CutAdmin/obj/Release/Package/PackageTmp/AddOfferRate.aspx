﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddOfferRate.aspx.cs" Inherits="CutAdmin.AddOfferRate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/AddOfferRate.js"></script>

       <!-- Additional styles -->
	<link rel="stylesheet" href="css/styles/form.css?v=1">
	<link rel="stylesheet" href="css/styles/switches.css?v=1">
	<link rel="stylesheet" href="css/styles/table.css?v=1">

	<!-- DataTables -->
	<link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">
   

	<!-- Microsoft clear type rendering -->
	<meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

		
        <div class="with-padding">

            <div class="columns" id="DivAddOffer">
             
                       <h3 class="wrapped margin-left white" style="background-color: #006699;width:90%">Add Offer Rate</h3>
                 <div class="new-row twelve-columns">
                    <input id="htlname" style="background:none;border:none;color:#006699;font-size:medium;font-weight:bold;width:100%" readonly/>
                </div>
                    <div class="new-row one-columns"></div>
                     <div class="five-columns">
                        <span class="text-center">Season</span>
                        <select class="full-width  select  expandable-list" name="selectSeason" id="ddlSeason" onchange="ChangeSeason()" style="margin-bottom: 10px;"></select>
                        <label style="color: red; margin-top: 3px; display: none" id="lbl_ddlSeason">
                            <b>* This field is required</b></label>
                    </div>
                    <div class="four-columns">
                        <span class="text-left">Meal Plan:</span>
                        <br />
                        <select id="SelectMealPlan" class="full-width  select  expandable-list">
                            <option value="-">Select Meal Plan</option>
                            <option value="Room Only">Room Only</option>
                            <option value="Bed & Breakfast">Bed & Breakfast</option>
                            <option value="Half Board">Half Board</option>
                            <option value="Full Board">Full Board</option>
                        </select>
                        <label style="color: red; margin-top: 3px; display: none" id="lbl_SelectMealPlan">
                            <b>* This field is required</b></label>
                    </div>

                            <div class="new-row one-columns"></div>
                            <div class="three-columns" id="DivRdbDaysPrior">
                               <br />
                                <input type="radio" id="rdb_DaysPrior" value="Normal" checked="checked" name="Rdb" onclick="PriorFixed()" />
                                 <label for="rdb_DaysPrior" class="text-left">Days Prior  </label>
                            </div>
                            <div class="two-columns" id="DivRdbFixedDate">
                                <br />
                                <input type="radio" id="rdb_FixedDate" value="Normal" name="Rdb" onclick="PriorFixed()" />
                                <label for="rdb_FixedDate" class="text-left">Book Before  </label>
                            </div>

                            <div class="five-columns" id="DaysPrior" >
                                <span class="text-left">Days Prior   </span>
                                <br>
                                <input type="text" class="input full-width" id="txtDaysPrior" />
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txtDaysPrior">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="five-columns" id="FixedDate" style="display:none" none;">
                                <span class="text-left">Fixed Date  </span>
                                <br>
                                <input class="input full-width mySelectCalendar"  type="text" id="datepicker3" readonly="readonly" style="cursor: pointer" />
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_datepicker3">
                                    <b>* This field is required</b></label>
                            </div>
                
                            <div class="new-row one-columns"></div>
                            <div class="three-columns">
                                <span class="text-left">Offer Type:</span>
                                <br>
                                <select id="ddlOfferType" onchange="OfferTypeChange()" class="full-width  select">
                                    <option value="Discount">Discount</option>
                                    <option value="Deal">Deal</option>
                                    <option value="Freebi">Freebi</option>
                                </select>
                            </div>
                            <div class="two-columns" id="DivRdbDiscountPer">
                                <br>
                                <input type="radio" id="rdb_DiscountPer" value="Normal" checked="checked" name="type" onclick="DiscountChange()" />
                                 <label for="rdb_DiscountPer" class="text-left">Discount % </label>
                            </div>
                            <div class="two-columns" id="DivRdbDiscountAmount">                               
                                <br>
                                <input type="radio" id="rdb_DiscountAmount" value="Normal" name="type" onclick="DiscountChange()" />
                                 <label for="rdb_DiscountAmount" class="text-left">Discount Amount </label>
                            </div>

                            <div class="three-columns" id="DivItemName" style="display:none" none">
                                <span class="text-left">Free Item Name </span>
                                <br>
                                <input id="txt_ItemName" class="input full-width" type="text" />
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txt_ItemName">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="three-columns" id="DivItemDetails" style="display:none" none;">
                                <span class="text-left">Free Item Details </span>
                                <br>
                                <textarea rows="1" cols="50" name="comment" id="txt_ItemDetails" class="input full-width"></textarea>
                                <%--<input id="txt_ItemDetails" class="form-control" type="text" />--%>
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txt_ItemDetails">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="three-columns" id="DivDiscount">
                                <span class="text-left">Discount %: </span>
                                <br>
                                <input id="txtDiscountPer" class="input full-width" type="text" />
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txtDiscountPer">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="three-columns" id="DivDiscountAmount" style="display:none" none">
                                <span class="text-left">Discount Amount: </span>
                                <br>
                                <input id="txt_DiscountAmount" class="input full-width" type="text" />
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txt_DiscountAmount">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="three-columns" id="DivNewRate" style="display:none" none">
                                <span class="text-left">New Rate: </span>
                                <br>
                                <input id="txtNewRate" class="input full-width" type="text" />
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txtNewRate">
                                    <b>* This field is required</b></label>
                            </div>
                       
                             <div class="new-row one-columns"></div>
                            <div class="five-columns">
                                <span class="text-left">Hotel Offer Code:</span>
                                <br>
                                <input id="txtHotelOfferCode" class="input full-width" type="text">
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txtHotelOfferCode">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="five-columns">
                                <span class="text-left">CUT Offer Code:</span>
                                <br>
                                <input id="txtHotelCode" class="input full-width" type="text">
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txtHotelCode">
                                    <b>* This field is required</b></label>
                            </div>

                             <div class="new-row one-columns"></div>
                            <div class="five-columns" >
                                <span class="text-left">Offer Term:</span>
                                <br>
                                <textarea rows="4" name="comment" id="txtOfferTerm" class="input full-width"></textarea>
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txtOfferTerm">
                                    <b>* This field is required</b></label>

                            </div>
                            <div class="five-columns" >
                                <span class="text-left">Offer Note:</span>
                                <br>
                                <textarea rows="4"  name="comment" id="txtOfferNote" class="input full-width"></textarea>
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txtOfferNote">
                                    <b>* This field is required</b></label>

                            </div>

                 <div class="new-row one-columns"></div>
                            <br />
                            <div class="five-columns">
                                <span class="text-left">Booking Type:</span>
                                <br>
                                <input id="txtBookingType" class="input full-width" type="text">
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txtBookingType">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="new-row  eleven-columns">
                                <hr /><br />
                                <input type="button" onclick="AddUpdateOfferDetails()" class="button blue-gradient" value="Submit" id="btn_Popup" style="float: right" />
                            </div>
                    </div><br /><br />
                    <!-- END OF TAB 1 -->
            </div>
        <div style="display:none">
             <!-- DROPDOWN 1 -->
                    <div class="tab-pane padding40" id="dropdown1">
                        dropdown1
                       
                    </div>
                    <!-- END OF DROPDOWN 1 -->
                    <!-- DROPDOWN 2 -->
                    <div class="tab-pane padding40" id="dropdown2">
                        Updates
                       
                    </div>
                    <!-- END OF DROPDOWN 2 -->
                    <!-- TAB 2 -->
                    <div class="tab-pane padding40" id="packages1">
                        <div class="padding40">
                        </div>
                    </div>
                    <!-- END OF TAB 2 -->
                    <!-- TAB 3 -->
                    <div class="tab-pane" id="media">
                        <div class="padding40">
                            Media - comingsoon
                           
                        </div>
                    </div>
                    <!-- END OF TAB 3 -->
                    <!-- TAB 4 -->
                    <div class="tab-pane" id="pages">
                        <div class="padding40">
                            Pages - comingsoon
                           
                        </div>
                    </div>
                    <!-- END OF TAB 4 -->
                    <!-- TAB 5 -->
                    <div class="tab-pane" id="comments">
                        <div class="padding40">
                            Comments - comingsoon
                           
                        </div>
                    </div>
                    <!-- END OF TAB 5 -->
                    <!-- TAB 6 -->
                    <div class="tab-pane" id="forums">
                        <div class="padding40">
                            Forums - comingsoon
                           
                        </div>
                    </div>
                    <!-- END OF TAB 6 -->
                    <!-- TAB 7 -->
                    <div class="tab-pane" id="topics">
                        <div class="padding40">
                            Topics - comingsoon
                           
                        </div>
                    </div>
                    <!-- END OF TAB 7 -->
                    <!-- TAB 8 -->
                    <div class="tab-pane" id="replies">
                        <div class="padding40">
                            Replies - comingsoon
                           
                        </div>
                    </div>
                    <!-- END OF TAB 8 -->
                    <!-- TAB 9 -->
                    <div class="tab-pane" id="appearance">
                        <div class="padding40">
                            Appearance - comingsoon
                           
                        </div>
                    </div>
                    <!-- END OF TAB 9 -->
                    <!-- TAB 10 -->
                    <div class="tab-pane" id="plugins">
                        <div class="padding40">
                            Plugins - comingsoon
                           
                        </div>
                    </div>
                    <!-- END OF TAB 10 -->
                    <!-- TAB 11 -->
                    <div class="tab-pane" id="users">
                        <div class="padding40">
                            Users - comingsoon
                           
                        </div>
                    </div>
                    <!-- END OF TAB 11 -->
                    <!-- TAB 12 -->
                    <div class="tab-pane" id="tools">
                        <div class="padding40">
                            Tools - comingsoon
                           
                        </div>
                    </div>
                    <!-- END OF TAB 12 -->
                    <!-- TAB 13 -->
                    <div class="tab-pane" id="settings">
                        <div class="padding40">
                            Settings - comingsoon
                           
                        </div>
                    </div>
                    <!-- END OF TAB 13 -->

        </div>

                   

                       
	</section>
	<!-- End main content -->



	<!-- JavaScript at the bottom for fast page loading -->
	<!-- Scripts -->
	<script src="js/libs/jquery-1.10.2.min.js"></script>
	<script src="js/setup.js"></script>

	<!-- Template functions -->
	<script src="js/developr.input.js"></script>
	<script src="js/developr.navigable.js"></script>
	<script src="js/developr.notify.js"></script>
	<script src="js/developr.scroll.js"></script>
	<script src="js/developr.tooltip.js"></script>
	<script src="js/developr.table.js"></script>
    <script src="js/developr.accordions.js"></script>
    <script src="js/developr.wizard.js"></script>

	<!-- Plugins -->
	<script src="js/libs/jquery.tablesorter.min.js"></script>
	<script src="js/libs/DataTables/jquery.dataTables.min.js"></script>

	<script>

	    // Call template init (optional, but faster if called manually)
	    $.template.init();

	    // Table sort - DataTables
	    var table = $('#sorting-advanced');
	    table.dataTable({
	        'aoColumnDefs': [
				{ 'bSortable': false, 'aTargets': [0, 5] }
	        ],
	        'sPaginationType': 'full_numbers',
	        'sDom': '<"dataTables_header"lfr>t<"dataTables_footer"ip>',
	        'fnInitComplete': function (oSettings) {
	            // Style length select
	            table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
	            tableStyled = true;
	        }
	    });

	    // Table sort - styled
	    $('#sorting-example1').tablesorter({
	        headers: {
	            0: { sorter: false },
	            5: { sorter: false }
	        }
	    }).on('click', 'tbody td', function (event) {
	        // Do not process if something else has been clicked
	        if (event.target !== this) {
	            return;
	        }

	        var tr = $(this).parent(),
				row = tr.next('.row-drop'),
				rows;

	        // If click on a special row
	        if (tr.hasClass('row-drop')) {
	            return;
	        }

	        // If there is already a special row
	        if (row.length > 0) {
	            // Un-style row
	            tr.children().removeClass('anthracite-gradient glossy');

	            // Remove row
	            row.remove();

	            return;
	        }

	        // Remove existing special rows
	        rows = tr.siblings('.row-drop');
	        if (rows.length > 0) {
	            // Un-style previous rows
	            rows.prev().children().removeClass('anthracite-gradient glossy');

	            // Remove rows
	            rows.remove();
	        }

	        // Style row
	        tr.children().addClass('anthracite-gradient glossy');

	        // Add fake row
	        $('<tr class="row-drop">' +
				'<td colspan="' + tr.children().length + '">' +
					'<div class="float-right">' +
						'<button type="submit" class="button glossy mid-margin-right">' +
							'<span class="button-icon"><span class="icon-mail"></span></span>' +
							'Send mail' +
						'</button>' +
						'<button type="submit" class="button glossy">' +
							'<span class="button-icon red-gradient"><span class="icon-cross"></span></span>' +
							'Remove' +
						'</button>' +
					'</div>' +
					'<strong>Name:</strong> John Doe<br>' +
					'<strong>Account:</strong> admin<br>' +
					'<strong>Last connect:</strong> 05-07-2011<br>' +
					'<strong>Email:</strong> john@doe.com' +
				'</td>' +
			'</tr>').insertAfter(tr);

	    }).on('sortStart', function () {
	        var rows = $(this).find('.row-drop');
	        if (rows.length > 0) {
	            // Un-style previous rows
	            rows.prev().children().removeClass('anthracite-gradient glossy');

	            // Remove rows
	            rows.remove();
	        }
	    });

	    // Table sort - simple
	    $('#sorting-example2').tablesorter({
	        headers: {
	            5: { sorter: false }
	        }
	    });

	</script>

    <script>

        $(document).ready(function () {
            // Elements
            var form = $('.wizard'),

				// If layout is centered
				centered;

            // Handle resizing (mostly for debugging)
            function handleWizardResize() {
                centerWizard(false);
            };

            // Register and first call
            $(window).on('normalized-resize', handleWizardResize);

            /*
			 * Center function
			 * @param boolean animate whether or not to animate the position change
			 * @return void
			 */
            function centerWizard(animate) {
                form[animate ? 'animate' : 'css']({ marginTop: Math.max(0, Math.round(($.template.viewportHeight - 30 - form.outerHeight()) / 2)) + 'px' });
            };

            // Initial vertical adjust
            centerWizard(false);

            // Refresh position on change step
            form.on('wizardchange', function () { centerWizard(true); });

            // Validation
            if ($.validationEngine) {
                form.validationEngine();
            }
        });

	</script>
 </asp:Content>
