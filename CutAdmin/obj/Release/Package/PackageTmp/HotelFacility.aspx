﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="HotelFacility.aspx.cs" Inherits="CutAdmin.HotelFacility" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!-- Additional styles -->
    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">

    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
    <script src="Scripts/HotelFacility.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <hgroup id="main-title" class="thin">
            <h1>Hotel Facility</h1>
        </hgroup>
        <hr />
        <div class="with-padding">
            <div class="columns">
                <div class="three-columns two-columns-tablet twelve-columns-mobile">
                    <label for="block-label-1" class="label">Facility Name</label>
                   
                    <input type="text" name="block-label-1" id="txt_facilityName" class="input full-width" value="">
                </div>
                <div class="three-columns two-columns-tablet twelve-columns-mobile" style="padding-top: 0.5%">
                     <br />
                    <span class="input file">
                        <span class="file-text">Select Icon</span>
                        <span class="button compact">Select files</span>
                        <input type="file" name="special-input-1" id="File_FacilityImage" value="" class="file withClearFunctions" multiple="">
                    </span>
                </div>
                <div class="three-columns two-columns-tablet twelve-columns-mobile" style="padding-top: 0.5%">
                    <br />
                    <button type="button" class="button anthracite-gradient" onclick="UploadingImage()" id="btn_Add">ADD</button>
                    <button type="button" class="button anthracite-gradient" onclick="UploadingFacilityImage()" style="display: none" id="btn_Update">Update</button>
                </div>
            </div>
            <table class="table responsive-table" id="tbl_HotelFacility">
                <thead>
                    <tr>
                        <th scope="col"  class="align-center">Sr No</th>
                        <th scope="col"  class="align-center">Facility Name</th>
                        <th scope="col"  class="align-center">Facility Icon</th>
                        <%--<th scope="col" width="15%" class="hide-on-tablet">Tags</th>--%>
                        <th scope="col"  class="align-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

    </section>
</asp:Content>
