var Langitude = "";
var Latitude = "";
var title = "";
var HotelName = "";
var HotelImage = "";
var infowindow;
var map;
function initialize() {
    // Create an array of styles.
  //  infowindow = new google.maps.InfoWindow();
		var styles = [
			{
				featureType: 'road.highway',
				elementType: 'all',
				stylers: [
					{ hue: '#e5e5e5' },
					{ saturation: -100 },
					{ lightness: 72 },
					{ visibility: 'simplified' }
				]
			},{
				featureType: 'water',
				elementType: 'all',
				stylers: [
					{ hue: '#30a5dc' },
					{ saturation: 47 },
					{ lightness: -31 },
					{ visibility: 'simplified' }
				]
			},{
				featureType: 'road',
				elementType: 'all',
				stylers: [
					{ hue: '#cccccc' },
					{ saturation: -100 },
					{ lightness: 44 },
					{ visibility: 'on' }
				]
			},{
				featureType: 'landscape',
				elementType: 'all',
				stylers: [
					{ hue: '#ffffff' },
					{ saturation: -100 },
					{ lightness: 100 },
					{ visibility: 'on' }
				]
			},{
				featureType: 'poi.park',
				elementType: 'all',
				stylers: [
					{ hue: '#d2df9f' },
					{ saturation: 12 },
					{ lightness: -4 },
					{ visibility: 'on' }
				]
			},{
				featureType: 'road.arterial',
				elementType: 'all',
				stylers: [
					{ hue: '#e5e5e5' },
					{ saturation: -100 },
					{ lightness: 56 },
					{ visibility: 'on' }
				]
			},{
				featureType: 'administrative.locality',
				elementType: 'all',
				stylers: [
					{ hue: '#000000' },
					{ saturation: 0 },
					{ lightness: 0 },
					{ visibility: 'on' }
				]
			}
		];

		
		var myLatlng = new google.maps.LatLng(Latitude, Langitude);


	  // Create a new StyledMapType object, passing it the array of styles,
	  // as well as the name to be displayed on the map type control.
	  //var styledMap = new google.maps.StyledMapType(styles,
	  //  {name: "Styled Map"});


	  // Create a map object, and include the MapTypeId to add
	  // to the map type control.
	//  var mapOptions = {
	//	zoom: 15,
	//	center: myLatlng,
	//	//mapTypeControlOptions: {
	//	//  mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
	//      //}
	////	mapTypeId: google.maps.MapTypeId.ROADMAP
	//  };

	//  var map = new google.maps.Map(document.getElementById('map-canvas'),
	//	mapOptions);
		
		var map = new google.maps.Map(document.getElementById('map-canvas'), {
		    zoom:12,
		    center: myLatlng,
		    mapTypeId: google.maps.MapTypeId.ROADMAP
		});

		var infowindow = new google.maps.InfoWindow();
	  var marker = new google.maps.Marker({
		  position: myLatlng,
		  map: map,
		  title: HotelName,

	  });

	  //google.maps.event.addListener(marker, 'click', function () {
	  //    return function () {
	  //        infowindow.setContent('<h4>' + HotelName + '</h4><br /><table><tr> <td><img src="' + HotelImage + '" width="200" height="100"/></td></tr></tabel>');
	  //        infowindow.open(map, marker);
	  //    };
    //});
	  google.maps.event.addListener(marker, 'click', (function (marker, HotelName, HotelImage) {
	      return function () {
	          infowindow.setContent('<h4>' + HotelName + '</h4><br /><table><tr> <td><img src="' + HotelImage + '" width="300" height="100"/></td></tr></tabel>');
	          infowindow.open(map, marker);
	      };
	  })(marker, HotelName, HotelImage));
	  ////Associate the styled map with the MapTypeId and set it to display.
	  map.mapTypes.set('map_style', styledMap);
	  map.setMapTypeId('map_style');
	}

	
	//function loadScript() {
	//    setTimeout(function (){
	//	  $('#map-canvas').css({'display':'block'});
	//	  var script = document.createElement('script');
	//	  script.type = 'text/javascript';
	//	  script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&' +
	//		  'callback=initialize';
	//	  document.body.appendChild(script);
		  
	//	  google.maps.event.trigger(map, 'resize');
	//    }, 500);	
//}
	function onItemClick(pin) {
	    var contentString = HotelName + "<hr/> <img alt=\"\" src=\"" + HotelImage + "\" />";
	    infowindow.setContent(contentString);
	    infowindow.setPosition(pin.position);
	    infowindow.open(map)
	}
	function loadScript() {
	   
	    Latitude = $('#map-canvas').attr('data-latitude');
	    Langitude = $('#map-canvas').attr('data-langitude');
	    title = $('#map-canvas').attr('data-Name');
	    HotelName = $('#map-canvas').attr('data-Name');
	    HotelImage = $('#map-canvas').attr('data-Image');
	    setTimeout(function () {
	        $('#map-canvas').css({ 'display': 'block' });
	        var script = document.createElement('script');
	        script.type = 'text/javascript';
	        script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&' +
                'callback=initialize';
	        document.body.appendChild(script);

	        google.maps.event.trigger(map, 'resize');
	    }, 500);
	}
	