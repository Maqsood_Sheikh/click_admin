﻿var arrFacilities = []; var arr;
var arrHotellist = new Array();
var arrFacilitiesList = new Array();
var pFacilities="", selectedFacilities = [];

$(document).ready(function () {
  
   var trRequest = "";
    // $("#tbl_HotelList").dataTable().fnClearTable();
    //$("#tbl_HotelList").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "HotelHandler.asmx/GetMappedHotels",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrHotellist = result.MappedHotelList;
               $("#tbl_HotelList tbody").remove();
                //Hotels List
                if (arrHotellist.length > 0) {
                    trRequest += '<tbody>';
                    for (i = 0; i < arrHotellist.length; i++) {
                        arr = arrHotellist[i].HotelFacilities;
                        arrFacilities = arr.split(',');
                        trRequest += '<tr onclick="ShowRow(' + arrHotellist[i].sid + ')">';
                        trRequest += '<th scope="col" width="35%">' + arrHotellist[i].HotelName + '</th>';
                        if (arrHotellist[i].DotwCode != '')
                        {
                            trRequest += '<td scope="col" width="10%">' + arrHotellist[i].DotwCode + '</td>';
                        }
                        else {
                            trRequest += '<td scope="col" width="10%" align="center"><a button  onclick="openHotelCodeModal(value,title)" value="' + arrHotellist[i].sid + '" title="DoTW Code" class="button icon-plus" style="background:none;">Add</a></td>';
                        }
                        if (arrHotellist[i].GRNCode != '')
                        {
                            trRequest += '<td scope="col" width="10%">' + arrHotellist[i].GRNCode + '</td>';
                        }
                        else {
                            trRequest += '<td scope="col" width="10%" align="center"><button  onclick="openHotelCodeModal(value,title)" value="' + arrHotellist[i].sid + '" title="GRN Code" class="button icon-plus" style="background:none;">Add</a></td>';
                        }
                        if (arrHotellist[i].HotelBedsCode != '')
                        {
                            trRequest += '<td scope="col" width="10%">' + arrHotellist[i].HotelBedsCode + '</td>';
                        }
                        else {
                            trRequest += '<td scope="col" width="10%" align="center"><a button  onclick="openHotelCodeModal(value,title)" value="' + arrHotellist[i].sid + '" title="HotelBeds Code" class="button icon-plus" style="background:none;">Add</a></td>';
                        }
                        if (arrHotellist[i].ExpediaCode != '')
                        {
                            trRequest += '<td scope="col" width="10%">' + arrHotellist[i].ExpediaCode + '</td>';
                        }
                        else
                        {
                            trRequest += '<td scope="col" width="10%" align="center"><a button  onclick="openHotelCodeModal(value,title)" value="' + arrHotellist[i].sid + '" title="Expedia Code" class="button icon-plus" style="background:none;">Add</a></td>';
                        }
                       
                      
                   
                        trRequest += '</tr>';
                      

                    }
                    trRequest += '</tbody>';
                    $("#tbl_HotelList").append(trRequest);
                }
                $("#tbl_HotelList").dataTable(
                    {
                        "bLength": false,
                         bSort: false, sPaginationType: 'full_numbers',

                    });
            }
            else if (result.retCode == 0) {
                $("#tbl_HotelList tbody").remove();
                var trRequest = '<tbody>';
                trRequest += '<tr><td align="center" style="padding-top: 2%" colspan="8"><span><b>No record found</b></span></td></tr>';
                trRequest += '</tbody>';
                $("#tbl_HotelList").append(trRequest);
            }
           
        }

    });
       
    //}
});

//complex modal
function GetMappedHotels() {
    GetMappedHotels();
    $.modal({
        content: 
                 '<p class="button-height inline-label ">' +
                           '<label for="input-3" class="label">Facilities</label>' +
                                 '<select id="ddl_HtlFacilities"  class="full-width select multiple-as-single easy-multiple-selection allow-empty blue-gradient check-list" multiple>' +
                                       '<option  value="" selected="selected" disabled>Select Facilities</option>' +
                            '</select>' +
                  '</p>' +
                  '<p class="button-height align-right">' +
						'<button onclick="AddtoTextArea();"  class="button black-gradient glossy">Add</button>' +
				 '</p>'+
                  '<p class="button-height inline-label ">' +
                         '<label class="label">Present Facilities</label>' +
                         '<textarea id="addedFacilities" class="input full-width align-right">' + arr + '</textarea>' +
                 '</p>' +
                 '<br><p class="button-height align-right">' +
						'<button onclick="SaveFacilities(' + HotelCode + ');"  class="button black-gradient glossy">Save</button>' +
                         '<label id="lblFacilitiesMsg" style="color:red;display:none">Facilities Added</button>' +
				 '</p>',

        title: 'Add Facilities',
        width: 500,
        height:200,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        
        buttonsLowPadding: true
    });

};

function openHotelCodeModal(value, title) {
    var cancelled = false;

    $.modal.prompt('Enter ' + title + '', function (value) {
        if (value == '') {
            $(this).getModalContentBlock().message('Please enter ' + title + '', { append: false, classes: ['red-gradient'] });
            return false;
        }

        $.modal.alert('' + title + ': <strong>' + value + '</strong> saved');

    }, function () {
        if (!cancelled) {           
            cancelled = true;            
        }
    });
};

function AddRatings(HotelCode, Ratings) {
    $.modal({
        content:
                 '<p class="button-height inline-label ">' +
                           '<label for="input-3" class="label">Existing Ratings</label>' +
                           '<input  class="input full-width align-right" value="' + Ratings + '" readonly>' +
                  '</p>' +
                  '<p class="button-height inline-label ">' +
                           '<label for="input-3" class="label">New Ratings</label>' +
                           '<input id ="addedRatings" class="input full-width align-right" >' +
                  '</p>' +
                 '<br><p class="button-height align-right">' +
						'<button onclick="SaveRatings(' + HotelCode + ');"  class="button black-gradient glossy">Save</button><br>' +
                        '<label id="lblRatingsMsg" style="color:red;display:none">Saved Ratings</button>' +
				 '</p>',

        title: 'Add Facilities',
        width: 500,
        height: 200,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        buttonsLowPadding: true
    });

};



function UpdateImage(HotelCode, Url) {
    $.modal({
        content:
                 '<p class="button-height inline-label ">' +
                           '<label for="input-3" class="label">Existing Ratings</label>' +
                           '<input  class="input full-width align-right" value="' + Url + '" readonly>' +
                  '</p>' +
                  '<p class="button-height inline-label ">' +
                           '<label for="input-3" class="label">New Ratings</label>' +
                           '<input id ="addedUrl" class="input full-width align-right" >' +
                  '</p>' +
                 '<br><p class="button-height align-right">' +
						'<button onclick="SaveImageUrl(' + HotelCode + ');"  class="button black-gradient glossy">Save</button><br>' +
                        '<label id="lblImageMsg" style="color:red;display:none">Image Saved </button>' +
				 '</p>',

        title: 'Add Facilities',
        width: 500,
        height: 200,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        buttonsLowPadding: true
    });

};



function fnFacilities() {

    var HtlFacilities = document.getElementsByClassName('HtlFacilities');

    for (var i = 0; i < HtlFacilities.length; i++) {
        if (HtlFacilities[i].selected == true) {
            selectedFacilities[i] = HtlFacilities[i].value;
            pFacilities += selectedFacilities[i] + ',';
        }


    }


}

function AddtoTextArea()
{
    fnFacilities();
    var sFacilities = pFacilities.replace(/^,|,$/g, '');
    var nFacilities = arr + ',' + sFacilities
    $('#addedFacilities').val(nFacilities);
   // document.getElementById("ddl_HtlFacilities").disabled = true;
}

function getFacilities() {
    $.ajax({
        type: "POST",
        url: "HotelHandler.asmx/GetFacilities",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrFacilitiesList = result.HotelFacilities;
                if (arrFacilitiesList.length > 0) {
                    $("#ddl_HtlFacilities").empty();
                    var ddlRequest = '<option selected="selected" value="-" disabled>Select Facilities</option>';
                    var j = 0; 
                    for (i = 0; i < arrFacilitiesList.length; i++) {
                        ddlRequest += '<option type="checkbox" class="HtlFacilities" value="' + arrFacilitiesList[i].HotelFacilityName + '">' + arrFacilitiesList[i].HotelFacilityName + '</option>';
                        
                    }
                    $("#ddl_HtlFacilities").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });

}

function SaveFacilities(HotelCode)
{
    var sFacilities = $('#addedFacilities').val();
    var sHotelId = HotelCode;
    var data = {
        sHotelId: sHotelId,
        sFacilities: sFacilities
    };
    $.ajax({
        type: "POST",
        url: "HotelHandler.asmx/SaveFacilities",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                document.getElementById('lblFacilitiesMsg').style.display = "";
            }
        },
        error: function () {
        }
    });
}

function SaveRatings(HotelCode) {
    var sRatings = $('#addedRatings').val();
    var sHotelId = HotelCode;
    var data = {
        sHotelId: sHotelId,
        sRatings: sRatings
    };
    $.ajax({
        type: "POST",
        url: "HotelHandler.asmx/SaveRatings",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                document.getElementById('lblRatingsMsg').style.display = "";
            }
        },
        error: function () {
        }
    });
}


function SaveImageUrl(HotelCode) {
    var addedUrl = $('#addedUrl').val();
    var sHotelId = HotelCode;
    var data = {
        sHotelId: sHotelId,
        addedUrl: addedUrl
    };
    $.ajax({
        type: "POST",
        url: "HotelHandler.asmx/SaveImageUrl",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                document.getElementById('lblImageMsg').style.display = "";
            }
        },
        error: function () {
        }
    });
}