﻿var arrGroup = new Array();
var arrSearchOccupancy = new Array();
var arrCharges = new Array();
function GenrateB2bRates() {
    try {

        $("#div_SearchTable").css("display", "none");
        arrSearchOccupancy = new Array();
        $("#SPN_HotelName").text($("#txt_HotelName").val().toUpperCase())
        var HotelName = $('#txt_HotelName').val();
        $("#SPN_HotelName").append('<a onclick="GetMapAll()"><small class="icon-marker icon-size3"><strong>' + $("#txt_Destination").val() + '</strong></small></a>');
        $("#div_Image").addClass("icon-info-round")
        $("#lbl_Fillter").show(1000);
        $("#filter").hide(2000);
        $("#btn_mdfSearch").show(1000);
        $("#div_Search").hide(1000);
        $("#div_Rates").empty();
        var Html = '';
        arrGroup = arrRoomRates[0];
        var arrRoomNo = new Array();
        for (var i = 0; i < arrGroup.RoomOccupancy.length; i++) {
            for (var r = 0; r < arrGroup.RoomOccupancy[i].RoomNo.length; r++) {
                arrRoomNo.push(arrGroup.RoomOccupancy[i].RoomNo[r])
            }
        }
        arrRoomNo.sort(function (a, b) { return a - b });// Sort By RoomNo
        for (var r = 0; r < arrRoomNo.length; r++) {
            for (var i = 0; i < arrGroup.RoomOccupancy.length; i++) {
                var arrRooms = arrGroup.RoomOccupancy[i].RoomNo;
                var arrOccupant = $.grep(arrRooms, function (R) { return R == arrRoomNo[r] })
                                    .map(function (R) { return R; });
                if (arrOccupant.length != 0) {
                    arrSearchOccupancy.push(arrGroup.RoomOccupancy[i]);
                    break;
                }
            }
        }
        //arrSearchOccupancy.reverse();
        // Re arrangging Room By Search
        for (var i = 0; i < arrSearchOccupancy.length; i++) {
            Html = ''
            var DefaultSelected = "checked";
            Html += '<br/><h3 class="silver-gradient">Room No. ' + parseInt(i + 1)
            Html += Rooms;
            Html += '<span class="size13 bold"> (' + arrSearchOccupancy[i].AdultCount + ' Adults' + ' & ' + arrSearchOccupancy[i].ChildCount + ' Children </span>';
            if (arrSearchOccupancy[i].ChildCount != 0)
                Html += ' with Ages : ' + arrSearchOccupancy[i].ChildAges;

            Html += ')</h3>';
            Html += '<table class="tbl_Rates table responsive-table RoomOccupancy' + i + '" id="tbl_Occupancy_' + i + '">'
            Html += '<thead>'
            Html += '<tr><th>Room Name</th><th>Room Type</th><th>Rate</th><th>Cancellation</th><th>Select'

            Html += '<input type="hidden" value="' + arrSearchOccupancy[i].RoomCount + '" class="TotalRoom_' + i + '" />'
            Html += '<input type="hidden" value="' + arrSearchOccupancy[i].AdultCount + '" class="AdultCount' + i + '" />'
            Html += '<input type="hidden" value="' + arrSearchOccupancy[i].ChildCount + '" class="ChildCount' + i + '" />'
            Html += '<input type="hidden" value="' + arrSearchOccupancy[i].ChildAges + '" class="ChildAges' + i + '" />'
            Html += '</th></tr>'
            Html += '</thead>'
            Html += '<tbody>'
            var arrRooms = arrSearchOccupancy[i].Rooms;
            Html += '';
            for (var r = 0; r < arrRooms.length; r++) {
                Html += '<tr >'
                Html += '<td style="cursor:pointer">' + arrRooms[r].RoomTypeName
                Html += '<input type="hidden" value="' + arrRooms[r].RoomTypeId + '" class="RoomTypeId' + r + '" />'
                Html += '<input type="hidden" value="' + arrRooms[r].RoomDescription + '" class="RoomDescription' + r + '" />'
                Html += '<input type="hidden" value="' + arrRooms[r].Total + '" class="TotalRate' + r + '" />'

                Html += '</td>'
                Html += '<td style="cursor:pointer">' + arrRooms[r].RoomDescription + '</td>'
                Html += '<td style="cursor:pointer"><i class="' + GetCurrencyIcon(arrRooms[r].Dates[0].Currency) + '"> <i/> ' + numberWithCommas(roundToTwo(arrRooms[r].Total).toString()) + '</td>'
                Html += '<td style="cursor:pointer"><ul class="bullet-list">'
                for (var c = 0; c < arrRooms[r].ListCancel.length; c++) {
                    Html += '<li >' + arrRooms[r].ListCancel[c] + '</li>';
                }
                Html += '</ul></td>'
                Html += '<td>'
                Html += '<input type="radio" name="chk_' + i + '" ' + DefaultSelected + ' class="Room_' + i + '" />'
                DefaultSelected = "";
                Html += '</td>'
                Html += '</tr>'
            }
            Html += '</tbody>'
            Html += '</table>'
            $("#div_Rates").append(Html);
            GenrateTable(i)
        }
        Html = ''
        Html += '<p class="SearchBtn"><input type="button" class="button anthracite-gradient buttonmrgTop" onclick="BookingRates();" value="Book"></p>'
        $("#div_Rates").append(Html);
    }
    catch (ex) {

    }

}
function ShowSearch() {
    $("#SPN_HotelName").text("Search Hotel");
    $("#div_Search").toggle(1000);
    $("#btn_mdfSearch").hide(1000);
    $("#filter").hide(2000);
}
function BookingRates() {
    var arrBookingDetails = new Array();
    for (var i = 0; i < arrSearchOccupancy.length; i++) {
        var ndRoomOccupancy = $(".RoomOccupancy" + i);
        var ndRoom = $(".RoomOccupancy" + i).find(".Room_" + i);
        var AdultCount = $($(".RoomOccupancy" + i).find(".AdultCount" + i)).val();
        var ChildCount = $($(".RoomOccupancy" + i).find(".ChildCount" + i)).val();
        var ChildAges = $($(".RoomOccupancy" + i).find(".ChildAges" + i)).val();
        if (ndRoom.length != 0) {
            for (var r = 0; r < ndRoom.length; r++) {
                if (ndRoom[r].checked) {
                    //var noRooms = $(ndRoom[r]).val();
                    var RoomTypeID = $($(".RoomOccupancy" + i).find(".RoomTypeId" + r)).val();
                    var RoomDescriptionId = $($(".RoomOccupancy" + i).find(".RoomDescription" + r)).val();
                    var Total = $($(".RoomOccupancy" + i).find(".TotalRate" + r)).val()
                    //if (noRooms != 0) {
                    //for (var n = 0; n < noRooms; n++) {
                    arrBookingDetails.push({
                        RoomTypeID: RoomTypeID,
                        RoomDescriptionId: RoomDescriptionId,
                        Total: Total,
                        noRooms: parseInt(i + 1),
                        AdultCount: AdultCount,
                        ChildCount: ChildCount,
                        ChildAges: ChildAges
                    });
                    //    }

                    //}
                }

            }

        }
    }
    if (arrBookingDetails.length != 0) {
        $.ajax({
            type: "POST",
            url: "../handler/BookingHandler.asmx/GenrateBookingDetails",
            data: JSON.stringify({ ListRates: arrBookingDetails, Serach: session }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    window.location.href = "hotelbooking.aspx?data=" + session + "&OnHold=" + result.OnHold + "&OnRequest=" + result.OnRequest;
                }
                else {
                    AlertDanger(result.ex)
                }
            }
        })

    }
    else
        Success("Please Select Rate for Booking!!")
}
var arrRates = new Array();
function GetBookingDetails(session) {
    $.ajax({
        type: "POST",
        url: "../handler/BookingHandler.asmx/GetBookingDetails",
        data: JSON.stringify({ Serach: session }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrRates = result.ListRates;
                arrCharges = result.Charge;
                GenratePax();
                GrnrateBreakup();
            }
        }
    })
}
function roundToTwo(value) {
    return (Math.round(value * 100) / 100);
} // Genrate Design
function GenratePax() {
    var html = '';
    try {
        for (var i = 0; i < arrRates.length; i++) {

            html += '<label>Room No.  ' + parseInt(i + 1) + '</label>'
            /* Genrate Adults Details*/
            for (var a = 0; a < arrRates[i].AdultCount; a++) {
                html += '<div class="new-row columns ' + arrRates[i].RoomTypeId + '' + arrRates[i].RoomDescription + '">'
                html += ''
                html += '<div class="three-columns twelve-columns-mobile">'
                html += '<select id="sel_Gender' + a + '_' + arrRates[i].RoomTypeId + '_' + arrRates[i].RoomDescription + '_' + parseInt(i + 1) + '" class="select full-width">'
                html += '<option value="Mr">Mr</option>'
                html += '<option value="Mrs">Mrs</option>'
                html += '</select>'
                html += '</div>'
                html += '<div class="four-columns twelve-columns-mobile">'
                html += ''
                html += '<input type="text" class="input full-width" placeholder="First Name" value="" id="txtFirstName' + a + '_' + arrRates[i].RoomTypeId + '_' + arrRates[i].RoomDescription + '_' + parseInt(i + 1) + '">'
                html += '</div>'
                html += '<div class="four-columns twelve-columns-mobile">'
                html += ''
                html += '<input type="text" class="input full-width" placeholder="Last Name" value="" id="txtLastName' + a + '_' + arrRates[i].RoomTypeId + '_' + arrRates[i].RoomDescription + '_' + parseInt(i + 1) + '">'
                html += '</div>'
                html += '</div>'
            }


            /* Genrate Child Details*/
            var sAges = arrRates[i].ChildAges.split(',');
            for (var c = 0; c < arrRates[i].ChildCount; c++) {
                html += '<div class="new-row columns">'
                html += ''
                html += '<div class="three-columns twelve-columns-mobile">'
                html += ''
                html += '<select id="sel_childGender' + c + '_' + arrRates[i].RoomTypeId + '_' + arrRates[i].RoomDescription + '_' + parseInt(i + 1) + '" class="select  full-width">'
                html += '<option value="Master">Master</option>'
                html += '<option value="Miss">Miss</option>'
                html += '</select>'
                html += '</div>'
                html += '<div class="four-columns twelve-columns-mobile">'
                html += ''
                html += '<input type="text" class="input full-width FirstNC" placeholder="First Name" value="" id="txtCHFirstName' + c + '_' + arrRates[i].RoomTypeId + '_' + arrRates[i].RoomDescription + '_' + parseInt(i + 1) + '">'
                html += '</div>'
                html += '<div class="four-columns twelve-columns-mobile">'
                html += ''
                html += '<input type="text" class="input full-width Age" placeholder="Child Age" value="' + sAges[c] + '" id="txtAge' + c + '_' + arrRates[i].RoomTypeId + '_' + arrRates[i].RoomDescription + '_' + parseInt(i + 1) + '">'
                html += '</div>'
                html += ''
                html += ''
                html += ''
                html += '</div>'
            }
            html += '<div class="new-row columns">'
            html += '<div class="eleven-columns twelve-columns-mobile">'
            html += '<fieldset class="fieldset">'
            html += '<legend class="legend">Cancellation Policy</legend><ul class="bullet-list">'
            for (var j = 0; j < arrRates[i].ListCancel.length; j++) {
                html += '<li >' + arrRates[i].ListCancel[j] + '</li>';
            }
            html += '</ul></fieldset>'
            html += '</div>'
            html += '</div>'

        }
        html += '<div style="padding:50px  !important">'
        html += '<p class="SearchBtn "><input type="button" class="button anthracite-gradient buttonmrgTop" onclick="CheckAvailCredit();" value="Book"> '
        html += '<input type="button" class="button anthracite-gradient buttonmrgTop" onclick="window.location.href=\'' + 'searchhotel.aspx' + '\'" value="Cancel"> '
        html += '</p> </div>'
        $("#div_Rooms").append(html);
    }
    catch (ex) {

    }
}



function GrnrateBreakup() {
    var html = '';
    try {
        for (var i = 0; i < arrRates.length; i++) {
            html += '<div class="new-row columns">'
            html += '<div class="twele-columns twelve-columns-mobile">'
            html += '<fieldset class="wrapped button-height">'
            html += '<legend class="legend">Room ' + parseInt(i + 1) + '</legend>'
            html += '<label class="thin">' + arrRates[i].RoomTypeName + ' (' + arrRates[i].RoomDescription + ')</label>'
            html += '<label style="float: right;"><u></u>'
            html += arrRates[i].AdultCount + ' Adults '
            if (arrRates[i].ChildCount != 0) {
                html += arrRates[i].ChildCount + " Child (" + arrRates[i].ChildAges + ")";
            }

            html += '</label><hr/>'


            html += '<label class="thin">Rate Breakup </label> <span class="info-spot"> '
            html += ' <span class="icon-chevron-down"></span>'
            html += '<span class="info-bubble" style="width: 150px;">'
            for (var t = 0; t < arrRates[i].Dates.length; t++) {
                html += '<small class="orange"><b>' + arrRates[i].Dates[t].datetime + '</b>:</small> <small class="white"><i class="' + GetCurrencyIcon(arrRates[i].Dates[t].Currency) + '" />' + numberWithCommas(arrRates[i].Dates[t].Total.toString()) + '</small><br/>'
            }
            html += '</span>'
            html += '</span>'
            html += '<label class="thin" style="float: right;">'
            html += 'Room ' + parseInt(i + 1) + ' Total'
            html += '<br/><strong ><i class="' + GetCurrencyIcon(arrRates[i].Dates[0].Currency) + '" />' + numberWithCommas(roundToTwo(arrRates[i].Total)) + '</strong>'
            html += '</label>'

            html += '</fieldset>'
            html += '</div>'
            html += '</div>'
        }

        html += '<div class="new-row columns">'
        html += '<div class="twele-columns twelve-columns-mobile">'
        html += '<fieldset class="wrapped button-height">'
        html += '<legend class="legend">Bill Summary</legend>'
        html += '<p><label class="thin">Room Total </label>'
        html += '<label class="thin" style="float: right;">'
        html += '<strong ><i class="' + GetCurrencyIcon(arrRates[0].Dates[0].Currency) + '" />' + numberWithCommas(roundToTwo(arrCharges.RoomRate)) + '</strong>'
        html += '</label></p><hr/>'
        for (var i = 0; i < arrCharges.OtherRates.length; i++) {

            html += '<p><label class="thin">' + arrCharges.OtherRates[i].RateName + ' </label>'
            //html += ' <span class="info-spot"> <span class="icon-chevron-down"></span>'
            //html += '<span class="info-bubble" style="width: 150px;">'
            //for (var t = 0; t < arrCharges.OtherRates[i].TaxOn.length; t++) {
            //    html += '<small class="orange"><b>' + arrCharges.OtherRates[i].TaxOn[t].TaxName + '</b>:</small> <small class="white"><i class="' + GetCurrencyIcon(arrRates[0].Dates[0].Currency) + '" />' + numberWithCommas(arrCharges.OtherRates[i].TaxOn[t].TaxRate.toString()) + '</small>'
            //}
            //html += '</span>'
            //html += '</span>'
            html += '<label class="thin" style="float: right;">'
            html += '<strong ><i class="' + GetCurrencyIcon(arrRates[0].Dates[0].Currency) + '" />' + numberWithCommas(roundToTwo(arrCharges.OtherRates[i].TotalRate)) + '</strong>'
            html += '</label></p><hr/>'

        }
        html += '<p><label class="thin">Total Payable </label>'
        html += '<label class="thin" style="float: right;">'
        html += '<strong ><i class="' + GetCurrencyIcon(arrRates[0].Dates[0].Currency) + '" />' + numberWithCommas(roundToTwo(arrCharges.TotalPrice)) + '</strong>'
        html += '</label></p>'
        html += '</fieldset>'
        html += '</div>'
        html += '</div>'
        $("#div_Charges").append(html);
    }
    catch (ex) {

    }
}

function numberWithCommas(x) {
    x = x.toString();
    // get stuff before the dot
    var d = x.indexOf('.');
    var s2 = d === -1 ? x : x.slice(0, d);

    // insert commas every 3 digits from the right
    for (var i = s2.length - 3; i > 0; i -= 3)
        s2 = s2.slice(0, i) + ',' + s2.slice(i);

    // append fractional part
    if (d !== -1)
        s2 += x.slice(d);
    return s2;

}

function GetCurrencyIcon(Currency) {
    CurrencyClass = "";
    // switch (Currency) {
    if (Currency == "AED")
        CurrencyClass = "Currency-AED";
    //  break;
    if (Currency == "INR")
        CurrencyClass = "fa fa-inr";
    // break;
    if (Currency == "USD")
        CurrencyClass = "fa fa-dollar";
    // break;
    if (Currency == "SAR")
        CurrencyClass = "Currency-SAR";
    //break;
    if (Currency == "EUR")
        CurrencyClass = "fa fa-eur";
    // break;
    if (Currency == "GBP")
        CurrencyClass = "fa fa-gbp";
    //break;    
    // }
    return CurrencyClass;
}

function CheckAvailCredit() {
    if (ValidateField()) {
        var arrAddOns = new Array();
        var Search = getParameterByName('data')
        $.ajax({
            type: "POST",
            url: "../handler/BookingHandler.asmx/ValidateTransaction",
            data: JSON.stringify({ arrAdons: arrAddOns, Serach: Search }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    BookHotel()
                }
                else {
                    Success(result.ErrorMsg);
                    return false;
                }
            }
        })
    }
}
var arrLisCustumer = new Array();
function BookHotel() {
    var Search = getParameterByName('data')
    $.ajax({
        type: "POST",
        url: "../handler/BookingHandler.asmx/BookHotel",
        data: JSON.stringify({ arrAddOns: arrAddOns, Serach: Search, LisCustumer: arrLisCustumer }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                window.location.href = "Bookinglist.aspx"
            }
            else {
                Success(result.ErrorMsg);
                return false;
            }
        }
    })
}


function ValidateField() {
    var bValid = true;
    arrLisCustumer = new Array();
    for (var r = 0; r < arrRates.length; r++) {
        var RateTypeID = arrRates[r].RoomTypeId;
        var MealID = arrRates[r].RoomDescription;
        AdultCount = arrRates[r].AdultCount;
        ChildCount = arrRates[r].ChildCount;
        for (var ad = 0; ad < AdultCount; ad++) {
            if (ad == 0) {
                if ($("#txtFirstName" + ad + "_" + RateTypeID + "_" + MealID + "_" + (r + 1)).val() == "") {
                    Success("Enter Adults First Name Of Room No" + (r + 1) + "");
                    return false;

                }
                if ($("#txtLastName" + ad + "_" + RateTypeID + "_" + MealID + "_" + (r + 1)).val() == "") {
                    Success("Enter Adults Last Name Of Room No" + (r + 1) + "");
                    return false;
                }
            }

            var arrCustumer = {
                Age: 30,
                type: "AD",
                LastName: $("#txtLastName" + ad + "_" + RateTypeID + "_" + MealID + "_" + (r + 1)).val(),
                Name: $("#txtFirstName" + ad + "_" + RateTypeID + "_" + MealID + "_" + (r + 1)).val(),
                Title: $("#sel_Gender" + ad + "_" + RateTypeID + "_" + MealID + "_" + (r + 1) + " option:selected").val(),
                RoomNo: r + 1,
            }
            arrLisCustumer.push(arrCustumer);
        }
        for (var cd = 0; cd < ChildCount; cd++) {
            //if ($("#txtCHFirstName" + cd + "_" + RateTypeID + "_" + MealID + "_" + (0 + 1)).val() == "") {
            //   // Success("Enter Childs Name");
            //  //  return false;
            //}
            //if ($("#txtAge" + cd + "_" + RateTypeID + "_" + MealID + "_" + (0 + 1)).val() == "") {
            //   // Success("Select Childs Age");
            //   // return false;
            //}
            var arrCustumer = {
                Age: $("#txtAge" + cd + "_" + RateTypeID + "_" + MealID + "_" + (r + 1)).val(),
                type: "CH",
                Name: $("#txtCHFirstName" + cd + "_" + RateTypeID + "_" + MealID + "_" + (r + 1)).val(),
                Title: $("#sel_childGender" + cd + "_" + RateTypeID + "_" + MealID + "_" + (r + 1) + " option:selected").val(),
                RoomNo: r + 1,
            }
            arrLisCustumer.push(arrCustumer);
        }

    }
    return bValid;
} // Booking 

function GetHotelInfo() {
    var HotelCode = $('#listHotels option').filter(function () { return this.value == HotelName; }).data('id');
    $.ajax({
        type: "POST",
        url: "../handler/GenralHandler.asmx/GetHotelInfoBySearch",
        data: JSON.stringify({ Search: session }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var arrHotel = result.arrHotel;
                var html = ''
                var Title = arrHotel.HotelName + ', ' + arrHotel.Address;
                if (arrHotel.Category != 0)
                    Title += ' <img src="../img/' + arrHotel.Category + '.png" style="float: right;">';
                html += '<div class="columns">'
                html += '<div id="div_Images" class="new-row four-columns   twelve-columns-mobile"> </div>'
                html += '<div class="eight-columns   twelve-columns-mobile">'
                if (arrHotel.Description != null) {
                    html += '<fieldset class="fieldset fields-list"><legend class="legend">About:</legend><p style="text-align: justify;">'
                    if (arrHotel.Description != null)
                        html += arrHotel.Description
                    html += ' <i class="icon-quote icon-size2"></i></p></fieldset>'
                }

            }

            html += '</div>'
            html += '<div class="new-row twelve-columns   twelve-columns-mobile">'
            if (arrHotel.Facility.length != 0) {
                html += '<fieldset class="fieldset fields-list"><legend class="legend">Hotel Facility:</legend>'
                html += '<div class="columns twelve-columns-mobile">'
                for (i = 0; i < arrHotel.Facility.length; i = i + 3) {
                    html += FacilityDetails(arrHotel.Facility[i])
                    if ((i + 1) < arrHotel.Facility.length)
                        html += FacilityDetails(arrHotel.Facility[i + 1])
                    if ((i + 2) < arrHotel.Facility.length)
                        html += FacilityDetails(arrHotel.Facility[i + 2])
                }
                html += '</div>'
                html += '</fieldset>'
                html += '</div>'
                html += '</div>'
                $.modal({
                    content: html,
                    title: Title,
                    width: 1000,
                    height: 400,
                    scrolling: true,
                });
                $("#div_Images").append(GetImages(arrHotel.Image))
            }
            else {
                Success(result.ErrorMsg);
                return false;
            }
        }
    })
}
function GenrateTable(ID) {
    // Call template init (optional, but faster if called manually)
    $.template.init();
    // Table sort - styled
    $('#tbl_Occupancy_' + ID).tablesorter({
        headers: {
            0: { sorter: false },
            6: { sorter: false }
        }
    }).on('click', 'tbody td', function (event) {
        var drophotelId = this.id;

        // Do not process if something else has been clicked
        if (event.target !== this) {
            return;
        }

        var tr = $(this).parent(),
            row = tr.next('.row-drop'),
            rows;

        // If click on a special row
        if (tr.hasClass('row-drop')) {
            return;
        }

        // If there is already a special row
        if (row.length > 0) {

            // Un-style row
            tr.children().removeClass('anthracite-gradient glossy');

            // Remove row
            row.remove();

            return;
        }

        // Remove existing special rows
        rows = tr.siblings('.row-drop');
        if (rows.length > 0) {
            // Un-style previous rows
            rows.prev().children().removeClass('anthracite-gradient glossy');

            // Remove rows
            rows.remove();
        }

        // Style row
        tr.children().addClass('anthracite-gradient glossy');
        var RoomNo = tr[0].offsetParent.id.split('_')[2];
        var chkRoom = $(tr).find(".Room_" + RoomNo)[0];
        $(chkRoom).click();
        var ndRoom = $(".RoomOccupancy" + RoomNo).find(".Room_" + RoomNo);
        var RoomTypeID = "", RoomDescriptionId = "", Total = "0.00";
        for (var r = 0; r < ndRoom.length; r++) {
            if (ndRoom[r].checked) {
                RoomTypeID = $($(".RoomOccupancy" + RoomNo).find(".RoomTypeId" + r)).val();
                RoomDescriptionId = $($(".RoomOccupancy" + RoomNo).find(".RoomDescription" + r)).val();
                Total = $($(".RoomOccupancy" + RoomNo).find(".TotalRate" + r)).val()
                break;
            }
        }
        // Add fake row
        $('<tr  class="row-drop">' +
            '<td  colspan="' + tr.children().length + '">' +
              '<div class="columns">' +
                '<div id="DropRight' + RoomNo + '"  class="four-columns   twelve-columns-mobile"> </div>' +
                '<div   id="div_Dates' + RoomNo + '"   class="eight-columns  five-columns-mobile"> </div>' +
              '</div>' +
            '</td>' +
        '</tr>').insertAfter(tr);
        GetAvailbility(drophotelId, RoomTypeID, RoomDescriptionId, RoomNo)
    }).on('sortStart', function () {
        var rows = $(this).find('.row-drop');
        if (rows.length > 0) {
            // Un-style previous rows
            rows.prev().children().removeClass('anthracite-gradient glossy');

            // Remove rows
            rows.remove();
        }
    });
}
function GetAvailbility(id, RoomTypeID, RoomDescriptionId, RoomNo) {
    $.ajax({
        type: "POST",
        url: "../handler/BookingHandler.asmx/GetAvailibility",
        data: JSON.stringify({ Serach: session, RoomID: RoomTypeID, RoomDescID: RoomDescriptionId, RoomNo: parseInt(RoomNo) + 1 }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var arrDates = result.arrDates;
                var arrImage = result.arrImage;
                var html = "";
                html += '<div class="new-row coloumns">'
                html += '<div class="new-row coloumns"><div class="three-row"><label >Note:<span class="wrapped red-gradient " style="padding-top: 10px;">   No Inventory </span></label> '
                html += '<label ><span class="margin-bottom wrapped orange-gradient with-mid-padding align-center">  Low  Inventory</span></label>'
                html += ' <label><span class="margin-bottom wrapped green-gradient with-mid-padding align-center">High  Inventory</span></label></div>'
                html += ' </div>'
                html += '</div>'
                html += '<div class="new-row coloumns">'
                for (i = 0; i < arrDates.length; i = i + 4) {
                    html += DateDetails(arrDates[i])
                    if ((i + 1) < arrDates.length)
                        html += DateDetails(arrDates[i + 1])
                    if ((i + 2) < arrDates.length)
                        html += DateDetails(arrDates[i + 2])
                    if ((i + 3) < arrDates.length)
                        html += DateDetails(arrDates[i + 3])
                }
                html += '</div>'
                $('#div_Dates' + RoomNo).append(html);
                if (arrImage.length != 0)
                    $("#DropRight" + RoomNo).append(GetImages(arrImage));
            }
        }
    });


}
function DateDetails(arrDate) {
    var html = "";
    var Avail = "red";
    if (arrDate.NoOfCount >= 1 && arrDate.NoOfCount <= 100)
        Avail = "orange"
    else if (arrDate.NoOfCount == 0)
        Avail = "red"
    else
        Avail = "green"
    html += '<label  class="two-columns margin-bottom wrapped ' + Avail + '-gradient with-mid-padding align-center  twelve-columns-mobile"><n>' + arrDate.datetime + ' <br/>'
    html += '<i class="' + GetCurrencyIcon(arrDate.Currency) + '"> </i>' + arrDate.Total
    html += '</n></label>'
    return html;
}
function FacilityDetails(arrFacility) {
    var html = "";
    if (arrFacility.length > 15)
        arrFacility = arrFacility.substr(0, 15) + ".."
    html += '<div class="three-columns twelve-columns-mobile"><a href="javascript:void(0)" class="button"><span class="button-icon green-gradient" ><span class="icon-tick"></span></span> '
    html += arrFacility
    html += '</a></div>'
    return html;
}
var slideIndex = 1;
function GetImages(arrImage) {
    var html = '';
    html += ' <div class="w3-content w3-display-container">'
    for (var i = 0; i < arrImage.length; i++) {
        if (i == 0)
            html += '<img class="mySlides" src="' + arrImage[i].Url + '" style="width:300px; height:250px;display:block" onerror=ErroImage(this)>'
        else
            html += '<img class="mySlides" src="' + arrImage[i].Url + '" style="width:300px; height:250px">'
    }
    html += '<button class="w3-button w3-black w3-display-left" onclick="plusDivs(-1)">&#10094;</button>'
    html += '<button class="w3-button w3-black w3-display-right" onclick="plusDivs(1)">&#10095;</button>'
    html += '</div>'
    return html;
    showDivs(slideIndex);
}
function plusDivs(n) {
    showDivs(slideIndex += n);
}
function showDivs(n) {
    var i;
    var x = document.getElementsByClassName("mySlides");
    if (n > x.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = x.length }
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    x[slideIndex - 1].style.display = "block";
}
function ErroImage(Image) {
    Image.src = "../img/noImage.png"
    Image.onerror = "";
}

