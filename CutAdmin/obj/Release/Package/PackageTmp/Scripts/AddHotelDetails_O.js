﻿var sCity = "", sCountry = "", sZipCode = "", sLangitude = "", sLatitude = "", MainImage = "", SubImages = "", HotelBedsCode = "", DotwCode = "", MGHCode = "", ExpediaCode = "", GRNCode = "";
var HotelCode; var sid = 0, facilitiesCode; var nFacilities;
var rateBySupplier='';
var HotelImage; var HotelImageAdd = "";

$(function () {
    debugger;
    GetLocation();
    var sPageURL = window.location.href;
    var code1 = sPageURL.split('?');
    if (code1[1].split('=')[0] == 'sHotelID')
    {
        var code = code1[1].split('=')[1];
        HotelCode = code.split('&')[0];
        $('.DivUpdateMapping').css('display', '');
        $("#AddfrmWizard").removeClass("wizard");
        getHotelDetails(HotelCode);
        getFacilities();
        $("#tab_facility1 .wizard-next").click(function () {
            var fields = $("input[name='chkFacilities']").serializeArray();
            if (fields.length === 0) {
                Success('Please Select Atleast One Facility!');
                $(".wizard-prev ").click();
                return false;
            }
            
        })
        $("#tab_Plocies1").click(function () {
            var fields = $("input[name='chkFacilities']").serializeArray();
            if (fields.length === 0) {
                Success('Please Select Atleast One Facility!');
                $(".wizard-prev ").click();
                return false;
            }

        })
    }
    else
    {
        var data = getParameterBytxtLocation1Name('data');
        var HotelDesSubImg = $('#HotelDescription').val();
        var Description = HotelDesSubImg.split('|||')[0];
        SubImages = HotelDesSubImg.split('|||')[1];
        var data1 = JSON.parse(data);
        sHotelName = data1.sHotelName;
        $('#lblHotelname').text(sHotelName);
        sHotelAddress = data1.sHotelAddress;
        sHotelDescription = Description;
        var sHotelRatings1 = data1.sHotelRatings;
        sHotelRatings = sHotelRatings1.split('-')[0];
        rateBySupplier = sHotelRatings1.split('-')[1];
        sHotelFacilitiesText = data1.sHotelFacilitiesText;
        facilitiesCode = data1.sHotelFacilitiesCode;
        sCity = data1.sCity;
        sCountry = data1.sCountry;
        sZipCode = data1.sZipCode;
        sLangitude = data1.sLangitude;
        sLatitude = data1.sLatitude;

        getAddFacilities(sHotelFacilitiesText);
        $("#tab_facility .wizard-next").click(function () {
            var fields = $("input[name='chkFacilities']").serializeArray();
            if (fields.length === 0) {
                Success('Please Select Atleast One Facility!');
                $(".wizard-prev ").click();
                return false;
            }

        })
        $("#tab_Plocies").click(function () {
            debugger;
            var fields = $("input[name='chkFacilities']").serializeArray();
            if (fields.length === 0) {
                Success('Please Select Atleast One Facility!');
                $(".wizard-prev ").click();
                return false;
            }

        })
        HotelBedsCode = data1.sHotelBedsCode;
        DotwCode = data1.sDotwCode;
        MGHCode = data1.sMGHCode;
        ExpediaCode = data1.sExpediaCode;
        GRNCode = data1.sGRNCode;
        if (data1.CutHotelCode != "")
        {
            HotelCode = data1.CutHotelCode;
            getHotelDetails(HotelCode);
            getFacilities();
        }
        else
        {
            HotelCode = 0;
        }
        
      
       
       

        $('#HtlName').val(sHotelName);
        // $('#HtlRatings').text(sHotelRatings);
        
        fnStarRatings(sHotelRatings, rateBySupplier, divAdd, dropAdd);

        
        $('#HtlDescription').val(sHotelDescription);
        $('#HtlAddress').val(sHotelAddress);
        $('#htlCity').val(sCity);
        $('#htlCountry').val(sCountry);
        $('#htlZipcode').val(sZipCode);
        $('#htlLangitude').val(sLangitude);
        $('#htlLatitude').val(sLatitude);
        $('#HtlFacilities').val(sHotelFacilitiesText);
        if (MainImage != "") {
            $('.selMainImage-Add').attr('src', MainImage);
        }
        $('#ImgUrl-Add').val(MainImage);

        $('.DivAddMapping').css('display', '');
        
    }

   

})

var divAdd = "#HtlRatings";
var divUpdate = "#HtlRatings1";
var dropAdd = 'SelStars';
var dropUpdate = 'SelStars1';

function fnStarRatings(sHotelRatings, rateBySupplier, div, dropDn)
{

    $(div).empty();
    var trReques3 = '<p>';
    //trReques3 += ' <label style="float:left">' + arrSelectedHotels[i].Category + '</label>';
    if (sHotelRatings == 'Other' || sHotelRatings == '48055' || sHotelRatings == '0') {
        trReques3 += ' <label style="float:left"><i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;(' + rateBySupplier + ')</label>';
        sHotelRatings = '0';
    }
    else if (sHotelRatings == '1EST' || sHotelRatings == '559' || sHotelRatings == '1') {
        trReques3 += '<label style="float:left"><i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;(' + rateBySupplier + ')</label>';
        sHotelRatings = '1';
    }
    else if (sHotelRatings == '2EST' || sHotelRatings == '560' || sHotelRatings == '2') {
        trReques3 += '<label style="float:left"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;(' + rateBySupplier + ')</label>';
        sHotelRatings = '2';
    }
    else if (sHotelRatings == '3EST' || sHotelRatings == '561' || sHotelRatings == '3') {
        trReques3 += '<label style="float:left"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;(' + rateBySupplier + ')</label>';
        sHotelRatings = '3';
    }
    else if (sHotelRatings == '4EST' || sHotelRatings == '562' || sHotelRatings == '4') {
        trReques3 += '<label style="float:left"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;(' + rateBySupplier + ')</label>';
        sHotelRatings = '4';
    }
    else if (sHotelRatings == '5EST' || sHotelRatings == '563' || sHotelRatings == '5') {
        trReques3 += '<label style="float:left"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;(' + rateBySupplier + ')</label>';
        sHotelRatings = '5';
    }
    else {
        trReques3 += '<label style="float:left">' + sHotelRatings + '</label>'
        sHotelRatings = sHotelRatings;
        // trRequest += '<td width="10%" align="left"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;</td>';
    }
    trReques3 += '</p>';
    $(div).append(trReques3);
    document.getElementById(dropDn).value = sHotelRatings;   
    //var select = document.getElementById(dropDn);
    //select.options[sHotelRatings - 1].setAttribute('selected', 'selected');
}


function getHotelDetails(HotelCode)
{
    var data =
  {
      HotelCode: HotelCode
  }
    $.ajax({
        type: "POST",
        url: "HotelHandler.asmx/GetHotelByHotelId",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrHotelDetail = result.HotelListbyId[0];
                ContactList = result.ContactList[0];              

                $('#HotelNameTitle').val(arrHotelDetail.HotelName);
                $('#HtlName1').val(arrHotelDetail.HotelName);
                $('#HtlRatings1').val(arrHotelDetail.HotelCategory);
                sHotelRatings = arrHotelDetail.HotelCategory;
                $('#HtlDescription1').val(arrHotelDetail.HotelDescription);
                $('#HtlAddress1').val(arrHotelDetail.HotelAddress);
                $('#htlCity1').val(arrHotelDetail.CityId);
                $('#htlCountry1').val(arrHotelDetail.CountryId);
                $('#htlZipcode1').val(arrHotelDetail.HotelZipCode);
                $('#htlLangitude1').val(arrHotelDetail.HotelLangitude);
                $('#htlLatitude1').val(arrHotelDetail.HotelLatitude);
                $('#txtChildAgeFrom1').val(arrHotelDetail.ChildAgeFrom);
                $('#txtChildAgeTo1').val(arrHotelDetail.ChildAgeTo);
                $('#txtTripAdviserLink1').val(arrHotelDetail.TripAdviserLink);
                $('#txtHotelGroup1').val(arrHotelDetail.HotelGroup);
                $('#txtCheckinTime1').val(arrHotelDetail.CheckinTime);
                $('#txtCheckoutTime1').val(arrHotelDetail.CheckoutTime);
                $('#selPatesAllowed1').val(arrHotelDetail.PatesAllowed);
                $('#selLiquorPolicy1').val(arrHotelDetail.LiquorPolicy);
                $('#selSmooking1').val(arrHotelDetail.Smooking);
                nFacilities = arrHotelDetail.HotelFacilities.split(',');
                if (arrHotelDetail.SubImages  != "") {
                    var SubImg = [];
                    SubImg = arrHotelDetail.SubImages.split('^');
                    var ImgRequest = '';
                    for (var j = 0; j < SubImg.length; j++) {
                        if (SubImg[j] != 'undefined' && SubImg[j] != '') {
                            ImgRequest += '<div class="three-columns divImg1" style="min-height:220px;"><img style="width:100%;"  onclick="divSelect1(this, \'' + SubImg[j] + '\');"  src="HotelImages/' + SubImg[j] + '"><a onclick="deletePreview1(this, \'' + j + '\',\'' + SubImg[j] + '\')"  class="button"><span class="icon-trash"></span></a></div>';
                            HotelImages1[j] = SubImg[j];
                        }

                    }
                    $('#image_preview1').append(ImgRequest);

                }
                

                var arrHotelContactPerson = [];
                var arrHotelEmail = [];
                var arrHotelPhone = [];
                var arrHotelMobile = [];
                var arrHotelFax = [];
                if (ContactList != undefined)
                {
                    if (ContactList.HotelContactPerson1 = null)
                        arrHotelContactPerson = ContactList.HotelContactPerson.split(",");
                    if (ContactList.HotelEmail = null)
                        arrHotelEmail = ContactList.HotelEmail.split(",");
                    if (ContactList.HotelEmail = null)
                        arrHotelPhone = ContactList.HotelPhone.split(",");
                    if (ContactList.HotelMobile = null)
                        arrHotelMobile = ContactList.HotelMobile.split(",");
                    if (ContactList.HotelFax = null)
                        arrHotelFax = ContactList.HotelFax.split(",");                
                   
                    
                    for (var i = 0; i < arrHotelContactPerson.length; i++) {
                        if (i == 0) {
                            $("#txtContactPersonU0").val(arrHotelContactPerson[0])
                            $("#txtEmailU0").val(arrHotelEmail[0])
                            $("#txtContactPhoneU0").val(arrHotelPhone[0])
                            $("#txtMobileNoU0").val(arrHotelMobile[0])
                            $("#txtFaxNoU0").val(arrHotelFax[0])
                        }
                        else {
                            AddHotelContacts1();
                            $("#txtContactPersonU" + i).val(arrHotelContactPerson[i])
                            $("#txtEmailU" + i).val(arrHotelEmail[i])
                            $("#txtContactPhoneU" + i).val(arrHotelPhone[i])
                            $("#txtMobileNoU" + i).val(arrHotelMobile[i])
                            $("#txtFaxNoU" + i).val(arrHotelFax[i])
                        }
                    }
                    var arrReservationPerson = [];
                    var arrReservationEmail = [];
                    var arrReservationPhone = [];
                    var arrReservationMobile = [];
                    var arrReservationFax = [];
                    if (ContactList.ReservationContactPerson = null)
                        arrReservationPerson = ContactList.ReservationContactPerson.split(",");
                    if (ContactList.ReservationEmail = null)
                        arrReservationEmail = ContactList.ReservationEmail.split(",");
                    if (ContactList.ReservationPhone = null)
                        arrReservationPhone = ContactList.ReservationPhone.split(",");
                    if (ContactList.ReservationMobile = null)
                        arrReservationMobile = ContactList.ReservationMobile.split(",");
                    if (ContactList.ReservationFax = null)
                        arrReservationFax = ContactList.ReservationFax.split(",");               
                    
                    for (var i = 0; i < arrReservationPerson.length; i++) {
                        if (i == 0) {
                            $("#txtReservationPersonU0").val(arrReservationPerson[0])
                            $("#txtReservationEmailU0").val(arrReservationEmail[0])
                            $("#txtReservationPhoneU0").val(arrReservationPhone[0])
                            $("#txtReservationMobNoU0").val(arrReservationMobile[0])
                            $("#txtReservationFaxNoU0").val(arrReservationFax[0])
                        }
                        else {
                            AddReservationContacts1();
                            $("#txtReservationPersonU" + i).val(arrReservationPerson[i])
                            $("#txtReservationEmailU" + i).val(arrReservationEmail[i])
                            $("#txtReservationPhoneU" + i).val(arrReservationPhone[i])
                            $("#txtReservationMobNoU" + i).val(arrReservationMobile[i])
                            $("#txtReservationFaxNoU" + i).val(arrReservationFax[i])
                        }
                    }
                    var arrAccountsPerson = [];
                    var arrAccountsEmail = [];
                    var arrAccountsPhone = [];
                    var arrAccountsMobile = [];
                    var arrAccountsFax = [];
                    if (ContactList.AccountsContactPerson = null)
                        arrAccountsPerson = ContactList.AccountsContactPerson.split(",");
                    if (ContactList.AccountsEmail = null)
                        arrAccountsEmail = ContactList.AccountsEmail.split(",");
                    if (ContactList.AccountsPhone = null)
                        arrAccountsPhone = ContactList.AccountsPhone.split(",");
                    if (ContactList.AccountsMobile = null)
                        arrAccountsMobile = ContactList.AccountsMobile.split(",");
                    if (ContactList.AccountsFax = null)
                        arrAccountsFax = ContactList.AccountsFax.split(",");
                    
                    for (var i = 0; i < arrAccountsPerson.length; i++) {
                        if (i == 0) {
                            $("#txtAccountsPersonU0").val(arrAccountsPerson[0])
                            $("#txtAccountsEmailU0").val(arrAccountsEmail[0])
                            $("#txtAccountsPhoneU0").val(arrAccountsPhone[0])
                            $("#txtAccountsMobNoU0").val(arrAccountsMobile[0])
                            $("#txtAccountsFaxNoU0").val(arrAccountsFax[0])
                        }
                        else {
                            AddAccountsContacts1();
                            $("#txtAccountsPersonU" + i).val(arrAccountsPerson[i])
                            $("#txtAccountsEmailU" + i).val(arrAccountsEmail[i])
                            $("#txtAccountsPhoneU" + i).val(arrAccountsPhone[i])
                            $("#txtAccountsMobNoU" + i).val(arrAccountsMobile[i])
                            $("#txtAccountsFaxNoU" + i).val(arrAccountsFax[i])
                        }
                    }
                }
                
               

                
                sid = arrHotelDetail.sid;
                HotelBedsCode = arrHotelDetail.HotelBedsCode;
                DotwCode = arrHotelDetail.DotwCode;
                MGHCode = arrHotelDetail.MGHCode;
                ExpediaCode = arrHotelDetail.ExpediaCode;
                GRNCode = arrHotelDetail.GRNCode;
                var rateBySupplier = "ClickUrTrip"
                HotelImageAdd = 'HotelImages/' + arrHotelDetail.HotelImage;
                $('.selMainImage-Add').attr('src', HotelImageAdd);
                fnStarRatings(arrHotelDetail.HotelCategory, rateBySupplier, divUpdate, dropUpdate);
                $('.HotelFacilities').val(nFacilities).checked;

            }
        },
        
    });
}



function getFacilities() {
    $.ajax({
        type: "POST",
        url: "HotelHandler.asmx/GetFacilities",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrFacilitiesList = result.HotelFacilities;
                if (arrFacilitiesList.length > 0) {
                    $("#divUpdateFacilities").empty();
                    var chkRequest = '<div class="columns">';
                    var j = 0;
                    for (i = 0; i < arrFacilitiesList.length; i++) {
                        chkRequest += '<div class="four-columns" align="left"><span>'
                        chkRequest += '<input type="checkbox" name="chkFacilities" id="hFacility' + i + '" class="HotelFacilities" value="' + arrFacilitiesList[i].HotelFacilityID + '" title="' + arrFacilitiesList[i].HotelFacilityName + '"><label for="hFacility' + i + '">' + arrFacilitiesList[i].HotelFacilityName + '</label></span>';
                        chkRequest += '</span></div>'
                    }
                    $("#divUpdateFacilities").append(chkRequest);
                    $.each(nFacilities, function (index, item) {
                        $('.HotelFacilities').each(function () {
                            if (this.value === item) {
                                this.checked = true;
                            }
                        });
                    });
                }
            }
        },
        error: function () {
        }
    });

}

var arrFacilitiesList = [];

function getAddFacilities(sHotelFacilitiesText) {
    $.ajax({
        type: "POST",
        url: "HotelHandler.asmx/GetFacilities",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrFacilitiesList = result.HotelFacilities;
                if (arrFacilitiesList.length > 0) {
                    $("#divAddFacilities").empty();
                    var chkRequest = '<div class="columns">';
                    var j = 0;
                    for (i = 0; i < arrFacilitiesList.length; i++) {
                        chkRequest += '<div class="four-columns" align="left"><span>'
                        chkRequest += '<input type="checkbox" name="chkFacilities" id="hFacility' + i + '" class="HotelFacilities" value="' + arrFacilitiesList[i].HotelFacilityID + '" title="' + arrFacilitiesList[i].HotelFacilityName + '"><label for="hFacility' + i + '">' + arrFacilitiesList[i].HotelFacilityName + '</label></span>';
                        chkRequest += '</span></div>'
                    }
                    $("#divAddFacilities").append(chkRequest);
                    var facil = sHotelFacilitiesText.split(',');
                    for (var j = 0; j < facil.length; j++)
                    {
                        $.each(arrFacilitiesList, function (index, item) {
                            $('.HotelFacilities').each(function () {
                                if (this.title === facil[j]) {
                                    this.checked = true;
                                }
                            });
                        });
                    }
                   
                }
            }
        },
        error: function () {
        }
    });

}

var arrLocationList = [];
function GetLocation() {
    var Div = '';
    $.ajax({
        url: "LocationHandler.asmx/GetLocation",
        type: "post",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                arrLocationList = result.LocationList;
                for (var i = 0; i < arrLocationList.length; i++)
                {
                    Div += '<option value="' + arrLocationList[i].LocationName + '" data-title="' + arrLocationList[i].Lid + '"></option>'
                   
                }
                $("#listLocation").append(Div);
                $("#listLocation1").append(Div);
            }
        }
    })
}

function ConfirmLocation()
{
    var Location = "";
    if ($("#txtLocation").val() != "") {
        Location = $("#txtLocation").val();
    }
    else if ($("#txtLocation1").val() != "") {
        Location = $("#txtLocation1").val();
    }
    var i = 0;
    while (i<arrLocationList.length)
    {
        if (arrLocationList[i].LocationName === Location){return false;}       
        i++;
    }
    var newLocation = confirm("New Location Found. Do you want to Save?");
    if (newLocation == true) {
        SaveLocation();
    } else {
        return false;
    }
}

function SaveLocation() {
    var Location = "";
    if ($("#txtLocation").val() != "")
    {
        Location = $("#txtLocation").val();
    }
    else if ($("#txtLocation1").val() != "")
    {
        Location = $("#txtLocation1").val();
    }
    var City = "";
    if ($("#htlCity").val() != "") {
        City = $("#htlCity").val();
    }
    else if ($("#htlCity1").val() != "") {
        City = $("#htlCity1").val();
    }
    var Country = "";
    if ($("#htlCountry").val() != "") {
        Country = $("#htlCountry").val();
    }
    else if ($("#htlCountry1").val() != "") {
        Country = $("#htlCountry1").val();
    }

    var Data = {
        Location: Location,
        Country: Country,
        City: City
    }
    $.ajax({
        type: "POST",
        url: "LocationHandler.asmx/SaveLocation",
        data: JSON.stringify(Data),
        async: false,
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Location Saved succesfully");
            }
            else {
                Success("Something Going Wrong");
            }
        },
    });
}



var divHotelContacts;
var arrHotelContacts = '';

var HotelContactDetails = [];

//div Add Contacts
var clicksC = 0;
var HotelPhone = '';
var HotelMobile = '';
var HotelFax = '';
var HotelPerson = '';
var HotelEmail = '';


function AddHotelContacts()
{
    HotelPhone = '';
    HotelMobile = '';
    HotelFax = '';
    HotelPerson = '';
    HotelEmail = '';

    var divRequest = '';
    

    clicksC += 1;
    var idHotelContacts = clicksC
    for (var i = 0; i < clicksC; i++) {
        if (($('#txtContactPerson' + i).val() == "") || ($('#txtEmail' + i).val() == "")) {
            Success('Please Fill Mandatory Fields')
            clicksC = clicksC - 1;
            return false;
        }
        if (($('#txtContactPhone' + i).val() == "") && ($('#txtMobileNo' + i).val() == "")){
            Success('Please Enter (Mobile No/Phone Number)')
            clicksC = clicksC - 1;
            return false;
        }       
    }   
    document.getElementById("clickC").innerHTML = clicksC+1;

    divRequest += ' <div class="columns" id="divContactsHotel'+idHotelContacts+'"> <div class="new-row three-columns"  >&nbsp;&nbsp;</div>';
    divRequest += ' <div class="five-columns"><input type="text"  id="txtContactPerson' + clicksC + '" size="9" class="input full-width HotelPerson" value="" placeholder="Contact Person Name*"></div>';
    divRequest += ' <div class="four-columns"><input type="text" id="txtEmail' + clicksC + '" class="input full-width HotelEmail" value="" placeholder="Email Id*"></div>';
    divRequest += ' <div class="three-columns"><input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" id="txtContactPhone' + clicksC + '"  size="9" class="input full-width HotelPhone" value="" placeholder="Phone Number*"></div>';
    divRequest += ' <div class="three-columns"><input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" id="txtMobileNo' + clicksC + '" size="9" class="input full-width HotelMobile" value="" placeholder="Mobile Number*"></div>';
    divRequest += ' <div class="three-columns"><input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" id="txtFaxNo' + clicksC + '" size="9" class="input full-width HotelFax" value="" placeholder="Fax Number"></div>';
    divRequest += ' <div class="one-columns button icon-minus-round red"  style="float:right;"> <input id="' + idHotelContacts + '" class="HotelContacts" type="button" value="Less" onclick="LessHotelContacts(id);" style="background-color: transparent; border: none;" /></div></div>';

    $('#idHotelContacts').append(divRequest);
    
   
}

function LessHotelContacts(valRemove)
{
    if (clicksC > 0)
    {
        clicksC -= 1;    
        document.getElementById('divContactsHotel' + valRemove).remove()
        document.getElementById("clickC").innerHTML = clicksC+1;
    }    
}


//div Add Reservation Contact
function CheckReservation()
{
    debugger
    var chkResrv = $('#chkSameReservation').get(0);
    var HContactCount = clicksC;
    if(chkResrv.checked==true)
    {
        for (var i = 0; i <= HContactCount; i++)
        {
            if (clicksR != HContactCount) {
                AddReservationContacts();
            }
            if (clicksR == HContactCount) {             

                $('#txtReservationPhone' + i).val($('#txtContactPhone' + i).val());
                $('#txtReservationMobNo' + i).val($('#txtMobileNo' + i).val());
                $('#txtReservationFaxNo' + i).val($('#txtFaxNo' + i).val());
                $('#txtReservationPerson' + i).val($('#txtContactPerson' + i).val());
                $('#txtReservationEmail' + i).val($('#txtEmail' + i).val());
            }
        }
       
       
        
    }
    else
    {
        for (var i = 0; i <= HContactCount; i++) {
            $('#txtReservationPhone' + i).val('');
            $('#txtReservationMobNo' + i).val('');
            $('#txtReservationFaxNo' + i).val('');
            $('#txtReservationPerson' + i).val('');
            $('#txtReservationEmail' + i).val('');
        }
    }
}


var HotelReservationDetails = [];
var clicksR = 0;
var ReservationPhone = '';
var ReservationMobile = '';
var ReservationFax = '';
var ReservationPerson = '';
var ReservationEmail = '';
function AddReservationContacts() {
    ReservationPhone = '';
    ReservationMobile = '';
    ReservationFax = '';
    ReservationPerson = '';
    ReservationEmail = '';

    var divRequest = '';    
    clicksR += 1;
    var idReservationContacts = clicksR
  
    var chkResrv = $('#chkSameReservation').get(0);
    if (chkResrv.checked == false)
    {
        for (var i = 0; i < clicksR; i++) {
            if (($('#txtReservationPerson' + i).val() == "") || ($('#txtReservationEmail' + i).val() == "")) {
                Success('Please Fill Mandatory Fields')
                clicksR = clicksR - 1;
                return false;
            }
            if (($('#txtReservationPhone' + i).val() == "") && ($('#txtReservationMobNo' + i).val() == "")) {
                Success('Please Enter (Mobile No/Phone Number)')
                clicksR = clicksR - 1;
                return false;
            }
        }

    }
    document.getElementById("clickR").innerHTML = clicksR+1;

    divRequest += ' <div class="columns" id="divReservationContacts' + idReservationContacts + '"><div class="new-row three-columns">&nbsp;&nbsp;</div>';
    divRequest += ' <div class="five-columns"><input type="text" id="txtReservationPerson' + clicksR + '" size="9" class="input full-width ReservationPerson" value="" placeholder="Contact Person Name*"></div>';
    divRequest += ' <div class="four-columns"><input type="text" id="txtReservationEmail' + clicksR + '"  class="input full-width ReservationEmail" value="" placeholder="Email Id*"></div></br>';
    divRequest += ' <div class="three-columns"><input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" id="txtReservationPhone' + clicksR + '"  size="9" class="input full-width ReservationPhone" value="" placeholder="Phone Number*"></div>';
    divRequest += ' <div class="three-columns"><input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" id="txtReservationMobNo' + clicksR + '" size="9" class="input full-width ReservationMobile" value="" placeholder="Mobile Number*"></div>';
    divRequest += ' <div class="three-columns"><input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" id="txtReservationFaxNo' + clicksR + '" size="9" class="input full-width ReservationFax" value="" placeholder="Fax Number"></div>';
    divRequest += ' <div class="one-columns button icon-minus-round red"  style="float:right;"> <input id="' + idReservationContacts + '" class="ReservationContacts" type="button" value="Less" onclick="LessReservationContacts(id);" style="background-color: transparent; border: none;" /></div></div>';

    $('#idReservationContacts').append(divRequest);
   
}

function LessReservationContacts(valRemove) {
    if (clicksR > 0) {
        clicksR -= 1;        
        document.getElementById('divReservationContacts' + valRemove).remove()
        document.getElementById("clickR").innerHTML = clicksR + 1;
    }
}

// Add Accounts Contact
function CheckAccounts() {
    debugger
    var chkAccounts = $('#chkSameAccounts').get(0);
    var HContactCount = clicksR;
    if (chkAccounts.checked == true) {
        for (var i = 0; i <= HContactCount; i++) {
            if (clickA != HContactCount) {
                AddAccountsContacts();
            }
            if (clickA == HContactCount) {
                $('#txtAccountsPhone' + i).val($('#txtReservationPhone' + i).val());
                $('#txtAccountsMobNo' + i).val($('#txtReservationMobNo' + i).val());
                $('#txtAccountsFaxNo' + i).val($('#txtReservationFaxNo' + i).val());
                $('#txtAccountsPerson' + i).val($('#txtReservationPerson' + i).val());
                $('#txtAccountsEmail' + i).val($('#txtReservationEmail' + i).val());
            }
        }



    }
    else {
        for (var i = 0; i <= HContactCount; i++) {
            $('#txtAccountsPhone' + i).val('');
            $('#txtAccountsMobNo' + i).val('');
            $('#txtAccountsFaxNo' + i).val('');
            $('#txtAccountsPerson' + i).val('');
            $('#txtAccountsEmail' + i).val('');
        }
    }
   
}


var HotelAccountsDetails = [];
var clickA = 0;
var AccountsPhone = '';
var AccountsMobile = '';
var AccountsFax = '';
var AccountsPerson = '';
var AccountsEmail = '';
function AddAccountsContacts() {
    debugger
    AccountsPhone = '';
    AccountsMobile = '';
    AccountsFax = '';
    AccountsPerson = '';
    AccountsEmail = '';

    var divRequest = '';
    clickA += 1;
    var idAccountsContacts = clickA
    

    var chkAccounts = $('#chkSameAccounts').get(0);
    if (chkAccounts.checked == false) {
        for (var i = 0; i < clickA; i++) {
            if (($('#txtAccountsPerson' + i).val() == "") || ($('#txtAccountsEmail' + i).val() == "")) {
                Success('Please Fill Mandatory Fields')
                clickA = clickA - 1;
                return false;
            }
            if (($('#txtAccountsPhone' + i).val() == "") && ($('#txtAccountsMobNo' + i).val() == "")) {
                Success('Please Enter (Mobile No/Phone Number)')
                clickA = clickA - 1;
                return false;
            }
        }

    }
    document.getElementById("clickA").innerHTML = clickA+1;
    divRequest += ' <div class="columns" id="divAccountsContacts' + idAccountsContacts + '"><div class="new-row three-columns">&nbsp;&nbsp;</div>';
    divRequest += ' <div class="five-columns"><input type="text" id="txtAccountsPerson' + clickA + '" size="9" class="input full-width AccountsPerson" value="" placeholder="Contact Person Name*"></div>';
    divRequest += ' <div class="four-columns"><input type="text" id="txtAccountsEmail' + clickA + '" class="input full-width AccountsEmail" value="" placeholder="Email Id*"></div></br>';
    divRequest += ' <div class="three-columns"><input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" id="txtAccountsPhone' + clickA + '" size="9" class="input full-width AccountsPhone" value="" placeholder="Phone Number*"></div>';
    divRequest += ' <div class="three-columns"><input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" id="txtAccountsMobNo' + clickA + '" size="9" class="input full-width AccountsMobile" value="" placeholder="Mobile Number*"></div>';
    divRequest += ' <div class="three-columns"><input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57"  id="txtAccountsFaxNo' + clickA + '" size="9" class="input full-width AccountsFax" value="" placeholder="Fax Number"></div>';
    divRequest += ' <div class="one-columns button icon-minus-round red"  style="float:right;"> <input id="' + idAccountsContacts + '" class="AccountsContacts" type="button" value="Less" onclick="LessAccountsContacts(id);" style="background-color: transparent; border: none;" /></div></div>';

    $('#idAccountsContacts').append(divRequest);
}

function LessAccountsContacts(valRemove) {
    if (clickA > 0) {
        clickA -= 1;        
        document.getElementById('divAccountsContacts' + valRemove).remove()
        document.getElementById("clickA").innerHTML = clickA+1;
    }
}





//Update Hotel Contacts
var HotelContactDetails1 = [];
var clickC1 = 0;
var HotelPhone1 = '';
var HotelMobile1 = '';
var HotelFax1 = '';
var HotelPerson1 = '';
var HotelEmail1 = '';


function AddHotelContacts1() {
    HotelPhone1 = '';
    HotelMobile1 = '';
    HotelFax1 = '';
    HotelPerson1 = '';
    HotelEmail1 = '';

    var divRequest = '';

    clickC1 += 1;
    var idHotelContacts1 = clickC1
    for (var i = 0; i < clickC1; i++) {
        if (($('#txtContactPersonU' + i).val() == "") || ($('#txtEmailU' + i).val() == "")) {
            Success('Please Fill Mandatory Fields')
            clickC1 = clickC1 - 1;
            return false;
        }
        if (($('#txtContactPhoneU' + i).val() == "") && ($('#txtMobileNoU' + i).val() == "")) {
            Success('Please Enter (Mobile No/Phone Number)')
            clickC1 = clickC1 - 1;
            return false;
        }
    }

    document.getElementById("clickC1").innerHTML = clickC1+1;
    divRequest += ' <div class="columns" id="divContactsHotel1' + idHotelContacts1 + '"> <div class="new-row three-columns"  >&nbsp;&nbsp;</div>';
    divRequest += ' <div class="five-columns"><input type="text" id="txtContactPersonU' + clickC1 + '" size="9" class="input full-width HotelPerson1" value="" placeholder="Contact Person Name"></div>';
    divRequest += ' <div class="four-columns"><input type="text" id="txtEmailU' + clickC1 + '" class="input full-width HotelEmail1" value="" placeholder="Email Id"></div>';
    divRequest += ' <div class="three-columns"><input type="text" id="txtContactPhoneU' + clickC1 + '" onkeypress="return event.charCode >= 48 && event.charCode <= 57"  size="9" class="input full-width HotelPhone1" value="" placeholder="Phone Number"></div>';
    divRequest += ' <div class="three-columns"><input type="text" id="txtMobileNoU' + clickC1 + '" onkeypress="return event.charCode >= 48 && event.charCode <= 57" size="9" class="input full-width HotelMobile1" value="" placeholder="Mobile Number"></div>';
    divRequest += ' <div class="three-columns"><input type="text" id="txtFaxNoU' + clickC1 + '" onkeypress="return event.charCode >= 48 && event.charCode <= 57" size="9" class="input full-width HotelFax1" value="" placeholder="Fax Number"></div>';
    divRequest += ' <div class="one-columns button icon-minus-round red"  style="float:right;"> <input id="' + idHotelContacts1 + '" class="HotelContacts1" type="button" value="Less" onclick="LessHotelContacts1(id);" style="background-color: transparent; border: none;" /></div></div>';

    $('#idHotelContacts1').append(divRequest);


}

function LessHotelContacts1(valRemove) {
    if (clickC1 > 0) {
        clickC1 -= 1;
        document.getElementById('divContactsHotel1' + valRemove).remove()
        document.getElementById("clickC1").innerHTML = clickC1+1;
    }
}



var HotelReservationDetails1 = [];
var clickR1 = 0;
var ReservationPhone1 = '';
var ReservationMobile1 = '';
var ReservationFax1 = '';
var ReservationPerson1 = '';
var ReservationEmail1 = '';

function CheckReservationU() {
    debugger
    var chkResrv = $('#chkSameReservationU').get(0);
    var HContactCount = clickC1;
    if (chkResrv.checked == true) {
        for (var i = 0; i <= HContactCount; i++) {
            if (clickR1 != HContactCount) {
                AddReservationContacts1();
            }
            if (clickR1 == HContactCount) {
                $('#txtReservationPhoneU' + i).val($('#txtContactPhoneU' + i).val());
                $('#txtReservationMobNoU' + i).val($('#txtMobileNoU' + i).val());
                $('#txtReservationFaxNoU' + i).val($('#txtFaxNoU' + i).val());
                $('#txtReservationPersonU' + i).val($('#txtContactPersonU' + i).val());
                $('#txtReservationEmailU' + i).val($('#txtEmailU' + i).val());
            }
        }    }
    else {
        for (var i = 0; i <= HContactCount; i++) {
            $('#txtReservationPhoneU' + i).val('');
            $('#txtReservationMobNoU' + i).val('');
            $('#txtReservationFaxNoU' + i).val('');
            $('#txtReservationPersonU' + i).val('');
            $('#txtReservationEmailU' + i).val('');
        }
    }
}

function AddReservationContacts1() {
    ReservationPhone1 = '';
    ReservationMobile1 = '';
    ReservationFax1 = '';
    ReservationPerson1 = '';
    ReservationEmail1 = '';

    var divRequest = '';
    clickR1 += 1;
    var idReservationContacts1 = clickR1

    var chkResrv = $('#chkSameReservationU').get(0);
    if (chkResrv.checked == false) {
        for (var i = 0; i < clickR1; i++) {
            if (($('#txtReservationPersonU' + i).val() == "") || ($('#txtReservationEmailU' + i).val() == "")) {
                Success('Please Fill Mandatory Fields')
                clickR1 = clickR1 - 1;
                return false;
            }
            if (($('#txtReservationPhoneU' + i).val() == "") && ($('#txtReservationMobNoU' + i).val() == "")) {
                Success('Please Enter (Mobile No/Phone Number)')
                clickR1 = clickR1 - 1;
                return false;
            }
        }

    }

    document.getElementById("clickR1").innerHTML = clickR1+1;

    divRequest += ' <div class="columns" id="divReservationContacts1' + idReservationContacts1 + '"><div class="new-row three-columns">&nbsp;&nbsp;</div>';
    divRequest += ' <div class="five-columns"><input type="text" id="txtReservationPersonU' + clickR1 + '" size="9" class="input full-width ReservationPerson1" value="" placeholder="Contact Person Name"></div>';
    divRequest += ' <div class="four-columns"><input type="text" id="txtReservationEmailU' + clickR1 + '" class="input full-width ReservationEmail1" value="" placeholder="Email Id"></div></br>';
    divRequest += ' <div class="three-columns"><input type="text" id="txtReservationPhoneU' + clickR1 + '" onkeypress="return event.charCode >= 48 && event.charCode <= 57" size="9" class="input full-width ReservationPhone1" value="" placeholder="Phone Number"></div>';
    divRequest += ' <div class="three-columns"><input type="text" id="txtReservationMobNoU' + clickR1 + '" onkeypress="return event.charCode >= 48 && event.charCode <= 57" size="9" class="input full-width ReservationMobile1" value="" placeholder="Mobile Number"></div>';
    divRequest += ' <div class="three-columns"><input type="text" id="txtReservationFaxNoU' + clickR1 + '" onkeypress="return event.charCode >= 48 && event.charCode <= 57" size="9" class="input full-width ReservationFax1" value="" placeholder="Fax Number"></div>';
    divRequest += ' <div class="one-columns button icon-minus-round red"  style="float:right;"> <input id="' + idReservationContacts1 + '" class="ReservationContacts1" type="button" value="Less" onclick="LessReservationContacts1(id);" style="background-color: transparent; border: none;" /></div></div>';

    $('#idReservationContacts1').append(divRequest);

}

function LessReservationContacts1(valRemove) {
    if (clickR1 > 0) {
        clickR1 -= 1;
        document.getElementById('divReservationContacts1' + valRemove).remove()
        document.getElementById("clickR1").innerHTML = clickR1+1;
    }
}



var HotelAccountsDetails1 = [];
var clickA1 = 0;
var AccountsPhone1 = '';
var AccountsMobile1 = '';
var AccountsFax1 = '';
var AccountsPerson1 = '';
var AccountsEmail1 = '';

function CheckAccountsU() {
    debugger
    var chkAccounts= $('#chkSameAccountsU').get(0);
    var HContactCount = clickR1;
    if (chkAccounts.checked == true) {
        for (var i = 0; i <= HContactCount; i++) {
            if (clickA1 != HContactCount) {
                AddAccountsContacts1();
            }
            if (clickA1 == HContactCount) {
                $('#txtAccountsPhoneU' + i).val($('#txtReservationPhoneU' + i).val());
                $('#txtAccountsMobNoU' + i).val($('#txtReservationMobNoU' + i).val());
                $('#txtAccountsFaxNoU' + i).val($('#txtReservationFaxNoU' + i).val());
                $('#txtAccountsPersonU' + i).val($('#txtReservationPersonU' + i).val());
                $('#txtAccountsEmailU' + i).val($('#txtReservationEmailU' + i).val());
            }
        }
    }
    else {
        for (var i = 0; i <= HContactCount; i++) {
            $('#txtAccountsPhoneU' + i).val('');
            $('#txtAccountsMobNoU' + i).val('');
            $('#txtAccountsFaxNoU' + i).val('');
            $('#txtAccountsPersonU' + i).val('');
            $('#txtAccountsEmailU' + i).val('');
        }
    }
}

function AddAccountsContacts1() {

    AccountsPhone1 = '';
    AccountsMobile1 = '';
    AccountsFax1 = '';
    AccountsPerson1 = '';
    AccountsEmail1 = '';

    var divRequest = '';
    clickA1 += 1;
    var idAccountsContacts1 = clickA1
    var chkResrv = $('#chkSameAccountsU').get(0);

    if (chkResrv.checked == false) {
        for (var i = 0; i < clickA1; i++) {
            if (($('#txtAccountsPersonU' + i).val() == "") || ($('#txtAccountsEmailU' + i).val() == "")) {
                Success('Please Fill Mandatory Fields')
                clickA1 = clickA1 - 1;
                return false;
            }
            if (($('#txtAccountsPhoneU' + i).val() == "") && ($('#txtAccountsMobNoU' + i).val() == "")) {
                Success('Please Enter (Mobile No/Phone Number)')
                clickA1 = clickA1 - 1;
                return false;
            }
        }

    }
    document.getElementById("clickA1").innerHTML = clickA1+1;

    divRequest += ' <div class="columns" id="divAccountsContacts1' + idAccountsContacts1 + '"><div class="new-row three-columns">&nbsp;&nbsp;</div>';
    divRequest += ' <div class="five-columns"><input type="text" id="txtAccountsPersonU' + clickA1 + '" size="9" class="input full-width AccountsPerson1" placeholder="Contact Person Name"></div>';
    divRequest += ' <div class="four-columns"><input type="text" id="txtAccountsEmailU' + clickA1 + '" class="input full-width AccountsEmail1"  placeholder="Email Id"></div></br>';
    divRequest += ' <div class="three-columns"><input type="text" id="txtAccountsPhoneU' + clickA1 + '" onkeypress="return event.charCode >= 48 && event.charCode <= 57" size="9" class="input full-width AccountsPhone1"  placeholder="Phone Number"></div>';
    divRequest += ' <div class="three-columns"><input type="text" id="txtAccountsMobNoU' + clickA1 + '" onkeypress="return event.charCode >= 48 && event.charCode <= 57" size="9" class="input full-width AccountsMobile1" placeholder="Mobile Number"></div>';
    divRequest += ' <div class="three-columns"><input type="text" id="txtAccountsFaxNoU' + clickA1 + '" onkeypress="return event.charCode >= 48 && event.charCode <= 57" size="9" class="input full-width AccountsFax1"  placeholder="Fax Number"></div>';
    divRequest += ' <div class="one-columns button icon-minus-round red"  style="float:right;"> <input id="' + idAccountsContacts1 + '" class="AccountsContacts1" type="button" value="Less" onclick="LessAccountsContacts1(id);" style="background-color: transparent; border: none;" /></div></div>';

    $('#idAccountsContacts1').append(divRequest);
}

function LessAccountsContacts1(valRemove) {
    if (clickA1 > 0) {
        clickA1 -= 1;
        document.getElementById('divAccountsContacts1' + valRemove).remove()
        document.getElementById("clickA1").innerHTML = clickA1+1;
    }
}


function SaveHotelMapping() {
    AddFacilities()
    debugger
    if (HotelCode == undefined) {
        HotelCode = 0;
    }
    if (MainImage == undefined) {
        MainImage = "";
    }
    if (SubImages == undefined) {
        SubImages = "";
    }
    
    var sHotelCode = HotelCode;
    var sHotelName = $('#HtlName').val();
    var sHotelAddress = $('#HtlAddress').val();
    var sDescription = $('#HtlDescription').val();
    var sRatings = sHotelRatings;
    var sLangitude = $('#htlLangitude').val();
    var sLatitude = $('#htlLatitude').val();

    var sFacilities = facilitiesCode;

    var sPatesAllowed = $('#selPatesAllowed').val();
    var sLiquorPolicy = $('#selLiquorPolicy').val();
    var Smooking = $('#selSmooking').val();
    sAdultMinAge = $('#txtChildAgeTo').val();
    sChildAgeFrom = $('#txtChildAgeFrom').val();
    sChildAgeTo = $('#txtChildAgeTo').val();
    var sCheckinTime = $('#txtCheckinTime').val();
    var sCheckoutTime = $('#txtCheckoutTime').val();
    //
    var TotalRooms = $('#TotalRooms').val();
    var TotalFloors = $('#TotalFloors').val();
    var BuildYear = $('#BuildYear').val();
    var RenovationYear = $('#RenovationYear').val();
    var sHotelGroup = $('#txtHotelGroup').val();
    var sTripAdviserLink = $('#txtTripAdviserLink').val();
    
    var sHotelBedsCode = HotelBedsCode.split("_")[0];
    var sDotwCode = DotwCode;
    var sMGHCode = MGHCode;
    var sExpediaCode = ExpediaCode;
    var sGRNCode = GRNCode;
    var sCountryCode = $('#htlCountry').val();
    var sCityCode = $('#htlCity').val();
    var sZipCode = $('#htlZipcode').val();
    

   

    var sContactPeron = "";
    var sContactMobile = "";
    var sContactEmail = "";

   
    var sHotelCode = HotelCode;

    if (sHotelName == "") {
        Success('Please Enter Hotel Name');
        return false;
    }
    if (sHotelAddress == "") {
        Success('Please Enter Hotel Address');
        return false;
    }
    if (sRatings == "") {
        Success('Please Enter Hotel Ratings ');
        return false;
    }
    if (sAdultMinAge == "") {
        sAdultMinAge = 0;
    }
    if (sChildAgeFrom == "") {
        sChildAgeFrom = 0;
    }
    if (sChildAgeTo == "") {
        sChildAgeTo = 0;
    }

    var HotelPhonet = document.getElementsByClassName('HotelPhone');
    var HotelMobilet = document.getElementsByClassName('HotelMobile');
    var HotelFaxt = document.getElementsByClassName('HotelFax');
    var HotelPersont = document.getElementsByClassName('HotelPerson');
    var HotelEmailt = document.getElementsByClassName('HotelEmail');

    var ReservationPhonet = document.getElementsByClassName('ReservationPhone');
    var ReservationMobilet = document.getElementsByClassName('ReservationMobile');
    var ReservationFaxt = document.getElementsByClassName('ReservationFax');
    var ReservationPersont = document.getElementsByClassName('ReservationPerson');
    var ReservationEmailt = document.getElementsByClassName('ReservationEmail');

    var AccountsPhonet = document.getElementsByClassName('AccountsPhone');
    var AccountsMobilet = document.getElementsByClassName('AccountsMobile');
    var AccountsFaxt = document.getElementsByClassName('AccountsFax');
    var AccountsPersont = document.getElementsByClassName('AccountsPerson');
    var AccountsEmailt = document.getElementsByClassName('AccountsEmail');

    for (var i = 0; i < HotelPersont.length; i++) {
        // HotelContactDetails[i] = HotelPhone[i].value + ',' + HotelMobile[i].value + ',' + HotelFax[i].value + ',' + HotelPerson[i].value + ',' + HotelEmail[i].value;
        HotelPhone += HotelPhonet[i].value + ',';
        HotelMobile += HotelMobilet[i].value + ',';
        HotelFax += HotelFaxt[i].value + ',';
        HotelPerson += HotelPersont[i].value + ',';
        HotelEmail += HotelEmailt[i].value + ',';
    }
    for (var i = 0; i < ReservationPersont.length; i++) {
        //HotelReservationDetails[i] = ReservationPhone[i].value + ',' + ReservationMobile[i].value + ',' + ReservationFax[i].value + ',' + ReservationPerson[i].value + ',' + ReservationEmail[i].value;
        ReservationPhone += ReservationPhonet[i].value + ',';
        ReservationMobile += ReservationMobilet[i].value + ',';
        ReservationFax += ReservationFaxt[i].value + ',';
        ReservationPerson += ReservationPersont[i].value + ',';
        ReservationEmail += ReservationEmailt[i].value + ',';
    }

    for (var i = 0; i < AccountsPersont.length; i++) {
        // HotelAccountsDetails[i] = AccountsPhone[i].value + ',' + AccountsMobile[i].value + ',' + AccountsFax[i].value + ',' + AccountsPerson[i].value + ',' + AccountsEmail[i].value;
        AccountsPhone += AccountsPhonet[i].value + ',';
        AccountsMobile += AccountsMobilet[i].value + ',';
        AccountsFax += AccountsFaxt[i].value + ',';
        AccountsPerson += AccountsPersont[i].value + ',';
        AccountsEmail += AccountsEmailt[i].value + ',';
    }


    HotelPhone = HotelPhone.replace(/,\s*$/, "");
    HotelMobile = HotelMobile.replace(/,\s*$/, "");
    HotelFax = HotelFax.replace(/,\s*$/, "");
    HotelPerson = HotelPerson.replace(/,\s*$/, "");
    HotelEmail = HotelEmail.replace(/,\s*$/, "");

    ReservationPhone = ReservationPhone.replace(/,\s*$/, "");
    ReservationMobile = ReservationMobile.replace(/,\s*$/, "");
    ReservationFax = ReservationFax.replace(/,\s*$/, "");
    ReservationPerson = ReservationPerson.replace(/,\s*$/, "");
    ReservationEmail = ReservationEmail.replace(/,\s*$/, "");

    AccountsPhone = AccountsPhone.replace(/,\s*$/, "");
    AccountsMobile = AccountsMobile.replace(/,\s*$/, "");
    AccountsFax = AccountsFax.replace(/,\s*$/, "");
    AccountsPerson = AccountsPerson.replace(/,\s*$/, "");
    AccountsEmail = AccountsEmail.replace(/,\s*$/, "");
    
    var dataToPass = {
        sHotelCode:sHotelCode,
        sHotelName: sHotelName,
        sHotelAddress: sHotelAddress,
        sDescription: sDescription,
        sRatings: sRatings,
        sFacilities: sFacilities,
        sLangitude: sLangitude,
        sLatitude: sLatitude,
        //sMainImage: sMainImage,
        //sSubImages: sSubImages,
        sHotelBedsCode: sHotelBedsCode,
        sDotwCode: sDotwCode,
        sMGHCode: sMGHCode,
        sGRNCode: sGRNCode,
        sExpediaCode: sExpediaCode,
        sCountryCode: sCountryCode,
        sCityCode: sCityCode,
        sZipCode: sZipCode,
        sPatesAllowed: sPatesAllowed,
        sLiquorPolicy: sLiquorPolicy,
        Smooking:Smooking,
        sTripAdviserLink: sTripAdviserLink,
        sHotelGroup: sHotelGroup,
        sAdultMinAge: sAdultMinAge,
        sChildAgeFrom: sChildAgeFrom,
        sChildAgeTo: sChildAgeTo,
        sCheckinTime: sCheckinTime,
        sCheckoutTime: sCheckoutTime,
        TotalRooms: TotalRooms,
        TotalFloors: TotalFloors,
        BuildYear: BuildYear,
        RenovationYear:RenovationYear,
        HotelPhone: HotelPhone,
        HotelMobile: HotelMobile,
        HotelFax: HotelFax,
        HotelPerson: HotelPerson,
        HotelEmail: HotelEmail,
        ReservationPhone: ReservationPhone,
        ReservationMobile: ReservationMobile,
        ReservationFax: ReservationFax,
        ReservationPerson: ReservationPerson,
        ReservationEmail: ReservationEmail,
        AccountsPhone: AccountsPhone,
        AccountsMobile: AccountsMobile,
        AccountsFax: AccountsFax,
        AccountsPerson: AccountsPerson,
        AccountsEmail: AccountsEmail
    }
    $.ajax({
        type: "POST",
        url: "HotelMappingHandler.asmx/AddHotelMapping",
        data: JSON.stringify(dataToPass),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var HotelID = result.HotelCode;
            if (result.retCode == 1) {
                if (HotelCode == 0)
                {
                    HotelCode = HotelID;
                }
                SaveSubImages(HotelCode)             
                
            }
            if (result.retCode == 0) {
                Success("Something went wrong");
            }
        },
        error: function () {
            Success("An error occured while Adding details");

        }
    });
}

function AddFacilities()
{
    var addedFacilitiesValues = "";
    var hFacilities = document.getElementsByClassName('HotelFacilities');
    var facility = document.getElementById('hFacility');
    for (var i = 0; i < hFacilities.length; i++) {
        if (hFacilities[i].checked) {
            addedFacilitiesValues = addedFacilitiesValues + ',' + hFacilities[i].value;
        }

    }
    addedFacilitiesValues = addedFacilitiesValues.replace(/^,|,$/g, '');
    facilitiesCode = addedFacilitiesValues;

 
}


function UpdateHotelMapping() {
    debugger
    AddFacilities();

    if (MainImage == undefined) {
        MainImage = "";
    }
    if (SubImages == undefined) {
        SubImages = "";
    }
    var sHotelCode = HotelCode;
    var sHotelName = $('#HtlName1').val();
    var sHotelAddress = $('#HtlAddress1').val();
    var sDescription = $('#HtlDescription1').val();
    var sRatings = sHotelRatings;
    var sFacilities = facilitiesCode;
    var sLangitude = $('#htlLangitude1').val();
    var sLatitude = $('#htlLatitude1').val();
   
    //var sSubImages = SubImages;
    var sHotelBedsCode = HotelBedsCode.split("_")[0];
    var sDotwCode = DotwCode;
    var sMGHCode = MGHCode;
    var sExpediaCode = ExpediaCode;
    var sGRNCode = GRNCode;
    var sCountryCode = $('#htlCountry1').val();
    var sCityCode = $('#htlCity1').val();
    var sZipCode = $('#htlZipcode1').val();
    var sPatesAllowed = $('#selPatesAllowed1').val();
    var sLiquorPolicy = $('#selLiquorPolicy1').val();
    var Smooking = $('#selSmooking').val();

    sAdultMinAge = $('#txtChildAgeTo1').val();
    sChildAgeFrom = $('#txtChildAgeFrom1').val();
    sChildAgeTo = $('#txtChildAgeTo1').val();
    var sCheckinTime = $('#txtCheckinTime1').val();
    var sCheckoutTime = $('#txtCheckoutTime1').val();
    //
    var TotalRooms = $('#TotalRooms1').val();
    var TotalFloors = $('#TotalFloors1').val();
    var BuildYear = $('#BuildYear1').val();
    var RenovationYear = $('#RenovationYear1').val();
    var sHotelGroup = $('#txtHotelGroup1').val();
    var sTripAdviserLink = $('#txtTripAdviserLink1').val();

    if (sHotelName == "") {
        Success('Please Enter Hotel Name');
        return false;
    }
    if (sHotelAddress == "") {
        Success('Please Enter Hotel Address');
        return false;
    }
    if (sRatings == "") {
        Success('Please Enter Hotel Ratings ');
        return false;
    }
    if (sAdultMinAge == "") {
        sAdultMinAge = 0;
    }
    if (sChildAgeFrom == "") {
        sChildAgeFrom = 0;
    }
    if (sChildAgeTo == "") {
        sChildAgeTo = 0;
    }
    var CurrentImg = $('#ImgUrl').val();

    if (CurrentImg != "")
    {
        var sMainImage = CurrentImg = $('#ImgUrl').val();
    }
    else {
        var sMainImage = MainImage;
    }
    var HotelPhonet = document.getElementsByClassName('HotelPhone1');
    var HotelMobilet = document.getElementsByClassName('HotelMobile1');
    var HotelFaxt = document.getElementsByClassName('HotelFax1');
    var HotelPersont = document.getElementsByClassName('HotelPerson1');
    var HotelEmailt = document.getElementsByClassName('HotelEmail1');

    var ReservationPhonet = document.getElementsByClassName('ReservationPhone1');
    var ReservationMobilet = document.getElementsByClassName('ReservationMobile1');
    var ReservationFaxt = document.getElementsByClassName('ReservationFax1');
    var ReservationPersont = document.getElementsByClassName('ReservationPerson1');
    var ReservationEmailt = document.getElementsByClassName('ReservationEmail1');

    var AccountsPhonet = document.getElementsByClassName('AccountsPhone1');
    var AccountsMobilet = document.getElementsByClassName('AccountsMobile1');
    var AccountsFaxt = document.getElementsByClassName('AccountsFax1');
    var AccountsPersont = document.getElementsByClassName('AccountsPerson1');
    var AccountsEmailt = document.getElementsByClassName('AccountsEmail1');

    for (var i = 0; i < HotelPersont.length; i++) {
        // HotelContactDetails[i] = HotelPhone[i].value + ',' + HotelMobile[i].value + ',' + HotelFax[i].value + ',' + HotelPerson[i].value + ',' + HotelEmail[i].value;
        HotelPhone += HotelPhonet[i].value + ',';
        HotelMobile += HotelMobilet[i].value + ',';
        HotelFax += HotelFaxt[i].value + ',';
        HotelPerson += HotelPersont[i].value + ',';
        HotelEmail += HotelEmailt[i].value + ',';
    }
    for (var i = 0; i < ReservationPersont.length; i++) {
        //HotelReservationDetails[i] = ReservationPhone[i].value + ',' + ReservationMobile[i].value + ',' + ReservationFax[i].value + ',' + ReservationPerson[i].value + ',' + ReservationEmail[i].value;
        ReservationPhone += ReservationPhonet[i].value + ',';
        ReservationMobile += ReservationMobilet[i].value + ',';
        ReservationFax += ReservationFaxt[i].value + ',';
        ReservationPerson += ReservationPersont[i].value + ',';
        ReservationEmail += ReservationEmailt[i].value + ',';
    }

    for (var i = 0; i < AccountsPersont.length; i++) {
        // HotelAccountsDetails[i] = AccountsPhone[i].value + ',' + AccountsMobile[i].value + ',' + AccountsFax[i].value + ',' + AccountsPerson[i].value + ',' + AccountsEmail[i].value;
        AccountsPhone += AccountsPhonet[i].value + ',';
        AccountsMobile += AccountsMobilet[i].value + ',';
        AccountsFax += AccountsFaxt[i].value + ',';
        AccountsPerson += AccountsPersont[i].value + ',';
        AccountsEmail += AccountsEmailt[i].value + ',';
    }


    HotelPhone = HotelPhone.replace(/,\s*$/, "");
    HotelMobile = HotelMobile.replace(/,\s*$/, "");
    HotelFax = HotelFax.replace(/,\s*$/, "");
    HotelPerson = HotelPerson.replace(/,\s*$/, "");
    HotelEmail = HotelEmail.replace(/,\s*$/, "");

    ReservationPhone = ReservationPhone.replace(/,\s*$/, "");
    ReservationMobile = ReservationMobile.replace(/,\s*$/, "");
    ReservationFax = ReservationFax.replace(/,\s*$/, "");
    ReservationPerson = ReservationPerson.replace(/,\s*$/, "");
    ReservationEmail = ReservationEmail.replace(/,\s*$/, "");

    AccountsPhone = AccountsPhone.replace(/,\s*$/, "");
    AccountsMobile = AccountsMobile.replace(/,\s*$/, "");
    AccountsFax = AccountsFax.replace(/,\s*$/, "");
    AccountsPerson = AccountsPerson.replace(/,\s*$/, "");
    AccountsEmail = AccountsEmail.replace(/,\s*$/, "");



    var dataToPass = {
        sHotelCode: sHotelCode,
        sHotelName: sHotelName,
        sHotelAddress: sHotelAddress,
        sDescription: sDescription,
        sRatings: sRatings,
        sFacilities: sFacilities,
        sLangitude: sLangitude,
        sLatitude: sLatitude,
        //sMainImage: sMainImage,
        //sSubImages: sSubImages,
        sHotelBedsCode: sHotelBedsCode,
        sDotwCode: sDotwCode,
        sMGHCode: sMGHCode,
        sGRNCode: sGRNCode,
        sExpediaCode: sExpediaCode,
        sCountryCode: sCountryCode,
        sCityCode: sCityCode,
        sZipCode: sZipCode,
        sPatesAllowed: sPatesAllowed,
        sLiquorPolicy: sLiquorPolicy,
        Smooking:Smooking,
        sTripAdviserLink: sTripAdviserLink,
        sHotelGroup: sHotelGroup,
        sAdultMinAge: sAdultMinAge,
        sChildAgeFrom: sChildAgeFrom,
        sChildAgeTo: sChildAgeTo,
        sCheckinTime: sCheckinTime,
        sCheckoutTime: sCheckoutTime,
        TotalRooms: TotalRooms,
        TotalFloors: TotalFloors,
        BuildYear: BuildYear,
        RenovationYear: RenovationYear,
        HotelPhone: HotelPhone,
        HotelMobile: HotelMobile,
        HotelFax: HotelFax,
        HotelPerson: HotelPerson,
        HotelEmail: HotelEmail,
        ReservationPhone: ReservationPhone,
        ReservationMobile: ReservationMobile,
        ReservationFax: ReservationFax,
        ReservationPerson: ReservationPerson,
        ReservationEmail: ReservationEmail,
        AccountsPhone: AccountsPhone,
        AccountsMobile: AccountsMobile,
        AccountsFax: AccountsFax,
        AccountsPerson: AccountsPerson,
        AccountsEmail: AccountsEmail
    }


    $.ajax({
        type: "POST",
        url: "HotelMappingHandler.asmx/AddHotelMapping",
        data: JSON.stringify(dataToPass),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                UpdateSubImages(sHotelCode);               
            }
            if (result.retCode == 0) {
                Success("Something went wrong");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
                
            }
        },
        error: function () {
            Success("An error occured while Adding details");

        }
    });


}

//Hotel Images
var HotelImages = []
function preview_images() {
    var fileUpload = $("#images").get(0);
    var files = fileUpload.files;
    var total_file = document.getElementById("images").files.length;
    for (var i = 0; i < total_file; i++) {
        $('#image_preview').append('<div class="three-columns divImg" ><img style="width:100%"  onclick="divSelect(this, \'' + files[i].name + '\');"  src="' + URL.createObjectURL(event.target.files[i]) + '"><a onclick="deletePreview(this, \'' + i + '\',\'' + files[i].name + '\')"  class="button"><span class="icon-trash"></span></a></div>');
        HotelImages[i] = files[i].name;
    }
   
}

deletePreview = function (ele, i, filename) {
    "use strict";
    var arrImg = [];
    try {
        $(ele).parent().remove();
        //window.filesToUpload.splice(i, 1);
        for (var i = 0; i < HotelImages.length; i++) {
            if (HotelImages[i] != filename) {
                arrImg[i] = HotelImages[i];
            }
        }
        HotelImages = arrImg;
        
    } catch (e) {
        console.log(e.message);
    }
}

divSelect = function (ele,filename) {
    "use strict";
    $('.divImg').removeClass('selectedImg');
    var arrImg = [];
    try {       
        $(ele).parent().addClass('selectedImg')
        
        arrImg[0] = filename;
        for (var i = 1; i < HotelImages.length; i++) {
            if (HotelImages[i] != filename) {
                arrImg[i] = HotelImages[i];
            }
            else
            {
                arrImg[i] = HotelImages[0];
            }
        }
        HotelImages = arrImg;

    } catch (e) {
        console.log(e.message);
    }
}


function SaveSubImages(HotelCode) {
    var SubImages = '';
    var fileUpload = $("#images").get(0);
    var files = fileUpload.files;

    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append(files[i].name, files[i]);
    }
    for (var i = 0; i < HotelImages.length; i++)
    {
        SubImages +=  HotelImages[i] + "^";
    }

    $.ajax({
        url: "HotelImageHandler.ashx?sid=" + HotelCode + "&ImagePath=" + SubImages,
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        success: function (result) {
            Success("Hotel Mapped Successfully")
            setTimeout(function () {
                window.location.href = "HotelList.aspx";
            }, 2000);
          
        },
    });
}

function ClearSubImage() {
    $("#image_preview").load(window.location.href + " #image_preview");
    HotelImages = '';
    $("#images").val('');
}


var HotelImages1 = []
function preview_images1() {
    var fileUpload = $("#images1").get(0);
    var files = fileUpload.files;
    var total_file = files.length;
    var OldLength = HotelImages1.length;
    var TotalImgs = OldLength + total_file;
    for (var i = 0; i < total_file; i++) {
        if (files[i].name != "undefined" || files[i].name != "") {           
            for (var j = (OldLength + 1) ; j <= TotalImgs; j++) {
                $('#image_preview1').append('<div class="three-columns divImg1" ><img style="width:100%"  onclick="divSelect1(this, \'' + files[i].name + '\');"  src="' + URL.createObjectURL(event.target.files[i]) + '"><a onclick="deletePreview1(this, \'' + i + '\',\'' + files[i].name + '\')"  class="button"><span class="icon-trash"></span></a></div>');
                HotelImages1[j] = files[i].name;
                i++;
            }
        }
    }
}

deletePreview1 = function (ele, i, filename) {
    "use strict";
    var arrImg = [];
    try {
        $(ele).parent().remove();
        //window.filesToUpload.splice(i, 1);
        for (var i = 0; i < HotelImages1.length; i++) {
            if (HotelImages1[i] != filename && HotelImages1[i] != undefined) {
                arrImg[i] = HotelImages1[i];
            }
        }
        
        HotelImages1 = arrImg;

    } catch (e) {
        console.log(e.message);
    }
}

divSelect1 = function (ele, filename) {
    "use strict";
    $('.divImg1').removeClass('selectedImg');
    var arrImg = [];
    try {
        $(ele).parent().addClass('selectedImg')

        arrImg[0] = filename;
        for (var i = 1; i < HotelImages1.length; i++) {
            if (HotelImages1[i] != filename) {
                arrImg[i] = HotelImages1[i];
            }
            else {
                arrImg[i] = HotelImages1[0];
            }
        }
        HotelImages1 = arrImg;

    } catch (e) {
        console.log(e.message);
    }
}


function UpdateSubImages(HotelCode) {
    var SubImages = '';
    var fileUpload = $("#images1").get(0);
    var files = fileUpload.files;
    for (var i = 0; i < HotelImages1.length; i++) {
        if (HotelImages1[i] != undefined ) {
            SubImages += HotelImages1[i] + "^";
        }
    }
    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append(files[i].name, files[i]);
    }    
   

    $.ajax({
        url: "HotelImageHandler.ashx?sid=" + HotelCode + "&ImagePath=" + SubImages,
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        success: function (result) {
            Success("Hotel Updated Successfully")
            setTimeout(function () {
                window.location.href = "HotelList.aspx";
            }, 2000);
          
        },
    });
}

function ClearSubImage1() {
    $("#image_preview1").load(window.location.href + " #image_preview1");
    HotelImages1 = [];
    $("#images1").val('');
}


//End Hotel Images


function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
