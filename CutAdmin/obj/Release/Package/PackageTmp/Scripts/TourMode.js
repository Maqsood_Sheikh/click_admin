﻿var Image; var id = 0;
function SaveMode() {

    if ($("#btn_Supplier").val() == "Update") {

        Update_Mode()
    }
    else {
        AddMode();
    }
}


$(function () {
    GetMode();
    var newId = Date.now().toString().substr(8); // or use any method that you want to achieve this string
    $("#Password").val(newId)
    //$("#AddCustomer").modal("show");
    //$("#btn_Supplier").val("Save");
})


function AddMode() {
    debugger;
    var fName = $("#Fname").val();

    Image = "Imges";

    fileUpload = $("#Imges").get(0);
    $("#hdn_SetImage").val("1");
    if (fileUpload.value == "") {
        Success("Please Select Image.");
        return false;
    }
    var files = fileUpload.files;

    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append(files[i].name, files[i]);
    }

    var param = { fName: fName }
    $.ajax({
        type: "POST",
        url: 'ModeImageUploader.ashx?id=' + id + "&fName=" + fName,
        data: data,
        contentType: false,
        processData: false,
        success: function (result) {
            if (result != "Only jpg file is allowed") {
                //$('#SpnMessege').text("Tour Type is Added");
                //$('#ModelMessege').modal('show');
                Success("Tour Type is Added");
                GetMode();
                $("#Fname").val('');
            }
            else {
                Success("Something Went Wrog");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
              
            }
        }
    });
}

var Pid;
function GetMode() {
    $("#tbl_Getmode").dataTable().fnClearTable();
    $("#tbl_Getmode").dataTable().fnDestroy();
    $.ajax({
        url: "ActivityHandller.asmx/GetMode",
        type: "post",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                var arr_MatchSummary = result.dtresult;

                for (var i = 0; i < arr_MatchSummary.length; i++) {
                    var html = '';
                    var sid = arr_MatchSummary[i].Sid;
                    html += '<tr><td>' + (i + 1) + '</td>'
                    html += '<td align="center">' + arr_MatchSummary[i].TourType + ' </td>'

                    if (arr_MatchSummary[i].Status == "False")
                        html += '<td align="center"><i style="Cursor:Pointer" onclick="GetModeActiveDeactive(\'' + sid + '\',\'True\')"  aria-hidden="true" title="Deactivate"><label for="pseudo-input-2" class="button blue-gradient" ><span class="icon-cross"></span></label></i></td>'
                    else
                        html += '<td align="center"><i style="Cursor:Pointer" onclick="GetModeActiveDeactive(\'' + sid + '\',\'False\')"  aria-hidden="true" title="Activate"><label for="pseudo-input-2" class="button blue-gradient" ><span class="icon-tick"></span></label></i></td>'


                    html += '<td align="center"><a style="cursor:pointer" onclick="GetModetoupdate(\'' + sid + '\',\'' + arr_MatchSummary[i].TourType + '\')" aria-hidden="true" title="Update Details"><span class="icon-pencil icon-size2"  ></span></a>&nbsp |&nbsp <i style="cursor:pointer" onclick="DeleteMode(\'' + sid + '\')" aria-hidden="true" title="Delete"><span class="icon-trash" data-placement="right" ></span></i></td>'
                    html += '</tr>'
                    $("#tbl_Getmode tbody").append(html);
                }

                $('[data-toggle="tooltip"]').tooltip()

                $("#tbl_Getmode").dataTable({
                     bSort: false, sPaginationType: 'full_numbers',
                });
                $("#tbl_Getmode").css("width", "100%")
            }
        }
    })
}

function GetModetoupdate(sid, name) {
    $("#btn_Supplier").val("Update");
    $("#Fname").val(name);
    Pid = sid;
}

function Update_Mode() {

    var fName = $("#Fname").val()

    Image = "Imges";

    fileUpload = $("#Imges").get(0);
    $("#hdn_SetImage").val("1");
    if (fileUpload.value == "") {
        Success("Please Select Image.");
        return false;
    }
    var files = fileUpload.files;

    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append(files[i].name, files[i]);
    }

    var param = { fName: fName }
    $.ajax({
        type: "POST",
        url: 'ModeImageUploader.ashx?id=' + Pid + "&fName=" + fName,
        data: data,
        contentType: false,
        processData: false,
        success: function (result) {
            if (result != "Only jpg file is allowed") {
                
                //$('#SpnMessege').text("Tour Type is Updated");
                //$('#ModelMessege').modal('show');
                Success("Tour Type is Updated");
                window.location.reload();
                GetMode();
                $("#Fname").val('');
                $("#btn_Supplier").val("Save");
            }
            if (result.retCode == 0) {
                Success("Something went wrong!")
            }
        },
        error: function () {
            Success("An error occured while updating details");
        }
    });
}

function DeleteMode(sid) {

    $.ajax({
        type: "POST",
        url: "ActivityHandller.asmx/DeleteMode",
        data: '{"sid":"' + sid + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0) {
                // window.location.href = "login.aspx"; // Session end
                Success("Error in deleting ");
                return false;
            }
            if (result.retCode == 1) {
                //$("#Dialog_Deletecandidate").dialog("close");
                //$('#SpnMessege').text("Tour Type is Deleted");
                //$('#ModelMessege').modal('show');
                Success("Tour Type is Deleted");
                GetMode();
            }
        },
        error: function () {
            Success("Error in deleting ");
        }
    });
}

function GetModeActiveDeactive(sId, Status) {
    var Data = { Sid: sId, Status: Status };
    $.ajax({
        type: "POST",
        url: "ActivityHandller.asmx/GetModeActiveDeactive",
        data: JSON.stringify(Data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d)
            var SeasonList = obj.SeasonList;
            if (obj.retCode == 1) {
                if (Status == "True")
                    Success("Tour Mode Activated Successfully");
                else
                    Success("Tour Mode Deactivated Successfully");
                GetMode();
            }
            else {
                Success("Error while Season Activate");
            }
        },
    });
}