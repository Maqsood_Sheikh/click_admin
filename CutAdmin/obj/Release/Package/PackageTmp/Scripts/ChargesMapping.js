﻿var arrTax = new Array();
var arrMealAddon = new Array();
var arrChargeRate = new Array();
var arrAddOnsCharge = new Array();
var sHotelID = 0;
var AddCount = 0;
var arrChargeDetails = new Array();
$(function () {
   
    if (location.href.indexOf('?') != -1) {
        sHotelID = GetQueryStringParams("sHotelID");
    }
    GetTaxDetails();
})

function GetTaxDetails() {
    $.ajax({
        url: "../handler/TaxHandler.asmx/GetTaxMapping",
        type: "post",
        data: '{"HotelID":"' + sHotelID + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                
                arrTax = result.arrTax
                arrCharges = result.arrChargeRate;
                arrMealAddon = result.arrAddOns;
                arrAddOnsCharge = result.arrAddOnsTax;

                if (location.href.indexOf('?') != -1 && arrCharges.length ==0)
                    AddTax(0, "div_UpdateTaxDetails")
                else if (location.href.indexOf('?') == -1 )
                    AddTax(0, "div_TaxDetails")
                else
                    GenrateRates();


                /*AddOns Taxtions*/
                if (location.href.indexOf('?') != -1 && arrAddOnsCharge.length == 0)
                    AddMeal(0, "div_UpdateMealDetails")
                else if (location.href.indexOf('?') == -1)
                    AddMeal(0, "div_MealDetails")
                else
                    GenrateMealRates();
            }
            else {
                Success(result.ErrorMsg);
                setTimeout(function () {
                    window.location.href = "default.aspx", 2000
                }, 2000);
            }

        }
    })
}
function GenrateRates() {
    var RatesType = [];
    
    for (var i = 0; i < arrCharges.length; i++) {
        RatesType.push(arrCharges[i].TaxID);
    }
    var unique = RatesType.filter(function (itm, i, RatesType) {
        return i == RatesType.indexOf(itm);
    });
    for (var j = 0; j < unique.length; j++) {
        var Rates = $.grep(arrCharges, function (H) { return H.TaxID == unique[j] })
           .map(function (H) { return H.TaxOnID; });
        AddTax(j, "div_UpdateTaxDetails");
        var arrRate = [];
        arrRate.push(unique[j])
        $("#sel_Type" + j).multipleSelect("setSelects", arrRate);
        $("#sel_Type" + j).val(unique[j])
        $("#sel_TaxOn" + j).multipleSelect("setSelects", Rates);
    }
}

function GenrateTaxOn() {
    var Opt = '';
    Opt += '<option value="0">Base Rate</option>'
    for (var i = 0; i < arrTax.length; i++) {
        Opt += '<option value="' + arrTax[i].ID + '">' + arrTax[i].Name + '(' + arrTax[i].Value + '%)</option>'
    }
    return Opt;
}
function GenrateChargeType(ID) {
    var Opt = '';
    Opt += '<option value="0" selected=selected style="color:red">Non Select</option>'
    Opt += '<option value="0">Base Rate</option>'
    for (var i = 0; i < arrTax.length; i++) {
        Opt += '<option value="' + arrTax[i].ID + '">' + arrTax[i].Name + '(' + arrTax[i].Value + '%)</option>'
    }
    $('#' + ID).append(Opt)
    $('#'+ ID).change(function () {
        console.log($(this).val());
    }).multipleSelect({
        selectAll: false,
        single: true,
        multiple: false,
        width: '100%',
        delimiter: '+ ',
        filter: true,
        multiple: false, keepOpen: false,
    })
    return Opt;
}



function AddTax(ID, el) {
    try
    {
        //$("#" + el).empty();
       // for (var i = AddCount; i <= AddCount; i++) {
            var html = '';
        //ID = i;

            html += '<div class="columns TaxDetails" id="div_' + ID + '">'
            html += '<div class="five-columns twelve-columns-mobile SelBox" id="TaxType' + ID + '">'
            if (ID == 0)
                html += '<label class="input-info">Rate Type&nbsp;&nbsp;&nbsp;&nbsp;</label>';
            html += '<input type="hidden" class="TaxId' + ID + '" value="0"/>'
            html += '<select id="sel_Type' + ID + '" name="validation-select" class=" full-width sel_TaxType">'
            html += '</select>'
            html += '</div>'
            
            html += ' <div class="five-columns twelve-columns-mobile SelBox" id="TaxOn' + ID + '">'
            if (ID == 0)
                html += '<label class="input-info">Tax On</label>';
            html += '<select id="sel_TaxOn' + ID + '" name="validation-select" multiple="multiple" class="  full-width sel_TaxOn">'
            html += ' </select>'
            html += '</div>'
            html += '<div class="ten-columns twelve-columns-mobile auto-expanding" id="TaxDetails' + ID + '" style="display:none">'
            if (ID == 0)
                html += '<label class="input-info">Tax Details</label>';
            html += '<textarea  name="autoexpanding" class="input full-width autoexpanding" id="txtTaxDetails' + ID + '"></textarea>'
            html += '</div>'
            html += '<div class="two-columns Action" id="Action' + ID + '">'
            if (ID == 0)
                html += '<br/>'
            html += '<a id="lnk_' + parseInt(ID) + '" onclick="AddTax(\'' + (parseInt(ID) + 1) + '\',\'' + el + '\');" class="button">'
            html += '<span class="icon-plus-round blue"></span>'
            html += '</a>'
            html += ' <a id="lnkR_' + parseInt(ID) + '" onclick="DeleteTax(' + parseInt(ID) + ',\'' + el + '\');" class="button"><span class="icon-minus-round red"></span></a>'
            html += '<br>'
            html += '</div>'
            html += '</div>'
           // $("#lnk_" + parseInt(ID - 1) + "").remove()
            //$("div_Add_" + parseInt(ID)).append()
            $("#" + el).append(html);
            $('#sel_TaxOn' + ID).append(GenrateTaxOn())
            GenrateChargeType('sel_Type' + ID)
            $('#sel_TaxOn' + ID).change(function () {
                console.log($(this).val());
            }).multipleSelect({
                selectAll: false,
                //single: true,
                multiple: false,
                width: '100%',
                delimiter: '+ ',
                filter: true,
                multiple: false,
                minimumCountSelected: 10,
                keepOpen: false,
            });
    }
    catch(ex)
    {
        Success(ex.message)
    }
 
}

function DeleteTax(ID, el) {
    $("#div_"+ ID).remove();
    //if ((AddCount-1) == ID )
    //{
    //    var html = '';
    //    var id = parseInt(ID - 1);
    //    $("#Action" + id).empty();
    //    html += '<a id="lnk_' + parseInt(id) + '" onclick="AddTax(' + parseInt(ID) + ');" class="button">'
    //    html += '<span class="icon-plus-round blue"></span>'
    //    html += '</a>'
    //    html += ' <a id="lnkR_' + parseInt(id) + '" onclick="DeleteTax(' + parseInt(id) + ',this);" class="button"><span class="icon-minus-round red"></span></a>'
    //    html += '<br>'
    //    $("#Action" + id).append(html);
    //}
   // AddCount--;
    //GetTaxRates()

    if($(".TaxDetails").length ==1)
    {
        var ndAction = $(".TaxDetails").find(".Action");
        $(ndAction).empty();
        var html = '';
        html += '<br/><a id="lnk_' + parseInt(1) + '" onclick="AddTax(1,\'' + el + '\');" class="button">'
            html += '<span class="icon-plus-round blue"></span>'
            html += '</a>'
            $(ndAction).append(html);
    }
    else
    {
        var ndAction = $($(".TaxDetails")[$(".TaxDetails").length-1]).find(".Action");
        $(ndAction).empty();
        var html = '';
        html += '<a id="lnk_' + parseInt(1) + '" onclick="AddTax(1,\'' + el + '\');" class="button">'
        html += '<span class="icon-plus-round blue"></span>'
        html += '</a>'
        html += ' <a id="lnkR_' + parseInt($(".TaxDetails").length - 1) + '" onclick="DeleteTax(' + parseInt($(".TaxDetails").length - 1) + ',\'' + el + '\');" class="button"><span class="icon-minus-round red"></span></a>'
            html += '<br>'
        $(ndAction).append(html);
    }
}
function GetTaxRates() {
    var arrTaxDetails = $(".TaxDetails");
    var arrRatesDetails = new Array();
    for (var i = 0; i < arrTaxDetails.length; i++) {
        var TaxOn = $(arrTaxDetails[i]).find(".sel_TaxOn")[0]
        for (var j = 0; j < TaxOn.length; j++) {
            if (TaxOn[j].selected)
            {
                if ($(arrTaxDetails[i]).find(".sel_TaxType").val() != "")
                {
                    arrRatesDetails.push({
                        TaxID: $(arrTaxDetails[i]).find(".sel_TaxType").val(),
                        TaxOnID: $(TaxOn[j]).val(),
                        IsAddOns: false,
                        HotelID: sHotelID,
                        ServiceID: 0,
                        ServiceName: "",
                        ParentID:0
                    });
                }
              
            }
            
        }
    }
    return arrRatesDetails;
}


/*AddOns Mapping*/
function AddMeal(ID, el) {
    try {
        var html = '';
        html += '<div class="columns MealDetails" id="divMeal_' + ID + '">'
        html += '<div class="five-columns twelve-columns-mobile SelBox" id="MealType' + ID + '">'
        if (ID == 0)
            html += '<label class="input-info">AddOns Type</label>';
        html += '<input type="hidden" class="MealId' + ID + '" value="0"/>'
        html += '<select id="sel_MealType' + ID + '" name="validation-select" class=" full-width sel_MealType">'
        html += '</select>'
        html += '</div>'
        html += ''
        html += ' <div class="five-columns twelve-columns-mobile SelBox" id="MealTaxOn' + ID + '">'
        if (ID == 0)
            html += '<label class="input-info">Tax On&nbsp;&nbsp;&nbsp;&nbsp;</label>';
        html += '<select id="sel_MealTaxOn' + ID + '" name="validation-select" multiple="multiple" class="  full-width sel_MealTaxOn">'
        html += ' </select>'
        html += '</div>'
        html += '<div class="three-columns" id="MealDetails' + ID + '" style="display:none">'
        if (ID == 0)
            html += '<label class="input-info">Tax Details</label>';
        html += '<textarea rows="1" class="input full-width" id="txtMealDetails' + ID + '"></textarea>'
        html += '</div>'
        if (ID == 0)
            html += '<br/>'
        html += '<div class="two-columns" id="ActionMeal' + ID + '">'
        html += '<a  onclick="AddMeal(\'' + (parseInt(ID) + 1) + '\',\'' + el + '\');" class="button">'
        html += '<span class="icon-plus-round blue"></span>'
        html += '</a>'
        if(ID !=0)
            html += ' <a onclick="DeleteMeal(' + parseInt(ID) + ',\'' + el + '\');" class="button"><span class="icon-minus-round red"></span></a>'
        html += '<br/>'
        html += '</div>'
        html += '</div>'
        if (ID != 0)
        {
            $("#ActionMeal" + parseInt(ID - 1) + "").empty();
            $("#ActionMeal" + parseInt(ID - 1) + "").append('<a onclick="DeleteMeal(' + parseInt(ID - 1) + ',\'' + el + '\');" class="button"><span class="icon-minus-round red"></span></a><br>')
        }
       
        $("#" + el).append(html);
        $('#sel_MealTaxOn' + ID).append(GenrateTaxOnMeal())
        GenrateMealType('sel_MealType' + ID)
        $('#sel_MealTaxOn' + ID).change(function () {
            console.log($(this).val());
        }).multipleSelect({
            selectAll: false,
            //single: true,
            multiple: false,
            width: '100%',
            delimiter: '+ ',
            filter: true,
            multiple: false,
            minimumCountSelected: 10
        });
    }
    catch (ex) {
        Success(ex.message)
    }

}

function DeleteMeal(ID, el) {
    $("#divMeal_" + ID).remove();
    var MealDetails = $(".MealDetails");
    if (MealDetails.length != 1) {
        for (var i = 1; i < MealDetails.length; i++) {
            var html = '';
            var id = i;
            $(MealDetails[i].childNodes[4]).empty();
            if (id == (MealDetails.length-1))
            {
                html += '<a  onclick="AddMeal(' + parseInt(id + 1) + ',\'' + el + '\');" class="button">'
                html += '<span class="icon-plus-round blue"></span>'
                html += '</a>'
            }
           
            html += ' <a  onclick="DeleteMeal(' + id + ',\'' + el + '\');" class="button"><span class="icon-minus-round red"></span></a>'
            html += '<br>'
            $(MealDetails[i].childNodes[4]).append(html);
        }
        
    }
}

function GenrateMealType(ID) {
    var Opt = '';
    Opt += '<option value="0" selected=selected style="color:red">Non Select</option>'
    for (var i = 0; i < arrMealAddon.length; i++) {
        Opt += '<option value="' + arrMealAddon[i].ID + '">' + arrMealAddon[i].AddOnName + '</option>'
    }
    $('#' + ID).append(Opt)
    $('#' + ID).change(function () {
        console.log($(this).val());
    }).multipleSelect({
        selectAll: false,
        single: true,
        multiple: false,
        width: '100%',
        delimiter: '+ ',
        filter: true,
        multiple: false,
    })
    return Opt;
}

function GenrateTaxOnMeal() {
    var Opt = '';
    Opt += '<option value="0">Base Rate</option>'
    for (var i = 0; i < arrTax.length; i++) {
        Opt += '<option value="' + arrTax[i].ID + '">' + arrTax[i].Name + '(' + arrTax[i].Value + '%)</option>'
    }
    return Opt;
}

function GenrateMealRates() {
    var AddOnsType = [];

    for (var i = 0; i < arrAddOnsCharge.length; i++) {
        AddOnsType.push(arrAddOnsCharge[i].TaxID);
    }
    var unique = AddOnsType.filter(function (itm, i, AddOnsType) {
        return i == AddOnsType.indexOf(itm);
    });
    for (var j = 0; j < unique.length; j++) {
        var Rates = $.grep(arrAddOnsCharge, function (H) { return H.TaxID == unique[j] })
           .map(function (H) { return H.TaxOnID; });
        AddMeal(j, "div_UpdateMealDetails");
        var arrAddOns = [];
        arrAddOns.push(unique[j])
        $("#sel_MealType" + j).multipleSelect("setSelects", arrAddOns);
        $("#sel_MealType" + j).val(unique[j])
        $("#sel_MealTaxOn" + j).multipleSelect("setSelects", Rates);
    }
}

function GetMealRates() {
    var arrMealDetails = $(".MealDetails");
    var arrMealRates = new Array();
    for (var i = 0; i < arrMealDetails.length; i++) {
        var TaxOn = $(arrMealDetails[i]).find(".sel_MealTaxOn")[0]
        for (var j = 0; j < TaxOn.length; j++) {
            if (TaxOn[j].selected) {
                if ($(arrMealDetails[i]).find(".sel_MealType").val() != "") {
                    arrMealRates.push({
                        TaxID: $(arrMealDetails[i]).find(".sel_MealType").val(),
                        TaxOnID: $(TaxOn[j]).val(),
                        IsAddOns: true,
                        HotelID: sHotelID,
                        ServiceID: 0,
                        ServiceName: "",
                        ParentID: 0
                    });
                }

            }

        }
    }
    return arrMealRates;
}
