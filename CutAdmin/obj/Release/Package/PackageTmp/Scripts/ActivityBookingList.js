﻿$(document).ready(function () {
    //   Validate();
    BookingList()
});

function BookingList() {
    $("#tbl_BookingList").dataTable().fnClearTable();
    $("#tbl_BookingList").dataTable().fnDestroy();
    // $("#tbl_BookingList tbody tr").remove();
    $.ajax({
        type: "POST",
        url: "ActivityHandller.asmx/ActivityBookingList",
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            //   Success("Booking Success");
            //  window.location.href = "BookingList.aspx"
            var BookingList = result.BookingList;


            var trow = '';

            for (var i = 0; i < BookingList.length; i++) {
                
                trow += '<tr>';
                trow += '<td>' + (i + 1) + '</td>';
                trow += '<td>' + BookingList[i].Date + ' </td>';
                trow += '<td>' + BookingList[i].Request_Id + ' </td>';
                trow += '<td>' + BookingList[i].Passenger_Name + '</td>';
                trow += '<td>' + BookingList[i].AgencyName + '</td>';
                trow += '<td>' + BookingList[i].Act_Name + '/' + BookingList[i].City + '-' + BookingList[i].Country + '   </td>';
                trow += '<td>' + BookingList[i].BookDate + ' </td>';
                var adults = parseFloat(BookingList[i].Adults);
                var Childs1 = parseFloat(BookingList[i].Childs1);
                var Childs2 = parseFloat(BookingList[i].Childs2);
                var Total = adults + Childs1 + Childs2;
                trow += '<td>' + Total + '</td>';
                if (BookingList[i].Status != null)
                    trow += '<td>' + BookingList[i].Status + '</td>';
                else
                    trow += '<td>-</td>';
                trow += '<td>' + BookingList[i].Total.toFixed(2) + '</td>';
                trow += '</tr>';
            }

            trow += '</tbody>'

            $("#tbl_BookingList").append(trow);
            $('[data-toggle="tooltip"]').tooltip()

            $("#tbl_BookingList").dataTable({
                 bSort: false, sPaginationType: 'full_numbers',
                "width": "104%"
            });
        }

    })
}