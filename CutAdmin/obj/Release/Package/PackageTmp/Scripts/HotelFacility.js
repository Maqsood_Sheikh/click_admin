﻿var File2;
var RandomNo;

$(document).ready(function () {
    GetHotelFacility();
});

function AddFacility() {
    var bValid = true;
    var HotelFacilityName = $("#txt_facilityName").val();
    var sPhoto = File1;


    $.ajax({
        type: "POST",
        url: "FacilityHandler.asmx/AddFacility",
        data: '{"HotelFacilityName":"' + HotelFacilityName + '","sPhoto":"' + sPhoto + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Facility Added Successfully");
                $("#txt_facilityName").val("");
                $("#File_FacilityImage").val("");
                GetHotelFacility();

            }
        }
    })


}

var File1 = "";
function UploadingImage() {
    var bValid = true;
    if ($("#txt_facilityName").val() == "") {
        Success("Please Insert Proper Type");
        bValid = false;
        $("#txt_facilityName").focus()
    }
    if ($("#File_FacilityImage").val() == "") {
        Success("Please Insert Proper Photo");
        bValid = false;
        $("#File_FacilityImage").focus()
    }

    if (bValid == true) {
        debugger;
        var RandomNo = $("#txt_facilityName").val()
        var fileUpload = $("#File_FacilityImage").get(0);
        var files = fileUpload.files;
        var test = new FormData();
        for (var i = 0; i < files.length; i++) {
            test.append(files[i].name, files[i]);
        }
        $.ajax({
            url: "UploadFacilityHandler.ashx?RandomNo=" + RandomNo,
            type: "POST",
            contentType: false,
            processData: false,
            data: test,
            // dataType: "json",
            success: function (result) {
                debugger
                File1 = result.split('!')[1];
                Success(result);
                AddFacility()
            },
            error: function (err) {
                Success(err.statusText);
            }
        });
    }
}


//function SaveImage() {
//    debugger;


//    var bValid = true;
//    if ($("#txt_facilityName").val() == "") {
//        alert("Please Insert Proper Type");
//        bValid = false;
//        $("#txt_facilityName").focus()
//    }
//    if ($("#File_FacilityImage").val()==""){
//        alert("Please Insert Proper Photo");
//        bValid = false;
//        $("#File_FacilityImage").focus()
//    }

//    if (bValid == true) {
//        $.ajax({
//            type: "POST",
//            url: "ImageHandler.asmx/GetRandomNo",
//            data: {},
//            contentType: "application/json; charset=utf-8",
//            datatype: "json",
//            success: function (response) {
//                //var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//                var result = response.d;
//                RandomNo = result;
//                UploadingImage();
//            }

//        });
//    }
//}


function GetHotelFacility() {
    debugger;
    $("#tbl_HotelFacility").dataTable().fnClearTable();
    $("#tbl_HotelFacility").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "FacilityHandler.asmx/GetHotelFacility",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var List_Facility = result.Staff;

                //var List_Amunities = Amunities[0];
                var tRow = '';
                //var tRow = '<tr><td><b>Name</b></td><td style="text-align:center"><b>Email | Password Manage</b></td><td><b>Mobile</b></td><td><b>Unique Code</b></td><td align="center"><b>Edit | Status | Delete</b></td></tr>';
                for (var i = 0; i < List_Facility.length; i++) {
                    tRow += '<tr>';
                    tRow += '<td style="width:15%" class="align-center hide-on-mobile">' + (i + 1) + '</td>';
                    tRow += '<td style="width:35%"  class="align-center">' + List_Facility[i].HotelFacilityName + '</td>';
                    tRow += '<td style="width:35%" class="align-center hide-on-mobile"><img src="../../uploads/FacilityIcon/' + List_Facility[i].FacilityIcon + '" alt="" height="50px" width="50px" onclick="imgopen(\'' + List_Facility[i].FacilityIcon + '\')" style="cursor:pointer"></td>'

                    //tRow += '<td align="center"><a style="cursor:pointer" href="AddStaff.aspx?nID=' + List_StaffDetails[i].sid + '&sName=' + List_StaffDetails[i].ContactPerson + '&sDesignation=' + List_StaffDetails[i].Designation + '&sAddress=' + List_StaffDetails[i].Address + '&sCity=' + List_StaffDetails[i].Code + '&sCountry=' + List_StaffDetails[i].Country + '&nPinCode=' + List_StaffDetails[i].PinCode + '&sEmail=' + List_StaffDetails[i].email + '&nPhone=' + List_StaffDetails[i].phone + '&nMobile=' + List_StaffDetails[i].Mobile + '&bLoginFlag=' + List_StaffDetails[i].LoginFlag + '&sGroup=' + List_StaffDetails[i].StaffCategory+ '"><span class="glyphicon glyphicon-edit" title="Edit" aria-hidden="true"></span></a> | <a href="#"><span class="' + List_StaffDetails[i].LoginFlag.replace("True", "glyphicon glyphicon-volume-up").replace("False", "glyphicon glyphicon-lock") + '" onclick="Activate(\'' + List_StaffDetails[i].sid + '\',\'' + List_StaffDetails[i].LoginFlag + '\',\'' + List_StaffDetails[i].AgencyName + '\')" title="' + List_StaffDetails[i].LoginFlag.replace("True", "Deactivate").replace("False", "Activate") + '" aria-hidden="true"></span></a> | <a style="cursor:pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor:pointer" onclick="DeleteStaff(\'' + List_StaffDetails[i].uid + '\',\'' + List_StaffDetails[i].ContactPerson + '\')"></span></a></td>';
                    // tRow += '<td align="center"><a style="cursor:pointer" href="AddStaff.aspx?nID=' + List_StaffDetails[i].sid + '&sName=' + List_StaffDetails[i].ContactPerson + '&sDesignation=' + List_StaffDetails[i].Designation + '&sAddress=' + List_StaffDetails[i].Address + '&sCity=' + List_StaffDetails[i].Code + '&sCountry=' + List_StaffDetails[i].Country + '&nPinCode=' + List_StaffDetails[i].PinCode + '&sEmail=' + List_StaffDetails[i].email + '&nPhone=' + List_StaffDetails[i].phone + '&nMobile=' + List_StaffDetails[i].Mobile + '&bLoginFlag=' + List_StaffDetails[i].LoginFlag + '&sDepartment=' + List_StaffDetails[i].Department + '"><span class="glyphicon glyphicon-edit" title="Edit" aria-hidden="true"></span></a> | <a href="#"><span class="' + List_StaffDetails[i].LoginFlag.replace("True", "glyphicon glyphicon-eye-open").replace("False", "glyphicon glyphicon-eye-close") + '" onclick="Activate(\'' + List_StaffDetails[i].sid + '\',\'' + List_StaffDetails[i].LoginFlag + '\',\'' + List_StaffDetails[i].ContactPerson + '\')" title="' + List_StaffDetails[i].LoginFlag.replace("True", "Deactivate").replace("False", "Activate") + '" aria-hidden="true"></span></a> | <a style="cursor:pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor:pointer" onclick="DeleteStaff(\'' + List_StaffDetails[i].uid + '\',\'' + List_StaffDetails[i].ContactPerson + '\')"></span></a></td>';

                    tRow += '<td style="width:15%" class="align-center hide-on-mobile"><span class="button-group compact"><a onclick="GetDetails(\'' + List_Facility[i].HotelFacilityID + '\')" class="button icon-pencil"></a><a onclick="DeleteHotelFacility(\'' + List_Facility[i].HotelFacilityID + '\',\'' + List_Facility[i].HotelFacilityName + '\')" class="button icon-trash" title="Delete"></a></span></td>';
                    tRow += '</tr>';
                }
                $("#tbl_HotelFacility tbody").html(tRow);
                $("#tbl_HotelFacility").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });


            }
            if (result.retCode == 0) {
                var tRow = '';
                //var tRow = '<tr><td><b>Name</b></td><td><b>Email</b></td><td><b>Mobile</b></td><td><b>Unique Code</b></td><td align="center"><b>Edit | Status | Delete</b></td></tr>';
                tRow += "<tr><td colspan=5><span> No Record Found</span></td></tr>";
                $("#tbl_Amunities").append(tRow);

            }

        },
        error: function () {
        }

    });
}

function DeleteHotelFacility(HotelFacilityID, HotelFacilityName) {
    //if (confirm("Are you sure you want to delete " + HotelFacilityName + "?") == true) {
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to delete <span class=\"orange\">' + HotelFacilityName + '</span>?</span>?</p>',
        function () {
            $.ajax({
                url: "FacilityHandler.asmx/DeleteHotelFacility",
                type: "post",
                data: '{"HotelFacilityID":"' + HotelFacilityID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        Success("Facility Type has been deleted successfully.");
                        GetHotelFacility();
                    } else if (result.retCode == 0) {
                        Success("Something went wrong while processing your request! Please try again.");
                    }
                },
                error: function () {
                    Success('Error occured while processing your request! Please try again.');
                }
            });
        },
        function () {
            $('#modals').remove();
        }
    );
    //}
}


var icon;
var id;
function GetDetails(HotelFacilityID) {
    id = HotelFacilityID;
    icon = $("#File_FacilityImage").val();
    $.ajax({
        url: "FacilityHandler.asmx/GetDetails",
        type: "post",
        data: '{"HotelFacilityID":"' + HotelFacilityID + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                var data = result.dttable;

                $("#txt_facilityName").val(data[0].HotelFacilityName);
                icon = data[0].FacilityIcon;
                //$("#File_AmunityPhoto").val(data[0].AmunityIcon);
                $("#btn_Add").hide();
                $("#btn_Update").show();

            } else if (result.retCode == 0) {
                Success("Something went wrong while processing your request! Please try again.");
            }
        },
        error: function () {
            Success('Error occured while processing your request! Please try again.');
        }
    });
}

function UpdateHotelFacility() {
    var bValid = true;
    var HotelFacilityName = $("#txt_facilityName").val();

    var FacilityIcon = $("#File_FacilityImage").val();

    //var AmunityIcon = File1;

    if (HotelFacilityName == "") {
        Success("Please Insert Proper Type");
        bValid = false;
        $("#txt_facilityName").focus()
    }

    if (FacilityIcon == "") {
        FacilityIcon = icon;
    }
    else if (FacilityIcon != null) {

        FacilityIcon = File2;
    }
    else {
        Success("Insert Icon");
        $("#File_FacilityImage").focus()
    }


    if (bValid == true) {
        $.ajax({
            type: "POST",
            url: "FacilityHandler.asmx/UpdateHotelFacility",
            data: '{"HotelFacilityName":"' + HotelFacilityName + '","FacilityIcon":"' + FacilityIcon + '","id":"' + id + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Facility Update Successfully")
                    $("#btn_Add").show();
                    $("#btn_Update").hide();
                    $("#txt_facilityName").val("");
                    $("#File_FacilityImage").val("");
                    GetHotelFacility();
                }
            }
        })
    }
}


function UploadingFacilityImage() {
    debugger;
    var bValid = true;
    var FacilityIcon = $("#File_FacilityImage").val();
    if ($("#txt_facilityName").val() == "") {
        Success("Please Insert Propert Type");
        bValid = false;
        $("#txt_facilityName").focus()
    }
    if (FacilityIcon == "") {
        FacilityIcon = icon;
    }

    if (bValid == true) {

        var RandomNo = $("#txt_facilityName").val()
        var fileUpload = $("#File_FacilityImage").get(0);
        var files = fileUpload.files;
        var test = new FormData();
        for (var i = 0; i < files.length; i++) {
            test.append(files[i].name, files[i]);
        }
        $.ajax({
            url: "UploadFacilityHandler.ashx?RandomNo=" + RandomNo,
            type: "POST",
            contentType: false,
            processData: false,
            data: test,
            // dataType: "json",
            success: function (result) {
                debugger
                File2 = result.split('!')[1];
                Success(result);
                UpdateHotelFacility();
            },
            error: function (err) {
                Success(err.statusText);
            }
        });
    }
}