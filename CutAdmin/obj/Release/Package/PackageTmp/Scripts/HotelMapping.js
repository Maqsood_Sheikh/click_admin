﻿var HB_HotelList, DOTW_HotelList;
var selectedHID = [], selectedHotelID = [], CurrentUser;
var sHotelName, sHotelDesciption, sHotelAddress, sHotelCatagory, sBaseSupplier, sHotelId;
var Country, City, MappedHotelID;
var SuplrCode = ["DO", "HB", "MG", "GR", "EX"];

$(document).ready(function () {
    GetNationality();
    DateDisAutos();
});
var session;
var arrCountry = new Array();
var arrCity = new Array();
var arrCityCode = new Array();
function GetNationality() {
    $.ajax({
        type: "POST",
        url: "HotelHandler.asmx/GetNationalityCOR",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrNationalityCOR = result.NationalityMaster;
                if (arrNationalityCOR.length > 0) {
                    $("#Select_Nationality").empty();
                    var ddlRequest = '<option selected="selected" value="IND">India</option>';
                    // var ddlRequest = '<option selected="selected" value="IN">India</option>';
                    for (i = 0; i < arrNationalityCOR.length; i++) {
                        ddlRequest += '<option value="' + arrNationalityCOR[i].Nationality + '">' + arrNationalityCOR[i].Country + '</option>';
                    }
                    $("#Select_Nationality").append(ddlRequest);


                }
            }
        },
        error: function () {
        }
    });
}
function GetCity(reccountry) {
    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetCity",
        data: '{"country":"' + reccountry + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCity = result.City;
                if (arrCity.length > 0) {
                    $("#selCity").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any City</option>';
                    for (i = 0; i < arrCity.length; i++) {
                        ddlRequest += '<option value="' + arrCity[i].Code + '">' + arrCity[i].Description + '</option>';
                    }
                    $("#selCity").append(ddlRequest);
                }
            }
            if (result.retCode == 0) {
                $("#selCity").empty();
            }
        },
        error: function () {
        }
    });
}

function DateDisAutos() {
    $("#txtfdate").datepicker({
        dateFormat: "dd-mm-yy",
        minDate: "dateToday",
        autoclose: true,
    });
    $("#txtldate").datepicker({
        minDate: $("#txtfdate").text(),
        dateFormat: "dd-mm-yy",
        autoclose: true,
    });
}

function daysLeftInMonth(date) {
    return new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate() - date.getDate();
}


function GetHotelDetails() {
    debugger;
    $("#tbl_GetHotelMapping").dataTable().fnClearTable();
    $("#tbl_GetHotelMapping ").dataTable().fnDestroy();
    //AddHotelSearchLog();
    var destination = $('#hdnDCode').val();
    var today = new Date();
    var fdate = $("#txtfdate").val();
    var tdate = $("#txtldate").val();

    if ((fdate == "") && (tdate == ""))
    {
        if (daysLeftInMonth(new Date()) > 0) {
            fdate = today.getDate() + 1 + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
            tdate = today.getDate() + 2 + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
        }
        else if (daysLeftInMonth(new Date()) == 1) {
            fdate = today.getDate() + 1 + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
            tdate = "1" + '-' + (today.getMonth() + 2) + '-' + today.getFullYear();
        }
        else if (daysLeftInMonth(new Date()) == 0) {
            fdate = "1" + '-' + (today.getMonth() + 2) + '-' + today.getFullYear();
            tdate = "2" + '-' + (today.getMonth() + 2) + '-' + today.getFullYear();
        }
    }
    else if ((fdate == "") && (tdate != ""))
    {
        Success("Please Enter Checkin Date");
        return false;
    }
    else if ((fdate != "") && (tdate == ""))
    {
        Success("Please Enter Checkout Date");
        return false;
    }

    var hotel = $('#hdnHCode').val();
    var rating = "";
    var HName = $('#txtHotelName').val();
    var DName = $('#txtCity').val();

    City = DName.split(',')[0];
    Country = DName.split(', ')[1];

    var room = 1;
    var nationalityCode = "IND";
    if (DName == "") {
        Success('Please enter City');
        return false;
    }
    if (hotel == null) {
        hotel = "";
    }
    if (HName == null) {
        HName = "";
    }
    if (rating == null) {
        rating = "";
    }
    //var nationalityCountry = $('#Select_Nationality').text();
    if (destination == "") {
        //Success("Enter the city name where you want to go!");
        $('#SpnMessege').text("Enter the city name where you want to go!");
        $("#ModelMessege").css("display", "")
        return false;
    }
    else if (nationalityCode == "-") {
        $('#SpnMessege').text("Please select nationality!");
        $("#ModelMessege").css("display", "")

        // Success("Please select nationality!");
        return false;
    }
    else {
        var roomcount = parseInt(room);
        var occupancy = '';
        for (var i = 0; i < roomcount; i++) {
            if (i == 0) {
                occupancy = Room1();
            }
           
        }
        $("#img_file_attach").css("display", "")

        session = destination + '_' + DName + '_' + fdate + '_' + tdate + '_' + room + '_' + occupancy + '_' + hotel + '_' + HName + '_' + rating + '_' + nationalityCode;
        
        $.ajax({
            url: "HotelMappingHandler.asmx/HotelList",
            type: "post",
            data: '{"session":"' + session + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                $("#tbl_GetHotelMapping tbody").remove();
                
                if (result.retCode == 1) {
                    debugger;
                    arrHotellist = result.List_CommonHotelDetails;
                    arrUserDetails = result.objUser;
                    //Hotels List
                    if (arrHotellist == null)
                    {
                        $("#tbl_GetHotelMapping tbody").remove();
                        var trRequest = '<tbody>';
                        trRequest += '<tr><td align="center" style="padding-top: 2%" colspan="8"><span><b>No record found</b></span></td></tr>';
                        trRequest += '</tbody>';
                        $("#tbl_GetHotelMapping").append(trRequest);
                        $("#img_file_attach").css("display", "none");
                    }
                    else 
                    {
                        var trRequest = '';
                        for (i = 0; i < arrHotellist.length; i++)
                        {
                            trRequest += '<tr>';
                           
                            if (arrHotellist[i].Supplier != "ClickUrTrip")
                            {
                                trRequest += '<td width="10%">' + arrHotellist[i].Supplier + '</td>';
                            }
                            else
                            {
                                // var SuplrCode = ["DO", "HB", "MG", "GR", "EX"];
                                trRequest += '<td width="10%">'
                                if (arrHotellist[i].DotwCode != "" && arrHotellist[i].DotwCode!= null)
                                {
                                    trRequest += '<small class="tag" style="cursor:pointer" title="Dotw Code-' + arrHotellist[i].DotwCode + '">' + SuplrCode[0] + '</small>&nbsp;';
                                }
                                if (arrHotellist[i].HotelBedsCode != "" && arrHotellist[i].HotelBedsCode != null) {
                                    trRequest += '<small class="tag red-bg" style="cursor:pointer"  title="HotelBeds Code-' + arrHotellist[i].HotelBedsCode + '">' + SuplrCode[1] + '</small>&nbsp;';
                                }
                                if (arrHotellist[i].MGHCode != "" && arrHotellist[i].MGHCode != null) {
                                    trRequest += '<small class="tag green-bg" style="cursor:pointer"  title="MGH Code-' + arrHotellist[i].MGHCode + '">' + SuplrCode[2] + '</small>&nbsp;';
                                }
                                if (arrHotellist[i].GRNCode != "" && arrHotellist[i].GRNCode != null) {
                                    trRequest += '<small class="tag orange-bg" style="cursor:pointer"  title="GRN Code-' + arrHotellist[i].GRNCode + '">' + SuplrCode[3] + '</small>&nbsp;';
                                }
                                if (arrHotellist[i].ExpediaCode != "" && arrHotellist[i].ExpediaCode != null) {
                                    trRequest += '<small class="tag grey-bg" style="cursor:pointer"  title="Expedia Code-' + arrHotellist[i].ExpediaCode + '">' + SuplrCode[4] + '</small>&nbsp;';
                                }
                                if (arrHotellist[i].GTACode != "" && arrHotellist[i].GTACode != null) {
                                    trRequest += '<small class="tag grey-bg" style="cursor:pointer"  title="GTA Code-' + arrHotellist[i].GTACode + '">' + SuplrCode[5] + '</small>&nbsp;';
                                }
                                trRequest += '</td>';
                            }
                           
                            trRequest += '<td width="20%" align="left">' + arrHotellist[i].HotelName + '</td>';
                            if (arrHotellist[i].Category == 'Other' || arrHotellist[i].Category == '48055' || arrHotellist[i].Category == '0')
                            {
                                trRequest += '<td width="10%" align="left"><i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</td>';
                            }
                            else if (arrHotellist[i].Category == '1EST' || arrHotellist[i].Category == '559' || arrHotellist[i].Category == '1') {
                                trRequest += '<td width="10%" align="left"><i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</td>';
                            }
                            else if (arrHotellist[i].Category == '2EST' || arrHotellist[i].Category == '560' || arrHotellist[i].Category == '2') {
                                trRequest += '<td width="10%" align="left"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</td>';
                            }
                            else if (arrHotellist[i].Category == '3EST' || arrHotellist[i].Category == '561' || arrHotellist[i].Category == '3') {
                                trRequest += '<td width="10%" align="left"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</td>';
                            }
                            else if (arrHotellist[i].Category == '4EST' || arrHotellist[i].Category == '562' || arrHotellist[i].Category == '4') {
                                trRequest += '<td width="10%" align="left"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</td>';
                            }
                            else if (arrHotellist[i].Category == '5EST' || arrHotellist[i].Category == '563' || arrHotellist[i].Category == '5') {
                                trRequest += '<td width="10%" align="left"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;</td>';
                            }
                            else {
                                trRequest += '<td width="10%" align="left">' + arrHotellist[i].Category + '</td>'
                               // trRequest += '<td width="10%" align="left"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;</td>';
                            }
                            trRequest += '<td width="30%" align="left">' + arrHotellist[i].Address + '</td>';
                            trRequest += '<td width="10%" align="left"><span><input type="checkbox" class="hotelcode"   name="checked[]" value="' + arrHotellist[i].HotelId + '"  id="selectHotel"  onclick="ManageSelectedHotel(this.value);">';
                            trRequest += '&nbsp;&nbsp;&nbsp;<button onclick="UpdateMappedHotel(value,title);" value="' + arrHotellist[i].Supplier + ',' + arrHotellist[i].HotelName + '"  title="' + arrHotellist[i].HotelId + '"><span class="icon-pencil"></span></button></td>';
                            trRequest += '</tr>';
                           
                           
                        }
                        $("#tbl_GetHotelMapping").append(trRequest);
                        $("#tbl_GetHotelMapping").dataTable(
                      {
                          aaSorting: [[1, "asc"]],
                          "processing": true,
                          "serverSide": true,
                      });
                        $("#img_file_attach").css("display", "none");

                    }
                   
                       
                    
                  
                }
                else if (result.retCode == 0) {
                    $("#tbl_GetHotelMapping tbody").remove();
                    var trRequest = '<tbody>';
                    trRequest += '<tr><td align="center" style="padding-top: 2%" colspan="8"><span><b>No record found</b></span></td></tr>';
                    trRequest += '</tbody>';
                    $("#tbl_GetHotelMapping").append(trRequest);
                }
                
            },
            error: function (ex)
            {
                //$("#img_file_attach").css("display", "none")
                Success(ex);
            }
            
        });
        //window.location.href = "waitforresponse.aspx?session=" + session;
    }
}

function UpdateMappedHotel(NewSupplier, NewHotelID) {
    var nSupplierName = NewSupplier.split(',')[0];
    var nHotelName = NewSupplier.split(',')[1];
    var nHotelID = NewHotelID;

    
    getMappedHotels();
    $.modal({
        content:
                 '<p class="button-height inline-label "><br>' +
                           '<label for="input-4" class="label">Hotel Selected:</label>' +
                                '<input id="HotelSelected"  class="input" readonly>' +
				 '</p>' +
                  '<p class="button-height inline-label ">' +
                           '<label for="input-4" class="label">Supplier Name:</label>' +
                                '<input id="newSupplierName"   class="input" readonly>' +
				'</p>' +
                 '<p class="button-height inline-label ">' +
                           '<label for="input-4" class="label">Supplier Code:</label>' +
                                '<input id="newSupplierCode"   class="input" readonly>' +
				'</p>' +
                 '<div class="button-height">' +

                     '<p class="button-height inline-label ">' +
                           '<label for="input-4" class="label">Hotel to Update:</label>' +
                                '<select name="select90" id="hotelToUpdate" onchange="selecthotelId(value);" name="country"    class="select blue-gradient glossy mid-margin-left">' +
							        	'<option selected="selected" value="-" >Please select</option>' +
                                       
							     '</select>'+
				    '</p>' +
                  '</div><br><br>'+
                
                 '<p class="button-height inline-label ">' +
                           '<label for="input-4" class="label">&nbsp;&nbsp;&nbsp;</label>' +
                                '<button type="button" onclick="UpdateHotelSupplier();" class="button blue-gradient glossy">Add</button>' +
				 '</p>',


        title: 'Update Hotel',
        width: 400,
        height: 300,
        scrolling: true,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        buttonsLowPadding: true
    });
    $('#HotelSelected').val(nHotelName);
    $('#newSupplierName').val(nSupplierName);
    $('#newSupplierCode').val(nHotelID);
};

function getMappedHotels()
{

    $.ajax({
        type: "POST",
        url: "HotelHandler.asmx/GetMappedHotelstoUpdate",
        data: '{"Country":"' + Country + '","City":"' + City + '"}',
        //data: '{Country:Country,City:City}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrHotellist = result.HotelListbyCity;
                //Hotels List
                if (arrHotellist.length > 0) {
                    $("#hotelToUpdate").empty();
                    var ddlRequest = '<option selected="selected" >Select Hotel</option>';
                    for (i = 0; i < arrHotellist.length; i++) {
                        ddlRequest += '<option  value="' + arrHotellist[i].sid + '">' + arrHotellist[i].HotelName + '</option>';
                     }
                    $("#hotelToUpdate").append(ddlRequest);
                }
              
            }
          
        }
    });

}

function selecthotelId(obj)
{
    MappedHotelID = obj;
}

function UpdateHotelSupplier()
{
    var srno = MappedHotelID;
    var SupplierName = $('#newSupplierName').val();
    var SupplierCode = $('#newSupplierCode').val();

    if (SupplierName == 'HotelBeds')
    {
        SupplierCode = SupplierCode.split('_')[0];
    }

    var dataToPass = {
        srno: srno,
        SupplierName: SupplierName,
        SupplierCode: SupplierCode
    };

    var jsonText = JSON.stringify(dataToPass);
    $.ajax({
        type: "POST",
        url: "HotelHandler.asmx/AddSupplier",
        data: jsonText,
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {

                Success("Hotel Updated Successfully")
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
               
            }
            if (result.retCode == 0) {
                Success("Hotel Updated Successfully");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
             
            }
        },
        error: function () {
            Success("An error occured while Adding details");
        }
    });

}


function Room1() {
    var adult = 2;
    var child = 0;
    var age = 0;
    //if (parseInt(child) > 0) {
    //    age = 0;
    //    if (parseInt(child) == 2) {
    //        age = age + '^' + $('#Select_AgeChildSecond1').val();
    //    }
    //    else if (parseInt(child) == 3) {
    //        age = age + '^' + $('#Select_AgeChildSecond1').val() + '^' + $('#Select_AgeChildThird1').val();
    //    }
    //    else if (parseInt(child) == 4) {
    //        age = age + '^' + $('#Select_AgeChildSecond1').val() + '^' + $('#Select_AgeChildThird1').val() + '^' + $('#Select_AgeChildFourth1').val();
    //    }
    //}
    return adult + '|' + child + '^' + age;
}

function ManageMapping(obj) {
   
    var hname = document.getElementsByClassName('hname');
   // var hname = document.getElementsByClassName('hname');

    for (var i = 0; i < hname.length; i++) {
        hname[i].checked = false;
    }
    obj.checked = true;
}
selectedHotelID = [];
var m_CheckBoxNew = [];
function ManageSelectedHotel(HotelId) {
    m_CheckBoxNew = document.getElementsByClassName('hotelcode');
    for (var i = 0; i < m_CheckBoxNew.length; i++) {
        if (m_CheckBoxNew[i].value == HotelId) {
            selectedHotelID.push(m_CheckBoxNew[i].value);
        }
      }
   }

selectedHID = [];
function MapHotel() {
    var MergedCityCountry = $('#txtCity').val();
    var CityCountry = MergedCityCountry.split(', ');
    var City = CityCountry[0];
    var Country = CityCountry[1];
   
    m_CheckBox = document.getElementsByClassName('hotelcode');
    for (var i = 0; i < m_CheckBox.length; i++) {
        if (m_CheckBox[i].checked) {
            selectedHID.push( m_CheckBox[i].value);
        }
    }
    if (selectedHotelID.length != selectedHID.length) {
        selectedHID = selectedHotelID;
    }
    if (selectedHID.length == 0)
    {
        Success('Please Select hotels to Map');
        return false;
    }
    //CurrentUser = arrUserDetails.sid;
    debugger
    window.location.href = "HotelMapSelected.aspx?HotelCode=" + selectedHID + "&City=" + City + "&Country=" + Country +"&session="+ session;
    //HotelMappingModal()
}

// Demo complex modal
function HotelMappingModal() {
   
    $.modal({
        content:  '<table class="table responsive-table" id="tbl_GetHotel">' +
                   '<thead>'+
                   '<tr style="background-color: #494747; color: white">'+
                   '<th>'+
                    '<span class="text-left">Supplier</span>'+
                    '</th>'+
                     '<th>'+
                    '<span class="text-left">Hotel Name</span>'+
                     '</th>' +
                      '<th>' +
                     '<span class="text-left">Address</span>' +
                     '</th>' +
                    '<th>'+
                     '<span class="text-left">Description</span>'+
                      '</th>' +
                      '<th>' +
                     '<span class="text-left">Catagory</span>' +
                      '</th>' +
                    '</tr>'+
                '</thead>'+
                  ' <tbody>' +
                  '</tbody>' +
                  '</table>',
        title: 'Hotel Mapping Window',
        width: 600,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            },
            'Center': {
                color: 'green',
                click: function (win) { win.centerModal(true); }
            },
            'Other action': {
                color: 'blue',
                click: function (win) { win.closeModal(); }
            },
            'Last action': {
                color: 'orange',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Save Mapping': {
                classes: 'huge blue-gradient glossy full-width',
                click: function () { saveHotelMapping(); }
            }
        },
        buttonsLowPadding: true
    });
    if (arrHotellist.length > 0) {

        for (i = 0; i < selectedHID.length; i++) {
            for (var j = 0; j < arrHotellist.length; j++)
            {
                if (arrHotellist[j].HotelId == selectedHID[i]) {
                    trRequest += '<tr><td align="left">';
                    trRequest += '<span class="text-left">' + arrHotellist[j].Supplier + '</span>';
                    trRequest += '</td><td align="left">';
                    trRequest += '<span><input type="checkbox" class="hname" onclick="ManageMapping(this)" name="checked[]" value="' + arrHotellist[j].HotelName + '"  id="pHotelName"></span>&nbsp;&nbsp;<sapn class="label">' + arrHotellist[j].HotelName + '</span>';
                    trRequest += '</td><td align="left">';
                    trRequest += '<span><input type="checkbox" class="hAddress" name="checked[]" value="' + arrHotellist[j].Address + '"  id="pHotelAddress"></span>&nbsp;&nbsp;<a href="javascript:void(0)" onclick="getAddr(\'' + arrHotellist[j].HotelId + '\')">View</a>';
                    trRequest += '</td><td align="left">';
                    trRequest += '<span><input type="checkbox" class="hDescription" name="checked[]" value="' + arrHotellist[j].Description + '"  id="pHotelDescrip"></span>&nbsp;&nbsp;<a href="javascript:void(0)" onclick="getdesc(\'' + arrHotellist[j].HotelId + '\')">View</a>';
                    trRequest += '</td><td align="left">';
                    trRequest += '<span><input type="checkbox" class="hCatagory" name="checked[]" value="' + arrHotellist[j].Category + '"  id="pHotelCatagory"></span>&nbsp;&nbsp;<sapn class="label">' + arrHotellist[j].Category + '</span>';
                    trRequest += '</td>';
                    trRequest += '</tr>';

                }

            }
        }
        $("#tbl_GetHotel").append(trRequest);

    }
    else if (result.retCode == 0) {
        $("#tbl_GetHotel tbody").remove();
        var trRequest = '<tbody>';
        trRequest += '<tr><td align="center" style="padding-top: 2%" colspan="8"><span><b>No record found</b></span></td></tr>';
        trRequest += '</tbody>';
        $("#tbl_GetHotel").append(trRequest);
    }
    ManageMapping();
};
function getdesc(HotelId) {
    HB_Supplier = $.grep(arrHotellist, function (p) { return p.HotelId == HotelId; })
       .map(function (p) { return p.Description; });
    Success(HB_Supplier)
}
function getAddr(HotelId) {
    HB_Supplier = $.grep(arrHotellist, function (p) { return p.HotelId == HotelId; })
       .map(function (p) { return p.Address; });
    Success(HB_Supplier)
}

