﻿$(function (){
    LoadMarkUp();
});

function LoadMarkUp() {
    $.ajax({
        type: "POST",
        url: "../handler/MarkUpHandler.asmx/LoadMarkUp",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0 && result.retCode == 0) {
                Success("Your session has been expired!")
                setTimeout(function () {
                    window.location.href = "../Default.aspx";
                }, 2000);
                
            }
            else if (result.retCode == 1) {
                var AgentMarkUps = result.AgentMarkUps;
                if (AgentMarkUps.length == 0 || AgentMarkUps == null || AgentMarkUps == undefined) {
                    $('#BtnMarkUp').attr("value", "Add");
                    $('#BtnMarkUp').attr("title", "Add Markup");
                    $('#BtnMarkUp').attr("onclick", "SaveMarkUp()");
                } else {
                    $('#BtnMarkUp').attr("value", "Update");
                    $('#BtnMarkUp').attr("title", "Update Markup");
                    $('#BtnMarkUp').attr("onclick", "UpdateMarkUp()");

                    if (AgentMarkUps[0].Percentage !== undefined) {
                        $('#txtPercentage').val(AgentMarkUps[0].Percentage);
                        $('#txtAmount').val(AgentMarkUps[0].Amount);
                    } else if (AgentMarkUps[0].Percentage == undefined) {
                        $('#txtPercentage').val(AgentMarkUps[0].MarkupPercentage);
                        $('#txtAmount').val(AgentMarkUps[0].MarkupAmmount);
                    }
                    
                }
            }
        },
        error: function () {
            Success("An error occured while saving details.");
        }
    });
}

function SaveMarkUp() {
    var Perc = $('#txtPercentage').val();
    var Amt = $('#txtAmount').val();

    if (!Perc) {
        Success("Please enter Percentage.");
        return false;
    }
    if (!Amt) {
        Success("Please enter Amount.");
        return false;
    }

    var data = {
        Perc: Perc,
        Amt: Amt
    }

    $.ajax({
        type: "POST",
        url: "../handler/MarkUpHandler.asmx/SaveMarkUp",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0 && result.retCode == 0) {
                Success("Your session has been expired!")
                setTimeout(function () {
                    window.location.href = "../Default.aspx";
                }, 500);
            
            }
            else if (result.retCode == 1) {
                Success("Markup saved successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 500);
            }
        },
        error: function () {
            Success("An error occured while saving details.");
        }
    });
}

function UpdateMarkUp() {
    var Perc = $('#txtPercentage').val();
    var Amt = $('#txtAmount').val();

    if (!Perc) {
        Success("Please enter Percentage.");
        return false;
    }
    if (!Amt) {
        Success("Please enter Amount.");
        return false;
    }

    var data = {
        Perc: Perc,
        Amt: Amt
    }

    $.ajax({
        type: "POST",
        url: "../handler/MarkUpHandler.asmx/UpdateMarkUp",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0 && result.retCode == 0) {
                Success("Your session has been expired!")
                setTimeout(function () {
                    window.location.href = "../Default.aspx";
                }, 2000);
         
            }
            else if (result.retCode == 1) {
                Success("Markup updated successfully.");
             
            }
        },
        error: function () {
            Success("An error occured while saving details.");
        }
    });
}