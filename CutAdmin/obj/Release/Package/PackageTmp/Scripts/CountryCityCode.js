﻿$(document).ready(function () {
    $('#selCountry').change(function () {
        var sndcountry = $('#selCountry').val();
        GetCity(sndcountry);
    });
});
var arrCountry = new Array();
var arrCity = new Array();
var arrCityCode = new Array();
function GetCountry() {
    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetCountry",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCountry = result.Country;
                if (arrCountry.length > 0) {
                    $("#selCountry").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any Country</option>';
                    for (i = 0; i < arrCountry.length; i++) {
                        ddlRequest += '<option value="' + arrCountry[i].Country + '">' + arrCountry[i].Countryname + '</option>';
                    }
                    $("#selCountry").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}

function GetCity(reccountry) {
    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetCity",
        data: '{"country":"' + reccountry + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCity = result.City;
                if (arrCity.length > 0) {
                    $("#selCity").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any City</option>';
                    for (i = 0; i < arrCity.length; i++) {
                        ddlRequest += '<option value="' + arrCity[i].Code + '">' + arrCity[i].Description + '</option>';
                    }
                    $("#selCity").append(ddlRequest);
                }
            }
            if (result.retCode == 0) {
                $("#selCity").empty();
            }
        },
        error: function () {
        }
    });
}

function GetCityCode() {
    if ($('#selCountry').val() == '-') {
        Success('Please select Country.');
    }
    else if ($('#selCountry').val() != '-' && $('#selCity').val() == '-') {
        Success('Please select City.');
    }
    else {
        $.ajax({
            type: "POST",
            url: "GenralHandler.asmx/GetCityCode",
            data: '{"Description":"' + $('#selCity option:selected').text() + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    arrCityCode = result.CityCode;
                    if (arrCityCode.length > 0) {
                        //....................//
                        $("#tbl_GetCityCode tbody").remove();
                        var trRequest = '<tbody><tr style="border-top:hidden">';
                        trRequest += '<td><span class="text-left">Country Name</span></td>';
                        trRequest += '<td><span class="text-left">Code</span></td>';
                        trRequest += '<td><span class="text-left">City Name</span></td>';
                        trRequest += '<td><span class="text-left">City Code</span></td>';
                        trRequest += '</tr>';
                        for (i = 0; i < arrCityCode.length; i++) {
                            trRequest += '<tr><td align="left">';
                            trRequest += '<span class="text-left">' + arrCityCode[i].Countryname + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrCityCode[i].Country + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrCityCode[i].Description + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrCityCode[i].Code + '</span>';
                            trRequest += '</td></tr>';
                        }
                        trRequest += '</tbody>';
                        $("#tbl_GetCityCode").append(trRequest);
                        //.......................//
                    }
                }
            },
            error: function () {
            }
        });
    }
}