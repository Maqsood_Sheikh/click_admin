﻿$(document).ready(function () {

    GetAllQuotationDetails();


});




function GetAllQuotationDetails() {
    $("#tbl_QuotationDetails").dataTable().fnClearTable();
    $("#tbl_QuotationDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "../Handler/QuotationHandler.asmx/GetAllQuotationDetail",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            //var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                List_QuotationDetail = result.Arr;
                for (var i = 0; i < List_QuotationDetail.length; i++) {
                    tRow += '<tr>';
                    tRow += '<td>' + (i + 1) + '</td>';
                    tRow += '<td>CUT/Quot-' + List_QuotationDetail[i].Sid + '</td>';
                    tRow += '<td>' + List_QuotationDetail[i].Date + '</td>';
                    tRow += '<td>' + List_QuotationDetail[i].Companyname + '</td>';
                    tRow += '<td>' + List_QuotationDetail[i].Leadguestname + '</td>';
                    tRow += '<td>Adults- ' + List_QuotationDetail[i].Adults + ', Child-' + List_QuotationDetail[i].Child + ', Infant- ' + List_QuotationDetail[i].Infant + ' </td>';
                    tRow += '<td><button type="button" class="button glossy blue-gradient" >View</button></td>';

                    tRow += '<td data-title="Update" style="text-align:center;"><a style="cursor:pointer" href="#"><span class="icon-pencil" title="Update" style="cursor:pointer" onclick="UpdateQuotation(\'' + List_QuotationDetail[i].Sid + '\')"></span></a></td>';

                    tRow += '<td data-title="Delete" style="text-align:center;"><a style="cursor:pointer" href="#"><span class="icon-trash" title="Delete" style="cursor:pointer" onclick="DeleteQuotation(\'' + List_QuotationDetail[i].Sid + '\')"></span></a></td>';

                    tRow += '</tr>';
                }
                $("#tbl_QuotationDetails tbody").html(tRow);
                $("#tbl_QuotationDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                document.getElementById("tbl_QuotationDetails").removeAttribute("style")
            }
            else if (result.retCode == 0) {
                $("#tbl_QuotationDetails tbody").remove();
                var tRow = '<tbody>';
                tRow += '<tr> <td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td></tr>';
                tRow += '</tbody>';
                $("#tbl_QuotationDetails").append(tRow);

                $("#tbl_QuotationDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }
        },
        error: function () {
        }

    });
}

function Search() {

    $("#tbl_QuotationDetails").dataTable().fnClearTable();
    $("#tbl_QuotationDetails").dataTable().fnDestroy();


    var CompanyName = $("#txt_Name").val();
  
    var data = {
        CompanyName: CompanyName

    }

    $.ajax({
        type: "POST",
        url: "../Handler/QuotationHandler.asmx/SearchQuotationDetail",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            // var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                List_QuotationDetail = result.Arr;
                for (var i = 0; i < List_QuotationDetail.length; i++) {
                    tRow += '<tr>';
                    tRow += '<td>' + (i + 1) + '</td>';
                    tRow += '<td>CUT/Quot-' + List_QuotationDetail[i].Sid + '</td>';
                    tRow += '<td>' + List_QuotationDetail[i].Date + '</td>';
                    tRow += '<td>' + List_QuotationDetail[i].Companyname + '</td>';
                    tRow += '<td>' + List_QuotationDetail[i].Leadguestname + '</td>';
                    tRow += '<td>Adults- ' + List_QuotationDetail[i].Adults + ', Child-' + List_QuotationDetail[i].Child + ', Infant- ' + List_QuotationDetail[i].Infant + ' </td>';
                    tRow += '<td><button type="button" class="button glossy blue-gradient" >View</button></td>';

                    tRow += '<td data-title="Update" style="text-align:center;"><a style="cursor:pointer" href="#"><span class="icon-pencil" title="Update" style="cursor:pointer" onclick="UpdateQuotation(\'' + List_QuotationDetail[i].Sid + '\')"></span></a></td>';

                    tRow += '<td data-title="Delete" style="text-align:center;"><a style="cursor:pointer" href="#"><span class="icon-trash" title="Delete" style="cursor:pointer" onclick="DeleteQuotation(\'' + List_QuotationDetail[i].Sid + '\')"></span></a></td>';

                    tRow += '</tr>';
                }
                $("#tbl_QuotationDetails tbody").html(tRow);
                $("#tbl_QuotationDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                document.getElementById("tbl_QuotationDetails").removeAttribute("style")
            }
            else if (result.retCode == 0) {
                $("#tbl_QuotationDetails tbody").remove();
                var tRow = '<tbody>';
                //tRow += '<tr><td align="center" style="padding-top: 2%" colspan="4"><span><b>No record found</b></span></td></tr>';
                tRow += '<tr> <td align="center" colspan="6" style="padding-top: 2%"><span><b>No record found</b></span></td></tr>';
                tRow += '</tbody>';
                $("#tbl_QuotationDetails").append(tRow);

                //$("#tbl_AgentDetails").dataTable({
                //    "bSort": false
                //});
            }
        },
        error: function () {
            //Success("An error occured while loading details.")
        }
    });

}

function UpdateQuotation(ID) {

    window.location.href = "AddQuotation.aspx?ID=" + ID;

}


function ChangeStatusModal(Sid, Currentstatus) {

    $('#ChangeStatusModal').modal("show");
    sid = Sid;
    $("#hdModelId").val(sid);

}

//function ViewQuotation(ID) {

//    window.location.href = "ViewQuotationDetails.aspx?ID=" + ID;

//}

function DeleteQuotation(Id) {
    debugger;
    var data = { Id: Id };
    $.ajax({
        type: "POST",
        url: "../Handler/QuotationHandler.asmx/DeleteQuotationByID",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                alert("Quotation deleted successfully");
                GetAllQuotationDetails();
            }
            else {
                alert("An error occured !!!");
            }
        },
        error: function () {
            var msg = "An error occured !!!"
            alert(msg);
        }
    });
}


//function Search() {

//    $("#tbl_SalesDetails").dataTable().fnClearTable();
//    $("#tbl_SalesDetails").dataTable().fnDestroy();


//    var Name = $("#txt_Name").val();
//    var Territory = $("#selTerritory").val();
//    //var sTerCity = $("#selTeritoryCity").val();
//    //var sTerCountry = $("#selTeritoryCountry").val();

//    var data = {
//        Name: Name,
//        Territory: Territory
//        //sTerCity: sTerCity,
//        //sTerCountry: sTerCountry

//    }

//    $.ajax({
//        type: "POST",
//        url: "GenralHandler.asmx/SearchSales",
//        data: JSON.stringify(data),
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        success: function (response) {
//            // var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//            var result = JSON.parse(response.d)
//            if (result.retCode == 1) {


//                var List_QuotationDetail = result.List_Sales;

//                var input = "\/Date(1458845940000)\/";
//                for (var i = 0; i < List_QuotationDetail.length; i++) {

//                    tRow += '<tr>';

//                    tRow += '<td style="width:5%" style="text-align:center">' + (i + 1) + '</td>';

//                    tRow += '<th scope="row" style="text-align:center">CUT/Quots-' + List_QuotationDetail[i].Sid + '</th>';

//                    tRow += '<th scope="row" style="text-align:center">' + List_QuotationDetail[i].Date + '</th>';

//                    tRow += '<th scope="row" style="text-align:center">' + List_QuotationDetail[i].Companyname + '</th>';

//                    tRow += '<th scope="row" style="text-align:center">' + List_QuotationDetail[i].Currentstatus + '</th>';

//                    tRow += '<td data-title="Update"><a style="cursor:pointer" href="#"><span class="glyphicon glyphicon-new-window" title="Update" style="cursor:pointer" onclick="UpdateQuotation(\'' + List_QuotationDetail[i].Sid + '\')"></span></a></td>';

//                    tRow += '<td data-title="Delete"><a style="cursor:pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" style="cursor:pointer" onclick="DeleteQuotation(\'' + List_QuotationDetail[i].Sid + '\')"></span></a></td>';

//                    tRow += '</tr>';
//                }
//                $("#tbl_QuotationDetails tbody").html(tRow);
//                $("#tbl_QuotationDetails").dataTable({
//                    "bSort": false,
//                    "bAutoWidth": false
//                });
//                document.getElementById("tbl_QuotationDetails").removeAttribute("style")
//            }
//            else if (result.retCode == 0) {
//                $("#tbl_QuotationDetails tbody").remove();
//                var tRow = '<tbody>';
//                //tRow += '<tr><td align="center" style="padding-top: 2%" colspan="4"><span><b>No record found</b></span></td></tr>';
//                tRow += '<tr> <td align="center" colspan="6" style="padding-top: 2%"><span><b>No record found</b></span></td></tr>';
//                tRow += '</tbody>';
//                $("#tbl_QuotationDetails").append(tRow);

//                //$("#tbl_AgentDetails").dataTable({
//                //    "bSort": false
//                //});
//            }
//        },
//        error: function () {
//            //Success("An error occured while loading details.")
//        }
//    });

//}
function ChangeStatus() {

    var rowid;
    var status;
    rowid = $("#hdModelId").val();
    status = $("#selCurrentstatus").val();

    //var selCurrentstatus = $("#selCurrentstatus  option:selected").val();
    var data = {
        ID: rowid,
        Currentstatus: status,
    }
    $.ajax({
        type: "POST",
        url: "../Handler/QuotationHandler.asmx/UpdateCurrentstatusById",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {

                Success("Quotation Status Updated successfully");
                $('#ChangeStatusModal').modal('hide');
                GetAllQuotationDetails();
            }
            else {
                alert("Something Went Wrong");
                window.location.reload();
            }
        }
    });
}

