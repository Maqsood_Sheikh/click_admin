﻿function Redirect(Page) {
    if (id == undefined) {
        Success("Please Add Basic Details First!!")
        return false;
    }

    else {
        if (Page == 'Rate') {
            window.location.href = "AddActivityTariff.aspx?id=" + id;
        }
        else if (Page == 'Image') {
            window.location.href = "Image.aspx?id=" + id;
        }
        else {
            window.location.href = "AddActivity.aspx?id=" + id
        }
    }
}

$(function () {

    $('#chk_SICAll').change(function () {
        if ($("#chk_SICAll")[0].checked) {
            $(".chk_SIC").prop('checked', true);
        }
        else
            $(".chk_SIC").prop('checked', false);
    })
    $('#chk_TKTAll').change(function () {
        if ($("#chk_TKTAll")[0].checked) {
            $(".chk_TKT").prop('checked', true);
        }
        else
            $(".chk_TKT").prop('checked', false);
    })
    $('#chk_PVTAll').change(function () {
        if ($("#chk_PVTAll")[0].checked) {
            $(".chk_PVT").prop('checked', true);
        }
        else
            $(".chk_PVT").prop('checked', false);
    })
})
var Label;
function InclusionsAdd(Type) {
    if ($('#txt_InclusionsTkt' + Type).val() != "") {
        var noAttraction = $(".lbl_Inclusions_" + Type).length + 1;
        var id = 'lbl_Inclusions' + Type + noAttraction;
        Label = '<label class="size12 lbl_Atraction' + Type + '" id="lbl_Inclusions' + Type + noAttraction + '"><span class="lbl_Inclusions_' + Type + '"> ' + $('#txt_InclusionsTkt' + Type).val() + ''+ ';' +'</span> <i class="icon-cross-round" aria-hidden="true"  style="padding-left:5px" onclick="DeleteInclusions(' + id + ')"></i><br></label><br>'
        // Label = '<label class="size12 lbl_Atraction' + Type + '" id="lbl_Atraction' + Type + noAttraction + '">' + $('#txt_Inclusions' + Type).val() + '<i class="fa fa-times" aria-hidden="true" onclick="DeleteAttraction(\''+lbl_Atraction'+ id +')"></i></label><br>'
        // Label = '<input type="checkbox" value="' + $('#txt_Inclusions' + Type).val() + '" checked="checked"  id="chk_Inclusions' + Type + noAttraction + '" class="chk_Inclusion' + Type + '"/> <label class="size12 lblInclusionsTkt" for="chk_Inclusions' + Type + noAttraction + '">' + $('#txt_Inclusions' + Type).val() + '</label><br>'
        $("#idinclusionTkt" + Type).append(Label);
        $("#txt_InclusionsTkt" + Type).val("");
    }
    else {
        $('#txt_InclusionsTkt' + Type).focus()
        Success("Please Insert Inclusions Name")
    }
}
function ExclusionsAdd(Type) {
    if ($('#txt_ExclusionsTkt' + Type).val() != "") {
        var noAttraction = $(".lbl_Exclusions_" + Type).length + 1;
        var id = 'lbl_Exclusions' + Type + noAttraction;
        Label = '<label class="size12 lbl_Atraction' + Type + '" id="lbl_Exclusions' + Type + noAttraction + '"><span class="lbl_Exclusions_' + Type + '"> ' + $('#txt_ExclusionsTkt' + Type).val() + '' + ';' + '</span><i class="icon-cross-round" aria-hidden="true"  style="padding-left:5px" onclick="DeleteExclusions(' + id + ')"></i><br></label><br>'
        //Label = '<input type="checkbox" value="' + $('#txt_Exclusions' + Type).val() + '" checked="checked"  id="chk_Exclusions' + Type + noAttraction + '" class="chk_Exclusions' + Type + '"/> <label class="size12 lblInclusionsTkt" for="chk_Exclusions' + Type + noAttraction + '">' + $('#txt_Exclusions' + Type).val() + '</label><br>'
        $("#idexclusionTkt" + Type).append(Label);
        $("#txt_ExclusionsTkt" + Type).val("");
    }
    else {
        $('#txt_Exclusions' + Type).focus()
        Success("Please Insert Exclusions Name")
    }
}

//TKT Spcl Date

function InclusionsAddSpl(Type) {
    if ($('#txt_InclusionsSpl' + Type).val() != "") {
        var noAttraction = $(".lbl_Inclusions_" + Type).length + 1;
        var id = 'lbl_Inclusionsp' + Type + noAttraction;
        Label = '<label class="size12 lbl_Atraction' + Type + '" id="lbl_Inclusionsp' + Type + noAttraction + '"><span class="lbl_Inclusions_' + Type + '"> ' + $('#txt_InclusionsSpl' + Type).val() + '' + ';' + '</span> <i class="icon-cross-round" aria-hidden="true"  style="padding-left:5px" onclick="DeleteInclusionsSpl(' + id + ')"></i><br></label><br>'
        // Label = '<label class="size12 lbl_Atraction' + Type + '" id="lbl_Atraction' + Type + noAttraction + '">' + $('#txt_Inclusions' + Type).val() + '<i class="fa fa-times" aria-hidden="true" onclick="DeleteAttraction(\''+lbl_Atraction'+ id +')"></i></label><br>'
        // Label = '<input type="checkbox" value="' + $('#txt_Inclusions' + Type).val() + '" checked="checked"  id="chk_Inclusions' + Type + noAttraction + '" class="chk_Inclusion' + Type + '"/> <label class="size12 lblInclusionsTkt" for="chk_Inclusions' + Type + noAttraction + '">' + $('#txt_Inclusions' + Type).val() + '</label><br>'
        $("#idinclusionSpl" + Type).append(Label);
        $("#txt_InclusionsSpl" + Type).val("");
    }
    else {
        $('#txt_InclusionsSpl' + Type).focus()
        Success("Please Insert Inclusions Name")
    }
}
function ExclusionsAddSpl(Type) {
    if ($('#txt_ExclusionsSpl' + Type).val() != "") {
        var noAttraction = $(".lbl_Exclusions_" + Type).length + 1;
        var id = 'lbl_Exclusionsp' + Type + noAttraction;
        Label = '<label class="size12 lbl_Atraction' + Type + '" id="lbl_Exclusionsp' + Type + noAttraction + '"><span class="lbl_Exclusions_' + Type + '"> ' + $('#txt_ExclusionsSpl' + Type).val() + '' + ';' + '</span><i class="icon-cross-round" aria-hidden="true"  style="padding-left:5px" onclick="DeleteExclusionsSpl(' + id + ')"></i><br></label><br>'
        //Label = '<input type="checkbox" value="' + $('#txt_Exclusions' + Type).val() + '" checked="checked"  id="chk_Exclusions' + Type + noAttraction + '" class="chk_Exclusions' + Type + '"/> <label class="size12 lblInclusionsTkt" for="chk_Exclusions' + Type + noAttraction + '">' + $('#txt_Exclusions' + Type).val() + '</label><br>'
        $("#idexclusionSpl" + Type).append(Label);
        $("#txt_ExclusionsSpl" + Type).val("");
    }
    else {
        $('#txt_ExclusionsSpl' + Type).focus()
        Success("Please Insert Exclusions Name")
    }
}

//End TKT Spcl Date

//SIC 
function InclusionsAddSIC(Type) {
    if ($('#txt_InclusionsSIC' + Type).val() != "") {
        var noAttraction = $(".lbl_Inclusions_" + Type).length + 1;
        var id = 'lbl_Inclusiontkt' + Type + noAttraction;
        Label = '<label class="size12 lbl_Atraction' + Type + '" id="lbl_Inclusiontkt' + Type + noAttraction + '"><span class="lbl_Inclusions_' + Type + '"> ' + $('#txt_InclusionsSIC' + Type).val() + '' + ';' + '</span> <i class="icon-cross-round" aria-hidden="true"  style="padding-left:5px" onclick="DeleteInclusionsSIC(' + id + ')"></i><br></label><br>'
        // Label = '<label class="size12 lbl_Atraction' + Type + '" id="lbl_Atraction' + Type + noAttraction + '">' + $('#txt_Inclusions' + Type).val() + '<i class="fa fa-times" aria-hidden="true" onclick="DeleteAttraction(\''+lbl_Atraction'+ id +')"></i></label><br>'
        // Label = '<input type="checkbox" value="' + $('#txt_Inclusions' + Type).val() + '" checked="checked"  id="chk_Inclusions' + Type + noAttraction + '" class="chk_Inclusion' + Type + '"/> <label class="size12 lblInclusionsTkt" for="chk_Inclusions' + Type + noAttraction + '">' + $('#txt_Inclusions' + Type).val() + '</label><br>'
        $("#idinclusionSIC" + Type).append(Label);
        $("#txt_InclusionsSIC" + Type).val("");
    }
    else {
        $('#txt_InclusionsSIC' + Type).focus()
        Success("Please Insert Inclusions Name")
    }
}
function ExclusionsAddSIC(Type) {
    if ($('#txt_ExclusionsSIC' + Type).val() != "") {
        var noAttraction = $(".lbl_Exclusions_" + Type).length + 1;
        var id = 'lbl_Exclusiontkt' + Type + noAttraction;
        Label = '<label class="size12 lbl_Atraction' + Type + '" id="lbl_Exclusiontkt' + Type + noAttraction + '"><span class="lbl_Exclusions_' + Type + '"> ' + $('#txt_ExclusionsSIC' + Type).val() + '' + ';' + '</span><i class="icon-cross-round" aria-hidden="true"  style="padding-left:5px" onclick="DeleteExclusionsSIC(' + id + ')"></i><br></label><br>'
        //Label = '<input type="checkbox" value="' + $('#txt_Exclusions' + Type).val() + '" checked="checked"  id="chk_Exclusions' + Type + noAttraction + '" class="chk_Exclusions' + Type + '"/> <label class="size12 lblInclusionsTkt" for="chk_Exclusions' + Type + noAttraction + '">' + $('#txt_Exclusions' + Type).val() + '</label><br>'
        $("#idexclusionSIC" + Type).append(Label);
        $("#txt_ExclusionsSIC" + Type).val("");
    }
    else {
        $('#txt_ExclusionsSIC' + Type).focus()
        Success("Please Insert Exclusions Name")
    }
}

//End SIC 

//SIC spcl dates
function InclusionsAddSSIC(Type) {
    if ($('#txt_InclusionsSplSIC' + Type).val() != "") {
        var noAttraction = $(".lbl_Inclusions_" + Type).length + 1;
        var id = 'lbl_Inclusiontsp' + Type + noAttraction;
        Label = '<label class="size12 lbl_Atraction' + Type + '" id="lbl_Inclusiontsp' + Type + noAttraction + '"><span class="lbl_Inclusions_' + Type + '"> ' + $('#txt_InclusionsSplSIC' + Type).val() + '' + ';' + '</span> <i class="icon-cross-round" aria-hidden="true"  style="padding-left:5px" onclick="DeleteInclusionsSICSP(' + id + ')"></i><br></label><br>'
        // Label = '<label class="size12 lbl_Atraction' + Type + '" id="lbl_Atraction' + Type + noAttraction + '">' + $('#txt_Inclusions' + Type).val() + '<i class="fa fa-times" aria-hidden="true" onclick="DeleteAttraction(\''+lbl_Atraction'+ id +')"></i></label><br>'
        // Label = '<input type="checkbox" value="' + $('#txt_Inclusions' + Type).val() + '" checked="checked"  id="chk_Inclusions' + Type + noAttraction + '" class="chk_Inclusion' + Type + '"/> <label class="size12 lblInclusionsTkt" for="chk_Inclusions' + Type + noAttraction + '">' + $('#txt_Inclusions' + Type).val() + '</label><br>'
        $("#idinclusionSplSIC" + Type).append(Label);
        $("#txt_InclusionsSplSIC" + Type).val("");
    }
    else {
        $('#txt_InclusionsSplSIC' + Type).focus()
        Success("Please Insert Inclusions Name")
    }
}
function ExclusionsAddSSIC(Type) {
    if ($('#txt_ExclusionsSplSIC' + Type).val() != "") {
        var noAttraction = $(".lbl_Exclusions_" + Type).length + 1;
        var id = 'lbl_Exclusionstsp' + Type + noAttraction;
        Label = '<label class="size12 lbl_Atraction' + Type + '" id="lbl_Exclusionstsp' + Type + noAttraction + '"><span class="lbl_Exclusions_' + Type + '"> ' + $('#txt_ExclusionsSplSIC' + Type).val() + '' + ';' + '</span><i class="icon-cross-round" aria-hidden="true"  style="padding-left:5px" onclick="DeleteExclusionsSICSP(' + id + ')"></i><br></label><br>'
        //Label = '<input type="checkbox" value="' + $('#txt_Exclusions' + Type).val() + '" checked="checked"  id="chk_Exclusions' + Type + noAttraction + '" class="chk_Exclusions' + Type + '"/> <label class="size12 lblInclusionsTkt" for="chk_Exclusions' + Type + noAttraction + '">' + $('#txt_Exclusions' + Type).val() + '</label><br>'
        $("#idexclusionSplSIC" + Type).append(Label);
        $("#txt_ExclusionsSplSIC" + Type).val("");
    }
    else {
        $('#txt_ExclusionsSplSIC' + Type).focus()
        Success("Please Insert Exclusions Name")
    }
}
//End SIC spcl dates

function DeleteInclusions(lblthis) {
    $(lblthis).remove()

}

function DeleteExclusions(lblthis) {
    $(lblthis).remove()

}

function DeleteInclusionsSpl(lblthis) {
    $(lblthis).remove()

}
function DeleteExclusionsSpl(lblthis) {
    $(lblthis).remove()

}

function DeleteInclusionsSIC(lblthis) {
    $(lblthis).remove()

}
function DeleteExclusionsSIC(lblthis) {
    $(lblthis).remove()

}

function DeleteInclusionsSICSP(lblthis) {
    $(lblthis).remove()

}
function DeleteExclusionsSICSP(lblthis) {
    $(lblthis).remove()

}


function GetInclusions(Type) {
    debugger
    var Incltusions = '';
    for (var i = 0; i < $(".lbl_Inclusions_" + Type).length; i++) {
        //if ($(".chk_Inclusion" + Type)[i].checked)
        Incltusions += $(".lbl_Inclusions_" + Type)[i].textContent + ";";
    }
    return Incltusions;

}

function GetExclusions(Type) {
    var Exclusions = '';
    for (var i = 0; i < $(".lbl_Exclusions_" + Type).length; i++) {
        //if ($(".chk_Exclusions" + Type)[i].checked)
        Exclusions += $(".lbl_Exclusions_" + Type)[i].textContent + ";";
    }
    return Exclusions;

}

function GetDays(Type) {
    var sDays = '';
    if ($("#chk_" + Type + "All")[0].checked == false) {
        for (var i = 0; i < $(".chk_" + Type).length; i++) {
            if ($(".chk_" + Type)[i].checked)
                sDays += $(".chk_" + Type)[i].defaultValue + ",";
        }
    }
    else {
        sDays = "All Days";
    }

    return sDays;

}

function SetInclusions(Type, sInclusions) {
    try {

        Type = Type.replace("sic", "Sic").replace("TKT", "Tkt").replace("pvt", "Pkt")
        $("#idinclusion" + Type).empty();
        var noInclusions = $(".lbl_Atraction").length;
        var ListInclusions;
        if (sInclusions == null)
        {
            ListInclusions = "";
        }
        else
        {
             ListInclusions = sInclusions.split(";")
        }

        
        Label = "";
        for (var i = 0; i < ListInclusions.length; i++) {
            var noInclusions = noInclusions + 1;
            // var noInclusions = $(".lbl_Atraction" + Type).length + 1;
            var id = 'lbl_Atraction' + Type + noInclusions;
            if (ListInclusions[i] == "")
                continue;
            Label += '<label class="size12 lbl_Atraction' + Type + '" id="lbl_Atraction' + Type + noInclusions + '">' + ListInclusions[i].trim() + '<i class="fa fa-times" aria-hidden="true" style="padding-left:5px" onclick="DeleteAttraction(' + id + ')"></i><br></label><br>'
            // Label += '<input type="checkbox" value="' + ListInclusions[i].trim() + '" checked="checked" id="chk_Inclusions' + Type + noInclusions + '" class="chk_Inclusion' + Type + '" /> <label class="size12 lblAtraction" for="chk_Atraction' + noInclusions + '">' + ListInclusions[i].trim() + '</label><br>'
        }
        $("#idinclusion" + Type).append(Label);
    }
    catch (ex) {
        Success(ex)
    }
}

function SetExclusions(Type, sExclusions) {
    try {

        Type = Type.replace("sic", "Sic").replace("TKT", "Tkt").replace("pvt", "Pkt")
        $("#idexclusion" + Type).empty();
        var noExclusions = $(".lbl_Atraction2").length;
        var ListExclusions = sExclusions.split(";");
        Label = "";
        for (var i = 0; i < ListExclusions.length; i++) {
            // $(".chk_Atraction").length;
            var noExclusions = noExclusions + 1;
            // var noExclusions = $(".chk_Exclusions" + Type).length + 1;
            var id2 = 'lbl_Atraction2' + Type + noExclusions;
            if (ListExclusions[i] == "")
                continue;
            Label += '<label class="size12 lbl_Atraction2' + Type + '" id="lbl_Atraction2' + Type + noExclusions + '">' + ListExclusions[i].trim() + '<i class="fa fa-times" aria-hidden="true" style="padding-left:5px" onclick="DeleteAttraction(' + id2 + ')"></i><br></label><br>'
            // Label += '<input type="checkbox" value="' + ListExclusions[i].trim() + '" checked="checked" id="chk_Exclusions' + Type + noExclusions + '" class="chk_Exclusions'+Type+'" /> <label class="size12 lblAtraction" for="chk_Exclusions' + Type + noExclusions + '">' + ListExclusions[i].trim() + '</label><br>'

        }
        $("#idexclusion" + Type).append(Label);
    }
    catch (ex) {
    }
}

function SetDays(Type, sDays) {
    try {
        Type = Type.replace("sic", "SIC").replace("pvt", "PVT").replace("tkt", "TKT")
        if (sDays != "All Days") {
            var sDays = sDays.split(",")
            for (var i = 0; sDays.length; i++) {
                $('input[value="' + sDays[i].trim() + '"][class="chk_' + Type + '"]').prop("checked", true);
            }
        }
        else {

            $("#chk_" + Type + "All")[0].click();
        }


    }
    catch (ex) {
    }
}