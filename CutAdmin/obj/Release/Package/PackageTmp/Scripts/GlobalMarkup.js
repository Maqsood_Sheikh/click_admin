﻿
$(document).ready(function () {
    GetGlobalMarkup();
});

function GetGlobalMarkup() {

    $.ajax({
        type: "POST",
        url: "../MarkUpHandler.asmx/GetGlobalMarkup",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                var List = result.Arr;

                $("#txt_MarkUpPercentage").val(List[0].MarkupPercentage);
                $("#txt_MarkUpAmount").val(List[0].MarkupAmmount);
                $("#txt_CommisionPercentage").val(List[0].CommessionPercentage);
                $("#txt_CommisionAmount").val(List[0].CommessionAmmount);

            }
            else if (result.retCode == 0) {
                $("#txt_MarkUpPercentage").val("0");
                $("#txt_MarkUpAmount").val("0");
                $("#txt_CommisionPercentage").val("0");
                $("#txt_CommisionAmount").val("0");
            }
        },
        error: function () {
        }

    });
}


function UpdateGlobalMarkup() {

    var MarkPercentage = $("#txt_MarkUpPercentage").val();
    if (MarkPercentage == "") {
        Success("Please enter Markup Percentage");
        return false;
    }
    var MarkUpAmount = $("#txt_MarkUpAmount").val();
    if (MarkUpAmount == "") {
        Success("Please enter MarkUp Amount");
        return false;
    }
    var CommPercentage = $("#txt_CommisionPercentage").val();
    if (CommPercentage == "") {
        Success("Please enter Commission Percentage");
        return false;
    }
    var CommAmount = $("#txt_CommisionAmount").val();
    if (CommAmount == "") {
        Success("Please enter Commission Amount");
        return false;
    }

    var data = {
        MarkPercentage: MarkPercentage,
        MarkUpAmount: MarkUpAmount,
        CommPercentage: CommPercentage,
        CommAmount: CommAmount,
    }

    $.ajax({
        type: "POST",
        url: "../MarkUpHandler.asmx/UpdateGlobalMarkupDetails",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                Success("Global Markup Detail Update successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
               
            }
            else if (result.retCode == 0) {
                Success("Something went wrong.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
             
            }
        },
        error: function () {
        }

    });
}