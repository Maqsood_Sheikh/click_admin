﻿$(function () {
    GetCountry();
    LoadAreaGroup();
    var tbl = document.getElementById("tbl_AreaGroup");
    tbl.style.width = "100%";
});
var contryname;
function GetCountry() {
    $.ajax({
        type: "POST",
        url: "MapLocationHandler.asmx/GetCountry",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCountry = result.GetCountry;
                if (arrCountry.length > 0) {
                    $("#selCountryMap").empty();
                    // $("#selCountryy").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any Country</option>';
                    for (i = 0; i < arrCountry.length; i++) {
                        ddlRequest += '<option value="' + arrCountry[i].Country2Code + '_' + arrCountry[i].Country3Code + '">' + arrCountry[i].CountryName + '</option>';
                        //contryname = arrCountry[i].Countryname;
                    }
                    $("#selCountryMap").append(ddlRequest);
                    // $("#selCountryy").append(ddlRequest);

                }
            }
        },
        error: function () {
        }
    });
}
var arrCountry;

var No = 1;
var RowNo;
function AddRow() {
    RowNo = No + 1;
    var div = '';
    div += '<div id="row' + RowNo + '" style="width: 100%">';
    div += '<div class="columns">';
    div += '<div class="new-row one-columns"></div>';
    div += '<div class="two-columns" id="div_GTA' + RowNo + '" style="margin-left: 24.125px;">';
    div += '<span class="text-left">GTA</span>';
    div += '<br>';
    div += '<select id="selGTA' + RowNo + '" onchange="" class="full-width  select OfferType">';
    div += '</select>';
    div += '</div>';
    div += '<div class="two-columns" id="div_HB' + RowNo + '">';
    div += '<span class="text-left">HB</span>';
    div += '<br>'
    div += '<select id="selHB' + RowNo + '" onchange="" class="full-width  select OfferType">';
    div += '</select>';
    div += '</div>';
    div += '<div class="two-columns" id="div_TBO' + RowNo + '">';
    div += '<span class="text-left">TBO</span>';
    div += '<br>';
    div += '<select id="selTBO' + RowNo + '" onchange="" class="full-width  select OfferType">';
    div += '</select>';
    div += '</div>';
    div += '<div class="two-columns" id="div_DOT' + RowNo + '">';
    div += '<span class="text-left">DOTW</span>';
    div += '<br>';
    div += '<select id="selDOT' + RowNo + '" onchange="" class="full-width  select OfferType">';
    div += '</select>';
    div += '</div>';
    div += '<div class="two-columns" id="div_MGH' + RowNo + '">';
    div += '<span class="text-left">MGH</span>';
    div += '<br>';
    div += '<select id="selMGH' + RowNo + '" onchange="" class="full-width  select OfferType">';
    div += '</select>';
    div += '</div>';

    div += '<div id="RemoveRowDiv' + RowNo + '" class="one-columns" style="padding-top:16px" title="Remove Row">';
    div += '<i onclick="RemoveRow(' + RowNo + ',' + No + ')" aria-hidden="true"><label class="button blue-gradient" ><span class="icon-minus"></span></label></i>';
    div += '</div>';

    div += '<div id="AddRowDiv' + RowNo + '" class="one-columns" style="padding-top:16px" title="Add Row">';
    div += '<i onclick="AddRow()" aria-hidden="true"><label class="button blue-gradient" ><span class="icon-plus"></span></label></i>';
    div += '</div>';

    $("#AddRowDiv" + No).hide();
    $("#RemoveRowDiv1").show();
    $(div).insertAfter("#row" + No);
    No = No + 1;
}

function RemoveRow(RowNum, Nobr) {
    if (RowNum == RowNo) {
        $("#row" + RowNum).remove();
        $("#AddRowDiv" + Nobr).show();
        RowNo = RowNo - 1;
        No = No - 1;
    } else {
        $("#row" + RowNum).remove();
        RowNo = RowNo - 1;
        No = No - 1;
    }
}



function GetLocation(Country) {

    var Code = Country.split('_');
    var Countryname = $.grep(arrCountry, function (p) { return p.Country2Code == Code[0]; })
         .map(function (p) { return p.CountryName; });

    $.ajax({
        type: "POST",
        url: "MapLocationHandler.asmx/GetLocation",
        data: '{"Country":"' + Country + '","Countryname":"' + Countryname + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrGTAlocation = result.GTAlocation;
                arrMGHAlocation = result.MGHAlocation;
                arrDOTWAlocation = result.DOTWAlocation;
                arrTBOAlocation = result.TBOAlocation;
                arrGRNAlocation = result.GRNAlocation;
                arrListHB = result.ListHB;
                arrGetAllMapCity = result.GetAllMapCity;
                var arrCity = [];
                var arrCity2 = [];
                if (arrGTAlocation.length > 0) {
                    $("#selGTA1").empty();
                    // $("#selCountryy").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any City</option>';
                    for (var i = 0; i < arrGetAllMapCity.length ; i++) {
                        var arrCity = [];
                        var arrCity2 = [];
                        for (j = 0; j < arrGTAlocation.length; j++) {
                            var MapCity = arrGetAllMapCity[i].Name.toUpperCase();
                            var City = arrGTAlocation[j].City_Name.toUpperCase();
                            if (MapCity.includes(City)) {
                               
                                     arrCity.push(arrGTAlocation[j])
                                    //ddlRequest += '<option value="' + arrGTAlocation[i].City_Code + '" onchange="GetLocation(this.value)">' + arrGTAlocation[i].City_Name + ' ' + '(' + arrGTAlocation[i].City_Code + ')' + '</option>';
                                    //contryname = arrCountry[i].Countryname;
                               }
                            else {
                                arrCity2.push(arrGTAlocation[j])
                            }
                            

                        }
                        arrGTAlocation = arrCity2;
                    }
                    for (var i = 0; i < arrGTAlocation.length; i++) {
                        ddlRequest += '<option value="'+ arrGTAlocation[i].City_Code +'">' + arrGTAlocation[i].City_Name + '(' + arrGTAlocation[i].City_Code + ')' + '</option>';
                    }
                  
                    $("#selGTA1").append(ddlRequest);
                    // $("#selCountryy").append(ddlRequest);

                }

                if (arrMGHAlocation.length > 0) {
                    $("#selMGH1").empty();
                    // $("#selCountryy").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any City</option>';
                    for (var i = 0; i < arrGetAllMapCity.length ; i++) {
                        var arrCity = [];
                        var arrCity2 = [];
                        for (j = 0; j < arrMGHAlocation.length; j++) {
                            var MapCity = arrGetAllMapCity[i].Name.toUpperCase();
                            var City = arrMGHAlocation[j].name.toUpperCase();
                            if (MapCity.includes(City)) {

                                arrCity.push(arrMGHAlocation[j])
                                //ddlRequest += '<option value="' + arrGTAlocation[i].City_Code + '" onchange="GetLocation(this.value)">' + arrGTAlocation[i].City_Name + ' ' + '(' + arrGTAlocation[i].City_Code + ')' + '</option>';
                                //contryname = arrCountry[i].Countryname;
                            }
                            else {
                                arrCity2.push(arrMGHAlocation[j])
                            }


                        }
                        arrMGHAlocation = arrCity2;
                    }
                    for (var i = 0; i < arrMGHAlocation.length; i++) {
                        ddlRequest += '<option value="' + arrMGHAlocation[i].id + '">' + arrMGHAlocation[i].name + '(' + arrMGHAlocation[i].id + ')' + '</option>';
                    }

                    $("#selMGH1").append(ddlRequest);
                    // $("#selCountryy").append(ddlRequest);

                }

                if (arrDOTWAlocation.length > 0) {
                    $("#selDOT1").empty();
                    // $("#selCountryy").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any City</option>';
                    for (var i = 0; i < arrGetAllMapCity.length ; i++) {
                        var arrCity = [];
                        var arrCity2 = [];
                        for (j = 0; j < arrDOTWAlocation.length; j++) {
                            var MapCity = arrGetAllMapCity[i].Name.toUpperCase();
                            var City = arrDOTWAlocation[j].name.toUpperCase();
                            if (MapCity.includes(City)) {

                                arrCity.push(arrDOTWAlocation[j])
                                //ddlRequest += '<option value="' + arrGTAlocation[i].City_Code + '" onchange="GetLocation(this.value)">' + arrGTAlocation[i].City_Name + ' ' + '(' + arrGTAlocation[i].City_Code + ')' + '</option>';
                                //contryname = arrCountry[i].Countryname;
                            }
                            else {
                                arrCity2.push(arrDOTWAlocation[j])
                            }


                        }
                        arrDOTWAlocation = arrCity2;
                    }
                    for (var i = 0; i < arrDOTWAlocation.length; i++) {
                        ddlRequest += '<option value="' + arrDOTWAlocation[i].code + '">' + arrDOTWAlocation[i].name + '(' + arrDOTWAlocation[i].code + ')' + '</option>';
                    }

                    $("#selDOT1").append(ddlRequest);
                    // $("#selCountryy").append(ddlRequest);

                }

                if (arrTBOAlocation.length > 0) {
                    $("#selTBO1").empty();
                    // $("#selCountryy").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any City</option>';
                    for (var i = 0; i < arrGetAllMapCity.length ; i++) {
                        var arrCity = [];
                        var arrCity2 = [];
                        for (j = 0; j < arrTBOAlocation.length; j++) {
                            var MapCity = arrGetAllMapCity[i].Name.toUpperCase();
                            var City = arrTBOAlocation[j].City_Name.toUpperCase();
                            if (City.includes(MapCity)) {

                                arrCity.push(arrTBOAlocation[j])
                                //ddlRequest += '<option value="' + arrGTAlocation[i].City_Code + '" onchange="GetLocation(this.value)">' + arrGTAlocation[i].City_Name + ' ' + '(' + arrGTAlocation[i].City_Code + ')' + '</option>';
                                //contryname = arrCountry[i].Countryname;
                            }
                            else {
                                arrCity2.push(arrTBOAlocation[j])
                            }


                        }
                        arrTBOAlocation = arrCity2;
                    }
                    for (var i = 0; i < arrTBOAlocation.length; i++) {
                        ddlRequest += '<option value="' + arrTBOAlocation[i].City_Code + '">' + arrTBOAlocation[i].City_Name + '(' + arrTBOAlocation[i].City_Code + ')' + '</option>';
                    }

                    $("#selTBO1").append(ddlRequest);
                    // $("#selCountryy").append(ddlRequest);

                }

                if (arrListHB.length > 0) {
                    $("#selHB1").empty();
                    // $("#selCountryy").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any City</option>';
                    for (var i = 0; i < arrGetAllMapCity.length ; i++) {
                        var arrCity = [];
                        var arrCity2 = [];
                        for (j = 0; j < arrListHB.length; j++)
                        {
                            var name = arrListHB[j].Destination.split(',');
                            var MapCity = arrGetAllMapCity[i].Name.toUpperCase();
                            var City = name[0].toUpperCase();
                            if (MapCity.includes(City)) {

                                arrCity.push(arrListHB[j])
                                //ddlRequest += '<option value="' + arrGTAlocation[i].City_Code + '" onchange="GetLocation(this.value)">' + arrGTAlocation[i].City_Name + ' ' + '(' + arrGTAlocation[i].City_Code + ')' + '</option>';
                                //contryname = arrCountry[i].Countryname;
                            }
                            else {
                                arrCity2.push(arrListHB[j])
                            }


                        }
                        arrListHB = arrCity2;

                    }
                    for (var i = 0; i < arrListHB.length; i++) {
                        var name = arrListHB[i].Destination.split(',');
                        ddlRequest += '<option value="' + arrListHB[i].DestinationCode + '" >' + name[0] + '(' + arrListHB[i].DestinationCode + ')' + '</option>';
                    }

                    $("#selHB1").append(ddlRequest);
                    // $("#selCountryy").append(ddlRequest);

                }

                if (arrGRNAlocation.length > 0) {
                    $("#selGRN1").empty();
                    // $("#selCountryy").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any City</option>';
                    for (var i = 0; i < arrGetAllMapCity.length ; i++) {
                        var arrCity = [];
                        var arrCity2 = [];
                        for (j = 0; j < arrGRNAlocation.length; j++) {
                            var MapCity = arrGetAllMapCity[i].Name.toUpperCase();
                            var City = arrGRNAlocation[j].Cityname.toUpperCase();
                            if (MapCity.includes(City)) {

                                arrCity.push(arrGRNAlocation[j])
                                //ddlRequest += '<option value="' + arrGTAlocation[i].City_Code + '" onchange="GetLocation(this.value)">' + arrGTAlocation[i].City_Name + ' ' + '(' + arrGTAlocation[i].City_Code + ')' + '</option>';
                                //contryname = arrCountry[i].Countryname;
                            }
                            else {
                                arrCity2.push(arrGRNAlocation[j])
                            }


                        }
                        arrGRNAlocation = arrCity2;
                    }
                    for (var i = 0; i < arrGRNAlocation.length; i++) {
                        ddlRequest += '<option value="' + arrGRNAlocation[i].Citycode + '">' + arrGRNAlocation[i].Cityname + '(' + arrGRNAlocation[i].Citycode + ')' + '</option>';
                    }

                    $("#selGRN1").append(ddlRequest);
                    // $("#selCountryy").append(ddlRequest);

                }


            }
        },
        error: function () {
        }
    });
}

function SaveCityLocation() {

    var CountryCode = $('#selCountryMap').val().split("_");
    if (CountryCode == "-")
    {
        Success("please select Country");
        return false;
    }
    var ClickCode = $('#txtCountryCode').val();
    if (ClickCode == "") {
        Success("please Enter Code");
        return false;
    }
  
    var GTACode = $('#selGTA1').val();
    var Name = $('#selHB1 option:selected').text().split("(");
   // var Name2 = Name[0].split(" ");
    var HBCode = $('#selHB1').val();
    var TBOCode = $('#selTBO1').val();
    var DOTCode = $('#selDOT1').val();
    var MGHCode = $('#selMGH1').val();
    var GRNCode = $('#selGRN1').val();

    var data = {
        ClickCode: ClickCode,
        Name: Name[0],
        CountryCode: CountryCode[0],
        GTACode: GTACode,
        HBCode: HBCode,
        TBOCode: TBOCode,
        DOTCode: DOTCode,
        MGHCode: MGHCode,
        GRNCode: GRNCode
    }

    $.ajax({
        type: "POST",
        url: "MapLocationHandler.asmx/SaveCityLocation",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Mapped Sucessfully")
                setTimeout(function () {
                    window.location.href = "MapLocation.aspx";
                }, 2000);
     
            }
            else {
                Success("Something Went Wrong")
            }
        },
        error: function () {
        }
    });
}

function LoadAreaGroup() {
    $("#tbl_AreaGroup").dataTable().fnClearTable();
    $("#tbl_AreaGroup").dataTable().fnDestroy();

    $.ajax({
        type: "POST",
        url: "MapLocationHandler.asmx/LoadAreaGroup",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var AreaGroup = result.areaGroup;
            console.log(result);
            if (result.retCode == 1) {
                var tr = '';
                for (var i = 0; i < AreaGroup.length; i++) {
                    var SrNo = i + 1;
                    tr += '<tr>';
                    tr += '<td>' + SrNo + '</td>';
                    tr += '<td>' + AreaGroup[i].Destination_ID + '</td>';
                    tr += '<td>' + AreaGroup[i].Name + '</td>';
                    tr += '<td>' + AreaGroup[i].Country_Code + '</td>';
                    //tr += '<td>' + AreaGroup[i].Group_ID + '</td>';
                    tr += '<td>' + AreaGroup[i].GTA + '</td>';
                    tr += '<td>' + AreaGroup[i].HB + '</td>';
                    tr += '<td>' + AreaGroup[i].DOTW + '</td>';
                    tr += '<td>' + AreaGroup[i].MGH + '</td>';
                    tr += '<td>' + AreaGroup[i].TBO + '</td>';
                    tr += '<td>' + AreaGroup[i].GRN + '</td>';
                    tr += '<td align="center"><a style="cursor:pointer"  onclick="UpdateCode(\'' + AreaGroup[i].Country_Code + '\',\'' + AreaGroup[i].GTA + '\',\'' + AreaGroup[i].HB + '\',\'' + AreaGroup[i].DOTW + '\',\'' + AreaGroup[i].MGH + '\',\'' + AreaGroup[i].TBO + '\',\'' + AreaGroup[i].GRN + '\',\'' + AreaGroup[i].Destination_ID + '\',\'' + AreaGroup[i].Sid + '\')"><span class="icon-pencil icon-size2" title="Edit"></span></a></td>'
                    tr += '</tr>';


                    //html += '<td style="width:20%" align="center"><a style="cursor:pointer" href="AddActivity.aspx?id=' + arrActivity[i].Aid + '"><span class="icon-pencil icon-size2" title="Edit"></span></a> &nbsp; | &nbsp; <a style="cursor:pointer" onclick="Delete(\'' + arrActivity[i].Aid + '\',\'' + arrActivity[i].Name + '\')"><span class="icon-trash" title="Delete"></span></a></td>'
                    //<td style="width:10%" align="center" class=" "><a style="cursor:pointer" onclick="Rates('0MN','Half Day City Tour','Deira','Dubai^','UAE')"><span class="icon-pencil icon-size2" title="Edit Rates"></span></a></td>
                }
                $("#tbl_AreaGroup tbody").append(tr);

                $('[data-toggle="tooltip"]').tooltip()

                $("#tbl_AreaGroup").dataTable({
                     bSort: false, sPaginationType: 'full_numbers',
                });
            }
        },
        error: function () {
        }
    });
}


function GetCode(id)
{
    if(id == "1")
    {
        var Code = $("#selGTA1").val();
        if(Code == "-" || Code == null)
        {
            Success("Please Select City First.");
            $("#GTAradio").removeAttr('checked')
        }
        else {
            $("#txtCountryCode").val(Code)
            
        }
    }

    if (id == "2") {
        var Code = $("#selHB1").val();
        if (Code == "-" || Code == null) {
            Success("Please Select City First.");
            $("#HBradio").removeAttr('checked')
        }
        else {
            $("#txtCountryCode").val(Code)

        }
    }

    if (id == "3") {
        var Code = $("#selTBO1").val();
        if (Code == "-" || Code == null) {
            Success("Please Select City First.");
            $("#TBOradio").removeAttr('checked')
        }
        else {
            $("#txtCountryCode").val(Code)

        }
    }

    if (id == "4") {
        var Code = $("#selDOT1").val();
        if (Code == "-" || Code == null) {
            Success("Please Select City First.");
            $("#DOTWradio").removeAttr('checked')
        }
        else {
            $("#txtCountryCode").val(Code)

        }
    }

    if (id == "5") {
        var Code = $("#selMGH1").val();
        if (Code == "-" || Code == null) {
            Success("Please Select City First.");
            $("#MGHWradio").removeAttr('checked')
        }
        else {
            $("#txtCountryCode").val(Code)

        }
    }

    if (id == "6") {
        var Code = $("#selGRN1").val();
        if (Code == "-" || Code == null) {
            Success("Please Select City First.");
            $("#GRNradio").removeAttr('checked')
        }
        else {
            $("#txtCountryCode").val(Code)

        }
    }
}

function BindCode(id)
{
    $("#txtCountryCode").val(id)
}

var DestCodeU;

function UpdateCode(Country, GTA, HB, DOTW, MGH, TBO, GRN,DestCode,sid) {
    $("#btn_save").hide();
    $("#btn_Update").show();
    DestCodeU = sid;
    var Countryname = $.grep(arrCountry, function (p) { return p.Country2Code == Country; })
           .map(function (p) { return p.CountryName; });

    $.ajax({
        type: "POST",
        url: "MapLocationHandler.asmx/GetLocation",
        data: '{"Country":"' + Country + '","Countryname":"' + Countryname + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrGTAlocation = result.GTAlocation;
                arrMGHAlocation = result.MGHAlocation;
                arrDOTWAlocation = result.DOTWAlocation;
                arrTBOAlocation = result.TBOAlocation;
                arrGRNAlocation = result.GRNAlocation;
                arrListHB = result.ListHB;
                arrGetAllMapCity = result.GetAllMapCity;
                var arrCity = [];
                var arrCity2 = [];
                if (arrGTAlocation.length > 0) {
                    $("#selGTA1").empty();
                    // $("#selCountryy").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any Country</option>';
                         
                    for (var i = 0; i < arrGTAlocation.length; i++) {
                        ddlRequest += '<option value="' + arrGTAlocation[i].City_Code + '">' + arrGTAlocation[i].City_Name + '(' + arrGTAlocation[i].City_Code + ')' + '</option>';
                    }

                    $("#selGTA1").append(ddlRequest);
                    // $("#selCountryy").append(ddlRequest);

                }

                if (arrMGHAlocation.length > 0) {
                    $("#selMGH1").empty();
                    // $("#selCountryy").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any Country</option>';
                    for (i = 0; i < arrMGHAlocation.length; i++) {
                        ddlRequest += '<option value="' + arrMGHAlocation[i].id + '" onchange="GetLocation(this.value)">' + arrMGHAlocation[i].name + ' ' + '(' + arrMGHAlocation[i].id + ')' + '</option>';
                        //contryname = arrCountry[i].Countryname;
                    }
                    $("#selMGH1").append(ddlRequest);
                    // $("#selCountryy").append(ddlRequest);

                }

               if (arrDOTWAlocation.length > 0) {
                    $("#selDOT1").empty();
                    // $("#selCountryy").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any Country</option>';
                    for (i = 0; i < arrDOTWAlocation.length; i++) {
                        ddlRequest += '<option value="' + arrDOTWAlocation[i].code + '" onchange="GetLocation(this.value)">' + arrDOTWAlocation[i].name + ' ' + '(' + arrDOTWAlocation[i].code + ')' + '</option>';
                        //contryname = arrCountry[i].Countryname;
                    }
                    $("#selDOT1").append(ddlRequest);
                    // $("#selCountryy").append(ddlRequest);

                }

                if (arrTBOAlocation.length > 0) {
                    $("#selTBO1").empty();
                    // $("#selCountryy").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any Country</option>';
                    for (i = 0; i < arrTBOAlocation.length; i++) {
                        ddlRequest += '<option value="' + arrTBOAlocation[i].City_Code + '" onchange="GetLocation(this.value)">' + arrTBOAlocation[i].City_Name + ' ' + '(' + arrTBOAlocation[i].City_Code + ')' + '</option>';
                        //contryname = arrCountry[i].Countryname;
                    }
                    $("#selTBO1").append(ddlRequest);
                    // $("#selCountryy").append(ddlRequest);

                }


                if (arrListHB.length > 0) {
                    $("#selHB1").empty();
                    // $("#selCountryy").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any Country</option>';
                    for (i = 0; i < arrListHB.length; i++) {
                        var name = arrListHB[i].Destination.split(',');
                        ddlRequest += '<option value="' + arrListHB[i].DestinationCode + '" onchange="GetLocation(this.value)">' + name[0] + ' ' + '(' + arrListHB[i].DestinationCode + ')' + '</option>';
                        //contryname = arrCountry[i].Countryname;
                    }
                    $("#selHB1").append(ddlRequest);
                    // $("#selCountryy").append(ddlRequest);

                }

                if (arrGRNAlocation.length > 0) {
                    $("#selGRN1").empty();
                    // $("#selCountryy").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any Country</option>';
                    for (i = 0; i < arrGRNAlocation.length; i++) {
                        //var name = arrListHB[i].Destination.split(',');
                        ddlRequest += '<option value="' + arrGRNAlocation[i].Citycode + '" onchange="GetLocation(this.value)">' + arrGRNAlocation[i].Cityname + ' ' + '(' + arrGRNAlocation[i].Citycode + ')' + '</option>';
                        //contryname = arrCountry[i].Countryname;
                    }
                    $("#selGRN1").append(ddlRequest);
                    // $("#selCountryy").append(ddlRequest);

                }
                 
                //$("#selCountryMap").val(Country);
                //$("#selGTA1").val(GTA);
                //$("#selMGH1").val(MGH);
                //$("#selDOT1").val(DOTW);
                //$("#selTBO1").val(TBO);
                //$("#selHB1").val(HB);
                //$("#selGRN1").val(GRN);
                $("#txtCountryCode").val(DestCode);

                GetCountryNew(Country, arrCountry);
                GetGTA(GTA, arrGTAlocation);
                GetMGH(MGH, arrMGHAlocation);
                GetDOTW(DOTW, arrDOTWAlocation);
                GetTBO(TBO, arrTBOAlocation);
                GetHB(HB, arrListHB);
                GetGRN(GRN, arrGRNAlocation);


             
                
            }

        },
        error: function () {
        }
    });
}
 

function UpdateCityLocation() {
    var ClickCode = $('#txtCountryCode').val();
    var CountryCode = $('#selCountryMap').val().split("_");
    // var CountryCode = $('#selCountryMap').val().split("_");
    var GTACode = $('#selGTA1').val();
    var Name = $('#selHB1 option:selected').text().split("(");
    // var Name2 = Name[0].split(" ");
    var HBCode = $('#selHB1').val();
    var TBOCode = $('#selTBO1').val();
    var DOTCode = $('#selDOT1').val();
    var MGHCode = $('#selMGH1').val();
    var GRNCode = $('#selGRN1').val();

    var data = {
        DestCodeU: DestCodeU,
        ClickCode: ClickCode,
        Name: Name[0],
        CountryCode: CountryCode[0],
        GTACode: GTACode,
        HBCode: HBCode,
        TBOCode: TBOCode,
        DOTCode: DOTCode,
        MGHCode: MGHCode,
        GRNCode: GRNCode
    }

    $.ajax({
        type: "POST",
        url: "MapLocationHandler.asmx/UpdateCityLocation",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Updated Sucessfully")
                setTimeout(function () {
                    window.location.href = "MapLocation.aspx";
                }, 2000);
 
            }
            else {
                Success("Something Went Wrong")
            }
        },
        error: function () {
        }
    });
}

function GetCountryNew(Country, arrCountry) {
    debugger;
    try {
        var checkclass = document.getElementsByClassName('check');
        var Country = Country.split(",")
        var Country3Code = $.grep(arrCountry, function (p) { return p.Country2Code == Country; })
           .map(function (p) { return p.Country3Code; });


        $("#div_Map .select span")[0].textContent = Country + '_' + Country3Code;
        for (var i = 0; i < arrCountry.length; i++) {
            if (arrCountry[i].Country2Code == Country) {
                //// $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').addclass('checked');     
                $("#div_Map .select span")[0].textContent = arrCountry[i].CountryName;
                $('input[value="' + arrCountry[i].Country2Code + '_' + arrCountry[i].Country3Code + '"][class="OfferType"]').prop("selected", true);

                $("#selCountryMap").val(Country + '_' + Country3Code);
                //  pGTA += GTA[i] + ",";
                var selected = [];
            }


        }
    }
    catch (ex)
    { }
}


function GetGTA(GTA, arrGTAlocation)
{
    debugger;
    try {
        var checkclass = document.getElementsByClassName('check');
        var GTA = GTA.split(",")

        $("#div_GTA1 .select span")[0].textContent = GTA;
        for (var i = 0; i < arrGTAlocation.length; i++) {
            if (arrGTAlocation[i].City_Code == GTA)
            {
                //// $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').addclass('checked');     
                $("#div_GTA1 .select span")[0].textContent = arrGTAlocation[i].City_Name + '(' + GTA + ')';
                $('input[value="' + GTA + '"][class="OfferType"]').prop("selected", true);
               
                $("#selGTA1").val(GTA);
                //  pGTA += GTA[i] + ",";
                var selected = [];
            }
         

       }
    }
    catch (ex)
    { }
}

function GetMGH(MGH, arrMGHAlocation) {
    debugger;
    try {
        var checkclass = document.getElementsByClassName('check');
        var MGH = MGH.split(",")

        $("#div_MGH1 .select span")[0].textContent = MGH;
        for (var i = 0; i < arrMGHAlocation.length; i++) {
            if (arrMGHAlocation[i].id == MGH) {
                //// $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').addclass('checked');     
                $("#div_MGH1 .select span")[0].textContent = arrMGHAlocation[i].name + '(' + MGH + ')';
                $('input[value="' + MGH + '"][class="OfferType"]').prop("selected", true);

                $("#selMGH1").val(MGH);
                //  pGTA += GTA[i] + ",";
                var selected = [];
            }


        }
    }
    catch (ex)
    { }
}

function GetDOTW(DOTW, arrDOTWAlocation) {
    debugger;
    try {
        var checkclass = document.getElementsByClassName('check');
        var DOTW = DOTW.split(",")

        $("#div_DOT1 .select span")[0].textContent = DOTW;
        for (var i = 0; i < arrDOTWAlocation.length; i++) {
            if (arrDOTWAlocation[i].code == DOTW) {
                //// $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').addclass('checked');     
                $("#div_DOT1 .select span")[0].textContent = arrDOTWAlocation[i].name + '(' + DOTW + ')';
                $('input[value="' + DOTW + '"][class="OfferType"]').prop("selected", true);

                $("#selDOT1").val(DOTW);
                //  pGTA += GTA[i] + ",";
                var selected = [];
            }


        }
    }
    catch (ex)
    { }
}

function GetTBO(TBO, arrTBOAlocation) {
    debugger;
    try {
        var checkclass = document.getElementsByClassName('check');
        var TBO = TBO.split(",")

        $("#div_TBO1 .select span")[0].textContent = TBO;
        for (var i = 0; i < arrTBOAlocation.length; i++) {
            if (arrTBOAlocation[i].City_Code == TBO) {
                //// $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').addclass('checked');     
                $("#div_TBO1 .select span")[0].textContent = arrTBOAlocation[i].City_Name + '(' + TBO + ')';
                $('input[value="' + TBO + '"][class="OfferType"]').prop("selected", true);

                $("#selTBO1").val(TBO);
                //  pGTA += GTA[i] + ",";
                var selected = [];
            }


        }
    }
    catch (ex)
    { }
}

function GetHB(HB, arrListHB) {
    debugger;
    try {
        var checkclass = document.getElementsByClassName('check');
        var HB = HB.split(",")

        $("#div_HB1 .select span")[0].textContent = HB;
        for (var i = 0; i < arrListHB.length; i++) {
            if (arrListHB[i].DestinationCode == HB) {
                //// $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').addclass('checked');     
                var name = arrListHB[i].Destination.split(',');
                $("#div_HB1 .select span")[0].textContent = name[0] + '(' + HB + ')';
                $('input[value="' + HB + '"][class="OfferType"]').prop("selected", true);

                $("#selHB1").val(HB);
                //  pGTA += GTA[i] + ",";
                var selected = [];
            }


        }
    }
    catch (ex)
    { }
}

function GetGRN(GRN, arrGRNAlocation) {
    debugger;
    try {
        var checkclass = document.getElementsByClassName('check');
        var GRN = GRN.split(",")

        $("#div_GRN1 .select span")[0].textContent = GRN;
        for (var i = 0; i < arrGRNAlocation.length; i++) {
            if (arrGRNAlocation[i].Citycode == GRN) {
                //// $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').addclass('checked');     
                $("#div_GRN1 .select span")[0].textContent = arrGRNAlocation[i].Cityname + '(' + GRN + ')';
                $('input[value="' + GRN + '"][class="OfferType"]').prop("selected", true);

                $("#selGRN1").val(GRN);
                //  pGTA += GTA[i] + ",";
                var selected = [];
            }


        }
    }
    catch (ex)
    { }
}
