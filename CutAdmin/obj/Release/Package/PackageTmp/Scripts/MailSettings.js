﻿$(document).ready(function () {
    HotelActivityMails();
})

function HotelActivityMails() {
    $("#tbl_HotelDetails").dataTable().fnClearTable();
    $("#tbl_HotelDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "../GenralHandler.asmx/GetHotelActivityMails",
        data: '{ "Type": "Hotel"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var HotelMailsList = result.Arr;
                var MailsList = result.MailsList;
                var tRow = '';

                var Email = '';
                var CcMail = '';
                var ErrorMessage = "";
                var Type = 'Hotel';
                for (var i = 0; i < HotelMailsList.length; i++)
                {

                    try {
                        var List = $.grep(MailsList, function (p) { return p.Activity == HotelMailsList[i].Activity; })
                        .map(function (p) { return p; });

                        if (List.length != 0) {
                            Email = List[0].Email;
                            CcMail = List[0].CcMail;
                            ErrorMessage = List[0].ErroMessage;
                        }
                        else {
                            Email = "-";
                            CcMail = '-';
                            ErrorMessage = '';
                        }
                    }
                    catch (ex) {

                    }
                   
                    tRow += '<tr>';
                    tRow += '<td style="width:20%">' + HotelMailsList[i].Activity + '</td>';
                    tRow += '<td style="width:25%">' + Email + '</td>';
                    tRow += '<td style="width:25%">' + CcMail + '</td>';
                    tRow += '<td style="width:20%">' + ErrorMessage + '</td>';
                    tRow += '<td style="width:10%" class="align-center"><a style="cursor:pointer" data-toggle="modal" data-target="#VisaModal"  onclick="VisaModalFunc(\'' + HotelMailsList[i].Activity + '\',\'' + Email + '\',\'' + CcMail + '\',\'' + Type + '\',\'' + ErrorMessage + '\'); return false" title="Click to view hotel details"><span class="icon-publish" title="Update" aria-hidden="true" ></span></a></td>';
                    tRow += '</tr>';
                }
                $("#tbl_HotelDetails tbody").html(tRow);
                $("#tbl_HotelDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                document.getElementById("tbl_HotelDetails").removeAttribute("style")
            }
        },
        error: function () {
        }
    });
}

var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
function AddMails(Act) {
    debugger
    var bValid = true;
   
    var Activity = Act;
    var Meassage = $('#txt_Error').val();
   
    var MailId = $("#txt_Email").val();
    var Type = "Hotel";
  
    var CcMails = "";
    var Mails = document.getElementsByClassName('email_name'), i;
    for (i = 0; i < Mails.length; i += 1) {
        if (i != (Mails.length - 1)) {

            if (!emailReg.test(Mails[i].textContent)) {
                bValid = false;
                $(".multiple_emails-input text-left").focus();
                Success("Please Insert Valid Mail Id");
            }
            else {
                if (!emailReg.test(Mails[i].textContent)) {
                    bValid = false;
                    $(".multiple_emails-input text-left").focus();
                    Success("Please Insert Valid Mail Id");
                }
                else {
                    CcMails += Mails[i].textContent + ";";
                }

            }
        }
        else {
            CcMails += Mails[i].textContent;
        }

        var Message = $("#Message")

    }
    if (bValid == true) {
        $.ajax({
            type: "POST",
            url: "../GenralHandler.asmx/UpdateVisaMails",
            data: '{ "Activity": "' + Activity + '","Type": "' + Type + '","MailsId": "' + MailId + '","CcMails": "' + CcMails + '","BCcMail": "","Message": "' + Meassage + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                debugger
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Mail Updated Successfully..")

                    $(".multiple_emails-ul").empty();
                   if (Type == "Hotel") {
                        $("#Sel_HotelActivity").val("-");
                        $("#txt_HotelMailId").val("");
                        window.location.reload();
                        //HotelActivityMails();
                    }
                }
                if (result.retCode == 0) {

                }
            },
            error: function () {
            }
        });
    }

}
function RemoveLi(link) {
    link.remove();
}
var Act = "";
function VisaModalFunc(Activity, Email, CcMail, type, Msg) {
    debugger;
    Act = Activity;
    var cont = '';
    cont += '<div class="modal-body">'
    cont += '<div class="scrollingDiv">'
    cont += '<div class="columns">'
    cont += '<div class="twelve-columns twelve-columns-mobile"><label>Activity</label><br/> <div class="input full-width">'
    cont += '<input name="prompt-value" id="txt_Activity" value="' + Activity + '" class="input-unstyled full-width"  placeholder="Activity Name" type="text" readonly="readonly" style="cursor:no-drop">'
    cont += '</div></div>'
    cont += '<div class="twelve-columns twelve-columns-mobile"><label>Mail Id </label><br/> <div class="input full-width">'
    cont += '<input name="prompt-value" id="txt_Email" value="' + Email + '" class="input-unstyled full-width"  placeholder="Mail Id" type="text">'
    cont += '</div></div>'
    cont += '<div class="twelve-columns twelve-columns-mobile"><label>Error Message </label><br/> <div class="input full-width">'
    cont += '<input name="prompt-value" id="txt_Error" value="' + Msg + '" class="input-unstyled full-width"  placeholder="Error Message" type="text">'
    cont += '</div></div>'
    cont += '<div class="twelve-columns twelve-columns-mobile">'
    cont += '<div class="updatemail"><ul id="CcMailList">'
    if (CcMail != "" && CcMail != "-") {
        var CcArr = CcMail.split(";");
        for (var i = 0; i < (CcArr.length) ; i++) {
            cont += '<li class="multiple_emails" onclick="RemoveLi(this)"><a style="cursor:pointer" class="multiple_emails-close" title="Remove"><i class="icon-cross"></i></a><span class="email_name">' + CcArr[i] + '</span></li>'
        }
    }
    cont += '</ul><input type="text" id="inp_CcAddMail" style="border:none; margin-left:10px; margin-bottom:2px" onblur="AddCcMail()" placeholder="Enter Cc Mail here"></div></div><button type="button" style="float: right;" class="button anthracite-gradient" onclick="AddMails(\'' + Act + '\')">Add</button></div></div></div>'

    $.modal({
        content: cont,
        title: 'Update Mail',
        width: 500,
        scrolling: true,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'anthracite-gradient glossy displayNone',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
}

//function VisaModalFunc(Activity, Email, CcMail, type, Msg) {
//    debugger;


//    $(".multiple_emails-ul").empty();

//    var CCmails = CcMail.split(';')
//    var li = ''
//    if (CCmails != "" && CCmails != "-") {
//        for (var i = 0; i < (CCmails.length) ; i++) {
//            li += '<li class="multiple_emails-email" onclick="RemoveLi(this)"><a style="cursor:pointer"   class="multiple_emails-close" title="Remove"><i class="glyphicon glyphicon-remove"></i></a><span class="email_name" data-email="' + CCmails[i] + '">' + CCmails[i] + '</span></li>'
//        }

//        $(".multiple_emails-ul").append(li)
//        //$("#txt_CCMailId").append(li)
//    }

//    if (type == "Visa") {
//        $("#divForVisa").empty();
//        $("#ErrorDiv").empty();
//        $("#header").text("Visa Mails");
//        //var VisaHtml = 'Select Activity :<select id="Sel_Activity" class="form-control" onchange="ActivityMails()"><option selected="selected" value="-">-Select Activity-</option><option>Visa Apply</option><option>Incomplete application</option> <option>Delete Incomplete application</option><option>Documents Required</option><option>Application Approved</option><option>Application Rejected</option><option>Application Cancelled</option><option>Entered Country</option><option>Exit Country</option><option>Stay Limit Expiry</option><option>Visa Expire</option></select><label style="color: red; margin-top: 3px; display: none" id="lbl_Name"><b>* This field is required</b></label><br> Mail Id :<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_MailId" placeholder="Mail Id" class="form-control ui-autocomplete-input" data-original-title=" " title=" " autocomplete="off">;'

//        var VisaHtml = 'Activity :<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="Sel_Activity" placeholder="Activity" class="form-control ui-autocomplete-input" data-original-title=" " title=" " autocomplete="off" readonly><label style="color: red; margin-top: 3px; display: none" id="lbl_Name"><b>* This field is required</b></label><br> Mail Id :<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_MailId" placeholder="Mail Id" class="form-control ui-autocomplete-input" data-original-title=" " title=" " autocomplete="off">';

//        $("#divForVisa").append(VisaHtml);

//        var Error = 'Error Message :<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span> <textarea id="VisaMessage" class="form-control col-md-10" style="height:60px;width:540px"></textarea><label style="color: red; margin-top: 3px; display: none" id="lbl_ErrorMessage"><b>* This field is required</b></label>';
//        $("#ErrorDiv").append(Error);
//        $("#VisaMessage").text(Msg);

//        $("#Sel_Activity").val(Activity);
//        if (Email == "-") {
//            $("#txt_MailId").val("");
//        } else {
//            $("#txt_MailId").val(Email);
//        }
//    }
//    else if (type == "Otb") {

//        $("#divForVisa").empty();
//        $("#ErrorDiv").empty();

//        var OTBHtml = 'Activity :<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="Sel_AirLineInMail" placeholder="Activity" class="form-control ui-autocomplete-input" data-original-title=" " title=" " autocomplete="off" readonly><label style="color: red; margin-top: 3px; display: none" id="lbl_Name"><b>* This field is required</b></label><br> Mail Id :<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_AirlinesMailId" placeholder="Mail Id" class="form-control ui-autocomplete-input" data-original-title=" " title=" " autocomplete="off">';
//        $("#header").text("OTB Mails");
//        $("#divForVisa").append(OTBHtml);

//        var Error = 'Error Message :<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span> <textarea id="OTBMessage" class="form-control" style="height:80px;width:540px"></textarea><label style="color: red; margin-top: 3px; display: none" id="lbl_ErrorMessage"><b>* This field is required</b></label>';
//        $("#ErrorDiv").append(Error);
//        $("#OTBMessage").text(Msg);

//        $("#Sel_AirLineInMail").val(Activity);
//        if (Email == "-") {
//            $("#txt_AirlinesMailId").val("");
//        } else {
//            $("#txt_AirlinesMailId").val(Email);
//        }
//    }
//    else if (type == "Hotel") {

//        $("#divForVisa").empty();
//        $("#ErrorDiv").empty();
//        $("#header").text("Hotel Mails");
//        //var HotelHtml = 'Select Activity :<select id="Sel_HotelActivity" class="form-control" onchange="ActivityMails()"><option selected="selected" value="-">-Select Activity-</option><option>Booking Confirm</option><option>Booking Error</option><option>Booking On Hold </option><option>Booking Cancelled</option><option>Booking Reconfirm</option><option>Cancelletion Dead Line</option><option>On Hold booking dead line</option><option>List of check in</option><option>Offer Booking Mail</option></select><label style="color: red; margin-top: 3px; display: none" id="lbl_Sel_HotelActivity"><b>* This field is required</b></label><br> Mail Id :<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_HotelMailId" placeholder="Mail Id" class="form-control ui-autocomplete-input" data-original-title=" " title=" " autocomplete="off">;'

//        var HotelHtml = 'Activity :<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="Sel_HotelActivity" placeholder="Activity" class="form-control ui-autocomplete-input" data-original-title=" " title=" " autocomplete="off" readonly><label style="color: red; margin-top: 3px; display: none" id="lbl_Sel_HotelActivity"><b>* This field is required</b></label><br> Mail Id :<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_HotelMailId" placeholder="Mail Id" class="form-control ui-autocomplete-input" data-original-title=" " title=" " autocomplete="off">';
//        $("#divForVisa").append(HotelHtml);


//        $("#Sel_HotelActivity").val(Activity);
//        if (Email == "-") {
//            $("#txt_HotelMailId").val("");
//        }
//        else {
//            $("#txt_HotelMailId").val(Email);
//        }

//        var Error = 'Error Message :<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span> <textarea id="HotelMessage" class="form-control" style="height:80px;width:540px"></textarea><label style="color: red; margin-top: 3px; display: none" id="lbl_ErrorMessage"><b>* This field is required</b></label>';
//        $("#ErrorDiv").append(Error);
//        $("#HotelMessage").text(Msg);

//    }


//    //$("#txt_CCMailId").val(CCmails);


//}

function AddCcMail() {
    var NewCcMail = $('#inp_CcAddMail').val();
    if (!NewCcMail) {
        return false;
    }
    if (!emailReg.test(NewCcMail)) {
        bValid = false;
        $("inp_CcAddMail").focus();
        Success("Please Insert Valid Mail Id");
        $('#inp_CcAddMail').val('');
        return false;
    }
    var ccli = '<li class="multiple_emails" onclick="RemoveLi(this)"><a style="cursor:pointer" class="multiple_emails-close" title="Remove"><i class="icon-cross"></i></a><span class="email_name">' + NewCcMail + '</span></li>'
    $('#CcMailList').append(ccli);
    $('#inp_CcAddMail').val('');
}