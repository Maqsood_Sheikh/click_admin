﻿
$(document).ready(function () {
    GetPropertyType();
});


function AddPropertyType()
{
    var bValid = true;
    var PropertyType = $("#txtPropertyType").val();
    var Description = $("#txtDescription").val();
   
    if (PropertyType == "-")
    {
        Success("Please Insert Propert Type");
        bValid = false;
        $("#txtPropertyType").focus()
    }
    if (Description == "") {
        Success("Please Insert Description")
        bValid = false;
        $("#txtDescription").focus()
    }
    if (bValid == true)
    {
        $.ajax({
            type: "POST",
            url: "PropertyHandler.asmx/AddPropertyType",
            data: '{"PropertyType":"' + PropertyType + '","Description":"' + Description + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Property Added Successfully")
                    GetPropertyType()
                    $("#txtPropertyType").val("");
                    $("#txtDescription").val("");
                }
            }
        })
    }
    
}



function GetPropertyType() {
    $("#tbl_PropertyType tbody tr").remove();
    debugger;
    //$("#tbl_StaffDetails").dataTable().fnClearTable();
    //$("#tbl_StaffDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "PropertyHandler.asmx/GetPropertyType",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var List_PropertyType = result.Staff;
                var tRow = '';
                //var tRow = '<tr><td><b>Name</b></td><td style="text-align:center"><b>Email | Password Manage</b></td><td><b>Mobile</b></td><td><b>Unique Code</b></td><td align="center"><b>Edit | Status | Delete</b></td></tr>';
                for (var i = 0; i < List_PropertyType.length; i++) {
                    tRow += '<tr>';
                    tRow += '<td>'+(i+1)+ '</td>';
                    tRow += '<td>' + List_PropertyType[i].PropertyType + '</td>';
                    tRow += '<td>' + List_PropertyType[i].PropertyDescription + '</td>';
                    
                    //tRow += '<td align="center"><a style="cursor:pointer" href="AddStaff.aspx?nID=' + List_StaffDetails[i].sid + '&sName=' + List_StaffDetails[i].ContactPerson + '&sDesignation=' + List_StaffDetails[i].Designation + '&sAddress=' + List_StaffDetails[i].Address + '&sCity=' + List_StaffDetails[i].Code + '&sCountry=' + List_StaffDetails[i].Country + '&nPinCode=' + List_StaffDetails[i].PinCode + '&sEmail=' + List_StaffDetails[i].email + '&nPhone=' + List_StaffDetails[i].phone + '&nMobile=' + List_StaffDetails[i].Mobile + '&bLoginFlag=' + List_StaffDetails[i].LoginFlag + '&sGroup=' + List_StaffDetails[i].StaffCategory+ '"><span class="glyphicon glyphicon-edit" title="Edit" aria-hidden="true"></span></a> | <a href="#"><span class="' + List_StaffDetails[i].LoginFlag.replace("True", "glyphicon glyphicon-volume-up").replace("False", "glyphicon glyphicon-lock") + '" onclick="Activate(\'' + List_StaffDetails[i].sid + '\',\'' + List_StaffDetails[i].LoginFlag + '\',\'' + List_StaffDetails[i].AgencyName + '\')" title="' + List_StaffDetails[i].LoginFlag.replace("True", "Deactivate").replace("False", "Activate") + '" aria-hidden="true"></span></a> | <a style="cursor:pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor:pointer" onclick="DeleteStaff(\'' + List_StaffDetails[i].uid + '\',\'' + List_StaffDetails[i].ContactPerson + '\')"></span></a></td>';
                   // tRow += '<td align="center"><a style="cursor:pointer" href="AddStaff.aspx?nID=' + List_StaffDetails[i].sid + '&sName=' + List_StaffDetails[i].ContactPerson + '&sDesignation=' + List_StaffDetails[i].Designation + '&sAddress=' + List_StaffDetails[i].Address + '&sCity=' + List_StaffDetails[i].Code + '&sCountry=' + List_StaffDetails[i].Country + '&nPinCode=' + List_StaffDetails[i].PinCode + '&sEmail=' + List_StaffDetails[i].email + '&nPhone=' + List_StaffDetails[i].phone + '&nMobile=' + List_StaffDetails[i].Mobile + '&bLoginFlag=' + List_StaffDetails[i].LoginFlag + '&sDepartment=' + List_StaffDetails[i].Department + '"><span class="glyphicon glyphicon-edit" title="Edit" aria-hidden="true"></span></a> | <a href="#"><span class="' + List_StaffDetails[i].LoginFlag.replace("True", "glyphicon glyphicon-eye-open").replace("False", "glyphicon glyphicon-eye-close") + '" onclick="Activate(\'' + List_StaffDetails[i].sid + '\',\'' + List_StaffDetails[i].LoginFlag + '\',\'' + List_StaffDetails[i].ContactPerson + '\')" title="' + List_StaffDetails[i].LoginFlag.replace("True", "Deactivate").replace("False", "Activate") + '" aria-hidden="true"></span></a> | <a style="cursor:pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor:pointer" onclick="DeleteStaff(\'' + List_StaffDetails[i].uid + '\',\'' + List_StaffDetails[i].ContactPerson + '\')"></span></a></td>';

                    tRow += '<td class="align-right vertical-center"><span class="button-group compact"><a onclick="GetDetails(\'' + List_PropertyType[i].PropertyTypeID + '\')" class="button icon-pencil">Edit</a><a onclick="DeletePropertyType(\'' + List_PropertyType[i].PropertyTypeID + '\',\'' + List_PropertyType[i].PropertyType +'\')" class="button icon-trash" title="Delete"></a></span></td>';
                    tRow += '</tr>';
                }
                $("#tbl_PropertyType tbody").html(tRow);
                //$("#tbl_StaffDetails").dataTable({
                //     bSort: false, sPaginationType: 'full_numbers',
                //});


            }
            if (result.retCode == 0) {
                var tRow = '';
                //var tRow = '<tr><td><b>Name</b></td><td><b>Email</b></td><td><b>Mobile</b></td><td><b>Unique Code</b></td><td align="center"><b>Edit | Status | Delete</b></td></tr>';
                tRow += "<tr><td colspan=5><span> No Record Found</span></td></tr>";
                $("#tbl_PropertyType").append(tRow);

            }

        },
        error: function () {
        }

    });
}



function DeletePropertyType(PropertyTypeID, PropertyType) {
    if (confirm("Are you sure you want to delete " + PropertyType + "?") == true) {
        $.ajax({
            url: "PropertyHandler.asmx/DeletePropertyType",
            type: "post",
            data: '{"PropertyTypeID":"' + PropertyTypeID + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                if (result.retCode == 1) {
                    Success("Property Type has been deleted successfully.");
                    GetPropertyType()
                } else if (result.retCode == 0) {
                    Success("Something went wrong while processing your request! Please try again.");
                }
            },
            error: function () {
                Success('Error occured while processing your request! Please try again.');
            }
        });
    }
}


var id;
function GetDetails(PropertyTypeID) {
     id = PropertyTypeID;
        $.ajax({
            url: "PropertyHandler.asmx/GetDetails",
            type: "post",
            data: '{"PropertyTypeID":"' + PropertyTypeID + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                if (result.retCode == 1) {
                    var data=result.dttable;

                    $("#txtPropertyType").val(data[0].PropertyType);
                    $("#txtDescription").val(data[0].PropertyDescription);
                    $("#btn_Add").hide();
                    $("#btn_Update").show();

                } else if (result.retCode == 0) {
                    Success("Something went wrong while processing your request! Please try again.");
                }
            },
            error: function () {
                Success('Error occured while processing your request! Please try again.');
            }
        });
    }


function UpdatePropertyType() {
    var bValid = true;
    var PropertyType = $("#txtPropertyType").val();
    var Description = $("#txtDescription").val();

    if (PropertyType == "-") {
        Success("Please Insert Propert Type");
        bValid = false;
        $("#txtPropertyType").focus()
    }
    if (Description == "") {
        Success("Please Insert Description")
        bValid = false;
        $("#txtDescription").focus()
    }
    if (bValid == true) {
        $.ajax({
            type: "POST",
            url: "PropertyHandler.asmx/UpdatePropertyType",
            data: '{"PropertyType":"' + PropertyType + '","Description":"' + Description + '","id":"' + id + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Property Update Successfully")
                    $("#btn_Add").show();
                    $("#btn_Update").hide();
                    $("#txtPropertyType").val("");
                    $("#txtDescription").val("");
                    GetPropertyType()
                }
            }
        })
    }

}



