﻿$(function () {
    GetAddOns()
});
var arrAddOns = new Array();
var sid = 0;
function SaveAddOns() {
    var Name = $("#txt_Name").val();
    var IsMeal = $("#Sel_Type").val();
    if (Validate()) {
        $.modal.confirm("Are You Sure you want to Save  " + Name, function () {
            var Desciption = $("#txt_Discription").val();
            var Active = false;
            var Data = {
                AddOnName: Name,
                Details: Desciption,
                IsMeal: IsMeal,
                ID: sid,
                Activate: true,
                Type:'-'
            }
            $.ajax({
                type: "POST",
                url: "handler/GenralHandler.asmx/SaveAddOn",
                data: JSON.stringify({ objAddOn: Data }),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.retCode == 1) {
                        sid = 0;
                        Success("Saved Successfully");
                        $("#btn_reset").click()
                        GetAddOns();
                    }
                    else if (result.retCode == 2) {
                        Success(result.ErrorMsg);
                    }
                    else {
                        Success(result.ErrorMsg);
                        setTimeout(function () {
                            window.location.href = "default.aspx";
                        }, 2000);
                    }
                }
            })
        }, function () {
            $('#modals').remove();
        });
    }
}

function Validate() {
    var bValid = true;
    if ($("#txt_Name").val() == "") {
        $("#lbl_Name").text("*Please insert Service Name");
        bValid = false;
    }
    else
        $("#lbl_Name").text("*");

    if ($("#Sel_Type").val() == "Select Type")
    {
        $("#lbl_Type").text("*Please Select Type");
        bValid = false;
    }
    else
        $("#lbl_Type").text("*");

    return bValid;
}
function GetAddOns() {
    $("#tbl_AddOns").dataTable().fnClearTable();
    $("#tbl_AddOns").dataTable().fnDestroy();
    $.ajax({
        url: "../handler/GenralHandler.asmx/LoadAddOns",
        type: "post",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                arrAddOns = result.arrAddOns
                for (var i = 0; i < arrAddOns.length; i++) {
                    var html = '';
                    html += '<tr><td style="width:8%" align="center">' + (i + 1) + '</td>'
                    html += '<td style="width:20%"  align="center">' + arrAddOns[i].AddOnName + ' </td>'
                    html += '<td style="width:30%"  align="center">' + arrAddOns[i].Details + ' </td>'
                    if (arrAddOns[i].IsMeal == true)
                        html += '<td style="width:30%"  align="center">Meal</td>'
                    else
                        html += '<td style="width:30%"  align="center">Genral</td>'
                    html += '<td style="width:12%">'
                    html += '<span class="button-group">'
                    html += '<a href="#" class="button" title="Edit" onclick="Update(\'' + arrAddOns[i].ID + '\',\'' + arrAddOns[i].AddOnName + '\',\'' + arrAddOns[i].IsMeal + '\')"><span class="icon-pencil"></a> '
                    html += '<a href="#" onclick="Delete(\'' + arrAddOns[i].ID + '\',\'' + arrAddOns[i].AddOnName + '\')" class="button" title="Delete"><span class="icon-trash"></span>'
                    html += '</span>'
                    html += '</td></tr>'
                    $("#tbl_AddOns").append(html);
                }
                $('[data-toggle="tooltip"]').tooltip()
                $("#tbl_AddOns").dataTable({
                     bSort: false, sPaginationType: 'full_numbers',
                });
                $('#tbl_AddOns').removeAttr("style");
            }
            else {
                Success(result.ErrorMsg);
                setTimeout(function () {
                    window.location.href = "default.aspx", 2000
                }, 2000);
            }

        }
    })
}

function Update(ID,Name,Type) {
    sid = ID;
    var AddOns = $.grep(arrAddOns, function (p) { return p.ID == ID })
                                            .map(function (p) { return p; })[0];
    $("#txt_Name").val(AddOns.AddOnName)
    $("#txt_Discription").val(AddOns.Details)
    $("#Sel_Type").val(AddOns.IsMeal.toString());
    if (AddOns.IsMeal)
        $(".select-value").text("Meal");
    else
        $(".select-value").text("Genral");
    $('#btn_Add').attr("value","Update");
}

function Delete(ID, Name) {
    $.modal.confirm("Are You Sure you want to Delete " + Name, function () {
        $.ajax({
            type: "POST",
            url: "../handler/GenralHandler.asmx/DeleteAddOns",
            data: JSON.stringify({ ID: ID }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Deleted Successfully");
                    GetAddOns()
                }
                else if (result.retCode == 2) {
                    Success(result.ErrorMsg);
                }
            }
        })

    }, function () {
        $('#modals').remove();
    });
}