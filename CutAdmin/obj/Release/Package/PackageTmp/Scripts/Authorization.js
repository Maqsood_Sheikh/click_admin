﻿$(document).ready(function () {
    PageLoad();
});

var arrFormList = new Array();
function PageLoad() {
    debugger;
    $.ajax({
        type: "POST",
        url: "HotelAdmin/Handler/AuthorizationHandler.asmx/GetAuthorizedFormList",
        data: '',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            //debugger;
            var arrFormList = response.d;
            if (arrFormList.length == 0) {
                window.location.href = 'Default.aspx'
            }
            else {
                //
                $.ajax({
                    type: "POST",
                    url: "HotelAdmin/Handler/AuthorizationHandler.asmx/CheckFranchisee",
                    data: '{}',
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    success: function (response) {
                        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        if (result.retCode == 1) {
                            if (result.Role != "3") {
                                $("#liUpdates").css("display", "");
                                $("#liAdministration").css("display", "");
                                $("#liPackages").css("display", "");
                            }
                        }
                    }
                })

                //
                for (i = 0; i < arrFormList.length; i++) {
                    switch (arrFormList[i]) {
                        case "SupplierList":
                            $("#liSu").css("display", "");
                            break;
                        case "Staff":
                            $("#liSt").css("display", "");
                            break;
                        case "ViewAccount":
                            $("#liVa").css("display", "");
                            break;
                        case "Statements":
                            $("#liSm").css("display", "");
                            break;
                        case "RoomRates":
                            $("#liRr").css("display", "");
                            break;
                        case "HotelList":
                            $("#liHl").css("display", "");
                            break;
                        case "AddHotel":
                            $("#liAh").css("display", "");
                            break;
                        case "GlobalMarkup":
                            $("#liGlm").css("display", "");
                            break;
                        case "GroupMarkup":
                            $("#liGpm").css("display", "");
                            break;
                        case "Tax":
                            $("#liTx").css("display", "");
                            break;
                        case "Role":
                            $("#liRl").css("display", "");
                            break;
                        case "EmailList":
                            $("#liEm").css("display", "");
                            break;
                        case "Location":
                            $("#liLn").css("display", "");
                            break;
                        case "AddOns":
                            $("#liAs").css("display", "");
                            break;
                        case "CancellationPolicy":
                            $("#liCp").css("display", "");
                            break;
                        case "Offer":
                            $("#liOr").css("display", "");
                            break;
                        case "CommissionReport":
                            $("#liCr").css("display", "");
                            break;
                        case "HotelBookingList":
                            $("#liHb").css("display", "");
                            break;
                        case "HotelFacility":
                            $("#liHf").css("display", "");
                            break;
                        case "AgentDetails":
                            $("#liAg").css("display", "");
                            break;
                        case "bankdetails":
                            $("#liBd").css("display", "");
                            break;
                        case "ApproveCredit":
                            $("#liAc").css("display", "");
                            break;
                        case "GroupBookingList":
                            $("#liGbl").css("display", "");
                            break;
                        case "Sales":
                            $("#lisSales").css("display", "");
                            break;
                        case "Suppliers":
                            $("#liSuppliers").css("display", "");
                            break;
                        case "StaticPackages":
                            $("#liStaticPackages").css("display", "");
                            break;
                        case "ExchangeMarkup":
                            $("#liExchangeMarkup").css("display", "");
                            break;
                        case "CityAddUpdate":
                            $("#liCityAddUpdate").css("display", "");
                            break;
                        case "ActivityMails":
                            $("#liActivityMails").css("display", "");
                            break;
                        case "PromoImage":
                            $("#liPromoImage").css("display", "");
                            break;
                        case "AddTaxces":
                            $("#liAddTaxces").css("display", "");
                            break;
                        case "QuotationDetails":
                            $("#liQuotationDetails").css("display", "");
                            break;
                    }
                }
            }
        },
        error: function () {
            window.location.href = '../Default.aspx'
        }
    });
}

function IsAuthorized(page) {
    $.ajax({
        type: "POST",
        url: "HotelAdmin/Handler/AuthorizationHandler.asmx/CheckUserAuthorities",
        data: '{"sFormName":"' + page + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                switch (page) {
                    case "SupplierList":
                        window.location = "SupplierDetails.aspx";
                        break;
                    case "Staff":
                        window.location = "StaffDetails.aspx";
                        break;
                    case "ViewAccount":
                        $("#liVa").css("display", "");
                        window.location = "Deposite.aspx";
                        break;
                    case "Statements":
                        window.location = "Statements.aspx";
                        break;
                    case "RoomRates":
                        window.location = "RoomRates.aspx";
                        break;
                    case "HotelList":
                        window.location = "HotelList.aspx";
                        break;
                    case "AddHotel":
                        window.location = "AddHotelDetails.aspx";
                        break;
                    case "GlobalMarkup":
                        window.location = "GlobalMarkup.aspx";
                        break;
                    case "GroupMarkup":
                        window.location = "GroupMarkup.aspx";
                        break;
                    case "Tax":
                        window.location = "addUpdateTax.aspx";
                        break;
                    case "Role":
                        window.location = "RoleManagement.aspx";
                        break;
                    case "EmailList":
                        window.location = "MailSettings.aspx";
                        break;
                    case "Location":
                        window.location = "AddLocation.aspx";
                        break;
                    case "AddOns":
                        window.location = "AddUpdateAddOns.aspx";
                        break;
                    case "CancellationPolicy":
                        window.location = "CancelationPolicies.aspx";
                        break;
                    case "Offer":
                        window.location = "MasterOffer.aspx";
                        break;
                    case "CommissionReport":
                        window.location = "CommissionReport.aspx";
                        break;
                    case "HotelBookingList":
                        window.location = "BookingList.aspx";
                        break;
                    case "HotelFacility":
                        window.location = "HotelFacility.aspx";
                        break;
                    case "AgentDetails":
                        window.location = "AgentDetails.aspx";
                        break;
                    case "bankdetails":
                        window.location = "bankdetails.aspx";
                        break;
                    case "ApproveCredit":
                        window.location = "agencydepositedetails.aspx";
                        break;
                    case "GroupBookingList":
                        window.location = "groupbookinglist.aspx";
                        break;
                    case "Sales":
                        window.location = "SalesPersonDetails.aspx";
                        break;
                    case "Suppliers":
                        window.location = "Suppliers.aspx";
                        break;
                    case "StaticPackages":
                        window.location = "StaticPackages.aspx";
                        break;
                    case "ExchangeMarkup":
                        window.location = "ExchangeMarkup.aspx";
                        break;
                    case "CityAddUpdate":
                        window.location = "CityAddUpdate.aspx";
                        break;
                    case "ActivityMails":
                        window.location = "ActivityMails.aspx";
                        break;
                    case "PromoImage":
                        window.location = "PromoImage.aspx";
                        break;
                    case "AddTaxces":
                        window.location = "AddTaxces.aspx";
                        break;
                    case "QuotationDetails":
                        window.location = "QuotationDetails.aspx";
                        break;
                }
            }
        },
        error: function () {
            Success("error occured while checking user authorization");
        }
    });
}

function UserProfile() {
    $.ajax({
        url: "HotelAdmin/Handler/GenralHandler.asmx/CheckUser",
        type: "post",
        data: {},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1 && result.UserType == "Supplier") {
                window.location.href = "Configration.aspx";
            }
            else if (result.retCode == 1 && result.UserType == "SupplierStaff") {
                window.location.href = "StaffProfile.aspx";
            }
            else
                window.location.href = "Default.aspx";
        },
        error: function () {
            Success('Error occured while authenticating user!');
        }
    });
}


function CheckUser() {
    $.ajax({
        url: "HotelAdmin/Handler/GenralHandler.asmx/CheckUser",
        type: "post",
        data: {},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1 && result.UserType == "Supplier") {
                window.location.href = "Configration.aspx";
            }
            else if (result.retCode == 1 && result.UserType == "SupplierStaff") {
                window.location.href = "ChangePassword.aspx";
            }
            else
                window.location.href = "Default.aspx";
        },
        error: function () {
            Success('Error occured while authenticating user!');
        }
    });
}