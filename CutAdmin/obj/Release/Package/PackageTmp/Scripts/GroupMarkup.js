﻿//$(document).ready(function () {
//    GetGroup();
//    GetGroupMarkup();
//});

//var List_AgentDetails = new Array();

//function GetGroup() {
//    $.ajax({
//        type: "POST",
//        url: "../handler/MarkUpHandler.asmx/GetGroup",
//        data: {},
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        success: function (response) {
//            var result = JSON.parse(response.d)
//            if (result.retCode == 1) {
//                var List = result.Arr;
//                var div = "";
//                for (var i = 0; i < List.length; i++) {
//                    div += '<option value="' + List[i].sid + '" >' + List[i].GroupName + '</option>'
//                }

//                $("#SelGroup").append(div);

//                // setTimeout(function () { $('#SelGroup option').filter(function () { return $.trim($(this).text()) == 'Group A'; }).attr('selected', true); }, 2000);

//                $("#SelGroup").val('5');
//                $("#DivGroupmark .select span")[0].textContent = "Select";
//                GetGroupMarkup()
//            }
//            else if (result.retCode == 0) {

//            }
//        },
//        error: function () {
//        }

//    });
//}

//function GetGroupMarkup() {
//    var Id = $("#SelGroup").val();

//    $.ajax({
//        type: "POST",
//        url: "../handler/MarkUpHandler.asmx/GetGroupMarkup",
//        data: JSON.stringify({ Id: Id }),
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        success: function (response) {
//            var result = JSON.parse(response.d)
//            if (result.retCode == 1) {
//                var List = result.Arr;

//                $("#txt_MarkUpPercentage").val(List[0].MarkupPercentage);
//                $("#txt_MarkUpAmount").val(List[0].MarkupAmmount);
//                $("#txt_CommisionPercentage").val(List[0].CommessionPercentage);
//                $("#txt_CommisionAmount").val(List[0].CommessionAmmount);

//            }
//            else if (result.retCode == 0) {
//                $("#txt_MarkUpPercentage").val("0");
//                $("#txt_MarkUpAmount").val("0");
//                $("#txt_CommisionPercentage").val("0");
//                $("#txt_CommisionAmount").val("0");
//            }
//        },
//        error: function () {
//        }

//    });
//}

//function AddGroup() {
//    $.modal({
//        content:

//                '<div class="columns">' +
//                '     <div class="twelve-columns-mobile six-columns-tablet">' +
//                '        <label>Groups  </label>' +
//                '        <div class="full-width">' +
//                '            <input name="prompt-value"  id="txt_GroupName"  class="input full-width"  type="text">' +
//                '        </div></div>' +
//                '</div>' +

//                '<div class="standard-tabs margin-bottom" id="add-tabs">' +
//               '<div class="respTable">' +
//                 '<table class="table responsive-table" id="sorting-advanced">                                                                                                   ' +
//                    ' <thead>                                                                                                                                                    ' +
//                        ' <tr>                                                                                                                                                   ' +
//                         '    <th scope="col">MarkUp Percentage </th>                                                                                                            ' +
//                            ' <th scope="col" class="align-center hide-on-mobile">MarkUp Amount</th>                                                                            ' +
//                            ' <th scope="col" class="align-center hide-on-mobile-portrait">Commision Percentage </th>                                                           ' +
//                            ' <th scope="col" class="align-center">Commision Amount</th>                                                                                        ' +
//                        ' </tr>                                                                                                                                                  ' +
//                   '  </thead>                                                                                                                                                   ' +
//                    ' <tbody>                                                                                                                                                    ' +
//                    '     <tr>                                                                                                                                                   ' +
//                    '         <td>                                                                                                                                               ' +
//                    '             <div class="input full-width">                                                                                                                 ' +
//                    '                 <input name="prompt-value" id="txt_MarkPercentage" value="" class="input-unstyled full-width" placeholder="0%" type="text"></div>        ' +
//                    '         </td>                                                                                                                                              ' +
//                    '         <td>                                                                                                                                               ' +
//                    '             <div class="input full-width">                                                                                                                 ' +
//                    '                 <input name="prompt-value" id="txt_MarkUpAmountt" value="" class="input-unstyled full-width" placeholder="0%" type="text"></div>            ' +
//                    '         </td>                                                                                                                                              ' +
//                    '         <td>                                                                                                                                               ' +
//                    '             <div class="input full-width">                                                                                                                 ' +
//                    '                 <input name="prompt-value" id="txt_CommPercentage" value="" class="input-unstyled full-width" placeholder="0%" type="text"></div>     ' +
//                    '         </td>                                                                                                                                              ' +
//                    '         <td>                                                                                                                                               ' +
//                    '             <div class="input full-width">                                                                                                                 ' +
//                    '                 <input name="prompt-value" id="txt_CommAmount" value="" class="input-unstyled full-width" placeholder="0%" type="text"></div>         ' +
//                    '         </td>                                                                                                                                              ' +
//                    '     </tr>                                                                                                                                                  ' +
//                    ' </tbody>                                                                                                                                                   ' +
//                    '</table>' +
//                   ' <div style="float:right;margin-top:6%">' +
//                    ' <button type="button" onclick="AddGroupDetails()" class="button anthracite-gradient UpdateMarkup">Add</button></div>' +
//                  '</div>' +
//                  '</div>'
//            ,

//        title: 'Add Group',
//        width: 450,
//        scrolling: true,
//        actions: {
//            'Close': {
//                color: 'red',
//                click: function (win) { win.closeModal(); }
//            }
//        },
//        buttons: {
//            //'Close': {
//            //    classes: 'huge anthracite-gradient glossy full-width',
//            //    click: function (win) { win.closeModal(); }
//            //}
//        },
//        buttonsLowPadding: true
//    });
//}

//function AddGroupDetails() {
//    var GroupName = $("#txt_GroupName").val();
//    if (GroupName == "") {
//        Success("Please enter Group Name");
//        return false;
//    }
//    var MarkPercentage = $("#txt_MarkPercentage").val();
//    if (MarkPercentage == "") {
//        Success("Please enter Markup Percentage");
//        return false;
//    }
//    var MarkUpAmount = $("#txt_MarkUpAmountt").val();
//    if (MarkUpAmount == "") {
//        Success("Please enter MarkUp Amount");
//        return false;
//    }
//    var CommPercentage = $("#txt_CommPercentage").val();
//    if (CommPercentage == "") {
//        Success("Please enter Commission Percentage");
//        return false;
//    }
//    var CommAmount = $("#txt_CommAmount").val();
//    if (CommAmount == "") {
//        Success("Please enter Commission Amount");
//        return false;
//    }

//    var data = {
//        GroupName: GroupName,
//        MarkPercentage: MarkPercentage,
//        MarkUpAmount: MarkUpAmount,
//        CommPercentage: CommPercentage,
//        CommAmount: CommAmount,
//    }

//    $.ajax({
//        type: "POST",
//        url: "../handler/MarkUpHandler.asmx/AddGroupDetails",
//        data: JSON.stringify(data),
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        success: function (response) {
//            var result = JSON.parse(response.d)
//            if (result.retCode == 1) {
//                Success("Group details added successfully.");
//                setTimeout(function () {
//                    window.location.reload();
//                }, 2000);
//            }
//            else if (result.retCode == 0) {
//                Success("Something went wrong.");
//                setTimeout(function () {
//                    window.location.reload();
//                }, 2000);
//            }
//        },
//        error: function () {
//        }

//    });
//}

//function Update() {

//    var GroupId = $("#SelGroup").val();
//    if (GroupId == "") {
//        Success("Please Select Group");
//        return false;
//    }

//    var MarkPercentage = $("#txt_MarkUpPercentage").val();
//    if (MarkPercentage == "") {
//        Success("Please enter Markup Percentage");
//        return false;
//    }
//    var MarkUpAmount = $("#txt_MarkUpAmount").val();
//    if (MarkUpAmount == "") {
//        Success("Please enter MarkUp Amount");
//        return false;
//    }
//    var CommPercentage = $("#txt_CommisionPercentage").val();
//    if (CommPercentage == "") {
//        Success("Please enter Commission Percentage");
//        return false;
//    }
//    var CommAmount = $("#txt_CommisionAmount").val();
//    if (CommAmount == "") {
//        Success("Please enter Commission Amount");
//        return false;
//    }

//    var data = {
//        GroupId: GroupId,
//        MarkPercentage: MarkPercentage,
//        MarkUpAmount: MarkUpAmount,
//        CommPercentage: CommPercentage,
//        CommAmount: CommAmount,
//    }

//    $.ajax({
//        type: "POST",
//        url: "../handler/MarkUpHandler.asmx/Update",
//        data: JSON.stringify(data),
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        success: function (response) {
//            var result = JSON.parse(response.d)
//            if (result.retCode == 1) {
//                Success("Group Detail Update successfully.");
//                setTimeout(function () {
//                    window.location.reload();
//                }, 2000);
//            }
//            else if (result.retCode == 0) {
//                Success("Something went wrong.");
//                setTimeout(function () {
//                    window.location.reload();
//                }, 2000);
//            }
//        },
//        error: function () {
//        }
//    });
//}


var arrGlobalMarkup;
var hiddenId;
var bValid;
var DefaultGroup; var chkServicetax;
var GroupId;
var Groupsid = "";
var SupplierOtb = [], MrkUpPrcntgOtb = [], MrkUpAmntOtb = [], CommisionPrcntgOtb = [], CommisionAmntOtb = [];
var SupplierFlight = [], MrkUpPrcntgFlight = [], MrkUpAmntFlight = [], CommisionPrcntgFlight = [], CommisionAmntFlight = [];
$(document).ready(function () {
    $("#alSuccess").css("display", "none");
    GetGroup();
    GetMarkupDetails(5, "1");
    GetFlightSupplier();
});
function GetGroup() {
    $.ajax({
        type: "POST",
        url: "../handler/GroupMarkUpHandler.asmx/GetGroup",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                var List = result.Arr;
                var div = "";
                for (var i = 0; i < List.length; i++) {
                    div += '<option value="' + List[i].sid + '" >' + List[i].GroupName + '</option>'
                }

                $("#SelGroup").append(div);

                // setTimeout(function () { $('#SelGroup option').filter(function () { return $.trim($(this).text()) == 'Group A'; }).attr('selected', true); }, 2000);

                $("#SelGroup").val('5');
                $("#DivGroupmark .select span")[0].textContent = "Select";
                //GetGroupMarkupDetails()
            }
            else if (result.retCode == 0) {

            }
        },
        error: function () {
        }

    });
}
//function GetGroup() {
//    debugger;
//    $("#lbl_Country").css("display", "none");
//    $("#tbl_MasterSupplier tbody tr").remove();
//    $.ajax({
//        type: "POST",
//        url: "GroupMarkupHandler.asmx/GetGroupMarkup",
//        data: {},
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        success: function (response) {
//            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//            if (result.retCode == 1) {
//                arrGlobalMarkup = result.tbl_GroupMarkup;
//                $("#selGroup").empty();
//                var ddlRequest = '<option selected="selected" value="-">Select Any Group</option>';

//                if (arrGlobalMarkup.length > 0) {
//                    GroupId = arrGlobalMarkup[0].sid;
//                    DefaultGroup = arrGlobalMarkup[0].GroupName;
//                    for (i = 0; i < arrGlobalMarkup.length; i++) {
//                        ddlRequest += '<option value="' + arrGlobalMarkup[i].sid + '">' + arrGlobalMarkup[i].GroupName + '</option>';

//                    }
//                    $("#selGroup").append(ddlRequest);
//                    //$("#selGroup option:selected").text(DefaultGroup);
//                    //GetGroupMarkupDetails()
//                }
//                if (arrGlobalMarkup.length = 0) {
//                    tRow += '<tr>No Record Found </tr>'
//                    $("#tbl_MasterSupplier tbody").append(tRow);
//                }

//            }
//        },
//        error: function () {
//            alert('error')
//        }
//    });
//}
function AddGroup() {
    $("#lbl_GroupSid").text('');
    $("#txtGroupName").val('');
    $("#lbl_Update").text("AddGroupMarkupMarkup")
    //GetMarkup("lst_AddHotel")
    $("#tblHotel").css("display", "");
    $("#tblVisa").css("display", "none");
    $("#tblOtb").css("display", "none");
    $("#AddGroupModal").modal("show", "")

    /////// just for getting supplier number ////// 
    ShowMarkup(5, "1");
}

function ShowMarkup(GroupId, Type) {

    $.ajax({
        url: "GroupMarkupHandler.asmx/GroupMarkupByGroupId",
        type: "post",
        data: '{"GroupId":"' + GroupId + '","Type":"' + Type + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                aaGroupMarkupDetails = result.tbl_GroupMarkupDetails
                if (aaGroupMarkupDetails.length > 0) {
                    for (i = 0; i < aaGroupMarkupDetails.length; i++) {

                        var tr = '';
                        if (Type == "1") {
                            if (i == 0) {

                                tr += '<tr style="width: 100%; height: 100%" class="hotelSupplier">'
                                tr += ' <td style="width: 10%;"><span class="dark bold"></span><br />'
                                tr += '<label id="lblSupplier1' + i + '">' + aaGroupMarkupDetails[i].Supplier + '</label>'
                                tr += '<label style="color: red; margin-top: 3px; display: none" id="lblSid1' + i + '">'
                                tr += '<b>' + aaGroupMarkupDetails[i].sid + '</b></label></td>'
                                tr += '<td style="width: 20%;"><span class="dark bold">MarkUp Percentage: </span>'
                                tr += '<input id="txtMrkUpPrcntg1' + i + '" class="form-control"  type="text" placeholder="0%" />'
                                tr += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txtMrkUpPrcntg1' + i + '">'
                                tr += '<b>* This field is required</b></label></td>'
                                tr += '<td style="width: 20%;"><span class="dark bold">MarkUp Amount:</span>'
                                tr += '<input id="txtMrkUpAmnt1' + i + '" class="form-control rupee"  type="text" placeholder="0" />'
                                tr += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txtMrkUpAmnt1' + i + '">'
                                tr += '<b>* This field is required</b></label></td>'
                                tr += '<td style="width: 20%;"><span class="dark bold">Commision Percentage: </span>'
                                tr += '<input id="txtCommisionPrcntg1' + i + '" class="form-control"  type="text" placeholder="0%" />'
                                tr += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txtCommisionPrcntg1' + i + '">'
                                tr += '<b>* This field is required</b></label></td>'
                                tr += '<td style="width: 20%;"><span class="dark bold"> Commision Amount:</span><input id="txtCommisionAmnt1' + i + '" class="form-control rupee" type="text"  placeholder="0">'
                                tr += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txtCommisionAmnt1' + i + '"> <b>* This field is required</b></label></td>'
                                tr += '</tr>'
                            }
                            else {
                                tr += '<tr style="width: 100%; height: 100%" class="hotelSupplier">'
                                tr += ' <td style="width: 10%;"><span class="dark bold"></span><br />'
                                tr += '<label id="lblSupplier1' + i + '">' + aaGroupMarkupDetails[i].Supplier + '</label>'
                                tr += '<label style="color: red; margin-top: 3px; display: none" id="lblSid1' + i + '">'
                                tr += '<b>' + aaGroupMarkupDetails[i].sid + '</b></label></td>'
                                tr += '<td style="width: 20%;">'
                                tr += '<input id="txtMrkUpPrcntg1' + i + '" class="form-control"  type="text" placeholder="0%" />'
                                tr += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txtMrkUpPrcntg1' + i + '">'
                                tr += '<b>* This field is required</b></label></td>'
                                tr += '<td style="width: 20%;">'
                                tr += '<input id="txtMrkUpAmnt1' + i + '" class="form-control rupee"  type="text" placeholder="0" />'
                                tr += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txtMrkUpAmnt1' + i + '">'
                                tr += '<b>* This field is required</b></label></td>'
                                tr += '<td style="width: 20%;">'
                                tr += '<input id="txtCommisionPrcntg1' + i + '" class="form-control"  type="text" placeholder="0%" />'
                                tr += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txtCommisionPrcntg1' + i + '">'
                                tr += '<b>* This field is required</b></label></td>'
                                tr += '<td style="width: 20%;"><input id="txtCommisionAmnt1' + i + '" class="form-control rupee" type="text"  placeholder="0">'
                                tr += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txtCommisionAmnt1' + i + '"> <b>* This field is required</b></label></td>'
                                tr += '</tr>'
                            }
                            $("#tblHotel tbody").append(tr);

                        }

                    }
                }
            }
            else if (result.retCode == 0) {
                Success("Something went wrong while processing your request! Please try again.");
            }
        },
        error: function () {
            Success('Error occured while processing your request! Please try again.');
        }
    });
}

function GetMarkup(id) {
    var Hotel = document.getElementById("lst_AddHotel");
    var Visa = document.getElementById("lst_AddVisa");
    var Otb = document.getElementById("lst_AddOtb");
    var Packages = document.getElementById("lst_AddPackages");
    var Activity = document.getElementById("lst_AddActivity");
    var Flight = document.getElementById("lst_AddFlight");
    Groupsid = $("#lbl_GroupSid").text();
    if (id == "lst_AddHotel") {
        Hotel.className = "";
        Visa.className = "active";
        Otb.className = "active";
        Packages.className = "active";
        Activity.className = "active";
        Flight.className = "active";
        $("#tblHotel").css("display", "");
        $("#tblVisa").css("display", "none");
        $("#tblOtb").css("display", "none");
        $("#tblPackages").css("display", "none");
        $("#tblActivity").css("display", "none");
        $("#lbl_Service").text("HotelGroupMarkup")
        $("#tblFlight").css("display", "none");
        if (Groupsid != "") {
            GetMarkupDetails(Groupsid, "1")
        }
    }
    else if (id == "lst_AddVisa") {
        Hotel.className = "active";
        Visa.className = "";
        Otb.className = "active";
        Packages.className = "active";
        Activity.className = "active";
        Flight.className = "active";
        $("#tblHotel").css("display", "none");
        $("#tblVisa").css("display", "");
        $("#tblOtb").css("display", "none");
        $("#tblPackages").css("display", "none");
        $("#tblActivity").css("display", "none");
        $("#lbl_Service").text("VisaGroupMarkup")
        $("#tblFlight").css("display", "none");
        //if (Groupsid != "") {
        //    GetMarkupDetails(Groupsid, "2")
        //}
        //if (Groupsid != "") {

        //    $("#lbl_Update").text("AddVisaGroupMarkup")
        //}
        //else {
        //    alert("Pleas Add HotelMarkup First");
        //    GetMarkup("lst_AddHotel")
        //}

    }
    else if (id == "lst_AddOtb") {
        Hotel.className = "active";
        Visa.className = "active";
        Otb.className = "";
        Packages.className = "active";
        Activity.className = "active";
        Flight.className = "active";
        $("#tblHotel").css("display", "none");
        $("#tblVisa").css("display", "none");
        $("#tblOtb").css("display", "");
        $("#tblPackages").css("display", "none");
        $("#tblActivity").css("display", "none");
        $("#lbl_Service").text("OtbGroupMarkup")
        $("#tblFlight").css("display", "none");
        if (Groupsid != "") {
            GetMarkupDetails(Groupsid, "3")
        }
        //if (Groupsid != "") {

        //    $("#lbl_Update").text("AddOtbGroupMarkup")
        //}
        //else {
        //    alert("Pleas Add HotelMarkup First");
        //    GetMarkup("lst_AddHotel")
        //}

    }
    else if (id == "lst_AddPackages") {
        Hotel.className = "active";
        Visa.className = "active";
        Otb.className = "active";
        Packages.className = "";
        Activity.className = "active";
        Flight.className = "active";
        $("#tblHotel").css("display", "none");
        $("#tblVisa").css("display", "none");
        $("#tblOtb").css("display", "none");
        $("#tblPackages").css("display", "");
        $("#tblActivity").css("display", "none");
        $("#lbl_Service").text("PackageGroupMarkup")
        $("#tblFlight").css("display", "none");
        if (Groupsid != "") {
            GetMarkupDetails(Groupsid, "4")
        }
    }

    else if (id == "lst_AddActivity") {
        Hotel.className = "active";
        Visa.className = "active";
        Otb.className = "active";
        Packages.className = "active";
        Activity.className = "";
        Flight.className = "active";
        $("#tblHotel").css("display", "none");
        $("#tblVisa").css("display", "none");
        $("#tblOtb").css("display", "none");
        $("#tblPackages").css("display", "none");
        $("#tblActivity").css("display", "");
        $("#lbl_Service").text("ActivityGroupMarkup")
        $("#tblFlight").css("display", "none");
        if (Groupsid != "") {
            GetMarkupDetails(Groupsid, "5")
        }
    }

    else if (id == "lst_AddFlight") {
        Hotel.className = "active";
        Visa.className = "active";
        Otb.className = "active";
        Packages.className = "active";
        Flight.className = "";
        $("#tblHotel").css("display", "none");
        $("#tblVisa").css("display", "none");
        $("#tblOtb").css("display", "none");
        $("#lbl_Service").text("FlightGroupMarkup")
        $("#tblPackages").css("display", "none");
        $("#tblFlight").css("display", "");
        if (Groupsid != "") {
            GetMarkupDetails(Groupsid, "7")
        }
    }
}
function GetGlobalMarkup(id) {
    debugger;
    $("#alSuccess").css("display", "none");
    $("#selSupplier").empty();
    $("#selGroupSupplier").empty();
    $("#selVisaSupplier").empty();
    $("#selOtbSupplier").empty();
    $.ajax({
        type: "POST",
        url: "GroupMarkupHandler.asmx/GetGlobalMarkup",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrGlobalMarkup = result.tbl_GlobalMarkup;
                var tRow = '';
                var ddlRequest = '<option selected="selected" value="-">Select Any Supplier</option>';
                var ddlVisaRequest = '<option selected="selected" value="-">Select Any Supplier</option>';
                var ddlOtbRequest = '<option selected="selected" value="-">Select Any Supplier</option>';
                if (arrGlobalMarkup.length > 0) {
                    for (i = 0; i < arrGlobalMarkup.length; i++) {
                        if (arrGlobalMarkup[i].Type == "1" && id == "1") {
                            ddlRequest += '<option value="' + arrGlobalMarkup[i].Supplier + '">' + arrGlobalMarkup[i].Supplier + '</option>';
                        }
                        else if (arrGlobalMarkup[i].Type == "2" && id == "2") {
                            ddlVisaRequest += '<option value="' + arrGlobalMarkup[i].Supplier + '">' + arrGlobalMarkup[i].Supplier + '</option>';
                        }
                        else if (arrGlobalMarkup[i].Type == "3" && id == "3") {
                            ddlOtbRequest += '<option value="' + arrGlobalMarkup[i].Supplier + '">' + arrGlobalMarkup[i].Supplier + '</option>';
                        }

                    }
                    $("#selGroupSupplier").append(ddlRequest);
                    $("#selVisaSupplier").append(ddlVisaRequest);
                    $("#selOtbSupplier").append(ddlOtbRequest);
                }
            }
        },
        error: function () {
            alert('error')
        }
    });
}
function GetGroupNem() {
    debugger;
    var GetgName = $("#txtGroupName").val();
    $.ajax({
        type: "POST",
        url: "GroupMarkupHandler.asmx/GetGuropName",
        data: '{"GetgName":"' + GetgName + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrGetgnameArr = result.GetgnameArr;
                for (i = 0; i < arrGetgnameArr.length; i++) {
                    var ReName = arrGetgnameArr[i].GroupName
                    if (ReName == GetgName) {
                        alert('Group Name Already Exist !');
                        // $("#alSuccess").css("display", "");
                        $("#txtGroupName").focus();
                        // $("#lbl_GroupSid").text(arrGetgnameArr[i].sid);
                    }
                }
            }

        },
    });

}

function GetMarkupDetails(GroupId, Type) {
    //if ($("#selGroup").val() != "-") {
    $("#tblhotels tbody").empty();
    var tr = '';
    $.ajax({
        url: "../Handler/GroupMarkupHandler.asmx/GroupMarkupByGroupId",
        type: "post",
        data: '{"GroupId":"' + GroupId + '","Type":"' + Type + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                aaGroupMarkupDetails = result.Arr
                var tr = '';
                if (aaGroupMarkupDetails.length > 0) {
                    for (i = 0; i < aaGroupMarkupDetails.length; i++) {
                       
                        if (Type == "1") {
                            //if (i == 0) {
                            //    tr += '<tr>                                                                                                                                        '
                            //    tr += '        <td><label id="lblSupplier' + i + '">' + aaGroupMarkupDetails[i].Supplier + '</label>         '                                          
                            //    tr += '<label style="color: red; margin-top: 3px; display: none" id="lblSid' + i + '">'
                            //    tr += '<b>' + aaGroupMarkupDetails[i].sid + '</b></label>'
                            //    tr += '        <td>                                                                                                                                '
                            //    tr += '            <div class="input full-width">                                                                                                  '
                            //    tr += '                <input name="prompt-value" id="txtMrkUpPrcntg' + i + '" value="' + aaGroupMarkupDetails[i].MarkupPercentage + '" class="input-unstyled full-width" placeholder="0%" type="text"></div> '
                            //    tr += '        </td>                                                                                                                               '
                            //    tr += '        <td>                                                                                                                                '
                            //    tr += '            <div class="input full-width">                                                                                                  '
                            //    tr += '                <input name="prompt-value" id="txtMrkUpAmnt' + i + '"  value="' + aaGroupMarkupDetails[i].MarkupAmmount + '" class="input-unstyled full-width" placeholder="0%" type="text"></div> '
                            //    tr += '        </td>                                                                                                                               '
                            //    tr += '        <td>                                                                                                                                '
                            //    tr += '            <div class="input full-width">                                                                                                  '
                            //    tr += '                <input name="prompt-value"  id="txtCommisionPrcntg' + i + '" class="form-control" value="' + aaGroupMarkupDetails[i].CommessionPercentage + '" class="input-unstyled full-width" placeholder="0%" type="text"></div> '
                            //    tr += '        </td>                                                                                                                               '
                            //    tr += '        <td>                                                                                                                                '
                            //    tr += '            <div class="input full-width">                                                                                                  '
                            //    tr += '                <input name="prompt-value" id="txtCommisionAmnt' + i + '"  value="' + aaGroupMarkupDetails[i].CommessionAmmount + '" class="input-unstyled full-width" placeholder="0%" type="text"></div> '
                            //    tr += '        </td>'
                            //    tr += '    </tr>'
                            //    //tr += '<tr style="width: 100%; height: 100%" class="SupplierHotel">'
                            //    //tr += ' <td style="width: 10%;"><span class="dark bold"></span><br />'
                            //    //tr += '<label id="lblSupplier' + i + '">' + aaGroupMarkupDetails[i].Supplier + '</label>'
                            //    //tr += '<label style="color: red; margin-top: 3px; display: none" id="lblSid' + i + '">'
                            //    //tr += '<b>' + aaGroupMarkupDetails[i].sid + '</b></label></td>'
                            //    //tr += '<td style="width: 20%;"><span class="dark bold">MarkUp Percentage: </span>'
                            //    //tr += '<input id="txtMrkUpPrcntg' + i + '" class="form-control" value="' + aaGroupMarkupDetails[i].MarkupPercentage + '" type="text" placeholder="0%" />'
                            //    //tr += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txtMrkUpPrcntg' + i + '">'
                            //    //tr += '<b>* This field is required</b></label></td>'
                            //    //tr += '<td style="width: 20%;"><span class="dark bold">MarkUp Amount:</span>'
                            //    //tr += '<input id="txtMrkUpAmnt' + i + '" class="form-control rupee" value="' + aaGroupMarkupDetails[i].MarkupAmmount + '" type="text" placeholder="0" />'
                            //    //tr += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txtMrkUpAmnt' + i + '">'
                            //    //tr += '<b>* This field is required</b></label></td>'
                            //    //tr += '<td style="width: 20%;"><span class="dark bold">Commision Percentage: </span>'
                            //    //tr += '<input id="txtCommisionPrcntg' + i + '" class="form-control" value="' + aaGroupMarkupDetails[i].CommessionPercentage + '" type="text" placeholder="0%" />'
                            //    //tr += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txtCommisionPrcntg' + i + '">'
                            //    //tr += '<b>* This field is required</b></label></td>'
                            //    //tr += '<td style="width: 20%;"><span class="dark bold"> Commision Amount:</span><input id="txtCommisionAmnt' + i + '" class="form-control rupee" type="text" value="' + aaGroupMarkupDetails[i].CommessionAmmount + '" placeholder="0">'
                            //    //tr += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txtCommisionAmnt' + i + '"> <b>* This field is required</b></label></td>'
                            //    //tr += '</tr>'
                            //}
                            //else {
                                tr += '<tr>                                                                                                                                        '
                                tr += '        <td><label id="lblSupplier' + i + '">' + aaGroupMarkupDetails[i].Supplier + '</label>         '
                                tr += '<label style="color: red; margin-top: 3px; display: none" id="lblSid' + i + '">'
                                tr += '<b>' + aaGroupMarkupDetails[i].sid + '</b></label>'
                                tr += '        <td>                                                                                                                                '
                                tr += '            <div class="input full-width">                                                                                                  '
                                tr += '                <input name="prompt-value" id="txtMrkUpPrcntg' + i + '" value="' + aaGroupMarkupDetails[i].MarkupPercentage + '" class="input-unstyled full-width" placeholder="0%" type="text"></div> '
                                tr += '        </td>                                                                                                                               '
                                tr += '        <td>                                                                                                                                '
                                tr += '            <div class="input full-width">                                                                                                  '
                                tr += '                <input name="prompt-value" id="txtMrkUpAmnt' + i + '"  value="' + aaGroupMarkupDetails[i].MarkupAmmount + '" class="input-unstyled full-width" placeholder="0%" type="text"></div> '
                                tr += '        </td>                                                                                                                               '
                                tr += '        <td>                                                                                                                                '
                                tr += '            <div class="input full-width">                                                                                                  '
                                tr += '                <input name="prompt-value"  id="txtCommisionPrcntg' + i + '" class="form-control" value="' + aaGroupMarkupDetails[i].CommessionPercentage + '" class="input-unstyled full-width" placeholder="0%" type="text"></div> '
                                tr += '        </td>                                                                                                                               '
                                tr += '        <td>                                                                                                                                '
                                tr += '            <div class="input full-width">                                                                                                  '
                                tr += '                <input name="prompt-value" id="txtCommisionAmnt' + i + '"  value="' + aaGroupMarkupDetails[i].CommessionAmmount + '" class="input-unstyled full-width" placeholder="0%" type="text"></div> '
                                tr += '        </td>'
                                tr += '    </tr>'
                                //tr += '<tr style="width: 100%; height: 100%" class="SupplierHotel">'
                                //tr += ' <td style="width: 10%;"><span class="dark bold"></span><br />'
                                //tr += '<label id="lblSupplier' + i + '">' + aaGroupMarkupDetails[i].Supplier + '</label>'
                                //tr += '<label style="color: red; margin-top: 3px; display: none" id="lblSid' + i + '">'
                                //tr += '<b>' + aaGroupMarkupDetails[i].sid + '</b></label></td>'
                                //tr += '<td style="width: 20%;">'
                                //tr += '<input id="txtMrkUpPrcntg' + i + '" class="form-control" value="' + aaGroupMarkupDetails[i].MarkupPercentage + '" type="text" placeholder="0%" />'
                                //tr += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txtMrkUpPrcntg' + i + '">'
                                //tr += '<b>* This field is required</b></label></td>'
                                //tr += '<td style="width: 20%;">'
                                //tr += '<input id="txtMrkUpAmnt' + i + '" class="form-control rupee" value="' + aaGroupMarkupDetails[i].MarkupAmmount + '" type="text" placeholder="0" />'
                                //tr += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txtMrkUpAmnt' + i + '">'
                                //tr += '<b>* This field is required</b></label></td>'
                                //tr += '<td style="width: 20%;">'
                                //tr += '<input id="txtCommisionPrcntg' + i + '" class="form-control" value="' + aaGroupMarkupDetails[i].CommessionPercentage + '" type="text" placeholder="0%" />'
                                //tr += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txtCommisionPrcntg' + i + '">'
                                //tr += '<b>* This field is required</b></label></td>'
                                //tr += '<td style="width: 20%;"><input id="txtCommisionAmnt' + i + '" class="form-control rupee" type="text" value="' + aaGroupMarkupDetails[i].CommessionAmmount + '" placeholder="0">'
                                //tr += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txtCommisionAmnt' + i + '"> <b>* This field is required</b></label></td>'
                                //tr += '</tr>'
                            //}
                            $("#tblhotels tbody").append(tr);
                            chkServicetax = $("#chkServicetax").val();
                        }
                            //************for VISA*************
                            //else if (Type == "2") {
                            //    if (aaGroupMarkupDetails[i].Supplier == "Dubai Visa (Normal)") {
                            //        $("#lblGmVisaNormal").text(aaGroupMarkupDetails[i].Supplier)
                            //        $("#lbl_lblVisaNormalsid").text(aaGroupMarkupDetails[i].sid);
                            //        $("#txtMrkUpPrcntgVisaNormal").val(aaGroupMarkupDetails[i].MarkupPercentage);
                            //        $("#txtMrkUpAmntVisaNormal").val(aaGroupMarkupDetails[i].MarkupAmmount);
                            //        $("#txtCommisionPrcntgVisaNormal").val(aaGroupMarkupDetails[i].CommessionPercentage);
                            //        $("#txtCommisionAmntVisaNormal").val(aaGroupMarkupDetails[i].CommessionAmmount)
                            //    }
                            //    else if (aaGroupMarkupDetails[i].Supplier == "Dubai Visa (Urgent)") {
                            //        $("#lblVisaUrgent").text(aaGroupMarkupDetails[i].Supplier)
                            //        $("#lbl_lblVisaUrgentsid").text(aaGroupMarkupDetails[i].sid);
                            //        $("#txtMrkUpPrcntgVisaUrgent").val(aaGroupMarkupDetails[i].MarkupPercentage);
                            //        $("#txtMrkUpAmntVisaUrgent").val(aaGroupMarkupDetails[i].MarkupAmmount);
                            //        $("#txtCommisionPrcntgVisaUrgent").val(aaGroupMarkupDetails[i].CommessionPercentage);
                            //        $("#txtCommisionAmntVisaUrgent").val(aaGroupMarkupDetails[i].CommessionAmmount)
                            //    }

                            //}
                            //************for OTB*************
                        else if (Type == "3") {
                            //$("#lblOtb").text(aaGroupMarkupDetails[i].Supplier)
                            //$("#lbl_lblOtbsid").text(aaGroupMarkupDetails[i].sid);
                            //$("#txtMrkUpPrcntgOtb").val(aaGroupMarkupDetails[i].MarkupPercentage);
                            //$("#txtMrkUpAmntOtb").val(aaGroupMarkupDetails[i].MarkupAmmount);
                            //$("#txtCommisionPrcntgOtb").val(aaGroupMarkupDetails[i].CommessionPercentage);
                            //$("#txtCommisionAmntOtb").val(aaGroupMarkupDetails[i].CommessionAmmount)
                            $("#lblAirlines0" + (i + 1)).text(aaGroupMarkupDetails[i].Supplier)
                            $("#lbl_Otbsid0" + (i + 1)).text(aaGroupMarkupDetails[i].sid);
                            $("#txtMrkUpPrcntgOtb0" + (i + 1)).val(aaGroupMarkupDetails[i].MarkupPercentage);
                            $("#txtMrkUpAmntOtb0" + (i + 1)).val(aaGroupMarkupDetails[i].MarkupAmmount);
                            $("#txtCommisionPrcntgOtb0" + (i + 1)).val(aaGroupMarkupDetails[i].CommessionPercentage);
                            $("#txtCommisionAmntOtb0" + (i + 1)).val(aaGroupMarkupDetails[i].CommessionAmmount)
                        }
                            //************for PACKAGE*************
                        else if (Type == "4") {
                            if (aaGroupMarkupDetails[i].Supplier == "Clickurtrip") {
                                $("#lblSupplier").text(aaGroupMarkupDetails[i].Supplier)
                                $("#lblSid").text(aaGroupMarkupDetails[i].sid);
                                $("#txtMrkUpPrcntg").val(aaGroupMarkupDetails[i].MarkupPercentage);
                                $("#txtMrkUpAmnt").val(aaGroupMarkupDetails[i].MarkupAmmount);
                                $("#txtCommisionPrcntg").val(aaGroupMarkupDetails[i].CommessionPercentage);
                                $("#txtCommisionAmnt").val(aaGroupMarkupDetails[i].CommessionAmmount)
                            }
                        }

                        else if (Type == "5") {
                            if (aaGroupMarkupDetails[i].Supplier == "Clickurtrip") {
                                $("#lblSupplierActivity").text(aaGroupMarkupDetails[i].Supplier)
                                $("#lblActivitySid").text(aaGroupMarkupDetails[i].sid);
                                $("#txtMrkUpPrcntgActivity").val(aaGroupMarkupDetails[i].MarkupPercentage);
                                $("#txtMrkUpAmntActivity").val(aaGroupMarkupDetails[i].MarkupAmmount);
                                $("#txtCommisionPrcntgActivity").val(aaGroupMarkupDetails[i].CommessionPercentage);
                                $("#txtCommisionAmntActivity").val(aaGroupMarkupDetails[i].CommessionAmmount)
                            }
                        }

                        else if (Type == "7") {
                            // $("#lbl_Update").text("UpdateFlight");
                            $("#lblFlightAirlines0" + (i + 1)).text(aaGroupMarkupDetails[i].Supplier)
                            $("#lbl_Flight0" + (i + 1)).text(aaGroupMarkupDetails[i].sid);
                            $("#txtMrkUpPrcntgFlight0" + (i + 1)).val(aaGroupMarkupDetails[i].MarkupPercentage);
                            $("#txtMrkUpAmntFlight0" + (i + 1)).val(aaGroupMarkupDetails[i].MarkupAmmount);
                            $("#txtCommisionPrcntgFlight0" + (i + 1)).val(aaGroupMarkupDetails[i].CommessionPercentage);
                            $("#txtCommisionAmntFlight0" + (i + 1)).val(aaGroupMarkupDetails[i].CommessionAmmount);


                        }
                    }
                }
            }
            else if (result.retCode == 0) {
                Success("Something went wrong while processing your request! Please try again.");
            }
        },
        error: function () {
            Success('Error occured while processing your request! Please try again.');
        }
    });
    // }


}
function GetVisaMarkup() {
    if ($("#selVisaCountry").val() != "-" && $("#selService").val() != "-" && $("#selProcessing").val() != "-") {
        var VisaSupplier = $("#selVisaCountry").val() + "-" + $("#selService").val() + "-" + $("#selProcessing").val()
        var GroupId = $("#lbl_GroupSid").text();
        $.ajax({
            type: "POST",
            url: "GroupMarkupHandler.asmx/GetVisaGroupMarkupMarkup",
            data: '{"Supplier":"' + VisaSupplier + '","GroupId":"' + GroupId + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    arrGlobalMarkup = result.tbl_VisaGlobalMarkup;
                    var tRow = '';
                    var ddlRequest = '<option selected="selected" value="-">Select Any Supplier</option>';
                    if (arrGlobalMarkup.length > 0) {
                        for (i = 0; i < arrGlobalMarkup.length; i++) {
                            $("#txtVisaMarkupAmmount").val(arrGlobalMarkup[0].MarkupAmmount)
                            $("#lbl_lblVisaNormalsid").text(arrGlobalMarkup[0].sid)

                        }
                    }

                }
            },
            error: function () {
                alert('error')
            }
        });

    }


}
function cleartextboxes() {
    $("#lbl_GroupSid").text('');
    $("#lbl_Update").text("AddHotelGroupMarkup")
    $("input[type='text']").val("");
    //GetMarkup("lst_AddHotel")
    $("#AddGroupModal").modal("hide", "");
    GetGroup()
}
function GetGroupMarkup(id) {
    $("#lbl_Country").css("display", "none");
    var Hotel = document.getElementById("lst_GroupMarkupHotel");
    var Visa = document.getElementById("lst_GroupMarkupVisa");
    var Otb = document.getElementById("lst_GroupMarkupOtb");
    var Packages = document.getElementById("lst_GroupMarkupPackage");
    var Activity = document.getElementById("lst_GroupMarkupActivity");
    var Flight = document.getElementById("lst_GroupMarkupFlight");
    chkServicetax.checked = false;
    if (id == "lst_GroupMarkupHotel") {
        if ($("#selGroup").val() != "-") {
            Hotel.className = "";
            Visa.className = "active";
            Otb.className = "active";
            Packages.className = "active";
            Activity.className = "active";
            Flight.className = "active";
            $("#tblGroupMarkupHotel").css("display", "");
            $("#tblGroupMarkupVisa").css("display", "none");
            $("#tblGroupMarkupOtb").css("display", "none");
            $("#tblGroupMarkupPackages").css("display", "none");
            $("#tblGroupMarkupActivity").css("display", "none");
            $("#lbl_GmUpdate").text("UpdateGroupMarkupHotel");
            $("#tblGroupMarkupFlight").css("display", "none");
            GetGroupMarkupDetails("1")
        }
        else {
            $("#selGroup").focus();
            $("#lbl_Country").css("display", "");
        }
    }
    else if (id == "lst_GroupMarkupVisa") {
        if ($("#selGroup").val() != "-") {
            Hotel.className = "active";
            Visa.className = "";
            Otb.className = "active";
            Packages.className = "active";
            Activity.className = "active";
            Flight.className = "active";
            $("#tblGroupMarkupHotel").css("display", "none");
            $("#tblGroupMarkupVisa").css("display", "");
            $("#tblGroupMarkupOtb").css("display", "none");
            $("#tblGroupMarkupPackages").css("display", "none");
            $("#tblGroupMarkupActivity").css("display", "none");
            $("#lbl_GmUpdate").text("UpdateGroupMarkupVisa");
            $("#tblGroupMarkupFlight").css("display", "none");
            ///GetGroupMarkupDetails("2")
        }
        else {
            $("#selGroup").focus();
            $("#lbl_Country").css("display", "");
        }

    }
    else if (id == "lst_GroupMarkupOtb") {
        if ($("#selGroup").val() != "-") {
            Hotel.className = "active";
            Visa.className = "active";
            Otb.className = "";
            Packages.className = "active";
            Activity.className = "active";
            Flight.className = "active";
            $("#tblGroupMarkupHotel").css("display", "none");
            $("#tblGroupMarkupVisa").css("display", "none");
            $("#tblGroupMarkupOtb").css("display", "");
            $("#tblGroupMarkupPackages").css("display", "none");
            $("#tblGroupMarkupActivity").css("display", "none");
            $("#lbl_GmUpdate").text("UpdateGroupMarkupOtb");
            $("#tblGroupMarkupFlight").css("display", "none");
            GetGroupMarkupDetails("3")
        }
        else {
            $("#selGroup").focus();
            $("#lbl_Country").css("display", "");
        }

    }
    else if (id == "lst_GroupMarkupPackage") {
        if ($("#selGroup").val() != "-") {
            Hotel.className = "active";
            Visa.className = "active";
            Otb.className = "active";
            Packages.className = "";
            Activity.className = "active";
            Flight.className = "active";
            $("#tblGroupMarkupHotel").css("display", "none");
            $("#tblGroupMarkupVisa").css("display", "none");
            $("#tblGroupMarkupOtb").css("display", "none");
            $("#tblGroupMarkupPackages").css("display", "");
            $("#tblGroupMarkupActivity").css("display", "none");
            $("#lbl_GmUpdate").text("UpdateGroupMarkupPackages");
            $("#tblGroupMarkupFlight").css("display", "none");
            GetGroupMarkupDetails("6")
        }
        else {
            $("#selGroup").focus();
            $("#lbl_Country").css("display", "");
        }

    }

    else if (id == "lst_GroupMarkupActivity") {
        if ($("#selGroup").val() != "-") {
            Hotel.className = "active";
            Visa.className = "active";
            Otb.className = "active";
            Packages.className = "active";
            Activity.className = "";
            Flight.className = "active";
            $("#tblGroupMarkupHotel").css("display", "none");
            $("#tblGroupMarkupVisa").css("display", "none");
            $("#tblGroupMarkupOtb").css("display", "none");
            $("#tblGroupMarkupPackages").css("display", "none");
            $("#tblGroupMarkupActivity").css("display", "");
            $("#lbl_GmUpdate").text("UpdateGroupMarkupActivity");
            $("#tblGroupMarkupFlight").css("display", "none");
            GetGroupMarkupDetails("5")
        }
        else {
            $("#selGroup").focus();
            $("#lbl_Country").css("display", "");
        }

    }

    else if (id == "lst_GroupMarkupFlight") {
        if ($("#selGroup").val() != "-") {
            Hotel.className = "active";
            Visa.className = "active";
            Otb.className = "active";
            Packages.className = "active";
            Flight.className = "";
            $("#tblGroupMarkupHotel").css("display", "none");
            $("#tblGroupMarkupVisa").css("display", "none");
            $("#tblGroupMarkupOtb").css("display", "none");
            $("#tblGroupMarkupPackages").css("display", "none");
            $("#lbl_GmUpdate").text("UpdateGroupMarkupFlight");
            $("#tblGroupMarkupFlight").css("display", "");
            GetGroupMarkupDetails("7");
        }
        else {
            $("#selGroup").focus();
            $("#lbl_Country").css("display", "");
        }
    }
}

var aaGroupMarkupDetails;
function GetGroupMarkupDetails(Type) {
    $("#tblGroupMarkupHotel tbody").empty();
    $("input[type='text']").val("");
    $("#lbl_Country").css("display", "none");
    GroupId = $("#selGroup").val();
    if ($("#selGroup").val() != "-") {
        $.ajax({
            url: "GroupMarkupHandler.asmx/GroupMarkupByGroupId",
            type: "post",
            data: '{"GroupId":"' + GroupId + '","Type":"' + Type + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                if (result.retCode == 1) {
                    aaGroupMarkupDetails = result.tbl_GroupMarkupDetails;
                    var tr = '';
                    if (aaGroupMarkupDetails.length > 0) {
                        for (i = 0; i < aaGroupMarkupDetails.length; i++) {
                            if (aaGroupMarkupDetails[i].Type == "1") {
                                /////////////////////////////////////////////////////////////////// Old logic /////////////////////////////
                                $("#alSuccess").css("display", "none");
                                $("#lbl_Service").text(aaGroupMarkupDetails[i].Type);
                                $("#lbl_Update").text("UpdateHotel");

                                //if (aaGroupMarkupDetails[i].Supplier == "HotelBeds")
                                //{
                                //    $("#lblGmHbds").text(aaGroupMarkupDetails[i].Supplier);
                                //    $("#lblGmHbdssid").text(aaGroupMarkupDetails[i].sid);
                                //    $("#txtGmMrkUpPrcntgHbds").val(aaGroupMarkupDetails[i].MarkupPercentage);
                                //    $("#txtGmMrkUpAmntHbds").val(aaGroupMarkupDetails[i].MarkupAmmount);
                                //    $("#txtGmCommisionPrcntgHbds").val(aaGroupMarkupDetails[i].CommessionPercentage);
                                //    $("#txtGmCommisionAmntHbds").val(aaGroupMarkupDetails[i].CommessionAmmount)
                                //    if (aaGroupMarkupDetails[i].TaxApplicable == "True") {
                                //        chkServicetax.checked = true;
                                //    }
                                //}
                                //else if (aaGroupMarkupDetails[i].Supplier == "Mgh") {

                                //    $("#lblGmMgh").text(aaGroupMarkupDetails[i].Supplier)
                                //    $("#lblGmMghsid").text(aaGroupMarkupDetails[i].sid);
                                //    $("#txtGmMrkUpPrcntgMgh").val(aaGroupMarkupDetails[i].MarkupPercentage);
                                //    $("#txtGmMrkUpAmntMgh").val(aaGroupMarkupDetails[i].MarkupAmmount);
                                //    $("#txtGmCommisionPrcntgMgh").val(aaGroupMarkupDetails[i].CommessionPercentage);
                                //    $("#txtGmCommisionAmntMgh").val(aaGroupMarkupDetails[i].CommessionAmmount)
                                //    if (aaGroupMarkupDetails[i].TaxApplicable == "True") {
                                //        chkServicetax.checked = true;
                                //    }
                                //}
                                //else if (aaGroupMarkupDetails[i].Supplier == "Dotw") {
                                //    $("#lblGmdotw").text(aaGroupMarkupDetails[i].Supplier);
                                //    $("#lblGmdotwsid").text(aaGroupMarkupDetails[i].sid);
                                //    $("#txtGmMrkUpPrcntgdotw").val(aaGroupMarkupDetails[i].MarkupPercentage);
                                //    $("#txtGmMrkUpAmntdotw").val(aaGroupMarkupDetails[i].MarkupAmmount);
                                //    $("#txtGmCommisionPrcntgdotw").val(aaGroupMarkupDetails[i].CommessionPercentage);
                                //    $("#txtGmCommisionAmntdotw").val(aaGroupMarkupDetails[i].CommessionAmmount)
                                //    if (aaGroupMarkupDetails[i].TaxApplicable == "True") {
                                //        chkServicetax.checked = true;
                                //    }
                                //}
                                //else if (aaGroupMarkupDetails[i].Supplier == "Expedia") {
                                //    $("#lblGmExpedia").text(aaGroupMarkupDetails[i].Supplier);
                                //    $("#lblGmExpediawsid").text(aaGroupMarkupDetails[i].sid);
                                //    $("#txtGmMrkUpPrcntgExpedia").val(aaGroupMarkupDetails[i].MarkupPercentage);
                                //    $("#txtGmMrkUpAmntExpedia").val(aaGroupMarkupDetails[i].MarkupAmmount);
                                //    $("#txtGmCommisionPrcntgExpedia").val(aaGroupMarkupDetails[i].CommessionPercentage);
                                //    $("#txtGmCommisionAmntExpedia").val(aaGroupMarkupDetails[i].CommessionAmmount)
                                //    if (aaGroupMarkupDetails[i].TaxApplicable == "True") {
                                //        chkServicetax.checked = true;
                                //    }
                                //}

                                /////////////////////////////////////////////////////////////

                                if (i == 0) {
                                    tr += ' <tr>'
                                    tr += '   <td colspan="1">Service Tax on Markup: '
                                    tr += '     </td>'
                                    tr += '     <td colspan="4">'
                                    tr += '         <input type="checkbox" id="chkServicetax" class="checkbox" />'
                                    tr += '     </td>'
                                    tr += ' </tr>'

                                    tr += '<tr style="width: 100%; height: 100%" class="SupplierHotel">'
                                    tr += ' <td style="width: 10%;"><span class="dark bold"></span><br />'
                                    tr += '<label id="lblSupplier' + i + '">' + aaGroupMarkupDetails[i].Supplier + '</label>'
                                    tr += '<label style="color: red; margin-top: 3px; display: none" id="lblSid' + i + '">'
                                    tr += '<b>' + aaGroupMarkupDetails[i].sid + '</b></label></td>'
                                    tr += '<td style="width: 20%;"><span class="dark bold">MarkUp Percentage: </span>'
                                    tr += '<input id="txtMrkUpPrcntg' + i + '" class="form-control" value="' + aaGroupMarkupDetails[i].MarkupPercentage + '" type="text" placeholder="0%" />'
                                    tr += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txtMrkUpPrcntg' + i + '">'
                                    tr += '<b>* This field is required</b></label></td>'
                                    tr += '<td style="width: 20%;"><span class="dark bold">MarkUp Amount:</span>'
                                    tr += '<input id="txtMrkUpAmnt' + i + '" class="form-control rupee" value="' + aaGroupMarkupDetails[i].MarkupAmmount + '" type="text" placeholder="0" />'
                                    tr += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txtMrkUpAmnt' + i + '">'
                                    tr += '<b>* This field is required</b></label></td>'
                                    tr += '<td style="width: 20%;"><span class="dark bold">Commision Percentage: </span>'
                                    tr += '<input id="txtCommisionPrcntg' + i + '" class="form-control" value="' + aaGroupMarkupDetails[i].CommessionPercentage + '" type="text" placeholder="0%" />'
                                    tr += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txtCommisionPrcntg' + i + '">'
                                    tr += '<b>* This field is required</b></label></td>'
                                    tr += '<td style="width: 20%;"><span class="dark bold"> Commision Amount:</span><input id="txtCommisionAmnt' + i + '" class="form-control rupee" type="text" value="' + aaGroupMarkupDetails[i].CommessionAmmount + '" placeholder="0">'
                                    tr += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txtCommisionAmnt' + i + '"> <b>* This field is required</b></label></td>'
                                    tr += '</tr>'

                                    if (aaGroupMarkupDetails[i].TaxApplicable == "True") {
                                        chkServicetax.checked = true;
                                    }
                                }
                                else {
                                    tr += '<tr style="width: 100%; height: 100%" class="SupplierHotel">'
                                    tr += ' <td style="width: 10%;"><span class="dark bold"></span><br />'
                                    tr += '<label id="lblSupplier' + i + '">' + aaGroupMarkupDetails[i].Supplier + '</label>'
                                    tr += '<label style="color: red; margin-top: 3px; display: none" id="lblSid' + i + '">'
                                    tr += '<b>' + aaGroupMarkupDetails[i].sid + '</b></label></td>'
                                    tr += '<td style="width: 20%;">'
                                    tr += '<input id="txtMrkUpPrcntg' + i + '" class="form-control" value="' + aaGroupMarkupDetails[i].MarkupPercentage + '" type="text" placeholder="0%" />'
                                    tr += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txtMrkUpPrcntg' + i + '">'
                                    tr += '<b>* This field is required</b></label></td>'
                                    tr += '<td style="width: 20%;">'
                                    tr += '<input id="txtMrkUpAmnt' + i + '" class="form-control rupee" value="' + aaGroupMarkupDetails[i].MarkupAmmount + '" type="text" placeholder="0" />'
                                    tr += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txtMrkUpAmnt' + i + '">'
                                    tr += '<b>* This field is required</b></label></td>'
                                    tr += '<td style="width: 20%;">'
                                    tr += '<input id="txtCommisionPrcntg' + i + '" class="form-control" value="' + aaGroupMarkupDetails[i].CommessionPercentage + '" type="text" placeholder="0%" />'
                                    tr += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txtCommisionPrcntg' + i + '">'
                                    tr += '<b>* This field is required</b></label></td>'
                                    tr += '<td style="width: 20%;"><input id="txtCommisionAmnt' + i + '" class="form-control rupee" type="text" value="' + aaGroupMarkupDetails[i].CommessionAmmount + '" placeholder="0">'
                                    tr += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txtCommisionAmnt' + i + '"> <b>* This field is required</b></label></td>'
                                    tr += '</tr>'
                                    if (aaGroupMarkupDetails[i].TaxApplicable == "True") {
                                        chkServicetax.checked = true;
                                    }
                                }

                            }

                                /////////  COMMENT ////////////
                                //else if (aaGroupMarkupDetails[i].Type == "2") {
                                //    if (aaGroupMarkupDetails[i].Supplier == "Dubai Visa (Normal)")
                                //    {
                                //        $("#lbl_Update").text("UpdateVisa");
                                //        $("#lblGmVisaNormal").text(aaGroupMarkupDetails[i].Supplier)
                                //        $("#lbl_VisaNormalsid").text(aaGroupMarkupDetails[i].sid);
                                //        $("#txtGmMrkUpPrcntgVisaNormal").val(aaGroupMarkupDetails[i].MarkupPercentage);
                                //        $("#txtGmMrkUpAmntVisaNormal").val(aaGroupMarkupDetails[i].MarkupAmmount);
                                //        $("#txtGmCommisionPrcntgVisaNormal").val(aaGroupMarkupDetails[i].CommessionPercentage);
                                //        $("#txtGmCommisionAmntVisaNormal").val(aaGroupMarkupDetails[i].CommessionAmmount)
                                //    }
                                //    else if (aaGroupMarkupDetails[i].Supplier == "Dubai Visa (Urgent)")
                                //    {
                                //        $("#lbl_Update").text("UpdateVisa");
                                //        $("#lblGmVisaUrgent").text(aaGroupMarkupDetails[i].Supplier)
                                //        $("#lbl_VisaUrgentsid").text(aaGroupMarkupDetails[i].sid);
                                //        $("#txtGmMrkUpPrcntgVisaUrgent").val(aaGroupMarkupDetails[i].MarkupPercentage);
                                //        $("#txtGmMrkUpAmntVisaUrgent").val(aaGroupMarkupDetails[i].MarkupAmmount);
                                //        $("#txtGmCommisionPrcntgVisaUrgent").val(aaGroupMarkupDetails[i].CommessionPercentage);
                                //        $("#txtGmCommisionAmntVisaUrgent").val(aaGroupMarkupDetails[i].CommessionAmmount)
                                //    }
                                //}
                                /////////// COMMENT END ////////

                            else if (aaGroupMarkupDetails[i].Type == "3") {
                                $("#lbl_Update").text("UpdateOtb");
                                //$("#lblGmOtb").text(aaGroupMarkupDetails[i].Supplier)
                                //$("#lbl_Otbsid").text(aaGroupMarkupDetails[i].sid);
                                //$("#txtGmMrkUpPrcntgOtb").val(aaGroupMarkupDetails[i].MarkupPercentage);
                                //$("#txtGmMrkUpAmntOtb").val(aaGroupMarkupDetails[i].MarkupAmmount);
                                //$("#txtGmCommisionPrcntgOtb").val(aaGroupMarkupDetails[i].CommessionPercentage);
                                //$("#txtGmCommisionAmntOtb").val(aaGroupMarkupDetails[i].CommessionAmmount)
                                $("#lblAirlines" + (i + 1)).text(aaGroupMarkupDetails[i].Supplier)
                                $("#lbl_Otbsid" + (i + 1)).text(aaGroupMarkupDetails[i].sid);
                                $("#txtMrkUpPrcntgOtb" + (i + 1)).val(aaGroupMarkupDetails[i].MarkupPercentage);
                                $("#txtMrkUpAmntOtb" + (i + 1)).val(aaGroupMarkupDetails[i].MarkupAmmount);
                                $("#txtCommisionPrcntgOtb" + (i + 1)).val(aaGroupMarkupDetails[i].CommessionPercentage);
                                $("#txtCommisionAmntOtb" + (i + 1)).val(aaGroupMarkupDetails[i].CommessionAmmount)

                            }

                            else if (aaGroupMarkupDetails[i].Type == "6") {
                                $("#lbl_Update").text("UpdatePackage");
                                if (aaGroupMarkupDetails[i].Supplier == "ClickUrTrip") {
                                    $("#lblGmCUT").text(aaGroupMarkupDetails[i].Supplier)
                                    $("#lblGmCUTssid").text(aaGroupMarkupDetails[i].sid);
                                    $("#txtGmMrkUpPrcntgCUT").val(aaGroupMarkupDetails[i].MarkupPercentage);
                                    $("#txtGmMrkUpAmntCUT").val(aaGroupMarkupDetails[i].MarkupAmmount);
                                    $("#txtGmCommisionPrcntgCUT").val(aaGroupMarkupDetails[i].CommessionPercentage);
                                    $("#txtGmCommisionAmntCUT").val(aaGroupMarkupDetails[i].CommessionAmmount)
                                }
                            }

                            else if (aaGroupMarkupDetails[i].Type == "5") {
                                $("#lbl_Update").text("UpdateActivity");
                                if (aaGroupMarkupDetails[i].Supplier == "ClickUrTrip") {
                                    $("#lblGmActivity").text(aaGroupMarkupDetails[i].Supplier)
                                    $("#lblGmActivityssid").text(aaGroupMarkupDetails[i].sid);
                                    $("#txtGmMrkUpPrcntgActivity").val(aaGroupMarkupDetails[i].MarkupPercentage);
                                    $("#txtGmMrkUpAmntActivity").val(aaGroupMarkupDetails[i].MarkupAmmount);
                                    $("#txtGmCommisionPrcntgActivity").val(aaGroupMarkupDetails[i].CommessionPercentage);
                                    $("#txtGmCommisionAmntActivity").val(aaGroupMarkupDetails[i].CommessionAmmount)
                                }
                            }

                            else if (Type == "7") {
                                $("#lbl_Update").text("UpdateFlight");
                                $("#lblFlightAirlines" + (i + 1)).text(aaGroupMarkupDetails[i].Supplier)
                                $("#lbl_Flight" + (i + 1)).text(aaGroupMarkupDetails[i].sid);
                                $("#txtMrkUpPrcntgFlight" + (i + 1)).val(aaGroupMarkupDetails[i].MarkupPercentage);
                                $("#txtMrkUpAmntFlight" + (i + 1)).val(aaGroupMarkupDetails[i].MarkupAmmount);
                                $("#txtCommisionPrcntgFlight" + (i + 1)).val(aaGroupMarkupDetails[i].CommessionPercentage);
                                $("#txtCommisionAmntFlight" + (i + 1)).val(aaGroupMarkupDetails[i].CommessionAmmount);


                            }
                        }
                        $("#tblGroupMarkupHotel tbody").append(tr);
                    }
                }
                else if (result.retCode == 0) {
                    Success("Something went wrong while processing your request! Please try again.");
                }
            },
            error: function () {
                Success('Error occured while processing your request! Please try again.');
            }
        });
    }


}
function GetVisaMarkupDetails() {
    if ($("#selGmProcessing").val() != "-" && $("#selGmService").val() != "-" && $("#selGmVisaCountry").val() != "-") {
        var VisaSupplier = $("#selGmVisaCountry").val() + "-" + $("#selGmService").val() + "-" + $("#selGmProcessing").val()
        var GroupId = $("#selGroup").val();
        $.ajax({
            type: "POST",
            url: "../Handler/GroupMarkupHandler.asmx/GetVisaGroupMarkupMarkup",
            data: '{"Supplier":"' + VisaSupplier + '","GroupId":"' + GroupId + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    arrGlobalMarkup = result.tbl_VisaGlobalMarkup;
                    var tRow = '';
                    var ddlRequest = '<option selected="selected" value="-">Select Any Supplier</option>';
                    if (arrGlobalMarkup.length > 0) {
                        for (i = 0; i < arrGlobalMarkup.length; i++) {
                            $("#txtGmVisaMarkupAmmount").val(arrGlobalMarkup[0].MarkupAmmount)
                            $("#txtGmMrkUpPrcntgVisaNormal").val(arrGlobalMarkup[0].MarkupPercentage)
                            $("#lbl_VisaNormalsid").text(arrGlobalMarkup[0].sid)

                        }
                    }

                }
            },
            error: function () {
                alert('error')
            }
        });

    }


}
function UpdateGroupMarkup() {
    $("#alSuccess").css("display", "none");
    var OtbSid = []; var FlightSid = [];
    GroupId = $("#selGroup").val();
    bValid = true;
    var reg = new RegExp('[0-9]$');


    if ($("#lbl_GmUpdate").text() == "UpdateGroupMarkupHotel") {
        var Sid = [], Supplier = [], MrkUpPrcntg = [], MrkUpAmnt = [], CommisionPrcntg = [], CommisionAmnt = [];
        var SupplierID;
        for (var i = 0; i < $(".SupplierHotel").length; i++) {
            Sid.push($("#lblSid" + i).text())
            Supplier.push($("#lblSupplier" + i).text())
            MrkUpPrcntg.push($("#txtMrkUpPrcntg" + i).val())
            MrkUpAmnt.push($("#txtMrkUpAmnt" + i).val())
            CommisionPrcntg.push($("#txtCommisionPrcntg" + i).val())
            CommisionAmnt.push($("#txtCommisionAmnt" + i).val())

            if (GroupId == "-") {
                Success('Please Select Group.');
                bValid = false;
            }

            if (MrkUpPrcntg[i] != "" && !reg.test(MrkUpPrcntg[i])) {
                bValid = false;
                $("#lbl_txtMrkUpPrcntg" + i).html("* Percent must be numeric.");
                $("#lbl_txtMrkUpPrcntg" + i).css("display", "");
            }
            if (MrkUpPrcntg[i] == "") {
                MrkUpPrcntg[i] = 0;
            }
            $("#lbl_txtMrkUpAmnt" + i).css("display", "none");
            if (MrkUpAmnt[i] != "" && !reg.test(MrkUpAmnt[i])) {
                bValid = false;
                $("#txtMrkUpAmnt" + i).html("* Percent must be numeric.");
                $("#lbl_txtMrkUpAmnt" + i).css("display", "");
            }
            if (MrkUpAmnt[i] == "") {
                MrkUpAmnt[i] = 0;
            }
            $("#lbl_txtCommisionPrcntg" + i).css("display", "none");
            if (CommisionPrcntg[i] != "" && !reg.test(CommisionPrcntg[i])) {
                bValid = false;
                $("#txtCommisionPrcntg" + i).html("* Percent must be numeric.");
                $("#lbl_txtCommisionPrcntg" + i).css("display", "");
            }
            if (CommisionPrcntg[i] == "") {
                CommisionPrcntg[i] = 0;
            }
            $("#lbl_txtCommisionAmnt" + i).css("display", "none");
            if (CommisionAmnt[i] != "" && !reg.test(CommisionAmnt[i])) {
                bValid = false;
                $("#txtCommisionAmnt" + i).html("* Percent must be numeric.");
                $("#lbl_txtCommisionAmnt" + i).css("display", "");
            }
            if (CommisionAmnt[i] == "") {
                CommisionAmnt[i] = 0;
            }
        }

        var Type = $("#lbl_Service").text();
        var TaxApplicable = false;
        if (chkServicetax.checked == true) {
            TaxApplicable = true;
        }
        $("#alSuccess1").css("display", "none");
        $("#alError1").css("display", "none")


        //// Old Logic ///////
        //      var SupplierHbds = $("#lblGmHbds").text();
        //var HbdsSid = $("#lblGmHbdssid").text();
        //var MrkUpPrcntgHbds = $("#txtGmMrkUpPrcntgHbds").val();
        //var MrkUpAmntHbds = $("#txtGmMrkUpAmntHbds").val();
        //var CommisionPrcntgHbds = $("#txtGmCommisionPrcntgHbds").val();
        //var CommisionAmntHbds = $("#txtGmCommisionAmntHbds").val();

        //var SupplierMgh = $("#lblGmMgh").text();
        //var MghSid = $("#lblGmMghsid").text();
        //var MrkUpPrcntgMgh = $("#txtGmMrkUpPrcntgMgh").val();
        //var MrkUpAmntMgh = $("#txtGmMrkUpAmntMgh").val();
        //var CommisionPrcntgMgh = $("#txtGmCommisionPrcntgMgh").val();
        //var CommisionAmntMgh = $("#txtGmCommisionAmntMgh").val();

        //var Supplierdotw = $("#lblGmdotw").text();
        //var lbldotwSid = $("#lblGmdotwsid").text();
        //var MrkUpPrcntgdotw = $("#txtGmMrkUpPrcntgdotw").val();
        //var MrkUpAmntdotw = $("#txtGmMrkUpAmntdotw").val();
        //var CommisionPrcntgdotw = $("#txtGmCommisionPrcntgdotw").val();
        //var CommisionAmntdotw = $("#txtGmCommisionAmntdotw").val();

        //// Expedia // 
        //var SupplierExpedia = $("#lblGmdotw").text();
        //var lblExpediaSid = $("#lblGmdotwsid").text();
        //var MrkUpPrcntgExpedia = $("#txtGmMrkUpPrcntgExpedia").val();
        //var MrkUpAmntExpedia = $("#txtGmMrkUpAmntExpedia").val();
        //var CommisionPrcntgExpedia = $("#txtGmCommisionPrcntgExpedia").val();
        //var CommisionAmntExpedia = $("#txtGmCommisionAmntExpedia").val();

        //var Type = $("#lbl_Service").text();
        //var TaxApplicable = false;
        //if (chkServicetax.checked == true)
        //{
        //    TaxApplicable = true;
        //}
        //$("#alSuccess1").css("display", "none");
        //$("#alError1").css("display", "none")
        //var bValid = true;

        //if (MrkUpPrcntgHbds == "") {
        //    MrkUpPrcntgHbds = 0;
        //}
        //if (MrkUpPrcntgHbds != "" && !reg.test(MrkUpPrcntgHbds)) {
        //    bValid = false;
        //    $("#lbl_txtGmMrkUpPrcntgHbds").html("* Percent must be numeric.");
        //    $("#lbl_txtGmMrkUpPrcntgHbds").css("display", "");
        //}
        //if (MrkUpPrcntgMgh == "")
        //{
        //    MrkUpPrcntgMgh = 0;
        //}
        //if (MrkUpPrcntgMgh != "" && !reg.test(MrkUpPrcntgMgh)) {
        //    bValid = false;
        //    $("#lbl_txtGmMrkUpPrcntgMgh").html("* Percent must be numeric.");
        //    $("#lbl_txtGmMrkUpPrcntgMgh").css("display", "");
        //}
        //if (MrkUpPrcntgdotw == "") {
        //    MrkUpPrcntgdotw = 0;
        //}
        //if (MrkUpPrcntgdotw != "" && !reg.test(MrkUpPrcntgdotw)) {
        //    bValid = false;
        //    $("#lbl_txtGmMrkUpPrcntgdotw").html("* Percent must be numeric.");
        //    $("#lbl_txtGmMrkUpPrcntgdotw").css("display", "");
        //}
        //if (MrkUpAmntHbds == "") {
        //    //bValid = false;
        //    //$("#lbl_txtMrkUpAmnt").css("display", "");
        //    MrkUpAmntHbds = 0;
        //}
        //if (MrkUpAmntHbds != "" && !reg.test(MrkUpAmntHbds)) {
        //    bValid = false;
        //    $("#lbl_txtGmMrkUpAmntHbds").html("* Amount must be numeric.");
        //    $("#lbl_txtGmMrkUpAmntHbds").css("display", "");
        //}
        //if (MrkUpAmntMgh == "") {
        //    //bValid = false;
        //    //$("#lbl_txtMrkUpAmnt").css("display", "");
        //    MrkUpAmntMgh = 0;
        //}
        //if (MrkUpAmntMgh != "" && !reg.test(MrkUpAmntMgh)) {
        //    bValid = false;
        //    $("#lbl_txtGmMrkUpAmntMgh").html("* Amount must be numeric.");
        //    $("#lbl_txtGmMrkUpAmntMgh").css("display", "");
        //}
        //if (MrkUpAmntdotw == "") {
        //    //bValid = false;
        //    //$("#lbl_txtMrkUpAmnt").css("display", "");
        //    MrkUpAmntdotw = 0;
        //}
        //if (MrkUpAmntdotw != "" && !reg.test(MrkUpAmntdotw)) {
        //    bValid = false;
        //    $("#lbl_txtGmMrkUpAmntdotw").html("* Amount must be numeric.");
        //    $("#lbl_txtGmMrkUpAmntdotw").css("display", "");
        //}
        //if (CommisionPrcntgHbds == "") {
        //    //bValid = false;
        //    //$("#lbl_txtCommisionAmnt").css("display", "");
        //    CommisionPrcntgHbds = "0";
        //}
        //if (CommisionPrcntgHbds != "" && !reg.test(CommisionPrcntgHbds)) {
        //    bValid = false;
        //    $("#lbl_txtGmCommisionHbds").html("* Amount must be numeric.");
        //    $("#lbl_txtGmCommisionAmntHbds").css("display", "");
        //}
        //if (CommisionPrcntgMgh == "") {
        //    //bValid = false;
        //    //$("#lbl_txtCommisionAmnt").css("display", "");
        //    CommisionPrcntgMgh = "0";
        //}
        //if (CommisionPrcntgMgh != "" && !reg.test(CommisionPrcntgMgh)) {
        //    bValid = false;
        //    $("#lbl_txtGmCommisionMgh").html("* Amount must be numeric.");
        //    $("#lbl_txtGmCommisionAmntMgh").css("display", "");
        //}
        //if (CommisionPrcntgdotw == "") {
        //    //bValid = false;
        //    //$("#lbl_txtCommisionAmnt").css("display", "");
        //    CommisionPrcntgdotw = "0";
        //}
        //if (CommisionPrcntgdotw != "" && !reg.test(CommisionPrcntgdotw)) {
        //    bValid = false;
        //    $("#lbl_txtGmCommisionMgh").html("* Amount must be numeric.");
        //    $("#lbl_txtGmCommisionAmntMgh").css("display", "");
        //}
        //if (CommisionAmntHbds == "") {
        //    //bValid = false;
        //    //$("#lbl_txtCommisionAmnt").css("display", "");
        //    CommisionAmntHbds = "0";
        //}
        //if (CommisionAmntHbds != "" && !reg.test(CommisionAmntHbds)) {
        //    bValid = false;
        //    $("#lbl_txtGmCommisionAmntHbds").html("* Amount must be numeric.");
        //    $("#lbl_txtGmCommisionAmntHbds").css("display", "");
        //}
        //if (CommisionAmntMgh == "") {
        //    //bValid = false;
        //    //$("#lbl_txtCommisionAmnt").css("display", "");
        //    CommisionAmntMgh = "0";
        //}
        //if (CommisionAmntMgh != "" && !reg.test(CommisionAmntMgh)) {
        //    bValid = false;
        //    $("#lbl_txtGmCommisionAmntMgh").html("* Amount must be numeric.");
        //    $("#lbl_txtGmCommisionAmntMgh").css("display", "");
        //}
        //if (CommisionAmntdotw == "") {
        //    //bValid = false;
        //    //$("#lbl_txtCommisionAmnt").css("display", "");
        //    CommisionAmntdotw = "0";
        //}
        //if (CommisionAmntdotw != "" && !reg.test(CommisionAmntdotw)) {
        //    bValid = false;
        //    $("#lbl_txtGmCommisionAmntdotw").html("* Amount must be numeric.");
        //    $("#lbl_txtGmCommisionAmntdotw").css("display", "");
        //}

        //if (MrkUpPrcntgExpedia != "" && !reg.test(MrkUpPrcntgExpedia)) {
        //    bValid = false;
        //    $("#lbl_txtGmCommisionAmntExpedia").html("* Amount must be numeric.");
        //    $("#lbl_txtGmCommisionAmntExpedia").css("display", "");
        //}
        //if (MrkUpAmntExpedia != "" && !reg.test(MrkUpAmntExpedia)) {
        //    bValid = false;
        //    $("#lbl_txtGmMrkUpAmntExpedia").html("* Amount must be numeric.");
        //    $("#lbl_txtGmMrkUpAmntExpedia").css("display", "");
        //}
        //if (CommisionPrcntgExpedia != "" && !reg.test(CommisionPrcntgExpedia)) {
        //    bValid = false;
        //    $("#lbl_txtGmCommisionPrcntgExpedia").html("* Amount must be numeric.");
        //    $("#lbl_txtGmCommisionPrcntgExpedia").css("display", "");
        //}
        //if (CommisionAmntExpedia != "" && !reg.test(CommisionAmntExpedia)) {
        //    bValid = false;
        //    $("#lbl_txtGmCommisionAmntExpedia").html("* Amount must be numeric.");
        //    $("#lbl_txtGmCommisionAmntExpedia").css("display", "");
        //}

        // if (bValid == true) {
        //var dataToPass = {
        //    SupplierHbds: SupplierHbds,
        //    HbdsSid: HbdsSid,
        //    MrkUpPrcntgHbds: MrkUpPrcntgHbds,
        //    MrkUpAmntHbds: MrkUpAmntHbds,
        //    CommisionPrcntgHbds: CommisionPrcntgHbds,
        //    CommisionAmntHbds: CommisionAmntHbds,
        //    SupplierMgh: SupplierMgh,
        //    MghSid: MghSid,
        //    MrkUpPrcntgMgh: MrkUpPrcntgMgh,
        //    MrkUpAmntMgh: MrkUpAmntMgh,
        //    CommisionPrcntgMgh: CommisionPrcntgMgh,
        //    CommisionAmntMgh: CommisionAmntMgh,
        //    Supplierdotw: Supplierdotw,
        //    dotwSid: lbldotwSid,
        //    MrkUpPrcntgdotw: MrkUpPrcntgdotw,
        //    MrkUpAmntdotw: MrkUpAmntdotw,
        //    CommisionPrcntgdotw: CommisionPrcntgdotw,
        //    CommisionAmntdotw: CommisionAmntdotw,
        //    GroupId: GroupId,
        //    TaxApplicable: TaxApplicable
        //};
        //// End /////////

        if (bValid == true) {

            var dataToPass = {
                Supplier: Supplier,
                Sid: Sid,
                MrkUpPrcntg: MrkUpPrcntg,
                MrkUpAmnt: MrkUpAmnt,
                CommisionPrcntg: CommisionPrcntg,
                CommisionAmnt: CommisionAmnt,
                GroupId: GroupId,
                TaxApplicable: TaxApplicable
            };


            var jsonText = JSON.stringify(dataToPass);
            $.ajax({
                url: "../Handler/GroupMarkupHandler.asmx/UpdateHotelGroupMarkup",
                type: "post",
                data: jsonText,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        $("#alSuccess").css("display", "");
                        GetMarkupDetails(5, "1");
                    } else if (result.retCode == 0) {
                        $("#alError1").css("display", " ")
                    }
                },
                error: function () {
                    Success('Error occured while processing your request! Please try again.');
                }
            });

        }
    }
    else if ($("#lbl_GmUpdate").text() == "UpdateGroupMarkupVisa") {
        var VisaSupplier = $("#selGmVisaCountry").val() + "-" + $("#selGmService").val() + "-" + $("#selGmProcessing").val()
        var SupplierVisaNormal = VisaSupplier;
        var VisaNormalSid = $("#lbl_VisaNormalsid").text();
        var MrkUpPrcntgVisaNormal = $("#txtGmMrkUpPrcntgVisaNormal").val();;
        var MrkUpAmntVisaNormal = $("#txtGmVisaMarkupAmmount").val();
        var CommisionPrcntgVisaNormal = 0;
        var CommisionAmntVisaNormal = 0;
        if (MrkUpPrcntgVisaNormal == "") {
            MrkUpPrcntgVisaNormal = 0;
        }
        if (MrkUpPrcntgVisaNormal != "" && !reg.test(MrkUpPrcntgVisaNormal)) {
            bValid = false;
            $("#lbl_txtGmMrkUpPrcntgVisaNormal").html("* Percent must be numeric.");
            $("#lbl_txtGmMrkUpPrcntgVisaNormal").css("display", "");
        }
        if (MrkUpAmntVisaNormal == "") {
            MrkUpAmntVisaNormal = 0;
        }
        if (MrkUpAmntVisaNormal != "" && !reg.test(MrkUpAmntVisaNormal)) {
            bValid = false;
            $("#lbl_txtGmMrkUpAmntVisaNormal").html("* Percent must be numeric.");
            $("#lbl_txtGmMrkUpAmntVisaNormal").css("display", "");
        }
        if (CommisionPrcntgVisaNormal == "") {
            CommisionPrcntgVisaNormal = 0;
        }
        if (CommisionPrcntgVisaNormal != "" && !reg.test(CommisionPrcntgVisaNormal)) {
            bValid = false;
            $("#lbl_txtGmCommisionPrcntgVisaNormal").html("* Percent must be numeric.");
            $("#lbl_txtGmCommisionPrcntgVisaNormal").css("display", "");
        }
        if (CommisionAmntVisaNormal == "") {
            CommisionAmntVisaNormal = 0;
        }
        if (CommisionAmntVisaNormal != "" && !reg.test(CommisionAmntVisaNormal)) {
            bValid = false;
            $("#lbl_txtGmCommisionAmntVisaNormal").html("* Percent must be numeric.");
            $("#lbl_txtGmCommisionAmntVisaNormal").css("display", "");
        }
        var dataToPass = {
            SupplierVisaNormal: SupplierVisaNormal,
            VisaNormalSid: VisaNormalSid,
            MrkUpPrcntgVisaNormal: MrkUpPrcntgVisaNormal,
            MrkUpAmntVisaNormal: MrkUpAmntVisaNormal,
            CommisionPrcntgVisaNormal: CommisionPrcntgVisaNormal,
            CommisionAmntVisaNormal: CommisionAmntVisaNormal,
            GroupId: GroupId,
        };
        var jsonText = JSON.stringify(dataToPass);
        if (bValid == true) {
            $.ajax({
                url: "../Handler/GroupMarkupHandler.asmx/UpdateVisaGroupMarkup",
                type: "post",
                data: jsonText,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        $("#alSuccess").css("display", "");
                    } else if (result.retCode == 0) {
                        $("#alError1").css("display", " ")
                    }
                },
                error: function () {
                    Success('Error occured while processing your request! Please try again.');
                }
            });
        }

    }
    else if ($("#lbl_GmUpdate").text() == "UpdateGroupMarkupOtb") {
        for (var i = 0; i < 12; i++) {

            SupplierOtb[i] = $("#lblAirlines" + (i + 1)).text();
            OtbSid[i] = $("#lbl_Otbsid" + (i + 1)).text();
            MrkUpPrcntgOtb[i] = $("#txtMrkUpPrcntgOtb" + (i + 1)).val();;
            MrkUpAmntOtb[i] = $("#txtMrkUpAmntOtb" + (i + 1)).val();
            CommisionPrcntgOtb[i] = 0;
            CommisionAmntOtb[i] = 0;
            if (MrkUpPrcntgOtb[i] == "") {
                MrkUpPrcntgOtb[i] = 0;
            }
            if (MrkUpPrcntgOtb[i] != "" && !reg.test(MrkUpPrcntgOtb[i])) {
                bValid = false;
                $("#txtMrkUpPrcntgOtb" + (i + 1)).focus();
                $("#lbl_txtMrkUpPrcntgOtb" + (i + 1)).html("* Percent must be numeric.");
                $("#lbl_txtMrkUpPrcntgOtb" + (i + 1)).css("display", "");
            }
            if (MrkUpAmntOtb == "") {
                MrkUpAmntOtb = 0;
            }
            if (MrkUpAmntOtb[i] != "" && !reg.test(MrkUpAmntOtb[i])) {
                bValid = false;
                $("#txtMrkUpAmntOtb" + (i + 1)).focus();
                $("#lbl_txtMrkUpAmntOtb" + (i + 1)).html("* Percent must be numeric.");
                $("#lbl_txtMrkUpAmntOtb" + (i + 1)).css("display", "");
            }
            if (CommisionPrcntgOtb[i] == "") {
                CommisionPrcntgOtb[i] = 0;
            }
            if (CommisionPrcntgOtb[i] != "" && !reg.test(CommisionPrcntgOtb[i])) {
                bValid = false;
                $("#txtCommisionPrcntgOtb" + (i + 1)).focus();
                $("#lbl_txtCommisionPrcntgOtb" + (i + 1)).html("* Percent must be numeric.");
                $("#lbl_txtCommisionPrcntgOtb" + (i + 1)).css("display", "");
            }
            if (CommisionAmntOtb[i] == "") {
                CommisionAmntOtb[i] = 0;
            }
            if (CommisionAmntOtb[i] != "" && !reg.test(CommisionAmntOtb[i])) {
                bValid = false;
                $("#txtCommisionAmntOtb" + (i + 1)).focus();
                $("#lbl_txtCommisionAmntOtb").html("* Percent must be numeric.");
                $("#lbl_txtCommisionAmntOtb").css("display", "");
            }

        }

        var dataToPass = {
            SupplierOtb: SupplierOtb,
            OtbSid: OtbSid,
            MrkUpPrcntgOtb: MrkUpPrcntgOtb,
            MrkUpAmntOtb: MrkUpAmntOtb,
            CommisionPrcntgOtb: CommisionPrcntgOtb,
            CommisionAmntOtb: CommisionAmntOtb,
            GroupId: GroupId,

        };
        var jsonText = JSON.stringify(dataToPass);
        if (bValid == true) {
            $.ajax({
                url: "../Handler/GroupMarkupHandler.asmx/UpdateOtbGroupMarkup",
                type: "post",
                data: jsonText,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        $("#alSuccess").css("display", "");

                    } else if (result.retCode == 0) {
                        $("#alError").css("display", "")
                    }
                },
                error: function () {
                    Success('Error occured while processing your request! Please try again.');
                }
            });
        }

    }
    else if ($("#lbl_GmUpdate").text() == "UpdateGroupMarkupPackages") {
        var SupplierCUT = $("#lblGmCUT").text();
        var CUTsSid = $("#lblGmCUTssid").text();
        var MrkUpPrcntgCUT = $("#txtGmMrkUpPrcntgCUT").val();
        var MrkUpAmntCUT = $("#txtGmMrkUpAmntCUT").val();
        var CommisionPrcntgCUT = $("#txtGmCommisionPrcntgCUT").val();
        var CommisionAmntCUT = $("#txtGmCommisionAmntCUT").val();



        var Type = $("#lbl_Service").text();

        $("#alSuccess1").css("display", "none");
        $("#alError1").css("display", "none")
        var bValid = true;

        if (MrkUpPrcntgCUT == "") {
            MrkUpPrcntgCUT = 0;
        }
        if (MrkUpPrcntgCUT != "" && !reg.test(MrkUpPrcntgCUT)) {
            bValid = false;
            $("#lbl_txtGmMrkUpPrcntgHbds").html("* Percent must be numeric.");
            $("#lbl_txtGmMrkUpPrcntgHbds").css("display", "");
        }
        if (MrkUpAmntCUT == "") {
            MrkUpAmntCUT = 0;
        }
        if (MrkUpAmntCUT != "" && !reg.test(MrkUpAmntCUT)) {
            bValid = false;
            $("#lbl_txtGmMrkUpPrcntgMgh").html("* Percent must be numeric.");
            $("#lbl_txtGmMrkUpPrcntgMgh").css("display", "");
        }
        if (CommisionPrcntgCUT == "") {
            CommisionPrcntgCUT = 0;
        }
        if (CommisionPrcntgCUT != "" && !reg.test(CommisionPrcntgCUT)) {
            bValid = false;
            $("#lbl_txtGmMrkUpPrcntgdotw").html("* Percent must be numeric.");
            $("#lbl_txtGmMrkUpPrcntgdotw").css("display", "");
        }
        if (CommisionAmntCUT == "") {
            //bValid = false;
            //$("#lbl_txtMrkUpAmnt").css("display", "");
            CommisionAmntCUT = 0;
        }
        if (CommisionAmntCUT != "" && !reg.test(CommisionAmntCUT)) {
            bValid = false;
            $("#lbl_txtGmMrkUpAmntHbds").html("* Amount must be numeric.");
            $("#lbl_txtGmMrkUpAmntHbds").css("display", "");
        }

        if (bValid == true) {
            var dataToPass = {
                SupplierCUT: SupplierCUT,
                CUTsSid: CUTsSid,
                MrkUpPrcntgCUT: MrkUpPrcntgCUT,
                MrkUpAmntCUT: MrkUpAmntCUT,
                CommisionPrcntgCUT: CommisionPrcntgCUT,
                CommisionAmntCUT: CommisionAmntCUT,
                GroupId: GroupId
            };
            var jsonText = JSON.stringify(dataToPass);
            $.ajax({
                url: "../Handler/GroupMarkupHandler.asmx/UpdatePackageGroupMarkup",
                type: "post",
                data: jsonText,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        $("#alSuccess").css("display", "");
                    } else if (result.retCode == 0) {
                        $("#alError1").css("display", " ")
                    }
                },
                error: function () {
                    Success('Error occured while processing your request! Please try again.');
                }
            });

        }
    }


    else if ($("#lbl_GmUpdate").text() == "UpdateGroupMarkupActivity") {
        var SupplierActivity = $("#lblGmActivity").text();
        var ActivitysSid = $("#lblGmActivityssid").text();
        var MrkUpPrcntgActivity = $("#txtGmMrkUpPrcntgActivity").val();
        var MrkUpAmntActivity = $("#txtGmMrkUpAmntActivity").val();
        var CommisionPrcntgActivity = $("#txtGmCommisionPrcntgActivity").val();
        var CommisionAmntActivity = $("#txtGmCommisionAmntActivity").val();



        var Type = $("#lbl_Service").text();

        $("#alSuccess1").css("display", "none");
        $("#alError1").css("display", "none")
        var bValid = true;

        if (MrkUpPrcntgActivity == "") {
            MrkUpPrcntgActivity = 0;
        }
        if (MrkUpPrcntgActivity != "" && !reg.test(MrkUpPrcntgActivity)) {
            bValid = false;
            $("#lbl_txtGmMrkUpPrcntgHbds").html("* Percent must be numeric.");
            $("#lbl_txtGmMrkUpPrcntgHbds").css("display", "");
        }
        if (MrkUpAmntActivity == "") {
            MrkUpAmntActivity = 0;
        }
        if (MrkUpAmntActivity != "" && !reg.test(MrkUpAmntActivity)) {
            bValid = false;
            $("#lbl_txtGmMrkUpPrcntgMgh").html("* Percent must be numeric.");
            $("#lbl_txtGmMrkUpPrcntgMgh").css("display", "");
        }
        if (CommisionPrcntgActivity == "") {
            CommisionPrcntgActivity = 0;
        }
        if (CommisionPrcntgActivity != "" && !reg.test(CommisionPrcntgActivity)) {
            bValid = false;
            $("#lbl_txtGmMrkUpPrcntgdotw").html("* Percent must be numeric.");
            $("#lbl_txtGmMrkUpPrcntgdotw").css("display", "");
        }
        if (CommisionAmntActivity == "") {
            //bValid = false;
            //$("#lbl_txtMrkUpAmnt").css("display", "");
            CommisionAmntActivity = 0;
        }
        if (CommisionAmntActivity != "" && !reg.test(CommisionAmntActivity)) {
            bValid = false;
            $("#lbl_txtGmMrkUpAmntHbds").html("* Amount must be numeric.");
            $("#lbl_txtGmMrkUpAmntHbds").css("display", "");
        }

        if (bValid == true) {
            var dataToPass = {
                SupplierActivity: SupplierActivity,
                ActivitysSid: ActivitysSid,
                MrkUpPrcntgActivity: MrkUpPrcntgActivity,
                MrkUpAmntActivity: MrkUpAmntActivity,
                CommisionPrcntgActivity: CommisionPrcntgActivity,
                CommisionAmntActivity: CommisionAmntActivity,
                GroupId: GroupId
            };
            var jsonText = JSON.stringify(dataToPass);
            $.ajax({
                url: "../Handler/GroupMarkupHandler.asmx/UpdateActivityGroupMarkup",
                type: "post",
                data: jsonText,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        $("#alSuccess").css("display", "");
                    } else if (result.retCode == 0) {
                        $("#alError1").css("display", " ")
                    }
                },
                error: function () {
                    Success('Error occured while processing your request! Please try again.');
                }
            });

        }
    }


    else if ($("#lbl_GmUpdate").text() == "UpdateGroupMarkupFlight") {

        var Type = $("#lbl_Service").text();

        $("#alSuccess1").css("display", "none");
        $("#alError1").css("display", "none")
        var bValid = true;

        for (var i = 0; i < 9; i++) {
            SupplierFlight[i] = $("#lblFlightAirlines" + (i + 1)).text();
            FlightSid[i] = $("#lbl_Flight" + (i + 1)).text();
            MrkUpPrcntgFlight[i] = $("#txtMrkUpPrcntgFlight" + (i + 1)).val();;
            MrkUpAmntFlight[i] = $("#txtMrkUpAmntFlight" + (i + 1)).val();
            CommisionPrcntgFlight[i] = $("#txtCommisionPrcntgFlight" + (i + 1)).val();
            CommisionAmntFlight[i] = $("#txtCommisionAmntFlight" + (i + 1)).val();
            if (MrkUpPrcntgFlight[i] == "") {
                MrkUpPrcntgFlight[i] = 0;
            }
            if (MrkUpPrcntgFlight[i] != "" && !reg.test(MrkUpPrcntgFlight)) {
                bValid = false;
                $("#lbl_txtMrkUpPrcntgFlight" + i).html("* Percent must be numeric.");
                $("#lbl_txtMrkUpPrcntgFlight" + i).css("display", "");
            }
            if (MrkUpAmntFlight[i] == "") {
                MrkUpAmntFlight[i] = 0;
            }
            if (MrkUpAmntFlight[i] != "" && !reg.test(MrkUpAmntFlight)) {
                bValid = false;
                $("#lbl_txtMrkUpAmntFlight" + i).html("* Percent must be numeric.");
                $("#lbl_txtMrkUpAmntFlight" + i).css("display", "");
            }
            if (CommisionPrcntgFlight[i] == "") {
                CommisionPrcntgFlight[i] = 0;
            }
            if (CommisionPrcntgFlight[i] != "" && !reg.test(CommisionPrcntgFlight[i])) {
                bValid = false;
                $("#txtCommisionPrcntgFlight").html("* Percent must be numeric.");
                $("#txtCommisionPrcntgFlight").css("display", "");
            }
            if (CommisionAmntFlight[i] == "") {
                CommisionAmntFlight[i] = 0;
            }
            if (CommisionAmntFlight[i] != "" && !reg.test(CommisionAmntFlight[i])) {
                bValid = false;
                $("#lbl_txtCommisionAmntFlight1").html("* Percent must be numeric.");
                $("#lbl_txtCommisionAmntFlight1").css("display", "");
            }

        }

        var dataToPass = {
            SupplierFlight: SupplierFlight,
            FlightSid: FlightSid,
            MrkUpPrcntgFlight: MrkUpPrcntgFlight,
            MrkUpAmntFlight: MrkUpAmntFlight,
            CommisionPrcntgFlight: CommisionPrcntgFlight,
            CommisionAmntFlight: CommisionAmntFlight,
            GroupId: GroupId
        };
        var jsonText = JSON.stringify(dataToPass);
        if (bValid == true) {
            $.ajax({
                url: "../Handler/GroupMarkupHandler.asmx/UpdateFlightGroupMarkup",
                type: "post",
                data: jsonText,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        $("#alSuccess").css("display", "");
                    } else if (result.retCode == 0) {
                        $("#alError1").css("display", " ")
                    }
                },
                error: function () {
                    alert('Error occured while processing your request! Please try again.');
                }
            });
        }

    }
}


function hidealert() {
    $("#alSuccess").css("display", "none");
    $("#alSuccess1").css("display", "none");
}
//************************* For flight Tab ******************************
function GetFlightSupplier() {
    $.ajax({
        type: "POST",
        url: "../Handler/../Handler/GroupMarkupHandler.asmx/GetFlightSupplier",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var Supplier = result.list;
                var tRow = ''; var ddlRequest = '';
                //  ddlRequest += '<option selected="selected" value="TOB_">Select Any Supplier</option>';
                if (Supplier.length > 0) {
                    for (i = 0; i < Supplier.length; i++) {
                        ddlRequest += '<option selected="" value="' + Supplier[i] + '" >' + Supplier[i] + '</option>';
                    }
                    $("#Supplier").append(ddlRequest);
                    $("#SupplierAdd").append(ddlRequest)
                }

            }
        },
        error: function () {
            alert('error')
        }
    });
}


function AddGroupMarkup() {

    var reg = new RegExp('[0-9]$');
    $("#alSuccess1").css("display", "none");
    bValid = true;
    if ($("#txtGroupName").val() == "") {
        $("#txtGroupName").focus();
        Success("Insert Group Name")
        return false;
    }
    else {
        if ($("#lbl_GroupSid").text() == "" && $("#lbl_Update").text("AddGroupMarkupMarkup")) {
            var HotelSupllier = [], HotelSid = [], HotelMarkupPer = [], HotelMarkupAmt = [], HotelCommPer = [], HotelCommAmt = [];
            var GroupName = $("#txtGroupName").val()
            var Type = $("#lbl_Service").text();
            $("#alSuccess1").css("display", "none");
            $("#alError1").css("display", "none")
            var bValid = true;
            for (var i = 0; i < $(".hotelSupplier").length; i++) {
                HotelSupllier.push($("#lblSupplier1" + i).text())
                HotelSid.push($("#lblSid1" + i).text())
                HotelMarkupPer.push($("#txtMrkUpPrcntg1" + i).val())
                HotelMarkupAmt.push($("#txtMrkUpAmnt1" + i).val())
                HotelCommPer.push($("#txtCommisionPrcntg1" + i).val())
                HotelCommAmt.push($("#txtCommisionAmnt1" + i).val())
                if (HotelMarkupPer[i] != "" && !reg.test(HotelMarkupPer[i])) {
                    bValid = false;
                    $("#lbl_txtMrkUpPrcntg1" + i).html("* Percent must be numeric.");
                    $("#lbl_txtMrkUpPrcntg1" + i).css("display", "");
                }
                if (HotelMarkupPer[i] == "") {
                    HotelMarkupPer[i] = 0;
                }
                if (HotelMarkupAmt[i] != "" && !reg.test(HotelMarkupAmt[i])) {
                    bValid = false;
                    $("#lbl_txtMrkUpAmnt1" + i).html("* Percent must be numeric.");
                    $("#lbl_txtMrkUpAmnt1" + i).css("display", "");
                }
                if (HotelMarkupAmt[i] == "") {
                    HotelMarkupAmt[i] = 0;
                }
                if (HotelCommPer[i] != "" && !reg.test(HotelCommPer[i])) {
                    bValid = false;
                    $("#lbl_txtCommisionPrcntg1" + i).html("* Percent must be numeric.");
                    $("#lbl_txtCommisionPrcntg1" + i).css("display", "");
                }
                if (HotelCommPer[i] == "") {
                    HotelCommPer[i] = 0;
                }
                if (HotelCommAmt[i] != "" && !reg.test(HotelCommAmt[i])) {
                    bValid = false;
                    $("#lbl_txtCommisionAmnt1" + i).html("* Percent must be numeric.");
                    $("#lbl_txtCommisionAmnt1" + i).css("display", "");
                }
                if (HotelCommAmt[i] == "") {
                    HotelCommAmt[i] = 0;
                }
            }
            var GroupId = $("#lbl_GroupSid").text();
            for (var i = 0; i < 12; i++) {
                SupplierOtb[i] = $("#lblAirlines0" + (i + 1)).text();
                MrkUpPrcntgOtb[i] = $("#txtMrkUpAmntOtb0" + (i + 1)).val();
                MrkUpAmntOtb[i] = $("#txtMrkUpAmntOtb0" + (i + 1)).val();
                CommisionPrcntgOtb[i] = 0;
                CommisionAmntOtb[i] = 0;
                if (MrkUpPrcntgOtb[i] == "") {
                    MrkUpPrcntgOtb[i] = 0;
                }
                if (MrkUpAmntOtb[i] == "") {
                    MrkUpAmntOtb[i] = 0;
                }
                if (MrkUpAmntOtb[i] != "" && !reg.test(MrkUpAmntOtb[i])) {
                    bValid = false;
                    $("#lbl_txtMrkUpAmntOtb0" + (i + 1)).html("* Percent must be numeric.");
                    $("#lbl_txtMrkUpAmntOtb0" + (i + 1)).css("display", "");
                }
            }


            var supplierNormal = $("#selVisaCountry").val() + "-" + $("#selService").val() + "-" + $("#selProcessing").val();
            var GroupId = $("#lbl_GroupSid").text();
            var MrkUpPrcntgVisaNormal = 0;
            var MrkUpAmntVisaNormal = $("#txtVisaMarkupAmmount").val();
            var CommisionPrcntgVisaNormal = 0;
            var CommisionAmntVisaNormal = 0;
            if (MrkUpAmntVisaNormal == "") {
                MrkUpAmntVisaNormal = 0;
            }
            if (MrkUpAmntVisaNormal != "" && !reg.test(MrkUpAmntVisaNormal)) {
                bValid = false;
                $("#lbl_txtMrkUpAmntVisaNormal").html("* Percent must be numeric.");
                $("#lbl_txtMrkUpAmntVisaNormal").css("display", "");
            }

            ////////////////////////////////

            var GroupId = $("#lbl_GroupSid").text();
            var PackageSupllier = $("#lblSupplier").text();
            var PackageSid = $("#lblSid").text();
            var PackageMarkupPer = $("#txtMrkUpPrcntg").val();
            var PackageMarkupAmt = $("#txtMrkUpAmnt").val();
            var PackageCommPer = $("#txtCommisionPrcntg").val();
            var PackageCommAmt = $("#txtCommisionAmnt").val();

            if (PackageMarkupPer != "" && !reg.test(PackageMarkupPer)) {
                bValid = false;
                $("#lbl_txtMrkUpPrcntg").html("* Percent must be numeric.");
                $("#lbl_txtMrkUpPrcntg").css("display", "");
            }
            if (PackageMarkupPer == "") {
                PackageMarkupPer = 0;
            }
            if (PackageMarkupAmt != "" && !reg.test(PackageMarkupAmt)) {
                bValid = false;
                $("#lbl_txtMrkUpAmnt").html("* Percent must be numeric.");
                $("#lbl_txtMrkUpAmnt").css("display", "");
            }
            if (PackageMarkupAmt == "") {
                PackageMarkupAmt = 0;
            }
            if (PackageCommPer != "" && !reg.test(PackageCommPer)) {
                bValid = false;
                $("#lbl_txtCommisionPrcntg").html("* Percent must be numeric.");
                $("#lbl_txtCommisionPrcntg").css("display", "");
            }
            if (PackageCommPer == "") {
                PackageCommPer = 0;
            }
            if (PackageCommAmt != "" && !reg.test(PackageCommAmt)) {
                bValid = false;
                $("#lbl_txtCommisionAmnt").html("* Percent must be numeric.");
                $("#lbl_txtCommisionAmnt").css("display", "");
            }
            if (PackageCommAmt == "") {
                PackageCommAmt = 0;
            }

            /////////////////////For Activity///////////////

            var GroupId = $("#lbl_GroupSid").text();
            var ActivitySupllier = $("#lblSupplierActivity").text();
            var ActivitySid = $("#lblActivitySid").text();
            var ActivityMarkupPer = $("#txtMrkUpPrcntgActivity").val();
            var ActivityMarkupAmt = $("#txtMrkUpAmntActivity").val();
            var ActivityCommPer = $("#txtCommisionPrcntgActivity").val();
            var ActivityCommAmt = $("#txtCommisionAmntActivity").val();

            if (ActivityMarkupPer != "" && !reg.test(ActivityMarkupPer)) {
                bValid = false;
                $("#lbl_txtMrkUpPrcntgActivity").html("* Percent must be numeric.");
                $("#lbl_txtMrkUpPrcntgActivity").css("display", "");
            }
            if (ActivityMarkupPer == "") {
                ActivityMarkupPer = 0;
            }
            if (ActivityMarkupAmt != "" && !reg.test(ActivityMarkupAmt)) {
                bValid = false;
                $("#lbl_txtMrkUpAmntActivity").html("* Percent must be numeric.");
                $("#lbl_txtMrkUpAmntActivity").css("display", "");
            }
            if (ActivityMarkupAmt == "") {
                ActivityMarkupAmt = 0;
            }
            if (ActivityCommPer != "" && !reg.test(ActivityCommPer)) {
                bValid = false;
                $("#lbl_txtCommisionPrcntgActivity").html("* Percent must be numeric.");
                $("#lbl_txtCommisionPrcntgActivity").css("display", "");
            }
            if (ActivityCommPer == "") {
                ActivityCommPer = 0;
            }
            if (ActivityCommAmt != "" && !reg.test(ActivityCommAmt)) {
                bValid = false;
                $("#lbl_txtCommisionAmntActivity").html("* Percent must be numeric.");
                $("#lbl_txtCommisionAmntActivity").css("display", "");
            }
            if (ActivityCommAmt == "") {
                ActivityCommAmt = 0;
            }

            /////////////////////////////////////////////////


            ///////////// For Flight ///////////
            var GroupId = $("#lbl_GroupSid").text();
            var SupplierName = $("#SupplierAdd").val();;
            for (var i = 0; i < 12; i++) {
                SupplierFlight[i] = SupplierName + "_" + $("#lblFlightAirlines0" + (i + 1)).text();
                MrkUpPrcntgFlight[i] = $("#txtMrkUpPrcntgFlight0" + (i + 1)).val();
                MrkUpAmntFlight[i] = $("#txtMrkUpAmntFlight0" + (i + 1)).val();
                CommisionPrcntgFlight[i] = $("#txtCommisionPrcntgFlight0" + (i + 1)).val();
                CommisionAmntFlight[i] = $("#txtCommisionAmntFlight0" + (i + 1)).val();
                if (MrkUpPrcntgFlight[i] == "") {
                    MrkUpPrcntgFlight[i] = 0;
                }
                if (MrkUpPrcntgFlight[i] != "" && !reg.test(MrkUpPrcntgFlight[i])) {
                    bValid = false;
                    $("#lbl_txtMrkUpPrcntgFlight0" + (i + 1)).html("* Percent must be numeric.");
                    $("#lbl_txtMrkUpPrcntgFlight0" + (i + 1)).css("display", "");
                }
                if (MrkUpAmntFlight[i] == "") {
                    MrkUpAmntFlight[i] = 0;
                }
                if (MrkUpAmntFlight[i] != "" && !reg.test(MrkUpAmntFlight[i])) {
                    bValid = false;
                    $("#lbl_txtMrkUpAmntFlight0" + (i + 1)).html("* Percent must be numeric.");
                    $("#lbl_txtMrkUpAmntFlight0" + (i + 1)).css("display", "");
                }
                if (CommisionPrcntgFlight[i] == "") {
                    CommisionPrcntgFlight[i] = 0;
                }
                if (CommisionPrcntgFlight[i] != "" && !reg.test(CommisionPrcntgFlight[i])) {
                    bValid = false;
                    $("#lbl_txtCommisionPrcntgFlight0" + (i + 1)).html("* Percent must be numeric.");
                    $("#lbl_txtCommisionPrcntgFlight0" + (i + 1)).css("display", "");
                }
                if (CommisionAmntFlight[i] == "") {
                    CommisionAmntFlight[i] = 0;
                }
                if (CommisionAmntFlight[i] != "" && !reg.test(CommisionAmntFlight[i])) {
                    bValid = false;
                    $("#lbl_txtCommisionAmntFlight0" + (i + 1)).html("* Percent must be numeric.");
                    $("#lbl_txtCommisionAmntFlight0" + (i + 1)).css("display", "");
                }
            }

            //////////// End Flight ///////////


            var dataToPass = {
                GroupName: GroupName,
                HotelSupllier: HotelSupllier,
                HotelMarkupPer: HotelMarkupPer,
                HotelMarkupAmt: HotelMarkupAmt,
                HotelCommPer: HotelCommPer,
                HotelCommAmt: HotelCommAmt,
                SupplierVisaNormal: supplierNormal,
                MrkUpPrcntgVisaNormal: MrkUpPrcntgVisaNormal,
                MrkUpAmntVisaNormal: MrkUpAmntVisaNormal,
                CommisionPrcntgVisaNormal: CommisionPrcntgVisaNormal,
                CommisionAmntVisaNormal: CommisionAmntVisaNormal,
                SupplierOtb: SupplierOtb,
                MrkUpPrcntgOtb: MrkUpPrcntgOtb,
                MrkUpAmntOtb: MrkUpAmntOtb,
                CommisionPrcntgOtb: CommisionPrcntgOtb,
                CommisionAmntOtb: CommisionAmntOtb,
                PackageSupllier: PackageSupllier,
                PackageMarkupPer: PackageMarkupPer,
                PackageMarkupAmt: PackageMarkupAmt,
                PackageCommPer: PackageCommPer,
                PackageCommAmt: PackageCommAmt,
                /////////For Activity//////
                GroupId: GroupId,
                ActivitySupllier: ActivitySupllier,
                ActivitySid: ActivitySid,
                ActivityMarkupPer: ActivityMarkupPer,
                ActivityMarkupAmt: ActivityMarkupAmt,
                ActivityCommPer: ActivityCommPer,
                ActivityCommAmt: ActivityCommAmt,

                ////// For Flight //////
                SupplierFlight: SupplierFlight,
                MrkUpPrcntgFlight: MrkUpPrcntgFlight,
                MrkUpAmntFlight: MrkUpAmntFlight,
                CommisionPrcntgFlight: CommisionPrcntgFlight,
                CommisionAmntFlight: CommisionAmntFlight,

            };
            var jsonText = JSON.stringify(dataToPass);
            if (bValid == true) {
                $.ajax({
                    //    url: "../Handler/GroupMarkupHandler.asmx/CreateGroupMarkup",
                    type: "post",
                    data: jsonText,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                        if (result.retCode == 1) {
                            $("#alSuccess1").css("display", "");
                            Groupsid = result.GroupSid;
                            $("#lbl_GroupSid").text(Groupsid);
                            //$("#lbl_Update").text("UpdateGroupMarkupMarkup")
                            //$("#btnUpdateGroup").value("Update Markup")
                            //GetMarkupDetails(Groupsid,"1")
                            cleartextboxes()
                            GetGroup();
                            //GetMarkup("lst_AddVisa")
                            //alert("Add Visa Markup")
                        } else if (result.retCode == 0) {
                            $("#alError1").css("display", " ")
                        }
                    },
                    error: function () {
                        Success('Error occured while processing your request! Please try again.');
                    }
                });

            }
        }
        if ($("#lbl_GroupSid").text() != "" && $("#lbl_Service").text() == "VisaGroupMarkup") {
            var SupplierVisaNormal = $("#lblVisaNormal").text();
            var GroupId = $("#lbl_GroupSid").text();
            var VisaNormalSid = $("#lbl_lblVisaNormalsid").text();
            var MrkUpPrcntgVisaNormal = 0;
            var MrkUpAmntVisaNormal = $("#txtVisaMarkupAmmount").val();
            var CommisionPrcntgVisaNormal = 0;
            var CommisionAmntVisaNormal = 0;
            if (MrkUpAmntVisaNormal == "") {
                MrkUpAmntVisaNormal = 0;
            }
            if (MrkUpAmntVisaNormal != "" && !reg.test(MrkUpAmntVisaNormal)) {
                bValid = false;
                $("#lbl_txtMrkUpAmntVisaNormal").html("* Percent must be numeric.");
                $("#lbl_txtMrkUpAmntVisaNormal").css("display", "");
            }
            var dataToPass = {
                SupplierVisaNormal: SupplierVisaNormal,
                VisaNormalSid: VisaNormalSid,
                MrkUpPrcntgVisaNormal: MrkUpPrcntgVisaNormal,
                MrkUpAmntVisaNormal: MrkUpAmntVisaNormal,
                CommisionPrcntgVisaNormal: CommisionPrcntgVisaNormal,
                CommisionAmntVisaNormal: CommisionAmntVisaNormal,
                GroupId: GroupId,
            };
            var jsonText = JSON.stringify(dataToPass);
            if (bValid == true) {
                $.ajax({
                    url: "../Handler/GroupMarkupHandler.asmx/UpdateVisaGroupMarkup",
                    type: "post",
                    data: jsonText,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                        if (result.retCode == 1) {
                            $("#alSuccess1").css("display", "");
                            GetGroup();
                            //GetMarkup("lst_AddOtb")
                            $("#alSuccess1").css("display", " ");
                        } else if (result.retCode == 0) {
                            $("#alError1").css("display", " ")
                        }
                    },
                    error: function () {
                        Success('Error occured while processing your request! Please try again.');
                    }
                });
            }
        }
        if ($("#lbl_GroupSid").text() != "" && $("#lbl_Service").text() == "OtbGroupMarkup" && $("#lbl_Service").text("UpdateGroupMarkupMarkup")) {
            var OtbSid;
            var GroupId = $("#lbl_GroupSid").text();
            for (var i = 0; i < 12; i++) {
                SupplierOtb[i] = $("#lblAirlines0" + (i + 1)).text();
                MrkUpPrcntgOtb[i] = $("#txtMrkUpPrcntgOtb0" + (i + 1)).val();
                MrkUpAmntOtb[i] = $("#txtMrkUpAmntOtb0" + (i + 1)).val();
                CommisionPrcntgOtb[i] = $("#txtCommisionPrcntgOtb0" + (i + 1)).val();
                CommisionAmntOtb[i] = $("#txtCommisionAmntOtb0" + (i + 1)).val();
                if (MrkUpPrcntgOtb[i] == "") {
                    MrkUpPrcntgOtb[i] = 0;
                }
                if (MrkUpPrcntgOtb[i] != "" && !reg.test(MrkUpPrcntgOtb[i])) {
                    bValid = false;
                    $("#lbl_txtMrkUpPrcntgOtb0" + (i + 1)).html("* Percent must be numeric.");
                    $("#lbl_txtMrkUpPrcntgOtb0" + (i + 1)).css("display", "");
                }
                if (MrkUpAmntOtb[i] == "") {
                    MrkUpAmntOtb[i] = 0;
                }
                if (MrkUpAmntOtb[i] != "" && !reg.test(MrkUpAmntOtb[i])) {
                    bValid = false;
                    $("#lbl_txtMrkUpAmntOtb0" + (i + 1)).html("* Percent must be numeric.");
                    $("#lbl_txtMrkUpAmntOtb0" + (i + 1)).css("display", "");
                }
                if (CommisionPrcntgOtb[i] == "") {
                    CommisionPrcntgOtb[i] = 0;
                }
                if (CommisionPrcntgOtb[i] != "" && !reg.test(CommisionPrcntgOtb[i])) {
                    bValid = false;
                    $("#lbl_txtCommisionPrcntgOtb0" + (i + 1)).html("* Percent must be numeric.");
                    $("#lbl_txtCommisionPrcntgOtb0" + (i + 1)).css("display", "");
                }
                if (CommisionAmntOtb[i] == "") {
                    CommisionAmntOtb[i] = 0;
                }
                if (CommisionAmntOtb[i] != "" && !reg.test(CommisionAmntOtb[i])) {
                    bValid = false;
                    $("#lbl_txtCommisionAmntOtb0" + (i + 1)).html("* Percent must be numeric.");
                    $("#lbl_txtCommisionAmntOtb0" + (i + 1)).css("display", "");
                }
            }
            var dataToPass = {
                SupplierOtb: SupplierOtb,
                OtbSid: OtbSid,
                MrkUpPrcntgOtb: MrkUpPrcntgOtb,
                MrkUpAmntOtb: MrkUpAmntOtb,
                CommisionPrcntgOtb: CommisionPrcntgOtb,
                CommisionAmntOtb: CommisionAmntOtb,
                GroupId: GroupId,

            };
            var jsonText = JSON.stringify(dataToPass);
            if (bValid == true) {
                $.ajax({
                    url: "../Handler/GroupMarkupHandler.asmx/UpdateOtbGroupMarkup",
                    type: "post",
                    data: jsonText,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                        if (result.retCode == 1) {
                            //$("#AddGroupModal").modal("hide", "")
                            GetGroup();
                            $("#alSuccess1").css("display", "");
                            GetMarkupDetails(GroupId);
                            $("#alSuccess1").css("display", "");
                            $("#alSuccess1").css("display", " ");
                        } else if (result.retCode == 0) {
                            $("#alError1").css("display", " ")
                        }
                    },
                    error: function () {
                        Success('Error occured while processing your request! Please try again.');
                    }
                });
            }
        }
        if ($("#lbl_GroupSid").text() != "" && $("#lbl_Service").text() == "HotelGroupMarkup" && $("#lbl_Service").text("UpdateGroupMarkupMarkup")) {
            var SupplierHbds = $("#lblHbds").text();
            var GroupId = $("#lbl_GroupSid").text();
            var HbdsSid = $("#lblHbdssid").text();
            var MrkUpPrcntgHbds = $("#txtMrkUpPrcntgHbds").val();
            var MrkUpAmntHbds = $("#txtMrkUpAmntHbds").val();
            var CommisionPrcntgHbds = $("#txtCommisionPrcntgHbds").val();
            var CommisionAmntHbds = $("#txtCommisionAmntHbds").val();
            var SupplierMgh = $("#lblMgh").text();
            var MghSid = $("#lblMghsid").text();
            var MrkUpPrcntgMgh = $("#txtMrkUpPrcntgMgh").val();
            var MrkUpAmntMgh = $("#txtMrkUpAmntMgh").val();
            var CommisionPrcntgMgh = $("#txtCommisionPrcntgMgh").val();
            var CommisionAmntMgh = $("#txtCommisionAmntMgh").val();
            var Supplierdotw = $("#lbldotw").text();
            var lbldotwSid = $("#lbldotwsid").text();
            var MrkUpPrcntgdotw = $("#txtMrkUpPrcntgdotw").val();
            var MrkUpAmntdotw = $("#txtMrkUpAmntdotw").val();
            var CommisionPrcntgdotw = $("#txtCommisionPrcntgdotw").val();
            var CommisionAmntdotw = $("#txtCommisionAmntdotw").val();
            var Type = $("#lbl_Service").text();
            $("#alSuccess1").css("display", "none");
            $("#alError1").css("display", "none")
            var bValid = true;

            if (MrkUpPrcntgHbds == "") {
                MrkUpPrcntgHbds = 0;
            }
            if (MrkUpPrcntgHbds != "" && !reg.test(MrkUpPrcntgHbds)) {
                bValid = false;
                $("#lbl_txtMrkUpPrcntgHbds").html("* Percent must be numeric.");
                $("#lbl_txtMrkUpPrcntgHbds").css("display", "");
            }
            if (MrkUpPrcntgMgh == "") {
                MrkUpPrcntgMgh = 0;
            }
            if (MrkUpPrcntgMgh != "" && !reg.test(MrkUpPrcntgMgh)) {
                bValid = false;
                $("#lbl_txtMrkUpPrcntgMgh").html("* Percent must be numeric.");
                $("#lbl_txtMrkUpPrcntgMgh").css("display", "");
            }
            if (MrkUpPrcntgdotw == "") {
                MrkUpPrcntgdotw = 0;
            }
            if (MrkUpPrcntgdotw != "" && !reg.test(MrkUpPrcntgdotw)) {
                bValid = false;
                $("#lbl_txtMrkUpPrcntgdotw").html("* Percent must be numeric.");
                $("#lbl_txtMrkUpPrcntgdotw").css("display", "");
            }
            if (MrkUpAmntHbds == "") {
                //bValid = false;
                //$("#lbl_txtMrkUpAmnt").css("display", "");
                MrkUpAmntHbds = 0;
            }
            if (MrkUpAmntHbds != "" && !reg.test(MrkUpAmntHbds)) {
                bValid = false;
                $("#lbl_txtMrkUpAmntHbds").html("* Amount must be numeric.");
                $("#lbl_txtMrkUpAmntHbds").css("display", "");
            }
            if (MrkUpAmntMgh == "") {
                //bValid = false;
                //$("#lbl_txtMrkUpAmnt").css("display", "");
                MrkUpAmntMgh = 0;
            }
            if (MrkUpAmntMgh != "" && !reg.test(MrkUpAmntMgh)) {
                bValid = false;
                $("#lbl_txtMrkUpAmntMgh").html("* Amount must be numeric.");
                $("#lbl_txtMrkUpAmntMgh").css("display", "");
            }
            if (MrkUpAmntdotw == "") {
                //bValid = false;
                //$("#lbl_txtMrkUpAmnt").css("display", "");
                MrkUpAmntdotw = 0;
            }
            if (MrkUpAmntdotw != "" && !reg.test(MrkUpAmntdotw)) {
                bValid = false;
                $("#lbl_txtMrkUpAmntdotw").html("* Amount must be numeric.");
                $("#lbl_txtMrkUpAmntdotw").css("display", "");
            }
            if (CommisionPrcntgHbds == "") {
                //bValid = false;
                //$("#lbl_txtCommisionAmnt").css("display", "");
                CommisionPrcntgHbds = "0";
            }
            if (CommisionPrcntgHbds != "" && !reg.test(CommisionPrcntgHbds)) {
                bValid = false;
                $("#lbl_txtCommisionHbds").html("* Amount must be numeric.");
                $("#lbl_txtCommisionAmntHbds").css("display", "");
            }
            if (CommisionPrcntgMgh == "") {
                //bValid = false;
                //$("#lbl_txtCommisionAmnt").css("display", "");
                CommisionPrcntgMgh = "0";
            }
            if (CommisionPrcntgMgh != "" && !reg.test(CommisionPrcntgMgh)) {
                bValid = false;
                $("#lbl_txtCommisionMgh").html("* Amount must be numeric.");
                $("#lbl_txtCommisionAmntMgh").css("display", "");
            }
            if (CommisionPrcntgdotw == "") {
                //bValid = false;
                //$("#lbl_txtCommisionAmnt").css("display", "");
                CommisionPrcntgdotw = "0";
            }
            if (CommisionPrcntgdotw != "" && !reg.test(CommisionPrcntgdotw)) {
                bValid = false;
                $("#lbl_txtCommisionMgh").html("* Amount must be numeric.");
                $("#lbl_txtCommisionAmntMgh").css("display", "");
            }
            if (CommisionAmntHbds == "") {
                //bValid = false;
                //$("#lbl_txtCommisionAmnt").css("display", "");
                CommisionAmntHbds = "0";
            }
            if (CommisionAmntHbds != "" && !reg.test(CommisionAmntHbds)) {
                bValid = false;
                $("#lbl_txtCommisionAmntHbds").html("* Amount must be numeric.");
                $("#lbl_txtCommisionAmntHbds").css("display", "");
            }
            if (CommisionAmntMgh == "") {
                //bValid = false;
                //$("#lbl_txtCommisionAmnt").css("display", "");
                CommisionAmntMgh = "0";
            }
            if (CommisionAmntMgh != "" && !reg.test(CommisionAmntMgh)) {
                bValid = false;
                $("#lbl_txtCommisionAmntMgh").html("* Amount must be numeric.");
                $("#lbl_txtCommisionAmntMgh").css("display", "");
            }
            if (CommisionAmntdotw == "") {
                //bValid = false;
                //$("#lbl_txtCommisionAmnt").css("display", "");
                CommisionAmntdotw = "0";
            }
            if (CommisionAmntdotw != "" && !reg.test(CommisionAmntdotw)) {
                bValid = false;
                $("#lbl_txtCommisionAmntdotw").html("* Amount must be numeric.");
                $("#lbl_txtCommisionAmntdotw").css("display", "");
            }
            if (bValid == true) {
                var dataToPass = {
                    SupplierHbds: SupplierHbds,
                    HbdsSid: HbdsSid,
                    MrkUpPrcntgHbds: MrkUpPrcntgHbds,
                    MrkUpAmntHbds: MrkUpAmntHbds,
                    CommisionPrcntgHbds: CommisionPrcntgHbds,
                    CommisionAmntHbds: CommisionAmntHbds,
                    SupplierMgh: SupplierMgh,
                    MghSid: MghSid,
                    MrkUpPrcntgMgh: MrkUpPrcntgMgh,
                    MrkUpAmntMgh: MrkUpAmntMgh,
                    CommisionPrcntgMgh: CommisionPrcntgMgh,
                    CommisionAmntMgh: CommisionAmntMgh,
                    Supplierdotw: Supplierdotw,
                    dotwSid: lbldotwSid,
                    MrkUpPrcntgdotw: MrkUpPrcntgdotw,
                    MrkUpAmntdotw: MrkUpAmntdotw,
                    CommisionPrcntgdotw: CommisionPrcntgdotw,
                    CommisionAmntdotw: CommisionAmntdotw,
                    GroupId: GroupId,
                };
                var jsonText = JSON.stringify(dataToPass);
                $.ajax({
                    url: "../Handler/GroupMarkupHandler.asmx/UpdateHotelGroupMarkup",
                    type: "post",
                    data: jsonText,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                        if (result.retCode == 1) {
                            $("#alSuccess1").css("display", "");
                        } else if (result.retCode == 0) {
                            $("#alError1").css("display", " ")
                        }
                    },
                    error: function () {
                        Success('Error occured while processing your request! Please try again.');
                    }
                });

            }
        }


        if ($("#lbl_GroupSid").text() != "" && $("#lbl_Service").text() == "PackageGroupMarkup" && $("#lbl_Service").text("UpdateGroupMarkupMarkup")) {
            var SupplierCUT = $("#lblSupplier").text();
            var GroupId = $("#lbl_GroupSid").text();
            var CUTsSid = $("#lblSid").text();
            var MrkUpPrcntgCUT = $("#txtMrkUpPrcntg").val();
            var MrkUpAmntCUT = $("#txtMrkUpAmnt").val();
            var CommisionPrcntgCUT = $("#txtCommisionPrcntg").val();
            var CommisionAmntCUT = $("#txtCommisionAmnt").val();

            var Type = $("#lbl_Service").text();
            $("#alSuccess1").css("display", "none");
            $("#alError1").css("display", "none")
            var bValid = true;

            if (MrkUpPrcntgCUT == "") {
                MrkUpPrcntgCUT = 0;
            }
            if (MrkUpPrcntgCUT != "" && !reg.test(MrkUpPrcntgCUT)) {
                bValid = false;
                $("#lbl_txtMrkUpPrcntgHbds").html("* Percent must be numeric.");
                $("#lbl_txtMrkUpPrcntgHbds").css("display", "");
            }

            if (MrkUpAmntCUT == "") {
                //bValid = false;
                //$("#lbl_txtMrkUpAmnt").css("display", "");
                MrkUpAmntCUT = 0;
            }
            if (MrkUpAmntCUT != "" && !reg.test(MrkUpAmntCUT)) {
                bValid = false;
                $("#lbl_txtMrkUpAmntHbds").html("* Amount must be numeric.");
                $("#lbl_txtMrkUpAmntHbds").css("display", "");
            }

            if (CommisionPrcntgCUT == "") {
                //bValid = false;
                //$("#lbl_txtCommisionAmnt").css("display", "");
                CommisionPrcntgCUT = "0";
            }
            if (CommisionPrcntgCUT != "" && !reg.test(CommisionPrcntgCUT)) {
                bValid = false;
                $("#lbl_txtCommisionHbds").html("* Amount must be numeric.");
                $("#lbl_txtCommisionAmntHbds").css("display", "");
            }

            if (CommisionAmntCUT == "") {
                //bValid = false;
                //$("#lbl_txtCommisionAmnt").css("display", "");
                CommisionAmntCUT = "0";
            }
            if (CommisionAmntCUT != "" && !reg.test(CommisionAmntCUT)) {
                bValid = false;
                $("#lbl_txtCommisionAmntHbds").html("* Amount must be numeric.");
                $("#lbl_txtCommisionAmntHbds").css("display", "");
            }

            if (bValid == true) {
                var dataToPass = {
                    SupplierCUT: SupplierCUT,
                    CUTsSid: CUTsSid,
                    MrkUpPrcntgCUT: MrkUpPrcntgCUT,
                    MrkUpAmntCUT: MrkUpAmntCUT,
                    CommisionPrcntgCUT: CommisionPrcntgCUT,
                    CommisionAmntCUT: CommisionAmntCUT,
                    GroupId: GroupId

                };
                var jsonText = JSON.stringify(dataToPass);
                $.ajax({
                    url: "../Handler/GroupMarkupHandler.asmx/UpdateHotelGroupMarkup",
                    type: "post",
                    data: jsonText,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                        if (result.retCode == 1) {
                            $("#alSuccess1").css("display", "");
                        } else if (result.retCode == 0) {
                            $("#alError1").css("display", " ")
                        }
                    },
                    error: function () {
                        Success('Error occured while processing your request! Please try again.');
                    }
                });

            }
        }



        if ($("#lbl_GroupSid").text() != "" && $("#lbl_Service").text() == "ActivityGroupMarkup" && $("#lbl_Service").text("UpdateGroupMarkupMarkup")) {
            var ActivitySupllier = $("#lblSupplierActivity").text();
            var GroupId = $("#lbl_GroupSid").text();
            var ActivitySid = $("#lblActivitySid").text();
            var MrkUpPrcntgActivity = $("#txtMrkUpPrcntgActivity").val();
            var MrkUpAmntActivity = $("#txtMrkUpAmntActivity").val();
            var CommisionPrcntgActivity = $("#txtCommisionPrcntgActivity").val();
            var CommisionAmntActivity = $("#txtCommisionAmntActivity").val();

            var Type = $("#lbl_Service").text();
            $("#alSuccess1").css("display", "none");
            $("#alError1").css("display", "none")
            var bValid = true;

            if (MrkUpPrcntgActivity == "") {
                MrkUpPrcntgActivity = 0;
            }
            if (MrkUpPrcntgActivity != "" && !reg.test(MrkUpPrcntgActivity)) {
                bValid = false;
                $("#lbl_txtMrkUpPrcntgHbds").html("* Percent must be numeric.");
                $("#lbl_txtMrkUpPrcntgHbds").css("display", "");
            }

            if (MrkUpAmntActivity == "") {
                //bValid = false;
                //$("#lbl_txtMrkUpAmnt").css("display", "");
                MrkUpAmntActivity = 0;
            }
            if (MrkUpAmntActivity != "" && !reg.test(MrkUpAmntActivity)) {
                bValid = false;
                $("#lbl_txtMrkUpAmntHbds").html("* Amount must be numeric.");
                $("#lbl_txtMrkUpAmntHbds").css("display", "");
            }

            if (CommisionPrcntgActivity == "") {
                //bValid = false;
                //$("#lbl_txtCommisionAmnt").css("display", "");
                CommisionPrcntgActivity = "0";
            }
            if (CommisionPrcntgActivity != "" && !reg.test(CommisionPrcntgActivity)) {
                bValid = false;
                $("#lbl_txtCommisionHbds").html("* Amount must be numeric.");
                $("#lbl_txtCommisionAmntHbds").css("display", "");
            }

            if (CommisionAmntActivity == "") {
                //bValid = false;
                //$("#lbl_txtCommisionAmnt").css("display", "");
                CommisionAmntActivity = "0";
            }
            if (CommisionAmntActivity != "" && !reg.test(CommisionAmntActivity)) {
                bValid = false;
                $("#lbl_txtCommisionAmntHbds").html("* Amount must be numeric.");
                $("#lbl_txtCommisionAmntHbds").css("display", "");
            }

            if (bValid == true) {
                var dataToPass = {
                    ActivitySupllier: ActivitySupllier,
                    ActivitySid: ActivitySid,
                    MrkUpPrcntgActivity: MrkUpPrcntgActivity,
                    MrkUpAmntActivity: MrkUpAmntActivity,
                    CommisionPrcntgActivity: CommisionPrcntgActivity,
                    CommisionAmntActivity: CommisionAmntActivity,
                    GroupId: GroupId

                };
                var jsonText = JSON.stringify(dataToPass);
                $.ajax({
                    url: "../Handler/GroupMarkupHandler.asmx/UpdateHotelGroupMarkup",
                    type: "post",
                    data: jsonText,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                        if (result.retCode == 1) {
                            $("#alSuccess1").css("display", "");
                        } else if (result.retCode == 0) {
                            $("#alError1").css("display", " ")
                        }
                    },
                    error: function () {
                        Success('Error occured while processing your request! Please try again.');
                    }
                });

            }
        }


        if ($("#lbl_GroupSid").text() != "" && $("#lbl_Service").text() == "FlightGroupMarkup" && $("#lbl_Service").text("UpdateGroupMarkupMarkup")) {
            var FlightSid;
            var GroupId = $("#lbl_GroupSid").text();
            for (var i = 0; i < 12; i++) {
                SupplierFlight[i] = $("#lblFlightAirlines0" + (i + 1)).text();
                MrkUpPrcntgFlight[i] = $("#txtMrkUpPrcntgFlight0" + (i + 1)).val();
                MrkUpAmntFlight[i] = $("#txtMrkUpAmntFlight0" + (i + 1)).val();
                CommisionPrcntgFlight[i] = $("#txtCommisionPrcntgFlight0" + (i + 1)).val();
                CommisionAmntFlight[i] = $("#txtCommisionAmntFlight0" + (i + 1)).val();
                if (MrkUpPrcntgFlight[i] == "") {
                    MrkUpPrcntgFlight[i] = 0;
                }
                if (MrkUpPrcntgFlight[i] != "" && !reg.test(MrkUpPrcntgFlight[i])) {
                    bValid = false;
                    $("#lbl_txtMrkUpPrcntgFlight0" + (i + 1)).html("* Percent must be numeric.");
                    $("#lbl_txtMrkUpPrcntgFlight0" + (i + 1)).css("display", "");
                }
                if (MrkUpAmntFlight[i] == "") {
                    MrkUpAmntFlight[i] = 0;
                }
                if (MrkUpAmntFlight[i] != "" && !reg.test(MrkUpAmntFlight[i])) {
                    bValid = false;
                    $("#lbl_txtMrkUpAmntFlight0" + (i + 1)).html("* Percent must be numeric.");
                    $("#lbl_txtMrkUpAmntFlight0" + (i + 1)).css("display", "");
                }
                if (CommisionPrcntgFlight[i] == "") {
                    CommisionPrcntgFlight[i] = 0;
                }
                if (CommisionPrcntgFlight[i] != "" && !reg.test(CommisionPrcntgFlight[i])) {
                    bValid = false;
                    $("#lbl_txtCommisionPrcntgFlight0" + (i + 1)).html("* Percent must be numeric.");
                    $("#lbl_txtCommisionPrcntgFlight0" + (i + 1)).css("display", "");
                }
                if (CommisionAmntFlight[i] == "") {
                    CommisionAmntFlight[i] = 0;
                }
                if (CommisionAmntFlight[i] != "" && !reg.test(CommisionAmntFlight[i])) {
                    bValid = false;
                    $("#lbl_txtCommisionAmntFlight0" + (i + 1)).html("* Percent must be numeric.");
                    $("#lbl_txtCommisionAmntFlight0" + (i + 1)).css("display", "");
                }
            }
            var dataToPass = {
                SupplierFlight: SupplierFlight,
                FlightSid: FlightSid,
                MrkUpPrcntgFlight: MrkUpPrcntgFlight,
                MrkUpAmntFlight: MrkUpAmntFlight,
                CommisionPrcntgFlight: CommisionPrcntgFlight,
                CommisionAmntFlight: CommisionAmntFlight,
                GroupId: GroupId,

            };
            var jsonText = JSON.stringify(dataToPass);
            if (bValid == true) {
                $.ajax({
                    url: "../Handler/GroupMarkupHandler.asmx/UpdateFlightGroupMarkup",
                    type: "post",
                    data: jsonText,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                        if (result.retCode == 1) {
                            //$("#AddGroupModal").modal("hide", "")
                            GetGroup();
                            $("#alSuccess1").css("display", "");
                            GetMarkupDetails(GroupId);
                            $("#alSuccess1").css("display", "");
                            $("#alSuccess1").css("display", " ");
                        } else if (result.retCode == 0) {
                            $("#alError1").css("display", " ")
                        }
                    },
                    error: function () {
                        Success('Error occured while processing your request! Please try again.');
                    }
                });

            }


        }
    }
}
