﻿$(document).ready(function () {
    $("#txt_QuotationDate").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#txt_TourStart").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#txt_TourEnd").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    id = GetQueryStringParams('ID');
    if (id != undefined) {
        GetQuotationByID(id);
        $("#btn_Quotation").val('Update');
        $("#btn_Quotation").text('Update');
    }
    setTimeout(function () {
        AllAgency();
    }, 500);
 

    GetCountry();

    GetStaffDetails();

    $('#selCountry').change(function () {
        var sndcountry = $('#selCountry').val();
        GetCity(sndcountry);
    });



    $('#selAgency').change(function () {
        var Id = $('#selAgency').val();
        GetAgencybyId(Id);
    });



});



function AllAgency() {
    debugger;
    // Ajax request to get categories
    $.ajax({
        type: "POST",
        url: "../Handler/QuotationHandler.asmx/GetAgentDetail",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            try {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    m_arrAgency = result.List_Agent;
                    if (arrCountry.length > 0) {
                        $("#selAgency").empty();
                       
                        var ddlRequest = '<option selected="selected" value="-">Select Agency</option>';
                        for (i = 0; i < arrCountry.length; i++) {
                            ddlRequest += '<option   value="' + m_arrAgency[i].sid + '" onchange="GetAgencybyId(' + m_arrAgency[i].sid + ')">' + m_arrAgency[i].AgencyName + '</option>';
                        }
                            $("#selAgency").append(ddlRequest);
                    }
                }
                
            }
            catch (e) { }
        },
        error: function () {
            alert("Error getting Agency");
        }
    });

}

function GetStaffDetails() {
    debugger;
    // Ajax request to get categories
    $.ajax({
        type: "POST",
        url: "../Handler/QuotationHandler.asmx/GetStaffDetails",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            try {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;

                if (result.Session == 0)// Session Expired
                {
                    window.location.href = "Default.aspx";
                    return false;
                }
                if (result.retCode == 1) {
                    m_arrStaff = result.Staff;

                    var ul = '';
                    $("#selQuotedby").empty();

                    ul += '<option selected="selected" value="-">Select Staff</option>';
                    if (m_arrStaff.length > 0) {
                        for (var i = 0; i < m_arrStaff.length; i++) {
                            ul += '<option   value=' + m_arrStaff[i].sid + '>' + m_arrStaff[i].ContactPerson + '</option>';
                        }

                    }
                    $('#selQuotedby').append(ul);
                }
                else {
                    $("#selQuotedby").append($('<option value="No record Found "></option>'));
                }
            }
            catch (e) { }
        },
        error: function () {
            alert("Error getting Staff");
        }
    });

}

function GetCountry() {
    $.ajax({
        type: "POST",
        url: "../Handler/QuotationHandler.asmx/GetCountry",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCountry = result.Country;
                if (arrCountry.length > 0) {
                    $("#selCountry").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any Country</option>';
                    for (i = 0; i < arrCountry.length; i++) {
                        ddlRequest += '<option value="' + arrCountry[i].Countryname + '">' + arrCountry[i].Countryname + '</option>';
                    }
                    $("#selCountry").append(ddlRequest);
                    GetState();
                }
            }
        },
        error: function () {
            alert("An error occured while loading countries")
        }
    });
}

var Country = "";

function GetStateDiv() {
    Country = $("#selCountry option:selected").val();
    if (Country == "IN") {
        $("#DivState").show();
        GetState();
    }
    else {
        $("#DivState").hide();
    }
}

function GetState() {
    $.ajax({
        type: "POST",
        url: "../Handler/QuotationHandler.asmx/GetState",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrState = result.List;
                if (arrState.length > 0) {
                    $("#selState").empty();

                    var ddlRequest = '<option selected="selected" value="-">Select Any State</option>';
                    for (i = 0; i < arrState.length; i++) {
                        ddlRequest += '<option value="' + arrState[i].SateName + '">' + arrState[i].SateName + '</option>';
                    }
                    $("#selState").append(ddlRequest);

                }
            }
            if (result.retCode == 0) {
                $("#selState").empty();
            }
        },
        error: function () {
            alert("An error occured while loading States")
        }
    });
}

function GetCity(recCountry) {
    $.ajax({
        type: "POST",
        url: "../Handler/QuotationHandler.asmx/GetCity",
        data: '{"country":"' + recCountry + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCity = result.City;
                if (arrCity.length > 0) {
                    $("#selCity").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any City</option>';
                    for (i = 0; i < arrCity.length; i++) {
                        ddlRequest += '<option value="' + arrCity[i].Description + '" >' + arrCity[i].Description + '</option>';
                    }
                    $("#selCity").append(ddlRequest);

                }
            }
            if (result.retCode == 0) {
                $("#selCity").empty();
            }
        },
        error: function () {
            alert("An error occured while loading cities")
        }
    });
    //$('#selCity option:selected').val(tempcitycode);
}

function GetAgencybyId(Id) {

    var data = { AgentID: Id };
    $.ajax({
        type: "POST",
        url: "../Handler/QuotationHandler.asmx/GetAgencyDetails",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var arrAgency = result.arrAgency;
                $("#txt_Companyname").val(arrAgency.AgencyName);
                $('#txt_Address').val(arrAgency.Address);
                $('#txt_Email').val(arrAgency.email);
                $('#txt_Phone').val(arrAgency.phone);
                $('#txt_Mobile').val(arrAgency.Mobile);
                //$('#selCountry').val(arrAgency.Country)
                GetNationality(arrAgency.Country);
                //$("#DivCountry .select span").textContent = arrAgency.Country;
                AppendCity(arrAgency.Description)
                //$('#selCity').val(arrAgency.Description)

                //$("#DivCity .select span").textContent = arrAgency.Description;

                //$('input[value="' + arrAgency.Country + '"][class="OfferType"]').prop("selected", true);
                //$("#selCountry").val(arrAgency.Country);

                //$('input[value="' + arrAgency.Description + '"][class="OfferType"]').prop("selected", true);
                //$("#selCity").val(arrAgency.Description);
                //$('select#selCity  option:selected').val(arrAgency.Description);
                //$('select#selCountry  option:selected').val(arrAgency.Country);

                
                $('#txt_Contactperson').val(arrAgency.ContactPerson);

            }
            else {
                alert("An error occured !!!");
            }
        },
        error: function () {
            alert("An error occured !!!");
        }
    });
}

function AddQuotation() {

    debugger;
    var bValid = true;

    var id = GetQueryStringParams('ID');

    var Date = $("#txt_QuotationDate").val();

    var Quoteto = $("#selAgency option:selected").val();

    var Companyname = $("#txt_Companyname").val();

    var Email = $("#txt_Email").val();

    var Phone = $("#txt_Phone").val();

    var Mobile = $("#txt_Mobile").val();

    var Contactperson = $("#txt_Contactperson").val();

    var Address = $("#txt_Address").val();

    var City = $("#selCity option:selected").val();

    var State = $("#selState option:selected").val();

    var Country = $("#selCountry option:selected").val();

    var Leadguestname = $("#txt_Leadguestname").val();

    var Adults = $("#selAdults").val();

    var Child = $("#selChild").val();

    var Infant = $("#selInfant").val();

    var TourStart = $("#txt_TourStart").val();

    var TourEnd = $("#txt_TourEnd").val();

    var QuotationDetails = document.getElementById('noise').value;

    var Quotedby = $("#selQuotedby option:selected").val();

    var Currentstatus = $("#selCurrentstatus option:selected").val();

    var NoteOrRemark = $("#txt_NoteorRemark").val();

    //if (Date == "") {
    //    bValid = false;
    //    Success("");
    //}


    //if (Quoteto == "") {
    //    bValid = false;
    //    $("#lbl_QuotetoAgency").css("display", "");
    //}



    if (Companyname == "") {
        bValid = false;
        Success("Please enter Company name");
    }

    if (Email == "") {
        bValid = false;
        Success("Please enter Email");
    }

    if (Mobile == "") {
        bValid = false;
        Success("Please enter Mobile");
    }

    if (Contactperson = "") {
        bValid = false;
        Success("Please enter Contact Person");
    }

    if (Adults == "") {
        bValid = false;
        Success("Please enter Adults");
    }
    if (Child == "") {
        bValid = false;
        Success("Please enter Child");
    }
    if (Infant == "") {
        bValid = false;
        Success("Please enter Infant");
    }
    if (TourStart == "") {
        bValid = false;
        Success("Please select TourStart");
    }
    if (TourEnd == "") {
        bValid = false;
        Success("Please select TourEnd");
    }
    if (Quotedby == "") {
        bValid = false;
        Success("Please enter Quotedby");
    }

    if (Currentstatus == "") {
        bValid = false;
        Success("Please select Currentstatus");
    }

    var data = {
        ID: id,
        Date: Date,
        Quoteto: Quoteto,
        Companyname: Companyname,
        Email: Email,
        Phone: Phone,
        Mobile: Mobile,
        Contactperson: Contactperson,
        Address: Address,
        City: City,
        State: State,
        Country: Country,
        Leadguestname: Leadguestname,
        Adults: Adults,
        Child: Child,
        Infant: Infant,
        TourStart: TourStart,
        TourEnd: TourEnd,
        QuotationDetails: QuotationDetails,
        Quotedby: Quotedby,
        Currentstatus: Currentstatus,
        NoteOrRemark: NoteOrRemark

    }
    if (bValid == true) {

        if ($("#btn_Quotation").val() == "Update") {
            $.ajax({
                type: "POST",
                url: "../Handler/QuotationHandler.asmx/UpdateQuotation",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.Session == 0) {
                        window.location.href = "QuotationDetails.aspx";

                        return false;
                    }
                    if (result.retCode == 1) {

                        Success("Quotation Details Updated successfully")
                    }
                    else {
                        Success("Something went wrong! Please contact administrator.")

                    }
                },
                error: function () {
                }
            });
        }

        else if ($("#btn_Quotation").val() == "Add Quotation") {

            $.ajax({
                type: "POST",
                url: "../Handler/QuotationHandler.asmx/AddQuotation",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.retCode == 1) {

                        alert("Quotation Added Successfully");
                        window.location.href = "QuotationDetails.aspx";
                    }
                    else {
                        alert(" Error");

                    }
                },

            });


        }

    }
}

function GetQuotationByID(Id) {

    var data = { Id: Id };
    $.ajax({
        type: "POST",
        url: "../Handler/QuotationHandler.asmx/GetQuotationByID",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var QuotationDetailArr = result.Arr[0];

                $("#txt_QuotationDate").val(QuotationDetailArr.Date);

                $("#selAgency option:selected").val(QuotationDetailArr.AgencyName);

                $("#txt_Companyname").val(QuotationDetailArr.Companyname);

                $('#txt_Email').val(QuotationDetailArr.Email);

                $('#txt_Phone').val(QuotationDetailArr.Phone);

                $('#txt_Mobile').val(QuotationDetailArr.Mobile);

                $('#selCity').val(QuotationDetailArr.City)
                $("#DivCity .select span").textContent = QuotationDetailArr.City;
                $('#selCountry').val(QuotationDetailArr.Country)
                $("#DivCountry .select span").textContent = QuotationDetailArr.Country;
                $('#selState').val(QuotationDetailArr.State)
                $("#DivState .select span").textContent = QuotationDetailArr.State;

                //$('#selCity  option:selected').val(QuotationDetailArr.City);

                //$('#selState  option:selected').val(QuotationDetailArr.State);

                //$('#selCountry  option:selected').val(QuotationDetailArr.Country);

                $('#txt_Contactperson').val(QuotationDetailArr.Contactperson);

                $('#txt_Address').val(QuotationDetailArr.Address);

                $('#txt_Leadguestname').val(QuotationDetailArr.Leadguestname);

                $('#selAdults  option:selected').val(QuotationDetailArr.Adults);

                $('#selChild  option:selected').val(QuotationDetailArr.Child);

                $('#selInfant  option:selected').val(QuotationDetailArr.Infant);

                $('#txt_TourStart').val(QuotationDetailArr.TourStart);

                $('#txt_TourEnd').val(QuotationDetailArr.TourEnd);

                $('#noise').val(QuotationDetailArr.QuotationDetails);

                $('#selQuotedby').val(QuotationDetailArr.Quotedby)
                $("#DivQuotedby .select span").textContent = QuotationDetailArr.Quotedby;

                $('#selCurrentstatus').val(QuotationDetailArr.Currentstatus)
                $("#DivCurrentstatus .select span").textContent = QuotationDetailArr.Currentstatus;
                //$('#selQuotedby option:selected').val(QuotationDetailArr.Quotedby);

                //$('#selCurrentstatus option:selected').val(QuotationDetailArr.Currentstatus);

                $('#txt_NoteorRemark').val(QuotationDetailArr.Remark);
            }
            else {
                alert("An error occured !!!");
            }
        },
        error: function () {
            alert("An error occured !!!");
        }
    });
}


function GetNationality(Countryname) {
    try {
        var checkclass = document.getElementsByClassName('check');
        var Country = $.grep(arrCountry, function (p) { return p.Countryname == Countryname; }).map(function (p) { return p.Country; });

        $("#DivCountry .select span")[0].textContent = Countryname;
        for (var i = 0; i <= Country.length - 1; i++) {
            $('input[value="' + Countryname + '"][class="country"]').prop("selected", true);
            $("#selCountry").val(Countryname);
        }
    }
    catch (ex)
    { }
}

function AppendCity(Description) {
    try {
        var checkclass = document.getElementsByClassName('check');
        var Description = $.grep(arrCity, function (p) { return p.Description == Description; }).map(function (p) { return p.Code; });

        $("#DivCity .select span")[0].textContent = Description;
        for (var i = 0; i <= Description.length; i++) {
            $('input[value="' + Description + '"][class="OfferType"]').prop("selected", true);
            $("#selCity").val(Description);
        }
    }
    catch (ex)
    { }
}



