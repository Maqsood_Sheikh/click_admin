﻿var HotelCode = 0, HotelName = '', RoomId = 0, RoomType = '';
var RoomDetails = [], arrRoomTypes = [], arrRoomList = [], arrSuppliers = [], arrCancelationPolicies = [], arrMasterOffer = [];
var Supplier;
var Rate;
$('.onlyNumbersText').find('input').on('keypress', function () {
    return event.charCode >= 48 && event.charCode <= 57;
})
var RateTypes = ["GN", "SL"];
var TType = ""
$(function () {
    HotelCode = getParameterByName('sHotelID');
    HotelName = getParameterByName('HotelName');
    RateID = getParameterByName('RateID');
    RoomID = getParameterByName('RoomID');
    TType = getParameterByName('RateType');
    $("#lblHotel").text(HotelName);
    GetCountry();
    GetSuppliers();
    GetCancelationPolicies();
    setTimeout(function () {
        GetURate(RateID);
    }, 5000);

    //GetOtherRates();
    // ChekAvailRate();
    GetCurrencyy();
    GetMealPlans();
    DateRateValidity(0);
    getRooms();

    document.getElementById('chkAllDays0').click();
    document.getElementById('chkAllSD0').click();
})

var datepickersOpt = {
    dateFormat: 'dd-mm-yy'
}

function GetURate(RateID) {

    $.ajax({
        type: "POST",
        url: "handler/RoomHandler.asmx/GetURate",
        data: '{"RateID":"' + RateID + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Rate = result.Rate;
                console.log(Rate);
                $("#sel_CurrencyCode").val(Rate[0].CurrencyCode);
                $("#Currency .select span")[0].textContent = Rate[0].CurrencyCode;

                $("#sel_MealPlan").val(Rate[0].MealPlan);
                 //MaelPlan////
                if (Rate[0].MealPlan == "BB")
                    meal = "Bed & Breakfast";
                else if (Rate[0].MealPlan == "HB")
                    meal = "Half Board";
                else if (Rate[0].MealPlan == "FB")
                    meal = "Full Board";
                else
                    meal = "Room Only"
                $("#MealPlan .select span")[0].textContent = meal;

                ///////////////
               
                var Supplier = $.grep(arrSuppliers, function (p) { return p.sid == Rate[0].SupplierId; })
                                    .map(function (p) { return p.Supplier; });

                $("#SelGroup").val('25');
                $("#sel_Supplier").val('25');
                $("#Supplier .select span")[0].textContent = Supplier[0];
                //$("#sel_Nationality").val(Rate[0].ValidNationality)[0];

                GetNationality(Rate[0].ValidNationality)

                $("#txtFromRV0").val(Rate[0].Checkin);
                $("#txtToRV0").val(Rate[0].Checkout);

                setTimeout(function () {
                    $("#btn_AddDates").click();
                }, 500);
            }
        },
        error: function () {
        }
    });
}

var arrCountry;
function GetCountry() {
    var ddlRequest = '';
    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetCountry",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCountry = result.Country;
                if (arrCountry.length > 0) {
                    ddlRequest += ' <option class="optCountry" value="AllCountry">For All Nationality</option>';
                    for (i = 0; i < arrCountry.length; i++) {
                        if (arrCountry[i].Country != "AllCountry")
                        ddlRequest += '<option class="optCountry" value="' + arrCountry[i].Country + '">' + arrCountry[i].Countryname + '</option>';
                    }
                    ddlRequest += ' <option value="Unselect" style="display:none"inline-block;" disabled>Unselect All</option>';
                    $("#sel_Nationality").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}

var arrMealPlans = [];
function GetMealPlans() {
    var ddlRequest = '';
    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetMealPlans",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            arrMealPlans = result.MealPlanList;
            if (result.retCode == 1) {
                for (i = 0; i < arrMealPlans.length; i++) {
                    ddlRequest += '<option value="' + arrMealPlans[i].MealPlanShort + '">' + arrMealPlans[i].MealPlanName + '</option>';
                }
            }
            else {
                ddlRequest += '<option value=""  disabled>Not Available</option>';
            }
            $("#sel_MealPlan").append(ddlRequest);
        },
        error: function () {
        }
    });


}

function CheckNationality(currentval) {
    if ($("#sel_Nationality option[value='All']").is(':selected')) {
        selectAll();
    }
    else if ($("#sel_Nationality option[value='Unselect']").is(':selected')) {
        UnselectAll();
    }

}

function selectAll() {
    $('#sel_Nationality option').prop('selected', true);
    $("#sel_Nationality option[value='All']").click();
}

function UnselectAll() {
    $('#sel_Nationality option').prop('selected', false);
    $("#sel_Nationality option[value='All']").click();
}

function GetCurrencyy() {
    var ddlRequest = '';
    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetCurrency",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCurrency = result.CurrencyList;
                if (arrCurrency.length > 0) {
                    for (i = 0; i < arrCurrency.length; i++) {
                        ddlRequest += '<option value="' + arrCurrency[i].CurrencyCode + '">' + arrCurrency[i].CurrencyCode + '</option>';
                    }
                    $("#sel_CurrencyCode").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}

function getRooms() {
    var sHotelId = HotelCode

    $.ajax({
        type: "POST",
        url: "handler/RoomHandler.asmx/GetRooms",
        data: '{"sHotelId":"' + sHotelId + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                //arrHotellist = result.MappedHotelList;
                arrRoomTypes = result.RoomTypeList;
                arrRoomList = result.RoomList;
                var tabs = '';
                var tabContent = '';
                tabs += '<ul class="tabs">';
                tabContent += '<div class="tabs-content" id="tabContentID">';
                for (var i = 0; i < arrRoomList.length; i++) {
                    for (var j = 0; j < arrRoomTypes.length; j++) {
                        if (arrRoomList[i].RoomTypeID == arrRoomTypes[j].RoomTypeID) {
                            if (i == 0) {
                                tabs += '<li class="active"><a href="#tabRoomtype' + i + '">' + arrRoomTypes[j].RoomType + '</a></li>';
                                tabContent += '<div class="with-padding tab-active divContents" id="tabRoomtype' + i + '">';
                                tabContent += '</div>';
                            }
                            else {
                                tabs += '<li class=""><a href="#tabRoomtype' + i + '" >' + arrRoomTypes[j].RoomType + '</a></li>';
                                tabContent += '<div class="with-padding divContents" style="display:none" id="tabRoomtype' + i + '">';
                                tabContent += '</div>';
                            }


                        }
                    }
                }
                tabs += '</ul>';
                tabContent += '</div>';
                $("#TabRooms").append(tabContent);
                $("#TabRooms").append(tabs);
                // $(".tabs li")[0].className = "Active";

            }

        }

    });
}

function getRoomsUp() {
    var sHotelId = HotelCode

    $.ajax({
        type: "POST",
        url: "handler/RoomHandler.asmx/GetRooms",
        data: '{"sHotelId":"' + sHotelId + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                //arrHotellist = result.MappedHotelList;
                arrRoomTypes = result.RoomTypeList;
                arrRoomList = result.RoomList;
                var tabs = '';
                var tabContent = '';
                tabs += '<ul class="tabs">';
                tabContent += '<div class="tabs-content" id="tabContentID">';
                for (var i = 0; i < arrRoomList.length; i++) {
                    for (var j = 0; j < arrRoomTypes.length; j++) {
                        if (arrRoomList[i].RoomTypeID == arrRoomTypes[j].RoomTypeID) {
                            if (arrRoomList[i].RoomTypeID == RoomID) {
                                if (i == 0) {
                                    tabs += '<li class="active"><a href="#tabRoomtype' + i + '">' + arrRoomTypes[j].RoomType + '</a></li>';
                                    tabContent += '<div class="with-padding tab-active divContents" id="tabRoomtype' + i + '">';
                                    tabContent += '</div>';
                                }
                                else {
                                    tabs += '<li class=""><a href="#tabRoomtype' + i + '" >' + arrRoomTypes[j].RoomType + '</a></li>';
                                    tabContent += '<div class="with-padding divContents" style="display:none" id="tabRoomtype' + i + '">';
                                    tabContent += '</div>';
                                }
                            }



                        }
                    }
                }
                tabs += '</ul>';
                tabContent += '</div>';
                $("#TabRooms").append(tabContent);
                $("#TabRooms").append(tabs);
                // $(".tabs li")[0].className = "Active";

            }

        }

    });
}
var arrSuppliers;
//cCancelationPolicies
function GetSuppliers() {
    var ddlRequest = '';
    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetSuppliers",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrSuppliers = result.SupplierList;
                if (arrSuppliers.length > 0) {
                    //ddlRequest += ' <option value="25" selected="selected">From Hotel</option>';
                    for (i = 0; i < arrSuppliers.length; i++) {
                        if (arrSuppliers[i].sid != '25')
                            ddlRequest += '<option value="' + arrSuppliers[i].sid + '">' + arrSuppliers[i].Supplier + '</option>';
                    }
                    $("#sel_Supplier").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}

function GetCancelationPolicies() {

    var ddlRequest = '';
    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetCancelationPolicies",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCancelationPolicies = result.CancelationPolicies;
                // $(".cCancelationPolicies select").empty();
                if (arrCancelationPolicies.length > 0) {
                    for (i = 0; i < arrCancelationPolicies.length; i++) {
                        ddlRequest += '<option value="' + arrCancelationPolicies[i].CancelationID + '">' + arrCancelationPolicies[i].CancelationPolicy + '</option>';
                    }
                    $(".cCancelationPolicies select").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}

//div Rate Validity
// <p><span class="tag">Tag</span></p>
function DateRateValidity(id) {
    if (id == 0) {
        $("#txtFromRV" + id).datepicker($.extend({
            minDate: 0,
            onSelect: function () {
                var minDate = $(this).datepicker('getDate');
                minDate.setDate(minDate.getDate() + 1); //add One days
                $("#txtToRV" + id).datepicker("option", "minDate", minDate);
            }
        }, datepickersOpt));

        $("#txtToRV" + id).datepicker($.extend({
            onSelect: function () {
                var maxDate = $(this).datepicker('getDate');
                maxDate.setDate(maxDate.getDate());
            }
        }, datepickersOpt));
    }
    else {
        var previous = moment($("#txtToRV" + (id - 1)).val(), "DD-MM-YYYY");
        $("#txtFromRV" + id).datepicker($.extend({
            minDate: previous._i,
            onSelect: function () {
                var minDate = $(this).datepicker('getDate');
                minDate.setDate(minDate.getDate() + 1); //add One days
                $("#txtToRV" + id).datepicker("option", "minDate", minDate);
            }
        }, datepickersOpt));
        $("#txtToRV" + id).datepicker($.extend({
            onSelect: function () {
                var maxDate = $(this).datepicker('getDate');
                maxDate.setDate(maxDate.getDate());
            }
        }, datepickersOpt));
    }

}

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}

function DateSpecialDates(id) {
    var cFromRVDate = $(".cRateValidFrom");
    var cToRVDate = $(".cRateValidTo");

    count = cFromRVDate.length - 1;
    var previousFrom = moment(cFromRVDate[0].value, "DD-MM-YYYY");

    count1 = cToRVDate.length - 1;
    var previousTo = moment(cToRVDate[count1].value, "DD-MM-YYYY");

    $("#txtFromSD" + id).datepicker($.extend({
        minDate: previousFrom._i,
        maxDate: previousTo._i,
        onSelect: function () {
            var minDate = $(this).datepicker('getDate');
            minDate.setDate(minDate.getDate() + 1);
            $("#txtToSD" + id).datepicker("option", "minDate", minDate);
        }
    }, datepickersOpt));

    $("#txtToSD" + id).datepicker($.extend({
        maxDate: previousTo._i,
        onSelect: function () {
            var maxDate = $(this).datepicker('getDate');
            maxDate.setDate(maxDate.getDate());
        }
    }, datepickersOpt));

}


//div Rate Validity
var clickRV = 0;
var arrRateValidities = [];

function CheckRVAllDays(id) {
    var AllDays = $("#chkAllDays" + id)[0];
    var Sunday = $("#chkSun" + id)[0];
    var Monday = $("#chkMon" + id)[0];
    var Tuesday = $("#chkTue" + id)[0];
    var Wednesday = $("#chkWed" + id)[0];
    var Thursday = $("#chkThu" + id)[0];
    var Friday = $("#chkFri" + id)[0];
    var Saturday = $("#chkSat" + id)[0];

    if (AllDays.checked == true) {
        Sunday.checked = true;
        Monday.checked = true;
        Tuesday.checked = true;
        Wednesday.checked = true;
        Thursday.checked = true;
        Friday.checked = true;
        Saturday.checked = true;
    }
    else {
        Sunday.checked = false;
        Monday.checked = false;
        Tuesday.checked = false;
        Wednesday.checked = false;
        Thursday.checked = false;
        Friday.checked = false;
        Saturday.checked = false;
    }

}

function CheckRVSingle(id) {
    var AllDays = $("#chkAllDays" + id)[0];
    var Sunday = $("#chkSun" + id)[0];
    var Monday = $("#chkMon" + id)[0];
    var Tuesday = $("#chkTue" + id)[0];
    var Wednesday = $("#chkWed" + id)[0];
    var Thursday = $("#chkThu" + id)[0];
    var Friday = $("#chkFri" + id)[0];
    var Saturday = $("#chkSat" + id)[0];
    if (Sunday.checked == false || Monday.checked == false || Tuesday.checked == false || Wednesday.checked == false || Thursday.checked == false || Friday.checked == false || Saturday.checked == false) {
        AllDays.checked = false;
    }
    if (Sunday.checked == true && Monday.checked == true && Tuesday.checked == true && Wednesday.checked == true && Thursday.checked == true && Friday.checked == true && Saturday.checked == true) {
        AllDays.checked = true;
    }

}

function RemoveRateValidity(valRemove) {
    if (clickRV > 0) {
        document.getElementById('divRV' + valRemove).remove();
        clickRV -= 1;
        document.getElementById("clickRV").innerHTML = clickRV;
        document.getElementById("lnkAddRV" + clickRV).style.display = "";

    }
}

var DateRangeRV = [];
var CheckinRV = [];
var CheckoutRV = [];

var DaysRV = [];
var RateValidityData = ''

function AddRVDates() {
    var trRooms = '';
    var tempDays = '';
    var cRateFormRV = $(".cRateValidFrom");
    var cRateToRV = $(".cRateValidTo");
    var ChkDay = $("#iRVDays0").children('label').children('input')
    ChkDay.each(function () {
        if (this.checked == true) {
            if (this.value != 'All') {
                tempDays += this.value + ',';
            }
            else {
                tempDays = 'All' + ',';
            }

        }
    })
    tempDays = tempDays.replace(/,\/|\/$/g, '');

    DaysRV = tempDays.split(',');
    var index = DaysRV.indexOf("All");    // <-- Not supported in <IE9
    if (index !== -1) {
        DaysRV.splice(index, 1);
    }

    for (var i = 0; i < cRateFormRV.length; i++) {

        var FromRV = cRateFormRV[i].value;
        var ToRV = cRateToRV[i].value;

        if (FromRV != "" && ToRV !== "") {
            CheckinRV[i] = FromRV;
            CheckoutRV[i] = ToRV;
            DateRangeRV[i] = FromRV + " TO " + ToRV;
            RateValidityData += DateRangeRV[i] + ' | ' + DaysRV + '^';
        }


    }





}
//End div Rate Validity

function CheckSpecialDate() {

    var ChkSD = $("#chkSpecialDate");
    var cRVFrom = $(".cRateValidFrom");
    var cRVTo = $(".cRateValidTo");

    if (ChkSD.is(":checked")) {
        for (var id = 0; id < cRVFrom.length; id++) {
            if (cRVFrom[id].value == "" || cRVTo[id].value == "") {
                Success("Please select Date Range");
                $("#chkSpecialDate").attr("checked", false);
                return false;
            }
        }
        $("#idSpecialDate")[0].style.cssText = "display:";
        //$("#dispSD")[0].style.cssText = "display:";
        DateSpecialDates(0);
    }
    else {
        $("#idSpecialDate")[0].style.cssText = "display: none;";
        $("#dispSD")[0].style.cssText = "display:none";
    }
}



function CheckSDAllDays(id) {
    if ($("#chkAllSD" + id)[0].checked == true) {
        $("#chkSunSD" + id)[0].checked = true;
        $("#chkMonSD" + id)[0].checked = true;
        $("#chkTueSD" + id)[0].checked = true;
        $("#chkWedSD" + id)[0].checked = true;
        $("#chkThuSD" + id)[0].checked = true;
        $("#chkFriSD" + id)[0].checked = true;
        $("#chkSatSD" + id)[0].checked = true;
    }
    else {
        $("#chkSunSD" + id)[0].checked = false;
        $("#chkMonSD" + id)[0].checked = false;
        $("#chkTueSD" + id)[0].checked = false;
        $("#chkWedSD" + id)[0].checked = false;
        $("#chkThuSD" + id)[0].checked = false;
        $("#chkFriSD" + id)[0].checked = false;
        $("#chkSatSD" + id)[0].checked = false;
    }
}

function CheckSDSingle(id) {
    var AllDays = $("#chkAllSD" + id)[0];
    var Sunday = $("#chkSunSD" + id)[0];
    var Monday = $("#chkMonSD" + id)[0];
    var Tuesday = $("#chkTueSD" + id)[0];
    var Wednesday = $("#chkWedSD" + id)[0];
    var Thursday = $("#chkThuSD" + id)[0];
    var Friday = $("#chkFriSD" + id)[0];
    var Saturday = $("#chkSatSD" + id)[0];
    if (Sunday.checked == false || Monday.checked == false || Tuesday.checked == false || Wednesday.checked == false || Thursday.checked == false || Friday.checked == false || Saturday.checked == false) {
        AllDays.checked = false;
    }
    if (Sunday.checked == true && Monday.checked == true && Tuesday.checked == true && Wednesday.checked == true && Thursday.checked == true && Friday.checked == true && Saturday.checked == true) {
        AllDays.checked = true;
    }

}

var clickSD = 0;
function AddSpecialDates(id) {
    var divRequest = '';
    if (($('#txtFromSD' + id).val() == "") || ($('#txtToSD' + id).val() == "")) {
        Success('Please Fill Mandatory Fields')
        return false;
    }
    if (id == clickSD) {
        document.getElementById("lnkAddSD" + id).style.display = "none";
        clickSD += 1;
        document.getElementById("clickSD").innerHTML = clickSD;
        divRequest += ' <div class="columns" id="divSD' + clickSD + '">';
        divRequest += '  <div class="three-columns"><small class="input-info">From:</small><br><span class="input"><span class="icon-calendar"></span><input type="text"  id="txtFromSD' + clickSD + '" class="input-unstyled ctxtfromSD" value=""></span></div>';
        divRequest += '  <div class="three-columns"><small class="input-info">To:</small><br><span class="input"><span class="icon-calendar"></span><input type="text"  id="txtToSD' + clickSD + '" class="input-unstyled ctxttoSD" value="" > </span></div>';
        divRequest += ' <div class="one-columns"><br />';
        divRequest += ' <a id="lnkAddSD' + clickSD + '"  onclick="AddSpecialDates(' + clickSD + ');" class="button"><span class="icon-plus-round blue"></span></a>';
        divRequest += ' <a  onclick="RemoveSpecialDate(' + clickSD + ');" class="button"><span class="icon-minus-round red"></span></a>';
        divRequest += ' </div>';


        divRequest += ' </div></div><br />';
        $('#idSpecialDate').append(divRequest);
        DateSpecialDates(clickSD);
        //document.getElementById('chkAllSD' + clickSD).click();
    }
}

function RemoveSpecialDate(valRemove) {
    if (clickSD > 0) {
        document.getElementById('divSD' + valRemove).remove();
        clickSD -= 1;
        document.getElementById("clickSD").innerHTML = clickSD;
        document.getElementById("lnkAddSD" + clickSD).style.display = "";

    }
}

var DateRangeSD = [];
var CheckinSD = [];
var CheckoutSD = [];
var DaysSD = [];
var SpecialDatesData = '';

function AddSDDates() {
    var trRooms = '';
    var cRateFromSD = $(".ctxtfromSD");
    var cRateToSD = $(".ctxttoSD");

    for (var i = 0; i < cRateFromSD.length; i++) {
        var tempDays = '';
        var FromSD = cRateFromSD[i].value;
        var ToSD = cRateToSD[i].value;

        var ChkSDDay = $("#iSDDays" + i).children('label').children('input')

        ChkSDDay.each(function () {
            if (this.checked == true) {
                if (this.value != 'All') {
                    tempDays += this.value + ',';
                }
                else {
                    tempDays = 'All';
                    return false;
                }

            }
        })
        tempDays = tempDays.replace(/,\/|\/$/g, '');
        DaysSD[i] = tempDays;
        if (FromSD != "" && ToSD != "") {
            DateRangeSD[i] = FromSD + " TO " + ToSD;
            CheckinSD[i] = FromSD;
            CheckoutSD[i] = ToSD;
            SpecialDatesData += DateRangeSD[i] + ' | ' + DaysSD[i] + '^';
        }

    }
}
//End div Special Dates

var WeekDaysRV = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
var WeekDaysSD = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
//var WeekRateType = ["RR", "EB", "CWB", "CNB"];
//var WeekRateType = ["RR", "EB"];
var WeekRateType = ["Room Rate"];


function AddDates() {

    var supl = $("#sel_Supplier").val();

    $(".divContents").empty();
    var divRoomType = $(".divContents")
    AddRVDates();
    setTimeout(function () {
        AddSDDates();
    }, 1000);

    var ValidFromRV = $('.cRateValidFrom')
    var ValidFromSD = $('.ctxtfromSD')

    var ValidToRV = $('.cRateValidTo')
    var ValidToSD = $('.ctxttoSD')

    //if (($('#txtFromRV' + id).val() == "") || ($('#txtToRV' + id).val() == "")) {
    //    alert('Please Fill Dates')
    //    $("#chkSpecialDate").attr("checked", false);
    //    return false;
    //}


    setTimeout(function () {
        if (DateRangeRV.length == 0) {
            alert("Please Select Date Range");
            return false
        }
        else {
            for (var j = 0; j < divRoomType.length; j++) {
                //Data Rate Validity
                var trRoomsRV = '';
                
                trRoomsRV += '<div class="tabs-content">';
                trRoomsRV += '<div class="with-padding">';
                trRoomsRV += '<div class="row block table-head margin-bottom">';
                trRoomsRV += '<span class="block-title anthracite-gradient glossy"><h3>ROOM VALIDITY</h3>';
                for (var i = 0; i < DateRangeRV.length; i++) {
                    //trRoomsRV += '<small>' + DateRangeRV[i] + '(' + DaysRV + ')&nbsp;<strong>|</strong>&nbsp;</small>';
                    trRoomsRV += '<small><strong>(</strong>' + DateRangeRV[i] + '<strong>)</strong>,</small>';
                }
                trRoomsRV += '  <div class="clear"></div>';
                trRoomsRV += '    </span>';

                trRoomsRV += '<div class="with-padding withpad tabwhite" id="RoomContentRV' + j + '">';

                //Inputs in RV 
                trRoomsRV += ' <div class="columns" id="RoomRatesByRoom' + j + '">';
                trRoomsRV += '  <div class="two-columns four-columns-tablet six-columns-mobile"><label>Room Rate(RR):</label><input type="text" value="0" onkeypress="return event.charCode >= 31 && event.charCode <= 57"  id="txtRoomRate' + j + '" class="input full-width cInputs"> </div>';
                trRoomsRV += '  <div class="two-columns four-columns-tablet six-columns-mobile"><label>Extra Bed(EB):</label> <input type="text" onkeypress="return event.charCode >= 31 && event.charCode <= 57"  id="txtExtraBed' + j + '" class="input full-width cInputs"> </div>';
                trRoomsRV += '  <div class="two-columns four-columns-tablet six-columns-mobile"><label>CWB:</label><input type="text" onkeypress="return event.charCode >= 31 && event.charCode <= 57" id="txtChildWithBed' + j + '" class="input full-width cInputs"></div>';
                trRoomsRV += '  <div class="two-columns four-columns-tablet six-columns-mobile"><label>CNB:</label><input type="text" onkeypress="return event.charCode >= 31 && event.charCode <= 57" id="txtChildNoBed' + j + '" class="input full-width cInputs"></div>';
                trRoomsRV += '  <div class="two-columns four-columns-tablet six-columns-mobile"><label>Booking Code:</label><input type="text" id="txtBookingCode' + j + '" class="input full-width"></div>';
                // trRoomsRV += '  <div class="new-row three-columns"><small class="input-info">Cancellation Policy:</small> <select id="sel_CancellationPolicyId' + j + '" class="select cCancelationPolicies" onchange="AddCancelations(this.id,' + j + ');"><option value="" selected="selected" disabled>Please select</option></select><ul id="iCancelationPolicies' + j + '" class="full-width sCanceltionPolicies"> </ul></div>';
                trRoomsRV += '  <div class="four-columns four-columns-tablet six-columns-mobile"><input type="checkbox" id="chkRVforDaywise' + j + '" onchange="CheckRVforDaywise(' + j + ')" /><label for="chkRVforDaywise' + j + '" class="label">Day Wise Rates</label></div>';
                trRoomsRV += '   </div>';


                trRoomsRV += '   <div class="columns" id="RoomRatesByDayRV' + j + '" style="display:none">';
                //Day wise Rates         

                
                for (var wr = 0; wr < WeekRateType.length; wr++)
                {
                    
                    trRoomsRV += '<div class="one-columns"><label>' + WeekRateType[wr] + '</label></div>';
                    trRoomsRV += ''
                    for (var wd = 0; wd < WeekDaysRV.length; wd++) {
                        trRoomsRV += '<div class="one-columns"><label>' + WeekDaysRV[wd] + '</label> <input type="text" onkeypress="return event.charCode >= 31 && event.charCode <= 57"  id="txtRV' + WeekDaysRV[wd] + j + wr + '" class="input cWeekDaysRV" placeholder="' + WeekDaysRV[wd] + '" style="width:40px;"></div>';
                    }
                   
                   
                }
               
                trRoomsRV += '</div>';
                // trRoomsRV += '</dl>';
                // trRoomsRV += '</div>';

                trRoomsRV += '<div  class="columns">';

                trRoomsRV += '  <div class="three-columns  twelve-columns-mobile"><label>Offer:</label><div class="full-width button-height SelBox"><select id="sel_OfferType' + j + '" name="validation-select" class="select full-width sel_OfferTyp"  onchange="AddOffer(this.id,\'' + j + '\');"><option value="" selected="selected" disabled>Please select</option></select><ul id="iOffer' + j + '" class="full-width sel_OfferTyp"></ul></div></div>';

                trRoomsRV += ' <div class="two-columns twelve-columns-mobile"><label>Offer Nights:</label><div class="full-width button-height SelBox"><select id="OfferNights' + j +'" name="validation-select" class="select full-width sel_Offernight"><option value="0" selected="selected">0</option><option value="1" >1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option></select></div></div>';


                trRoomsRV += '  <div class="four-columns  twelve-columns-mobile"><label>Cancellation Policy:</label><div id="ButtonSearch" style="height: 20px"><span class="button-group">'
                trRoomsRV += ' <input type="button" id="chkCancellationpolicyNonRefundable' + j + '" class="button black-gradient" value="NonRefundable"  onclick="CancellationpolicyRV(\'' + 'NonRefundable' + '\',\'' + j + '\')">'
                trRoomsRV += ' <input type="button"  id="chkCancellationpolicyRefundable' + j + '" class="button white-gradient" value="Refundable" onclick="CancellationpolicyRV(\'' + 'Refundable' + '\',\'' + j + '\')"></span></div>';
                trRoomsRV += ' </div>';

                trRoomsRV += '  <div id="divTaxRV' + j + '" class="three-columns twelve-columns-mobile" style="display:none" none"><label>Select Policy:</label><div class="full-width button-height SelBox"> <select id="sel_CancellationPolicyId' + j + '" name="validation-select" class="select cCancelationPolicies" onchange="AddCancelations(this.id,\'' + j + '\');"><option value="" selected="selected" disabled>Please select</option></select><ul id="iCancelationPolicies' + j + '" class="full-width sCanceltionPolicies"></ul></div></div>';
                trRoomsRV += ' </div>';

                trRoomsRV += '<div  class="columns">';

                trRoomsRV += '  <div id="Type' + j + '" class="four-columns twelve-columns-mobile"><label>Rate Type:</label><div class="full-width button-height SelBox"><select id="sel_Type' + j + '" name="validation-select" class="select full-width"><option value="-" disabled>Please select</option><option value="Contracted">Contracted</option><option value="Promo">Promo</option><option value="Tactical">Tactical</option></select></div></div>';
                trRoomsRV += '  <div class="eight-columns twelve-columns-mobile"><label>Rate Note:</label><textarea type="text" id="txtRateNote' + j + '" col="6" class="input full-width autoexpanding"></textarea></div>';
                trRoomsRV += '</div>';

                trRoomsRV += '<div  class="columns">';
                trRoomsRV += '  <div class="twelve-columns">';
                trRoomsRV += ' <label>Meals Rate</label><hr/>'
                trRoomsRV += '</div>'
                trRoomsRV += '</div>'

                trRoomsRV += '<div  class="columns">';
                trRoomsRV += '  <div class="two-columns"><label class="input-info">Meal</label></div>'
                trRoomsRV += '  <div class="two-columns"><label class="input-info">Adult Rate</label></div>'
                trRoomsRV += '  <div class="two-columns"><label class="input-info">CWB Rate</label></div>'
                trRoomsRV += '  <div class="two-columns"><label class="input-info">CNB Rate</label></div>'
                trRoomsRV += '  <div class="two-columns"><label class="input-info">Type</label></div>'
                trRoomsRV += '  <div class="two-columns"><label class="input-info">Compulsary</label></div>'
                trRoomsRV += '</div>'

                trRoomsRV += '<div class="AddMeal' + 'Nor_' + arrRoomList[j].RoomTypeID + '"></div>';

                trRoomsRV += '<div class="columns">';
                trRoomsRV += '<div class="twelve-columns">'
                trRoomsRV += '<label>AddOns Rate</label><hr/>'
                trRoomsRV += '</div>'
                trRoomsRV += '</div>'

                trRoomsRV += ' </div>';

               
                //End Inputs in RV      
                var arrDays = [];
                arrDays = DaysRV;
                //trRoomsRV += '<div id="RoomRatesByDayRV' + j + '" style="display:none"><div class="with-padding">';
                ////Day wise Rates         

                //for (var wr = 0; wr < WeekRateType.length; wr++) {

                //    trRoomsRV += '<div  class="columns">';
                //    trRoomsRV += '<div class="new-row two-columns"><label>' + WeekRateType[wr] + '</label></div>';
                //    trRoomsRV += ''
                //    for (var wd = 0; wd < WeekDaysRV.length; wd++) {
                //        trRoomsRV += '<div class="one-columns"><small>' + WeekDaysRV[wd] + '</small><br/><input type="text" onkeypress="return event.charCode >= 31 && event.charCode <= 57"  id="txtRV' + WeekDaysRV[wd] + j + wr + '" class="input cWeekDaysRV" placeholder="' + WeekDaysRV[wd] + '" style="width:40px;"></div>';
                //    }
                //    trRoomsRV += '</div>';
                //}

                //trRoomsRV += '</div></div></dd>';
                // trRoomsRV += '</dl>';

              

                trRoomsRV += '<div class="with-padding"  id="AddOns' + j + arrRoomList[j].RoomTypeID + '">';
                //trRoomsRV += '<label>AddOns & Tax Policy:</label>'
                GetAddDetails(j, "Nor_" + arrRoomList[j].RoomTypeID, 'AddOns' + j + arrRoomList[j].RoomTypeID);
                trRoomsRV += '</div>';
                trRoomsRV += '  </div>';
                trRoomsRV += '  </div>';
                trRoomsRV += '  </div>';
                trRoomsRV += ' </div>';
                $("#tabRoomtype" + j).append(trRoomsRV);

              
                //End Day wise Rates     
                //GetOtherRates();
            }

        }
        //End Data Rate Validity

        for (var j = 0; j < divRoomType.length; j++) {
            var trRoomsSD = '';
            //Data Special Dates
            if (DateRangeSD.length != 0) {
                trRoomsSD = '';
                trRoomsSD += '<div class="tabs-content">';
                trRoomsSD += '<div class="with-padding">';
                trRoomsSD += '<div class="row block table-head margin-bottom">';
                trRoomsSD += '<span class="block-title anthracite-gradient glossy"><h3>SPECIAL DATES</h3>';
                for (var i = 0; i < DateRangeSD.length; i++) {
                    trRoomsSD += '<small><strong>(</strong>' + DateRangeSD[i] + '<strong>)</strong>,</small>';
                }
                trRoomsSD += '  <div class="clear"></div>';
                trRoomsSD += '    </span>';

                trRoomsSD += '<div class="with-padding withpad tabwhite" id="RoomContentSD' + j + '" >';
                //Inputs in SD          
                trRoomsSD += ' <div class="columns">';
                trRoomsSD += '  <div class="two-columns four-columns-tablet six-columns-mobile"><label>Room Rate(RR):</label><input type="text" value="0" onkeypress="return event.charCode >= 31 && event.charCode <= 57" id="txtRoomRateSD' + j + '" class="input full-width cInputs"> </div>';
                trRoomsSD += '  <div class="two-columns four-columns-tablet six-columns-mobile"><label>Extra Bed(EB):</label> <input type="text" onkeypress="return event.charCode >= 31 && event.charCode <= 57" id="txtExtraBedSD' + j + '" class="input full-width cInputs"> </div>';
                trRoomsSD += '  <div class="two-columns four-columns-tablet six-columns-mobile"><label>CWB:</label><input type="text" onkeypress="return event.charCode >= 31 && event.charCode <= 57" id="txtChildWithBedSD' + j + '" class="input full-width cInputs"></div>';
                trRoomsSD += '  <div class="two-columns four-columns-tablet six-columns-mobile"><label>CNB:</label><input type="text" onkeypress="return event.charCode >= 31 && event.charCode <= 57" id="txtChildNoBedSD' + j + '" class="input full-width cInputs"></div>';
                trRoomsSD += '  <div class="two-columns four-columns-tablet six-columns-mobile"><label>Booking Code:</label><input type="text"  id="txtBookingCodeSD' + j + '" class="input full-width"></div>';
                //trRoomsSD += '  <div class="new-row three-columns"><small class="input-info">Cancellation Policy:</small> <select id="sel_CancellationPolicyIdSD' + j + '" name="validation-select" class="select full-width cCancelationPolicies" onchange="AddCancelationsSD(this.id,' + j + ');"><option value="" selected="selected" disabled>Please select</option></select><ul id="iCancelationPoliciesSD' + j + '" class="full-width sCanceltionPoliciesSD"> </ul> </div>';
                trRoomsSD += '  <div class="four-columns four-columns-tablet six-columns-mobile"><input type="checkbox" id="chkSDforDaywise' + j + '" onchange="CheckSDforDaywise(' + j + ')" /><label for="chkSDforDaywise' + j + '" class="label">Day Wise Rates</label></div>';
                trRoomsSD += '  </div>';

                trRoomsSD += '<div id="RoomRatesByDaySD' + j + '" style="display:none"><div class="with-padding">';
                //Day wise Rates SD         
                for (var wr = 0; wr < WeekRateType.length; wr++) {
                    trRoomsSD += '<div  class="columns">';
                    trRoomsSD += '<div class="new-row two-columns"><label>' + WeekRateType[wr] + '</label></div>';
                    for (var wd = 0; wd < WeekDaysSD.length; wd++) {
                        trRoomsSD += '<div class="one-columns"><small>' + WeekDaysSD[wd] + '</small><br/><input type="text" onkeypress="return event.charCode >= 31 && event.charCode <= 57" id="txtSD' + WeekDaysSD[wd] + j + wr + '" class="input cWeekDaysSD" placeholder="' + WeekDaysSD[wd] + '" style="width:40px;"></div>';
                    }
                    trRoomsSD += '</div>';
                }
                trRoomsSD += '</div></div>';

                trRoomsSD += '  <div  class="columns">';
                trRoomsSD += '  <div class="three-columns  twelve-columns-mobile"><label>Offer:</label><div class="full-width button-height SelBox"> <select id="sel_OfferTypeSD' + j + '" name="validation-select" class="select full-width sel_OfferTyp"  onchange="AddOfferSD(this.id,\'' + j + '\');"><option value="" selected="selected" disabled>Please select</option></select><ul id="iOfferSD' + j + '" class="full-width sel_OfferTyp"></ul></div></div>';
               
                trRoomsSD += ' <div class="two-columns twelve-columns-mobile"><label>Offer Nights:</label><div class="full-width button-height SelBox"><select id="OfferNightsSD' + j +'" name="validation-select" class="select full-width sel_Offernight"><option value="0" selected="selected">0</option><option value="1" >1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option></select></div></div>';

                
                trRoomsSD += '  <div class="four-columns  twelve-columns-mobile"><label>Cancellation Policy:</label><div id="ButtonSearch" style="height: 20px"><span class="button-group">'
                trRoomsSD += ' <input type="button" id="chkCancellationpolicyNonRefundableSD' + j + '" class="button black-gradient" value="NonRefundable"  onclick="CancellationpolicySD(\'' + 'NonRefundable' + '\',\'' + j + '\')">'
                trRoomsSD += ' <input type="button"  id="chkCancellationpolicyRefundableSD' + j + '" class="button white-gradient" value="Refundable" onclick="CancellationpolicySD(\'' + 'Refundable' + '\',\'' + j + '\')"></span></div>';
                trRoomsSD += '  </div>';

                trRoomsSD += '  <div id="divTaxSD' + j + '" class="three-columns twelve-columns-mobile" style="display:none" none"><label>Select Policy:</label> <div class="full-width button-height SelBox"><select id="sel_CancellationPolicyIdSD' + j + '"name="validation-select" class="select cCancelationPolicies" onchange="AddCancelationsSD(this.id,\'' + j + '\');"><option value="" selected="selected" disabled>Please select</option></select><ul id="iCancelationPoliciesSD' + j + '" class="full-width sCanceltionPoliciesSD"> </ul></div></div>';
                trRoomsSD += ' </div>';

                trRoomsSD += '<div  class="columns">';
                trRoomsSD += '  <div class="eight-columns twelve-columns-mobile"><label>Rate Note:</label><textarea type="text" id="txtRateNoteSD' + j + '" col="6" class="input full-width autoexpanding"></textarea></div>';
                trRoomsSD += '  </div>';

               

                //trRoomsSD += '  <div class="new-row three-columns"><small class="input-info">Cancellation Policy:</small><br/><span class="button-group"><label for="chkCancellationpolicySD1" class="button blue-active"><input type="radio" checked name="button-radio chkCancellationpolicy" id="chkCancellationpolicySD1" value="NonRefundable" onclick="CancellationpolicySD(this.value);">Non Refundable</label><label for="chkCancellationpolicySD2" class="button blue-active"><input type="radio" name="button-radio chkCancellationpolicy" id="chkCancellationpolicySD2" value="Refundable" onclick="CancellationpolicySD(this.value);">Refundable</label></span></div>';
                //trRoomsSD += '  <div id="divTaxSD" class="three-columns" style="display:none" none"><div class="three-columns" style="width:100%"><small class="input-info">Select Policy:</small><select id="sel_CancellationpolicySD" name="validation-select" class="select full-width" onchange="GetCancellatiocSD(this.value")><option value="-" selected="selected" disabled>Please select</option></div></div>';
                //trRoomsSD += ' </div>';
                //End Inputs in SD    

                trRoomsSD += '<div  class="columns with-padding">';
                trRoomsSD += '<div class="twelve-columns">'
                trRoomsSD += '<label class="input-info">AddOns Rate</label><hr/>'
                trRoomsSD += '</div>'
                trRoomsSD += '</div>'

                trRoomsSD += '<div class="with-padding"  id="AddSPOns' + j + arrRoomList[j].RoomTypeID + '">';
              //  trRoomsSD += '<small class="input-info">AddOns & Tax Policy:</small>'
                GetAddDetails(j, "sp_" + arrRoomList[j].RoomTypeID, 'AddSPOns' + j + arrRoomList[j].RoomTypeID);
                trRoomsSD += '</div>';
                trRoomsSD += '</div>';
                trRoomsSD += '</div>';
                trRoomsSD += '</div>';
                trRoomsRV += ' </div>';
                //End Data Special Dates
                $("#tabRoomtype" + j).append(trRoomsSD);
               
            }

        }
        GetCancelationPolicies();
        GetMasterOffer();
        GetOtherRates();
       
        //getRoomsUp();
        // GetMasterOfferSD();
    }, 1000);


}

function GetMasterOffer() {
    var Country = [];
    var ChkList = $('.chkNationality option:checked').map(function () {
        return this.value;
    }).get();
    //var Country;
    for (var i = 0; i < ChkList.length; i++) {
        if (ChkList[i] == "All") {
            Country += '';
        }
        else {
            Country += ChkList[i] + ",";
        }

    }

    CheckinRV = Rate[0].Checkin;
    CheckoutRV = Rate[0].Checkout;

    var ddlRequest = '';
    var data = {
        CheckinRV: CheckinRV,
        CheckoutRV: CheckoutRV,
        Country: Country
    }
    $.ajax({
        type: "POST",
        url: "MasterHotelOfferHandler.asmx/GetMasterOfferUp",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                $(".sel_OfferTyp select").empty();
                arrMasterOffer = result.arrOffer;
                // $(".cCancelationPolicies select").empty();
                if (arrMasterOffer.length > 0) {
                    ddlRequest += '<option value="" selected="selected" disabled>Please select</option>';
                    for (i = 0; i < arrMasterOffer.length; i++)
                    {
                        ddlRequest += '<option value="' + arrMasterOffer[i].OfferId + '">' + arrMasterOffer[i].SeasonName + '</option>';
                    }
                    // $(".sel_OfferTypeSD").append(ddlRequest);
                    $(".sel_OfferTyp select").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}

function AddCancelations(name, count) {
    var row = "";
    var CancelPoliciesValue = "";
    var CancelPoliciesText = "";

    var e = document.getElementById(name);
    var value = e.options[e.selectedIndex].value;
    var text = e.options[e.selectedIndex].text;
    CancelPoliciesValue = value;
    CancelPoliciesText = text;
    row += '<li id="' + value + '">' + CancelPoliciesText + ' <span class="deleteMe" onclick="DeleteLi();">X</span></li>'

    $("#iCancelationPolicies" + count).append(row);

    // $("#txtCancellationRV" + count).append(row);
    GetCancelationsRV(value, count);
}

function GetCancelationsRV(CancelId, count) {
    var ddlRequest = '';

    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetCancelations",
        data: '{"CancelId":"' + CancelId + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCancelationPolicies = result.NewCancelationPolicies;
                $("#sel_CancellationPolicyId" + count).empty();
                if (arrCancelationPolicies.length > 0) {
                    for (i = 0; i < arrCancelationPolicies.length; i++) {

                        ddlRequest += '<option value="' + arrCancelationPolicies[i].CancelationID + '">' + arrCancelationPolicies[i].CancelationPolicy + '</option>';
                    }
                    $("#sel_CancellationPolicyId" + count).append(ddlRequest);

                }
            }
        },
        error: function () {
        }
    });
}

function GetCancelationsSD(CancelId, count) {
    var ddlRequest = '';

    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetCancelations",
        data: '{"CancelId":"' + CancelId + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCancelationPolicies = result.NewCancelationPolicies;
                $("#sel_CancellationPolicyIdSD" + count).empty();
                if (arrCancelationPolicies.length > 0) {
                    for (i = 0; i < arrCancelationPolicies.length; i++) {

                        ddlRequest += '<option value="' + arrCancelationPolicies[i].CancelationID + '">' + arrCancelationPolicies[i].CancelationPolicy + '</option>';
                    }

                    $("#sel_CancellationPolicyIdSD" + count).append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}

function AddCancelationsSD(name, count) {
    var row = "";
    var CancelPoliciesValue = "";
    var CancelPoliciesText = "";

    var e = document.getElementById(name);
    var value = e.options[e.selectedIndex].value;
    var text = e.options[e.selectedIndex].text;
    CancelPoliciesValue = value;
    CancelPoliciesText = text;
    row += '<li id="' + value + '">' + CancelPoliciesText + ' <span class="deleteMe" onclick="DeleteLi();">X</span></li>'

    $("#iCancelationPoliciesSD" + count).append(row);
    GetCancelationsSD(value, count);
}

function AddOffer(name, count) {
    var row = "";
    var CancelPoliciesValue = "";
    var CancelPoliciesText = "";

    var e = document.getElementById(name);
    var value = e.options[e.selectedIndex].value;
    var text = e.options[e.selectedIndex].text;
    OfferValue = value;
    OfferText = text;
    row += '<li id="' + value + '">' + OfferText + ' <span class="deleteMe" onclick="DeleteLi();">X</span></li>'

    $("#iOffer" + count).append(row);
    // GetCancelationsSD(value, count);
}

function AddOfferSD(name, count) {
    var row = "";
    var CancelPoliciesValue = "";
    var CancelPoliciesText = "";

    var e = document.getElementById(name);
    var value = e.options[e.selectedIndex].value;
    var text = e.options[e.selectedIndex].text;
    OfferValue = value;
    OfferText = text;
    row += '<li id="' + value + '">' + OfferText + ' <span class="deleteMe" onclick="DeleteLi();">X</span></li>'

    $("#iOfferSD" + count).append(row);
    // GetCancelationsSD(value, count);
}

function DeleteLi() {
    $(".deleteMe").on("click", function () {
        $(this).closest("li").remove();
    });
}

function CheckRVforDaywise(id) {
    var ChkSD = $("#chkRVforDaywise" + id);

    if (ChkSD.is(":checked")) {
        $("#RoomRatesByDayRV" + id)[0].style.display = "";
        for (var rt = 0; rt < arrRoomTypes.length; rt++) {
            $("#txtRV" + WeekDaysRV[0] + rt + 0).val($("#txtRoomRate" + rt).val());
            $("#txtRV" + WeekDaysRV[1] + rt + 0).val($("#txtRoomRate" + rt).val());
            $("#txtRV" + WeekDaysRV[2] + rt + 0).val($("#txtRoomRate" + rt).val());
            $("#txtRV" + WeekDaysRV[3] + rt + 0).val($("#txtRoomRate" + rt).val());
            $("#txtRV" + WeekDaysRV[4] + rt + 0).val($("#txtRoomRate" + rt).val());
            $("#txtRV" + WeekDaysRV[5] + rt + 0).val($("#txtRoomRate" + rt).val());
            $("#txtRV" + WeekDaysRV[6] + rt + 0).val($("#txtRoomRate" + rt).val());

            $("#txtRV" + WeekDaysRV[0] + rt + 1).val($("#txtExtraBed" + rt).val());
            $("#txtRV" + WeekDaysRV[1] + rt + 1).val($("#txtExtraBed" + rt).val());
            $("#txtRV" + WeekDaysRV[2] + rt + 1).val($("#txtExtraBed" + rt).val());
            $("#txtRV" + WeekDaysRV[3] + rt + 1).val($("#txtExtraBed" + rt).val());
            $("#txtRV" + WeekDaysRV[4] + rt + 1).val($("#txtExtraBed" + rt).val());
            $("#txtRV" + WeekDaysRV[5] + rt + 1).val($("#txtExtraBed" + rt).val());
            $("#txtRV" + WeekDaysRV[6] + rt + 1).val($("#txtExtraBed" + rt).val());
        }
    }
    else {
        $("#RoomRatesByDayRV" + id)[0].style.display = "none";
    }
}

function CheckSDforDaywise(id) {
    var ChkSD = $("#chkSDforDaywise" + id);

    if (ChkSD.is(":checked")) {
        $("#RoomRatesByDaySD" + id)[0].style.display = "";
        for (var rt = 0; rt < arrRoomTypes.length; rt++) {
            $("#txtSD" + WeekDaysRV[0] + rt + 0).val($("#txtRoomRateSD" + rt).val());
            $("#txtSD" + WeekDaysRV[1] + rt + 0).val($("#txtRoomRateSD" + rt).val());
            $("#txtSD" + WeekDaysRV[2] + rt + 0).val($("#txtRoomRateSD" + rt).val());
            $("#txtSD" + WeekDaysRV[3] + rt + 0).val($("#txtRoomRateSD" + rt).val());
            $("#txtSD" + WeekDaysRV[4] + rt + 0).val($("#txtRoomRateSD" + rt).val());
            $("#txtSD" + WeekDaysRV[5] + rt + 0).val($("#txtRoomRateSD" + rt).val());
            $("#txtSD" + WeekDaysRV[6] + rt + 0).val($("#txtRoomRateSD" + rt).val());

            $("#txtSD" + WeekDaysRV[0] + rt + 1).val($("#txtExtraBedSD" + rt).val());
            $("#txtRV" + WeekDaysRV[1] + rt + 1).val($("#txtExtraBedSD" + rt).val());
            $("#txtSD" + WeekDaysRV[2] + rt + 1).val($("#txtExtraBedSD" + rt).val());
            $("#txtSD" + WeekDaysRV[3] + rt + 1).val($("#txtExtraBedSD" + rt).val());
            $("#txtSD" + WeekDaysRV[4] + rt + 1).val($("#txtExtraBedSD" + rt).val());
            $("#txtSD" + WeekDaysRV[5] + rt + 1).val($("#txtExtraBedSD" + rt).val());
            $("#txtSD" + WeekDaysRV[6] + rt + 1).val($("#txtExtraBedSD" + rt).val());
        }
    }
    else {
        $("#RoomRatesByDaySD" + id)[0].style.display = "none";
    }
}

function checkTaxApllied(val) {
    if (val == "NO") {
        $("#divTax")[0].style.display = "";
    }
    else {
        $("#divTax")[0].style.display = "none";
    }
}


function CancellationpolicyRV(val, ID) {

    if (val == "Refundable") {
        $("#divTaxRV" + ID)[0].style.display = "";
        //$('#chkCancellationpolicyNonRefundable' + ID).prop('disabled', true);
        //$('#chkCancellationpolicyRefundable' + ID).prop('disabled', false);
        // $('#chkCancellationpolicyNonRefundable' + ID).addClass("white");
        $("#chkCancellationpolicyRefundable" + ID).removeClass("button white-gradient");
        $("#chkCancellationpolicyRefundable" + ID).addClass("button black-gradient");
        $("#chkCancellationpolicyNonRefundable" + ID).removeClass("button black-gradient");
        $("#chkCancellationpolicyNonRefundable" + ID).addClass("button white-gradient");

    }
    else {
        $("#divTaxRV" + ID)[0].style.display = "none";
        //$('#chkCancellationpolicyRefundable' + ID).prop('disabled', true);
        //$('#chkCancellationpolicyNonRefundable' + ID).prop('disabled', false);
        $("#chkCancellationpolicyRefundable" + ID).removeClass("button black-gradient");
        $("#chkCancellationpolicyRefundable" + ID).addClass("button white-gradient");
        $("#chkCancellationpolicyNonRefundable" + ID).removeClass("button white-gradient");
        $("#chkCancellationpolicyNonRefundable" + ID).addClass("button black-gradient");
    }
}

function CancellationpolicySD(val, ID) {
    if (val == "Refundable") {
        $("#divTaxSD" + ID)[0].style.display = "";
        $("#chkCancellationpolicyRefundableSD" + ID).removeClass("button white-gradient");
        $("#chkCancellationpolicyRefundableSD" + ID).addClass("button black-gradient");
        $("#chkCancellationpolicyNonRefundableSD" + ID).removeClass("button black-gradient");
        $("#chkCancellationpolicyNonRefundableSD" + ID).addClass("button white-gradient");
    }
    else {
        $("#divTaxSD" + ID)[0].style.display = "none";
        $("#chkCancellationpolicyRefundableSD" + ID).removeClass("button black-gradient");
        $("#chkCancellationpolicyRefundableSD" + ID).addClass("button white-gradient");
        $("#chkCancellationpolicyNonRefundableSD" + ID).removeClass("button white-gradient");
        $("#chkCancellationpolicyNonRefundableSD" + ID).addClass("button black-gradient");
    }
}


function SaveRates() {

    //  SaveInventory();
    var ChkList = $('.chkNationality option:checked').map(function () {
        return this.value;
    }).get();

    var Nationalities = '';
    for (var i = 0; i < ChkList.length; i++) {
        if (ChkList[i] == "All" || ChkList[i] == "Unselect") {
            Nationalities += '';
        }
        else {
            Nationalities += ChkList[i] + "^";
        }

    }

    var Sunday = '', Monday = '', Tuesday = '', Wednesday = '', Thursday = '', Friday = '', Saturday = '';
    var RVCheckin = '', RVCheckout = '';


    //Common Data
    var supl = document.getElementById("sel_Supplier");
    var Supplier = supl.options[supl.selectedIndex].value;

    //var Ctry = document.getElementById("sel_Nationality");
    //var Country = Ctry.options[Ctry.selectedIndex].value;
    Country = Nationalities;

    var Crency = document.getElementById("sel_CurrencyCode");
    var Currency = Crency.options[Crency.selectedIndex].value;

    var Mealp = document.getElementById("sel_MealPlan");
    var MealPlan = Mealp.options[Mealp.selectedIndex].value;

    var TaxIncluded, TaxType, TaxOn, TaxValue, TaxDetails;
    //var RateInclude = $("#txtRateInclude").val();
    //var RateExclude = $("#txtRateExclude").val();
    var RtInc = $('.cInclutions');
    var RateInclude = '';
    for (var ri = 0; ri < RtInc.length; ri++) {
        RateInclude += RtInc[ri].innerHTML.split('<')[0] + "^";
    }

    var RtExc = $('.cExclutions');
    var RateExclude = '';
    for (var re = 0; re < RtExc.length; re++) {
        RateExclude += RtExc[re].innerHTML.split('<')[0] + "^";
    }

    if (Supplier == "-") {
        Success("Please Select Supplier")
        return false;
    }
    if (Country == "") {
        Success("Please Select Country")
        return false;
    }
    if (Currency == "-") {
        Success("Please Select Currency")
        return false;
    }
    if (MealPlan == "-") {
        Success("Please Select MealPlan")
        return false;
    }

    var MinStay = $("#txtminStay").val();
    //var MaxStay = $("#txtmaxStay").val();

    var inputs = $(".cInputs")
    var AllInputs = "";
    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].value == "") {
            AllInputs = "Blank";
        }
    }
    if (AllInputs == "Blank") {
        Success("Please Fill Rates of All Rooms");
        return false;
    }

    if ($("#chkTaxIncluded1").is(":checked")) {
        TaxIncluded = "YES";
        TaxType = 0;
        TaxOn = 0;
        TaxValue = 0;
        TaxDetails = 0;

    }
    else {
        TaxIncluded = "NO";

        var TxType = document.getElementById("sel_TaxType");
        TaxType = TxType.options[TxType.selectedIndex].value;

        var TxOn = document.getElementById("sel_TaxOn");
        TaxOn = TxOn.options[TxOn.selectedIndex].value;

        TaxValue = $("#txtTaxValue").val();
        TaxDetails = $("#txtTaxDetails").val();
    }
    var CheckinR = [], CheckoutR = [], RateTypeCode = [];

    for (var fr = 0; fr < CheckinRV.length; fr++) {
        CheckinR.push(CheckinRV[fr]);
        CheckoutR.push(CheckoutRV[fr]);
    }


    var RoomTypeID = [], RoomTypeName = [];
    var RoomRates = [], ExtraBeds = [], CWB = [], CWN = [], BookingCode = [], CancelPolicy = [], Offers = [], RateNote = [], RateType = [], CWN = [], CWN = [], InventoryType = [], NoOfInventoryRoom = [];
    var Daywise = [], Sunday = [], Monday = [], Tuesday = [], Wednesday = [], Thursday = [], Friday = [], Saturday = [];

    var ListAddOnsRates = new Array();
    var divData = "";
    var Roomdivs = $(".divContents")
    for (var c = 0; c < Roomdivs.length; c++) {
        RoomTypeID.push(arrRoomList[c].RoomTypeID);
        RoomTypeName.push(arrRoomList[c].RoomType);
        RateTypeCode.push(RateTypes[0]);
        RoomRates.push($("#txtRoomRate" + c).val());
        ExtraBeds.push($("#txtExtraBed" + c).val());
        CWB.push($("#txtChildWithBed" + c).val());
        CWN.push($("#txtChildNoBed" + c).val());
        BookingCode.push($("#txtBookingCode" + c).val());
        RateNote.push($("#txtRateNote" + c).val());
        RateType.push($("#sel_Type" + c).val());
        ListAddOnsRates.push(GenrateAddOnsRates('divRow_' + c + 'Nor_' + arrRoomList[c].RoomTypeID, arrRoomList[c].RoomTypeID));
        InventoryType.push($("#sel_InventoryRV" + c).val());
        NoOfInventoryRoom.push($("#sel_divinventoryRoomRV" + c).val());

        var cpolicies = "";
        var cppolicy = $('#iCancelationPolicies' + c)[0].children
        for (var cp = 0; cp < cppolicy.length; cp++) {
            cpolicies += cppolicy[cp].id + ",";
        }
        cpolicies.replace(/^,|,$/g, "");
        //var ClPolicy = document.getElementById("sel_CancellationPolicyId" + c);
        if (cpolicies != null)
            //CancelPolicy.push(ClPolicy.options[ClPolicy.selectedIndex].value);
            CancelPolicy.push(cpolicies);
        else
            divData = "NotApplied"

        var offers = "";
        var coffer = $('#iOffer' + c)[0].children
        for (var of = 0; of < coffer.length; of++) {
            offers += coffer[of].id + ",";
        }
        offers.replace(/^,|,$/g, "");
        if (offers != null)
            Offers.push(offers);
        else
            divData = "NotApplied"
        //var ofrs = document.getElementById("sel_OfferType" + c);
        //if (ofrs != null)
        //    Offers.push(ofrs.options[ofrs.selectedIndex].value);
        //else
        //divData = "NotApplied"

        var Day = '', sun = '', mon = '', tue = '', wed = '', thu = '', fri = '', sat = '';
        var sunRR = '', monRR = '', tueRR = '', wedRR = '', thuRR = '', friRR = '', satRR = '';
        var sunEB = '', monEB = '', tueEB = '', wedEB = '', thuEB = '', friEB = '', satEB = '';
        if ($("#chkRVforDaywise" + c).is(":checked")) {
            Day = "Yes";
            sunRR = $("#txtRV" + WeekDaysRV[0] + c + 0).val();
            monRR = $("#txtRV" + WeekDaysRV[1] + c + 0).val();
            tueRR = $("#txtRV" + WeekDaysRV[2] + c + 0).val();
            wedRR = $("#txtRV" + WeekDaysRV[3] + c + 0).val();
            thuRR = $("#txtRV" + WeekDaysRV[4] + c + 0).val();
            friRR = $("#txtRV" + WeekDaysRV[5] + c + 0).val();
            satRR = $("#txtRV" + WeekDaysRV[6] + c + 0).val();

            sunEB = $("#txtRV" + WeekDaysRV[0] + c + 1).val();
            monEB = $("#txtRV" + WeekDaysRV[1] + c + 1).val();
            tueEB = $("#txtRV" + WeekDaysRV[2] + c + 1).val();
            wedEB = $("#txtRV" + WeekDaysRV[3] + c + 1).val();
            thuEB = $("#txtRV" + WeekDaysRV[4] + c + 1).val();
            friEB = $("#txtRV" + WeekDaysRV[5] + c + 1).val();
            satEB = $("#txtRV" + WeekDaysRV[6] + c + 1).val();

            sun = sunRR + ',' + sunEB;
            mon = monRR + ',' + monEB;
            tue = tueRR + ',' + tueEB;
            wed = wedRR + ',' + wedEB;
            thu = thuRR + ',' + thuEB;
            fri = friRR + ',' + friEB;
            sat = satRR + ',' + satEB;
        }
        else {
            Day = "No";
            sun = 0;
            mon = 0;
            tue = 0;
            wed = 0;
            thu = 0;
            fri = 0;
            sat = 0;
        }
        Daywise.push(Day);
        Sunday.push(sun);
        Monday.push(mon);
        Tuesday.push(tue);
        Wednesday.push(wed);
        Thursday.push(thu);
        Friday.push(fri);
        Saturday.push(sat);
    }
    if (divData == "NotApplied") {
        Success("Please Select Cancelation Poliies or Offers");
        return false;
    }
    if (MinStay == "") {
        MinStay = 0;
    }

    //if (MaxStay == "") {
    //    alert("Please Enter Max Stay")
    //    return false;
    //}
    var data =
         {
             HotelCode: HotelCode,
             Supplier: Supplier,
             Country: Country,
             Currency: Currency,
             MealPlan: MealPlan,
             CheckinR: CheckinR,
             CheckoutR: CheckoutR,
             RateInclude: RateInclude,
             RateExclude: RateExclude,
             MinStay: MinStay,
             //MaxStay: MaxStay,
             MaxStay: 0,
             TaxIncluded: TaxIncluded,
             TaxType: TaxType,
             TaxOn: TaxOn,
             TaxValue: TaxValue,
             TaxDetails: TaxDetails,
             RoomTypeID: RoomTypeID,
             RateTypeCode: RateTypeCode,
             RoomRates: RoomRates,
             ExtraBeds: ExtraBeds,
             CWB: CWB,
             CWN: CWN,
             BookingCode: BookingCode,
             CancelPolicy: CancelPolicy,
             Offers: Offers,
             RateNote: RateNote,
             RateType:RateType,
             Daywise: Daywise,
             Sunday: Sunday,
             Monday: Monday,
             Tuesday: Tuesday,
             Wednesday: Wednesday,
             Thursday: Thursday,
             Friday: Friday,
             Saturday: Saturday,
             ListAddOnsRates: ListAddOnsRates
             //InventoryType: InventoryType,
             //NoOfInventoryRoom: NoOfInventoryRoom
         }
    $.ajax({
        type: "POST",
        url: "handler/RoomHandler.asmx/AddRates",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                //  alert("General Rates Added Sucessfully")
                if ($("#chkSpecialDate").is(":checked")) {
                    SaveSLRates();
                }
                else {
                    Success("Rates Added Sucessfully");
                    setTimeout(function () {
                        window.location.href = "Ratelist.aspx?sHotelID=" + HotelCode + "&HName=" + GetQueryStringParams("HotelName")
                    }, 500);
                }

            }
            else {
                Success("Something Went Wrong")
                return false;
            }
        }
    })
}

function SaveSLRates() {
    var Sunday = '', Monday = '', Tuesday = '', Wednesday = '', Thursday = '', Friday = '', Saturday = '';

    var ChkList = $('.chkNationality option:checked').map(function () {
        return this.value;
    }).get();

    var Nationalities = '';
    for (var i = 0; i < ChkList.length; i++) {
        if (ChkList[i] == "All" || ChkList[i] == "Unselect") {
            Nationalities += '';
        }
        else {
            Nationalities += ChkList[i] + "^";
        }

    }

    //Common Data
    var supl = document.getElementById("sel_Supplier");
    var Supplier = supl.options[supl.selectedIndex].value;

    //var Ctry = document.getElementById("sel_Nationality");
    var Country = Nationalities;

    var Crency = document.getElementById("sel_CurrencyCode");
    var Currency = Crency.options[Crency.selectedIndex].value;

    var Mealp = document.getElementById("sel_MealPlan");
    var MealPlan = Mealp.options[Mealp.selectedIndex].value;

    var TaxIncluded, TaxType, TaxOn, TaxValue, TaxDetails;
    //var RateInclude = $("#txtRateInclude").val();
    //var RateExclude = $("#txtRateExclude").val();
    var RtInc = $('.cInclutions');
    var RateInclude = '';
    for (var ri = 0; ri < RtInc.length; ri++) {
        RateInclude += RtInc[ri].innerHTML.split('<')[0] + "^";
    }

    var RtExc = $('.cExclutions');
    var RateExclude = '';
    for (var re = 0; re < RtExc.length; re++) {
        RateExclude += RtExc[re].innerHTML.split('<')[0] + "^";
    }

    var MinStay = $("#txtminStay").val();
    //var MaxStay = $("#txtmaxStay").val();


    if ($("#chkTaxIncluded1").is(":checked")) {
        TaxIncluded = "YES";
        TaxType = 0;
        TaxOn = 0;
        TaxValue = 0;
        TaxDetails = 0;

    }
    else {
        TaxIncluded = "NO";
        var TxType = document.getElementById("sel_TaxType");
        TaxType = TxType.options[TxType.selectedIndex].value;

        var TxOn = document.getElementById("sel_TaxOn");
        TaxOn = TxOn.options[TxOn.selectedIndex].value;

        TaxValue = $("#txtTaxValue").val();
        TaxDetails = $("#txtTaxDetails").val();
    }
    var CheckinR = [], CheckoutR = [], RateTypeCode = [];

    for (var fr = 0; fr < CheckinSD.length; fr++) {
        CheckinR.push(CheckinSD[fr]);
        CheckoutR.push(CheckoutSD[fr]);
    }


    var RoomTypeID = [], RoomTypeName = [];
    var RoomRates = [], ExtraBeds = [], CWB = [], CWN = [], BookingCode = [], CancelPolicy = [], Offers = [], RateNote = [], CWN = [], CWN = [], InventoryType = [], NoOfInventoryRoom = [];
    var Daywise = [], Sunday = [], Monday = [], Tuesday = [], Wednesday = [], Thursday = [], Friday = [], Saturday = [];

    var ListAddOnsRates = new Array();
    var divData = "";
    var Roomdivs = $(".divContents")
    for (var c = 0; c < Roomdivs.length; c++) {
        RoomTypeID.push(arrRoomList[c].RoomTypeID);
        RoomTypeName.push(arrRoomTypes[c].RoomType);
        RateTypeCode.push(RateTypes[1]);
        RoomRates.push($("#txtRoomRateSD" + c).val());
        ExtraBeds.push($("#txtExtraBedSD" + c).val());
        CWB.push($("#txtChildWithBedSD" + c).val());
        CWN.push($("#txtChildNoBedSD" + c).val());
        BookingCode.push($("#txtBookingCodeSD" + c).val());
        RateNote.push($("#txtRateNoteSD" + c).val());
        ListAddOnsRates.push(GenrateAddOnsRates('divRow_' + c + 'sp_' + arrRoomList[c].RoomTypeID, arrRoomList[c].RoomTypeID));
        InventoryType.push($("#sel_InventorySD" + c).val());
        NoOfInventoryRoom.push($("#sel_divinventoryRoomSD" + c).val());

        var cpolicies = "";
        var cppolicy = $('#iCancelationPoliciesSD' + c)[0].children;
        for (var cp = 0; cp < cppolicy.length; cp++) {
            cpolicies += cppolicy[cp].id + ",";
        }
        cpolicies.replace(/^,|,$/g, "");;
        //var ClPolicy = document.getElementById("sel_CancellationPolicyIdSD" + c);
        if (cpolicies != null)
            //CancelPolicy.push(ClPolicy.options[ClPolicy.selectedIndex].value);
            CancelPolicy.push(cpolicies);
        else
            divData = "NotApplied"


        var offers = "";
        var coffer = $('#iOfferSD' + c)[0].children;
        for (var of = 0; of < coffer.length; of++) {
            offers += coffer[of].id + ",";
        }
        offers.replace(/^,|,$/g, "");
        if (offers != null)
            Offers.push(offers);
        else
            divData = "NotApplied"

        //var ofrs = document.getElementById("sel_OfferTypeSD" + c);
        //if (ofrs != null)
        //    Offers.push(ofrs.options[ofrs.selectedIndex].value);
        //else
        //divData = "NotApplied"


        var Day = '', sun = '', mon = '', tue = '', wed = '', thu = '', fri = '', sat = '';
        var sunRR = '', monRR = '', tueRR = '', wedRR = '', thuRR = '', friRR = '', satRR = '';
        var sunEB = '', monEB = '', tueEB = '', wedEB = '', thuEB = '', friEB = '', satEB = '';
        if ($("#chkSDforDaywise" + c).is(":checked")) {
            Day = "Yes";
            sunRR = $("#txtSD" + WeekDaysSD[0] + c + 0).val();
            monRR = $("#txtSD" + WeekDaysSD[1] + c + 0).val();
            tueRR = $("#txtSD" + WeekDaysSD[2] + c + 0).val();
            wedRR = $("#txtSD" + WeekDaysSD[3] + c + 0).val();
            thuRR = $("#txtSD" + WeekDaysSD[4] + c + 0).val();
            friRR = $("#txtSD" + WeekDaysSD[5] + c + 0).val();
            satRR = $("#txtSD" + WeekDaysSD[6] + c + 0).val();

            sunEB = $("#txtSD" + WeekDaysSD[0] + c + 1).val();
            monEB = $("#txtSD" + WeekDaysSD[1] + c + 1).val();
            tueEB = $("#txtSD" + WeekDaysSD[2] + c + 1).val();
            wedEB = $("#txtSD" + WeekDaysSD[3] + c + 1).val();
            thuEB = $("#txtSD" + WeekDaysSD[4] + c + 1).val();
            friEB = $("#txtSD" + WeekDaysSD[5] + c + 1).val();
            satEB = $("#txtSD" + WeekDaysSD[6] + c + 1).val();

            sun = sunRR + ',' + sunEB;
            mon = monRR + ',' + monEB;
            tue = tueRR + ',' + tueEB;
            wed = wedRR + ',' + wedEB;
            thu = thuRR + ',' + thuEB;
            fri = friRR + ',' + friEB;
            sat = satRR + ',' + satEB;
        }
        else {
            Day = "No";
            sun = 0;
            mon = 0;
            tue = 0;
            wed = 0;
            thu = 0;
            fri = 0;
            sat = 0;
        }
        Daywise.push(Day);
        Sunday.push(sun);
        Monday.push(mon);
        Tuesday.push(tue);
        Wednesday.push(wed);
        Thursday.push(thu);
        Friday.push(fri);
        Saturday.push(sat);

    }
    if (divData == "NotApplied") {
        Success("Please Select Cancelation Poliies or Offers");
        return false;
    }
    if (MinStay == "") {
        MinStay = 0;
    }
    var data =
         {
             HotelCode: HotelCode,
             Supplier: Supplier,
             Country: Country,
             Currency: Currency,
             MealPlan: MealPlan,
             CheckinR: CheckinR,
             CheckoutR: CheckoutR,
             RateInclude: RateInclude,
             RateExclude: RateExclude,
             MinStay: MinStay,
             // MaxStay: MaxStay,
             MaxStay: 0,
             TaxIncluded: TaxIncluded,
             TaxType: TaxType,
             TaxOn: TaxOn,
             TaxValue: TaxValue,
             TaxDetails: TaxDetails,
             RoomTypeID: RoomTypeID,
             RateTypeCode: RateTypeCode,
             RoomRates: RoomRates,
             ExtraBeds: ExtraBeds,
             CWB: CWB,
             CWN: CWN,
             BookingCode: BookingCode,
             CancelPolicy: CancelPolicy,
             Offers: Offers,
             RateNote: RateNote,
             Daywise: Daywise,
             Sunday: Sunday,
             Monday: Monday,
             Tuesday: Tuesday,
             Wednesday: Wednesday,
             Thursday: Thursday,
             Friday: Friday,
             Saturday: Saturday,
             ListAddOnsRates: ListAddOnsRates
             //InventoryType: InventoryType,
             // NoOfInventoryRoom: NoOfInventoryRoom

         }
    $.ajax({
        type: "POST",
        url: "handler/RoomHandler.asmx/AddRates",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Rates Added Sucessfully");
                setTimeout(function () {
                    window.location.href = "Ratelist.aspx?sHotelID=" + HotelCode + "&HName=" + GetQueryStringParams("HotelName")
                }, 500);
            }
        }
    })
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function SaveInventory() {


    var inventoryType = "";
    var cInventoryType = $('#sel_InventoryRV' + c)[0].children
    for (var Iv = 0; Iv < cInventoryType.length; Iv++) {
        inventoryType += cInventoryType[Iv].id + ",";
    }
    inventoryType.replace(/^,|,$/g, "");
    if (inventoryType != null)
        InventoryType.push(inventoryType);
    else
        divData = "NotApplied"


    var noOfRoom = "";
    var cNoOfRoom = $('#sel_divinventoryRoomR' + c)[0].children
    for (var Nr = 0; Nr < cNoOfRoom.length; Nr++) {
        noOfRoom += cNoOfRoom[Nr].id + ",";
    }
    noOfRoom.replace(/^,|,$/g, "");
    if (noOfRoom != null)
        NoOfRoom.push(noOfRoom);
    else
        divData = "NotApplied"
    var InventoryType = $('#sel_InventoryRV' + c).val();
    var NoOfRoom = $('#sel_divinventoryRoomR' + c).val();

}

function GetNationality(Nationality) {

    try {

        for (var i = 0; i < Nationality.length; i++) {
            var Nationality = Nationality.split('^');

            if (Nationality[i] == "AllCountry")
                var Name = "For All Nationality"
            else {

                var checkclass = document.getElementsByClassName('check');

                var Name = $.grep(arrCountry, function (p) { return p.Country == Nationality[i]; })
                                                .map(function (p) { return p.Countryname; });
            }


            $("#drp_Nationality .select span")[0].textContent = Name;
            for (var i = 0; i <= Nationality.length; i++) {
                //// $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').addclass('checked');         
                $('input[value="' + Nationality[i] + '"][class="chkNationality"]').prop("selected", true);
                $("#sel_Nationality").val(Nationality);
                pNationality += Nationality[i] + ",";
                var selected = [];

            }
        }

    }
    catch (ex)
    { }

}

function UpdateDates() {
    // $(".divContents").empty();
    var divRoomType = $(".divContents")
    AddRVDates();
    setTimeout(function () {
        AddSDDates();
    }, 1000);

    for (var i = 0; i < divRoomType.length; i++) {

        $("#txtRoomRate" + i).val(Rate[0].RR);
        $("#txtExtraBed" + i).val(Rate[0].EB);
        $("#txtChildWithBed" + i).val(Rate[0].CWB);
        $("#txtChildNoBed" + i).val(Rate[0].CNB);
        $("#txtBookingCode" + i).val(Rate[0].BookingCode);

        $("#OfferNights" + i).val(Rate[0].OfferNight);
       
    }
}

function GetOtherRates() {

    var Currency = Rate[0].CurrencyCode;
    var Supplier = Rate[0].SupplierId;
    var Country = Rate[0].ValidNationality;
    var MealPlan = Rate[0].MealPlan;
    var CheckinR = Rate[0].Checkin;
    var CheckoutR = Rate[0].Checkout;

    var data =
      {
          HotelCode: HotelCode,
          Supplier: Supplier,
          Country: Country,
          Currency: Currency,
          MealPlan: MealPlan,
          CheckinR: CheckinR,
          CheckoutR: CheckoutR
      }

    $.ajax({
        type: "POST",
        url: "handler/RoomHandler.asmx/GetOtherRates",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var Rate = result.listRate;
                var Tax = result.ListTax;
                var divRoomType = $(".divContents")
                AddRVDates();
                setTimeout(function () {
                    AddSDDates();
                }, 1000);

                for (var i = 0; i < Rate.length; i++) {

                    $("#txtRoomRate" + i).val(Rate[i].RR);
                    $("#txtExtraBed" + i).val(Rate[i].EB);
                    $("#txtChildWithBed" + i).val(Rate[i].CWB);
                    $("#txtChildNoBed" + i).val(Rate[i].CNB);
                    $("#txtBookingCode" + i).val(Rate[i].BookingCode);

                    $("#sel_Type" + i).val(Rate[i].Type);

                    $("#OfferNights" + i).val(Rate[i].OfferNight);
                    $("#OfferNightsSD" + i).val(Rate[i].OfferNight);

                    //$("#OfferNights"+i+".select span")[0].textContent = Rate[i].OfferNight;
                    //$("#OfferNightsSD" + i + " .select span")[0].textContent = Rate[i].OfferNight;

                    if (Rate[i].DayWise == "Yes") {
                        $("#txtRoomRate" + i).val(0);
                        //$("#txtExtraBed" + i).val(Rate[i].MonRR);
                        //$("#txtChildWithBed" + i).val(Rate[i].MonRR);
                        //$("#txtChildNoBed" + i).val(Rate[i].MonRR);
                        $("#chkRVforDaywise" + i).click();
                        $("#txtRV" + WeekDaysRV[0] + i + 0).val(Rate[i].SunRR);
                        $("#txtRV" + WeekDaysRV[1] + i + 0).val(Rate[i].MonRR);
                        $("#txtRV" + WeekDaysRV[2] + i + 0).val(Rate[i].TueRR);
                        $("#txtRV" + WeekDaysRV[3] + i + 0).val(Rate[i].WedRR);
                        $("#txtRV" + WeekDaysRV[4] + i + 0).val(Rate[i].ThuRR);
                        $("#txtRV" + WeekDaysRV[5] + i + 0).val(Rate[i].FriRR);
                        $("#txtRV" + WeekDaysRV[6] + i + 0).val(Rate[i].SatRR);

                    }

                    if (Rate[i].CancellationPolicyId != 1) {
                        $("#chkCancellationpolicyRefundable" + i).click();
                        GetUpCancellation(Rate[i].CancellationPolicyId, i)
                    }
                    if (Rate[i].OfferId != "")
                        GetUpOffer(Rate[i].OfferId, i);

                    //var TaxDetails = $.grep(Tax, function (p) { return p.ServiceID == Rate[i].HotelRateID; })
                    //  .map(function (p) { return p; });

                    //if (TaxDetails != "") {
                    //    GetAddOnType(TaxDetails[0].TaxID, i, Rate[i].RoomId)

                    //    var SelTaxOn="";
                    //    for (var j = 0; j < TaxDetails.length; j++)
                    //    {
                    //        SelTaxOn += TaxDetails[j].TaxOnID + '^';
                    //    }

                    //    GetTaxOn(SelTaxOn, i)
                    //}

                }
            }
        }
    })
}

function GetTaxOn(SelTaxOn, i)
{
    var Taxon = SelTaxOn.split('^');
    for (var i = 0; i < Taxon.length; i++) {

        var Name = $.grep(arrTax, function (p) { return p.ID == Taxon[i]; })
                      .map(function (p) { return p.Name; });
    }
}

function GetAddOnType(TaxDetails, i, RoomId) {

    try
    {
        var Name = $.grep(arrAddOns, function (p) { return p.ID == TaxDetails; })
                   .map(function (p) { return p.AddOnName; });
        var checkclass = document.getElementsByClassName('check');

        $("#DivAddons .select span")[0].textContent = Name;
        $('input[value="' + TaxDetails + '"][class="sel_Addons"]').prop("selected", true);
        $("#sel_Addons" + i + "Nor_" + RoomId + "").val(TaxDetails);
        //pCities += TaxDetails + "^";
        //var selected = [];

    }

    catch (ex)
    { }
}


function GetUpCancellation(CancellationPolicyId, i) {
    try {

        for (var j = 0; j <= CancellationPolicyId.length - 2; j++) {
            if (j == 0)
                var CancellationPolicyId = CancellationPolicyId.split(',');
            var Name = $.grep(arrCancelationPolicies, function (p) { return p.CancelationID == CancellationPolicyId[j]; })
                       .map(function (p) { return p.CancelationPolicy; });

            var checkclass = document.getElementsByClassName('check');
            if (CancellationPolicyId[j] == "1") {
                $('#chkCancellationpolicyNonRefundable' + i).click();
            }
            else
                AddCancelationsUp(Name, CancellationPolicyId[j], i);
        }
    }

    catch (ex)
    { }
}

function GetUpOffer(OfferId, i) {
    try {

        for (var j = 0; j <= OfferId.length - 2; j++) {
            if (j == 0)
                var OfferId = OfferId.split(',');
            var Name = $.grep(arrMasterOffer, function (p) { return p.Sid == OfferId[j]; })
                       .map(function (p) { return p.SeasonName; });

            var checkclass = document.getElementsByClassName('check');
            AddOfferUp(Name, OfferId[j], i);
        }
    }

    catch (ex)
    { }
}


function AddOfferUp(name, OfferId, count) {
    var row = "";
    var CancelPoliciesValue = "";
    var CancelPoliciesText = "";

    OfferValue = OfferId;
    OfferText = name;
    row += '<li id="' + OfferValue + '">' + OfferText + ' <span class="deleteMe" onclick="DeleteLi();">X</span></li>'

    $("#iOffer" + count).append(row);
    // GetCancelationsSD(value, count);
}
function AddCancelationsUp(name, CancellationPolicyId, count) {

    var row = "";
    var CancelPoliciesValue = "";
    var CancelPoliciesText = "";

    var value = CancellationPolicyId;
    var text = name[0];
    CancelPoliciesValue = value;
    CancelPoliciesText = text;
    row += '<li id="' + value + '">' + CancelPoliciesText + ' <span class="deleteMe" onclick="DeleteLi();">X</span></li>'

    $("#iCancelationPolicies" + count).append(row);

    // $("#txtCancellationRV" + count).append(row);
    GetCancelationsRV(value, count);


}

function UpdateRates() {

    var CheckinR = [], CheckoutR = [], RateTypeCode = [];
    var ChkList = $('.chkNationality option:checked').map(function () {
        return this.value;
    }).get();

    var Nationalities = '';
    for (var i = 0; i < ChkList.length; i++) {
        if (ChkList[i] == "All" || ChkList[i] == "Unselect") {
            Nationalities += '';
        }
        else {
            Nationalities += ChkList[i] + "^";
        }

    }

    //var Currency = Rate[0].CurrencyCode;
    //var Supplier = Rate[0].SupplierId;
    //var Country = Nationalities;
    //var MealPlan = Rate[0].MealPlan;

    //Common Data
    var supl = document.getElementById("sel_Supplier");
    var Supplier = supl.options[supl.selectedIndex].value;

    //var Ctry = document.getElementById("sel_Nationality");
    //var Country = Ctry.options[Ctry.selectedIndex].value;
    var Country = Nationalities;

    var Crency = document.getElementById("sel_CurrencyCode");
    var Currency = Crency.options[Crency.selectedIndex].value;

    var Mealp = document.getElementById("sel_MealPlan");
    var MealPlan = Mealp.options[Mealp.selectedIndex].value;

    if (Supplier == "-") {
        Success("Please Select Supplier")
        return false;
    }
    if (Country == "") {
        Success("Please Select Country")
        return false;
    }
    if (Currency == "-") {
        Success("Please Select Currency")
        return false;
    }
    if (MealPlan == "-") {
        Success("Please Select MealPlan")
        return false;
    }

    var CheckinR = $('#txtFromRV0').val();
    var CheckoutR = $('#txtToRV0').val();

    var Sunday = '', Monday = '', Tuesday = '', Wednesday = '', Thursday = '', Friday = '', Saturday = '';
    var RVCheckin = '', RVCheckout = '';


    //Common Data

    if (Country == "") {
        Success("Please Select Country")
        return false;
    }

    var inputs = $(".cInputs")
    var AllInputs = "";
    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].value == "") {
            AllInputs = "Blank";
        }
    }
    if (AllInputs == "Blank") {
        Success("Please Fill Rates of All Rooms");
        return false;
    }



    var RoomTypeID = [], RoomTypeName = [], RateTypeCode = [];
    var RoomRates = [], ExtraBeds = [], CWB = [], CWN = [], BookingCode = [], CancelPolicy = [], Offers = [], RateNote = [], CWN = [], CWN = [], InventoryType = [], NoOfInventoryRoom = [], OfferNight = [];
    var Daywise = [], Sunday = [], Monday = [], Tuesday = [], Wednesday = [], Thursday = [], Friday = [], Saturday = [];
    var TypeRate = [];
    var ListAddOnsRates = new Array();
    var divData = "";
    var Roomdivs = $(".divContents")
    for (var c = 0; c < Roomdivs.length; c++) {
        RoomTypeID.push(arrRoomList[c].RoomTypeID);
        RoomTypeName.push(arrRoomList[c].RoomType);
        RateTypeCode.push(TType);
        RoomRates.push($("#txtRoomRate" + c).val());
        ExtraBeds.push($("#txtExtraBed" + c).val());
        CWB.push($("#txtChildWithBed" + c).val());
        CWN.push($("#txtChildNoBed" + c).val());
        BookingCode.push($("#txtBookingCode" + c).val());
        RateNote.push($("#txtRateNote" + c).val());
        TypeRate.push($("#sel_Type" + c).val());
        ListAddOnsRates.push(GenrateAddOnsRates('divRow_' + c + 'Nor_' + arrRoomList[c].RoomTypeID, arrRoomList[c].RoomTypeID, "Nor_" + arrRoomList[c].RoomTypeID)); //"Nor_" + i + '_' + arrRoomList[j].RoomTypeID

        OfferNight.push($("#OfferNights" + c).val());

        var btnchek = $("#chkCancellationpolicyRefundable" + c)[0].className;

        var cpolicies = "";
        if (btnchek == "button white-gradient") {
            var cppolicy = "1";
            CancelPolicy.push(cppolicy);
        }
        else {
            var cppolicy = $('#iCancelationPolicies' + c)[0].children
            for (var cp = 0; cp < cppolicy.length; cp++) {
                cpolicies += cppolicy[cp].id + ",";
            }
            cpolicies.replace(/^,|,$/g, "");
            //var ClPolicy = document.getElementById("sel_CancellationPolicyId" + c);
            if (cpolicies != null)
                //CancelPolicy.push(ClPolicy.options[ClPolicy.selectedIndex].value);
                CancelPolicy.push(cpolicies);
            else
                divData = "NotApplied"
        }




        var offers = "";
        var coffer = $('#iOffer' + c)[0].children
        for (var of = 0; of < coffer.length; of++) {
            offers += coffer[of].id + ",";
        }
        offers.replace(/^,|,$/g, "");
        if (offers != null)
            Offers.push(offers);
        else
            divData = "NotApplied"
        //var ofrs = document.getElementById("sel_OfferType" + c);
        //if (ofrs != null)
        //    Offers.push(ofrs.options[ofrs.selectedIndex].value);
        //else
        //divData = "NotApplied"

        var Day = '', sun = '', mon = '', tue = '', wed = '', thu = '', fri = '', sat = '';
        var sunRR = '', monRR = '', tueRR = '', wedRR = '', thuRR = '', friRR = '', satRR = '';
        var sunEB = '', monEB = '', tueEB = '', wedEB = '', thuEB = '', friEB = '', satEB = '';
        if ($("#chkRVforDaywise" + c).is(":checked")) {
            Day = "Yes";
            sunRR = $("#txtRV" + WeekDaysRV[0] + c + 0).val();
            monRR = $("#txtRV" + WeekDaysRV[1] + c + 0).val();
            tueRR = $("#txtRV" + WeekDaysRV[2] + c + 0).val();
            wedRR = $("#txtRV" + WeekDaysRV[3] + c + 0).val();
            thuRR = $("#txtRV" + WeekDaysRV[4] + c + 0).val();
            friRR = $("#txtRV" + WeekDaysRV[5] + c + 0).val();
            satRR = $("#txtRV" + WeekDaysRV[6] + c + 0).val();

            sunEB = $("#txtRV" + WeekDaysRV[0] + c + 1).val();
            monEB = $("#txtRV" + WeekDaysRV[1] + c + 1).val();
            tueEB = $("#txtRV" + WeekDaysRV[2] + c + 1).val();
            wedEB = $("#txtRV" + WeekDaysRV[3] + c + 1).val();
            thuEB = $("#txtRV" + WeekDaysRV[4] + c + 1).val();
            friEB = $("#txtRV" + WeekDaysRV[5] + c + 1).val();
            satEB = $("#txtRV" + WeekDaysRV[6] + c + 1).val();

            sun = sunRR + ',' + sunEB;
            mon = monRR + ',' + monEB;
            tue = tueRR + ',' + tueEB;
            wed = wedRR + ',' + wedEB;
            thu = thuRR + ',' + thuEB;
            fri = friRR + ',' + friEB;
            sat = satRR + ',' + satEB;
        }
        else {
            Day = "No";
            sun = 0;
            mon = 0;
            tue = 0;
            wed = 0;
            thu = 0;
            fri = 0;
            sat = 0;
        }
        Daywise.push(Day);
        Sunday.push(sun);
        Monday.push(mon);
        Tuesday.push(tue);
        Wednesday.push(wed);
        Thursday.push(thu);
        Friday.push(fri);
        Saturday.push(sat);
    }
    if (divData == "NotApplied") {
        Success("Please Select Cancelation Poliies or Offers");
        return false;
    }

    var data =
         {
             HotelCode: HotelCode,
             Supplier: Supplier,
             Country: Country,
             Currency: Currency,
             MealPlan: MealPlan,
             CheckinR: CheckinR,
             CheckoutR: CheckoutR,
             RoomTypeID: RoomTypeID,
             RateTypeCode: RateTypeCode,
             RoomRates: RoomRates,
             ExtraBeds: ExtraBeds,
             CWB: CWB,
             CWN: CWN,
             BookingCode: BookingCode,
             CancelPolicy: CancelPolicy,
             Offers: Offers,
             RateNote: RateNote,
             Daywise: Daywise,
             Sunday: Sunday,
             Monday: Monday,
             Tuesday: Tuesday,
             Wednesday: Wednesday,
             Thursday: Thursday,
             Friday: Friday,
             Saturday: Saturday,
             TypeRate: TypeRate,
             ListAddOnsRates: ListAddOnsRates,
             OfferNight: OfferNight
             //RateID: RateID
         }
    $.ajax({
        type: "POST",
        url: "handler/RoomHandler.asmx/UpdateRates",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Rates Added Sucessfully");
                setTimeout(function () {
                    window.location.href = "Ratelist.aspx?sHotelID=" + HotelCode + "&HName=" + GetQueryStringParams("HotelName")
                }, 500);
            }
            else {
                Success("Something Went Wrong")
                return false;
            }
        }
    })
}
