﻿$(document).ready(function () {


    // $("#tbl_HotelList").dataTable().fnClearTable();
    //$("#tbl_HotelList").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetMappedCities",
        //data: '{"dFrom":"' + dFrom + '","dTo":"' + dTo + '"}',
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCitiesList = result.MappedCitiesList;

                $("#tbl_CityList tbody").remove();
                //Hotels List
                if (arrCitiesList.length > 0) {
                    trRequest += '<tbody>';
                    for (i = 0; i < arrCitiesList.length; i++) {
                        trRequest += '<tr>';
                        trRequest += '<td scope="col"">' + arrCitiesList[i].CityName + '</td>';
                        trRequest += '<td scope="col" >' + arrCitiesList[i].CountryName + '</td>';
                        trRequest += '<td scope="col" >' + arrCitiesList[i].CityCode + '</td>';
                        trRequest += '<td scope="col" >' + arrCitiesList[i].DotwCode + '</td>';
                        trRequest += '<td scope="col" >' + arrCitiesList[i].ExpediaCode + '</td>';
                        trRequest += '<td scope="col" >' + arrCitiesList[i].GRNCode + '</td>';
                     
                        trRequest += '</tr>';


                    }
                    trRequest += '</tbody>';
                    $("#tbl_CityList").append(trRequest);
                }
                $("#tbl_HotelList").dataTable(
                    {
                        "bLength": false,
                         bSort: false, sPaginationType: 'full_numbers',

                    });
            }
            else if (result.retCode == 0) {
                $("#tbl_HotelList tbody").remove();
                var trRequest = '<tbody>';
                trRequest += '<tr><td align="center" style="padding-top: 2%" colspan="8"><span><b>No record found</b></span></td></tr>';
                trRequest += '</tbody>';
                $("#tbl_HotelList").append(trRequest);
            }
        }
    });

    //}
});