﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Invoice.aspx.cs" Inherits="CutAdmin.Invoice" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="ckeditor/adapters/jquery.js"></script>
    <script type="text/javascript" src="scripts/Cancellation.js"></script>
    <script type="text/javascript" src="scripts/Invoice.js?v=1.9"></script>

    <%--<link href="../updates/update1/css/style01.css" rel="stylesheet" />--%>
    <style type="text/css">
        div.scrollingDiv {
            width: auto;
            /*height: 680px;*/
            height:auto;
            /*overflow: scroll;*/
            /*padding-right: 20px;*/
        }

        div.scrollingDiv2 {
            width: auto;
            height: auto;
            overflow-x: scroll;
            padding-right: 20px;
        }
    </style>
    <script type="text/javascript" src="../scripts/jQuery.print.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#datepicker3").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "d-m-yy"
            });
            $("#datepicker4").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "d-m-yy"
            });
            $("#datepicker5").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "d-m-yy"
            });
            $("#datepickerHold").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "d-m-yy"
            });
            $("#ConfirmDate").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "d-m-yy"
            });
            //GetAgentList();
        });

    </script>
    <script type="text/javascript">
        var tpj = jQuery;
        tpj(document).ready(function () {
            debugger;
            tpj("#txt_AgencyList").autocomplete({
                source: function (request, response) {
                    tpj.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "RoleManagementHandler.asmx/GetAgentRoleList",
                        data: "{'name':'" + document.getElementById('txt_AgencyList').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 1,
                select: function (event, ui) {
                    debugger;
                    tpj('#hdnDCode').val(ui.item.id);

                }
            });
        });
    </script>

    <script type="text/javascript">
        var tpj = jQuery;
        tpj(document).ready(function () {
            debugger;
            tpj("#txt_Reference").autocomplete({
                source: function (request, response) {
                    tpj.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "RoleManagementHandler.asmx/GetRefNo",
                        data: "{'InvoiceID':'" + document.getElementById('txt_Reference').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 1,
                select: function (event, ui) {
                    debugger;
                    tpj('#hdnDCodee').val(ui.item.id);

                }
            });
        });
    </script>

    <script type="text/javascript">
        var tpj = jQuery;
        tpj(document).ready(function () {
            debugger;
            tpj("#txt_PName").autocomplete({
                source: function (request, response) {
                    tpj.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "RoleManagementHandler.asmx/LoadPassengerName",
                        data: "{'PName':'" + document.getElementById('txt_PName').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 1,
                select: function (event, ui) {
                    debugger;
                    tpj('#hdnPCode').val(ui.item.id);

                }
            });
        });
    </script>

    <script type="text/javascript">
        var tpj = jQuery;
        tpj(document).ready(function () {
            debugger;
            tpj("#txt_hotel").autocomplete({
                source: function (request, response) {
                    tpj.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "RoleManagementHandler.asmx/LoadHotelName",
                        data: "{'HName':'" + document.getElementById('txt_hotel').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 1,
                select: function (event, ui) {
                    debugger;
                    tpj('#hdnHNCode').val(ui.item.id);

                }
            });
        });
    </script>

    <script type="text/javascript">
        var tpj = jQuery;
        tpj(document).ready(function () {
            debugger;
            tpj("#txt_SupRefNo").autocomplete({
                source: function (request, response) {
                    tpj.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "RoleManagementHandler.asmx/LoadSupRefNo",
                        data: "{'SupRefNo':'" + document.getElementById('txt_SupRefNo').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 1,
                select: function (event, ui) {
                    debugger;
                    tpj('#hdnSRNCode').val(ui.item.id);

                }
            });
        });
    </script>

    <script type="text/javascript">
        var tpj = jQuery;
        tpj(document).ready(function () {
            debugger;
            tpj("#txt_location").autocomplete({
                source: function (request, response) {
                    tpj.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "RoleManagementHandler.asmx/LoadLocation",
                        data: "{'Location':'" + document.getElementById('txt_location').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 1,
                select: function (event, ui) {
                    debugger;
                    tpj('#hdnLCode').val(ui.item.id);

                }
            });
        });
    </script>
   <%-- <script type="text/javascript" src="../htmltable_export/tableExport.js"></script>
    <script type="text/javascript" src="../htmltable_export/jquery.base64.js"></script>--%>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />

   
        <%--<div class="pagecontainer offset-0" style="opacity: 1;">
                                            <div class="offset-0">
                                                <div class="form-style-10">
                                                    <h2 class="hd2">Hotel Booking Report<a class="right size13 white mt5" style="cursor: pointer" data-toggle="collapse" data-target="#filter">Filter</a> </h2>

                                                    <div class="collapse" id="filter" style="height: auto;">
                                                        <form class="form-horizontal">
                                                            <div class="inner-wrap">
                                                                <div class="form-group">
                                                                    <div class="col-sm-3">
                                                                        <label>Agency</label>
                                                                        <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>

                                                                        <input type="text" id="txt_AgencyList" class="form-control ui-autocomplete-input" autocomplete="off">
                                                                        <input type="hidden" id="hdnDCode">
                                                                    </div>

                                                                    <div class="col-sm-3">
                                                                        <label>Check-In</label>
                                                                        <div class="input-group">
                                                                            <input type="text" class="form-control mySelectCalendar" id="datepicker3" aria-describedby="inputGroupSuccess3Status" style="cursor: pointer; width: 116%">
                                                                            <span class="input-group-addon" style="cursor: pointer"></span>                                                                      </div>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <label>Check-Out</label>
                                                                        <div class="input-group">
                                                                            <input type="text" class="form-control mySelectCalendar" id="datepicker4" aria-describedby="inputGroupSuccess3Status" style="cursor: pointer; width: 116%">
                                                                            <span class="input-group-addon" style="cursor: pointer"><i class="fa fa-calendar"></i></span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <label>Passenger Name</label>
                                                                        <input type="text" id="txt_PName" class="form-control ui-autocomplete-input" autocomplete="off">
                                                                        <input type="hidden" id="hdnPCode">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-sm-3">
                                                                        <label>Booking Date</label>
                                                                        <div class="input-group">
                                                                            <input type="text" class="form-control mySelectCalendar" id="datepicker5" aria-describedby="inputGroupSuccess3Status" style="cursor: pointer; width: 116%">
                                                                            <span class="input-group-addon" style="cursor: pointer"><i class="fa fa-calendar"></i></span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <label>Reference No.</label>
                                                                        <input type="text" id="txt_Reference" class="form-control ui-autocomplete-input" autocomplete="off" />
                                                                        <input type="hidden" id="hdnDCodee">
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <label>Supplier Reference No.</label>
                                                                        <input type="text" id="txt_SupRefNo" class="form-control ui-autocomplete-input" autocomplete="off">
                                                                        <input type="hidden" id="hdnSRNCode">
                                                                    </div>

                                                                    <div class="col-sm-3">
                                                                        <label>Supplier</label>

                                                                        <select id="ddl_suppliers" class="form-control">
                                                                            <option selected="selected">All</option>
                                                                            <option>HOTELBEDS</option>
                                                                            <option>DOTW</option>
                                                                            <option>GRN</option>
                                                                            <option>MGH</option>
                                                                            <option>GTA</option>

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-sm-3">
                                                                        <label>Hotel Name</label>
                                                                        <input type="text" id="txt_hotel" class="form-control ui-autocomplete-input" autocomplete="off">
                                                                        <input type="hidden" id="hdnHNCode">
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <label>Location</label>
                                                                        <input type="text" id="txt_location" class="form-control ui-autocomplete-input" autocomplete="off">
                                                                        <input type="hidden" id="hdnLCode">
                                                                    </div>

                                                                    <div class="col-sm-3">
                                                                        <label>Reservation Status</label>

                                                                        <select id="Sel_Type" class="form-control">
                                                                            <option selected="selected">All</option>
                                                                            <option>Vouchered</option>
                                                                            <option>Cancelled</option>
                                                                            <option>On hold</option>
                                                                            <option>Incomplete</option>

                                                                        </select>
                                                                    </div>
                                                                    <div class="button-section text-center col-sm-3">
                                                                        <label>&nbsp;</label>
                                                                        <div class="clearfix" style="text-align: left">
                                                                            <button type="button" class="bluebtn" onclick="BookingListByDate();return false;">Search</button>
                                                                            <button type="button" class="bluebtn" style="margin-left: 20px" onclick="Reset()">Reset</button>
                                                                            <span class="icon-pdf right"></span>
                                                                            <span class="icon-excel right"></span>

                                                                        </div>

                                                                    </div>
                                                                    <div class="button-section text-center col-sm-2">
                                                                        <label>&nbsp;</label>
                                                                        <div class="margtop-15 clearfix">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-sm-3">
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                    </div>

                                                                    <div class="col-sm-3">

                                                                        <%-- <input id="btn_ViewReport" type="button" value="Search" class="btn-search margtop-2" style="margin-left: 15%;" onclick="BookingListByDate()" />
                                                                        <%--<img src="../images/Excel-2-icon.png" style="cursor: pointer" title="Export To Excel" id="btnExport" onclick="ExportBookingToExcel(); " height="42" width="42" />                                                                    <img src="../images/Excel-2-icon.png" style="cursor: pointer" title="Export To Excel" id="btnExcel" onclick="ExportAgencyStatement('Excel');" height="42" width="42" />
                                                                        <img src="../images/PDF-Viewer-icon.png" style="cursor: pointer" title="Export To PDF" id="btnPDF" onclick="ExportAgencyStatement('PDF');" height="42" width="42" />

                                                                    </div>
                                                                    <div class="button-section text-center col-sm-3">
                                                                    </div>
                                                                    <div class="button-section text-center col-sm-2">
                                                                        <label>&nbsp;</label>
                                                                        <div class="margtop-15 clearfix">
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                            </div>
                                                        </form>

                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="pagecontainer offset-0">

                                                <div class="sortable" style="overflow:scroll">
                                                    <div class="table-responsive">
                                                        <table id="tbl_Invoice" style="margin-bottom: 0px" class="size12 table table-striped custm-Table">
                                                            <thead>
                                                                <tr>
                                                                    <th>S.N </th>
                                                                    <th>Date</th>
                                                                    <th>Ref No.</th>
                                                                    <th>Agency</th>
                                                                    <th>Supplier Ref</th>
                                                                    <th>Passenger </th>
                                                                    <th>Hotel &amp; Location</th>
                                                                    <th>Check-In </th>
                                                                    <th>Check-Out </th>
                                                                    <th>Rooms </th>
                                                                    <th>Status </th>
                                                                    <th>Dead Line</th>
                                                                    <th>Amount </th>
                                                                    <th>Invoice </th>
                                                                    <th>Voucher </th>
                                                                    <th> </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            </tbody>
                                                            <!-- <tr><td align="center" colspan="15"> No Record Found</td></tr> -->
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>--%>


        <%--<div class="modal fade bs-example-modal-lg" id="AgencyDetailModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" style="width: 75%">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="close"><span aria-hidden="true">�</span></button>
                                                    <h4 class="modal-title" id="gridSystemModalLabel2" style="text-align: left">Invoice</h4>
                                                    <a href="#" id="hrefPrint"><span class="glyphicon glyphicon-print" title="Print"></span></a>
                                                </div>
                                                <div class="modal-body" id="AgencyDModal">
                                                    <div class="scrollingDiv">


                                                        <table class="table table-hover table-responsive" id="tbl_AgencyDetails">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="border-top: 1px solid #fff">
                                                                        <table>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="border-top: 1px solid #fff"><span class="text-left" style="font-weight: bold">Agency Details</span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Agency Name:</td>
                                                                                    <td><span id="spnIAgencyName"></span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Address:</td>
                                                                                    <td><span id="spnIAddress"></span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>City</td>
                                                                                    <td><span id="spnICity"></span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Country</td>
                                                                                    <td><span id="spnICountry"></span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Phone Number:</td>
                                                                                    <td><span id="spnIPhoneNumber"></span></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                    <td style="border-top: 1px solid #fff">
                                                                        <table>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="border-top: 1px solid #fff"><span class="text-left" style="font-weight: bold">Agency Details</span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Hotel Name:</td>
                                                                                    <td><span id="spnIHotelName"></span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Check-In:</td>
                                                                                    <td><span id="spnICheckIn"></span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Check-Out:</td>
                                                                                    <td><span id="spnICheckOut"></span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Number Of Nights:</td>
                                                                                    <td><span id="spnINumberOfNights"></span></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                    <td style="border-top: 1px solid #fff">
                                                                        <table>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="border-top: 1px solid #fff"><span class="text-left" style="font-weight: bold">Agency Details</span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Booking Date:</td>
                                                                                    <td><span id="spnIBookingDate"></span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Booking Ref:</td>
                                                                                    <td><span id="spnIBookingRef"></span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Agent Ref:</td>
                                                                                    <td><span id="spnIAgentRef"></span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Voucher Id:</td>
                                                                                    <td><span id="spnIVoucherId"></span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Agent Code:</td>
                                                                                    <td><span id="spnIAgentCode"></span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Consultant:</td>
                                                                                    <td><span id="spnIConsultant"></span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Passenger:</td>
                                                                                    <td><span id="spnIPassenger"></span></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="tbl_InvoiceDetails">
                                                            <tbody>
                                                                <tr>
                                                                    <td><b>Sr. Number</b>
                                                                    </td>
                                                                    <td><b>Room Type</b>
                                                                    </td>
                                                                    <td><b>Room Number</b>
                                                                    </td>
                                                                    <td><b>Rate Break Ups</b>
                                                                    </td>
                                                                    <td><b>Amount</b>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <table style="float: right">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Sub Total Amount:</td>
                                                                    <td><span id="spnISubTotalAmount"></span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Extra Charges:</td>
                                                                    <td><span id="spnIExtraCharges"></span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Total:</td>
                                                                    <td><span id="spnITotal"></span></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <div style="text-align: center">
                                                            <span style="float: left">Date: </span>
                                                            <span style="margin: 0 auto">Customer Signature</span>
                                                            <span style="float: right;">Accountant   </span>
                                                        </div>

                                                        <hr>
                                                        <div style="text-align: center;"><a data-original-title="" href="#" title="ClickUrTrip.com">ClickUrTrip.com</a></div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>--%>

        <%--<div class="modal fade bs-example-modal-lg" id="AgencyBookingModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" style="width: 60%">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="close"><span aria-hidden="true">�</span></button>

                                                </div>
                                                <div class="modal-body">
                                                    <div class="frow2" style="overflow-x: scroll;">
                                                        <h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left; background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;">Booking Details</h4>
                                                        <div>
                                                            <table>

                                                                <tbody>
                                                                    <tr>
                                                                        <td><span class="dark bold">Hotel: </span></td>
                                                                        <td style="padding-left: 15px;"><span class="dark" id="dspHotelName1"></span></td>
                                                                        <td style="padding-left: 20px;"><span class="dark bold">Check-In: </span></td>
                                                                        <td style="padding-left: 15px;"><span class="dark" id="dspCheckin1"></span></td>
                                                                        <td style="padding-left: 20px;"><span class="dark bold">Check-Out: </span></td>
                                                                        <td style="padding-left: 15px;"><span class="dark" id="dspCheckout1"></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><span class="dark bold">Passenger: </span></td>
                                                                        <td style="padding-left: 15px;"><span class="dark" id="dspPasssengerName1"></span></td>
                                                                        <td style="padding-left: 20px;"><span class="dark bold">Location: </span></td>
                                                                        <td style="padding-left: 15px;"><span class="dark" id="dspLocation1"></span></td>
                                                                        <td style="padding-left: 20px;"><span class="dark bold">Nights: </span></td>
                                                                        <td style="padding-left: 15px;"><span class="dark" id="dspNights1"></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><span class="dark bold">Booking ID: </span></td>
                                                                        <td style="padding-left: 15px;"><span class="dark" id="dspBookingID1"></span></td>
                                                                        <td style="padding-left: 20px;"><span class="dark bold">Booking Date: </span></td>
                                                                        <td style="padding-left: 15px;"><span class="dark" id="dspBookingDate1"></span></td>
                                                                        <td style="padding-left: 20px;"><span class="dark bold">Amount: </span></td>
                                                                        <td style="padding-left: 15px;"><span class="dark" id="dspBookingAmount1"></span></td>
                                                                    </tr>

                                                                </tbody>
                                                            </table>
                                                            <div class="clearfix"></div>
                                                            <br>
                                                        </div>

                                                        <div style="text-align: center;"><a data-original-title="" href="#" title="ClickUrTrip.com">ClickUrTrip.com</a></div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>--%>


        <%--<div class="modal fade bs-example-modal-lg" id="ConfirmAlertForOnHoldModel" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content" style="width: 700px;">
                                                <div class="modal-header" style="border-bottom: 1px solid #fff">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">�</span></button>
                                                    <h4 class="modal-title" id="gridSystemModalLabel5" style="text-align: left"></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Booking Details</b></div>
                                                    <div class="frow2">
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td><span class="dark bold">Hotel: </span></td>
                                                                    <td style="padding-left: 15px;"><span class="dark" id="dspHotelName"></span></td>
                                                                    <td style="padding-left: 20px;"><span class="dark bold">Check-In: </span></td>
                                                                    <td style="padding-left: 15px;"><span class="dark" id="dspCheckin"></span></td>
                                                                    <td style="padding-left: 20px;"><span class="dark bold">Check-Out: </span></td>
                                                                    <td style="padding-left: 15px;"><span class="dark" id="dspCheckout"></span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span class="dark bold">Passenger: </span></td>
                                                                    <td style="padding-left: 15px;"><span class="dark" id="dspPasssengerName"></span></td>
                                                                    <td style="padding-left: 20px;"><span class="dark bold">Location: </span></td>
                                                                    <td style="padding-left: 15px;"><span class="dark" id="dspLocation"></span></td>
                                                                    <td style="padding-left: 20px;"><span class="dark bold">Nights: </span></td>
                                                                    <td style="padding-left: 15px;"><span class="dark" id="dspNights"></span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span class="dark bold">Booking ID: </span></td>
                                                                    <td style="padding-left: 15px;"><span class="dark" id="dspBookingID"></span></td>
                                                                    <td style="padding-left: 20px;"><span class="dark bold">Booking Date: </span></td>
                                                                    <td style="padding-left: 15px;"><span class="dark" id="dspBookingDate"></span></td>
                                                                    <td style="padding-left: 20px;"><span class="dark bold">Amount: </span></td>
                                                                    <td style="padding-left: 15px;"><span class="dark" id="dspBookingAmount"></span></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <div class="clearfix"></div>
                                                        <br>
                                                    </div>
                                                    <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Cancellation Policy</b></div>
                                                    <div class="frow2">
                                                        <div id="dspCancellationPolicy">
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <br/>
                                                    </div>
                                                    <div class="alert alert-warning" id="dspAlertMessage" style="display: none"></div>


                                                    <div class="frow2" style="text-align: center;">
                                                        <img style="-webkit-user-select: none; margin-top: 10px; display: none;" id="dlgLoader" src="../images/loader.gif" /><br/>
                                                        <input type="button" value="Confirm Hold Booking" onclick="ProceedToConfirmation();" id="btnConfirmBooking" class="btn-search4 margtop20" style="float: none;"/>
                                                        <input type="button" value="Cancel Hold Booking" onclick="ProceedToCancelHoldBooking();" id="btnCancelHoldBooking" class="btn-search4 margtop20" style="float: none;"/>
                                                       
                                                        <input type="button" value="Update Hold Date" onclick="OpenUpdateHoldDateModal();" id="btnUpdateHoldDateModal" class="btn-search4 margtop20" style="float: none;"/>

                                                        
                                                         <input type="hidden" id="hndReferenceCode"/>
                                                        <input type="hidden" id="hndCancellationAmount"/>
                                                        <input type="hidden" id="hndReservatonID"/>
                                                        <input type="hidden" id="hndIsCancelable"/>
                                                        <input type="hidden" id="hndStatus"/>
                                                        <input type="hidden" id="hndIsConfirmable"/>
                                                        <input type="hidden" id="hdn_TotalFare"/>
                                                        <input type="hidden" id="hdn_ServiceCharge"/>
                                                        <input type="hidden" id="hdn_Total"/>
                                                        <input type="hidden" id="hdn_Supplier"/>
                                                        <input type="hidden" id="hdn_AffiliateCode"/>

                                                        <input type="hidden" id="hdn_HoldDate" />
                                                         <input type="hidden" id="hdn_DeadLineDate" />
                                                        
                                                    </div>




                                                </div>

                                            </div>
                                        </div>
                                    </div>--%>

        <%--<div class="modal fade bs-example-modal-lg" id="InvoiceModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" style="width: 60%">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="close"><span aria-hidden="true">�</span></button>
                                                    <h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left">Send Invoice</h4>
                                                </div>
                                                <div class="modal-body">

                                                    <table class="table table-hover table-responsive" id="tbl_SendInvoice" style="width: 100%">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <span class="text-left" style="font-weight: bold;">Type your Email : </span>&nbsp;&nbsp;
												   
											<input type="text" id="txt_sendInvoice" placeholder="Email adress" class="form-control1 logpadding margtop5 " style="width: 70%;">
                                                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_txt_sendInvoice"><b>* This field is required</b></label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <input id="btn_sendInvoice" type="button" value="Send Invoice" class="btn-search mr20" style="width: 40%; margin-left: 30%" onclick="MailInvoice();">
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>--%>

     <%--   <div class="modal fade bs-example-modal-lg" id="VoucherModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" style="width: 60%">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="close"><span aria-hidden="true">�</span></button>
                                                    <h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left">Send Voucher</h4>
                                                </div>
                                                <div class="modal-body">

                                                    <table class="table table-hover table-responsive" id="tbl_SendVoucher" style="width: 100%">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <span class="text-left" style="font-weight: bold;">Type your Email : </span>&nbsp;&nbsp;
												   
											<input type="text" id="txt_sendVoucher" placeholder="Email adress" class="form-control1 logpadding margtop5 " style="width: 70%;"/>
                                                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_txt_sendVoucher"><b>* This field is required</b></label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <input id="btn_sendVoucher" type="button" value="Send Invoice" class="btn-search mr20" style="width: 40%; margin-left: 30%" onclick="MailVoucher();"/>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>


        <div class="modal fade in" id="IncompleteBookingDetailModal" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" style="width: 70%">
                                            <div class="modal-content">
                                                <div class="modal-header">

                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="gridSystemModalLabel2" style="text-align: left">Incomplete Booking Details</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="scrollingDiv">
                                                        <div class="row">
                                                            <div class="col-md-2 bold">Invoice No.</div>
                                                            <div class="col-md-4"><span id="spn_InvoiceNo" class="text-left"></span></div>
                                                            <div class="col-md-2 bold">Agent Name</div>
                                                            <div class="col-md-4"><span id="spn_AgentName" class="text-left"></span></div>
                                                        </div>
                                                        <br />
                                                        <div class="row">
                                                            <div class="col-md-2 bold">Hotel Name</div>
                                                            <div class="col-md-4"><span id="spn_HotelName" class="text-left"></span></div>
                                                            <div class="col-md-2 bold">Nationality</div>
                                                            <div class="col-md-4"><span id="spn_Nationality" class="text-left"></span></div>
                                                        </div>
                                                        <br />
                                                        <div class="row">
                                                            <div class="col-md-2 bold">Room Price</div>
                                                            <div class="col-md-4"><span id="spn_RoomPrice" class="text-left"></span></div>
                                                            <div class="col-md-2 bold ">No. of Room</div>
                                                            <div class="col-md-4"><span id="spn_RoomCount" class="text-left"></span></div>
                                                        </div>
                                                        <br />
                                                        <div class="row">
                                                            <div class="col-md-2 bold">Cancellation Date </div>
                                                            <div class="col-md-4"><span id="spn_CancellationDate" class="text-left"></span></div>
                                                            <div class="col-md-2 bold">Cancellation Price</div>
                                                            <div class="col-md-4"><span id="spn_CancellationPrice" class="text-left"></span></div>
                                                        </div>
                                                         <br />
                                                        <div class="row">
                                                            <div class="col-md-2 bold">Check In </div>
                                                            <div class="col-md-4"><span id="spn_CheckIn" class="text-left"></span></div>
                                                            <div class="col-md-2 bold">Check Out </div>
                                                            <div class="col-md-4"><span id="spn_CheckOut" class="text-left"></span></div>
                                                        </div>
                                                        <br />
                                                        <div class="row">
                                                            <div class="col-md-2 bold">Adult Count </div>
                                                            <div class="col-md-4"><span id="spn_AdultCount" class="text-left"></span></div>
                                                            <div class="col-md-2 bold">Child Count </div>
                                                            <div class="col-md-4"><span id="spn_ChildCount" class="text-left"></span></div>
                                                        </div>
                                                         <br />
                                                        <div class="row">
                                                            <div class="col-md-2 bold">Supplier </div>
                                                            <div class="col-md-4"><span id="spn_Supplier" class="text-left"></span></div>
                                                        </div>
                                                       
                                                           
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


        <div class="modal fade bs-example-modal-lg" id="UpdateHoldBookingDateModel" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content" style="width: 700px;">
                                                <div class="modal-header" style="border-bottom: 1px solid #fff">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                                    <h4 class="modal-title" id="gridSystemModalLabel5" style="text-align: left"></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Update Hold Date</b></div>
                                                    <div class="frow2">
                                                         <br />
                                                         <div class="row" style="padding-left: 15px;">
                                                             <div class="col-md-6">
                                                                 <div class="col-md-10 bold">Hold Date : </div>
                                                            <div class="col-md-10 bold"><label id="HoldDate"></label></div>
                                                             </div>
                                                             <div class="col-md-6">
                                                                 <div class="col-md-10 bold">Dead Line Date : </div>
                                                            <div class="col-md-10 bold"><label id="DeadLineDate"></label></div>
                                                             </div>                                                           
                                                        </div>
                                                         <br /><br />
                                                        <div class="row" style="padding-left: 30px;">
                                                            <div class="col-md-2 bold">New Hold Date : </div>
                                                            <div class="col-md-4"><input type="text" class="form-control mySelectCalendar" id="datepickerHold" aria-describedby="inputGroupSuccess3Status" style="cursor: pointer; width: 100%"/></div>
                                                            
                                                        </div>
                                                         <br />
                                                               
                                                        <%--<div class="clearfix"></div>
                                                        <br/>--%>
                          <%--                          </div>
                                               
                                                    <div class="frow2" style="text-align: center;">
                                                        <img style="-webkit-user-select: none; margin-top: 10px; display: none;" id="dlgLoader" src="../images/loader.gif"><br>
                                                        <input type="button" value="Update" onclick="UpdateHoldDate();" id="" class="btn-search4 margtop20" style="float: right;"/>
                                                     <input type="hidden" id="holdReferenceCode">-
                                                        <input type="hidden" id="holdReservatonID"/>
                                                        <input type="hidden" id="holdDeadLineDate"/>                                                    
                                                       
                                                    </div>




                                                </div>

                                            </div>
                                        </div>
                                    </div>--%>
        
<%--<div class="modal fade bs-example-modal-lg" id="ConfirmHoldBookingDetailModel" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content" style="width: 700px;">
                                                <div class="modal-header" style="border-bottom: 1px solid #fff">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">�</span></button>
                                                    <h4 class="modal-title" id="gridSystemModalLabel5" style="text-align: left"></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Re-Confirmation Details</b></div>
                                                    <div class="frow2">
                                                        <br/>
                                                        <table>
                                                            <tbody>
                                                                
                                                                <tr>
                                                                    <td><span class="dark bold">Hotel: </span></td>
                                                                    <td style="padding-left: 15px;"><span class="dark" id="HotelName"></span></td>
                                                                    <td style="padding-left: 20px;"><span class="dark bold">Check-In: </span></td>
                                                                    <td style="padding-left: 15px;"><span class="dark" id="Checkin"></span></td>
                                                                    <td style="padding-left: 20px;"><span class="dark bold">Check-Out: </span></td>
                                                                    <td style="padding-left: 15px;"><span class="dark" id="Checkout"></span></td>
                                                                </tr>
                                                                
                                                                <tr>
                                                                    <td><span class="dark bold">Passenger: </span></td>
                                                                    <td style="padding-left: 15px;"><span class="dark" id="PasssengerName"></span></td>
                                                                    <td style="padding-left: 20px;"><span class="dark bold">Location: </span></td>
                                                                    <td style="padding-left: 15px;"><span class="dark" id="Location"></span></td>
                                                                    <td style="padding-left: 20px;"><span class="dark bold">Nights: </span></td>
                                                                    <td style="padding-left: 15px;"><span class="dark" id="Nights"></span></td>
                                                                </tr>
                                                                
                                                                <tr>
                                                                    <td><span class="dark bold">Booking ID: </span></td>
                                                                    <td style="padding-left: 15px;"><span class="dark" id="BookingID"></span></td>
                                                                    <td style="padding-left: 20px;"><span class="dark bold">Booking Date: </span></td>
                                                                    <td style="padding-left: 15px;"><span class="dark" id="BookingDate"></span></td>
                                                                    <td style="padding-left: 20px;"><span class="dark bold">Amount: </span></td>
                                                                    <td style="padding-left: 15px;"><span class="dark" id="BookingAmount"></span></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>

                                                    </div>
                                                     
                                                    <div class="frow2">
                                                         <br />
                                                         
                                                        <div class="row">
                                                            <div class="col-md-2 bold"> Date : </div>
                                                            <div class="col-md-3"><input type="text" class="form-control mySelectCalendar" id="ConfirmDate"  style="cursor: pointer; width: 100%"/></div>
                                                        
                                                            <div class="col-md-2 bold">Hotel Confirmation No : </div>
                                                            <div class="col-md-3">
                                                                <input type="text" class="form-control" id="HotelConfirmationNo" style="cursor: pointer; width: 100%" /></div>
                                                            </div> 
                                                         <br />

                                                        <div class="row">
                                                            <div class="col-md-2 bold">Staff Name : </div>
                                                            <div class="col-md-3">
                                                                <input type="text" class="form-control" id="StaffName" style="cursor: pointer; width: 100%" /></div>

                                                            <div class="col-md-2 bold">Reconfirm Through : </div>
                                                             <div class="col-md-3">
                                                                <select id="ReconfirmThrough" class="form-control">
                                                                    <option value="-">Select Reconfirm Through</option>
                                                                    <option value="Mail">Mail</option>
                                                                    <option value="Phone">Phone</option>
                                                                    <option value="Whatsapp">Whatsapp</option>
                                                                </select>
                                                                <input type="text" class="form-control" id="ReconfirmThrough"  style="cursor: pointer; width: 100%"/>
                                                            </div>
                                                        </div>
                                                        <br />

                                                         <div class="row">
                                                            <div class="col-md-2 bold">Comment : </div>
                                                            <div class="col-md-12">
                                                                <input type="text" class="form-control" id="Comment" style="cursor: pointer; width: 100%" />
                                                          <textarea class="form-control" id="Comment"></textarea>
                                                                  </div>
                                                            </div>

                                                        </div>
                                                        
                                                    <div class="frow2" style="text-align: center;">
                                                         <br />
                                                        <img style="-webkit-user-select: none; margin-top: 10px; display: none;" id="dlgLoader" src="../images/loader.gif"><br>
                                                        <input type="button" value="Submit" onclick="SaveConfirmDetail();" id="" class="btn-search4 margtop20" style="float: right;">
                                                                                                              
                                                        <input type="hidden" id="ReservatonID">
                                                       
                                                    </div>




                                                </div>

                                            </div>
                                        </div>
                                    </div>--%>
                                    
     
</asp:Content>
