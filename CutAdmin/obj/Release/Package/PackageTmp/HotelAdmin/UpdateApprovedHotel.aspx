﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelAdmin/Master.Master" AutoEventWireup="true" CodeBehind="UpdateApprovedHotel.aspx.cs" Inherits="CutAdmin.HotelAdmin.UpdateApprovedHotel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/UpdateApprovedHotel.js?v=1.6"></script>
    <script src="../Scripts/ChargesMapping.js?v=1.21"></script>

    <!-- Additional styles -->
    <link rel="stylesheet" href="../css/styles/form.css?v=1">
    <link rel="stylesheet" href="../css/styles/switches.css?v=1">
    <link rel="stylesheet" href="../css/styles/table.css?v=1">
    <link href="../css/styles/custom.css" rel="stylesheet" />

    <!-- DataTables -->
    <style>
        .selectedImg {
            border: 3px solid #1b397b;
        }
    </style>
    <style>
        .wizard-spacer {
            height: 0px;
        }
    </style>
    <script>
        $(function () {
            $(".wizard-spacer").removeAttr("style");
        });
    </script>
    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <!-- Main content -->
    <section id="main">


        <hgroup id="main-title" class="thin">
            <h1>Hotel Details</h1>
            <hr />
        </hgroup>

        <div class="with-padding hotelmap">

            <div class="DivAddMapping">
                <%--<div class="full-page-wizard">--%>
                <form id="AddfrmWizard" class="block wizard same-height">

                    <%-- <h3 class="block-title">Add Hotel  <small style="float: right" class="blue">HOTEL:
                                <label id="lblHotelname"></label>
                    </small></h3>--%>

                    <fieldset class="wizard-fieldset" style="border: 1px ridge">
                        <legend class="legend">Hotel Details</legend>
                        <div class="button-height">
                            <%-- style="min-height: 300px;"--%>
                            <div class="columns">
                                <div class="six-columns twelve-columns-mobile lftField">
                                    <div class="columns">
                                        <div class="six-columns twelve-columns-mobile">
                                            <label class="input-info">Hotel Name</label>
                                            <input type="text" id="HtlName" size="9" class="input full-width" value="">
                                        </div>
                                        <div class="six-columns twelve-columns-mobile">
                                            <label class="input-info">Ratings </label>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label class="input-info" id="HtlRatings"></label>

                                            <select id="SelStars" class="select full-width" style="height: 25px; background-color: #f0efef" onchange="fnStarRatings(this.value,'','#HtlRatings',this.id);">
                                                <option value="1">1 Star</option>
                                                <option value="2">2 Star</option>
                                                <option value="3">3 Star</option>
                                                <option value="4">4 Star</option>
                                                <option value="5">5 Star</option>
                                                <option value="0">No Ratings</option>
                                            </select>

                                        </div>
                                        <div class="six-columns twelve-columns-mobile">
                                            <label class="input-info">Location</label>
                                            <input type="text" id="txtLocation" list="listLocation" class="input" style="width: 90%" value="" onchange="ConfirmLocation()">
                                            <datalist id="listLocation"></datalist>
                                        </div>
                                        <div class="six-columns twelve-columns-mobile">
                                            <label class="input-info">Zip Code</label>
                                            <input type="text" id="htlZipcode" class="input full-width" value="">
                                        </div>
                                    </div>

                                    <%--<div class="columns">
                                        <div class="six-columns twelve-columns-mobile">
                                            <label class="input-info">City</label>
                                            <input type="text" id="htlCity" class="input full-width" value="" readonly>
                                        </div>

                                        <div class="six-columns twelve-columns-mobile">
                                            <label class="input-info">Country</label>
                                            <input type="text" id="htlCountry" class="input full-width" value="" readonly>
                                        </div>
                                    </div>--%>
                                    <div class="columns">
                                        <div class="six-columns twelve-columns-mobile">
                                            <label class="input-info">Country</label>
                                            <%--<input type="text" id="htlCountry" class="input full-width" value="" readonly>--%>
                                            <div class="full-width button-height" id="DivCountry">
                                                <select id="htlCountry" class="select full-width country" onchange="GetCountryCityy()">
                                                    <option selected="selected" value="-">Select Any Country</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="six-columns twelve-columns-mobile">
                                            <label class="input-info">City</label>
                                            <%--<input type="text" id="htlCity" class="input full-width" value="" readonly>--%>
                                            <div class="full-width button-height" id="City">
                                                <select id="htlCity" class="select full-width City">
                                                    <option selected="selected" value="-">Select Any City</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="six-columns twelve-columns-mobile Descrp-text">
                                    <label class="input-info">Description</label>
                                    <textarea name="autoexpanding" id="HtlDescription" class="input full-width autoexpanding"></textarea>

                                    <div class="columns">
                                        <div class="six-columns twelve-columns-mobile">
                                            <label class="input-info">Langitude</label>
                                            <input type="text" id="htlLangitude" class="input full-width" value="">
                                        </div>

                                        <div class="six-columns twelve-columns-mobile">
                                            <label class="input-info">Latitude</label>
                                            <input type="text" id="htlLatitude" class="input full-width" value="">
                                        </div>
                                    </div>
                                </div>

                                <div class="twelve-columns twelve-columns-mobile">
                                    <div class="columns">
                                        <div class="twelve-columns twelve-columns-mobile lftField">
                                            <label class="input-info">Hotel Adress</label>
                                            <textarea name="autoexpanding" id="HtlAddress" class="input full-width autoexpanding"></textarea>
                                            <%--<input type="text" id="HtlAddress" class="input full-width" value="">--%>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <hr />
                        </div>
                    </fieldset>

                    <fieldset class="wizard-fieldset fields-list" id="tab_facility">
                        <legend class="legend">Facilities</legend>
                        <div class="columns selAmenities" style="height: 400px; overflow: auto">

                            <div class="twelve-columns">
                                <label class="input-info">Facilities Selected </label>
                                <div id="divAddFacilities" class="boxed">
                                </div>
                                <%-- style="border-style: groove; border-width: 1px; background-color: white" <textarea name="autoexpanding" id="HtlFacilities" class="input full-width autoexpanding" style="min-height: 50px;" readonly></textarea>--%>
                            </div>



                        </div>
                        <hr />

                    </fieldset>

                    <fieldset class="wizard-fieldset" style="border: 1px ridge">
                        <legend class="legend">Policies</legend>
                        <div class="button-height" id="tab_Plocies">
                            <b><u>Genral Policies</u></b>
                            <div class="columns">

                                <div class="new-row four-columns twelve-columns-mobile" id="addPates">
                                    <label class="input-info">Pets Allowed </label>
                                    <select id="selPatesAllowed" class="pates full-width  select  expandable-list ">
                                        <option selected="selected" value="-" disabled>Select</option>
                                        <option value="No">No</option>
                                        <option value="Yes">Yes</option>
                                        <option value="Not Specified">Not Specified</option>
                                    </select>
                                </div>

                                <div class="four-columns twelve-columns-mobile" id="addLiquor">
                                    <label class="input-info">Liquor Policy </label>
                                    <select id="selLiquorPolicy" class="full-width select  expandable-list ">
                                        <option value="Not allowed">Not allowed</option>
                                    </select>
                                </div>

                                <div class="four-columns twelve-columns-mobile" id="addSmooking">
                                    <label class="input-info">Smooking </label>
                                    <select id="selSmooking" class="pates full-width  select  expandable-list ">
                                        <option selected="selected" value="-" disabled>Select</option>
                                        <option value="No">No</option>
                                        <option value="Yes">Yes</option>
                                        <option value="Specific Area">Specific Area</option>
                                        <option value="Not Specified">Not Specified</option>
                                    </select>
                                </div>
                            </div>

                            <div class="columns">

                                <div class="three-columns six-columns-mobile">
                                    <label class="input-info">Child Age From</label>
                                    <input type="number" class="input full-width" value="0" id="txtChildAgeFrom">
                                </div>

                                <div class="three-columns six-columns-mobile">
                                    <label class="input-info">Child Age To</label>
                                    <input type="number" class="input full-width" value="0" id="txtChildAgeTo">
                                </div>

                                <div class="three-columns six-columns-mobile">
                                    <label class="input-info">Checkin Time</label>
                                    <input type="time" class="input full-width" value="" id="txtCheckinTime">
                                </div>

                                <div class="three-columns six-columns-mobile">
                                    <label class="input-info">Checkout Time</label>
                                    <input type="time" class="input full-width" value="" id="txtCheckoutTime">
                                </div>

                            </div>

                            <div class="columns">

                                <div class="three-columns twelve-columns-mobile">
                                    <label class="input-info">Total Rooms</label>
                                    <input type="number" class="input full-width" value="0" id="TotalRooms">
                                </div>

                                <div class="three-columns twelve-columns-mobile">
                                    <label class="input-info">Total Floors</label>
                                    <input type="number" class="input full-width " value="0" id="TotalFloors">
                                </div>

                                <div class="three-columns twelve-columns-mobile">
                                    <label class="input-info">Build Year</label>
                                    <input type="text" class="input full-width" value="" id="BuildYear">
                                </div>

                                <div class="three-columns twelve-columns-mobile">
                                    <label class="input-info">Renovated ON</label>
                                    <input type="text" class="input full-width " value="" id="RenovationYear">
                                </div>

                            </div>

                            <div class="columns">

                                <div class="three-columns twelve-columns-mobile">
                                    <label class="input-info">Hotel Group</label>
                                    <input type="text" class="input full-width" value="" id="txtHotelGroup">
                                </div>
                                <div class="three-columns twelve-columns-mobile">
                                    <label class="input-info">Trip Adviser Link</label>
                                    <input type="text" class="input full-width " value="" id="txtTripAdviserLink">
                                </div>

                            </div>

                            <div class="clearfix"></div>

                            <div class="columns">
                                <div class="six-columns twelve-columns-mobile">

                                    <h3 class="tax-police">Tax Policies</h3>
                                    <hr />

                                    <div id="div_TaxDetails">
                                    </div>
                                </div>

                                <div class="six-columns twelve-columns-mobile">
                                    <h3 class="tax-police">Meal Policies</h3>
                                    <hr />
                                    <div id="div_MealDetails">
                                    </div>

                                </div>
                            </div>
                            <hr />
                        </div>
                    </fieldset>


                    <%-- <fieldset class="wizard-fieldset" style="border:1px ridge">
                                <legend class="legend">Image</legend>
                                <div class="button-height">
                                    <div class="columns" style="margin-bottom: 200px;">
                                          <div class="new-row one-columns">
                                            <small class="input-info">New Image Url</small>
                                        </div>
                                        <div class="six-columns" style="margin-left:90px;">
                                            <input type="text" id="ImgUrl-Add" size="9" class="input full-width" value="" style="display:none;">   
                                            <input type="file" id="ImgHotelUpload" size="9" class="input full-width" onchange="ShowMainImg(event);">                                   
                                        </div>
                                        <div class="new-row one-columns">
                                            <small class="input-info">Hotel Image:</small>
                                        </div>
                                        <div class="eight-columns">
                                             <img src='' id='ImgMain-Add' class='selMainImage-Add' width='70%'>
                                        </div>
                                       
                                    </div>

                                    <hr />
                                </div>
                            </fieldset>--%>

                    <fieldset class="wizard-fieldset" style="border: 1px ridge">
                        <legend class="legend">Images</legend>

                        <div class="columns" style="height: 300px; overflow: auto">
                            <div class="twelve-columns twelve-columns-mobile">
                                <label class="input-info">Please attach the image</label>
                            </div>
                            <div class="six-columns twelve-columns-mobile">
                                <input type="file" name="images[]" id="images" size="9" class="input full-width" onchange="preview_images();" multiple>
                                <p class="red">(Please select multiple Images by pressing '<b>CTRL</b> button')</p>
                            </div>
                            <div class="two-columns four-columns-mobile">
                                <a class="button icon-trash">
                                    <input id="btnSubImageClearAdd" type="button" value="Clear All" onclick="ClearSubImageAdd();" style="background-color: transparent; border: none;" />
                                </a>
                            </div>

                        </div>
                        <div class="columns" id="image_preview">
                        </div>

                        <hr />

                    </fieldset>


                    <fieldset class="wizard-fieldset" style="border: 1px ridge">
                        <legend class="legend">Contacts</legend>
                        <div class="button-height ContactsBox">
                            <div id="idHotelContacts">
                                <div class="columns">
                                    <div class="three-columns four-columns-tablet twelve-columns-mobile contdetais">

                                        <div class="columns">
                                            <div class="twelve-columns twelve-columns-mobile"><strong><u>HOTEL CONTACT</u></strong></div>
                                            <div class="six-columns">
                                                <p>
                                                    Contacts
                                                    <label id="clickC"><strong>1</strong></label>
                                                </p>
                                            </div>


                                            <%-- <div class="one-columns button icon-plus-round" style="float:left;">--%>
                                            <div class="six-columns">
                                                <div class="one-columns button icon-plus-round blue">
                                                    <input id="btnAddHotelContacts" class="HotelContacts" type="button" value="More" onclick="AddHotelContacts()" style="background-color: transparent; border: none;" />
                                                </div>
                                            </div>
                                            <%--<div class="one-columns button icon-minus"  style="float:right;">
                                                <input id="btnLessHotelContacts" class="HotelContacts" type="button" value="Less" onclick="LessHotelContacts()" style="background-color: transparent; border: none;" /></div>--%>
                                        </div>
                                    </div>


                                    <div class="nine-columns eight-columns-tablet twelve-columns-mobile contdetais">
                                        <div class="columns">

                                            <div class="six-columns twelve-columns-mobile">
                                                <input type="text" id="TxtContactPerson0" size="9" class="input full-width HotelPerson" value="" placeholder="Contact Person Name*">
                                            </div>
                                            <div class="six-columns twelve-columns-mobile">
                                                <input type="email" id="txtEmail0" class="input full-width HotelEmail" value="" placeholder="Email Id*">
                                            </div>
                                            <div class="four-columns twelve-columns-mobile">
                                                <input type="text" id="TxtContactPhone0" onkeypress='return event.charCode >= 48 && event.charCode <= 57' size="9" class="input full-width HotelPhone" value="" placeholder="Phone Number*">
                                            </div>
                                            <div class="four-columns twelve-columns-mobile">
                                                <input type="text" id="txtMobileNo0" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="input full-width HotelMobile" value="" placeholder="Mobile Number*">
                                            </div>
                                            <div class="four-columns twelve-columns-mobile">
                                                <input type="text" id="txtFaxNo0" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="input full-width HotelFax" value="" placeholder="Fax Number">
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <br />
                            </div>

                            <hr />

                            <div id="idReservationContacts">
                                <div class="columns">
                                    <div class="three-columns twelve-columns-mobile contdetais">
                                        <div class="columns">
                                            <div class="twelve-columns twelve-columns-mobile">
                                                <label for="chkSameReservation" class="label">
                                                    <input type="checkbox" id="chkSameReservation" value="1" class="" onchange="CheckReservation();">
                                                    Same as Above</label><br />
                                                <b><u>RESERVATION CONTACT</u></b>
                                            </div>
                                            <div class="six-columns">
                                                <p>
                                                    Contacts: <b>
                                                        <label id="clickR">1</label></b>
                                                </p>
                                            </div>
                                            <div class="six-columns">
                                                <div class="one-columns button icon-plus-round blue">
                                                    <input id="btnAddReservationContacts" class="ReservationContacts" type="button" value="More" onclick="AddReservationContacts()" style="background-color: transparent; border: none;" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="nine-columns twelve-columns-mobile contdetais">
                                        <div class="columns">

                                            <div class="six-columns twelve-columns-mobile">
                                                <input type="text" id="TxtReservationPerson0" size="9" class="input full-width ReservationPerson" value="" placeholder="Contact Person Name*">
                                            </div>
                                            <div class="six-columns twelve-columns-mobile">
                                                <input type="email" id="txtReservationEmail0" class="input full-width ReservationEmail" value="" placeholder="Email Id*">
                                            </div>
                                            <div class="four-columns twelve-columns-mobile">
                                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="TxtReservationPhone0" size="9" class="input full-width ReservationPhone" value="" placeholder="Phone Number*">
                                            </div>
                                            <div class="four-columns twelve-columns-mobile">
                                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtReservationMobNo0" class="input full-width ReservationMobile" value="" placeholder="Mobile Number*">
                                            </div>
                                            <div class="four-columns twelve-columns-mobile">
                                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtReservationFaxNo0" class="input full-width ReservationFax" value="" placeholder="Fax Number">
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <br />
                            </div>

                            <br />
                            <hr />


                            <div id="idAccountsContacts">
                                <div class="columns">
                                    <div class="three-columns twelve-columns-mobile contdetais">

                                        <div class="columns">
                                            <div class="twelve-columns twelve-columns-mobile">
                                                <label for="chkSameAccounts" class="label">
                                                    <input type="checkbox" id="chkSameAccounts" value="1" class="" onchange="CheckAccounts();">
                                                    Same as Above</label><br />
                                                <b><u>ACCOUNTS CONTACT</u></b>
                                            </div>
                                            <div class="six-columns">
                                                <p>
                                                    Contact <b>
                                                        <label id="clickA">1</label></b>
                                                </p>
                                            </div>
                                            <div class="six-columns">
                                                <div class="one-columns button icon-plus-round blue">
                                                    <input id="btnAddAccountsContacts" class="AccountsContacts" type="button" value="More" onclick="AddAccountsContacts()" style="background-color: transparent; border: none;" />
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="nine-columns twelve-columns-mobile contdetais">
                                        <div class="columns">

                                            <div class="six-columns twelve-columns-mobile">
                                                <input type="text" id="TxtAccountsPerson0" size="9" class="input full-width AccountsPerson" value="" placeholder="Contact Person Name*">
                                            </div>
                                            <div class="six-columns twelve-columns-mobile">
                                                <input type="email" id="txtAccountsEmail0" class="input full-width AccountsEmail" value="" placeholder="Email Id*">
                                            </div>
                                            <div class="four-columns twelve-columns-mobile">
                                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="TxtAccountsPhone0" size="9" class="input full-width AccountsPhone" value="" placeholder="Phone Number*">
                                            </div>
                                            <div class="four-columns twelve-columns-mobile">
                                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtAccountsMobNo0" class="input full-width AccountsMobile" value="" placeholder="Mobile Number*">
                                            </div>
                                            <div class="four-columns twelve-columns-mobile">
                                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtAccountsFaxNo0" class="input full-width AccountsFax" value="" placeholder="Fax Number">
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <br />

                            </div>

                            <br />
                            <br />

                            <div class="clear"></div>
                            <hr />

                        </div>

                        <div class="field-block button-height wizard-controls align-right">
                            <button type="button" class="button anthracite-gradient float-right" onclick="SaveHotelMapping();">Save</button>
                        </div>

                    </fieldset>
                </form>
                <%-- </div>--%>
            </div>

            <div class="DivUpdateMapping" style="display: none;">
                <%--<div class="full-page-wizard">--%>
                <form class="block wizard same-height">

                    <fieldset class="wizard-fieldset" style="border: 1px ridge">
                        <legend class="legend">Hotel Details</legend>
                        <div class="button-height">
                            <div class="columns">
                                <div class="six-columns twelve-columns-mobile lftField">

                                    <div class="columns">
                                        <div class="six-columns twelve-columns-mobile">
                                            <label class="input-info">Hotel Name:&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                            <input type="text" id="HtlName1" size="9" class="input full-width" value="">
                                        </div>

                                        <div class="six-columns twelve-columns-mobile">
                                            <label class="input-info">Ratings:</label>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label class="input-info" id="HtlRatings1"></label>
                                            <div class="full-width" id="Span_Stars1">
                                                <select id="SelStars1" class="select full-width" onchange="fnStarRatings(this.value,'','#HtlRatings1',this.id);">
                                                    <option value="1">1 Star</option>
                                                    <option value="2">2 Star</option>
                                                    <option value="3">3 Star</option>
                                                    <option value="4">4 Star</option>
                                                    <option value="5">5 Star</option>
                                                    <option value="0">No Ratings</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="six-columns twelve-columns-mobile">
                                            <label class="input-info">Location</label>
                                            <input type="text" id="txtLocation1" list="listLocation1" class="input" style="width: 90%" value="" onchange="ConfirmLocation()">
                                            <datalist id="listLocation1"></datalist>
                                        </div>

                                        <div class="six-columns twelve-columns-mobile">
                                            <label class="input-info">Zip Code:</label>
                                            <input type="text" id="htlZipcode1" size="9" class="input full-width" value="">
                                        </div>

                                    </div>

                                    <div class="columns">

                                        <div class="six-columns twelve-columns-mobile">
                                            <label class="input-info">City:</label>
                                            <input type="text" id="htlCity1" class="input full-width" value="" readonly>
                                        </div>

                                        <div class="six-columns twelve-columns-mobile">
                                            <label class="input-info">Country</label>
                                            <input type="text" id="htlCountry1" class="input full-width" value="" readonly>
                                        </div>

                                    </div>
                                </div>

                                <div class="six-columns twelve-columns-mobile Descrp-text">

                                    <label class="input-info">Description:</label>
                                    <textarea name="autoexpanding" id="HtlDescription1" class="input full-width autoexpanding"></textarea>

                                    <div class="columns">
                                        <div class="six-columns twelve-columns-mobile">
                                            <label class="input-info">Langitude:</label>
                                            <input type="text" id="htlLangitude1" class="input full-width" value="">
                                        </div>

                                        <div class="six-columns twelve-columns-mobile">
                                            <label class="input-info">Latitude:</label>
                                            <input type="text" id="htlLatitude1" class="input full-width" value="">
                                        </div>

                                    </div>
                                </div>


                                <div class="twelve-columns twelve-columns-mobile">
                                    <div class="columns">
                                        <div class="twelvecolumns twelve-columns-mobile">
                                            <label class="input-info">Hotel Adress:</label>
                                            <textarea name="autoexpanding" id="HtlAddress1" class="input full-width autoexpanding" style="resize: none;"></textarea>
                                            <%-- <input type="text" id="HtlAddress1" class="input full-width" value="">--%>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <hr />
                        </div>
                    </fieldset>

                    <fieldset class="wizard-fieldset fields-list" id="tab_facility1">
                        <legend class="legend">Facilities</legend>

                        <div class="columns selAmenities" style="height: 400px; overflow: auto">
                            <div class="twelve-columns">
                                <label class="input-info">Facilities</label>
                                <div id="divUpdateFacilities" class="boxed">
                                </div>
                            </div>

                        </div>
                        <hr />

                    </fieldset>

                    <fieldset class="wizard-fieldset" style="border: 1px ridge">
                        <legend class="legend">Policies</legend>
                        <div class="button-height" id="tab_Plocies1">
                            <b><u>Genral Policies</u></b>
                            <div class="columns">
                                <div class="new-row four-columns twelve-columns-mobile" id="Pates">
                                    <label class="input-info">Pets Allowed: </label>
                                    <select id="selPatesAllowed1" class="pates full-width  select  expandable-list ">
                                        <option value="-" disabled>Select</option>
                                        <option value="No">No</option>
                                        <option value="Yes">Yes</option>
                                        <option value="Not Specified">Not Specified</option>
                                    </select>
                                </div>

                                <div class="four-columns twelve-columns-mobile" id="Liquor">
                                    <label class="input-info">Liquor Policy </label>
                                    <select id="selLiquorPolicy1" class="full-width select  expandable-list ">
                                        <option value="Not allowed">Not allowed</option>
                                    </select>
                                </div>

                                <div class="four-columns twelve-columns-mobile" id="Smooking">
                                    <label class="input-info">Smooking </label>
                                    <select id="selSmooking1" class="pates full-width  select  expandable-list ">
                                        <option value="-" disabled>Select</option>
                                        <option value="No">No</option>
                                        <option value="Yes">Yes</option>
                                        <option value="Specific Area">Specific Area</option>
                                        <option value="Not Specified">Not Specified</option>
                                    </select>
                                </div>

                            </div>
                            <div class="columns">

                                <div class="three-columns six-columns-mobile">
                                    <label class="input-info">Child Age From</label>
                                    <input type="number" class="input full-width" value="0" id="txtChildAgeFrom1">
                                </div>

                                <div class="three-columns six-columns-mobile">
                                    <label class="input-info">Child Age To</label>
                                    <input type="number" class="input full-width" value="0" id="txtChildAgeTo1">
                                </div>

                                <div class="three-columns six-columns-mobile">
                                    <label class="input-info">Checkin Time</label>
                                    <input type="time" class="input full-width" value="" id="txtCheckinTime1">
                                </div>

                                <div class="three-columns six-columns-mobile">
                                    <label class="input-info">Checkout Time</label>
                                    <input type="time" class="input full-width" value="" id="txtCheckoutTime1">
                                </div>

                            </div>

                            <div class="columns">


                                <div class="three-columns twelve-columns-mobile">
                                    <label class="input-info">Total Rooms</label>
                                    <input type="number" class="input full-width" value="0" id="TotalRooms1">
                                </div>

                                <div class="three-columns twelve-columns-mobile">
                                    <label class="input-info">Total Floors</label>
                                    <input type="number" class="input full-width " value="0" id="TotalFloors1">
                                </div>

                                <div class="three-columns twelve-columns-mobile">
                                    <label class="input-info">Build Year</label>
                                    <input type="text" class="input full-width" value="" id="BuildYear1">
                                </div>

                                <div class="three-columns twelve-columns-mobile">
                                    <label class="input-info">Renovated ON</label>
                                    <input type="text" class="input full-width " value="" id="RenovationYear1">
                                </div>

                            </div>
                            <div class="columns">

                                <div class="three-columns twelve-columns-mobile">
                                    <label class="input-info">Hotel Group</label>
                                    <input type="text" class="input full-width" value="" id="txtHotelGroup1">
                                </div>

                                <div class="three-columns twelve-columns-mobile">
                                    <label class="input-info">Trip Adviser Link</label>
                                    <input type="text" class="input full-width " value="" id="txtTripAdviserLink1">
                                </div>

                            </div>
                            <div class="clear"></div>

                            <div class="columns">
                                <div class="six-columns twelve-columns-mobile">

                                    <h3 class="tax-police">Tax Policies</h3>
                                    <hr />

                                    <div class="" id="div_UpdateTaxDetails">
                                    </div>

                                </div>


                                <div class="six-columns twelve-columns-mobile">
                                    <h3 class="tax-police">Meal Policies</h3>
                                    <hr />
                                    <div id="div_UpdateMealDetails" style="margin-bottom: 200px;">
                                    </div>

                                </div>

                            </div>

                            <hr />
                        </div>

                    </fieldset>

                    <%-- <fieldset class="wizard-fieldset" style="border:1px ridge">
                                <legend class="legend">Image</legend>
                                <div class="button-height">
                                    <div class="columns" style="margin-bottom: 200px;">
                                          <div class="new-row one-columns">
                                            <small class="input-info">New Image Url</small>
                                        </div>
                                        <div class="six-columns" style="margin-left:90px;">
                                            <input type="text" id="ImgUrl" size="9" class="input full-width" value="" onkeyup="ShowNewImg();">
                                        </div>
                                        <div class="new-row one-columns">
                                            <small class="input-info">Hotel Image:</small>
                                        </div>
                                        <div class="eight-columns">
                                            <img src='' id='ImgMain' class='selMainImage' width='70%'>
                                        </div>
                                       
                                    </div>

                                    <hr />
                                </div>
                            </fieldset>--%>

                    <fieldset class="wizard-fieldset fields-list">
                        <legend class="legend">Images</legend>
                        <div class="columns" style="height: 300px; overflow: auto">
                            <div class="twelve-columns twelve-columns-mobile">
                                <label class="input-info">Please attach the image</label>
                            </div>
                            <div class="six-columns twelve-columns-mobile">
                                <input type="file" id="images1" size="9" class="input full-width" onchange="preview_images1();" multiple>
                                <p class="red">(Please select multiple Images by pressing '<b>CTRL</b> button')</p>
                            </div>
                            <div class="two-columns four-columns-mobile">
                                <a class="button icon-trash">
                                    <input id="btnSubImageClearAdd1" type="button" value="Clear All" onclick="ClearSubImage1();" style="background-color: transparent; border: none;" />
                                </a>
                            </div>



                        </div>
                        <div class="columns" id="image_preview1">
                        </div>

                        <hr />

                    </fieldset>


                    <fieldset class="wizard-fieldset" style="border: 1px ridge">
                        <legend class="legend">Contacts</legend>
                        <div class="button-height ContactsBox">
                            <div id="idHotelContacts1">
                                <div class="columns">

                                    <div class="three-columns four-columns-tablet twelve-columns-mobile contdetais">
                                        <div class="columns">
                                            <div class="twelve-columns twelve-columns-mobile"><strong><u>HOTEL CONTACT</u></strong></div>
                                            <div class="six-columns">
                                                <p>
                                                    Contacts: <b>
                                                        <label id="clickC1">1</label></b>
                                                </p>
                                            </div>
                                            <div class="six-columns">
                                                <div class="one-columns button icon-plus-round blue">
                                                    <input id="btnAddHotelContacts1" class="HotelContacts" type="button" value="More" onclick="AddHotelContacts1()" style="background-color: transparent; border: none;" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="nine-columns eight-columns-tablet twelve-columns-mobile contdetais">
                                        <div class="columns">

                                            <div class="six-columns twelve-columns-mobile">
                                                <input type="text" id="TxtContactPersonU0" size="9" class="input full-width HotelPerson1" value="" placeholder="Contact Person Name">
                                            </div>
                                            <div class="six-columns twelve-columns-mobile">
                                                <input type="email" id="txtEmailU0" class="input full-width HotelEmail1" value="" placeholder="Email Id">
                                            </div>
                                            <div class="four-columns twelve-columns-mobile">
                                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="TxtContactPhoneU0" size="9" class="input full-width HotelPhone1" value="" placeholder="Phone Number">
                                            </div>
                                            <div class="four-columns twelve-columns-mobile">
                                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtMobileNoU0" class="input full-width HotelMobile1" value="" placeholder="Mobile Number">
                                            </div>
                                            <div class="four-columns twelve-columns-mobile">
                                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtFaxNoU0" class="input full-width HotelFax1" value="" placeholder="Fax Number">
                                            </div>

                                        </div>

                                    </div>

                                </div>
                                <br />
                            </div>
                            <br />

                            <hr />

                            <div id="idReservationContacts1">
                                <div class="columns">
                                    <div class="three-columns twelve-columns-mobile contdetais">

                                        <div class="columns">
                                            <div class="twelve-columns twelve-columns-mobile">
                                                <label for="chkSameReservationU" class="label">
                                                    <input type="checkbox" id="chkSameReservationU" value="1" class="checkbox" onchange="CheckReservationU();">
                                                    Same as Above</label><br />
                                                <b><u>RESERVATION CONTACT</u></b>
                                            </div>
                                            <div class="six-columns">
                                                <p>
                                                    Contacts: <b>
                                                        <label id="clickR1">1</label></b>
                                                </p>
                                            </div>
                                            <div class="six-columns">
                                                <div class="one-columns button icon-plus-round blue">
                                                    <input id="btnAddReservationContacts1" class="ReservationContacts" type="button" value="More" onclick="AddReservationContacts1()" style="background-color: transparent; border: none;" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="nine-columns twelve-columns-mobile contdetais">
                                        <div class="columns">

                                            <div class="six-columns twelve-columns-mobile">
                                                <input type="text" id="txtReservationPersonU0" size="9" class="input full-width ReservationPerson1" value="" placeholder="Contact Person Name">
                                            </div>
                                            <div class="six-columns twelve-columns-mobile">
                                                <input type="email" id="txtReservationEmailU0" class="input full-width ReservationEmail1" value="" placeholder="Email Id">
                                            </div>
                                            <div class="four-columns twelve-columns-mobile">
                                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtReservationPhoneU0" size="9" class="input full-width ReservationPhone1" value="" placeholder="Phone Number">
                                            </div>
                                            <div class="four-columns twelve-columns-mobile">
                                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtReservationMobNoU0" class="input full-width ReservationMobile1" value="" placeholder="Mobile Number">
                                            </div>
                                            <div class="four-columns twelve-columns-mobile">
                                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtReservationFaxNoU0" class="input full-width ReservationFax1" value="" placeholder="Fax Number">
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <br />
                            </div>


                            <br />
                            <hr />

                            <div id="idAccountsContacts1">
                                <div class="columns">
                                    <div class="three-columns twelve-columns-mobile contdetais">

                                        <div class="columns">
                                            <div class="twelve-columns twelve-columns-mobile">
                                                <label for="chkSameAccountsU" class="label">
                                                    <input type="checkbox" id="chkSameAccountsU" value="1" class="checkbox" onchange="CheckAccountsU();">
                                                    Same as Above</label><br />
                                                <b><u>ACCOUNTS CONTACT</u></b>
                                            </div>
                                            <div class="six-columns">
                                                <p>
                                                    Contact: <b>
                                                        <label id="clickA1">1</label></b>
                                                </p>
                                            </div>
                                            <div class="six-columns">
                                                <div class="one-columns button icon-plus-round blue">
                                                    <input id="btnAddAccountsContacts1" class="AccountsContacts1" type="button" value="More" onclick="AddAccountsContacts1()" style="background-color: transparent; border: none;" />
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="nine-columns twelve-columns-mobile contdetais">
                                        <div class="columns">

                                            <div class="six-columns twelve-columns-mobile">
                                                <input type="text" id="txtAccountsPersonU0" size="9" class="input full-width AccountsPerson1" value="" placeholder="Contact Person Name">
                                            </div>
                                            <div class="six-columns twelve-columns-mobile">
                                                <input type="email" id="txtAccountsEmailU0" class="input full-width AccountsEmail1" value="" placeholder="Email Id">
                                            </div>
                                            <div class="four-columns twelve-columns-mobile">
                                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtAccountsPhoneU0" size="9" class="input full-width AccountsPhone1" value="" placeholder="Phone Number">
                                            </div>
                                            <div class="four-columns twelve-columns-mobile">
                                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtAccountsMobNoU0" class="input full-width AccountsMobile1" value="" placeholder="Mobile Number">
                                            </div>
                                            <div class="four-columns twelve-columns-mobile">
                                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtAccountsFaxNoU0" class="input full-width AccountsFax1" value="" placeholder="Fax Number">
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <br />
                            </div>

                            <br />
                            <br />


                            <div class="clear"></div>

                            <hr />
                        </div>

                        <div class="field-block button-height wizard-controls align-right">
                            <button type="button" class="button anthracite-gradient float-right" onclick="UpdateHotelMapping();">Save & Approved</button>
                        </div>
                    </fieldset>

                </form>


                <input value="<%=DescriptionHotel %>" id="HotelDescription" hidden="hidden" />
            </div>

        </div>
    </section>
    <!-- End main content -->



    <!-- JavaScript at the bottom for fast page loading -->
    <!-- Scripts -->
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    <script src="../js/setup.js"></script>

    <!-- Template functions -->
    <script src="../js/developr.input.js"></script>
    <script src="../js/developr.navigable.js"></script>
    <script src="../js/developr.notify.js"></script>
    <script src="../js/developr.scroll.js"></script>
    <script src="../js/developr.tooltip.js"></script>
    <script src="../js/developr.table.js"></script>
    <script src="../js/developr.accordions.js"></script>
    <script src="../js/developr.wizard.js"></script>

    <!-- Plugins -->
    <script src="../js/libs/jquery.tablesorter.min.js"></script>

    <script>

        // Call template init (optional, but faster if called manually)
        $.template.init();

        // Table sort - DataTables
        var table = $('#sorting-advanced');
        table.dataTable({
            'aoColumnDefs': [
				{ 'bSortable': false, 'aTargets': [0, 5] }
            ],
            'sPaginationType': 'full_numbers',
            'sDom': '<"dataTables_header"lfr>t<"dataTables_footer"ip>',
            'fnInitComplete': function (oSettings) {
                // Style length select
                table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
                tableStyled = true;
            }
        });

        // Table sort - styled
        $('#sorting-example1').tablesorter({
            headers: {
                0: { sorter: false },
                5: { sorter: false }
            }
        }).on('click', 'tbody td', function (event) {
            // Do not process if something else has been clicked
            if (event.target !== this) {
                return;
            }

            var tr = $(this).parent(),
				row = tr.next('.row-drop'),
				rows;

            // If click on a special row
            if (tr.hasClass('row-drop')) {
                return;
            }

            // If there is already a special row
            if (row.length > 0) {
                // Un-style row
                tr.children().removeClass('anthracite-gradient glossy');

                // Remove row
                row.remove();

                return;
            }

            // Remove existing special rows
            rows = tr.siblings('.row-drop');
            if (rows.length > 0) {
                // Un-style previous rows
                rows.prev().children().removeClass('anthracite-gradient glossy');

                // Remove rows
                rows.remove();
            }

            // Style row
            tr.children().addClass('anthracite-gradient glossy');

            // Add fake row
            $('<tr class="row-drop">' +
				'<td colspan="' + tr.children().length + '">' +
					'<div class="float-right">' +
						'<button type="submit" class="button glossy mid-margin-right">' +
							'<span class="button-icon"><span class="icon-mail"></span></span>' +
							'Send mail' +
						'</button>' +
						'<button type="submit" class="button glossy">' +
							'<span class="button-icon red-gradient"><span class="icon-cross"></span></span>' +
							'Remove' +
						'</button>' +
					'</div>' +
					'<strong>Name</strong> John Doe<br>' +
					'<strong>Account</strong> admin<br>' +
					'<strong>Last connect</strong> 05-07-2011<br>' +
					'<strong>Email</strong> john@doe.com' +
				'</td>' +
			'</tr>').insertAfter(tr);

        }).on('sortStart', function () {
            var rows = $(this).find('.row-drop');
            if (rows.length > 0) {
                // Un-style previous rows
                rows.prev().children().removeClass('anthracite-gradient glossy');

                // Remove rows
                rows.remove();
            }
        });

        // Table sort - simple
        $('#sorting-example2').tablesorter({
            headers: {
                5: { sorter: false }
            }
        });

    </script>

    <script>

        $(document).ready(function () {
            // Elements
            var form = $('.wizard'),

				// If layout is centered
				centered;

            // Handle resizing (mostly for debugging)
            function handleWizardResize() {
                centerWizard(false);
            };

            // Register and first call
            $(window).on('normalized-resize', handleWizardResize);

            /*
			 * Center function
			 * @param boolean animate whether or not to animate the position change
			 * @return void
			 */
            function centerWizard(animate) {
                form[animate ? 'animate' : 'css']({ marginTop: Math.max(0, Math.round(($.template.viewportHeight - 30 - form.outerHeight()) / 2)) + 'px' });
            };

            // Initial vertical adjust
            centerWizard(false);

            // Refresh position on change step
            form.on('wizardchange', function () { centerWizard(true); });

            // Validation
            if ($.validationEngine) {
                form.validationEngine();
            }
        });

    </script>
</asp:Content>
