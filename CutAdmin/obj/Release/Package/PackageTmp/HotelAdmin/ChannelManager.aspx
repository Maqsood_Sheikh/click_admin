﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelAdmin/Master.Master" AutoEventWireup="true" CodeBehind="ChannelManager.aspx.cs" Inherits="CutAdmin.HotelAdmin.ChannelManager" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/ChannelManager.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Channel Manager</h1>
            <h4>
                 <span class="lato size20 grey" style="float: right">Name:<span class="orange" id="spName"></span>,                  
            </h4>
           
        </hgroup>
        <hr />
        <div class="with-padding">
            <div class="columns">
               <%-- <div class="two-columns"></div>--%>
                <%--<div class="eight-columns">--%>
                    <div class="respTable">
                        <table class="table responsive-table" id="tbl_Channels">
                            <thead>
                                <tr>
                                    <th scope="col" class="align-center" style="width:10%">S.No</th>
                                    <th scope="col" class="align-center" style="width:70%">Agency Name</th>
                                    <th scope="col" class="align-center" style="width:20%">Channels</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
              <%--  </div>--%>
                <%-- <div class="two-columns">

                 </div>--%>
            </div>

            <div class="twelve-columns-mobile formBTn">
                <br />
                <button type="button" class="button anthracite-gradient" style="float: right" onclick="SaveChannels()">Save</button>
            </div>
        </div>

    </section>
</asp:Content>
