﻿$(document).ready(function () {
    BookingRequests();
    BookingOnHold();
    GetCount();
    GetHotelUnApproved();
    GetRoomUnApproved();
});

function GetCount() {
    $.ajax({
        type: "POST",
        url: "Handler/DashBoard.asmx/GetCount",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            $("#listHotels").empty();
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var CommissionReports = result.CommissionReports;
                var Supplier = result.Supplier;
                $('#hotel_Count').text(CommissionReports);
                $('#Agents').text(Supplier);
            }
        },
    });
}

function GetHotelUnApproved() {
    $.ajax({
        type: "POST",
        url: "Handler/DashBoard.asmx/GetHotelUnApproved",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            $("#listHotels").empty();
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var HotelListCount = result.HotelListCount;

                var html = "";

                html += '<div class="block-title">'
                html += '     <h3><i class="icon-pencil icon-size2"></i> Hotels </h3>'
                html += '</div>'
                html += '<ul class="events">'
                html += '    <li>'
                html += '        <span class="event-date orange" style="font-size: 12px;">Unapproved</span>'
                html += '        <a onclick="GetHotelsData()" style="cursor: pointer;" class="event-description">'
                html += '            <br />'
                html += '            <h4>' + HotelListCount + '</h4>'
                html += '        </a>'
                html += '    </li>'
                html += '</ul>'
                $("#HotelListCount").append(html);
            }
        },
    });
}


function GetRoomUnApproved() {
    $.ajax({
        type: "POST",
        url: "Handler/DashBoard.asmx/GetRoomUnApproved",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            $("#listHotels").empty();
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var RoomListCount = result.RoomListCount;

                var html = "";

                html += '<div class="block-title">'
                html += '     <h3><i class="icon-pencil icon-size2"></i> Rooms </h3>'
                html += '</div>'
                html += '<ul class="events">'
                html += '    <li>'
                html += '        <span class="event-date orange" style="font-size: 12px;">Unapproved</span>'
                html += '        <a onclick="GetRoomsData()" style="cursor: pointer;" class="event-description">'
                html += '            <br />'
                html += '            <h4>' + RoomListCount + '</h4>'
                html += '        </a>'
                html += '    </li>'
                html += '</ul>'
                $("#RoomListCount").append(html);
            }
        },
    });
}


function BookingRequests() {
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/BookingRequests",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {

                var Pending = result.BookingPending;
                var Confirmed = result.BookingConfirmed;
                var Rejected = result.BookingRejected;

                var html = "";

                html += '<div class="block-title">'
                html += '     <h3><i class="icon-pencil icon-size2"></i> Bookings Requests</h3>'
                html += '</div>'
                html += '<ul class="events">'
                html += '    <li>'
                html += '        <span class="event-date orange" style="font-size: 12px;">Pending</span>'
                html += '        <a href="Invoice.aspx?Type=BookingPending&Status=Vouchered" class="event-description">'
                html += '            <br />'
                html += '            <h4>' + Pending + '</h4>'
                html += '        </a>'
                html += '    </li>'
                html += '    <li>'
                html += '        <span class="event-date orange" style="font-size: 12px;">Confirmed</span>'
                html += '       <a href="Invoice.aspx?Type=BookingConfirmed&Status=Vouchered" class="event-description">'
                html += '            <br />'
                html += '            <h4>' + Confirmed + '</h4>'
                html += '        </a>'
                html += '    </li>'
                html += '    <li>'
                html += '        <span class="event-date orange" style="font-size: 12px;">Rejected</span>'
                html += '        <a href="Invoice.aspx?Type=BookingRejected&Status=Vouchered" class="event-description">'
                html += '            <br />'
                html += '            <h4>' + Rejected + '</h4>'
                html += '        </a>'
                html += '    </li>'
                html += '</ul>'
                $("#BookingsRequests").append(html);
            }
        },
    });
}

function BookingOnHold() {
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/BookingOnHold",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {

                var Bookings = result.OnHoldBookings;
                var Requested = result.OnHoldRequested;
                var Confirmed = result.OnHoldConfirmed;
                var html = "";
                html += '<div class="block-title">'
                html += '     <h3><i class="icon-pencil icon-size2"></i> Booking On Hold</h3>'
                html += '</div>'
                html += '<ul class="events">'
                html += '    <li>'
                html += '        <span class="event-date orange" style="font-size: 12px;">Bookings</span>'
                html += '      <a href="Invoice.aspx?Type=Bookings&Status=OnRequest" class="event-description">'
                html += '            <br />'
                html += '            <h4>' + Bookings + '</h4>'
                html += '        </a>'
                html += '    </li>'
                html += '    <li>'
                html += '        <span class="event-date orange" style="font-size: 12px;">Requested</span>'
                html += '        <a href="Invoice.aspx?Type=Requested&Status=OnRequest" class="event-description">'
                html += '            <br />'
                html += '            <h4>' + Requested + '</h4>'
                html += '        </a>'
                html += '    </li>'
                html += '    <li>'
                html += '        <span class="event-date orange" style="font-size: 12px;">Confirmed</span>'
                html += '        <a href="Invoice.aspx?Type=Confirmed&Status=OnRequest" class="event-description">'
                html += '            <br />'
                html += '            <h4>' + Confirmed + '</h4>'
                html += '        </a>'
                html += '    </li>'
                html += '</ul>'
                $("#BookingOnHold").append(html);
            }
        },
    });
}

function GetHotelsData() {
    $.ajax({
        type: "POST",
        url: "Handler/DashBoard.asmx/GetHotelsData",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            $("#listHotels").empty();
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var HotelList = result.HotelList;
                var trForms = '<thead>';
                trForms += '<tr>';
                trForms += '<th scope="col" style="text-align:center">Sr No </th>';
                trForms += '<th scope="col" style="text-align:center">Hootel Name </th>';
                trForms += '<th scope="col" style="text-align:center">Agency Name</th>';
                trForms += '<th scope="col" style="text-align:center">Approved</th>';
                trForms += '<th scope="col" style="text-align:center">Unapproved</th>';
                trForms += '</tr>';
                trForms += '</thead>';
                trForms += '<tbody>';
                for (i = 0; i < HotelList.length; i++) {
                    trForms += '<tr>';
                    trForms += '<td style="text-align:center">' + (i + 1) + '</td>';
                    trForms += '<td style="text-align:center">' + HotelList[i].HotelName + '</td>';
                    trForms += '<td style="text-align:center">' + HotelList[i].AgencyName + '</td>';
                    trForms += '<td style="text-align:center"><a class="button" title="Edit" href="UpdateApprovedHotel.aspx?sHotelID=' + HotelList[i].sid + '&Parent=' + HotelList[i].ParentID + '"><span class="icon-pencil"></span></a></a></td>';
                    trForms += '<td style="text-align:center"><button type="button" class="button anthracite-gradient"  onclick="Unapproved(' + HotelList[i].sid + ')">Unapproved</button></td>';
                    trForms += '</tr>';
                }
                trForms += '</tbody>';
                //$("#tblStaffForms").append(trForms);
                //$('input[type=checkbox]').attr("disabled", true);

                $.modal({
                    content: '<table class="table table-striped table-bordered" id="tblHotelList" cellspacing="0" cellpadding="0" border="0">' + trForms + '' +
                 '</table><br>',
                    //'<button style="float:right" type="submit" class="button anthracite-gradient" onclick="SubmitFormsForRole()">Assign Roles</button>',

                    title: 'Hotels',
                    width: 600,
                    scrolling: true,
                    actions: {
                        'Close': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttons: {
                        'Close': {
                            classes: 'anthracite-gradient displayNone',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttonsLowPadding: true
                });
                $(".tiny").click(function () {
                    $(this).find("input:checkbox").click();
                })
            }
        },
    });
}

function GetRoomsData() {
    $.ajax({
        type: "POST",
        url: "Handler/DashBoard.asmx/GetRoomsData",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            $("#listHotels").empty();
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var RoomList = result.RoomList;
                var trForms = '<thead>';
                trForms += '<tr>';
                trForms += '<th scope="col" style="text-align:center">Sr No </th>';
                trForms += '<th scope="col" style="text-align:center">Room Name </th>';
                trForms += '<th scope="col" style="text-align:center">Hootel Name </th>';
                trForms += '<th scope="col" style="text-align:center">Agency Name</th>';
                trForms += '<th scope="col" style="text-align:center">Approved</th>';
                trForms += '<th scope="col" style="text-align:center">Unapproved</th>';
                trForms += '</tr>';
                trForms += '</thead>';
                trForms += '<tbody>';
                for (i = 0; i < RoomList.length; i++) {
                    trForms += '<tr>';
                    trForms += '<td style="text-align:center">' + (i + 1) + '</td>';
                    trForms += '<td style="text-align:center">' + RoomList[i].RoomType + '</td>';
                    trForms += '<td style="text-align:center">' + RoomList[i].HotelName + '</td>';
                    trForms += '<td style="text-align:center">' + RoomList[i].AgencyName + '</td>';
                    trForms += '<td style="text-align:center"><a class="button" title="Edit" href="UpdateApprovedRooms.aspx?sHotelID=' + RoomList[i].sid + '&HotelName=' + RoomList[i].HotelName + '&RoomId=' + RoomList[i].RoomId + '&ParentId=' + RoomList[i].CreatedBy + '"><span class="icon-pencil"></span></a></a></td>';
                    trForms += '<td style="text-align:center"><button type="button" class="button anthracite-gradient"  onclick="UnapprovedRoom(' + RoomList[i].RoomId + ')">Unapproved</button></td>';
                    trForms += '</tr>';
                }
                trForms += '</tbody>';
                //$("#tblStaffForms").append(trForms);
                //$('input[type=checkbox]').attr("disabled", true);

                $.modal({
                    content: '<table class="table table-striped table-bordered" id="tblHotelList" cellspacing="0" cellpadding="0" border="0">' + trForms + '' +
                 '</table><br>',
                    //'<button style="float:right" type="submit" class="button anthracite-gradient" onclick="SubmitFormsForRole()">Assign Roles</button>',

                    title: 'Rooms',
                    width: 600,
                    scrolling: true,
                    actions: {
                        'Close': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttons: {
                        'Close': {
                            classes: 'anthracite-gradient displayNone',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttonsLowPadding: true
                });
                $(".tiny").click(function () {
                    $(this).find("input:checkbox").click();
                })
            }
        },
    });
}

function Unapproved(HotelCode) {
    var data = {
        HotelCode: HotelCode,
    }
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to Unapproved Hotel?</p>',
            function () {
                $.ajax({
                    type: "POST",
                    url: "Handler/DashBoard.asmx/Unapproved",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    success: function (response) {
                        $("#listHotels").empty();
                        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        if (result.retCode == 1) {
                            Success("Hotel Unapproved successfully!");
                            window.location.reload();
                        }
                        else {
                            Success("Something went wrong!");
                        }
                    }
                });
            },
            function () {
                $('#modals').remove();
            }
           );
}

function UnapprovedRoom(RoomId) {
    var data = {
        RoomId: RoomId,
    }
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to Unapproved Room?</p>',
            function () {
                $.ajax({
                    type: "POST",
                    url: "Handler/DashBoard.asmx/UnapprovedRoom",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    success: function (response) {
                        $("#listHotels").empty();
                        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        if (result.retCode == 1) {
                            Success("Room Unapproved successfully!");
                            setTimeout(function () {
                                window.location.reload();
                            }, 3000);
                        }
                        else {
                            Success("Something went wrong!");
                        }
                    }
                });
            },
            function () {
                $('#modals').remove();
            }
           );
}