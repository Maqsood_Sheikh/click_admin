﻿var reg = new RegExp('[0-9]$');
var floatRegex = new RegExp('[-+]?([0-9]*.[0-9]+|[0-9]+)');
var href;
var Select_TypeOfCash;
var sBankAccount;
var sAmountGiven;
var sDepositeDate;
var ReciptNo;
var Remark;

$(document).ready(function () {
    //$("#dteBankDeposite").datepicker({
    //    changeMonth: true,
    //    changeYear: true,
    //    dateFormat: "dd-mm-yy"
    //});
    //$("#dteCashDeposit").datepicker({
    //    changeMonth: true,
    //    changeYear: true,
    //    dateFormat: "dd-mm-yy"
    //});
    href = "#Bank";
    $('#tabs li a').click(function (e) {
        e.preventDefault()
        href = $(this).attr('href');
        //ClearAllTabs();
    })
});

function ValidateDepositUpdates() {
    debugger;
    var isValid = true;

    if (href == "#Bank") {
        if ($('#Select_TypeOfCash option:selected').val() == "-") {
            isValid = false;
            $("#lblerr_Select_TypeOfCash").css("display", "");
        }
        if ($('#Select_BankAccount option:selected').val() == "-") {
            isValid = false;
            $("#lblerr_Select_BankAccount").css("display", "");
        }

        if ($('#txt_AmountGiven').val() == "") {
            isValid = false;
            $("#lblerr_txt_AmountGiven").css("display", "");
        }
        if ($('#dteBankDeposite').val() == "") {
            isValid = false;
            $("#lblerr_dteBankDeposite").css("display", "");
        }

        if (($('#txt_BankTransactionNumber').val() == "")) {
            isValid = false;
            $("#lblerr_txt_BankTransactionNumber").css("display", "");
        }
    }
    return isValid;
}

function Submit() {
    debugger;
    $("label").css("display", "none");
    var bValue = ValidateDepositUpdates();
    if (bValue == true) {
        //if (href == "#Bank") {
            // sending date in transaction id, to not to change parameter type in handlers and c#
            var BankWAct = ($('#Select_BankAccount option:selected').val()).split('-');
            TypeOfDeposit = $('#Select_TypeOfCash option:selected').val();
            SelectBankName = BankWAct[0];
            AccountNumber = BankWAct[1];
            DepositAmount = $('#txt_AmountGiven').val();
            Datee = $('#dteBankDeposite').val();
            txtRemarks = $("#txt_BankRemarks").val();

            SelectTypeOfCash = "Bank";
            txtEmployeeName = "N/A";
            txtReceiptNumber = $('#txt_BankTransactionNumber').val();
            txtChequeDDNumber = "N/A";
            txtChequeDDNumber = "N/A";
            txtChequeDrwanOnBank = "N/A";
            SelectTypeOfCheque = "N/A";

        //}
        $.ajax({
            url: "../HotelAdmin/Handler/TransactionHandler.asmx/InsertDeposit",
            type: "POST",
            data: '{"Uid":"' + hiddensid + '","BankName":"' + SelectBankName + '","AccountNumber":"' + AccountNumber + '","AmountDeposit":"' + DepositAmount + '","Remarks":"' + txtRemarks + '","TypeOfDeposit":"' + TypeOfDeposit + '","TypeOfCash":"' + SelectTypeOfCash + '","TypeOfCheque":"' + SelectTypeOfCheque + '","EmployeeName":"' + txtEmployeeName + '","ReceiptNumber":"' + txtReceiptNumber + '","ChequeDDNumber":"' + txtChequeDDNumber + '","ChequeDrawn":"' + txtChequeDrwanOnBank + '","Date":"' + Datee + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.Session != 1) {
                    Success("Session Expired!");
                }
                if (result.retCode == 1) {
                    Success("Your deposit details has been submitted successfully");
                    GetBankDetails();
                    GetLastUpdateCreditDetails();
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                }
                if (result.retCode == 0) {
                    Success("Something went wrong while processing your request! Please try again.");
                }
            },
            error: function () {
                Success('An error occured while processing your request! Please try again.');
            }
        });
    }
}




function SaveCash() {
    var isValid = true;
    if ($('#txt_Ammount').val() == "") {
        isValid = false;
        $("#lblerr_txt_Ammount").css("display", "");
    }
    if ($('#dteCashDeposit').val() == "") {
        isValid = false;
        $("#lblerr_dteCashDeposit").css("display", "");
    }

    if ($('#txt_Given_To').val() == "") {
        isValid = false;
        $("#lblerr_txt_Given_To").css("display", "");
    }
    if ($('#txt_ReceiptNumberCheque').val() == "") {
        isValid = false;
        $("#lblerr_txt_ReceiptNumberCheque").css("display", "");
    }
    if (isValid == true) {
        SelectBankName = "";                          //$('#Select_Branch option:selected').val();
        TypeOfDeposit = "Cash";
        DepositAmount = $('#txt_Ammount').val();
        txtEmployeeName = $('#txt_Given_To').val();
        Datee = $('#dteCashDeposit').val();
        txtReceiptNumber = $('#txt_ReceiptNumberCheque').val();
        txtRemarks = $("#txt_CashRemarks").val();
        SelectTypeOfCash = "Cash";
        SelectTypeOfCheque = "N/A";
        AccountNumber = "";
        txtChequeDDNumber = "N/A";
        txtTransactionId = "N/A";
        txtChequeDrwanOnBank = "N/A";

        var data = {
            Uid: hiddensid,
            BankName: SelectBankName,
            AccountNumber: AccountNumber,
            AmountDeposit: DepositAmount,
            Remarks: txtRemarks,
            TypeOfDeposit: TypeOfDeposit,
            TypeOfCash: SelectTypeOfCash,
            TypeOfCheque: SelectTypeOfCheque,
            EmployeeName: txtEmployeeName,
            ReceiptNumber: txtReceiptNumber,
            ChequeDDNumber: txtChequeDDNumber,
            ChequeDrawn: txtChequeDrwanOnBank,
            Date: Datee
        }
        $.ajax({
            url: "../HotelAdmin/Handler/TransactionHandler.asmx/InsertDeposit",
            type: "POST",
            data: JSON.stringify(data),
            //data: '{"Uid":"' + hiddensid + '","BankName":"' + SelectBankName + '","AccountNumber":"' + AccountNumber + '","AmountDeposit":"' + DepositAmount + '","Remarks":"' + txtRemarks + '","TypeOfDeposit":"' + TypeOfDeposit + '","TypeOfCash":"' + SelectTypeOfCash + '","TypeOfCheque":"' + SelectTypeOfCheque + '","EmployeeName":"' + txtEmployeeName + '","ReceiptNumber":"' + txtReceiptNumber + '","ChequeDDNumber":"' + txtChequeDDNumber + '","ChequeDrawn":"' + txtChequeDrwanOnBank + '","Date":"' + Date + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.Session != 1) {
                    Success("Session Expired!");
                }
                if (result.retCode == 1) {
                    Success("Your deposit details has been submitted successfully");
                    GetBankDetails();
                    GetLastUpdateCreditDetails();
                }
                if (result.retCode == 0) {
                    Success("Something went wrong while processing your request! Please try again.");
                }
            },
            error: function () {
                Success('An error occured while processing your request! Please try again.');
            }
        });
    }
}


//BakDeposite()
function ClearAllTabs() {
    $("input[type=text]").val("");
}
function ChangeTransaction(Id) {

    var Type = $("#Select_CreditType").val();
    if (Type == "Credit") {
        $("#dll_Credit").css("display", "")
        $("#dll_Debit").css("display", "none")
    }
    else {
        $("#dll_Credit").css("display", "none")
        $("#dll_Debit").css("display", "")
    }
}
var isValid;
function ValidateCreditLimit() {
    debugger;
    isValid = true;

    if ($("#Select_Limit").val() == "-") {
        $("#lbl_Select_Limit").css("display", "");
        isValid = false;
    }
    if ($("#Select_Limit").val() == "Fix Limit" && $("#txt_LimitGiven").val() == "") {
        $("#txt_LimitGiven").focus()
        $("#lblerr_txt_LimitGiven").css("display", "");
        isValid = false;
    }
    if ($("#txt_LimitAmountGiven").val() == "") {
        $("#txt_LimitAmountGiven").focus()
        $("#lbl_txt_LimitAmountGiven").css("display", "");
        isValid = false;
    }
    if ($("#txt_LimtRemarks").val() == "") {
        $("#txt_LimtRemarks").focus()
        $("#lblerr_txt_LimtRemarks").css("display", "");
        isValid = false;
    }

    return isValid;
}




function UpdateCreditLmit() {
    debugger;
    var Type = $("#Select_Limit").val();
    var OTC, days, Remark, CreditLimit;

    if (Type == "Fix Limit") {
        OTC = 0;
        CreditLimit = $("#txt_LimitAmountGiven").val();
    }
    if (Type == "One Time Limit") {
        OTC = $("#txt_LimitAmountGiven").val();
        CreditLimit = 0;
    }

    days = $("#txt_LimitGiven").val();
    Remark = $("#txt_LimtRemarks").val();
    isValid = ValidateCreditLimit()
    if (isValid == true) {
        $.ajax({
            url: "../HotelAdmin/Handler/TransactionHandler.asmx/UpdateCreditLmit",
            type: "POST",
            data: '{"sid":"' + sid + '","uid":"' + hiddensid + '","MaxCreditAmmount":"' + OTC + '","CreditLimit":"' + CreditLimit + '","days":"' + days + '","Remark":"' + Remark + '" }',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.Session != 1) {
                    Success("Session Expired!");
                }
                if (result.retCode == 1) {
                    Success("New Limit Set.")
                    ClearAllTabs()
                    GetLastUpdateCreditDetails();
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                }
                if (result.retCode == 2) {
                    Success("Credit Limit return is pending.Cannot set new Limit")
                    ClearAllTabs()
                    GetLastUpdateCreditDetails();

                }
                if (result.retCode == 3) {
                    Success("One Time Credit Limit return is pending.Cannot set new Limit")
                    ClearAllTabs()
                    GetLastUpdateCreditDetails();

                }
                if (result.retCode == 0) {
                    GetLastUpdateCreditDetails();
                    Success("Something went wrong while processing your request! Please try again.");
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                }
            },
            //error: function () {
            //    alert('Error occured while processing your request! Please try again.');
            //}
        });
    }

}
function SelectLimit() {
    var LimitType = $("#Select_Limit").val();
    if (LimitType == "Fix Limit") {
        $("#LimitGiven").css("display", "");
    }
    else {
        $("#LimitGiven").css("display", "none");
    }
}
function DebitCredit() {
    var Creditlim, Add, maxlim, type, reduce, availablelimit, Remark, InvoiceNo;
    var bValue = true;
    bValue = ValidateCredit();
    if (bValue == true) {
        if (sid == null) {
            sid = 0;
        }
        if ($("#Select_CreditType").val() == "Credit") {
            type = $("#Select_CreditTransaction").val()
        }
        else {
            type = $("#Select_DebitTransaction").val();
        }
        availablelimit = $("#spAvailableLimit").text();

        if ($("#Select_CreditType").val() == "Credit") {
            if (type == 'Refund' || type == 'Incentive' || type == 'Other') {
                reduce = 0.00;
                Creditlim = 0.00;
                maxlim = 0.00;
                //Creditlim = "";
                //maxlim = "";
                Add = $('#txt_CreditAmmount').val();
            }
        }
        else {
            if (type == 'Against invoice' || type == 'Other') {
                Add = 0.00;
                Creditlim = 0.00;
                maxlim = 0.00;
                reduce = $('#txt_CreditAmmount').val();
                if (type == 'Against invoice') {
                    InvoiceNo = $('#txt_InvoiceNo').val();
                }
            }

        }
        Remark = $('#txt_CreditRemarks').val();
        debugger;
        $.ajax({
            url: "../HotelAdmin/Handler/TransactionHandler.asmx/UpdateCredit",
            type: "POST",
            data: '{"sid":"' + sid + '","uid":"' + hiddensid + '","DepositAmmount":"' + Add + '","CreditAmmount":"' + Creditlim + '","MaxCreditAmmount":"' + maxlim + '","Remark":"' + Remark + '","Invoice":"' + InvoiceNo + '","ReduceAmmount":"' + reduce + '" }',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.Session != 1) {
                    Success("Session Expired!");
                }
                if (result.retCode == 1) {
                    ClearAllTabs()
                    GetLastUpdateCreditDetails();
                    Success($("#Select_CreditType").val() + " " + "Successfully")
                }
                if (result.retCode == 0) {
                    Success("Something went wrong while processing your request! Please try again.");
                }
            },
            error: function () {
                Success('Error occured while processing your request! Please try again.');
            }
        });
    }
}
function ValidateInvoice() {
    var Invoice;
    Invoice = $("#Select_DebitTransaction").val();
    if (Invoice == "Against invoice") {
        $("#Invoice").css("display", "")
    }
    else {
        $("#Invoice").css("display", "none")
    }
}
function ValidateCredit() {
    if ($("#Select_CreditType").val() == "-") {
        $("#Select_CreditType").focus();
        $("#lblerr_Select_CreditTypet").css("display", "")
        return false;

    }
    if ($("#Select_CreditType").val() != "-") {
        if ($("#Select_CreditType").val() == "Credit") {
            if ($("#Select_CreditTransaction").val() == "-") {
                $("#Select_CreditTransaction").focus();
                $("#lblerr_Select_CreditTransaction").css("display", "")
                return false;
            }

            $("#lblerr_Select_CreditTransaction").css("display", "none")
        }
        else if ($("#Select_CreditTransaction").val() == "") {
            $("#Select_DebitTransaction").focus();
            $("#lblerr_Select_CreditTransaction").css("display", "")
            return false;
        }
        else if ($("#Select_DebitTransaction").val() == "Against invoice" && $("#txt_InvoiceNo").val() == "") {
            $("#txt_InvoiceNo").focus();
            $("#lbl_txt_InvoiceNo").css("display", "")
            return false;

        }
    }
    if ($("#txt_CreditAmmount").val() == "") {
        $("#txt_CreditAmmount").focus();
        $("#lbl_txt_CreditAmmount").css("display", "")
        return false;

    }
    if ($("#txt_CreditRemarks").val() == "") {
        $("#txt_CreditRemarks").focus();
        $("#lbl_txt_CreditRemarks").css("display", "")
        return false;
    }
    return true;
}