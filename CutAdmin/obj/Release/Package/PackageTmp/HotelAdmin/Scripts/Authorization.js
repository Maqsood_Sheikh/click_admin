﻿$(document).ready(function () {
    PageLoad();
});

var arrFormList = new Array();
function PageLoad() {
    $.ajax({
        type: "POST",
        url: "Handler/AuthorizationHandler.asmx/GetAuthorizedFormList",
        data: '',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            //debugger;
            var arrFormList = response.d;
            if (arrFormList.length == 0) {
                window.location.href = '../Default.aspx'
            }
            else {
                //
                $.ajax({
                    type: "POST",
                    url: "Handler/AuthorizationHandler.asmx/CheckFranchisee",
                    data: '{}',
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    success: function (response) {
                        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        if (result.retCode == 1) {
                            if (result.Role != "3") {
                                $("#liUpdates").css("display", "");
                                $("#liAdministration").css("display", "");
                                $("#liPackages").css("display", "");
                            }
                        }
                    }
                })

                //
                for (i = 0; i < arrFormList.length; i++) {
                    switch (arrFormList[i]) {
                        case "SupplierList":
                            $("#liSu").css("display", "");
                            break;
                        case "Staff":
                            $("#liSt").css("display", "");
                            break;
                        case "ViewAccount":
                            $("#liVa").css("display", "");
                            break;
                        case "Statements":
                            $("#liSm").css("display", "");
                            break;
                        case "RoomRates":
                            $("#liRr").css("display", "");
                            break;
                        case "HotelList":
                            $("#liHl").css("display", "");
                            break;
                        case "AddHotel":
                            $("#liAh").css("display", "");
                            break;
                        case "GlobalMarkup":
                            $("#liGlm").css("display", "");
                            break;
                        case "GroupMarkup":
                            $("#liGpm").css("display", "");
                            break;
                        case "Tax":
                            $("#liTx").css("display", "");
                            break;
                        case "Role":
                            $("#liRl").css("display", "");
                            break;
                        case "EmailList":
                            $("#liEm").css("display", "");
                            break;
                        case "Location":
                            $("#liLn").css("display", "");
                            break;
                        case "AddOns":
                            $("#liAs").css("display", "");
                            break;
                        case "CancellationPolicy":
                            $("#liCp").css("display", "");
                            break;
                        case "Offer":
                            $("#liOr").css("display", "");
                            break;
                        case "CommissionReport":
                            $("#liCr").css("display", "");
                            break;
                        case "HotelBookingList":
                            $("#liHb").css("display", "");
                            break;
                        case "BankDetails":
                            $("#liBd").css("display", "");
                            break;
                        case "ExchangeRate":
                            $("#liEr").css("display", "");
                            break;
                        case "CommissionStatement":
                            $("#liCt").css("display", "");
                            break;
                    }
                }
            }
        },
        error: function () {
            window.location.href = '../Default.aspx'
        }
    });
}

function IsAuthorized(page) {
    $.ajax({
        type: "POST",
        url: "Handler/AuthorizationHandler.asmx/CheckUserAuthorities",
        data: '{"sFormName":"' + page + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                switch (page) {
                    case "SupplierList":
                        window.location = "SupplierDetails.aspx";
                        break;
                    case "Staff":
                        window.location = "StaffDetails.aspx";
                        break;
                    case "ViewAccount":
                        $("#liVa").css("display", "");
                        window.location = "BankDepositDetails.aspx";
                        break;
                    case "Statements":
                        window.location = "Statements.aspx";
                        break;
                    case "RoomRates":
                        window.location = "RoomRates.aspx";
                        break;
                    case "HotelList":
                        window.location = "HotelList.aspx";
                        break;
                    case "AddHotel":
                        window.location = "AddHotelDetails.aspx";
                        break;
                    case "GlobalMarkup":
                        window.location = "GlobalMarkup.aspx";
                        break;
                    case "GroupMarkup":
                        window.location = "GroupMarkup.aspx";
                        break;
                    case "Tax":
                        window.location = "AddUpdateTax.aspx";
                        break;
                    case "Role":
                        window.location = "RoleManagement.aspx";
                        break;
                    case "EmailList":
                        window.location = "ActivityMails.aspx";
                        break;
                    case "Location":
                        window.location = "AddLocation.aspx";
                        break;
                    case "AddOns":
                        window.location = "AddOns.aspx";
                        break;
                    case "CancellationPolicy":
                        window.location = "CancellationPolicy.aspx";
                        break;
                    case "Offer":
                        window.location = "Offer.aspx";
                        break;
                    case "CommissionReport":
                        window.location = "CommissionReport.aspx";
                        break;
                    case "HotelBookingList":
                        window.location = "Invoice.aspx";
                        break;                    
                    case "BankDetails":
                        window.location = "bankdetails.aspx";
                        break;
                    case "ExchangeRate":
                        window.location = "ExchangeMarkup.aspx";
                        break;
                    case "CommissionStatement":
                        window.location = "CommissionStatement.aspx";
                        break;

                }
            }
        },
        error: function () {
            Success("error occured while checking user authorization");
        }
    });
}

function UserProfile() {
    $.ajax({
        url: "Handler/GenralHandler.asmx/CheckUser",
        type: "post",
        data: {},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1 && result.UserType == "Admin") {
                window.location.href = "AdminProfile.aspx";
            }
            else if (result.retCode == 1 && result.UserType == "AdminStaff") {
                window.location.href = "StaffProfile.aspx";
            }
            else
                window.location.href = "../Default.aspx";
        },
        error: function () {
            Success('Error occured while authenticating user!');
        }
    });
}