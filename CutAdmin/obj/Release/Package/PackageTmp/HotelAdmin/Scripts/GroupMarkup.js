﻿
$(document).ready(function () {
    GetGroup();
    GetGroupMarkup();
});

var List_AgentDetails = new Array();

function GetGroup()
{
    $.ajax({
        type: "POST",
        url: "handler/MarkUpHandler.asmx/GetGroup",
        data: {},
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = JSON.parse(response.d)
                if (result.retCode == 1) {
                    var List = result.Arr;
                    var div = "";
                    for (var i = 0; i < List.length; i++)
                    {
                            div += '<option value="' + List[i].sid + '" >' + List[i].GroupName + '</option>'
                    }

                    $("#SelGroup").append(div);
              
                   // setTimeout(function () { $('#SelGroup option').filter(function () { return $.trim($(this).text()) == 'Group A'; }).attr('selected', true); }, 2000);

                    $("#SelGroup").val('5');
                    $("#DivGroupmark .select span")[0].textContent = "Group A";
                    GetGroupMarkup()
                }
                else if (result.retCode == 0) {
               
                }
            },
            error: function () {
            }

        });
}

function GetGroupMarkup()
{
    var Id = $("#SelGroup").val();

    $.ajax({
        type: "POST",
        url: "handler/MarkUpHandler.asmx/GetGroupMarkup",
        data: JSON.stringify({ Id: Id }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.retCode == 1)
            {
                var List = result.Arr;

                $("#txt_MarkUpPercentage").val(List[0].MarkupPercentage);
                $("#txt_MarkUpAmount").val(List[0].MarkupAmmount);
                $("#txt_CommisionPercentage").val(List[0].CommessionPercentage);
                $("#txt_CommisionAmount").val(List[0].CommessionAmmount);
               
            }
            else if (result.retCode == 0)
            {
                $("#txt_MarkUpPercentage").val("0");
                $("#txt_MarkUpAmount").val("0");
                $("#txt_CommisionPercentage").val("0");
                $("#txt_CommisionAmount").val("0");
            }
        },
        error: function () {
        }

    });
}

function AddGroup() {
    $.modal({
        content:  

                '<div class="columns">'+
                '     <div class="twelve-columns-mobile six-columns-tablet">' +
                '        <label>Groups </label>' +
                '        <div class="full-width">'+
                '            <input name="prompt-value"  id="txt_GroupName"  class="input full-width"  type="text">' +
                '        </div></div>' +
                '</div>' +

                '<div class="standard-tabs margin-bottom" id="add-tabs">'+
               '<div class="respTable">'+
                 '<table class="table responsive-table" id="sorting-advanced">                                                                                                   '+
                    ' <thead>                                                                                                                                                    '+
                        ' <tr>                                                                                                                                                   '+
                         '    <th scope="col">MarkUp Percentage </th>                                                                                                            '+
                            ' <th scope="col" class="align-center hide-on-mobile">MarkUp Amount</th>                                                                            '+
                            ' <th scope="col" class="align-center hide-on-mobile-portrait">Commision Percentage</th>                                                           '+
                            ' <th scope="col" class="align-center">Commision Amount</th>                                                                                        '+
                        ' </tr>                                                                                                                                                  '+
                   '  </thead>                                                                                                                                                   '+
                    ' <tbody>                                                                                                                                                    '+
                    '     <tr>                                                                                                                                                   '+
                    '         <td>                                                                                                                                               '+
                    '             <div class="input full-width">                                                                                                                 '+
                    '                 <input name="prompt-value" id="txt_MarkPercentage" value="" class="input-unstyled full-width" placeholder="0%" type="text"></div>        '+
                    '         </td>                                                                                                                                              '+
                    '         <td>                                                                                                                                               '+
                    '             <div class="input full-width">                                                                                                                 '+
                    '                 <input name="prompt-value" id="txt_MarkUpAmountt" value="" class="input-unstyled full-width" placeholder="0%" type="text"></div>            ' +
                    '         </td>                                                                                                                                              '+
                    '         <td>                                                                                                                                               '+
                    '             <div class="input full-width">                                                                                                                 '+
                    '                 <input name="prompt-value" id="txt_CommPercentage" value="" class="input-unstyled full-width" placeholder="0%" type="text"></div>     '+
                    '         </td>                                                                                                                                              '+
                    '         <td>                                                                                                                                               '+
                    '             <div class="input full-width">                                                                                                                 '+
                    '                 <input name="prompt-value" id="txt_CommAmount" value="" class="input-unstyled full-width" placeholder="0%" type="text"></div>         '+
                    '         </td>                                                                                                                                              '+
                    '     </tr>                                                                                                                                                  '+
                    ' </tbody>                                                                                                                                                   '+
                    '</table>'+
                   ' <p class="text-right">'+
                    ' <button type="button" onclick="AddGroupDetails()" class="button anthracite-gradient UpdateMarkup">Add</button></p>'+
                  '</div>'+
                  '</div>'
            ,

        title: 'Add Group',
        width: 450,
        scrolling: true,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'huge anthracite-gradient glossy full-width',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
}

function AddGroupDetails()
{
    var GroupName = $("#txt_GroupName").val();
    if (GroupName == "")
    {
        Success("Please enter Group Name");
        return false;
    }
    var MarkPercentage = $("#txt_MarkPercentage").val();
    if (MarkPercentage == "") {
        Success("Please enter Markup Percentage");
        return false;
    }
    var MarkUpAmount = $("#txt_MarkUpAmountt").val();
    if (MarkUpAmount == "") {
        Success("Please enter MarkUp Amount");
        return false;
    }
    var CommPercentage = $("#txt_CommPercentage").val();
    if (CommPercentage == "") {
        Success("Please enter Commission Percentage");
        return false;
    }
    var CommAmount = $("#txt_CommAmount").val();
    if (CommAmount == "") {
        Success("Please enter Commission Amount");
        return false;
    }
   
    var data = {
        GroupName: GroupName,
        MarkPercentage: MarkPercentage,
        MarkUpAmount: MarkUpAmount,
        CommPercentage: CommPercentage,
        CommAmount: CommAmount,
    }

    $.ajax({
        type: "POST",
        url: "handler/MarkUpHandler.asmx/AddGroupDetails",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.retCode == 1)
            {
                Success("Group details added successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000)
            }
            else if (result.retCode == 0)
            {
                Success("Something went wrong.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000)
            }
        },
        error: function () {
        }

    });
}

function Update() {
    
    var GroupId = $("#SelGroup").val();
    if (GroupId == "")
    {
        Success("Please Select Group");
        return false;
    }

    var MarkPercentage = $("#txt_MarkUpPercentage").val();
    if (MarkPercentage == "") {
        Success("Please enter Markup Percentage");
        return false;
    }
    var MarkUpAmount = $("#txt_MarkUpAmount").val();
    if (MarkUpAmount == "") {
        Success("Please enter MarkUp Amount");
        return false;
    }
    var CommPercentage = $("#txt_CommisionPercentage").val();
    if (CommPercentage == "") {
        Success("Please enter Commission Percentage");
        return false;
    }
    var CommAmount = $("#txt_CommisionAmount").val();
    if (CommAmount == "") {
        Success("Please enter Commission Amount");
        return false;
    }

    var data = {
        GroupId: GroupId,
        MarkPercentage: MarkPercentage,
        MarkUpAmount: MarkUpAmount,
        CommPercentage: CommPercentage,
        CommAmount: CommAmount,
    }

    $.ajax({
        type: "POST",
        url: "handler/MarkUpHandler.asmx/Update",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                Success("Group Detail Update successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000)
            }
            else if (result.retCode == 0) {
                Success("Something went wrong.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000)
            }
        },
        error: function () {
        }

    });
}


