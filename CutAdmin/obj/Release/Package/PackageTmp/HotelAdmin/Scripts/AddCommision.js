﻿var arrSuppliers = [];

$(function () {
    //GetSuppliers();
    //GetGlobalMarkup(1);
    //  CommisionList();
})

function GetSuppliers() {
    var ddlRequest = '';
    $.ajax({
        type: "POST",
        url: "CommisionHandler.asmx/GetSuppliers",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrSuppliers = result.SupplierList;
                if (arrSuppliers.length > 0) {
                    //ddlRequest += ' <option value="25" selected="selected">From Hotel</option>';
                    for (i = 0; i < arrSuppliers.length; i++) {
                        ddlRequest += '<option value="' + arrSuppliers[i].sid + '">' + arrSuppliers[i].AgencyName + '</option>';
                    }
                    $("#sel_Supplier").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}

function Save() {

    var Supplier = $("#sel_Supplier option:selected").val();
    var Group = $("#sel_Group option:selected").val();
    var Commision = $("#txt_Commision").val();



    if (Supplier == "-") {
        Success("Please Select Supplier")
        return false;
    }
    if (Group == "-") {
        Success("Please Select Group")
        return false;
    }

    var data = {
        Supplier: Supplier,
        Group: Group,
        Commision: Commision
    }

    $.ajax({
        type: "POST",
        url: "CommisionHandler.asmx/SaveCommision",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Commision Added Successfully");
            }
            else {
                Success("Something Went Wrong")
                return false;
            }
        },
        error: function () {
        }
    });
}

function CommisionList() {

    $("#tbl_CommisionList").dataTable().fnClearTable();
    $("#tbl_CommisionList").dataTable().fnDestroy();
    // $("#tbl_BookingList tbody tr").remove();
    $.ajax({
        type: "POST",
        url: "CommisionHandler.asmx/CommisionList",
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var CommisionList = result.CommisionList;
            var trow = '';
            trow += '<tbody>'
            for (var i = 0; i < CommisionList.length; i++) {
                trow += '<tr>';
                trow += '<td>' + (i + 1) + '</td>';
                trow += '<td>' + CommisionList[i].SupplierId + ' </td>';
                trow += '<td>' + CommisionList[i].GroupNo + '</td>';
                trow += '<td>' + CommisionList[i].Commision + '</td>';
                trow += '<td align="center" style="width:10%"><i style="cursor:pointer" aria-hidden="true" title="Update Details"><label class="icon-pencil icon-size2" onclick="EditCommision(\'' + CommisionList[i].Sid + '\',\'' + CommisionList[i].SupplierId + '\',\'' + CommisionList[i].GroupNo + '\',\'' + CommisionList[i].Commision + '\')" ></label></i> | <i style="cursor:pointer" onclick="Delete(\'' + CommisionList[i].Sid + '\')" aria-hidden="true" title="Delete"><label class="icon-trash" ></label></i></td>'
            }

            trow += '</tbody>'

            $("#tbl_CommisionList").append(trow);
            $('[data-toggle="tooltip"]').tooltip()

            $("#tbl_CommisionList").dataTable({
                "bSort": false,
                "width": "104%"
            });
        }

    })
}

function EditCommision(sid, SupplierId, GroupNo, Commision) {

    $("#txt_Commision").val(Commision);
    GetUpSupplier(SupplierId);
    $("#sel_Group option:selected").val(GroupNo);

    var Supplier = $("#sel_Supplier option:selected").val();
    var Group = $("#sel_Group option:selected").val();
    var Commision = $("#txt_Commision").val();



    if (Supplier == "-") {
        Success("Please Select Supplier")
        return false;
    }
    if (Group == "-") {
        Success("Please Select Group")
        return false;
    }

    var data = {
        Supplier: Supplier,
        Group: Group,
        Commision: Commision,
        Sid: sid,
    }

    $.ajax({
        type: "POST",
        url: "CommisionHandler.asmx/EditCommision",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Commision Updated Successfully");
            }
            else {
                Success("Something Went Wrong")
                return false;
            }
        },
        error: function () {
        }
    });
}

function GetUpSupplier(SupplierId) {

    try {
        var Supplier = $.grep(arrSuppliers, function (p) { return p.sid == SupplierId })
                  .map(function (p) { return p; });

        $("#div_Supplier .select span")[0].textContent = Supplier;
        for (var i = 0; i < Supplier.length; i++) {
            //// $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').addclass('checked');         
            $('input[value="' + SupplierId + '"][class="OfferType"]').prop("selected", true);
            $("#sel_Supplier").val(SupplierId);

        }
    }
    catch (ex)
    { }
}

function UpdateCommissionModal(sid, agentCategory, UserType) {
    GetCommision(sid);
    $.modal({
        //    '<div class=" standard-tabs at-left margin-bottom"><ul class="tabs">' +
        //            '<li class="active"><a href="#Seatting">Seattings</a></li>' +
        //            '<li class="active"><a href="#AddComission">Groups </a></li></ul>' +
        //            '<div class="tabs-content"><div id="Seatting" class="with-padding active">' +
        content: '<div class="columns with-padding ">' +
                    '<div class="three-columns  twelve-columns-mobile"><label class="input-info">Billing Cycle</label><br />' +
                    '<input type="text"  onkeypress="return event.charCode >= 48 && event.charCode <= 57" id="txt_Billing" class="input full-width" placeholder="0" >' +
                    '</div>' +
                    '<div class="four-columns  twelve-columns-mobile" style="margin-left:10%">' +
                    '<label class="input-info">On Checking</label><br />' +
                    '<input type="checkbox" id="chk_OnChecking" name="medium-label-3" id="medium-label-3" class="switch tiny Checking" value="true" >' +
                    '</div>' +
                    '<div class="four-columns  twelve-columns-mobile">' +
                    '<label class="input-info">On Cancellation</label><br />' +
                    '<input type="checkbox" id="chk_OnCancel" name="medium-label-3" id="medium-label-3" class="switch tiny Cancel" value="true" >' +
                    '</div>' +
                   // '<p class="text-alignright"><a style="cursor:pointer" href="#" class="button compact anthracite-gradient editpro" onclick="SaveSeatting(\'' + sid + '\')">Save</a></p>' +
                    '</div>' +

                  '<div id="AddComission" class="with-padding ">' +
                    '<table class="table responsive-table" id="sorting-advanced">' +
			     	'<thead>' +
			     	'<tr>' +
			     	'<th scope="col">Star Ratings</th>' +
			     	'<th scope="col" class="align-center">Commision Amount</th>' +
                    '<th scope="col" class="align-center">Commision On</th>' +
			     	'</tr>' +
			     	'</thead>' +
			     	'<tbody>' +
			     	'	<tr class="CommissionGrp">' +
			     	'<td><img src="../img/1.png" /></td>' +
                    '<td><div class="input full-width"><input onkeypress="return event.charCode >= 48 && event.charCode <= 57" name="prompt-value" id="txt_Commision1" value="" class="input-unstyled full-width ClsCommision" placeholder="0" type="text"></div></td>' +
                    '<td><input type="radio" name="1" id="rdNight1" value="2" class="radio mid-margin-left checked"> <label for="radio-2" class="label">Per Night</label></br></br><input type="radio" name="1" id="rdBooking1" value="2" class="radio mid-margin-left"> <label for="radio-2" class="label">Per Booking</label></td>' +
			     	'	</tr>' +
                    '<tr  class="CommissionGrp">' +
			     	'<td><img src="../img/2.png" /></td>' +
                    '<td><div class="input full-width"><input onkeypress="return event.charCode >= 48 && event.charCode <= 57" name="prompt-value" id="txt_Commision2" value="" class="input-unstyled full-width ClsCommision" placeholder="0" type="text"></div></td>' +
                    '<td><input type="radio" name="2" id="rdNight2" value="2" class="radio mid-margin-left checked"> <label for="radio-2" class="label">Per Night</label></br></br><input type="radio" name="2" id="rdBooking2" value="2" class="radio mid-margin-left"> <label for="radio-2" class="label">Per Booking</label></td>' +
			     	'</tr>' +
                    '<tr  class="CommissionGrp">' +
			     	'<td><img src="../img/3.png" /></td>' +
                    '<td><div class="input full-width"><input onkeypress="return event.charCode >= 48 && event.charCode <= 57" name="prompt-value" id="txt_Commision3" value="" class="input-unstyled full-width ClsCommision" placeholder="0" type="text"></div></td>' +
                    '<td><input type="radio" name="3" id="rdNight3" value="2" class="radio mid-margin-left checked"> <label for="radio-2" class="label">Per Night</label></br></br><input type="radio" name="3" id="rdBooking3" value="2" class="radio mid-margin-left"> <label for="radio-2" class="label">Per Booking</label></td>' +
			     	'</tr>' +
                    '<tr  class="CommissionGrp">' +
			     	'<td><img src="../img/4.png" /></td>' +
                    '<td><div class="input full-width"><input onkeypress="return event.charCode >= 48 && event.charCode <= 57" name="prompt-value" id="txt_Commision4" value="" class="input-unstyled full-width ClsCommision" placeholder="0" type="text"></div></td>' +
                    '<td><input type="radio" name="4" id="rdNight4" value="2" class="radio mid-margin-left checked"> <label for="radio-2" class="label">Per Night</label></br></br><input type="radio" name="4" id="rdBooking4" value="2" class="radio mid-margin-left"> <label for="radio-2" class="label">Per Booking</label></td>' +
			     	'</tr>' +
			     	'<tr  class="CommissionGrp">' +
			     	'<td><img src="../img/5.png" /></td>' +
                      '<td><div class="input full-width"><input onkeypress="return event.charCode >= 48 && event.charCode <= 57" name="prompt-value" id="txt_Commision5" value="" class="input-unstyled full-width ClsCommision" placeholder="0" type="text"></div></td>' +
                    '<td><input type="radio" name="5" id="rdNight5" value="2" class="radio mid-margin-left checked"> <label for="radio-2" class="label">Per Night</label></br></br><input type="radio" name="5" id="rdBooking5" value="2" class="radio mid-margin-left"> <label for="radio-2" class="label">Per Booking</label></td>' +
			     	'</tr>' +
			     	'</tbody>' +
			        '</table><br>' +
                    '<div class="columns">' +
                    //'<div class="three-columns bold"</div>' +
                    //'<div class="five-columns bold"><br><div class="input full-width"></div>' +
                    '<div class="new row-columns twelve-columns bold"><button type="button" style="float:right" class="button anthracite-gradient UpdateMarkup" onclick="UpdateCommission(' + sid + ')">Update</button>' +
                    '</div>' +
                    '</div>',

        title: '  <i class="icon-gear"></i>  Comission Setting',
        width: 500,
        scrolling: true,
        height:500,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'huge anthracite-gradient displayNone',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
}

function UpdateCommission(Id) {
       
    var Cycle = $("#txt_Billing").val();
    if (Cycle == "")
    {
        Success("Please enter cycle.");
        return false;
    }

    var Commision = new Array();
    var Billing = $("#txt_Billing").val();
    var CountCommision = $(".ClsCommision");
    for (var i = 0; i < CountCommision.length; i++)
    {
        var PerNight = false, IsPerBooking = false;
        var arrNode = $($(".CommissionGrp")[i]).find(".replacement");
        if ($(arrNode[0]).hasClass("checked"))
            PerNight = true;
        else
            IsPerBooking = true;

        var arrCommission = {
            GroupID: parseInt(i + 1),
            SupplierID: Id,
            Commission: $("#txt_Commision" + parseInt(i + 1)).val(),
            PerNight: PerNight,
            PerBooking: IsPerBooking,
            ParentID: 0,

        }
        Commision.push(arrCommission);
    }
    $.ajax({
        type: "POST",
        url: "Handler/CommisionHandler.asmx/SaveCommision",
        data: JSON.stringify({ ListCommision: Commision }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                SaveSeatting(Id, Billing);
            }
            else {
                Success("Something Went Wrong")
                setTimeout(function () {
                    window.location.reload();
                }, 2000);

                return false;
            }
        },
        error: function () {
        }
    });
}


function SaveSeatting(uid, Billing) {
    var OnCheckIn = false;
    if ($(".Checking").hasClass("checked"))
        OnCheckIn = true;
    var OnCancel = false;
    if ($(".Cancel").hasClass("checked"))
        OnCancel = true;
    var Cycle = $("#txt_Billing").val();
    var data = {
        Supplier: uid,
        OnCheckIn: OnCheckIn,
        OnCancel: OnCancel,
        Cycle: Cycle
    }
    $.ajax({
        type: "POST",
        url: "Handler/CommisionHandler.asmx/_Seatting",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            //$("#modals").remove();
            if (result.retCode == 1)
                AddBillingCycle(uid, Billing);
            else
                Success("Something Went Wrong");

        }

    });
}

function AddBillingCycle(Id, Billing) {

    var data = {
        Billing: Billing,
        Id: Id,
    }
    $.ajax({
        type: "POST",
        url: "Handler/GenralHandler.asmx/AddBillingCycle",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            $("#modals").remove();
            if (result.retCode == 1)
                Success("Commision Setting Saved Successfully.");
            else
                Success("Something Went Wrong");
        },
        error: function (ex) {
            Success("Something Went Wrong")
        }
        
    });
}

function GetCommision(sid) {
    var data = {
        Supplier: sid,
    }
    $.ajax({
        type: "POST",
        url: "Handler/CommisionHandler.asmx/GetCommision",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var SupplierList = result.SupplierList;
                if (result.sCycle.CommisionCycle == null)
                    $("#txt_Billing").val(0)
                 else
                    $("#txt_Billing").val(result.sCycle.CommssionsDays)
                if (SupplierList != 0) {
                    for (var i = 0; i < SupplierList.length; i++) {
                        $("#txt_Commision" + parseInt(i + 1)).val(SupplierList[i].Commission);
                        if (SupplierList[i].PerNight == true) {
                            $("#rdNight" + parseInt(i + 1)).click();
                        }
                        if (SupplierList[i].PerBooking == true) {
                            $("#rdBooking" + parseInt(i + 1)).click();
                        }
                    }
                }

                if (result.sCycle.RefundCancel)
                    $(".Cancel").click();
                if (result.sCycle.CheckInCommission)
                    $(".Checking").click();
            }
            else {
                Success("Something Went Wrong")
                return false;
            }
        },
        error: function () {
        }
    });
}