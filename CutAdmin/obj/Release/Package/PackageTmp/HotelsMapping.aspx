﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="HotelsMapping.aspx.cs" Inherits="CutAdmin.HotelsMapping" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/HotelMapping.js?V=1.1"></script>

    <!-- Additional styles -->
	<link rel="stylesheet" href="css/styles/form.css?v=1">
	<link rel="stylesheet" href="css/styles/switches.css?v=1">
	<link rel="stylesheet" href="css/styles/table.css?v=1">

	<!-- DataTables -->
	<link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

    <style type="text/css">
      
        .pageNumber {
            padding: 2px;
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: auto;
        }

    </style>



	<!-- Microsoft clear type rendering -->
	<meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

		<hgroup id="main-title" class="thin">
			<h3> Hotels Mapping</h3><hr />
		</hgroup>
        
        <div class="with-padding ">
			<div class="table-header white-gradient">
                <div class="columns">
                    <div class="three-columns">
                        <input type="text"  id="txtCity" placeholder="Search with City" class="input  ui-autocomplete-input full-width" >
                    </div>
                     <div class="three-columns">                         
				          <input type="text"  id="txtHotelName" placeholder="Search with Hotel"  class="input  ui-autocomplete-input full-width" >
                     </div>
                      <div class="three-columns">
                          <input type="text"  id="txtfdate" placeholder="From Date" class="input  ui-autocomplete-input full-width" >
                      </div>
                    <div class="three-columns">
                        <input type="text"  id="txtldate"  placeholder="To Date" size="30" class="input ui-autocomplete-input full-width" >
                    </div>
                    <hr />
                      <div class="new-row three-columns" style="float:right">
                            <input id="hdnDCode" hidden="hidden" value="DXB"/>
                          <button type="button" class="button  glossy" id="btn_GetCityCode"  onclick="GetHotelDetails();" style="float:right">Search</button>
                      </div>
                </div>
			   
                <%--Nationality:
				<select name="select90"  id="Select_Nationality" class="select blue-gradient glossy mid-margin-left">
                    <option selected="selected" value="-" >India</option>
				</select>--%>		
            </div>
               
            <table class="table responsive-table" id="tbl_GetHotelMapping" width="100%">
                <thead>
                    <tr>

                        <th align="center" scope="col">Supplier </th>
                        <th align="center" scope="col">Hotel Name</th>
                        <th align="center" scope="col">Ratings</th>
                        <th align="center" scope="col">Address</th>
                        <th align="center" scope="col">Select</th>
                    </tr>
                </thead>
                 <tbody>
                     
                </tbody>
                
            </table>
           <hr />
              <img src="loader.gif"  style="-webkit-user-select: none;display: none;padding-left:20%;padding-right:20%" id="img_file_attach"  alt="" />
            
			<div  class="table-footer white-gradient">
                <button type="button" class="button glossy mid-margin-right blue-gradient float-right" id="btn_Map" onclick="MapHotel();">Map Selected Hotel</button>
			<br /><br />
			</div>

	
		</div>

        <input id="MhotelID" hidden="hidden"/>

	</section>
	<!-- End main content -->



		<!-- JavaScript at the bottom for fast page loading -->
	<!-- Scripts -->
	<script src="js/libs/jquery-1.10.2.min.js"></script>
	<script src="js/setup.js"></script>

	<!-- Template functions -->
	<script src="js/developr.input.js"></script>
	<script src="js/developr.navigable.js"></script>
	<script src="js/developr.notify.js"></script>
	<script src="js/developr.scroll.js"></script>
	<script src="js/developr.tooltip.js"></script>
	<script src="js/developr.table.js"></script>

	<!-- Plugins -->
	<script src="js/libs/jquery.tablesorter.min.js"></script>
	<script src="js/libs/DataTables/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="css/styles/modal.css?v=1">
    <script src="js/developr.modal.js"></script>
	
    <script>
	    

	    // Call template init (optional, but faster if called manually)
	    $.template.init();

	    // Table sort - DataTables
	    var table = $('#sorting-advanced');
	    table.dataTable({
	        'aoColumnDefs': [
				{ 'bSortable': false, 'aTargets': [0, 5] }
	        ],
	        'sPaginationType': 'full_numbers',
	        'sDom': '<"dataTables_header"lfr>t<"dataTables_footer"ip>',
	        'fnInitComplete': function (oSettings) {
	            // Style length select
	            table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
	            tableStyled = true;
	        }
	    });

	    // Table sort - styled
	    $('#sorting-example1').tablesorter({
	        headers: {
	            0: { sorter: false },
	            5: { sorter: false }
	        }
	    }).on('click', 'tbody td', function (event) {
	        // Do not process if something else has been clicked
	        if (event.target !== this) {
	            return;
	        }

	        var tr = $(this).parent(),
				row = tr.next('.row-drop'),
				rows;

	        // If click on a special row
	        if (tr.hasClass('row-drop')) {
	            return;
	        }

	        // If there is already a special row
	        if (row.length > 0) {
	            // Un-style row
	            tr.children().removeClass('anthracite-gradient glossy');

	            // Remove row
	            row.remove();

	            return;
	        }

	        // Remove existing special rows
	        rows = tr.siblings('.row-drop');
	        if (rows.length > 0) {
	            // Un-style previous rows
	            rows.prev().children().removeClass('anthracite-gradient glossy');

	            // Remove rows
	            rows.remove();
	        }

	        // Style row
	        tr.children().addClass('anthracite-gradient glossy');

	        // Add fake row
	        $('<tr class="row-drop">' +
				'<td colspan="' + tr.children().length + '">' +
					'<div class="float-right">' +
						'<button type="submit" class="button glossy mid-margin-right">' +
							'<span class="button-icon"><span class="icon-mail"></span></span>' +
							'Send mail' +
						'</button>' +
						'<button type="submit" class="button glossy">' +
							'<span class="button-icon red-gradient"><span class="icon-cross"></span></span>' +
							'Remove' +
						'</button>' +
					'</div>' +
					'<strong>Name:</strong> John Doe<br>' +
					'<strong>Account:</strong> admin<br>' +
					'<strong>Last connect:</strong> 05-07-2011<br>' +
					'<strong>Email:</strong> john@doe.com' +
				'</td>' +
			'</tr>').insertAfter(tr);

	    }).on('sortStart', function () {
	        var rows = $(this).find('.row-drop');
	        if (rows.length > 0) {
	            // Un-style previous rows
	            rows.prev().children().removeClass('anthracite-gradient glossy');

	            // Remove rows
	            rows.remove();
	        }
	    });

	    // Table sort - simple
	    $('#sorting-example2').tablesorter({
	        headers: {
	            5: { sorter: false }
	        }
	    });

	</script>


    <script>
        $(function myfunction() {
            $("#txtCity").autocomplete({
                source: function (request, response) {
                    jQuery.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "HotelHandler.asmx/GetDestinationCode",
                        data: "{'name':'" + document.getElementById('txtCity').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    jQuery('#hdnDCode').val(ui.item.id);
                }
            });
        })
    </script>
</asp:Content>
