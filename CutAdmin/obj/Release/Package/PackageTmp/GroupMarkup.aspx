﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="GroupMarkup.aspx.cs" Inherits="CutAdmin.GroupMarkup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    <script src="Scripts/GroupMarkup.js?v=1.1"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">


        <hgroup id="main-title" class="thin">
            <h1>GroupMarkup</h1>
        </hgroup>

        <div class="with-padding">

            <div class="columns">
                <div class="one-column four-columns-mobile bold">
                    <label>Groups:</label>
                </div>
                <div class="four-columns eight-columns-mobile">
                    <div class="full-width Groupmark" id="DivGroupmark">
                        <select id="SelGroup" onchange="GetGroupMarkup('lst_GroupMarkupHotel')" name="validation-select" class="select Groupmark">
                        </select>
                    </div>
                </div>
                <%--<div class="four-columns eight-columns-mobile">
                    <div class="full-width Groupmark" id="DivGroupmark">
                        <span class="select replacement select-styled-list" tabindex="0">
                            <select id="selGroup" name="validation-select" class="select" onchange="GetGroupMarkup('lst_GroupMarkupHotel')" tabindex="-1">
                                <option selected="selected" value="-">Select Any Group</option>
                               
                            </select><span class="select-value">Select Any Group</span><span class="select-arrow"></span><span class="drop-down"></span></span>
                    </div>
                </div>--%>
                <div class="two-columns six-columns-mobile bold text-right">
                    <button type="button" id="btn_RegiterAgent" class="button anthracite-gradient Updategroup" onclick="AddGroup();" title="Submit Details">Add Group</button>
                </div>
            </div>


            <div class="standard-tabs margin-bottom tabs-active" id="add-tabs" style="height: 30px;">

                <ul class="tabs">
                    <li class="active" id="lst_GroupMarkupHotel"><a href="#tblGroupMarkupHotel">Hotel</a></li>
                    <li id="lst_GroupMarkupVisa"><a href="#tblGroupMarkupVisa">Visa</a></li>
                    <li id="lst_GroupMarkupOtb"><a href="#tblGroupMarkupOtb">OTB</a></li>
                    <li id="lst_GroupMarkupPackage"><a href="#tblGroupMarkupPackages">Packages</a></li>
                </ul>

                <div class="tabs-content" style="min-height: 29px;">
                    <span class="tabs-back with-left-arrow top-bevel-on-light dark-text-bevel">Back</span>
                    <div id="tblGroupMarkupHotel" class="with-padding tab-active">
                        <p>
                            service tax on markup: <input type="checkbox" name="checkbox-2" id="chkServicetax" value="1"><%--<span class="checkbox mid-margin-left replacement" tabindex="0"><span class="check-knob"></span>
                                <input type="checkbox" name="checkbox-2" id="checkbox-2" value="1" class="" tabindex="-1"></span>--%>
                        </p>
                        <div class="resptable">
                            <table class="table responsive-table" id="tblhotels">

                                <thead>
                                    <tr>
                                        <th scope="col">s.no</th>
                                        <th scope="col">markup percentage </th>
                                        <th scope="col" class="align-center hide-on-mobile">markup amount:</th>
                                        <th scope="col" class="align-center hide-on-mobile-portrait">commision percentage: </th>
                                        <th scope="col" class="align-center">commision amount:</th>

                                    </tr>
                                </thead>


                                <tbody >
                                </tbody>
                            </table>
                            <p class="text-right" style="margin-top: 15px; text-align: right;">
                                <button type="button" onclick="UpdateGroupMarkup()" class="button anthracite-gradient UpdateMarkup">Update</button>
                            </p>

                        </div>
                    </div>
                    <div id="tblGroupMarkupVisa" class="with-padding" style="display: none;">

                        <table class="table responsive-table" id="sorting-advanced" colspan="2">

                            <thead>
                                <tr>
                                    <th scope="col">Processing: </th>
                                    <th scope="col">Visa Type:</th>
                                    <th scope="col" class="align-center hide-on-mobile">Nationality:</th>
                                    <th scope="col" class="align-center hide-on-mobile-portrait">Markup Amount:</th>
                                    <th scope="col" class="align-center">Markup Percentage:</th>

                                </tr>
                            </thead>


                            <tbody>
                                <tr>
                                    <td>
                                        <div class="four-columns eight-columns-mobile">
                                            <div class="full-width Groupmark" >
                                                <select id="selProcessing" onchange="GetVisaMarkup()" name="validation-select" class="select" tabindex="-1">
                                                        <option value="-">-Select-</option>
                                                        <option value="1" selected="selected">Normal</option>
                                                        <option value="2">Urgent</option>
                                                    </select>
                                            </div>
                                        </div>
                                        <%--<div class="full-width">
                                                <span class="select replacement select-styled-list" tabindex="0">
                                                    <select id="selGmProcessing" onchange="GetVisaMarkupDetails()" name="validation-select" class="select" tabindex="-1">
                                                        <option value="-">-Select-</option>
                                                        <option value="1" selected="selected">Normal</option>
                                                        <option value="2">Urgent</option>
                                                    </select><span class="select-value">Normal</span><span class="select-arrow"></span><span class="drop-down"></span></span>
                                            </div>--%>
                                    </td>
                                    <td>
                                        <div class="full-width">
                                           
                                                <select id="selService" onchange="GetVisaMarkup()" name="validation-select" class="select">
                                                    <option value="-" selected="selected">--Select Visa Type--</option>
                                                    <option value="6">96 hours Visa transit Single Entery</option>
                                                    <option value="1">14 days Service VISA</option>
                                                    <option value="2">30 days Tourist Single Entry</option>
                                                    <option value="3">30 days Tourist Multiple Entry</option>
                                                    <option value="4">90 days Tourist Single Entry</option>
                                                    <option value="5">90 days Tourist Multiple Entry</option>
                                                    <option value="7">90 Days Tourist Single Entry Convertible</option>
                                                </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="full-width">
                                           
                                                <select id="selVisaCountry" onchange="GetVisaMarkup()" name="validation-select" class="select" tabindex="-1">
                                                    <option value="-" selected="selected">--Select Visa Country--</option>
                                                    <option value="Indian">Indian</option>
                                                    <option value="Pakistani">Pakistani</option>
                                                    <option value="Sri Lankan">Sri Lankan</option>
                                                    <option value="South African">South African</option>
                                                    <option value="Mozambican">Mozambican</option>
                                                    <option value="Malaysian">Malaysian</option>
                                                    <option value="Indonesian">Indonesian</option>
                                                    <option value="Philippines">Philippines</option>
                                                </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input full-width">
                                            <input name="prompt-value"  id="txtVisaMarkupAmmount" value="" class="input-unstyled full-width" placeholder="0" type="text">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input full-width">
                                            <input name="prompt-value" id="txtMrkUpPrcntgVisaNormal" value="" class="input-unstyled full-width" placeholder="0%" type="text">
                                        </div>
                                    </td>
                                </tr>

                            </tbody>

                        </table>
                        <p class="text-right" style="margin-top: 15px; text-align: right;">
                            <button type="button" onclick="UpdateGroupMarkup()" class="button anthracite-gradient UpdateMarkup">Update</button>
                        </p>

                    </div>
                    <div id="tblGroupMarkupOtb" class="with-padding" style="display: none;">

                        <div class="respTable">
                            <table class="table responsive-table" id="sorting-advanced">

                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col" class="align-center">MarkUp Percentage: </th>
                                        <th scope="col" class="align-center">MarkUp Amount:</th>
                                    </tr>
                                </thead>


                                <tbody>
                                    <tr>
                                        <td id="lblAirlines1">Air Arabia</td>
                                        <td>
                                            <div class="input full-width">
                                                <input name="prompt-value" id="txtMrkUpPrcntgOtb01" value="" class="input-unstyled full-width" placeholder="0.00" type="text">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input full-width">
                                                <input name="prompt-value" id="txtMrkUpAmntOtb01" value="" class="input-unstyled full-width" placeholder="20.00" type="text">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="lblAirlines2">Air India</td>
                                        <td>
                                            <div class="input full-width">
                                                <input name="prompt-value" id="txtMrkUpPrcntgOtb02" value="" class="input-unstyled full-width" placeholder="0.00" type="text">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input full-width">
                                                <input name="prompt-value" id="txtMrkUpAmntOtb02" value="" class="input-unstyled full-width" placeholder="20.00" type="text">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="lblAirlines3">Air India Express</td>
                                        <td>
                                            <div class="input full-width">
                                                <input name="prompt-value" id="txtMrkUpPrcntgOtb03" value="" class="input-unstyled full-width" placeholder="0.00" type="text">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input full-width">
                                                <input name="prompt-value" id="txtMrkUpAmntOtb03" value="" class="input-unstyled full-width" placeholder="20.00" type="text">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="lblAirlines4">Jet Airways Airlines</td>
                                        <td>
                                            <div class="input full-width">
                                                <input name="prompt-value" id="txtMrkUpPrcntgOtb04" value="" class="input-unstyled full-width" placeholder="0.00" type="text">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input full-width">
                                                <input name="prompt-value" id="txtMrkUpAmntOtb04" value="" class="input-unstyled full-width" placeholder="20.00" type="text">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="lblAirlines5">Indigo Airlines</td>
                                        <td>
                                            <div class="input full-width">
                                                <input name="prompt-value" id="txtMrkUpPrcntgOtb05" value="" class="input-unstyled full-width" placeholder="0.00" type="text">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input full-width">
                                                <input name="prompt-value" id="txtMrkUpAmntOtb05" value="" class="input-unstyled full-width" placeholder="20.00" type="text">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="lblAirlines6">Oman Airways</td>
                                        <td>
                                            <div class="input full-width">
                                                <input name="prompt-value" id="txtMrkUpPrcntgOtb06" value="" class="input-unstyled full-width" placeholder="0.00" type="text">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input full-width">
                                                <input name="prompt-value" id="txtMrkUpAmntOtb06" value="" class="input-unstyled full-width" placeholder="20.00" type="text">
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>

                            </table>
                            <p class="text-right" style="margin-top: 15px; text-align: right;">
                                <button type="button" onclick="UpdateGroupMarkup()" class="button anthracite-gradient UpdateMarkup">Update</button>
                            </p>
                        </div>

                    </div>
                    <div id="tblGroupMarkupPackages" class="with-padding" style="display: none;">

                        <table class="table responsive-table" id="sorting-advanced">

                            <thead>
                                <tr>
                                    <th scope="col">S.No</th>
                                    <th scope="col">MarkUp Percentage:</th>
                                    <th scope="col" class="align-center hide-on-mobile">MarkUp Amount:</th>
                                    <th scope="col" class="align-center hide-on-mobile-portrait">Commision Percentage: </th>
                                    <th scope="col" class="align-center">Commision Amount:</th>

                                </tr>
                            </thead>


                            <tbody>
                                <tr>
                                    <td id="lblSupplier">ClickUrTrip</td>
                                    <td>
                                        <div class="input full-width">
                                            <input name="prompt-value" id="txtMrkUpPrcntg" value="" class="input-unstyled full-width" placeholder="0" type="text">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input full-width">
                                            <input name="prompt-value" id="txtMrkUpAmnt" value="" class="input-unstyled full-width" placeholder="0%" type="text">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input full-width">
                                            <input name="prompt-value" id="txtCommisionPrcntg"  value="" class="input-unstyled full-width" placeholder="0" type="text">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input full-width">
                                            <input name="prompt-value" id="txtCommisionAmnt" value="" class="input-unstyled full-width" placeholder="0%" type="text">
                                        </div>
                                    </td>
                                </tr>
                            </tbody>

                        </table>
                        <p class="text-right" style="margin-top: 15px; text-align: right;">
                            <button type="button" onclick="UpdateGroupMarkup()" class="button anthracite-gradient UpdateMarkup">Update</button>
                        </p>
                    </div>

                </div>

            </div>


        </div>

    </section>
</asp:Content>
