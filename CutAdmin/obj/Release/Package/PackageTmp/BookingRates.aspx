﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="BookingRates.aspx.cs" Inherits="CutAdmin.BookingRates" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/AddOnsCharges.js?v=1.1"></script>
    <script src="Scripts/Booking.js?v=2.45"></script>

    <script>
        $(document).ready(function () {
            GetBookingRates();
        });
    </script>
    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">

    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
    <link href="css/ScrollingTable.css" rel="stylesheet" />
    <script src="Scripts/moments.js"></script>
    <%--<link rel="stylesheet" href="css/styles/modal.css?v=1">--%>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">

        <hgroup id="main-title" class="thin">
            <h1>Booking Rates</h1>
            <hr />
        </hgroup>

        <div class="with-padding">
            <div class="row block table-head margin-bottom">

                <span class="block-title anthracite-gradient glossy">
                    <h3 id="lbl_Hotel">
                    </h3>

                    <label id="lbl_address" style="float: right"></label>
                    <label id="lbl_nattionalty" style="float: left"></label>
                    <div class="clear"></div>
                </span>

                <%--   <h3 class="block-title blue-gradient glossy" id="lbl_nattionalty" style="float:right"></h3>--%>


                <div class="with-padding withpad" id="div_Rooms">

                    <%--    <div class="one-columns" style="float: right">
                <input type="button" class="button blue-gradient" onclick="" value="Book" />
            </div>--%>
                </div>
                <div class="with-padding " id="div_RateList">
                </div>
            </div>


        </div>

    </section>
</asp:Content>
