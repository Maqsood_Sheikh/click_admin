﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="StopSale.aspx.cs" Inherits="CutAdmin.StopSale" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">

    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">
    <script src="Scripts/HotelInventory.js?v=1.0"></script>


    <%--<script type="text/javascript">
        $(document).ready(function () {
            // GetCountry();

            $("#datepicker_From").datepicker({
                dateFormat: "dd-mm-yy",
                //minDate: "dateToday",
                autoclose: true,
            });
            $("#datepicker_To").datepicker({
                // minDate: $("#datepicker_To").text(),
                dateFormat: "dd-mm-yy",
                autoclose: true,
            });


        });


    </script>--%>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- Main content -->
    <section role="main" id="main">
        <div class="with-padding">
            <div class="row block margin-bottom">



                <span class="block-title blue-gradient glossy">
                    <h3>
                        <u>
                            <label id="lbl_Hotel" style="float: left"></label>
                        </u>


                    </h3>
                    <br />

                    <label id="lbl_roomtype" style="float: right"></label>
                    <br />
                    <label id="lbl_address"></label>
                </span>


                <div class="with-padding " id="div_Rooms">
                    <div class="columns">
                        <div class="new-row three-columns" id="" style="">

                            <small class="input-info">Select Room*:</small>
                            <select id="ddlRoom" class="full-width select validate[required]"  onchange="">
                                
                            </select>
                        </div>
                        <div class="three-columns" id="" style="">

                            <small class="input-info">Select Rate Type*:</small>
                            <select id="ddlTimeSlots" class="full-width glossy select multiple-as-single easy-multiple-selection check-list Rate" multiple onchange="fnRatType()">
                                <%--<option class="" value="" selected="selected">Rate Type</option>--%>
                                <option class="" value="Contracted">Contracted</option>
                                <option class="" value="Promo">Promo</option>
                                <option class="" value="Tactical">Tactical</option>
                            </select>
                        </div>
                        <div class="six-columns" id="DatesUI" style="">
                        </div>

                        <div class="new-row twelve-columns" id="" >
                            <button type="button" style="float:right" value="ss" class="button glossy mid-margin-right" onclick="SaveInventory(this.value)" title="Submit Details">
                                <span class="button-icon"><span class="icon-tick"></span></span>
                                Save
                            </button>
                        </div>
                    </div>


                </div>

            </div>


        </div>

    </section>
    <!-- End main content -->




    <!-- JavaScript at the bottom for fast page loading -->
    <!-- Scripts -->
    <script src="js/libs/jquery-1.10.2.min.js"></script>
    <script src="js/setup.js"></script>

    <!-- Template functions -->
    <script src="js/developr.input.js"></script>
    <script src="js/developr.navigable.js"></script>
    <script src="js/developr.notify.js"></script>
    <script src="js/developr.scroll.js"></script>
    <script src="js/developr.tooltip.js"></script>
    <script src="js/developr.table.js"></script>

    <!-- Plugins -->
    <script src="js/libs/jquery.tablesorter.min.js"></script>
    <script src="js/libs/DataTables/jquery.dataTables.min.js"></script>

</asp:Content>
