﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="HotelOffer.aspx.cs" Inherits="CutAdmin.HotelOffer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="Scripts/HotelOffer.js"></script>

     <!-- Additional styles -->
	<link rel="stylesheet" href="css/styles/form.css?v=1">
	<link rel="stylesheet" href="css/styles/switches.css?v=1">
	<link rel="stylesheet" href="css/styles/table.css?v=1">

	<!-- DataTables -->
	<link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">
   
    <!-- glDatePicker -->
	<link rel="stylesheet" href="js/libs/glDatePicker/developr.fixed.css?v=1">

	<!-- Microsoft clear type rendering -->
	<meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

		
        <div class="with-padding">
           
            <div class="columns" id="DivAddOffer">

                <h3 class="wrapped margin-left white" style="background-color: #006699">Create Offer</h3>
                <div class="new-row twelve-columns">
                    <input id="htlname" style="background:none;border:none;color:#006699;font-size:medium;font-weight:bold;width:100%" readonly/>
                </div>
               
                <div class="new-row three-columns">
                    <h6>Room:</h6>
                   <select id="SelectRoomCat" class="full-width  select  expandable-list" onchange="CheckOfferDuplicate()">
                       <option value="-">Select Room</option>
                       <option value="555555555">Standard Room </option>
                       <option value="555555556">Executive Room</option>
                    </select>
                </div>

                <div class="three-columns">
                    <h6>From Inventory:</h6>
                    <select id="SelectInventory" class="full-width  select  expandable-list">
                                 <option value="DOTW">DOTW</option>
                                 <option value="HotelBeds">HotelBeds</option>
                                 <option value="MGH" selected="selected">MGH</option>
                    </select>
                </div>

                <div class="three-columns">
                   <h6>Offer Nationality:</h6>
                    <select id="select_OfferNationality" class="full-width select multiple-as-single easy-multiple-selection allow-empty check-list" multiple>
                      <option value="-">Select Country</option> <option value="AFGHANISTAN">AFGHANISTAN</option><option value="ALBANIA">ALBANIA</option><option value="ALGERIA">ALGERIA</option><option value="ANDORRA">ANDORRA</option><option value="ANGUILLA">ANGUILLA</option><option value="ARGENTINA">ARGENTINA</option><option value="AUSTRALIA">AUSTRALIA</option><option value="AUSTRIA">AUSTRIA</option><option value="BAHAMAS">BAHAMAS</option><option value="BAHRAIN">BAHRAIN</option><option value="BANGLADESH">BANGLADESH</option><option value="BARBADOS">BARBADOS</option><option value="BELARUS">BELARUS</option><option value="BELGIUM">BELGIUM</option><option value="BELIZE">BELIZE</option><option value="BENIN">BENIN</option><option value="BHUTAN">BHUTAN</option><option value="BOLIVIA">BOLIVIA</option><option value="BOSNIA" and="" herzegovina="">BOSNIA AND HERZEGOVINA</option><option value="BRAZIL">BRAZIL</option><option value="BULGARIA">BULGARIA</option><option value="CAMBODIA">CAMBODIA</option><option value="CANADA">CANADA</option><option value="CHILE">CHILE</option><option value="CHINA">CHINA</option><option value="COLOMBIA">COLOMBIA</option><option value="COSTA" rica="">COSTA RICA</option><option value="CROATIA">CROATIA</option><option value="CYPRUS">CYPRUS</option><option value="CZECH" republic="">CZECH REPUBLIC</option><option value="DENMARK">DENMARK</option><option value="ECUADOR">ECUADOR</option><option value="EGYPT">EGYPT</option><option value="ESTONIA">ESTONIA</option><option value="FIJI">FIJI</option><option value="FINLAND">FINLAND</option><option value="FRANCE">FRANCE</option><option value="GEORGIA">GEORGIA</option><option value="GERMANY">GERMANY</option><option value="GREECE">GREECE</option><option value="GREENLAND">GREENLAND</option><option value="HONG" kong="">HONG KONG</option><option value="INDIA">INDIA</option><option value="INDONESIA">INDONESIA</option><option value="IRELAND">IRELAND</option><option value="ISRAEL">ISRAEL</option><option value="ITALY">ITALY</option><option value="JAMAICA">JAMAICA</option><option value="JAPAN">JAPAN</option><option value="JORDAN">JORDAN</option><option value="KAZAKHSTAN">KAZAKHSTAN</option><option value="KENYA">KENYA</option><option value="KUWAIT">KUWAIT</option><option value="KYRGYZSTAN">KYRGYZSTAN</option><option value="LAOS">LAOS</option><option value="LATVIA">LATVIA</option><option value="LEBANON">LEBANON</option><option value="LIBERIA">LIBERIA</option><option value="LIBYA">LIBYA</option><option value="LIECHTENSTEIN">LIECHTENSTEIN</option><option value="LITHUANIA">LITHUANIA</option><option value="LUXEMBOURG">LUXEMBOURG</option><option value="MACAU">MACAU</option><option value="MACEDONIA">MACEDONIA</option><option value="MALAYSIA">MALAYSIA</option><option value="MALDIVES">MALDIVES</option><option value="MALTA">MALTA</option><option value="MARTINIQUE">MARTINIQUE</option><option value="MAURITANIA">MAURITANIA</option><option value="MAURITIUS">MAURITIUS</option><option value="MEXICO">MEXICO</option><option value="MICRONESIA">MICRONESIA</option><option value="MOLDOVA">MOLDOVA</option><option value="MONACO">MONACO</option><option value="MONGOLIA">MONGOLIA</option><option value="MONTENEGRO">MONTENEGRO</option><option value="MOROCCO">MOROCCO</option><option value="MOZAMBIQUE">MOZAMBIQUE</option><option value="MYANMAR">MYANMAR</option><option value="NAMIBIA">NAMIBIA</option><option value="NEPAL">NEPAL</option><option value="NETHERLANDS">NETHERLANDS</option><option value="NEW" zealand="">NEW ZEALAND</option><option value="NICARAGUA">NICARAGUA</option><option value="NIGER">NIGER</option><option value="NIGERIA">NIGERIA</option><option value="NORWAY">NORWAY</option><option value="OMAN">OMAN</option><option value="PAKISTAN">PAKISTAN</option><option value="PALAU">PALAU</option><option value="PANAMA">PANAMA</option><option value="PARAGUAY">PARAGUAY</option><option value="PERU">PERU</option><option value="PHILIPPINES">PHILIPPINES</option><option value="POLAND">POLAND</option><option value="PORTUGAL">PORTUGAL</option><option value="PUERTO" rico="">PUERTO RICO</option><option value="QATAR">QATAR</option><option value="ROMANIA">ROMANIA</option><option value="RUSSIA">RUSSIA</option><option value="RWANDA">RWANDA</option><option value="SAINT" barthÉlemy="">SAINT BARTHÉLEMY</option><option value="SAINT" kitts="" and="" nevis="">SAINT KITTS AND NEVIS</option><option value="SAMOA">SAMOA</option><option value="SAN" marino="">SAN MARINO</option><option value="SAUDI" arabia="">SAUDI ARABIA</option><option value="SENEGAL">SENEGAL</option><option value="SERBIA">SERBIA</option><option value="SEYCHELLES">SEYCHELLES</option><option value="SINGAPORE">SINGAPORE</option><option value="SLOVAKIA">SLOVAKIA</option><option value="SLOVENIA">SLOVENIA</option><option value="SOUTH" africa="">SOUTH AFRICA</option><option value="SOUTH" korea="">SOUTH KOREA</option><option value="SPAIN">SPAIN</option><option value="SRI" lanka="">SRI LANKA</option><option value="ST" lucia="">ST LUCIA</option><option value="SUDAN">SUDAN</option><option value="SWEDEN">SWEDEN</option><option value="SWITZERLAND">SWITZERLAND</option><option value="SYRIA">SYRIA</option><option value="TAIWAN">TAIWAN</option><option value="TAJIKISTAN">TAJIKISTAN</option><option value="TANZANIA">TANZANIA</option><option value="THAILAND">THAILAND</option><option value="TRINIDAD" and="" tobago="">TRINIDAD AND TOBAGO</option><option value="TUNISIA">TUNISIA</option><option value="TURKEY">TURKEY</option><option value="UGANDA">UGANDA</option><option value="UKRAINE">UKRAINE</option><option value="UNITED" arab="" emirates="">UNITED ARAB EMIRATES</option><option value="UNITED" kingdom="">UNITED KINGDOM</option><option value="UNITED" states="">UNITED STATES</option><option value="URUGUAY">URUGUAY</option><option value="UZBEKISTAN">UZBEKISTAN</option><option value="VANUATU">VANUATU</option><option value="VENEZUELA">VENEZUELA</option><option value="VIETNAM">VIETNAM</option><option value="VIRGIN" islands="" (usa)="">VIRGIN ISLANDS (USA)</option><option value="YEMEN">YEMEN</option><option value="ZAMBIA">ZAMBIA</option><option value="ZIMBABWE">ZIMBABWE</option>
                    </select>
                </div>

                <div class="three-columns">
                    <h6>Supplier</h6>
                    <select id="SelectSupplier" class="full-width  select  expandable-list">
                        <option value="1">HotelBeds</option>
                        <option value="2">MGH</option>
                        <option value="3">Dotw</option>
                        <option value="8">Expedia</option>
                        <option value="16">ClickUrTrip</option>
                        <option value="25" selected="">From Hotel</option>
                    </select>
                </div>



                <div class="new-row twelve-columns" style="margin-bottom: -2px;">
                    <h3 class="thin underline blue">Season Dates</h3>
                </div>


               <div class="line2"></div>
                    <div id="SeasonUI" style="width:100%">
                    </div>

                <div class="new-row twelve-columns" style="margin-bottom: -2px;">
                    <h3 class="thin underline blue">Block Dates</h3>
                </div>

                <div class="line2"></div>
                    <div id="SpecialUI" style="width:100%">
                    </div>
              
                  <div class="new-row twelve-columns" style="margin-bottom: -2px;">
                    <h3 class="thin underline blue">Block Days</h3>
                </div>
                <div class="line2"></div>
                          <div class="one-columns">
                              <input type="checkbox" id="Mon"> <label for="Mon" class="label">Mon</label>
                          </div>

                         <div class="one-columns">
                              <input type="checkbox" id="Tue"> <label for="Tue" class="label">Tue</label>
                          </div>
                         <div class="one-columns">
                              <input type="checkbox" id="Wed"> <label for="Wed" class="label">Wed</label>
                          </div>
                          <div class="one-columns">
                              <input type="checkbox" id="Thu"> <label for="Thu" class="label">Thu</label>
                          </div>
                         <div class="one-columns">
                              <input type="checkbox" id="Fri"> <label for="Fri" class="label">Fri</label>
                          </div>
                         <div class="one-columns">
                              <input type="checkbox" id="Sat"> <label for="Sat" class="label">Sat</label>
                          </div>
                         <div class="one-columns">
                              <input type="checkbox" id="Sun"> <label for="Sun" class="label">Sun</label>
                          </div>
                         <div class="new-row twelve-columns">
                              <label id="lbl_BlockDays" style="color:red;display:none">* You can not block all days</label>
                          </div>
                        
                 

                        

                <div class="new-row twelve-columns">
                    <button type="button" class="button glossy blue-gradient float-right"  id="btn" onclick="AddOffer()">Add Offer</button>
              
                </div>
                </div>
           

            <div class="columns" id="DivOfferList" style="min-height: 200px;display:none;">
                <div class="new-row twelve-columns" style="margin-bottom: -2px;">
                    <h3 class="thin underline blue">Season List</h3>
                </div>
                <div class="new-row twelve-columns" style="margin-bottom: -2px;">
                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="tbl_Hotel">
                        <thead>
                            <tr>
                                <th align="center" style="width: 5%">S.No</th>
                                <th align="center" style="width: 5%">Season Name</th>
                                <th align="center" style="width: 5%">Valid From</th>
                                <th align="center" style="width: 5%">Valid To</th>
                                <th align="center" style="width: 10%">Room Category</th>
                            </tr>
                        </thead>
                        <tbody id="HotelList">
                        </tbody>
                    </table>
                </div>


                <div class="new-row twelve-columns" style="display:none" none">

                    <div class="three-columns">
                        <span class="text-left">Check-in:</span>
                        <input class="form-control mySelectCalendar" type="text" id="CheckInDate" readonly="readonly" style="cursor: pointer" />
                        <label style="color: red; margin-top: 3px; display: none" id="lbl_datepicker4">
                            <b>* This field is required</b></label>
                    </div>
                    <div class="three-columns">
                        <span class="text-left">Check-out:</span>
                        <br />
                        <input class="form-control mySelectCalendar" type="text" id="CheckOutDate" readonly="readonly" style="cursor: pointer" />
                    </div>
                    <div class="two-columns">
                        <span class="text-left">Total Nights:</span>
                        <br />
                        <select id="Select_TotalNights" class="form-control">
                            <option selected="selected" value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                        </select>
                    </div>
                </div>
            </div>
     



      </div>   

                       
	</section>
	<!-- End main content -->



	<!-- JavaScript at the bottom for fast page loading -->
	<!-- Scripts -->
	<script src="js/libs/jquery-1.10.2.min.js"></script>
	<script src="js/setup.js"></script>

	<!-- Template functions -->
	<script src="js/developr.input.js"></script>
	<script src="js/developr.navigable.js"></script>
	<script src="js/developr.notify.js"></script>
	<script src="js/developr.scroll.js"></script>
	<script src="js/developr.tooltip.js"></script>
	<script src="js/developr.table.js"></script>
    <script src="js/developr.accordions.js"></script>
    <script src="js/developr.wizard.js"></script>

	<!-- Plugins -->
	<script src="js/libs/jquery.tablesorter.min.js"></script>
	<script src="js/libs/DataTables/jquery.dataTables.min.js"></script>

    <!-- glDatePicker -->
	<script src="js/libs/glDatePicker/glDatePicker.min.js?v=1"></script>


	<script>

	    // Call template init (optional, but faster if called manually)
	    $.template.init();

	    // Table sort - DataTables
	    var table = $('#sorting-advanced');
	    table.dataTable({
	        'aoColumnDefs': [
				{ 'bSortable': false, 'aTargets': [0, 5] }
	        ],
	        'sPaginationType': 'full_numbers',
	        'sDom': '<"dataTables_header"lfr>t<"dataTables_footer"ip>',
	        'fnInitComplete': function (oSettings) {
	            // Style length select
	            table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
	            tableStyled = true;
	        }
	    });

	    // Table sort - styled
	    $('#sorting-example1').tablesorter({
	        headers: {
	            0: { sorter: false },
	            5: { sorter: false }
	        }
	    }).on('click', 'tbody td', function (event) {
	        // Do not process if something else has been clicked
	        if (event.target !== this) {
	            return;
	        }

	        var tr = $(this).parent(),
				row = tr.next('.row-drop'),
				rows;

	        // If click on a special row
	        if (tr.hasClass('row-drop')) {
	            return;
	        }

	        // If there is already a special row
	        if (row.length > 0) {
	            // Un-style row
	            tr.children().removeClass('anthracite-gradient glossy');

	            // Remove row
	            row.remove();

	            return;
	        }

	        // Remove existing special rows
	        rows = tr.siblings('.row-drop');
	        if (rows.length > 0) {
	            // Un-style previous rows
	            rows.prev().children().removeClass('anthracite-gradient glossy');

	            // Remove rows
	            rows.remove();
	        }

	        // Style row
	        tr.children().addClass('anthracite-gradient glossy');

	        // Add fake row
	        $('<tr class="row-drop">' +
				'<td colspan="' + tr.children().length + '">' +
					'<div class="float-right">' +
						'<button type="submit" class="button glossy mid-margin-right">' +
							'<span class="button-icon"><span class="icon-mail"></span></span>' +
							'Send mail' +
						'</button>' +
						'<button type="submit" class="button glossy">' +
							'<span class="button-icon red-gradient"><span class="icon-cross"></span></span>' +
							'Remove' +
						'</button>' +
					'</div>' +
					'<strong>Name:</strong> John Doe<br>' +
					'<strong>Account:</strong> admin<br>' +
					'<strong>Last connect:</strong> 05-07-2011<br>' +
					'<strong>Email:</strong> john@doe.com' +
				'</td>' +
			'</tr>').insertAfter(tr);

	    }).on('sortStart', function () {
	        var rows = $(this).find('.row-drop');
	        if (rows.length > 0) {
	            // Un-style previous rows
	            rows.prev().children().removeClass('anthracite-gradient glossy');

	            // Remove rows
	            rows.remove();
	        }
	    });

	    // Table sort - simple
	    $('#sorting-example2').tablesorter({
	        headers: {
	            5: { sorter: false }
	        }
	    });

	</script>

    <script>

        $(document).ready(function () {
            // Elements
            var form = $('.wizard'),

				// If layout is centered
				centered;

            // Handle resizing (mostly for debugging)
            function handleWizardResize() {
                centerWizard(false);
            };

            // Register and first call
            $(window).on('normalized-resize', handleWizardResize);

            /*
			 * Center function
			 * @param boolean animate whether or not to animate the position change
			 * @return void
			 */
            function centerWizard(animate) {
                form[animate ? 'animate' : 'css']({ marginTop: Math.max(0, Math.round(($.template.viewportHeight - 30 - form.outerHeight()) / 2)) + 'px' });
            };

            // Initial vertical adjust
            centerWizard(false);

            // Refresh position on change step
            form.on('wizardchange', function () { centerWizard(true); });

            // Validation
            if ($.validationEngine) {
                form.validationEngine();
            }
        });

	</script>
</asp:Content>
