﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ViewOfferDetials.aspx.cs" Inherits="CutAdmin.ViewOfferDetials" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/ViewOfferDetials.js"></script>

     <!-- Additional styles -->
	<link rel="stylesheet" href="css/styles/form.css?v=1">
	<link rel="stylesheet" href="css/styles/switches.css?v=1">
	<link rel="stylesheet" href="css/styles/table.css?v=1">

	<!-- DataTables -->
	<link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

	<!-- Microsoft clear type rendering -->
	<meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

		<hgroup id="main-title" class="thin">
			<h3>Hotels List</h3><hr />
		</hgroup>
        
		<div class="with-padding">
             <div class="new-row twelve-columns">
                    <input id="htlname" style="background:none;border:none;color:#006699;font-size:medium;font-weight:bold;width:100%" readonly/>
                
                </div> <br />
		<table class="simple-table responsive-table full-width"  id="tbl_HotelList">
            
				<thead>
					<tr>
						 <th  scope="col" width="5%">S.No</th>
                         <th scope="col" width="15%">Room Category</th>
                         <th scope="col" width="15%">Season Name</th>
                         <th scope="col" width="10%">Valid From</th>
                         <th scope="col" width="10%">Valid To</th>
                         <th scope="col" width="10%">Meal Plan</th>
                         <th scope="col" width="10%">Book Before</th>
                         <th scope="col" width="10%">Offer</th>
                           <%-- <th scope="col" width="5%">Offer Type</th>--%>
                         <th scope="col" width="5%">Edit</th>
                         <th scope="col" width="5%">Active</th>
					</tr>
				</thead>
            <tbody id="offerlistId">
                    </tbody>
			</table>
	
		</div>

	</section>
	<!-- End main content -->



	<!-- JavaScript at the bottom for fast page loading -->
	<!-- Scripts -->
	<script src="js/libs/jquery-1.10.2.min.js"></script>
	<script src="js/setup.js"></script>
     <link rel="stylesheet" href="css/styles/modal.css?v=1">
    <script src="js/developr.modal.js"></script>

	<!-- Template functions -->
	<script src="js/developr.input.js"></script>
	<script src="js/developr.navigable.js"></script>
	<script src="js/developr.notify.js"></script>
	<script src="js/developr.scroll.js"></script>
	<script src="js/developr.tooltip.js"></script>
	<script src="js/developr.table.js"></script>

		<!-- Plugins -->
	<script src="js/libs/jquery.tablesorter.min.js"></script>
	<script src="js/libs/DataTables/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="css/styles/modal.css?v=1">
    <script src="js/developr.modal.js"></script>
	
      <script>
	    

	    // Call template init (optional, but faster if called manually)
	    $.template.init();

	    // Table sort - DataTables
	    var table = $('#sorting-advanced');
	    table.dataTable({
	        'aoColumnDefs': [
				{ 'bSortable': false, 'aTargets': [0, 5] }
	        ],
	        'sPaginationType': 'full_numbers',
	        'sDom': '<"dataTables_header"lfr>t<"dataTables_footer"ip>',
	        'fnInitComplete': function (oSettings) {
	            // Style length select
	            table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
	            tableStyled = true;
	        }
	    });

	    // Table sort - styled
	    $('#sorting-example1').tablesorter({
	        headers: {
	            0: { sorter: false },
	            5: { sorter: false }
	        }
	    }).on('click', 'tbody td', function (event) {
	        // Do not process if something else has been clicked
	        if (event.target !== this) {
	            return;
	        }

	        var tr = $(this).parent(),
				row = tr.next('.row-drop'),
				rows;

	        // If click on a special row
	        if (tr.hasClass('row-drop')) {
	            return;
	        }

	        // If there is already a special row
	        if (row.length > 0) {
	            // Un-style row
	            tr.children().removeClass('anthracite-gradient glossy');

	            // Remove row
	            row.remove();

	            return;
	        }

	        // Remove existing special rows
	        rows = tr.siblings('.row-drop');
	        if (rows.length > 0) {
	            // Un-style previous rows
	            rows.prev().children().removeClass('anthracite-gradient glossy');

	            // Remove rows
	            rows.remove();
	        }

	        // Style row
	        tr.children().addClass('anthracite-gradient glossy');

	        // Add fake row
	        $('<tr class="row-drop">' +
				'<td colspan="' + tr.children().length + '">' +
					'<div class="float-right">' +
						'<button type="submit" class="button glossy mid-margin-right">' +
							'<span class="button-icon"><span class="icon-mail"></span></span>' +
							'Send mail' +
						'</button>' +
						'<button type="submit" class="button glossy">' +
							'<span class="button-icon red-gradient"><span class="icon-cross"></span></span>' +
							'Remove' +
						'</button>' +
					'</div>' +
					'<strong>Name:</strong> John Doe<br>' +
					'<strong>Account:</strong> admin<br>' +
					'<strong>Last connect:</strong> 05-07-2011<br>' +
					'<strong>Email:</strong> john@doe.com' +
				'</td>' +
			'</tr>').insertAfter(tr);

	    }).on('sortStart', function () {
	        var rows = $(this).find('.row-drop');
	        if (rows.length > 0) {
	            // Un-style previous rows
	            rows.prev().children().removeClass('anthracite-gradient glossy');

	            // Remove rows
	            rows.remove();
	        }
	    });

	    // Table sort - simple
	    $('#sorting-example2').tablesorter({
	        headers: {
	            5: { sorter: false }
	        }
	    });

	</script>

</asp:Content>
