﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using CutAdmin;
using CutAdmin.DataLayer;
using CutAdmin.BL;

namespace CutAdmin
{
    public partial class VoucherPDF : System.Web.UI.Page
    {
        static DBHandlerDataContext db = new DBHandlerDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {

            string ReservationID = Request.QueryString["ReservationID"];
            string Uid = Request.QueryString["Uid"];
            string Status = Request.QueryString["Status"];
            StringBuilder sb = new StringBuilder();

            sb.Append(VoucherManager.GenrateVoucher(ReservationID, Uid, Status));

            HttpContext context = System.Web.HttpContext.Current;
            var htmlContent = String.Format(sb.ToString());
            NReco.PdfGenerator.HtmlToPdfConverter pdfConverter = new NReco.PdfGenerator.HtmlToPdfConverter();
            pdfConverter.Size = NReco.PdfGenerator.PageSize.A3;
            // pdfConverter.PdfToolPath = "E:\\ClickUrTrip\\CUT";
            //pdfConverter.PdfToolPath = "C:\\inetpub\\vhosts\\clickurtrip.com\\httpdocs\\Agent";                    
            // pdfConverter.PdfToolPath = "C:\\inetpub\\wwwroot\\httpdocs\\Agent";              \\for local
            pdfConverter.PdfToolPath = "D:\\inetpub\\wwwroot\\Agent";                          //for live
            var pdfBytes = pdfConverter.GeneratePdf(htmlContent);
            context.Response.ContentType = "application/pdf";
            context.Response.BinaryWrite(pdfBytes);

        }


        private static bool UrlExists(string url)
        {
            try
            {
                new System.Net.WebClient().DownloadData(url);
                return true;
            }
            catch (System.Net.WebException e)
            {
                if (((System.Net.HttpWebResponse)e.Response).StatusCode == System.Net.HttpStatusCode.NotFound)
                    return false;
                else
                    throw;
            }
        }
    }
}