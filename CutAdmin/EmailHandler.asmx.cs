﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for EmailHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class EmailHandler : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public string SendInvoice(string sEmail, string ReservationId, string UID, string Night)
        {
            string sJsonString = "{\"retCode\":\"0\"}";
            DBHelper.DBReturnCode retCode = EmailManager.SendInvoice(sEmail, ReservationId, UID, Night);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                //if (CUT.DataLayer.EmailManager.SendInvoice(sEmail, ReservationId, UID) == true)//info@tmauto.in

                sJsonString = "{\"retCode\":\"1\",}";
            }
            return sJsonString;
        }

        [WebMethod(EnableSession = true)]
        public string SendCommissionInvoice(string sEmail, string ReservationId, string UID, string Night)
        {
            string sJsonString = "{\"retCode\":\"0\"}";
            DBHelper.DBReturnCode retCode = EmailManager.SendCommissionInvoice(sEmail, ReservationId, UID, Night);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                //if (CUT.DataLayer.EmailManager.SendInvoice(sEmail, ReservationId, UID) == true)//info@tmauto.in

                sJsonString = "{\"retCode\":\"1\",}";
            }
            return sJsonString;
        }

        [WebMethod(EnableSession = true)]
        public string SendVoucher(string sEmail, string ReservationId, string UID, string Latitude, string Longitude, string Night, string Supp, string Status)
        {
            string sJsonString = "{\"retCode\":\"0\"}";
            DBHelper.DBReturnCode retCode = EmailManager.SendVoucher(sEmail, ReservationId, UID, Latitude, Longitude, Night, Supp, Status);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                //if (CUT.DataLayer.EmailManager.SendInvoice(sEmail, ReservationId, UID) == true)//info@tmauto.in

                sJsonString = "{\"retCode\":\"1\",}";
            }
            return sJsonString;
        }

        //[WebMethod(EnableSession = true)]
        //public string SendHotelDetails(string sTo, string sCountry, string sCity, string sURLHotelImage, string sURLStarRating, string sTotalPrice, string sCurrency, string sDescription, string sCheckIn, string sCheckOut, string sAddress, string sHotelName)
        //{
        //    sURLStarRating = "https://ci3.googleusercontent.com/proxy/VMLYnQ9HZxL-IfNhcw_Sh11g3Lfd1L1V7QjOQGoHygWynzLwarYgRProIVBchHi4V8cFsMDpspj1JuGx2g=s0-d-e1-ft#http://ibe.ezeeibe.com/images/2star.png";
        //    sURLHotelImage = "https://ci4.googleusercontent.com/proxy/SIIbEdcM4fB4eUdkWiPGUCrtkTguDub1BGusQ4tNaa_R3Q86O1RQBqiA9_0rSN334lavP578rz_0XQ0c_eyW4MffB9RqhNjX8NnNAb-vYuZ3yru36whleqEsINJWlA64X2eRkGYzXpBLVopoRv_jOqwmpA=s0-d-e1-ft#http://us.dotwconnect.com/poze_hotel/10/1032818/RoMLa1Ct_f2234aaf0dce061716144dad7566ef88.jpg";
            //if (EmailManager.SendHotelDetails(sTo, sCountry, sCity, sURLHotelImage, sURLStarRating, sTotalPrice, sCurrency, sDescription, sCheckIn, sCheckOut, sAddress, sHotelName))
            //    return "1";
            //else
            //    return "0";
        }
    }

