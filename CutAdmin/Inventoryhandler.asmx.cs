﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Globalization;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for Inventoryhandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. e




    [System.Web.Script.Services.ScriptService]
    public class Inventoryhandler : System.Web.Services.WebService
    {
        public class Inventory
        {
            public Int64 ID { get; set; }
            public string Name { get; set; }
            public List<string> Date { get; set; }
        }
        //DBHandlerDataContext DB = new DBHandlerDataContext();
        string jsonString = "";
        JavaScriptSerializer objserialize = new JavaScriptSerializer();
        
        public static List<DatesInv> ListDatesNew { get; set; }
        [WebMethod(EnableSession = true)]
        public string SaveInventory(string HotelCode, Int64[] Room, string MaxRoom, string DtTill, string[] DateInvFr, string[] DateInvTo, string pRateType, string OptRoomperDate, string InvLiveOrReq, string InType)
        {
            using (var DB = new DBHandlerDataContext())
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                string json = "";
                // var DataList = (from obj in DB.tbl_CommonHotelInventories where obj.HotelCode == HotelCode && obj.RoomType ==Convert.ToString(Room) select obj).ToList();
                for (int r = 0; r < Room.Length; r++)
                {
                    string InvSoldType = InvLiveOrReq;
                    if (InvSoldType == "Live")
                        InvSoldType = "L";
                    else if (InvSoldType == "Request")
                        InvSoldType = "R";
                    else
                        InvSoldType = "";

                    string NoOfSold = "0";
                    string[] RateType = pRateType.Split('^');
                    List<ListDates> ListAddDates = new List<ListDates>();
                    List<ListNewDates> ListNDates = new List<ListNewDates>();
                    List<DateTime> ListGDates = new List<DateTime>();

                    for (int i = 0; i < DateInvFr.Length; i++)
                    {
                        DateTime FromDt = DateTime.ParseExact(DateInvFr[i], "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        DateTime ToDt = DateTime.ParseExact(DateInvTo[i], "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                        double noGDays = (ToDt - FromDt).TotalDays;

                        for (int j = 0; j <= noGDays; j++)
                        {
                            ListGDates.Add(FromDt.AddDays(j));

                        }
                    }
                    string mon = "";
                    for (int l = 0; l < ListGDates.Count; l++)
                    {

                        mon += ListGDates[l].Month.ToString() + '^';

                    }

                    string[] mothn = mon.Split('^');
                    string dat = "";
                    var distinctArrayn = mothn.Distinct().ToArray();
                    for (int j = 0; j < distinctArrayn.Length - 1; j++)
                    {
                        List<DateTime> ListAdDates = new List<DateTime>();
                        for (int d = 0; d < ListGDates.Count; d++)
                        {
                            var mn = ListGDates[d].Month.ToString();
                            if (distinctArrayn[j] == mn)
                                ListAdDates.Add(ListGDates[d].Date);
                        }

                        ListNDates.Add(new ListNewDates
                        {
                            List_Newdates = ListAdDates,
                        });

                    }



                    //ListAddDates.Add(new ListDates
                    //{
                    //    List_dates = ListGDates,
                    //});
                    // }

                    for (int i = 0; i < RateType.Length - 1; i++)
                    {
                        List<string> ListAdds = new List<string>();
                        for (int k = 0; k < ListNDates.Count; k++)
                        {
                            string months = "";
                            string month = "";
                            for (int l = 0; l < ListNDates[k].List_Newdates.Count; l++)
                            {

                                month += ListNDates[k].List_Newdates[l].Month.ToString() + '^';

                            }
                            string[] moth = month.Split('^');
                            var distinctArray = moth.Distinct().ToArray();

                            string years = "";
                            string year = "";
                            for (int m = 0; m < ListNDates[k].List_Newdates.Count; m++)
                            {
                                year += ListNDates[k].List_Newdates[m].Year.ToString() + '^';

                            }
                            string[] yr = year.Split('^');
                            var distinctArrayYr = yr.Distinct().ToArray();

                            for (int j = 0; j < distinctArray.Length - 1; j++)
                            {
                                int length = distinctArray[j].ToString().Length;
                                if (length == 1)
                                {
                                    distinctArray[j] = "0" + distinctArray[j];
                                }

                                var DatList = (from obj in DB.tbl_CommonHotelInventories where obj.HotelCode == HotelCode && obj.RoomType == Convert.ToString(Room[r]) && obj.Year == distinctArrayYr[j] && obj.Month == distinctArray[j] && obj.RateType == RateType[i] select obj).ToList();

                                if (DatList.Count == 0)
                                {

                                    tbl_CommonHotelInventory Add = new tbl_CommonHotelInventory();
                                    Add.HotelCode = HotelCode;
                                    Add.SupplierId = AccountManager.GetSupplierByUser().ToString();
                                    Add.RoomType = Room[r].ToString();
                                    Add.RateType = RateType[i];
                                    Add.Month = distinctArray[j];
                                    Add.InventoryType = "FreeSale";
                                    Add.Year = distinctArrayYr[j];
                                    Add.FreeSaleTill = DtTill;
                                    for (int d = 0; d < ListNDates[k].List_Newdates.Count; d++)
                                    {
                                        var mont = ListNDates[k].List_Newdates[d].Month.ToString();
                                        if (InType == "fs")
                                        {
                                            switch (ListNDates[k].List_Newdates[d].Day.ToString())
                                            {

                                                case "1":
                                                    Add.Date_1 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "2":
                                                    Add.Date_2 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "3":
                                                    Add.Date_3 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "4":
                                                    Add.Date_4 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "5":
                                                    Add.Date_5 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "6":
                                                    Add.Date_6 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "7":
                                                    Add.Date_7 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "8":
                                                    Add.Date_8 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "9":
                                                    Add.Date_9 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "10":
                                                    Add.Date_10 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "11":
                                                    Add.Date_11 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "12":
                                                    Add.Date_12 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "13":
                                                    Add.Date_13 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "14":
                                                    Add.Date_14 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "15":
                                                    Add.Date_15 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "16":
                                                    Add.Date_16 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "17":
                                                    Add.Date_17 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "18":
                                                    Add.Date_18 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "19":
                                                    Add.Date_19 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "20":
                                                    Add.Date_20 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "21":
                                                    Add.Date_21 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "22":
                                                    Add.Date_22 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "23":
                                                    Add.Date_23 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "24":
                                                    Add.Date_24 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "25":
                                                    Add.Date_25 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "26":
                                                    Add.Date_26 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "27":
                                                    Add.Date_27 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "28":
                                                    Add.Date_28 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "29":
                                                    Add.Date_29 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "30":
                                                    Add.Date_30 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "31":
                                                    Add.Date_31 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                            }

                                            switch (ListNDates[k].List_Newdates[d].Day.ToString())
                                            {

                                                case "1":
                                                    Add.Option_Date_1 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "2":
                                                    Add.Option_Date_2 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "3":
                                                    Add.Option_Date_3 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "4":
                                                    Add.Option_Date_4 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "5":
                                                    Add.Option_Date_5 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "6":
                                                    Add.Option_Date_6 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "7":
                                                    Add.Option_Date_7 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "8":
                                                    Add.Option_Date_8 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "9":
                                                    Add.Option_Date_9 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "10":
                                                    Add.Option_Date_10 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "11":
                                                    Add.Option_Date_11 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "12":
                                                    Add.Option_Date_12 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "13":
                                                    Add.Option_Date_13 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "14":
                                                    Add.Option_Date_14 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "15":
                                                    Add.Option_Date_15 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "16":
                                                    Add.Option_Date_16 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "17":
                                                    Add.Option_Date_17 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "18":
                                                    Add.Option_Date_18 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "19":
                                                    Add.Option_Date_19 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "20":
                                                    Add.Option_Date_20 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "21":
                                                    Add.Option_Date_21 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "22":
                                                    Add.Option_Date_22 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "23":
                                                    Add.Option_Date_23 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "24":
                                                    Add.Option_Date_24 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "25":
                                                    Add.Option_Date_25 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "26":
                                                    Add.Option_Date_26 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "27":
                                                    Add.Option_Date_27 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "28":
                                                    Add.Option_Date_28 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "29":
                                                    Add.Option_Date_29 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "30":
                                                    Add.Option_Date_30 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "31":
                                                    Add.Option_Date_31 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                            }
                                        }
                                        else
                                        {
                                            switch (ListNDates[k].List_Newdates[d].Day.ToString())
                                            {

                                                case "1":
                                                    Add.Date_1 = InType;
                                                    break;
                                                case "2":
                                                    Add.Date_2 = InType;
                                                    break;
                                                case "3":
                                                    Add.Date_3 = InType;
                                                    break;
                                                case "4":
                                                    Add.Date_4 = InType;
                                                    break;
                                                case "5":
                                                    Add.Date_5 = InType;
                                                    break;
                                                case "6":
                                                    Add.Date_6 = InType;
                                                    break;
                                                case "7":
                                                    Add.Date_7 = InType;
                                                    break;
                                                case "8":
                                                    Add.Date_8 = InType;
                                                    break;
                                                case "9":
                                                    Add.Date_9 = InType;
                                                    break;
                                                case "10":
                                                    Add.Date_10 = InType;
                                                    break;
                                                case "11":
                                                    Add.Date_11 = InType;
                                                    break;
                                                case "12":
                                                    Add.Date_12 = InType;
                                                    break;
                                                case "13":
                                                    Add.Date_13 = InType;
                                                    break;
                                                case "14":
                                                    Add.Date_14 = InType;
                                                    break;
                                                case "15":
                                                    Add.Date_15 = InType;
                                                    break;
                                                case "16":
                                                    Add.Date_16 = InType;
                                                    break;
                                                case "17":
                                                    Add.Date_17 = InType;
                                                    break;
                                                case "18":
                                                    Add.Date_18 = InType;
                                                    break;
                                                case "19":
                                                    Add.Date_19 = InType;
                                                    break;
                                                case "20":
                                                    Add.Date_20 = InType;
                                                    break;
                                                case "21":
                                                    Add.Date_21 = InType;
                                                    break;
                                                case "22":
                                                    Add.Date_22 = InType;
                                                    break;
                                                case "23":
                                                    Add.Date_23 = InType;
                                                    break;
                                                case "24":
                                                    Add.Date_24 = InType;
                                                    break;
                                                case "25":
                                                    Add.Date_25 = InType;
                                                    break;
                                                case "26":
                                                    Add.Date_26 = InType;
                                                    break;
                                                case "27":
                                                    Add.Date_27 = InType;
                                                    break;
                                                case "28":
                                                    Add.Date_28 = InType;
                                                    break;
                                                case "29":
                                                    Add.Date_29 = InType;
                                                    break;
                                                case "30":
                                                    Add.Date_30 = InType;
                                                    break;
                                                case "31":
                                                    Add.Date_31 = InType;
                                                    break;
                                            }

                                            switch (ListNDates[k].List_Newdates[d].Day.ToString())
                                            {

                                                case "1":
                                                    Add.Option_Date_1 = "-";
                                                    break;
                                                case "2":
                                                    Add.Option_Date_2 = "-";
                                                    break;
                                                case "3":
                                                    Add.Option_Date_3 = "-";
                                                    break;
                                                case "4":
                                                    Add.Option_Date_4 = "-";
                                                    break;
                                                case "5":
                                                    Add.Option_Date_5 = "-";
                                                    break;
                                                case "6":
                                                    Add.Option_Date_6 = "-";
                                                    break;
                                                case "7":
                                                    Add.Option_Date_7 = "-";
                                                    break;
                                                case "8":
                                                    Add.Option_Date_8 = "-";
                                                    break;
                                                case "9":
                                                    Add.Option_Date_9 = "-";
                                                    break;
                                                case "10":
                                                    Add.Option_Date_10 = "-";
                                                    break;
                                                case "11":
                                                    Add.Option_Date_11 = "-";
                                                    break;
                                                case "12":
                                                    Add.Option_Date_12 = "-";
                                                    break;
                                                case "13":
                                                    Add.Option_Date_13 = "-";
                                                    break;
                                                case "14":
                                                    Add.Option_Date_14 = "-";
                                                    break;
                                                case "15":
                                                    Add.Option_Date_15 = "-";
                                                    break;
                                                case "16":
                                                    Add.Option_Date_16 = "-";
                                                    break;
                                                case "17":
                                                    Add.Option_Date_17 = "-";
                                                    break;
                                                case "18":
                                                    Add.Option_Date_18 = "-";
                                                    break;
                                                case "19":
                                                    Add.Option_Date_19 = "-";
                                                    break;
                                                case "20":
                                                    Add.Option_Date_20 = "-";
                                                    break;
                                                case "21":
                                                    Add.Option_Date_21 = "-";
                                                    break;
                                                case "22":
                                                    Add.Option_Date_22 = "-";
                                                    break;
                                                case "23":
                                                    Add.Option_Date_23 = "-";
                                                    break;
                                                case "24":
                                                    Add.Option_Date_24 = "-";
                                                    break;
                                                case "25":
                                                    Add.Option_Date_25 = "-";
                                                    break;
                                                case "26":
                                                    Add.Option_Date_26 = "-";
                                                    break;
                                                case "27":
                                                    Add.Option_Date_27 = "-";
                                                    break;
                                                case "28":
                                                    Add.Option_Date_28 = "-";
                                                    break;
                                                case "29":
                                                    Add.Option_Date_29 = "-";
                                                    break;
                                                case "30":
                                                    Add.Option_Date_30 = "-";
                                                    break;
                                                case "31":
                                                    Add.Option_Date_31 = "-";
                                                    break;
                                            }
                                        }

                                    }
                                    DB.tbl_CommonHotelInventories.InsertOnSubmit(Add);
                                    DB.SubmitChanges();
                                }
                                else
                                {

                                    tbl_CommonHotelInventory Update = DB.tbl_CommonHotelInventories.Single(x => x.Sid == DatList[0].Sid);
                                    Update.HotelCode = HotelCode;
                                    Update.SupplierId = AccountManager.GetSupplierByUser().ToString();
                                    Update.RoomType = Room[r].ToString();
                                    Update.RateType = RateType[i];
                                    Update.Month = distinctArray[j];
                                    Update.InventoryType = "FreeSale";
                                    Update.Year = distinctArrayYr[j];
                                    Update.FreeSaleTill = DtTill;
                                    for (int d = 0; d < ListNDates[k].List_Newdates.Count; d++)
                                    {
                                        var mont = ListNDates[k].List_Newdates[d].Month.ToString();
                                        if (InType == "fs")
                                        {
                                            switch (ListNDates[k].List_Newdates[d].Day.ToString())
                                            {

                                                case "1":
                                                    Update.Date_1 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "2":
                                                    Update.Date_2 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "3":
                                                    Update.Date_3 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "4":
                                                    Update.Date_4 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "5":
                                                    Update.Date_5 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "6":
                                                    Update.Date_6 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "7":
                                                    Update.Date_7 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "8":
                                                    Update.Date_8 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "9":
                                                    Update.Date_9 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "10":
                                                    Update.Date_10 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "11":
                                                    Update.Date_11 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "12":
                                                    Update.Date_12 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "13":
                                                    Update.Date_13 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "14":
                                                    Update.Date_14 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "15":
                                                    Update.Date_15 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "16":
                                                    Update.Date_16 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "17":
                                                    Update.Date_17 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "18":
                                                    Update.Date_18 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "19":
                                                    Update.Date_19 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "20":
                                                    Update.Date_20 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "21":
                                                    Update.Date_21 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "22":
                                                    Update.Date_22 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "23":
                                                    Update.Date_23 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "24":
                                                    Update.Date_24 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "25":
                                                    Update.Date_25 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "26":
                                                    Update.Date_26 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "27":
                                                    Update.Date_27 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "28":
                                                    Update.Date_28 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "29":
                                                    Update.Date_29 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "30":
                                                    Update.Date_30 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                                case "31":
                                                    Update.Date_31 = InType + "_" + MaxRoom + "_" + NoOfSold;
                                                    break;
                                            }

                                            switch (ListNDates[k].List_Newdates[d].Day.ToString())
                                            {

                                                case "1":
                                                    Update.Option_Date_1 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "2":
                                                    Update.Option_Date_2 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "3":
                                                    Update.Option_Date_3 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "4":
                                                    Update.Option_Date_4 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "5":
                                                    Update.Option_Date_5 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "6":
                                                    Update.Option_Date_6 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "7":
                                                    Update.Option_Date_7 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "8":
                                                    Update.Option_Date_8 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "9":
                                                    Update.Option_Date_9 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "10":
                                                    Update.Option_Date_10 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "11":
                                                    Update.Option_Date_11 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "12":
                                                    Update.Option_Date_12 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "13":
                                                    Update.Option_Date_13 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "14":
                                                    Update.Option_Date_14 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "15":
                                                    Update.Option_Date_15 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "16":
                                                    Update.Option_Date_16 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "17":
                                                    Update.Option_Date_17 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "18":
                                                    Update.Option_Date_18 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "19":
                                                    Update.Option_Date_19 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "20":
                                                    Update.Option_Date_20 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "21":
                                                    Update.Option_Date_21 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "22":
                                                    Update.Option_Date_22 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "23":
                                                    Update.Option_Date_23 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "24":
                                                    Update.Option_Date_24 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "25":
                                                    Update.Option_Date_25 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "26":
                                                    Update.Option_Date_26 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "27":
                                                    Update.Option_Date_27 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "28":
                                                    Update.Option_Date_28 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "29":
                                                    Update.Option_Date_29 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "30":
                                                    Update.Option_Date_30 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                                case "31":
                                                    Update.Option_Date_31 = OptRoomperDate + "_" + DtTill + "_" + InvSoldType;
                                                    break;
                                            }
                                        }
                                        else
                                        {
                                            switch (ListNDates[k].List_Newdates[d].Day.ToString())
                                            {

                                                case "1":
                                                    Update.Date_1 = InType;
                                                    break;
                                                case "2":
                                                    Update.Date_2 = InType;
                                                    break;
                                                case "3":
                                                    Update.Date_3 = InType;
                                                    break;
                                                case "4":
                                                    Update.Date_4 = InType;
                                                    break;
                                                case "5":
                                                    Update.Date_5 = InType;
                                                    break;
                                                case "6":
                                                    Update.Date_6 = InType;
                                                    break;
                                                case "7":
                                                    Update.Date_7 = InType;
                                                    break;
                                                case "8":
                                                    Update.Date_8 = InType;
                                                    break;
                                                case "9":
                                                    Update.Date_9 = InType;
                                                    break;
                                                case "10":
                                                    Update.Date_10 = InType;
                                                    break;
                                                case "11":
                                                    Update.Date_11 = InType;
                                                    break;
                                                case "12":
                                                    Update.Date_12 = InType;
                                                    break;
                                                case "13":
                                                    Update.Date_13 = InType;
                                                    break;
                                                case "14":
                                                    Update.Date_14 = InType;
                                                    break;
                                                case "15":
                                                    Update.Date_15 = InType;
                                                    break;
                                                case "16":
                                                    Update.Date_16 = InType;
                                                    break;
                                                case "17":
                                                    Update.Date_17 = InType;
                                                    break;
                                                case "18":
                                                    Update.Date_18 = InType;
                                                    break;
                                                case "19":
                                                    Update.Date_19 = InType;
                                                    break;
                                                case "20":
                                                    Update.Date_20 = InType;
                                                    break;
                                                case "21":
                                                    Update.Date_21 = InType;
                                                    break;
                                                case "22":
                                                    Update.Date_22 = InType;
                                                    break;
                                                case "23":
                                                    Update.Date_23 = InType;
                                                    break;
                                                case "24":
                                                    Update.Date_24 = InType;
                                                    break;
                                                case "25":
                                                    Update.Date_25 = InType;
                                                    break;
                                                case "26":
                                                    Update.Date_26 = InType;
                                                    break;
                                                case "27":
                                                    Update.Date_27 = InType;
                                                    break;
                                                case "28":
                                                    Update.Date_28 = InType;
                                                    break;
                                                case "29":
                                                    Update.Date_29 = InType;
                                                    break;
                                                case "30":
                                                    Update.Date_30 = InType;
                                                    break;
                                                case "31":
                                                    Update.Date_31 = InType;
                                                    break;
                                            }

                                            switch (ListNDates[k].List_Newdates[d].Day.ToString())
                                            {

                                                case "1":
                                                    Update.Option_Date_1 = "-";
                                                    break;
                                                case "2":
                                                    Update.Option_Date_2 = "-";
                                                    break;
                                                case "3":
                                                    Update.Option_Date_3 = "-";
                                                    break;
                                                case "4":
                                                    Update.Option_Date_4 = "-";
                                                    break;
                                                case "5":
                                                    Update.Option_Date_5 = "-";
                                                    break;
                                                case "6":
                                                    Update.Option_Date_6 = "-";
                                                    break;
                                                case "7":
                                                    Update.Option_Date_7 = "-";
                                                    break;
                                                case "8":
                                                    Update.Option_Date_8 = "-";
                                                    break;
                                                case "9":
                                                    Update.Option_Date_9 = "-";
                                                    break;
                                                case "10":
                                                    Update.Option_Date_10 = "-";
                                                    break;
                                                case "11":
                                                    Update.Option_Date_11 = "-";
                                                    break;
                                                case "12":
                                                    Update.Option_Date_12 = "-";
                                                    break;
                                                case "13":
                                                    Update.Option_Date_13 = "-";
                                                    break;
                                                case "14":
                                                    Update.Option_Date_14 = "-";
                                                    break;
                                                case "15":
                                                    Update.Option_Date_15 = "-";
                                                    break;
                                                case "16":
                                                    Update.Option_Date_16 = "-";
                                                    break;
                                                case "17":
                                                    Update.Option_Date_17 = "-";
                                                    break;
                                                case "18":
                                                    Update.Option_Date_18 = "-";
                                                    break;
                                                case "19":
                                                    Update.Option_Date_19 = "-";
                                                    break;
                                                case "20":
                                                    Update.Option_Date_20 = "-";
                                                    break;
                                                case "21":
                                                    Update.Option_Date_21 = "-";
                                                    break;
                                                case "22":
                                                    Update.Option_Date_22 = "-";
                                                    break;
                                                case "23":
                                                    Update.Option_Date_23 = "-";
                                                    break;
                                                case "24":
                                                    Update.Option_Date_24 = "-";
                                                    break;
                                                case "25":
                                                    Update.Option_Date_25 = "-";
                                                    break;
                                                case "26":
                                                    Update.Option_Date_26 = "-";
                                                    break;
                                                case "27":
                                                    Update.Option_Date_27 = "-";
                                                    break;
                                                case "28":
                                                    Update.Option_Date_28 = "-";
                                                    break;
                                                case "29":
                                                    Update.Option_Date_29 = "-";
                                                    break;
                                                case "30":
                                                    Update.Option_Date_30 = "-";
                                                    break;
                                                case "31":
                                                    Update.Option_Date_31 = "-";
                                                    break;
                                            }
                                        }
                                    }


                                }
                                DB.SubmitChanges();
                            }

                        }

                        json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                    }
                }
                return json;
            }
        }

        [WebMethod(EnableSession = true)]
        public string SaveAllocation(string HotelCode, Int64[] Room, string NoOfRoom, string[] DateInvFr, string[] DateInvTo, string pRateType, string InvLiveOrReq, string InType, string DaysPrior)
        {
            using (var DB = new DBHandlerDataContext())
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                string json = "";
                // var DataList = (from obj in DB.tbl_CommonHotelInventories where obj.HotelCode == HotelCode && obj.RoomType ==Convert.ToString(Room) select obj).ToList();
                for (int r = 0; r < Room.Length; r++)
                {
                    string InvSoldType = InvLiveOrReq;
                    if (InvSoldType == "Live")
                        InvSoldType = "L";
                    else if (InvSoldType == "Request")
                        InvSoldType = "R";
                    else
                        InvSoldType = "";

                    string NoOfSold = "0";
                    string[] RateType = pRateType.Split('^');
                    List<ListDates> ListAddDates = new List<ListDates>();
                    List<ListNewDates> ListNDates = new List<ListNewDates>();
                    List<DateTime> ListGDates = new List<DateTime>();

                    for (int i = 0; i < DateInvFr.Length; i++)
                    {
                        DateTime FromDt = DateTime.ParseExact(DateInvFr[i], "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        DateTime ToDt = DateTime.ParseExact(DateInvTo[i], "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                        double noGDays = (ToDt - FromDt).TotalDays;

                        for (int j = 0; j <= noGDays; j++)
                        {
                            ListGDates.Add(FromDt.AddDays(j));

                        }
                    }
                    string mon = "";
                    for (int l = 0; l < ListGDates.Count; l++)
                    {

                        mon += ListGDates[l].Month.ToString() + '^';

                    }

                    string[] mothn = mon.Split('^');
                    string dat = "";
                    var distinctArrayn = mothn.Distinct().ToArray();
                    for (int j = 0; j < distinctArrayn.Length - 1; j++)
                    {
                        List<DateTime> ListAdDates = new List<DateTime>();
                        for (int d = 0; d < ListGDates.Count; d++)
                        {
                            var mn = ListGDates[d].Month.ToString();
                            if (distinctArrayn[j] == mn)
                                ListAdDates.Add(ListGDates[d].Date);
                        }

                        ListNDates.Add(new ListNewDates
                        {
                            List_Newdates = ListAdDates,
                        });

                    }



                    //ListAddDates.Add(new ListDates
                    //{
                    //    List_dates = ListGDates,
                    //});
                    // }

                    for (int i = 0; i < RateType.Length - 1; i++)
                    {
                        List<string> ListAdds = new List<string>();
                        for (int k = 0; k < ListNDates.Count; k++)
                        {
                            string months = "";
                            string month = "";
                            for (int l = 0; l < ListNDates[k].List_Newdates.Count; l++)
                            {

                                month += ListNDates[k].List_Newdates[l].Month.ToString() + '^';

                            }
                            string[] moth = month.Split('^');
                            var distinctArray = moth.Distinct().ToArray();

                            string years = "";
                            string year = "";
                            for (int m = 0; m < ListNDates[k].List_Newdates.Count; m++)
                            {
                                year += ListNDates[k].List_Newdates[m].Year.ToString() + '^';

                            }
                            string[] yr = year.Split('^');
                            var distinctArrayYr = yr.Distinct().ToArray();

                            for (int j = 0; j < distinctArray.Length - 1; j++)
                            {
                                int length = distinctArray[j].ToString().Length;
                                if (length == 1)
                                {
                                    distinctArray[j] = "0" + distinctArray[j];
                                }

                                var DatList = (from obj in DB.tbl_CommonHotelInventories where obj.HotelCode == HotelCode && obj.RoomType == Convert.ToString(Room) && obj.Year == distinctArrayYr[j] && obj.Month == distinctArray[j] && obj.RateType == RateType[i] && obj.InventoryType == InType select obj).ToList();
                                if (DatList.Count == 0)
                                {

                                    tbl_CommonHotelInventory Add = new tbl_CommonHotelInventory();
                                    Add.HotelCode = HotelCode;
                                    Add.SupplierId = AccountManager.GetSupplierByUser().ToString();
                                    Add.RoomType = Room[r].ToString();
                                    Add.RateType = RateType[i];
                                    Add.Month = distinctArray[j];
                                    Add.InventoryType = InType;
                                    Add.Year = distinctArrayYr[j];

                                    for (int d = 0; d < ListNDates[k].List_Newdates.Count; d++)
                                    {
                                        var mont = ListNDates[k].List_Newdates[d].Month.ToString();

                                        switch (ListNDates[k].List_Newdates[d].Day.ToString())
                                        {

                                            case "1":
                                                Add.Date_1 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "2":
                                                Add.Date_2 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "3":
                                                Add.Date_3 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "4":
                                                Add.Date_4 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "5":
                                                Add.Date_5 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "6":
                                                Add.Date_6 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "7":
                                                Add.Date_7 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "8":
                                                Add.Date_8 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "9":
                                                Add.Date_9 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "10":
                                                Add.Date_10 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "11":
                                                Add.Date_11 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "12":
                                                Add.Date_12 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "13":
                                                Add.Date_13 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "14":
                                                Add.Date_14 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "15":
                                                Add.Date_15 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "16":
                                                Add.Date_16 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "17":
                                                Add.Date_17 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "18":
                                                Add.Date_18 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "19":
                                                Add.Date_19 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "20":
                                                Add.Date_20 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "21":
                                                Add.Date_21 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "22":
                                                Add.Date_22 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "23":
                                                Add.Date_23 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "24":
                                                Add.Date_24 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "25":
                                                Add.Date_25 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "26":
                                                Add.Date_26 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "27":
                                                Add.Date_27 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "28":
                                                Add.Date_28 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "29":
                                                Add.Date_29 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "30":
                                                Add.Date_30 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "31":
                                                Add.Date_31 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                        }

                                        switch (ListNDates[k].List_Newdates[d].Day.ToString())
                                        {

                                            case "1":
                                                Add.Option_Date_1 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "2":
                                                Add.Option_Date_2 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "3":
                                                Add.Option_Date_3 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "4":
                                                Add.Option_Date_4 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "5":
                                                Add.Option_Date_5 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "6":
                                                Add.Option_Date_6 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "7":
                                                Add.Option_Date_7 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "8":
                                                Add.Option_Date_8 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "9":
                                                Add.Option_Date_9 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "10":
                                                Add.Option_Date_10 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "11":
                                                Add.Option_Date_11 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "12":
                                                Add.Option_Date_12 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "13":
                                                Add.Option_Date_13 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "14":
                                                Add.Option_Date_14 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "15":
                                                Add.Option_Date_15 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "16":
                                                Add.Option_Date_16 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "17":
                                                Add.Option_Date_17 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "18":
                                                Add.Option_Date_18 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "19":
                                                Add.Option_Date_19 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "20":
                                                Add.Option_Date_20 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "21":
                                                Add.Option_Date_21 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "22":
                                                Add.Option_Date_22 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "23":
                                                Add.Option_Date_23 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "24":
                                                Add.Option_Date_24 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "25":
                                                Add.Option_Date_25 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "26":
                                                Add.Option_Date_26 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "27":
                                                Add.Option_Date_27 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "28":
                                                Add.Option_Date_28 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "29":
                                                Add.Option_Date_29 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "30":
                                                Add.Option_Date_30 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "31":
                                                Add.Option_Date_31 = DaysPrior + "_" + InvSoldType;
                                                break;
                                        }

                                    }
                                    DB.tbl_CommonHotelInventories.InsertOnSubmit(Add);
                                    DB.SubmitChanges();
                                }
                                else
                                {
                                    tbl_CommonHotelInventory Update = DB.tbl_CommonHotelInventories.Single(x => x.Sid == DatList[0].Sid);
                                    Update.HotelCode = HotelCode;
                                    Update.SupplierId = AccountManager.GetSupplierByUser().ToString();
                                    Update.RoomType = Room[r].ToString();
                                    Update.RateType = RateType[i];
                                    Update.Month = distinctArray[j];
                                    Update.InventoryType = "FreeSale";
                                    Update.Year = distinctArrayYr[j];

                                    for (int d = 0; d < ListNDates[k].List_Newdates.Count; d++)
                                    {
                                        var mont = ListNDates[k].List_Newdates[d].Month.ToString();

                                        switch (ListNDates[k].List_Newdates[d].Day.ToString())
                                        {

                                            case "1":
                                                Update.Date_1 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "2":
                                                Update.Date_2 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "3":
                                                Update.Date_3 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "4":
                                                Update.Date_4 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "5":
                                                Update.Date_5 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "6":
                                                Update.Date_6 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "7":
                                                Update.Date_7 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "8":
                                                Update.Date_8 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "9":
                                                Update.Date_9 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "10":
                                                Update.Date_10 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "11":
                                                Update.Date_11 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "12":
                                                Update.Date_12 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "13":
                                                Update.Date_13 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "14":
                                                Update.Date_14 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "15":
                                                Update.Date_15 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "16":
                                                Update.Date_16 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "17":
                                                Update.Date_17 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "18":
                                                Update.Date_18 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "19":
                                                Update.Date_19 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "20":
                                                Update.Date_20 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "21":
                                                Update.Date_21 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "22":
                                                Update.Date_22 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "23":
                                                Update.Date_23 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "24":
                                                Update.Date_24 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "25":
                                                Update.Date_25 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "26":
                                                Update.Date_26 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "27":
                                                Update.Date_27 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "28":
                                                Update.Date_28 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "29":
                                                Update.Date_29 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "30":
                                                Update.Date_30 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                            case "31":
                                                Update.Date_31 = InType + "_" + NoOfRoom + "_" + NoOfSold;
                                                break;
                                        }

                                        switch (ListNDates[k].List_Newdates[d].Day.ToString())
                                        {

                                            case "1":
                                                Update.Option_Date_1 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "2":
                                                Update.Option_Date_2 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "3":
                                                Update.Option_Date_3 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "4":
                                                Update.Option_Date_4 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "5":
                                                Update.Option_Date_5 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "6":
                                                Update.Option_Date_6 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "7":
                                                Update.Option_Date_7 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "8":
                                                Update.Option_Date_8 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "9":
                                                Update.Option_Date_9 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "10":
                                                Update.Option_Date_10 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "11":
                                                Update.Option_Date_11 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "12":
                                                Update.Option_Date_12 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "13":
                                                Update.Option_Date_13 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "14":
                                                Update.Option_Date_14 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "15":
                                                Update.Option_Date_15 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "16":
                                                Update.Option_Date_16 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "17":
                                                Update.Option_Date_17 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "18":
                                                Update.Option_Date_18 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "19":
                                                Update.Option_Date_19 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "20":
                                                Update.Option_Date_20 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "21":
                                                Update.Option_Date_21 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "22":
                                                Update.Option_Date_22 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "23":
                                                Update.Option_Date_23 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "24":
                                                Update.Option_Date_24 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "25":
                                                Update.Option_Date_25 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "26":
                                                Update.Option_Date_26 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "27":
                                                Update.Option_Date_27 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "28":
                                                Update.Option_Date_28 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "29":
                                                Update.Option_Date_29 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "30":
                                                Update.Option_Date_30 = DaysPrior + "_" + InvSoldType;
                                                break;
                                            case "31":
                                                Update.Option_Date_31 = DaysPrior + "_" + InvSoldType;
                                                break;
                                        }
                                    }
                                }
                                DB.SubmitChanges();
                            }

                        }

                        json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                    }
                }


                return json;
            }
        }


        [WebMethod(EnableSession = true)]
        public string SearchInventory(string HotelCode, string Startdt, string Enddt, string SupplierId)
        {
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    //ManageInventory.GetInventory(Startdt, Enddt);
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    List<ListDates> ListAddDates = new List<ListDates>();
                    List<ListNewDates> ListNDates = new List<ListNewDates>();
                    List<DateTime> ListGDates = new List<DateTime>();
                    //var DatList = ""; 
                    DateTime FromDt = DateTime.ParseExact(Startdt, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    DateTime ToDt = DateTime.ParseExact(Enddt, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                    double noGDays = (ToDt - FromDt).TotalDays;

                    for (int j = 0; j <= noGDays; j++)
                    {
                        ListGDates.Add(FromDt.AddDays(j));

                    }

                    string mon = "";
                    for (int l = 0; l < ListGDates.Count; l++)
                    {

                        mon += ListGDates[l].Month.ToString() + '^';

                    }

                    string[] mothn = mon.Split('^');
                    string dat = "";
                    var distinctArrayn = mothn.Distinct().ToArray();
                    for (int j = 0; j < distinctArrayn.Length - 1; j++)
                    {
                        List<DateTime> ListAdDates = new List<DateTime>();
                        for (int d = 0; d < ListGDates.Count; d++)
                        {
                            var mn = ListGDates[d].Month.ToString();
                            if (distinctArrayn[j] == mn)
                                ListAdDates.Add(ListGDates[d].Date);
                        }

                        ListNDates.Add(new ListNewDates
                        {
                            List_Newdates = ListAdDates,
                        });

                    }
                    string json = "";
                    //var DatList = "";
                    var DatList = new List<tbl_CommonHotelInventory>();
                    List<string> Listdata = new List<string>();
                    List<ListData> ListDatas = new List<ListData>();
                    for (int k = 0; k < ListNDates.Count; k++)
                    {
                        string months = "";
                        string month = "";
                        for (int l = 0; l < ListNDates[k].List_Newdates.Count; l++)
                        {
                            if (ListNDates[k].List_Newdates[l].Month.ToString().Length == 1)
                                month += "0" + ListNDates[k].List_Newdates[l].Month.ToString() + '^';
                            else
                                month += ListNDates[k].List_Newdates[l].Month.ToString() + '^';

                        }
                        string[] moth = month.Split('^');
                        var distinctArray = moth.Distinct().ToArray();

                        string years = "";
                        string year = "";
                        for (int m = 0; m < ListNDates[k].List_Newdates.Count; m++)
                        {
                            year += ListNDates[k].List_Newdates[m].Year.ToString() + '^';

                        }
                        string[] yr = year.Split('^');
                        var distinctArrayYr = yr.Distinct().ToArray();
                        List<object> arrDates = new List<object>();
                        for (int j = 0; j < distinctArray.Length - 1; j++)
                        {
                            DatList = (from obj in DB.tbl_CommonHotelInventories where obj.HotelCode == HotelCode && obj.Year == distinctArrayYr[j] && obj.Month == distinctArray[j] && obj.SupplierId == SupplierId select obj).ToList();

                        }

                        ListDatas.Add(new ListData
                          {
                              Listdata = DatList
                          });


                    }
                    List<string> RateType = new List<string>();
                    foreach (var objDates in ListDatas)
                    {
                        var Details = objDates.Listdata.Select(d => d.RoomType).Distinct().ToList();
                        foreach (string obj in Details)
                        {
                            if (!RateType.Contains(obj))
                                RateType.Add(obj);
                        }
                    }
                    return objserialize.Serialize(new { Session = 1, retCode = 1, ListDatas = ListDatas, ListNDates = ListNDates, RateType = RateType });
                }
            }

            catch (Exception)
            {

                throw;
            }
            return objserialize.Serialize(new { Session = 1, retCode = 0 });
        }

        [WebMethod(EnableSession = true)]
        public string GetInventory(string Startdt, string Enddt)
        {
            try
            {
                DateTime From = DateTime.ParseExact(Startdt, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                DateTime To = DateTime.ParseExact(Enddt, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                double noGDays = (To - From).TotalDays;
                List<string> ListDates = new List<string>();
                for (int i = 0; i <= noGDays; i++)
                {
                    ListDates.Add(From.AddDays(i).ToString("dd-MM"));
                }
                
                var arrInventory= ManageInventory.GetInventory(Startdt, Enddt);
                if (arrInventory.Count != 0)
                 return objserialize.Serialize(new { Session = 1, retCode = 1, arrInventory = arrInventory.OrderBy(d=>d.HotelName).ToList(), ListDates = ListDates });
                else
                    return objserialize.Serialize(new { Session = 1, retCode = 2,  ListDates = ListDates });
            }
            catch (Exception)
            {
                return objserialize.Serialize(new { Session = 1, retCode = 0 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string UpdateInventory(List<Inventory> ListRoomRate, string status)
        {
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    if (status == "Start Sale")
                        status = "fs";
                    else
                        status = "ss";

                    for (int i = 0; i < ListRoomRate.Count; i++)
                    {
                        tbl_CommonHotelInventory Update = DB.tbl_CommonHotelInventories.Single(x => x.Sid == ListRoomRate[i].ID);
                        for (int j = 0; j < ListRoomRate[i].Date.Count; j++)
                        {
                            DateTime dt = DateTime.ParseExact(ListRoomRate[i].Date[j], "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                            switch (dt.Day.ToString())
                            {

                                case "1":
                                    Update.Date_1 = UpdateDate(Update.Date_1, status);
                                    break;
                                case "2":
                                    Update.Date_2 = UpdateDate(Update.Date_2, status);
                                    break;
                                case "3":
                                    Update.Date_3 = UpdateDate(Update.Date_3, status);
                                    break;
                                case "4":
                                    Update.Date_4 = UpdateDate(Update.Date_4, status);
                                    break;
                                case "5":
                                    Update.Date_5 = UpdateDate(Update.Date_5, status);
                                    break;
                                case "6":
                                    Update.Date_6 = UpdateDate(Update.Date_6, status);
                                    break;
                                case "7":
                                    Update.Date_7 = UpdateDate(Update.Date_7, status);
                                    break;
                                case "8":
                                    Update.Date_8 = UpdateDate(Update.Date_8, status);
                                    break;
                                case "9":
                                    Update.Date_9 = UpdateDate(Update.Date_9, status);
                                    break;
                                case "10":
                                    Update.Date_10 = UpdateDate(Update.Date_10, status);
                                    break;
                                case "11":
                                    Update.Date_11 = UpdateDate(Update.Date_11, status);
                                    break;
                                case "12":
                                    Update.Date_12 = UpdateDate(Update.Date_12, status);
                                    break;
                                case "13":
                                    Update.Date_13 = UpdateDate(Update.Date_13, status);
                                    break;
                                case "14":
                                    Update.Date_14 = UpdateDate(Update.Date_14, status);
                                    break;
                                case "15":
                                    Update.Date_15 = UpdateDate(Update.Date_15, status);
                                    break;
                                case "16":
                                    Update.Date_16 = UpdateDate(Update.Date_16, status);
                                    break;
                                case "17":
                                    Update.Date_17 = UpdateDate(Update.Date_17, status);
                                    break;
                                case "18":
                                    Update.Date_18 = UpdateDate(Update.Date_18, status);
                                    break;
                                case "19":
                                    Update.Date_19 = UpdateDate(Update.Date_19, status);
                                    break;
                                case "20":
                                    Update.Date_20 = UpdateDate(Update.Date_20, status);
                                    break;
                                case "21":
                                    Update.Date_21 = UpdateDate(Update.Date_21, status);
                                    break;
                                case "22":
                                    Update.Date_22 = UpdateDate(Update.Date_22, status);
                                    break;
                                case "23":
                                    Update.Date_23 = UpdateDate(Update.Date_23, status);
                                    break;
                                case "24":
                                    Update.Date_24 = UpdateDate(Update.Date_24, status);
                                    break;
                                case "25":
                                    Update.Date_25 = UpdateDate(Update.Date_25, status);
                                    break;
                                case "26":
                                    Update.Date_26 = UpdateDate(Update.Date_26, status);
                                    break;
                                case "27":
                                    Update.Date_27 = UpdateDate(Update.Date_27, status);
                                    break;
                                case "28":
                                    Update.Date_28 = UpdateDate(Update.Date_28, status);
                                    break;
                                case "29":
                                    Update.Date_29 = UpdateDate(Update.Date_29, status);
                                    break;
                                case "30":
                                    Update.Date_30 = UpdateDate(Update.Date_30, status);
                                    break;
                                case "31":
                                    Update.Date_31 = UpdateDate(Update.Date_31, status);
                                    break;
                            }
                        }


                    }
                    DB.SubmitChanges();

                    return objserialize.Serialize(new { Session = 1, retCode = 1 });
                }
            }

            catch (Exception)
            {

                throw;
            }
            return objserialize.Serialize(new { Session = 1, retCode = 0 });
        }

        public string UpdateDate(string OldDate, string InventType)
        {
            string Update = "";
            try
            {
                Update += InventType;
                if (OldDate.Split('_').Length > 1)
                {
                    Update += "_" + OldDate.Split('_')[1];
                }
                if (OldDate.Split('_').Length == 3)
                {
                    Update += "_" + OldDate.Split('_')[2];
                }

            }
            catch { }
            return Update;
        }

        [WebMethod(EnableSession = true)]
        public string GetSupplierList(string HotelCode)
        {
            try
            {
                using (var DB = new DBHandlerDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

                    var SipplierList = (from obj in DB.tbl_CommonHotelInventories
                                        join Adm in DB.tbl_AdminLogins on Convert.ToInt64(obj.SupplierId) equals Adm.sid
                                        join Hotel in DB.tbl_CommonHotelMasters on Convert.ToInt64(obj.HotelCode) equals Hotel.sid
                                        where obj.HotelCode == HotelCode
                                       select new
                                       {
                                         Adm.AgencyName,
                                         obj.HotelCode,
                                         obj.SupplierId,
                                         Hotel.HotelAddress,
                                         Hotel.HotelName                                         
                                       }).ToList().Distinct();

                    return objserialize.Serialize(new { Session = 1, retCode = 1, SipplierList = SipplierList });
                }
            }

            catch (Exception)
            {

                return objserialize.Serialize(new { Session = 1, retCode = 0 });
            }
         
        }

        public class ListDates
        {
            public List<DateTime> List_dates { get; set; }
        }

        public class ListNewDates
        {
            public List<DateTime> List_Newdates { get; set; }
        }

        public class DatesInv
        {
            public DateTime From { get; set; }
            public DateTime To { get; set; }
        }
        public class ListData
        {
            public List<tbl_CommonHotelInventory> Listdata { get; set; }
        }
    }
}
