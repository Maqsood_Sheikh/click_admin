﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Thumnail.aspx.cs" Inherits="CUTAE.activities_orange_files.Thumnail" %>
<%@Import Namespace="System.Drawing" %>
<%@Import Namespace="System.Drawing.Imaging" %>
<%@Import Namespace="System.Drawing.Drawing2D" %>
<%@Import Namespace="System.IO" %>
<%@Import Namespace="System.Data" %>
<%@Import Namespace="System.Configuration" %>
<script runat="server">
    /// <summary>
    /// This function is called every time page is loaded
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    ///  <returns><c>void</c></returns>
    protected void Page_Load(object sender, EventArgs e)
    {
        //User objUser = Session["User"] as User;
        //if (objUser == null)
        //{
        //    Response.Write("Your Session expired");
        //}
        //else
        //{

        try
        {
            string sDocumentPageFileName = Request.QueryString["fn"] ;
            //string Serverpath = System.Configuration.ConfigurationManager.AppSettings["FolderPath"];
            string Serverpath = Server.MapPath("~/activityImages/");
            string currentDirectory = Server.MapPath("/");
            //string sProductFolder = Request.QueryString["productfolder"];
            string sSourceDocumentPageFolder = Serverpath;
            //string sSourceDocumentPageFolder = currentDirectory + "\\" + Serverpath + sProductFolder;           

            int nWidth = Convert.ToInt32(Request.QueryString["w"]);
            int nHeight = Convert.ToInt32(Request.QueryString["h"]);
            int nOrint = 0;
            try
            {
                nOrint = Convert.ToInt32(Request.QueryString["o"]);
            }
            catch { }
            SendDocumentPage(sSourceDocumentPageFolder, sDocumentPageFileName, nWidth, nHeight, nOrint);
        }
        catch { }

    }
           
    //    }
    //}
    
    /// <summary>
    /// This function send document pages
    /// </summary>
    /// <param name="sSourceDocumentPageFolder">Source document page folder(Type:String)</param>
    /// <param name="sDocumentPageFileName">Document page file name(Type:String)</param>
    /// <param name="nWidth">Width(Type:int)</param>
    /// <param name="nHeight">Height(Type:int)</param>
    ///  <returns><c>void</c></returns>
    public void SendDocumentPage(string sSourceDocumentPageFolder, string sDocumentPageFileName, int nWidth, int nHeight,int nOrintation)
    {
        System.Drawing.Image oImg = null;
        System.Drawing.Image oThumbNail = null;
        try
        {
            string sPhysicalPath;
            sPhysicalPath = sSourceDocumentPageFolder + @"\" + sDocumentPageFileName;
            oImg = System.Drawing.Image.FromFile(sPhysicalPath);
            if(nOrintation == 90)
                oImg.RotateFlip(RotateFlipType.Rotate90FlipNone);
            else if(nOrintation == 180)
                oImg.RotateFlip(RotateFlipType.Rotate180FlipNone);
            else if (nOrintation == 270)
                oImg.RotateFlip(RotateFlipType.Rotate270FlipNone);
            if (oImg.Height < oImg.Width)
            {
                nWidth = 600;
                nHeight = (int)((float.Parse(oImg.Height.ToString()) / float.Parse(oImg.Width.ToString())) * (float)nWidth);
            }
            Size ThumbNailSize = new Size(nWidth , nHeight );
            oThumbNail = new Bitmap( ThumbNailSize.Width, ThumbNailSize.Height);
            Graphics oGraphic = Graphics.FromImage(oThumbNail);
            oGraphic.Clear(Color.White); 
            oGraphic.CompositingQuality = CompositingQuality.HighQuality;
            oGraphic.SmoothingMode = SmoothingMode.HighQuality;
            oGraphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
            //Rectangle oRectangle = new Rectangle(0, 0,ThumbNailSize.Width, ThumbNailSize.Height);
            oGraphic.DrawImage(oImg,0,0 ,ThumbNailSize.Width ,ThumbNailSize.Height);

            
            
            EncoderParameters codecParams = new EncoderParameters(1);
            codecParams.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);
    	    ImageCodecInfo[] encoders;
    	    encoders = ImageCodecInfo.GetImageEncoders();
            Response.ContentType = "image/jpeg";
	        oThumbNail.Save(Response.OutputStream,encoders[1],codecParams); 
            oImg.Dispose();
            oThumbNail.Dispose();
            oGraphic.Dispose();
        }
        catch (Exception ex) 
        {
            //Response.Write(ex.Message);
            oImg.Dispose();
            oThumbNail.Dispose();
        }
    }
</script>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>
