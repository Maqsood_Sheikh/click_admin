﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ShowInventory.aspx.cs" Inherits="CutAdmin.ShowInventory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment-with-locales.min.js"></script>
    <script src="Scripts/ShowInventory.js?v=1.2"></script>
    <script src="Scripts/moments.js"></script>
    <script src="Scripts/jquery-ui.js"></script>
    <link href="css/ScrollingTable.css" rel="stylesheet" />
    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

    <script type="text/javascript">

        var date = new Date();
        var time = new Date(date.getTime());
        time.setMonth(date.getMonth() + 1);
        time.setDate(0);
        var days = time.getDate() > date.getDate() ? time.getDate() - date.getDate() : 0;

        $(document).ready(function () {
            // GetCountry();

            $("#datepicker_start").datepicker({
                //dateFormat: "dd-mm-yy",
                //minDate: "dateToday",
                //autoclose: true,

                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy",
                onSelect: insertDepartureDate,
                //dateFormat: "yy-m-d",
                minDate: "dateToday",
                //setDate: new Date()
                //maxDate: "+3M +10D"
            });
            $("#datepicker_end").datepicker({
                dateFormat: "dd-mm-yy",
                changeMonth: true,
                changeYear: true,
                //onSelect: insertEndDate,
                // autoclose: true,
            });

            $("#datepicker_start").datepicker("setDate", new Date());
            var StartSelDate = $("#datepicker_start").val();
            $("#datepicker_end").val(StartSelDate);
            var dateObject = $("#datepicker_start").datepicker('getDate', days);
            dateObject.setDate(dateObject.getDate() + days);
            $("#datepicker_end").datepicker("setDate", dateObject);
            var date = $("#datepicker_end").datepicker("setDate", dateObject);
            // $("#datepicker_end").datepicker("maxDate", dateObject);
            // $('#datepicker_end').datepicker("option", "maxDate", days);
            debugger;
          
            setTimeout(function () {
                SearchInventory();
            }, 1000);
        });

    </script>
    <script>
        var splitMonth = "";
        var lastDay = "";
        function insertDepartureDate() {
            var StartSelDate = $("#datepicker_start").val();
            var splitDate = StartSelDate.split('-');
            var splitMonth = splitDate[1];
            var date = new Date(), y = date.getFullYear(), m = new Date().getMonth()
            //var firstDay = new Date(y, m, 1);
            var lastDay = new Date(y, splitMonth, 0);
            lastDay = moment(lastDay).format("DD-MM-YYYY")
            $("#datepicker_end").val(lastDay);
            // DateDisAuto();
            // DateDisable(1);
            SearchInventory();
        }

        //function DateDisAuto() {
        //    DateDisable(1);
        //    $("#datepicker_end").datepicker({
        //        dateFormat: "DD-MM-YYYY",
        //        minDate: "dateToday",
        //        onSelect: function (date) {
        //            DateDisable(1);
        //        }
        //    });

        //}

        var endDate = new Date();
        var endDates = new Date();
        function DateDisable(chk) {
            var dateObject = $("#datepicker_end").datepicker('getDate', '+1d');
            endDate.setDate(dateObject.getDate() + 1);

            if (chk == 1) {
                $("#datepicker_end").datepicker("destroy");
                $('#datepicker_end').datepicker({
                    dateFormat: 'DD-MM-YYYY',
                    minDate: endDate,
                    onSelect: function (date) {
                        DateDisable(2);
                    }
                });

            }
        }

        function ChangeDate()
        {
            var StartSelDate = $("#datepicker_start").val();
            $("#datepicker_end").val(StartSelDate);

        }

        //function insertEndDate()
        //{
        //    SelEndDate = $("#datepicker_end").val();
        //    splitEndDate = SelEndDate.split("-");
        //    endMonth = splitEndDate[1];
        //    if (endMonth != splitMonth)
        //    {
        //        alert("Please Select Date from Same Month of Start Date");
        //        $("#datepicker_end").val(SelEndDate);
        //    }
        //    else {
        //        $("#datepicker_end").val(SelEndDate);
        //    }
        //}
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section role="main" id="main">
        <div class="with-padding">
            <hgroup id="main-title" class="thin">
			<h1>Inventory Update</h1>
            <h2><b class="grey" id="lbl_Hotel"></b></h2><br />
            <u><h2 style=" margin-top: 20px;" class="grey" id="lbl_address"></h2></u>
            <hr>			
		</hgroup>
        <div class="with-padding ">
                    <div class="columns">
                        <div class="two-columns">
                            <label class="input-info">Start Date</label><span class="red">*</span>
                            <input class="input ui-autocomplete-input full-width dt1" type="text" id="datepicker_start" onchange="ChangeDate()" name="datepicker_From" style="cursor: pointer" value="" />
                        </div>
                        <div class="two-columns">
                            <label class="input-info">End Date</label><span class="red">*</span>
                            <input class="input ui-autocomplete-input full-width dt2" type="text" id="datepicker_end" onchange="SearchInventory()" style="cursor: pointer" value="" />
                        </div>
                        <%-- <div class="one-columns" id="" style="margin-top: 15px">
                            <input type="button" class="button blue-gradient" onclick="SearchInventory();" value="Search" />
                        </div>--%>
                        <div class="one-columns" id="" style="margin-top: 15px">
                            <input type="button" class="button anthracite-gradient" onclick="StartSaleModal();" value="Start Sale" />
                        </div>
                        <div class="one-columns" id="" style="margin-top: 15px">
                            <input type="button" class="button anthracite-gradient" onclick="StopSaleModal();" value="Stop Sale" />
                        </div>
                        <div class="one-columns" id="" style="margin-top: 15px">
                            <input type="radio" name="radio" id="Freesale_Id" onchange="AllotmentInventory()" checked>
                            <label>Freesale</label>
                        </div>
                        <div class="one-columns" id="" style="margin-top: 15px">
                            <input type="radio" name="radio" id="Allocation_Id" onchange="AllotmentInventory()">
                            <label>Allocation</label>
                        </div>
                         <div class="one-columns" id="" style="margin-top: 15px">
                            <input type="radio" name="radio" id="Allotmnet_Id" onchange="AllotmentInventory()">
                            <label>Allotment</label>
                        </div>
                    </div>

                    <div id="table-scroll" class="table-scroll">
                        <div class="respTable" id="div_tbl_InvList">
                            <table class="table responsive-table colorTable responsive-table-on InvList" id="tbl_InvList" style="font-size: small;"  >
                            </table>
                        </div>
                    </div>
                </div>
        </div>
    </section>

    <!-- JavaScript at the bottom for fast page loading -->
    <!-- Scripts -->
    <script src="js/libs/jquery-1.10.2.min.js"></script>
    <script src="js/setup.js"></script>

    <!-- Template functions -->
    <script src="js/developr.input.js"></script>
    <script src="js/developr.navigable.js"></script>
    <script src="js/developr.notify.js"></script>
    <script src="js/developr.scroll.js"></script>
    <script src="js/developr.tooltip.js"></script>
    <script src="js/developr.table.js"></script>

    <!-- Plugins -->
    <script src="js/libs/jquery.tablesorter.min.js"></script>
    <script src="js/libs/DataTables/jquery.dataTables.min.js"></script>
    <script src="js/developr.modal.js"></script>
    <script src="js/libs/glDatePicker/glDatePicker.min.js?v=1"></script>
</asp:Content>
