﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="TypesOfRooms.aspx.cs" Inherits="CutAdmin.TypesOfRooms" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <!-- Additional styles -->
	<link rel="stylesheet" href="css/styles/form.css?v=1">
	<link rel="stylesheet" href="css/styles/switches.css?v=1">
	<link rel="stylesheet" href="css/styles/table.css?v=1">

	<!-- DataTables -->
	<link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

	<!-- Microsoft clear type rendering -->
	<meta http-equiv="cleartype" content="on">
    <script src="Scripts/AddRoomType.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

		<hgroup id="main-title" class="thin">
			<h1> Room Type</h1>
		</hgroup>

		<div class="with-padding">

			<p class="wrapped left-icon">
				Room Type Master 
			</p>
           
                
            <div class="columns">
                <div class="new-row-mobile four-columns five-columns-tablet twelve-columns-mobile">
                    <h3 class="thin underline"><b>Add Room Type</b></h3>
                    <p class="block-label button-height">
						<label for="block-label-1" class="label">Room Type</label>
						<input type="text" name="block-label-1" id="txtRoomType" class="input full-width" value="">
					</p>
                 
                    <p class="button-height">
						<button type="button" class="button big" onclick="AddRoomType()" id="btn_Add">ADD </button>

						<button type="button" class="button big" onclick="UpdateRoomType()" style="display:none" id="btn_Update">Update</button>
						
					</p>

                </div>
				<div class="new-row-tablet new-row-mobile eight-columns twelve-columns-tablet">

           <div class="table-header button-height">
				
			</div>
		
            <table class="table responsive-table responsive-table-on" id="tbl_RoomType">

				<thead>
					<tr>
						<%--<th scope="col"><input type="checkbox" name="check-all" id="check-all" value="1"></th>--%>
						<th scope="col" width="15%" class="align-center hide-on-mobile">Sr No</th>
						<th scope="col">Room Type</th>
						<%--<th scope="col">Property Description</th>--%>
						<%--<th scope="col" width="15%" class="hide-on-tablet">Tags</th>--%>
						<th scope="col" width="100" class="align-right">Actions</th>
					</tr>
				</thead>

				<tbody >
					<%--<tr>
						<td>1</td>
						<td>John Doe</td>
						
						<td class="hide-on-mobile-portrait">Enabled</td>
						
						<td class="align-right vertical-center">
							<span class="button-group compact">
								<a href="#" class="button icon-pencil">Edit</a>
								
								<a href="#" class="button icon-trash with-tooltip confirm" title="Delete"></a>
							</span>
						</td>
					</tr>--%>
					
					</tbody>

			</table>
			<form method="post" action="" class="table-footer button-height large-margin-bottom">
				
			</form>

				</div>
            </div>
		</div>

	</section>

    <script src="js/libs/jquery-1.10.2.min.js"></script>
    <script src="js/setup.js"></script>

    <!-- Template functions -->
    <script src="js/developr.input.js"></script>
    <script src="js/developr.navigable.js"></script>
    <script src="js/developr.notify.js"></script>
    <script src="js/developr.scroll.js"></script>
    <script src="js/developr.tooltip.js"></script>
    <script src="js/developr.table.js"></script>

    <!-- Plugins -->
    <script src="js/libs/jquery.tablesorter.min.js"></script>
    <script src="js/libs/DataTables/jquery.dataTables.min.js"></script>


</asp:Content>
