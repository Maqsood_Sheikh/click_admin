﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddPackage.aspx.cs" Inherits="CutAdmin.AddPackage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/AddPackage.js"></script>
    <script src="Scripts/PricingDetails.js"></script>
    <%--<script type="text/javascript">
        $(document).ready(function () {
            $('#txt_Desctiption').ckeditor();
            $('#txt_TermsCondition').ckeditor();
        });
        $(function () {
            $('#select_Category, #select_Themes').change(function () {
                console.log($(this).val());
            }).multipleSelect({
                width: '100%'
            });
        });

        var tpj = jQuery;
        var chkinDate;
        var chkinMonth;
        var chkoutDate;
        var chkoutMonth;
        tpj(document).ready(function () {
            debugger;
            tpj("#txt_City").autocomplete({
                source: function (request, response) {
                    tpj.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "..Handler/AddPackageHandler.asmx/GetDestinationCode",
                        data: "{'name':'" + document.getElementById('txt_City').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 4,
                select: function (event, ui) {
                    debugger;
                    tpj('#hdnDCode').val(ui.item.id);
                    var h1 = ui.item.id;
                }
            });
        });


    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Add Package</h1>
               <div class="input full-width">
             <input value="" id="hdpackageId" class="input-unstyled full-width"  type="text" />
                   </div>
        </hgroup>
        <div class="with-padding">

            <div class="standard-tabs margin-bottom" id="add-tabs">

                <ul class="tabs">
                    <li id="lst_BasicInformation" class="active" onclick="CheckValidationForTabs('BasicInformation')"><a href="#BasicInformation">Basic Information</a></li>
                    <li id="lst_Pricing" onclick="CheckValidationForTabs('Pricing')"><a href="#Pricing">Pricing</a></li>
                    <li id="lst_Itinerary" onclick="CheckValidationForTabs('Itinerary')"><a href="#Itinerary">Itinerary</a></li>
                    <li id="lst_HotelDetails" onclick="CheckValidationForTabs('HotelDetails')"><a href="#HotelDetails">Hotel Details</a></li>
                    <li id="lst_PackageImages" onclick="CheckValidationForTabs('PackageImages')"><a href="#PackageImages">Package Images</a></li>
                </ul>

                <div class="tabs-content">

                    <div id="BasicInformation">
                        <div class="with-padding">
                            <h4>Basic Details</h4>
                            
                            <hr />
                            <div class="columns">
                                <div class="three-columns twelve-columns-mobile">
                                    <label>Package Name:</label><div class="input full-width">
                                        <input value="" id="txt_PackageName" class="input-unstyled full-width" placeholder="Package Name" type="text">
                                    </div>
                                </div>

                                <div class="three-columns twelve-columns-mobile">
                                    <label>Destination</label>
                                    <div class="input full-width">
                                        <input value="" id="txt_City" class="input-unstyled full-width" placeholder="City" type="text">
                                        <input type="hidden" id="hdnDCode" />
                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile">
                                    <label>Duration</label><div class="full-width button-height">
                                        <select id="txt_Duration" name="validation-select" class="select">
                                            <option value="0" selected="selected">Select Duration</option>
                                            <option value="1">01 Days</option>
                                            <option value="2">02 Days</option>
                                            <option value="3">03 Days</option>
                                            <option value="4">04 Days</option>
                                            <option value="5">05 Days</option>
                                            <option value="6">06 Days</option>
                                            <option value="7">07 Days</option>
                                            <option value="8">08 Days</option>
                                            <option value="9">09 Days</option>
                                            <option value="10">10 Days</option>
                                            <option value="11">11 Days</option>
                                            <option value="12">12 Days</option>
                                            <option value="13">13 Days</option>
                                            <option value="14">14 Days</option>
                                            <option value="15">15 Days</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile">
                                    <label>Tax</label><div class="input full-width">
                                        <input value="" id="txt_Tax" class="input-unstyled full-width" placeholder="Tax" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="columns">
                                <div class="three-columns twelve-columns-mobile">
                                    <label>Category</label>
                                    <br>
                                    <div class="full-width button-height" id="Divselect_Category">
                                        <select id="select_Category" multiple="multiple" class=" full-width Category">
                                            <option value="1">Standard</option>
                                            <option value="2">Deluxe</option>
                                            <option value="3">Premium</option>
                                            <option value="4">Luxury</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile">
                                    <label>Themes</label>
                                    <br>
                                    <div class="full-width button-height" id="Divselect_Themes">
                                        <select id="select_Themes" multiple="multiple" class=" full-width Category">
                                            <option value="1">Holidays</option>
                                            <option value="2">Umrah</option>
                                            <option value="3">Hajj</option>
                                            <option value="4">Honeymoon</option>
                                            <option value="5">Summer</option>
                                            <option value="6">Adventure</option>
                                            <option value="7">Deluxe</option>
                                            <option value="8">Business</option>
                                            <option value="9">Premium</option>
                                            <option value="10">Wildlife</option>
                                            <option value="11">Weekend</option>
                                            <option value="12">New Year</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="three-columns twelve-columns-mobile">
                                    <label>Valid From:</label>
                                    <span class="input full-width">
                                        <span class="icon-calendar"></span>
                                        <input type="text" name="datepicker" id="datepicker" placeholder="mm/dd/yyyy" class="input-unstyled datepicker" value="">
                                    </span>
                                </div>
                                <div class="three-columns twelve-columns-mobile">
                                    <label>Valid Up to:</label><span class="input full-width">
                                        <span class="icon-calendar"></span>
                                        <input type="text" name="datepicker" id="datepicker2" placeholder="mm/dd/yyyy" class="input-unstyled datepicker" value="">
                                    </span>
                                </div>
                            </div>

                            <h4>Cancellation Policy</h4>
                            <hr />
                            <div class="columns">
                                <div class="six-columns twelve-columns-mobile">
                                    <label>Before Check In Days:</label><div class="input full-width">
                                        <input value="" id="txt_cancelDays" class="input-unstyled full-width" placeholder="Before Check In Days" type="text">
                                    </div>
                                </div>
                                <div class="six-columns twelve-columns-mobile">
                                    <label>Cancellation Charge In Percentage:</label><div class="input full-width">
                                        <input value="" id="txt_CancelCharge" class="input-unstyled full-width" placeholder="Cancellation Charge In Percentage" type="text">
                                    </div>
                                </div>
                            </div>

                            <h4>Inventory</h4>
                            <hr />
                            <div class="columns">
                                <div class="six-columns twelve-columns-mobile">
                                    <input type="radio" name="radio" id="radio_InventoryRequest" onchange="setInvetoryFixed();" class="radio mid-margin-left">
                                    <label for="radio-2" class="label">On Request</label>
                                </div>
                                <div class="six-columns twelve-columns-mobile">
                                    <input type="radio" name="radio" id="radio_InventoryFixed" onchange="setInvetoryFixed();" class="radio mid-margin-left">
                                    <label for="radio-2" class="label">Fixed no of packages</label>
                                    <div class="columns" id="Divtxt_Inventory" style="display: none;">
                                        <div class="six-columns twelve-columns-mobile">
                                            <div class="input full-width">
                                                <input value="" id="txt_Inventory" class="input-unstyled full-width" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <%--  <input type="text" id="txt_Inventory" class="input-unstyled full-width"/>--%>
                                </div>
                            </div>
                            <h4>Description</h4>
                            <div class="columns">
                                <div class="twelve-columns">
                                    <textarea name="ckeditor" id="txt_Desctiption" class="input full-width autoexpanding"></textarea>
                                </div>
                            </div>

                            <h4>Terms & Condition</h4>
                            <div class="columns">
                                <div class="twelve-columns">
                                    <textarea name="ckeditor" id="txt_TermsCondition" class="input full-width autoexpanding"></textarea>
                                </div>
                            </div>

                            <hr />
                            <p class="text-alignright">
                                <button type="button" class="button anthracite-gradient" onclick="AddPackageBasicDetails();">Save</button>
                            </p>
                        </div>
                    </div>


                    <div id="Pricing">
                        <div class="with-padding">
                            <h4>Pricing Details</h4>
                            <hr />
                             <div class="with-padding" id="div_BasicDetails">
                            <div class="columns">
                                <div class="four-columns twelve-columns-mobile">
                                    <label>Single:</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="53500.00" type="text">
                                    </div>
                                </div>
                                <div class="four-columns twelve-columns-mobile">
                                    <label>Twin Sharing</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="0.00" type="text">
                                    </div>
                                </div>

                                <div class="four-columns twelve-columns-mobile">
                                    <label>Extra Adult:</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="4.00" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="columns">
                                <div class="four-columns twelve-columns-mobile">
                                    <label>Infant Kid:</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="0.00" type="text">
                                    </div>
                                </div>
                                <div class="four-columns twelve-columns-mobile">
                                    <label>Kid rate without bed:</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="0.00" type="text">
                                    </div>
                                </div>

                                <div class="four-columns twelve-columns-mobile">
                                    <label>Kid rate with bed:</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="0.00" type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="columns listchkBox">
                                <div class="six-columns twelve-columns-mobile">
                                    <strong>Inclusions:</strong>
                                    <ul>
                                        <li>
                                            <input type="checkbox" name="checkbox-2" id="checkbox-1" value="1" class="checkbox mid-margin-left">
                                            <label for="checkbox-2" class="label">Airfare with Airport transfer</label></li>
                                        <li>
                                            <input type="checkbox" name="checkbox-2" id="checkbox-2" value="1" class="checkbox mid-margin-left">
                                            <label for="checkbox-2" class="label">Sight Seeing</label></li>
                                        <li>
                                            <input type="checkbox" name="checkbox-2" id="checkbox-3" value="1" class="checkbox mid-margin-left">
                                            <label for="checkbox-2" class="label">Breakfast</label></li>
                                        <li>
                                            <input type="checkbox" name="checkbox-2" id="checkbox-4" value="1" class="checkbox mid-margin-left">
                                            <label for="checkbox-2" class="label">Tour Guide</label></li>
                                        <li>
                                            <input type="checkbox" name="checkbox-2" id="checkbox-5" value="1" class="checkbox mid-margin-left">
                                            <label for="checkbox-2" class="label">Lunch</label></li>
                                        <li>
                                            <input type="checkbox" name="checkbox-2" id="checkbox-6" value="1" class="checkbox mid-margin-left">
                                            <label for="checkbox-2" class="label">Dinner</label></li>
                                        <li>
                                            <input type="checkbox" name="checkbox-2" id="checkbox-7" value="1" class="checkbox mid-margin-left">
                                            <label for="checkbox-2" class="label">Hotel pickup and drop off.</label></li>
                                        <li>
                                            <input type="checkbox" name="checkbox-2" id="checkbox-8" value="1" class="checkbox mid-margin-left">
                                            <label for="checkbox-2" class="label">Round trip Hotel Transfers</label></li>
                                        <li>
                                            <input type="checkbox" name="checkbox-2" id="checkbox-9" value="1" class="checkbox mid-margin-left">
                                            <label for="checkbox-2" class="label">Service fee for chauffeur-guide. </label>
                                        </li>
                                    </ul>
                                    <div class="clear">&nbsp;</div>
                                    <div class="columns">
                                        <div class="nine-columns ten-columns-mobile">
                                            <div class="input full-width">
                                                <input value="" class="input-unstyled full-width" type="text">
                                            </div>
                                        </div>
                                        <div class="one-column">
                                            <i class="fa fa-plus button anthracite-gradient"></i>
                                        </div>
                                    </div>

                                </div>
                                <div class="six-columns twelve-columns-mobile">
                                    <strong>Exclusion:</strong>
                                    <ul>
                                        <li>
                                            <input type="checkbox" name="checkbox-2" id="checkbox-1" value="1" class="checkbox mid-margin-left">
                                            <label for="checkbox-2" class="label">Airfare with Airport transfer</label></li>
                                        <li>
                                            <input type="checkbox" name="checkbox-2" id="checkbox-2" value="1" class="checkbox mid-margin-left">
                                            <label for="checkbox-2" class="label">Sight Seeing</label></li>
                                        <li>
                                            <input type="checkbox" name="checkbox-2" id="checkbox-3" value="1" class="checkbox mid-margin-left">
                                            <label for="checkbox-2" class="label">Breakfast</label></li>
                                        <li>
                                            <input type="checkbox" name="checkbox-2" id="checkbox-4" value="1" class="checkbox mid-margin-left">
                                            <label for="checkbox-2" class="label">Tour Guide</label></li>
                                        <li>
                                            <input type="checkbox" name="checkbox-2" id="checkbox-5" value="1" class="checkbox mid-margin-left">
                                            <label for="checkbox-2" class="label">Lunch</label></li>
                                        <li>
                                            <input type="checkbox" name="checkbox-2" id="checkbox-6" value="1" class="checkbox mid-margin-left">
                                            <label for="checkbox-2" class="label">Dinner</label></li>
                                        <li>
                                            <input type="checkbox" name="checkbox-2" id="checkbox-7" value="1" class="checkbox mid-margin-left">
                                            <label for="checkbox-2" class="label">Personal expenses</label></li>
                                        <li>
                                            <input type="checkbox" name="checkbox-2" id="checkbox-8" value="1" class="checkbox mid-margin-left">
                                            <label for="checkbox-2" class="label">Camera & video permits.</label></li>
                                    </ul>

                                    <div class="clear">&nbsp;</div>
                                    <div class="columns">
                                        <div class="nine-columns ten-columns-mobile">
                                            <div class="input full-width">
                                                <input value="" class="input-unstyled full-width" type="text">
                                            </div>
                                        </div>
                                        <div class="one-column">
                                            <i class="fa fa-plus button anthracite-gradient"></i>
                                        </div>
                                    </div>


                                </div>
                            </div>

                            <hr />
                            <p class="text-alignright">
                                <button type="button" class="button anthracite-gradient" onclick="SavePricingDetails();">Save</button>
                            </p>
                                 </div>
                        </div>
                    </div>

                    <div id="Itinerary">
                        <div class="with-padding">
                            <h4>Itinerary Details</h4>
                            <hr />
                            <div class="columns">
                                <div class="two-columns twelve-columns-mobile">Day 1</div>
                                <div class="ten-columns twelve-columns-mobile">
                                    <div class="columns">

                                        <div class="six-columns">
                                            <div class="input full-width">
                                                <input value="" class="input-unstyled full-width" placeholder="HOTEL TOPAZ KANDY" type="text">
                                            </div>
                                        </div>

                                        <div class="six-columns">
                                            <div class="input full-width">
                                                <input value="" class="input-unstyled full-width" placeholder="AIRPORT –KANDY" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <textarea name="ckeditor" id="ckeditor3"><p>- PICK UP FROM THE AIRPORT</p>
<p>- PINNAWALA ELEPHANT ORPHANAGE (EN-ROUTE)</p>
<p>- KANDY CITY TOUR</p>
<p>- TEMPLE OF TOOTH</p>
<p>- CULTURAL SHOW (Complimentary Tickets)</p>
<p>- SPICE GARDEN</p>
<p>- ROYAL BOTANICAL GARDENS</p></textarea>
                                </div>
                            </div>

                            <div class="columns">
                                <div class="two-columns twelve-columns-mobile">Day 2</div>
                                <div class="ten-columns twelve-columns-mobile">
                                    <div class="columns">

                                        <div class="six-columns">
                                            <div class="input full-width">
                                                <input value="" class="input-unstyled full-width" placeholder="HOTEL TOPAZ KANDY" type="text">
                                            </div>
                                        </div>

                                        <div class="six-columns">
                                            <div class="input full-width">
                                                <input value="" class="input-unstyled full-width" placeholder="AIRPORT –KANDY" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <textarea name="ckeditor" id="ckeditor4"><p>- PICK UP FROM THE AIRPORT</p>
<p>- PINNAWALA ELEPHANT ORPHANAGE (EN-ROUTE)</p>
<p>- KANDY CITY TOUR</p>
<p>- TEMPLE OF TOOTH</p>
<p>- CULTURAL SHOW (Complimentary Tickets)</p>
<p>- SPICE GARDEN</p>
<p>- ROYAL BOTANICAL GARDENS</p></textarea>
                                </div>
                            </div>


                            <hr />
                            <p class="text-alignright">
                                <button type="button" class="button anthracite-gradient" onclick="SaveItinerary();">Save</button>
                            </p>
                        </div>
                    </div>

                    <div id="HotelDetails">
                        <div class="with-padding">
                            <h4>Hotel Details</h4>
                            <hr />
                            <div class="columns">
                                <div class="four-columns twelve-columns-mobile">
                                    Day 1 
    
    <p class="uploadimg">
        <img src="img/upload-image.png" />
    </p>
                                    <p class="button-height">
                                        <input type="file" name="special-input-1" id="special-input-1" value="" class="file" multiple>
                                    </p>
                                </div>
                                <div class="eight-columns twelve-columns-mobile">


                                    <div class="input full-width mrbot20">
                                        <input value="" class="input-unstyled full-width" placeholder="AIRPORT –KANDY" type="text">
                                    </div>

                                    <textarea name="ckeditor" id="ckeditor"></textarea>
                                </div>
                            </div>

                            <div class="columns">
                                <div class="four-columns twelve-columns-mobile">
                                    Day 2
    <p class="uploadimg">
        <img src="img/upload-image.png" />
    </p>
                                    <p class="button-height">
                                        <input type="file" name="special-input-1" id="special-input-1" value="" class="file" multiple>
                                    </p>
                                </div>
                                <div class="eight-columns twelve-columns-mobile">

                                    <div class="input full-width mrbot20">
                                        <input value="" class="input-unstyled full-width" placeholder="HOTEL NAME" type="text">
                                    </div>
                                    <textarea name="ckeditor" id="ckeditor5"></textarea>
                                </div>
                            </div>


                            <hr />
                            <p class="text-alignright">
                                <button type="button" class="button anthracite-gradient">Save</button>
                            </p>
                        </div>

                        <div id="PackageImages" class="with-padding">
                            <h4>Current Images</h4>
                            <hr />
                            <div class="columns">
                                <div class="four-columns twelve-columns-mobile">

                                    <p class="uploadimg">
                                        <img src="img/upload-image.png" />
                                    </p>

                                </div>

                            </div>

                            <h4>Update Images</h4>
                            <hr />
                            <div class="columns">
                                <div class="six-columns twelve-columns-mobile">
                                    <p>Image 1:</p>
                                    <p class="button-height">
                                        <input type="file" name="special-input-1" id="special-input-1" value="" class="file" multiple>
                                    </p>
                                </div>
                                <div class="six-columns twelve-columns-mobile">
                                    <p>Image 2:</p>
                                    <p class="button-height">
                                        <input type="file" name="special-input-1" id="special-input-1" value="" class="file" multiple>
                                    </p>
                                </div>
                                <div class="six-columns twelve-columns-mobile">
                                    <p>Image 2:</p>
                                    <p class="button-height">
                                        <input type="file" name="special-input-1" id="special-input-1" value="" class="file" multiple>
                                    </p>
                                </div>
                                <div class="six-columns twelve-columns-mobile">
                                    <p>Image 4:</p>
                                    <p class="button-height">
                                        <input type="file" name="special-input-1" id="special-input-1" value="" class="file" multiple>
                                    </p>
                                </div>
                                <div class="six-columns twelve-columns-mobile">
                                    <p>Image 5:</p>
                                    <p class="button-height">
                                        <input type="file" name="special-input-1" id="special-input-1" value="" class="file" multiple>
                                    </p>
                                </div>
                                <div class="six-columns twelve-columns-mobile">
                                    <p>Image 5:</p>
                                    <p class="button-height">
                                        <input type="file" name="special-input-1" id="special-input-1" value="" class="file" multiple>
                                    </p>
                                </div>

                            </div>


                            <hr />
                            <p class="text-alignright">
                                <button type="button" class="button anthracite-gradient">Upload Images</button>
                            </p>
                        </div>
                    </div>
                </div>

            </div>



        </div>


    </section>
    <!-- End main content -->

    <!-- Scripts for TextEditor-->
    <script src="js/libs/jquery-1.10.2.min.js"></script>
    <script src="js/setup.js"></script>


    <script src="js/libs/ckeditor/ckeditor.js"></script>
    <script>
        // Call template init (optional, but faster if called manually)
        $.template.init();

        // CKEditor
        CKEDITOR.replace('ckeditor3', {
            height: 200
        });

    </script>
    <script>
        // Call template init (optional, but faster if called manually)
        $.template.init();

        // CKEditor
        CKEDITOR.replace('ckeditor4', {
            height: 200
        });

    </script>
    <script>
        // Call template init (optional, but faster if called manually)
        $.template.init();

        // CKEditor
        CKEDITOR.replace('ckeditor', {
            height: 200
        });

    </script>
    <%-- <script>
        // Call template init (optional, but faster if called manually)
        $.template.init();

        // CKEditor
        CKEDITOR.replace('txt_TermsCondition', {
            height: 200
        });

    </script>--%>
    <script>
        // Call template init (optional, but faster if called manually)
        $.template.init();

        // CKEditor
        CKEDITOR.replace('ckeditor5', {
            height: 200
        });

    </script>
    <%-- <script>
        // Call template init (optional, but faster if called manually)
        $.template.init();

        // CKEditor
        CKEDITOR.replace('txt_Desctiption', {
            height: 200
        });

    </script>--%>
</asp:Content>
