﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Inventory.aspx.cs" Inherits="CutAdmin.Inventory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <!-- Additional styles -->
    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">

    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

    <link href="css/dynamicDiv.css" rel="stylesheet" />

    <script src="Scripts/moments.js"></script>
    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
    <style>
        ul li {
            list-style: none;
            cursor:pointer;
        }

        .deleteMe {
            float: right;
            background: #bb3131;
            color: white;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="standard-tabs margin-bottom tabs-active" id="add-tabs" style="height: 30px;">

						<ul class="tabs">
							<li class="active"><a href="#tab-1">Selected tab</a></li>
							<li><a href="#tab-2">Another tab</a></li>
							<li><a href="#tab-3">Another tab</a></li>
							<li class="disabled"><a href="#tab-4">Disabled tab</a></li>
							<li>Non-active</li>
						</ul>

						<div class="tabs-content" style="min-height: 29px;"><span class="tabs-back with-left-arrow top-bevel-on-light dark-text-bevel">Back</span>

							<div id="tab-1" class="with-padding tab-active">

								Selected tab

							</div>

							<div id="tab-2" class="with-padding" style="display: none;">

								Alternate tab 1

							</div>

							<div id="tab-3" class="with-padding" style="display: none;">

								Alternate tab 2

							</div>

							<div id="tab-4" class="with-padding" style="display: none;">

								Disabled tab

							</div>

						</div>

					</div>


     <!-- JavaScript at the bottom for fast page loading -->
    <!-- Scripts -->
    <script src="js/libs/jquery-1.10.2.min.js"></script>
    <script src="js/setup.js"></script>

    <!-- Template functions -->
    <script src="js/developr.input.js"></script>
    <script src="js/developr.navigable.js"></script>
    <script src="js/developr.notify.js"></script>
    <script src="js/developr.scroll.js"></script>
    <script src="js/developr.tooltip.js"></script>
    <script src="js/developr.table.js"></script>
    <script src="js/developr.accordions.js"></script>
    <script src="js/developr.wizard.js"></script>

    <!-- Plugins -->
    <script src="js/libs/jquery.tablesorter.min.js"></script>
    <script src="js/libs/DataTables/jquery.dataTables.min.js"></script>

    <!-- glDatePicker -->
    <script src="js/libs/glDatePicker/glDatePicker.min.js?v=1"></script>
</asp:Content>
