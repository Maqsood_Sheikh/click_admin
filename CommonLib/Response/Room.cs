﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Response
{
    public class Room
    {
        public RoomType RoomType { get; set; }
        public List<RoomType> LRoomType { get; set; }
        public Int64 AvailCount { get; set; }
        public List<CancellationPolicy> CancellationPolicy { get; set; }
        public float Price { get; set; }
        public float CutPrice { get; set; }
        public List<string> FacilityList { get; set; }
        public string EncryptCUTPrice { get; set; }
        public float AgentMarkup { get; set; }
        public List<Offers> OfferList { get; set; }
        public int MaxOccupancy { get; set; }
        public string MealPlan { get; set; }
        public bool TextOnMarkup { get; set; }
    }
}
