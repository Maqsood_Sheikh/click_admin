﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Response
{
    public class Facilities
    {
        public string Name { get; set; }
        public string HotelCode { get; set; }
        public int Count { get; set; }
    }
}
