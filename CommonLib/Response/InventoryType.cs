﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Response
{
    public class InventoryType
    {
        public int InvNoOfRoom { get; set; }
        public string InventoryName { get; set; }
        public string InvDate { get; set; }
        public bool IsStop { get; set; }
        public int Sold { get; set; }

    }
}
