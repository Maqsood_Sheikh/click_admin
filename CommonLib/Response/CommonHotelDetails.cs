﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Response
{
    public class CommonHotelDetails
    {
        public string Address { get; set; }
        public string Category { get; set; }
        public string Currency { get; set; }       
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string DotwCode { get; set; }
        public string HotelBedsCode { get; set; }
        public string MGHCode { get; set; }
        public string GRNCode { get; set; }
        public string GTACode { get; set; }
        public string ExpediaCode { get; set; }
        public string Description { get; set; }
        public List<String> Facility { get; set; }
        public string HotelId { get; set; }
        public string HotelName { get; set; }       
        public List<Image> Image { get; set; }
        public string Langitude { get; set; }
        public string Latitude { get; set; }
        public List<Location> Location { get; set; }
        public List<Room> Room { get; set; }
        public float UserRating { get; set; }
        public float Price { get; set; }
        public float CUTPrice { get; set; }
        public float FeeAtHotel { get; set; }
        public float AgentMarkup { get; set; }
        public string Supplier { get; set; }
        public List<RoomOccupancy> sRooms { get; set; }
        public bool PreferedHotel { get; set; }
        public int Night { get; set; }

        public List<RateGroup> RateList { get; set; }

        public float MinPrice { get; set; }
        public float MaxPrice { get; set; }
        public float Total { get; set; }
    }
}
