﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Response
{
  public  class includedMeal
    {
        public int runno { get; set; }
        public string mealName { get; set; }
        public string mealType { get; set; }
    }
  public class Inventory
  {
      public int InvNoOfRoom { get; set; }
      public string InventoryName { get; set; }
      public string InvDate { get; set; }
      public int Sold { get; set; }

  }
}
