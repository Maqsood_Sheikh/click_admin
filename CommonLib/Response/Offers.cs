﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Response
{
    public class Offers
    {
        public Int64 Sid { get; set; }
        public Int64 Offer_Id { get; set; }
        public Int64 Hotel_Id { get; set; }
        public Int64 Room_Id { get; set; }
        public Int64 Id { get; set; }
        public decimal RoomPrice { get; set; }
        public string BookBefore { get; set; }
        public string DateType { get; set; }
        public string DaysPrior { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal DiscountPer { get; set; }
        public string FreebiItem { get; set; }
        public string FreebiItemDetail { get; set; }
        public string HotelOfferCode { get; set; }
        public string MealPlan { get; set; }
        public string MinNights { get; set; }
        public decimal NewRate { get; set; }
        public string OfferCode { get; set; }
        public string OfferNote { get; set; }
        public string OfferOn { get; set; }
        public string OfferTerms { get; set; }
        public string OfferType { get; set; }
        public string SeasonName { get; set; }
        public string ValidFrom { get; set; }
        public string ValidTo { get; set; }
        public bool IsOffer { get; set; }
        public string ApplyingOffer { get; set; }
        public Int64 Supplier { get; set; }
        public decimal AgentMarkupOffer { get; set; }
        public decimal AgentMarkup { get; set; }
    }
}
