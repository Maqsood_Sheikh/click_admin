﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Response
{
    public class sRoom
    {
        public string onRequest { get; set; }
        public string availCount { get; set; }
        public string SHRUI { get; set; }
        public string Boardtype { get; set; }
        public string Boardcode { get; set; }
        public string Boardshortname { get; set; }
        public string Boardtext { get; set; }
        public string Roomtype { get; set; }
        public string Roomcode { get; set; }
        public string Roomcharacteristic { get; set; }
        public string Roomtext { get; set; }
        public float RoomAmount { get; set; }
        public float CUTRoomAmount { get; set; }
        public float AgentRoomMarkup { get; set; }
        public string Roomdate { get; set; }
        public string Roomtime { get; set; }
        public List<float> CancellationAmount { get; set; }
        public List<float> CUTCancellationAmount { get; set; }
        public List<float> AgentCancellationMarkup { get; set; }
        public DateTime NoChargeDate { get; set; }
        public List<DateTime> ChargeDate { get; set; }
        public string sNoChargeDate { get; set; }
        public List<string> sChargeDate { get; set; }
        public int RoomCount { get; set; }
        public int AdultCount { get; set; }
        public int ChildCount { get; set; }
        public List<float> StaffRoomMarkup { get; set; }
        public List<float> StaffCancellationMarkup { get; set; }
    }
}
