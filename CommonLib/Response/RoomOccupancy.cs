﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Response
{
    public class RoomOccupancy
    {
        public List<int> noRooms { get; set; }
        public string RoomID { get; set; }
        public List<sRoom> sRoom { get; set; }
        public float RoomPrice { get; set; }
        public float CUTRoomPrice { get; set; }
        public float AgentMarkup { get; set; }
        public float StaffMarkup { get; set; }
      
    }
}
