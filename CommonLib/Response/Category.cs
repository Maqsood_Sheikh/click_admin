﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Response
{
    public class Category
    {
        public string Name { get; set; }
        public string HotelCode { get; set; }
        public string Supplier { get; set; }
        public int Count { get; set; }
    }
}
