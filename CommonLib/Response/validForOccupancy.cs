﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Response
{
    public class validForOccupancy
    {
        public Int64 adults { get; set; }
        public Int64 children { get; set; }
        public string childrenAges { get; set; }
        public Int64 extraBed { get; set; }
        public string extraBedOccupant { get; set; }
    }
}
