﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Response
{
    public class special
    {
        public int runno { get; set; }
        public string type { get; set; }
        public string specialName { get; set; }
        public string upgradeToRoomId { get; set; }
        public string upgradeToMealId { get; set; }
        public string condition { get; set; }
        public string description { get; set; }
        public string notes { get; set; }
        public string discount { get; set; }
        public string stay { get; set; }
        public string discountedNights { get; set; }
        public string pay { get; set; }
    }
}
