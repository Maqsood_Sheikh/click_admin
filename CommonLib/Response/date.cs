﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Response
{
  public  class date
    {
        public string InventoryType { get; set; }
        public string Type { get; set; }
        public string Currency { get; set; }
        public string MealPlan { get; set; }
        public int runno { get; set; }
        public string datetime { get; set; }
        public string day { get; set; }
        public string wday { get; set; }
        public float price { get; set; }

        public float S2SMarkup { get; set; }
        public float AdminMarkup { get; set; }
        public float Markup { get; set; }
        public float RoomRate { get; set; }

        public float ChidWBedRate { get; set; }
        public float CNBRate { get; set; }
        public float EBRate { get; set; }

        public Int64 noCWB { get; set; }
        public Int64 noCNB { get; set; }
        public Int64 noEXB { get; set; }


        public List<string> offerID { get; set; }
        public int RoomTypeId { get; set; }
        public int RateTypeId { get; set; }
        public List<InventoryType> NoOfInventory { get; set; }
        public float OfferRate { get; set; }
        public float Total { get; set; }
        public int dayOnRequest { get; set; }
        public List<includedMeal> including { get; set; }
        public string discount { get; set; }
        public string discountInfoText { get; set; }
        public List<TaxRate> ListRates { get; set; }
        public Int64 NoOfCount { get; set; }
    }
  public class MarkupCommission
  {
      public Markup S2SMarkup { get; set; }
      public Markup GlobalMarkup { get; set; }
      public Markup GroupMarkup { get; set; }
      public Markup IndividualMarkup { get; set; }
      public Markup SupplierMarkup { get; set; }
  }

  public class Markup
  {
      public float MarkupPer { get; set; }
      public float MarkupAmt { get; set; }
      public float CommessionPer { get; set; }
      public float CommessionAmt { get; set; }
  }

  public class TaxRate
  {
      public float ID { get; set; }
      public float BaseRate { get; set; }
      public float Per { get; set; }
      public string RateName { get; set; }
      public List<Tax> TaxOn { get; set; }
      public float  TotalRate { get; set; }
      public string Type { get; set; }


  }
  public class Tax
  {
      public Int64  ID { get; set; }
      public string TaxName { get; set; }
      public float TaxPer { get; set; }
      public float TaxRate { get; set; }
  }
}
